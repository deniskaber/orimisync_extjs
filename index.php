<?php
session_start ();
$legend = 'Orimi';
$legend2 = false;
function generate_pass ($number) {
  $arr = array ('1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
  $pass = "";
  for ($i = 0; $i < $number; $i++) {
    $index = mt_rand (0, count ($arr) - 1);
    $pass .= $arr[$index];
  }
  return $pass;
}
function sms_send ($phone = null, $code = null) {
  if ( !$phone || !$code )
    return FALSE;
  if (substr($phone,0,1) == '8')
    $phone = '7'.substr($phone,1,strlen($phone));
  if (substr($phone,0,2) == '+7')
    $phone = '7'.substr($phone,2,strlen($phone));
    return file_get_contents ('http://gate.smsaero.ru/send/?user=berezin@lasmart.ru&password='.md5('xu3thooo').'&to='.urlencode($phone).'&text='.urlencode($code).'&from='.urlencode('RDS Portal').'&answer=json');
}
function ms_escape_string($data) {
  if ( !isset($data) or empty($data) ) return '';
  if ( is_numeric($data) ) return $data;

  $non_displayables = array(
    '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
    '/%1[0-9a-f]/',             // url encoded 16-31
    '/[\x00-\x08]/',            // 00-08
    '/\x0b/',                   // 11
    '/\x0c/',                   // 12
    '/[\x0e-\x1f]/'             // 14-31
  );
  foreach ( $non_displayables as $regex )
    $data = preg_replace( $regex, '', $data );
  $data = str_replace("'", "''", $data );
  return $data;
}

if( (isset($_REQUEST['submit']) && !empty($_REQUEST['login']) && !empty($_REQUEST['pass']))
or  (isset($_REQUEST['login']) && isset($_REQUEST['time']) && isset($_REQUEST['hash']))){
  require_once 'conf_info.php';
  $connect = mssql_connect ($_PAR['host'], $_PAR['login'], $_PAR['pass']) or die ("Could not connect to MSSQL");
  mssql_select_db($_PAR['db'],$connect);

  $name = strip_tags(trim(ms_escape_string($_REQUEST['login'])));
  $pass = strip_tags(trim(ms_escape_string($_REQUEST['pass'])));
  $query = "SELECT top 1 [ID],[isDeleted],case when [DateEndAccess] >= getdate() then 0 else 1 end as [expired], [admin] from [lasmart_v_sync_Users] WHERE [Name] COLLATE Cyrillic_General_CI_AS ='$name' AND [Password] COLLATE Cyrillic_General_CS_AS ='$pass'";

  $result = mssql_query($query);
  if(mssql_num_rows($result)) {
    while ($row = mssql_fetch_assoc($result)) {
        if ($row ['isDeleted'] == 0) {
          if ($row ['expired'] == 0) {
            $_SESSION ['db_pwd'] = $pass;
            $_SESSION ['db_log'] = $name;
            $_SESSION ['admin'] = $row['admin'];
            $_SESSION ['idUser'] = $row ['ID'];
            //$_SESSION ['ph'] = $row["Tel1"];
            //$_SESSION ['code'] = generate_pass(6);
            $_SESSION ['HTTP_USER_AGENT'] = md5($_SERVER ['HTTP_USER_AGENT']);
            $_SESSION ['UserIP'] = $_SERVER ['REMOTE_ADDR'];
            /*if ($row["Sms"]) {
              $response = sms_send ($_SESSION ['ph'], "Код для авторизации: ".$_SESSION ['code']);
              if (!$response) {
                echo 'Ошибка отправки SMS - сообщения!';
                header('Location: index.php');
                die ();
              }
              header('Location: sms_check.php');
              die();
            }*/
              $_SESSION['auth'] = 1;
              switch ($_SESSION ['admin']) {
                case 1 :
                  header ('Location: admin/admin.php');
                  die();
                Break;
                default:
                  header ('Location: portal.php');
                  die();
                Break;
            }
          } else {
            $legend2 = '<span class="errorIn">Время доступа к системе истекло. Обратитесь к администратору системы</span>';
          }
        } else {
          $legend2 = '<span class="errorIn">Пользователь неактивен. Обратитесь к администратору системы</span>';
        }
    }
  } else {
    $legend2 = '<span class="errorIn">Неправильные данные</span>';
  }
  mssql_close($connect);
}

?><!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
  <meta http-equiv="Content-Language" content="ru">
	<title>НСИ - авторизация</title>
	<link rel="stylesheet" href="resources/css/admin.css">
	<!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
  <section class="container">
    <div class="login">
      <h1>Войти в систему</h1>

      <form method="post" action="">
      <fieldset>
      <legend>
          <?php
            if($legend2) {
              print_r($legend2);
            }
          ?>
        </legend>
        <p><input type="text" name="login" id="login" value="" placeholder="Логин или Email"></p>
        <p><input type="password" name="pass" id="pass" value="" placeholder="Пароль"></p>
        <!--p class="remember_me">
          <label>
            <input type="checkbox" name="remember_me" id="remember_me">
            Запомнить меня
          </label>
        </p-->
        <p class="submit"><input type="submit" name="submit" value="Войти"></p>
        </fieldset>
      </form>
    </div>

    <!--div class="login-help">
      <a href="index.php">Забыли пароль?</a> Восстановите его!
    </div-->
  </section>
</body>
  <script type="text/javascript">
    document.getElementsByTagName('input')[0].focus();
  </script>
</html>

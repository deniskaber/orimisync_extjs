# TestOrimi/app

This folder contains the javascript files for the application.

# TestOrimi/resources

This folder contains static resources (typically an `"images"` folder as well).

# TestOrimi/overrides

This folder contains override classes. All overrides in this folder will be 
automatically included in application builds if the target class of the override
is loaded.

# TestOrimi/sass/etc

This folder contains misc. support code for sass builds (global functions, 
mixins, etc.)

# TestOrimi/sass/src

This folder contains sass files defining css rules corresponding to classes
included in the application's javascript code build.  By default, files in this 
folder are mapped to the application's root namespace, 'TestOrimi'. The
namespace to which files in this directory are matched is controlled by the
app.sass.namespace property in TestOrimi/.sencha/app/sencha.cfg. 

# TestOrimi/sass/var

This folder contains sass files defining sass variables corresponding to classes
included in the application's javascript code build.  By default, files in this 
folder are mapped to the application's root namespace, 'TestOrimi'. The
namespace to which files in this directory are matched is controlled by the
app.sass.namespace property in TestOrimi/.sencha/app/sencha.cfg. 

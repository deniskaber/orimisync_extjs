<?php
	require_once 'config.php';

	function getFilterQuery($filter) {
		$where = '';
		if (is_array($filter)) {
			for ($i=0;$i<count($filter);$i++) {
				$value = iconv("UTF-8", "CP1251",$filter[$i]['value']);
				if (strpos($value,'=') === 0)
					$qs .= " AND replace(cast([".$filter[$i]['property']."] as varchar(max)),'.00','') = '".substr($value,1)."'";
				else if (strpos($value,'>') === 0)
					$qs .= " AND [".$filter[$i]['property']."] > '".substr($value,1)."'";
				else if (strpos($value,'<') === 0)
					$qs .= " AND [".$filter[$i]['property']."] < '".substr($value,1)."'";
				else
					$qs .= " AND [".$filter[$i]['property']."] like '%".$value."%'";
			}
			$where .= $qs;
		}

		return $where;
	}

	function parseFilters($_REQUEST, $refType) {
		$qs = '';
		$where = "where 0 = 0 ";
		if (isset($_REQUEST['filter'])) {
			$filter = json_decode(str_replace("'","",strip_tags(trim($_REQUEST['filter']))), true);
			$where .= getFilterQuery($filter);
		}
		if (isset($_REQUEST['masterFilters'])) {
			$filter = json_decode(str_replace("'","",strip_tags(trim($_REQUEST['masterFilters']))), true);
			$where .= getFilterQuery($filter);
		}
		if (isset($_REQUEST['slaveFilters'])) {
			$filter = json_decode(str_replace("'","",strip_tags(trim($_REQUEST['slaveFilters']))), true);
			$where .= getFilterQuery($filter);
		}
		if (isset($_REQUEST['searchall'])) {
			if (!empty($_REQUEST['searchall'])) {
				$searchall = str_replace("'","",strip_tags(trim($_REQUEST['searchall'])));
				switch ($refType) {
					//Поля справочников без ID
					case 'masterGood':
						$fields = array('Parent','Code','Name','ShortName','TempName','From','Manufacturer','Brand','SubBrand'
						,'Category','Segment','SubSegment','TeaColor','ExternalPack','SachetPack','InternalPack','BagQty','BagWeight','Weight'
						,'WeightGroup','isWithFlavor','Flavor','isWithAdding','Adding','isHorekaPack','PriceSegment','Country','CoffeeSort'
						,'isCaffeine','Gift','GiftType','Photo','Length','Width','Height','isWeightNet','Comment','Collection','isPrivateMark'
						,'Barcode','Author','DateChange','Status');
					break;
					case 'syncedGood':
						$fields = array('Code','OrimiCode','Name','From','Manufacturer','Brand','Category','Segment','TeaColor'
						,'Pack','BagQty','BagWeight','Weight','isWithFlavor','isWithAdding','PriceSegment','Country','DataSource','DateChange','SyncType');
					break;
					case 'slaveGood':
						$fields = array('Code','OrimiCode','Name','From','Manufacturer','Brand','Category','Segment','TeaColor'
						,'Pack','BagQty','BagWeight','Weight','isWithFlavor','isWithAdding','PriceSegment','Country','DataSource','DateChange');
					break;
					case 'masterStores':
						$fields = array('Code','Name','TempAddress','FullAddress','Address',
						'Country','PostCode','County','Region','District','City','Street','Building',
						'Division','Area','Sector','Channel','SubChannel','Format',
						'isNetName','Net','NetType','isNielsen','RespMerch','RespManager',
						'ContractorGroup','DateOpen','DateClose','ShopArea','Coordinates','GLN',
						'Comment','FIAS','isInRBP','RespTR','Selection','AdvertisingChannel',
						'Author','DateChange','Status');
						//isConsignee
					break;
					case 'syncedStores':
						$fields = array('Code','DistrCode','Name','TempAddress','County','Region','City','Division','Sector'
						,'SubChannel','Format','isNet','Net','ContractorGroup','DataSource','DateChange','SyncType');
					break;
					case 'slaveStores':
						$fields = array('DataSource','DateChange','DistrCode','Code','Name','TempAddress'
						,'County','Region','City','Division','Sector','SubChannel','Format','isNet','Net','ContractorGroup');
					break;
					case 'masterStaff':
						$fields = array('Code','SurName','Name','LastName','FIO','BirthDate','Type','Division','DateHired','DateFired','EMail',
						'Phone','Comment','Motivation','FeatureName','Sector','Parent','ConsiderInTeam','Status','Author','DateChange');
					break;
					case 'syncedStaff':
						$fields = array('Code','FIO','BirthDate','Type','Division','DateHired','DateFired','EMail','Phone','Motivation'
						,'Feature','Sector','Parent','DataSource','DateChange','SyncType');
					break;
					case 'slaveStaff':
						$fields = array('DataSource','DateChange','Code','FIO','BirthDate','Type','Division','DateHired','DateFired'
						,'EMail','Phone','Motivation','Feature','Sector','Parent');
					break;
					case 'masterDistributors':
						$fields = array('Code','Name','TempAddress','FullAddress','Address',
						'Country','PostCode','County','Region','District','City','Street','Building',
						'Division','Area','Sector','Channel','SubChannel','Format',
						'RespManager', 'ContractorGroup','Currancy','isSubDistributor','isAccounting','NonAccReason',
						'DateOpen','DateClose','ShopArea','Coordinates','GLN',
						'Comment','FIAS','isInRBP', 'Author','DateChange','Status');
					break;
					case 'masterWarehouses':
						$fields = array('Code','Name','TempAddress','FullAddress','Address',
						'Country','PostCode','County','Region','District','City','Street','Building',
						'Division','Area','Sector','Format','DateOpen','DateClose','ShopArea',
						'Comment','FIAS','Author','DateChange','Status');
					case 'syncedWarehouses':
						$fields = array('Code','DistrCode','Name','TempAddress','County','Region','City','Division','Sector'
						,'DataSource','DateChange','SyncType');
					break;
					case 'slaveWarehouses':
						$fields = array('DataSource','DateChange','DistrCode','Code','Name','TempAddress'
						,'County','Region','City','Division','Sector');
					break;
					case 'advertisingBossHeaders':
						$fields = array('DocNum','Title','DateBegin','DateEnd','Status','Author');
					break;
					case 'advertisingBossRows':
						$fields = array('GoodID','GoodName','Brand','OrimiCode','FGD','MinPrice','BPLPrice','MaxDiscount','OrderID');
					break;
					case 'advertisingEventsHeaders':
						$fields = array('DocNum','Title','DocDate','Area','Status','ContractorGroup','Net','StoreName','StoreAddress','StoreAmount',
							'MaxNumberToShip','AdvertisingChannel','DateStart','DateEnd','Comment','Author','DateChange', 'isInTradent', 'TSZCode', 'PaymentType');
					break;
					case 'advertisingEventsRows':
						$fields = array('RequestID',
						'GoodID','GoodName','Brand','OrimiCode','Segment','ExternalPack','InternalPack','Comment','AmountOfDaysAfterPrev',
						'OTDiscount','DistrDiscount','NetDiscount','MaxDiscountDepth','FGD','MinPrice','BPLPrice','DistrIncomePrice','NetIncomePriceBefore',
						'SellingPriceBefore','NetMarkup','EstimatedSellingPrice','FactDiscount','SupplyPrice','NetIncomePriceAction','IncomePriceDifference',
						'InvoiceDiscount','SalesAmountBeforeQty','SalesAmountBeforeVal','EstimatedSalesAmountQty','EstimatesSalesAmountVal','EstimatesSalesAmountFGD','EstimatesSalesAmountD0Bonus','EstimatedSalesAmountQtyPerStr','CataloguePubCost',
						'RealSellingPrice','RealSalesAmountQty','RealSalesAmountVal','RealSalesAmountInRegularPrices','DistrCost','OTCost','FGDComp','D0Comp','SalesIncrease','IncreasePercent');
					break;
				}
				if (strpos($searchall,'=') === 0) {
					$where .= " and ([ID] = '".iconv("UTF-8", "CP1251",substr($searchall,1))."'";
					for ($i=0;$i<count($fields);$i++) {
						$where .= " or cast([".$fields[$i]."] as varchar(max)) = '".iconv("UTF-8", "CP1251",substr($searchall,1))."'";
					}
				} else {
					$where .= " and ([ID] like '%".iconv("UTF-8", "CP1251",$searchall)."%'";
					for ($i=0;$i<count($fields);$i++) {
						$where .= " or [".$fields[$i]."] like '%".iconv("UTF-8", "CP1251",$searchall)."%'";
					}
				}
				$where .= ")";
			}
		}
		
		if (isset($_SESSION['SkuStatus'])) {
			if ($refType == 'masterGood')
				$where .= ' and StatusID in ('.$_SESSION['SkuStatus'].')';
		}

		if (isset($_SESSION['TTStatus'])) {
			if ($refType == 'masterStores')
				$where .= ' and StatusID in ('.$_SESSION['TTStatus'].')';
		}

		if (isset($_SESSION['StaffStatus'])) {
			if ($refType == 'masterStaff')
				$where .= ' and StatusID in ('.$_SESSION['StaffStatus'].')';
		}

		if (isset($_SESSION['WrhsStatus'])) {
			if ($refType == 'masterWarehouses')
				$where .= ' and StatusID in ('.$_SESSION['WrhsStatus'].')';
		}

		if (isset($_SESSION['SkuSlaveSource'])) {
			if ($refType == 'syncedGood' || $refType == 'slaveGood')
				$where .= ' and DataSourceID in ('.$_SESSION['SkuSlaveSource'].')';
		}

		if (isset($_SESSION['TTSlaveSource'])) {
			if ($refType == 'syncedStores' || $refType == 'slaveStores')
				$where .= ' and DataSourceID in ('.$_SESSION['TTSlaveSource'].')';
		}

		if (isset($_SESSION['StaffSlaveSource'])) {
			if ($refType == 'syncedStaff' || $refType == 'slaveStaff')
				$where .= ' and DataSourceID in ('.$_SESSION['StaffSlaveSource'].')';
		}

		if (isset($_SESSION['WrhsSlaveSource'])) {
			if ($refType == 'syncedWarehouses' || $refType == 'slaveWarehouses')
				$where .= ' and DataSourceID in ('.$_SESSION['WrhsSlaveSource'].')';
		}

		if (isset($_SESSION['Sectors'])) {
			if ($refType == 'masterStores')
				$where .= ' and [AreaID] in ('.$_SESSION['Sectors'].')';
		}

		if (isset($_SESSION['SectorNames'])) {
			if ($refType == 'syncedStores' || $refType == 'slaveStores')
				$where .= ' and ([Sector] in ('.$_SESSION['SectorNames'].') or (DataSourceID <> 3))';
		}

		if (isset($_SESSION['Nets'])) {
			if ($refType == 'masterStores')
				$where .= ' and [NetID] in ('.$_SESSION['Nets'].')';
		}

		if (isset($_SESSION['Categories'])) {
			if ($refType == 'masterGood')
				$where .= ' and [CategoryID] in ('.$_SESSION['Categories'].')';
		}

		if (isset($_SESSION['Manufacturers'])) {
			if ($refType == 'masterGood')
				$where .= ' and [ManufacturerID] in ('.$_SESSION['Manufacturers'].')';
		}

		if (isset($_SESSION['TT_isNet'])) {
			if ($refType == 'masterStores')
				$where .= ' and [isNet] in ('.$_SESSION['TT_isNet'].')';
		}

		if (isset($_SESSION['TT_isConsignee'])) {
			if ($refType == 'masterStores')
				$where .= ' and isnull([isConsignee],0) in ('.$_SESSION['TT_isConsignee'].')';
		}

		return $where;
	}

	function parseSorters($_REQUEST) {
		$Sort =	(isset($_REQUEST['sort']))	?	str_replace("'","",strip_tags(trim($_REQUEST['sort'])))	:	'';
		$Dir =	(isset($_REQUEST['dir']))	?	str_replace("'","",strip_tags(trim($_REQUEST['dir'])))	:	'';

		$order = strlen($Sort.$Dir) ? "order by $Sort $Dir" : '';

		return $order;
	}

	function updateParseParameter($par) {
		if (isset($par)) {
			if (strlen($par)) {
				$par = str_replace("'","",strip_tags(trim($par)));
			} else {
				$par = -1;
			}
		} else {
			$par = 'null';
		}
		return $par;
	}

	function updateParseParameterWithEncode($par) {
		if (isset($par)) {
			if (strlen($par)) {
				$par = "'".iconv("UTF-8", "CP1251",str_replace("'","",(strip_tags(trim($par)))))."'";
			} else {
				$par = "'blank'";
			}
		} else {
			$par = 'null';
		}
		return $par;
	}

	function parseParameter($par) {
		if (isset($par)) {
			if (strlen($par)) {
				$par = str_replace("'","",strip_tags(trim($par)));
			} else {
				$par = 'null';
			}
		} else {
			$par = 'null';
		}
		return $par;
	}

	function parseParameterWithEncode($par) {
		if (isset($par)) {
			if (strlen($par)) {
				$par = "'".iconv("UTF-8", "CP1251",str_replace("'","",strip_tags(trim($par))))."'";
			} else {
				$par = 'null';
			}
		} else {
			$par = 'null';
		}
		return $par;
	}

	function getPortalData($connection, $_REQUEST) {
		$arr = array();

		$idUser = $_SESSION ['idUser'];
		$SkuStatus = '';
		$TTStatus = '';
		$StaffStatus = '';
		$DistrStatus = '';
		$WrhsStatus = '';

		$SkuSlaveSource = '';
		$TTSlaveSource = '';
		$StaffSlaveSource = '';
		$WrhsSlaveSource = '';

		$query = "select top 1 * FROM [lasmart_v_sync_PortalData] where ID = $idUser";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gpd.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['FullName'] = iconv("cp1251", "UTF-8", $row['FullName']);

			$arr [] = $row;
			
			$_SESSION['RoleID'] = $row['RoleID'];

			if (!($row['Sku_draft_get'] && $row['Sku_active_get'] && $row['Sku_deleted_get'])) {
				if ($row['Sku_draft_get'])
					$SkuStatus .= '1,';
				if ($row['Sku_active_get'])
					$SkuStatus .= '2,';
				if ($row['Sku_deleted_get'])
					$SkuStatus .= '3,';
			}

			if (!($row['TT_draft_get'] && $row['TT_active_get'] && $row['TT_deleted_get'])) {
				if ($row['TT_draft_get'])
					$TTStatus .= '1,';
				if ($row['TT_active_get'])
					$TTStatus .= '2,';
				if ($row['TT_deleted_get'])
					$TTStatus .= '3,';
			}

			if (!($row['Staff_draft_get'] && $row['Staff_active_get'] && $row['Staff_deleted_get'])) {
				if ($row['Staff_draft_get'])
					$StaffStatus .= '1,';
				if ($row['Staff_active_get'])
					$StaffStatus .= '2,';
				if ($row['Staff_deleted_get'])
					$StaffStatus .= '3,';
			}

			if (!($row['Distr_draft_get'] && $row['Distr_active_get'] && $row['Distr_deleted_get'])) {
				if ($row['Distr_draft_get'])
					$DistrStatus .= '1,';
				if ($row['Distr_active_get'])
					$DistrStatus .= '2,';
				if ($row['Distr_deleted_get'])
					$DistrStatus .= '3,';
			}

			if (!($row['Wrhs_draft_get'] && $row['Wrhs_active_get'] && $row['Wrhs_deleted_get'])) {
				if ($row['Wrhs_draft_get'])
					$WrhsStatus .= '1,';
				if ($row['Wrhs_active_get'])
					$WrhsStatus .= '2,';
				if ($row['Wrhs_deleted_get'])
					$WrhsStatus .= '3,';
			}

			if (!($row['Sku_slave_tradent_get'] && $row['Sku_slave_net_get'] && $row['Sku_slave_distr_get'])) {
				if ($row['Sku_slave_tradent_get'])
					$SkuSlaveSource .= '1,';
				if ($row['Sku_slave_net_get'])
					$SkuSlaveSource .= '2,';
				if ($row['Sku_slave_distr_get'])
					$SkuSlaveSource .= '3,';
			}

			if (!($row['TT_slave_tradent_get'] && $row['TT_slave_net_get'] && $row['TT_slave_distr_get'])) {
				if ($row['TT_slave_tradent_get'])
					$TTSlaveSource .= '1,';
				if ($row['TT_slave_net_get'])
					$TTSlaveSource .= '2,';
				if ($row['TT_slave_distr_get'])
					$TTSlaveSource .= '3,';
			}

			if (!($row['Staff_slave_tradent_get'] && $row['Staff_slave_net_get'] && $row['Staff_slave_distr_get'])) {
				if ($row['Staff_slave_tradent_get'])
					$StaffSlaveSource .= '1,';
				if ($row['Staff_slave_net_get'])
					$StaffSlaveSource .= '2,';
				if ($row['Staff_slave_distr_get'])
					$StaffSlaveSource .= '3,';
			}

			if (!($row['Wrhs_slave_tradent_get'] && $row['Wrhs_slave_net_get'] && $row['Wrhs_slave_distr_get'])) {
				if ($row['Wrhs_slave_tradent_get'])
					$WrhsSlaveSource .= '1,';
				if ($row['Wrhs_slave_net_get'])
					$WrhsSlaveSource .= '2,';
				if ($row['Wrhs_slave_distr_get'])
					$WrhsSlaveSource .= '3,';
			}

			if ($row['SectorIDs'] != -1) {
				//These are Areas in fact!!!
				$_SESSION['Sectors'] = $row['SectorIDs'];
			}

			if ($row['SectorNames'] && strlen($row['SectorNames']) > 2) {
				//These are Areas in fact!!!
				$_SESSION['SectorNames'] = $row['SectorNames'];
			}

			if ($row['NetIDs'] != -1) {
				$_SESSION['Nets'] = $row['NetIDs'];
			}

			if ($row['CategoryIDs'] != -1) {
				$_SESSION['Categories'] = $row['CategoryIDs'];
			}

			if ($row['ManufacturerIDs'] != -1) {
				$_SESSION['Manufacturers'] = $row['ManufacturerIDs'];
			}

			if ($row['custom']) {
				$TT_isNet = '';
				$TT_isConsignee = '';

				if (!($row['TT_isNet_0'] && $row['TT_isNet_1'] && $row['TT_isNet_2'])) {
					if ($row['TT_isNet_0'])
						$TT_isNet .= '0,';
					if ($row['TT_isNet_1'])
						$TT_isNet .= '1,';
					if ($row['TT_isNet_2'])
						$TT_isNet .= '2,';
				}

				if (strlen($TT_isNet)) {
					$TT_isNet = substr($TT_isNet,0,-1);
					$_SESSION['TT_isNet'] = $TT_isNet;
				}

				if (!($row['TT_isConsignee_0'] && $row['TT_isConsignee_1'])) {
					if ($row['TT_isConsignee_0'])
						$TT_isConsignee .= '0,';
					if ($row['TT_isConsignee_1'])
						$TT_isConsignee .= '1,';
				}

				if (strlen($TT_isConsignee)) {
					$TT_isConsignee = substr($TT_isConsignee,0,-1);
					$_SESSION['TT_isConsignee'] = $TT_isConsignee;
				}
			}
		}
		if (strlen($SkuStatus)) {
			$SkuStatus = substr($SkuStatus,0,-1);
			$_SESSION['SkuStatus'] = $SkuStatus;
		}

		if (strlen($TTStatus)) {
			$TTStatus = substr($TTStatus,0,-1);
			$_SESSION['TTStatus'] = $TTStatus;
		}

		if (strlen($StaffStatus)) {
			$StaffStatus = substr($StaffStatus,0,-1);
			$_SESSION['StaffStatus'] = $StaffStatus;
		}

		if (strlen($WrhsStatus)) {
			$WrhsStatus = substr($WrhsStatus,0,-1);
			$_SESSION['WrhsStatus'] = $WrhsStatus;
		}

		if (strlen($SkuSlaveSource)) {
			$SkuSlaveSource = substr($SkuSlaveSource,0,-1);
			$_SESSION['SkuSlaveSource'] = $SkuSlaveSource;
		}

		if (strlen($TTSlaveSource)) {
			$TTSlaveSource = substr($TTSlaveSource,0,-1);
			$_SESSION['TTSlaveSource'] = $TTSlaveSource;
		}

		if (strlen($StaffSlaveSource)) {
			$StaffSlaveSource = substr($StaffSlaveSource,0,-1);
			$_SESSION['StaffSlaveSource'] = $StaffSlaveSource;
		}

		if (strlen($WrhsSlaveSource)) {
			$WrhsSlaveSource = substr($WrhsSlaveSource,0,-1);
			$_SESSION['WrhsSlaveSource'] = $WrhsSlaveSource;
		}

		//ob_start();print_r($_REQUEST);print_r($_SESSION);echo "\n\n";var_dump($arr);file_put_contents('out_gpd.txt',ob_get_clean());
		return $arr;
	}

	function getMasterGoods($connection, $_REQUEST) {
		$arr = array();
		$ret = array();

		$where = parseFilters($_REQUEST, 'masterGood');
		$order = parseSorters($_REQUEST);
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));
		
		$query = "select top $top * FROM [lasmart_v_dim_MasterGood] $where $order";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$ret['items'] = $arr;

		$count = 0;
		$query = "select count(*) FROM [lasmart_v_dim_MasterGood] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		
		$ret['total'] = $count;

		return $ret;
	}

	function createMasterGoods($connection, $_REQUEST) {
		$arr = array();

		$UserID = $_SESSION['idUser'];
		$ParentID = 		parseParameter($_REQUEST['ParentID']);
		$SubBrandID = 		parseParameter($_REQUEST['SubBrandID']);
		$SubSegmentID = 	parseParameter($_REQUEST['SubSegmentID']);
		$TeaColorID = 		parseParameter($_REQUEST['TeaColorID']);
		$ExternalPackID = 	parseParameter($_REQUEST['ExternalPackID']);
		$SachetPackID = 	parseParameter($_REQUEST['SachetPackID']);
		$InternalPackID = 	parseParameter($_REQUEST['InternalPackID']);
		$BagWeight = 		parseParameter($_REQUEST['BagWeight']);
		$BagQty = 			parseParameter($_REQUEST['BagQty']);
		$isWeightNet = 		parseParameter($_REQUEST['isWeightNet']);
		$isHorekaPack = 	parseParameter($_REQUEST['isHorekaPack']);
		$GiftID = 			parseParameter($_REQUEST['GiftID']);
		$GiftTypeID = 		parseParameter($_REQUEST['GiftTypeID']);
		$isPrivateMark = 	parseParameter($_REQUEST['isPrivateMark']);
		$WeightGroupID = 	parseParameter($_REQUEST['WeightGroupID']);
		$isWithFlavor = 	parseParameter($_REQUEST['isWithFlavor']);
		$isWithAdding = 	parseParameter($_REQUEST['isWithAdding']);
		$Length = 			parseParameter($_REQUEST['Length']);
		$Width = 			parseParameter($_REQUEST['Width']);
		$Height = 			parseParameter($_REQUEST['Height']);
		$isCaffeine = 		parseParameter($_REQUEST['isCaffeine']);
		$PriceSegmentID = 	parseParameter($_REQUEST['PriceSegmentID']);
		$CountryID = 		parseParameter($_REQUEST['CountryID']);
		$CoffeeSortID = 	parseParameter($_REQUEST['CoffeeSortID']);
		$DataSourceID = 	parseParameter($_REQUEST['DataSourceID']);

		$Adding = 			parseParameterWithEncode($_REQUEST['Adding']);
		$Flavor = 			parseParameterWithEncode($_REQUEST['Flavor']);
		$Barcode = 			parseParameterWithEncode($_REQUEST['Barcode']);
		$Photo = 			parseParameterWithEncode($_REQUEST['Photo']);
		$Collection = 		parseParameterWithEncode($_REQUEST['Collection']);
		$TempName = 		parseParameterWithEncode($_REQUEST['TempName']);
		$Comment = 			parseParameterWithEncode($_REQUEST['Comment']);
		$Code = 			parseParameterWithEncode($_REQUEST['Code']);

		$query = "
		exec [lasmart_p_MasterGood_Create]
		@UserID = $UserID
		,@ParentID = $ParentID
		,@SubBrandID = $SubBrandID
		,@SubSegmentID = $SubSegmentID
		,@TeaColorID = $TeaColorID
		,@ExternalPackID = $ExternalPackID
		,@SachetPackID = $SachetPackID
		,@InternalPackID = $InternalPackID
		,@BagWeight = $BagWeight
		,@BagQty = $BagQty
		,@isWeightNet = $isWeightNet
		,@isHorekaPack = $isHorekaPack
		,@GiftID = $GiftID
		,@GiftTypeID = $GiftTypeID
		,@isPrivateMark = $isPrivateMark
		,@WeightGroupID = $WeightGroupID
		,@isWithAdding = $isWithAdding
		,@isWithFlavor = $isWithFlavor
		,@Length = $Length
		,@Width = $Width
		,@Height = $Height
		,@isCaffeine = $isCaffeine
		,@PriceSegmentID = $PriceSegmentID
		,@CountryID = $CountryID
		,@CoffeeSortID = $CoffeeSortID
		,@DataSourceID = $DataSourceID

		,@Adding = $Adding
		,@Flavor = $Flavor
		,@Barcode = $Barcode
		,@Photo = $Photo
		,@Collection = $Collection
		,@TempName = $TempName
		,@Comment = $Comment
		,@Code = $Code";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_cg.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function updateMasterGoods($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$ParentID = 		updateParseParameter($_REQUEST['ParentID']);
		$SubBrandID = 		updateParseParameter($_REQUEST['SubBrandID']);
		$SubSegmentID = 	updateParseParameter($_REQUEST['SubSegmentID']);
		$TeaColorID = 		updateParseParameter($_REQUEST['TeaColorID']);
		$ExternalPackID = 	updateParseParameter($_REQUEST['ExternalPackID']);
		$SachetPackID = 	updateParseParameter($_REQUEST['SachetPackID']);
		$InternalPackID = 	updateParseParameter($_REQUEST['InternalPackID']);
		$BagWeight = 		updateParseParameter($_REQUEST['BagWeight']);
		$BagQty = 			updateParseParameter($_REQUEST['BagQty']);
		$isWeightNet = 		updateParseParameter($_REQUEST['isWeightNet']);
		$isHorekaPack = 	updateParseParameter($_REQUEST['isHorekaPack']);
		$GiftID = 			updateParseParameter($_REQUEST['GiftID']);
		$GiftTypeID = 		updateParseParameter($_REQUEST['GiftTypeID']);
		$isPrivateMark = 	updateParseParameter($_REQUEST['isPrivateMark']);
		$WeightGroupID = 	updateParseParameter($_REQUEST['WeightGroupID']);
		$isWithFlavor = 	updateParseParameter($_REQUEST['isWithFlavor']);
		$isWithAdding = 	updateParseParameter($_REQUEST['isWithAdding']);
		$Length = 			updateParseParameter($_REQUEST['Length']);
		$Width = 			updateParseParameter($_REQUEST['Width']);
		$Height = 			updateParseParameter($_REQUEST['Height']);
		$isCaffeine = 		updateParseParameter($_REQUEST['isCaffeine']);
		$PriceSegmentID = 	updateParseParameter($_REQUEST['PriceSegmentID']);
		$CountryID = 		updateParseParameter($_REQUEST['CountryID']);
		$CoffeeSortID = 	updateParseParameter($_REQUEST['CoffeeSortID']);

		$Adding = 			updateParseParameterWithEncode($_REQUEST['Adding']);
		$Flavor = 			updateParseParameterWithEncode($_REQUEST['Flavor']);
		$Barcode = 			updateParseParameterWithEncode($_REQUEST['Barcode']);
		$Photo = 			updateParseParameterWithEncode($_REQUEST['Photo']);
		$Collection = 		updateParseParameterWithEncode($_REQUEST['Collection']);
		$TempName = 		updateParseParameterWithEncode($_REQUEST['TempName']);
		$Comment = 			updateParseParameterWithEncode($_REQUEST['Comment']);
		$Code = 			updateParseParameterWithEncode($_REQUEST['Code']);

		$query =
		"exec [lasmart_p_MasterGood_Update]
		@UserID = $UserID
		,@ID = $ID
		,@ParentID = $ParentID
		,@SubBrandID = $SubBrandID
		,@SubSegmentID = $SubSegmentID
		,@TeaColorID = $TeaColorID
		,@ExternalPackID = $ExternalPackID
		,@SachetPackID = $SachetPackID
		,@InternalPackID = $InternalPackID
		,@BagWeight = $BagWeight
		,@BagQty = $BagQty
		,@isWeightNet = $isWeightNet
		,@isHorekaPack = $isHorekaPack
		,@GiftID = $GiftID
		,@GiftTypeID = $GiftTypeID
		,@isPrivateMark = $isPrivateMark
		,@WeightGroupID = $WeightGroupID
		,@isWithAdding = $isWithAdding
		,@isWithFlavor = $isWithFlavor
		,@Length = $Length
		,@Width = $Width
		,@Height = $Height
		,@isCaffeine = $isCaffeine
		,@PriceSegmentID = $PriceSegmentID
		,@CountryID = $CountryID
		,@CoffeeSortID = $CoffeeSortID

		,@Adding = $Adding
		,@Flavor = $Flavor
		,@Barcode = $Barcode
		,@Photo = $Photo
		,@Collection = $Collection
		,@TempName = $TempName
		,@Comment = $Comment
		,@Code = $Code";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection)) {
            $message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0) {
                $arr['success']	= FALSE;
                $arr['msg']	= $message;
            } else
                $arr['success']	= TRUE;
        } else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function groupUpdateMasterGoods($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameterWithEncode($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не заданы ID позиций';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$ParentID = 		updateParseParameter($_REQUEST['ParentID']);
		$SubBrandID = 		updateParseParameter($_REQUEST['SubBrandID']);
		$SubSegmentID = 	updateParseParameter($_REQUEST['SubSegmentID']);
		$TeaColorID = 		updateParseParameter($_REQUEST['TeaColorID']);
		$ExternalPackID = 	updateParseParameter($_REQUEST['ExternalPackID']);
		$SachetPackID = 	updateParseParameter($_REQUEST['SachetPackID']);
		$InternalPackID = 	updateParseParameter($_REQUEST['InternalPackID']);
		$BagWeight = 		updateParseParameter($_REQUEST['BagWeight']);
		$BagQty = 			updateParseParameter($_REQUEST['BagQty']);
		$isWeightNet = 		updateParseParameter($_REQUEST['isWeightNet']);
		$isHorekaPack = 	updateParseParameter($_REQUEST['isHorekaPack']);
		$GiftID = 			updateParseParameter($_REQUEST['GiftID']);
		$GiftTypeID = 		updateParseParameter($_REQUEST['GiftTypeID']);
		$isPrivateMark = 	updateParseParameter($_REQUEST['isPrivateMark']);
		$WeightGroupID = 	updateParseParameter($_REQUEST['WeightGroupID']);
		$isWithFlavor = 	updateParseParameter($_REQUEST['isWithFlavor']);
		$isWithAdding = 	updateParseParameter($_REQUEST['isWithAdding']);
		$Length = 			updateParseParameter($_REQUEST['Length']);
		$Width = 			updateParseParameter($_REQUEST['Width']);
		$Height = 			updateParseParameter($_REQUEST['Height']);
		$isCaffeine = 		updateParseParameter($_REQUEST['isCaffeine']);
		$PriceSegmentID = 	updateParseParameter($_REQUEST['PriceSegmentID']);
		$CountryID = 		updateParseParameter($_REQUEST['CountryID']);
		$CoffeeSortID = 	updateParseParameter($_REQUEST['CoffeeSortID']);

		$Adding = 			updateParseParameterWithEncode($_REQUEST['Adding']);
		$Flavor = 			updateParseParameterWithEncode($_REQUEST['Flavor']);
		$Barcode = 			updateParseParameterWithEncode($_REQUEST['Barcode']);
		$Photo = 			updateParseParameterWithEncode($_REQUEST['Photo']);
		$Collection = 		updateParseParameterWithEncode($_REQUEST['Collection']);
		$TempName = 		updateParseParameterWithEncode($_REQUEST['TempName']);
		$Comment = 			updateParseParameterWithEncode($_REQUEST['Comment']);
		$Code = 			updateParseParameterWithEncode($_REQUEST['Code']);

		$query =
		"exec [lasmart_p_MasterGood_Update]
		@UserID = $UserID
		,@ID = $ID
		,@ParentID = $ParentID
		,@SubBrandID = $SubBrandID
		,@SubSegmentID = $SubSegmentID
		,@TeaColorID = $TeaColorID
		,@ExternalPackID = $ExternalPackID
		,@SachetPackID = $SachetPackID
		,@InternalPackID = $InternalPackID
		,@BagWeight = $BagWeight
		,@BagQty = $BagQty
		,@isWeightNet = $isWeightNet
		,@isHorekaPack = $isHorekaPack
		,@GiftID = $GiftID
		,@GiftTypeID = $GiftTypeID
		,@isPrivateMark = $isPrivateMark
		,@WeightGroupID = $WeightGroupID
		,@isWithAdding = $isWithAdding
		,@isWithFlavor = $isWithFlavor
		,@Length = $Length
		,@Width = $Width
		,@Height = $Height
		,@isCaffeine = $isCaffeine
		,@PriceSegmentID = $PriceSegmentID
		,@CountryID = $CountryID
		,@CoffeeSortID = $CoffeeSortID

		,@Adding = $Adding
		,@Flavor = $Flavor
		,@Barcode = $Barcode
		,@Photo = $Photo
		,@Collection = $Collection
		,@TempName = $TempName
		,@Comment = $Comment
		,@Code = $Code";

		//ob_start();var_dump($_REQUEST);echo "\n\n".$query;file_put_contents('out_gug.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getMasterStores($connection, $_REQUEST) {
		$arr = array();
		$ret = array();
		$notNeedCount = false;

		$where = parseFilters($_REQUEST, 'masterStores');
		$order = parseSorters($_REQUEST);

		if ($_SESSION['masterStores']['fitlers'] === $where){
			$notNeedCount = true;
		}

		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));

		$query = "select top $top * FROM [lasmart_v_dim_MasterStores] $where $order";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$ret['items'] = $arr;

		if (!$notNeedCount) {
			$count = 0;
			$query = "select count(*) FROM [lasmart_v_dim_MasterStores] $where";
			$result = mssql_query($query,$connection);
			while ($row = mssql_fetch_row($result)) {
				$count = $row[0];
			}
			
			$ret['total'] = $count;

			$_SESSION['masterStores']['fitlers'] = $where;
			$_SESSION['masterStores']['count'] = $count;
		} else {
			$ret['total'] = $_SESSION['masterStores']['count'];
		}

		return $ret;
	}

	function createMasterStores($connection, $_REQUEST) {
		$arr = array();

		$UserID = $_SESSION['idUser'];
		$Name = 				parseParameterWithEncode($_REQUEST['Name']);
		$TempAddress = 			parseParameterWithEncode($_REQUEST['TempAddress']);
		$PostCode = 			parseParameter($_REQUEST['PostCode']);
		$CityID = 				parseParameter($_REQUEST['CityID']);
		$Street = 				parseParameterWithEncode($_REQUEST['Street']);
		$Building = 			parseParameterWithEncode($_REQUEST['Building']);
		$ChannelID = 			parseParameter($_REQUEST['ChannelID']);
		$SubChannelID = 		parseParameter($_REQUEST['SubChannelID']);
		$FormatID = 			parseParameter($_REQUEST['FormatID']);
		$isNet = 				parseParameter($_REQUEST['isNet']);
		$NetID = 				parseParameter($_REQUEST['NetID']);
		$ContractorGroupID = 	parseParameter($_REQUEST['ContractorGroupID']);
		$DateOpen = 			parseParameterWithEncode($_REQUEST['DateOpen']);
		$DateClose = 			parseParameterWithEncode($_REQUEST['DateClose']);
		$ShopArea = 			parseParameter($_REQUEST['ShopArea']);
		$Coordinates = 			parseParameterWithEncode($_REQUEST['Coordinates']);
		$GLN = 					parseParameterWithEncode($_REQUEST['GLN']);
		$Comment = 				parseParameterWithEncode($_REQUEST['Comment']);
		$FIAS = 				parseParameterWithEncode($_REQUEST['FIAS']);
		$isInRBP = 				parseParameter($_REQUEST['isInRBP']);
		$RespTRID = 			parseParameter($_REQUEST['RespTRID']);
		$RespMerchID = 			parseParameter($_REQUEST['RespMerchID']);
		$DataSourceID = 		parseParameter($_REQUEST['DataSourceID']);
		$Code = 				parseParameterWithEncode($_REQUEST['Code']);
		$isConsignee = 			parseParameter($_REQUEST['isConsignee']);
		$AdvertisingChannelID = parseParameter($_REQUEST['AdvertisingChannelID']);

		$query = "
		exec [lasmart_p_MasterStores_Create]
		 @UserID = $UserID
		,@Name = $Name
		,@TempAddress = $TempAddress
		,@PostCode = $PostCode
		,@CityID = $CityID
		,@Street = $Street
		,@Building = $Building
		,@ChannelID = $ChannelID
		,@SubChannelID = $SubChannelID
		,@FormatID = $FormatID
		,@isNet = $isNet
		,@NetID = $NetID
		,@ContractorGroupID = $ContractorGroupID
		,@DateOpen = $DateOpen
		,@DateClose = $DateClose
		,@ShopArea = $ShopArea
		,@Coordinates = $Coordinates
		,@GLN = $GLN
		,@Comment = $Comment
		,@FIAS = $FIAS
		,@isInRBP = $isInRBP
		,@RespTRID = $RespTRID
		,@RespMerchID = $RespMerchID
		,@DataSourceID = $DataSourceID
		,@Code = $Code
		,@isConsignee = $isConsignee
		,@AdvertisingChannelID = $AdvertisingChannelID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_cg.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function updateMasterStores($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$Name = 				updateParseParameterWithEncode($_REQUEST['Name']);
		$TempAddress = 			updateParseParameterWithEncode($_REQUEST['TempAddress']);
		$PostCode = 			updateParseParameter($_REQUEST['PostCode']);
		$CityID = 				updateParseParameter($_REQUEST['CityID']);
		$Street = 				updateParseParameterWithEncode($_REQUEST['Street']);
		$Building = 			updateParseParameterWithEncode($_REQUEST['Building']);
		$ChannelID = 			updateParseParameter($_REQUEST['ChannelID']);
		$SubChannelID = 		updateParseParameter($_REQUEST['SubChannelID']);
		$FormatID = 			updateParseParameter($_REQUEST['FormatID']);
		$isNet = 				updateParseParameter($_REQUEST['isNet']);
		$NetID = 				updateParseParameter($_REQUEST['NetID']);
		$ContractorGroupID = 	updateParseParameter($_REQUEST['ContractorGroupID']);
		$DateOpen = 			updateParseParameterWithEncode($_REQUEST['DateOpen']);
		$DateClose = 			updateParseParameterWithEncode($_REQUEST['DateClose']);
		$ShopArea = 			updateParseParameter($_REQUEST['ShopArea']);
		$Coordinates = 			updateParseParameterWithEncode($_REQUEST['Coordinates']);
		$GLN = 					updateParseParameterWithEncode($_REQUEST['GLN']);
		$Comment = 				updateParseParameterWithEncode($_REQUEST['Comment']);
		$FIAS = 				updateParseParameterWithEncode($_REQUEST['FIAS']);
		$isInRBP = 				updateParseParameter($_REQUEST['isInRBP']);
		$RespTRID = 			updateParseParameter($_REQUEST['RespTRID']);
		$RespMerchID = 			updateParseParameter($_REQUEST['RespMerchID']);
		$Code = 				updateParseParameterWithEncode($_REQUEST['Code']);
		$isConsignee = 			updateParseParameter($_REQUEST['isConsignee']);
		$AdvertisingChannelID = 			updateParseParameter($_REQUEST['AdvertisingChannelID']);

		$query =
		"exec [lasmart_p_MasterStores_Update]
			@UserID = $UserID
			,@ID = $ID
			,@Name = $Name
			,@TempAddress = $TempAddress
			,@PostCode = $PostCode
			,@CityID = $CityID
			,@Street = $Street
			,@Building = $Building
			,@ChannelID = $ChannelID
			,@SubChannelID = $SubChannelID
			,@FormatID = $FormatID
			,@isNet = $isNet
			,@NetID = $NetID
			,@ContractorGroupID = $ContractorGroupID
			,@DateOpen = $DateOpen
			,@DateClose = $DateClose
			,@ShopArea = $ShopArea
			,@Coordinates = $Coordinates
			,@GLN = $GLN
			,@Comment = $Comment
			,@FIAS = $FIAS
			,@isInRBP = $isInRBP
			,@RespTRID = $RespTRID
			,@RespMerchID = $RespMerchID
			,@Code = $Code
			,@isConsignee = $isConsignee
			,@AdvertisingChannelID = $AdvertisingChannelID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			ob_start();print_r($message);file_put_contents('out_gu.txt',ob_get_clean());
            if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0) {
                $arr['success']	= FALSE;
                $arr['msg']	= $message;
            } else {
                $arr['success']	= TRUE;
                if (strpos($message,'Смена канала для рекламы не произошла') === 0) {
                	$arr['msg']	= $message;
                }
            }
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		ob_start();var_dump($arr);file_put_contents('out_gu.txt',ob_get_clean());
		return $arr;
	}

	function groupUpdateMasterStores($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameterWithEncode($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не заданы ID позиций';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$Name = 				updateParseParameterWithEncode($_REQUEST['Name']);
		$TempAddress = 			updateParseParameterWithEncode($_REQUEST['TempAddress']);
		$PostCode = 			updateParseParameter($_REQUEST['PostCode']);
		$CityID = 				updateParseParameter($_REQUEST['CityID']);
		$Street = 				updateParseParameterWithEncode($_REQUEST['Street']);
		$Building = 			updateParseParameterWithEncode($_REQUEST['Building']);
		$ChannelID = 			updateParseParameter($_REQUEST['ChannelID']);
		$SubChannelID = 		updateParseParameter($_REQUEST['SubChannelID']);
		$FormatID = 			updateParseParameter($_REQUEST['FormatID']);
		$isNet = 				updateParseParameter($_REQUEST['isNet']);
		$NetID = 				updateParseParameter($_REQUEST['NetID']);
		$ContractorGroupID = 	updateParseParameter($_REQUEST['ContractorGroupID']);
		$DateOpen = 			updateParseParameterWithEncode($_REQUEST['DateOpen']);
		$DateClose = 			updateParseParameterWithEncode($_REQUEST['DateClose']);
		$ShopArea = 			updateParseParameter($_REQUEST['ShopArea']);
		$Coordinates = 			updateParseParameterWithEncode($_REQUEST['Coordinates']);
		$GLN = 					updateParseParameterWithEncode($_REQUEST['GLN']);
		$Comment = 				updateParseParameterWithEncode($_REQUEST['Comment']);
		$FIAS = 				updateParseParameterWithEncode($_REQUEST['FIAS']);
		$isInRBP = 				updateParseParameter($_REQUEST['isInRBP']);
		$RespTRID = 			updateParseParameter($_REQUEST['RespTRID']);
		$RespMerchID = 			updateParseParameter($_REQUEST['RespMerchID']);
		$isConsignee = 			updateParseParameter($_REQUEST['isConsignee']);
		$AdvertisingChannelID = 			updateParseParameter($_REQUEST['AdvertisingChannelID']);

		$query =
			"exec [lasmart_p_MasterStores_Update]
			@UserID = $UserID
			,@ID = $ID
			,@Name = $Name
			,@TempAddress = $TempAddress
			,@PostCode = $PostCode
			,@CityID = $CityID
			,@Street = $Street
			,@Building = $Building
			,@ChannelID = $ChannelID
			,@SubChannelID = $SubChannelID
			,@FormatID = $FormatID
			,@isNet = $isNet
			,@NetID = $NetID
			,@ContractorGroupID = $ContractorGroupID
			,@DateOpen = $DateOpen
			,@DateClose = $DateClose
			,@ShopArea = $ShopArea
			,@Coordinates = $Coordinates
			,@GLN = $GLN
			,@Comment = $Comment
			,@FIAS = $FIAS
			,@isInRBP = $isInRBP
			,@RespTRID = $RespTRID
			,@RespMerchID = $RespMerchID
			,@Code = null
			,@isConsignee = $isConsignee
			,@AdvertisingChannelID = $AdvertisingChannelID";

		//ob_start();var_dump($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0) {
                $arr['success']	= FALSE;
                $arr['msg']	= $message;
            } else {
                $arr['success']	= TRUE;
                if (strpos($message,'Смена канала для рекламы не произошла') === 0) {
                	$arr['msg']	= $message;
                }
            }
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	=  'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getMasterStaff($connection, $_REQUEST) {
		$arr = array();
		$ret = array();

		$where = parseFilters($_REQUEST, 'masterStaff');
		$order = parseSorters($_REQUEST);
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));

		$query = "select top $top * FROM [lasmart_v_dim_MasterStaff] $where $order";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$count = 0;
		$query = "select count(*) FROM [lasmart_v_dim_MasterStaff] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		$ret['items'] = $arr;
		$ret['total'] = $count;

		return $ret;
	}

	function createMasterStaff($connection, $_REQUEST) {
		$arr = array();

		$UserID = $_SESSION['idUser'];		
		$ParentID = 		parseParameter($_REQUEST['ParentID']);
		$SurName = 			parseParameterWithEncode($_REQUEST['SurName']);
		$Name = 			parseParameterWithEncode($_REQUEST['Name']);
		$LastName = 		parseParameterWithEncode($_REQUEST['LastName']);
		$FIO = 				parseParameterWithEncode($_REQUEST['FIO']);
		$BirthDate = 		parseParameterWithEncode($_REQUEST['BirthDate']);
		$TypeID = 			parseParameter($_REQUEST['TypeID']);
		$DivisionID = 		parseParameter($_REQUEST['DivisionID']);
		$DateHired = 		parseParameterWithEncode($_REQUEST['DateHired']);
		$DateFired = 		parseParameterWithEncode($_REQUEST['DateFired']);
		$EMail = 			parseParameterWithEncode($_REQUEST['EMail']);
		$Phone = 			parseParameterWithEncode($_REQUEST['Phone']);
		$Comment = 			parseParameterWithEncode($_REQUEST['Comment']);
		$MotivationID = 	parseParameter($_REQUEST['MotivationID']);
		$Feature = 			parseParameter($_REQUEST['Feature']);
		$ConsiderInTeam = 	parseParameter($_REQUEST['ConsiderInTeam']);
		$Sector = 			parseParameterWithEncode($_REQUEST['Sector']);
		$Area = 			parseParameterWithEncode($_REQUEST['Area']);
		$DataSourceID = 	parseParameter($_REQUEST['DataSourceID']);

		$Code = 			parseParameterWithEncode($_REQUEST['Code']);

		$query = "
		exec [lasmart_p_MasterStaff_Create]
		@UserID = $UserID
		,@ParentID = $ParentID
		,@SurName = $SurName
		,@Name = $Name
		,@LastName = $LastName
		,@FIO = $FIO
		,@BirthDate = $BirthDate
		,@TypeID = $TypeID
		,@DivisionID = $DivisionID
		,@DateHired = $DateHired
		,@DateFired = $DateFired
		,@EMail = $EMail
		,@Phone = $Phone
		,@Comment = $Comment
		,@MotivationID = $MotivationID
		,@Feature = $Feature
		,@ConsiderInTeam = $ConsiderInTeam
		,@Sector = $Sector
		,@Area = $Area
		,@DataSourceID = $DataSourceID
		,@Code = $Code";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_cg.txt',ob_get_clean());

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= mssql_get_last_message();
		}
		return $arr;
	}

	function updateMasterStaff($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$ParentID = 		updateParseParameter($_REQUEST['ParentID']);
		$SurName = 			updateParseParameterWithEncode($_REQUEST['SurName']);
		$Name = 			updateParseParameterWithEncode($_REQUEST['Name']);
		$LastName = 		updateParseParameterWithEncode($_REQUEST['LastName']);
		$FIO = 				updateParseParameterWithEncode($_REQUEST['FIO']);
		$BirthDate = 		updateParseParameterWithEncode($_REQUEST['BirthDate']);
		$TypeID = 			updateParseParameter($_REQUEST['TypeID']);
		$DivisionID = 		updateParseParameter($_REQUEST['DivisionID']);
		$DateHired = 		updateParseParameterWithEncode($_REQUEST['DateHired']);
		$DateFired = 		updateParseParameterWithEncode($_REQUEST['DateFired']);
		$EMail = 			updateParseParameterWithEncode($_REQUEST['EMail']);
		$Phone = 			updateParseParameterWithEncode($_REQUEST['Phone']);
		$Comment = 			updateParseParameterWithEncode($_REQUEST['Comment']);
		$MotivationID = 	updateParseParameter($_REQUEST['MotivationID']);
		$Feature = 			updateParseParameter($_REQUEST['Feature']);
		$ConsiderInTeam = 	parseParameter($_REQUEST['ConsiderInTeam']);
		$Sector = 			updateParseParameterWithEncode($_REQUEST['Sector']);
		$Area = 			updateParseParameterWithEncode($_REQUEST['Area']);

		$Code = 			updateParseParameterWithEncode($_REQUEST['Code']);

		$query =
		"exec [lasmart_p_MasterStaff_Update]
			@UserID = $UserID
			,@ID = $ID
			,@ParentID = $ParentID
			,@SurName = $SurName
			,@Name = $Name
			,@LastName = $LastName
			,@FIO = $FIO
			,@BirthDate = $BirthDate
			,@TypeID = $TypeID
			,@DivisionID = $DivisionID
			,@DateHired = $DateHired
			,@DateFired = $DateFired
			,@EMail = $EMail
			,@Phone = $Phone
			,@Comment = $Comment
			,@MotivationID = $MotivationID
			,@Feature = $Feature
			,@ConsiderInTeam = $ConsiderInTeam
			,@Sector = $Sector
			,@Area = $Area
			,@Code = $Code";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection)) {
            $message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0) {
                $arr['success']	= FALSE;
                $arr['msg']	= $message;
            } else
                $arr['success']	= TRUE;
        } else {
			$arr['success']	= FALSE;
			$arr['msg']	= mssql_get_last_message();
		}
		return $arr;
	}

	function groupUpdateMasterStaff($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameterWithEncode($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$ParentID = 		updateParseParameter($_REQUEST['ParentID']);
		$SurName = 			updateParseParameterWithEncode($_REQUEST['SurName']);
		$Name = 			updateParseParameterWithEncode($_REQUEST['Name']);
		$LastName = 		updateParseParameterWithEncode($_REQUEST['LastName']);
		$FIO = 				updateParseParameterWithEncode($_REQUEST['FIO']);
		$BirthDate = 		updateParseParameterWithEncode($_REQUEST['BirthDate']);
		$TypeID = 			updateParseParameter($_REQUEST['TypeID']);
		$DivisionID = 		updateParseParameter($_REQUEST['DivisionID']);
		$DateHired = 		updateParseParameterWithEncode($_REQUEST['DateHired']);
		$DateFired = 		updateParseParameterWithEncode($_REQUEST['DateFired']);
		$EMail = 			updateParseParameterWithEncode($_REQUEST['EMail']);
		$Phone = 			updateParseParameterWithEncode($_REQUEST['Phone']);
		$Comment = 			updateParseParameterWithEncode($_REQUEST['Comment']);
		$MotivationID = 	updateParseParameter($_REQUEST['MotivationID']);
		$Feature = 			updateParseParameter($_REQUEST['Feature']);
		$ConsiderInTeam = 	parseParameter($_REQUEST['ConsiderInTeam']);
		$Sector = 			updateParseParameterWithEncode($_REQUEST['Sector']);
		$Area = 			updateParseParameterWithEncode($_REQUEST['Area']);

		$query =
		"exec [lasmart_p_MasterStaff_Update]
			@UserID = $UserID
			,@ID = $ID
			,@ParentID = $ParentID
			,@SurName = $SurName
			,@Name = $Name
			,@LastName = $LastName
			,@FIO = $FIO
			,@BirthDate = $BirthDate
			,@TypeID = $TypeID
			,@DivisionID = $DivisionID
			,@DateHired = $DateHired
			,@DateFired = $DateFired
			,@EMail = $EMail
			,@Phone = $Phone
			,@Comment = $Comment
			,@MotivationID = $MotivationID
			,@Feature = $Feature
			,@ConsiderInTeam = $ConsiderInTeam
			,@Sector = $Sector
			,@Area = $Area";

        //ob_start();var_dump($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	/*function getStaff($connection, $_REQUEST) {
		$arr = array();
		$ret = array();

		//$where = parseFilters($_REQUEST, 'masterStaff');
		//$order = parseSorters($_REQUEST);
		//$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));

		$query = "select * FROM [lasmart_v_dim_Staff]";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$count = 0;
		$query = "select count(*) FROM [lasmart_v_dim_Staff] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		$ret['items'] = $arr;
		$ret['total'] = $count;

		return $arr;
	}*/

	function getMasterDistributors($connection, $_REQUEST) {
		$arr = array();
		$ret = array();
		$notNeedCount = false;

		$where = parseFilters($_REQUEST, 'masterDistributors');
		$order = parseSorters($_REQUEST);

		if ($_SESSION['masterDistributors']['fitlers'] === $where){
			$notNeedCount = true;
		}

		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));

		$query = "select top $top * FROM [lasmart_v_dim_MasterDistributors] $where $order";
		//ob_start();var_dump($_REQUEST);echo "\n\n".$query;file_put_contents('out_gmd.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$ret['items'] = $arr;

		if (!$notNeedCount) {
			$count = 0;
			$query = "select count(*) FROM [lasmart_v_dim_MasterDistributors] $where";
			$result = mssql_query($query,$connection);
			while ($row = mssql_fetch_row($result)) {
				$count = $row[0];
			}
			
			$ret['total'] = $count;

			$_SESSION['masterDistributors']['fitlers'] = $where;
			$_SESSION['masterDistributors']['count'] = $count;
		} else {
			$ret['total'] = $_SESSION['masterDistributors']['count'];
		}

		return $ret;
	}

	function createMasterDistributors($connection, $_REQUEST) {
		$arr = array();

		$UserID = $_SESSION['idUser'];
		$Name = 				parseParameterWithEncode($_REQUEST['Name']);
		$TempAddress = 			parseParameterWithEncode($_REQUEST['TempAddress']);
		$PostCode = 			parseParameter($_REQUEST['PostCode']);
		$CityID = 				parseParameter($_REQUEST['CityID']);
		$Street = 				parseParameterWithEncode($_REQUEST['Street']);
		$Building = 			parseParameterWithEncode($_REQUEST['Building']);
		$ChannelID = 			parseParameter($_REQUEST['ChannelID']);
		$SubChannelID = 		parseParameter($_REQUEST['SubChannelID']);
		$FormatID = 			parseParameter($_REQUEST['FormatID']);
		$ContractorGroupID = 	parseParameter($_REQUEST['ContractorGroupID']);
		$CurrancyID = 			parseParameter($_REQUEST['CurrancyID']);
		$isSubDistributor = 	parseParameter($_REQUEST['isSubDistributor']);
		$DateOpen = 			parseParameterWithEncode($_REQUEST['DateOpen']);
		$DateClose = 			parseParameterWithEncode($_REQUEST['DateClose']);
		$ShopArea = 			parseParameter($_REQUEST['ShopArea']);
		$Coordinates = 			parseParameterWithEncode($_REQUEST['Coordinates']);
		$GLN = 					parseParameterWithEncode($_REQUEST['GLN']);
		$Comment = 				parseParameterWithEncode($_REQUEST['Comment']);
		$FIAS = 				parseParameterWithEncode($_REQUEST['FIAS']);
		$isInRBP = 				parseParameter($_REQUEST['isInRBP']);
		$isAccounting = 		parseParameter($_REQUEST['isAccounting']);
		$NonAccReasonID = 		parseParameter($_REQUEST['NonAccReasonID']);
		$DataSourceID = 		parseParameter($_REQUEST['DataSourceID']);
		$Code = 				parseParameterWithEncode($_REQUEST['Code']);

		$query = "
		exec [lasmart_p_MasterDistributors_Create]
		 @UserID = $UserID
		,@Name = $Name
		,@TempAddress = $TempAddress
		,@PostCode = $PostCode
		,@CityID = $CityID
		,@Street = $Street
		,@Building = $Building
		,@ChannelID = $ChannelID
		,@SubChannelID = $SubChannelID
		,@FormatID = $FormatID
		,@ContractorGroupID = $ContractorGroupID
		,@CurrancyID = $CurrancyID
		,@isSubDistributor = $isSubDistributor
		,@DateOpen = $DateOpen
		,@DateClose = $DateClose
		,@ShopArea = $ShopArea
		,@Coordinates = $Coordinates
		,@GLN = $GLN
		,@Comment = $Comment
		,@FIAS = $FIAS
		,@isInRBP = $isInRBP
		,@isAccounting = $isAccounting
		,@NonAccReasonID = $NonAccReasonID
		,@DataSourceID = $DataSourceID
		,@Code = $Code";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_cg.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function updateMasterDistributors($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$Name = 				updateParseParameterWithEncode($_REQUEST['Name']);
		$TempAddress = 			updateParseParameterWithEncode($_REQUEST['TempAddress']);
		$PostCode = 			updateParseParameter($_REQUEST['PostCode']);
		$CityID = 				updateParseParameter($_REQUEST['CityID']);
		$Street = 				updateParseParameterWithEncode($_REQUEST['Street']);
		$Building = 			updateParseParameterWithEncode($_REQUEST['Building']);
		$ChannelID = 			updateParseParameter($_REQUEST['ChannelID']);
		$SubChannelID = 		updateParseParameter($_REQUEST['SubChannelID']);
		$FormatID = 			updateParseParameter($_REQUEST['FormatID']);
		$ContractorGroupID = 	updateParseParameter($_REQUEST['ContractorGroupID']);
		$CurrancyID = 			updateParseParameter($_REQUEST['CurrancyID']);
		$isSubDistributor = 	updateParseParameter($_REQUEST['isSubDistributor']);
		$DateOpen = 			updateParseParameterWithEncode($_REQUEST['DateOpen']);
		$DateClose = 			updateParseParameterWithEncode($_REQUEST['DateClose']);
		$ShopArea = 			updateParseParameter($_REQUEST['ShopArea']);
		$Coordinates = 			updateParseParameterWithEncode($_REQUEST['Coordinates']);
		$GLN = 					updateParseParameterWithEncode($_REQUEST['GLN']);
		$Comment = 				updateParseParameterWithEncode($_REQUEST['Comment']);
		$FIAS = 				updateParseParameterWithEncode($_REQUEST['FIAS']);
		$isInRBP = 				updateParseParameter($_REQUEST['isInRBP']);
		$isAccounting = 		updateParseParameter($_REQUEST['isAccounting']);
		$NonAccReasonID = 		updateParseParameter($_REQUEST['NonAccReasonID']);
		$Code = 				updateParseParameterWithEncode($_REQUEST['Code']);

		$query =
		"exec [lasmart_p_MasterDistributors_Update]
			@UserID = $UserID
			,@ID = $ID
			,@Name = $Name
			,@TempAddress = $TempAddress
			,@PostCode = $PostCode
			,@CityID = $CityID
			,@Street = $Street
			,@Building = $Building
			,@ChannelID = $ChannelID
			,@SubChannelID = $SubChannelID
			,@FormatID = $FormatID
			,@ContractorGroupID = $ContractorGroupID
			,@CurrancyID = $CurrancyID
			,@isSubDistributor = $isSubDistributor
			,@DateOpen = $DateOpen
			,@DateClose = $DateClose
			,@ShopArea = $ShopArea
			,@Coordinates = $Coordinates
			,@GLN = $GLN
			,@Comment = $Comment
			,@FIAS = $FIAS
			,@isInRBP = $isInRBP
			,@isAccounting = $isAccounting
			,@NonAccReasonID = $NonAccReasonID
			,@Code = $Code";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0) {
                $arr['success']	= FALSE;
                $arr['msg']	= $message;
            } else
                $arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	=  mssql_get_last_message(); //'Произошла ошибка';
		}
		return $arr;
	}

	function groupUpdateMasterDistributors($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameterWithEncode($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не заданы ID позиций';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$Name = 				updateParseParameterWithEncode($_REQUEST['Name']);
		$TempAddress = 			updateParseParameterWithEncode($_REQUEST['TempAddress']);
		$PostCode = 			updateParseParameter($_REQUEST['PostCode']);
		$CityID = 				updateParseParameter($_REQUEST['CityID']);
		$Street = 				updateParseParameterWithEncode($_REQUEST['Street']);
		$Building = 			updateParseParameterWithEncode($_REQUEST['Building']);
		$ChannelID = 			updateParseParameter($_REQUEST['ChannelID']);
		$SubChannelID = 		updateParseParameter($_REQUEST['SubChannelID']);
		$FormatID = 			updateParseParameter($_REQUEST['FormatID']);
		$ContractorGroupID = 	updateParseParameter($_REQUEST['ContractorGroupID']);
		$CurrancyID = 			updateParseParameter($_REQUEST['CurrancyID']);
		$isSubDistributor = 	updateParseParameter($_REQUEST['isSubDistributor']);
		$DateOpen = 			updateParseParameterWithEncode($_REQUEST['DateOpen']);
		$DateClose = 			updateParseParameterWithEncode($_REQUEST['DateClose']);
		$ShopArea = 			updateParseParameter($_REQUEST['ShopArea']);
		$Coordinates = 			updateParseParameterWithEncode($_REQUEST['Coordinates']);
		$GLN = 					updateParseParameterWithEncode($_REQUEST['GLN']);
		$Comment = 				updateParseParameterWithEncode($_REQUEST['Comment']);
		$FIAS = 				updateParseParameterWithEncode($_REQUEST['FIAS']);
		$isInRBP = 				updateParseParameter($_REQUEST['isInRBP']);
		$isAccounting = 		updateParseParameter($_REQUEST['isAccounting']);
		$NonAccReasonID = 		updateParseParameter($_REQUEST['NonAccReasonID']);
		$Code = 				updateParseParameterWithEncode($_REQUEST['Code']);

		$query =
			"exec [lasmart_p_MasterDistributors_Update]
			@UserID = $UserID
			,@ID = $ID
			,@Name = $Name
			,@TempAddress = $TempAddress
			,@PostCode = $PostCode
			,@CityID = $CityID
			,@Street = $Street
			,@Building = $Building
			,@ChannelID = $ChannelID
			,@SubChannelID = $SubChannelID
			,@FormatID = $FormatID
			,@ContractorGroupID = $ContractorGroupID
			,@CurrancyID = $CurrancyID
			,@isSubDistributor = $isSubDistributor
			,@DateOpen = $DateOpen
			,@DateClose = $DateClose
			,@ShopArea = $ShopArea
			,@Coordinates = $Coordinates
			,@GLN = $GLN
			,@Comment = $Comment
			,@FIAS = $FIAS
			,@isInRBP = $isInRBP
			,@isAccounting = $isAccounting
			,@NonAccReasonID = $NonAccReasonID
			,@Code = $Code";

		//ob_start();var_dump($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	=  mssql_get_last_message(); //'Произошла ошибка';
		}
		return $arr;
	}

	function getMasterWarehouses($connection, $_REQUEST) {
		$arr = array();
		$ret = array();
		$notNeedCount = false;

		$where = parseFilters($_REQUEST, 'masterWarehouses');
		$order = parseSorters($_REQUEST);

		if ($_SESSION['masterWarehouses']['fitlers'] === $where){
			$notNeedCount = true;
		}

		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));

		$query = "select top $top * FROM [lasmart_v_dim_MasterWarehouses] $where $order";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$ret['items'] = $arr;

		if (!$notNeedCount) {
			$count = 0;
			$query = "select count(*) FROM [lasmart_v_dim_MasterWarehouses] $where";
			$result = mssql_query($query,$connection);
			while ($row = mssql_fetch_row($result)) {
				$count = $row[0];
			}
			
			$ret['total'] = $count;

			$_SESSION['masterWarehouses']['fitlers'] = $where;
			$_SESSION['masterWarehouses']['count'] = $count;
		} else {
			$ret['total'] = $_SESSION['masterWarehouses']['count'];
		}

		return $ret;
	}

	function createMasterWarehouses($connection, $_REQUEST) {
		$arr = array();

		$UserID = $_SESSION['idUser'];
		$Name = 				parseParameterWithEncode($_REQUEST['Name']);
		$TempAddress = 			parseParameterWithEncode($_REQUEST['TempAddress']);
		$PostCode = 			parseParameter($_REQUEST['PostCode']);
		$CityID = 				parseParameter($_REQUEST['CityID']);
		$Street = 				parseParameterWithEncode($_REQUEST['Street']);
		$Building = 			parseParameterWithEncode($_REQUEST['Building']);
		$FormatID = 			parseParameter($_REQUEST['FormatID']);
		$ContractorGroupID = 	parseParameter($_REQUEST['ContractorGroupID']);
		$DateOpen = 			parseParameterWithEncode($_REQUEST['DateOpen']);
		$DateClose = 			parseParameterWithEncode($_REQUEST['DateClose']);
		$ShopArea = 			parseParameter($_REQUEST['ShopArea']);
		$Comment = 				parseParameterWithEncode($_REQUEST['Comment']);
		$FIAS = 				parseParameterWithEncode($_REQUEST['FIAS']);
		$DataSourceID = 		parseParameter($_REQUEST['DataSourceID']);
		$Code = 				parseParameterWithEncode($_REQUEST['Code']);
		$DistributorID = 		parseParameter($_REQUEST['DistributorID']);

		$query = "
		exec [lasmart_p_MasterWarehouses_Create]
		 @UserID = $UserID
		,@Name = $Name
		,@TempAddress = $TempAddress
		,@PostCode = $PostCode
		,@CityID = $CityID
		,@Street = $Street
		,@Building = $Building
		,@FormatID = $FormatID
		,@ContractorGroupID = $ContractorGroupID
		,@DateOpen = $DateOpen
		,@DateClose = $DateClose
		,@ShopArea = $ShopArea
		,@Comment = $Comment
		,@FIAS = $FIAS
		,@DataSourceID = $DataSourceID
		,@DistributorID = $DistributorID
		,@Code = $Code";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_cg.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function updateMasterWarehouses($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$Name = 				updateParseParameterWithEncode($_REQUEST['Name']);
		$TempAddress = 			updateParseParameterWithEncode($_REQUEST['TempAddress']);
		$PostCode = 			updateParseParameter($_REQUEST['PostCode']);
		$CityID = 				updateParseParameter($_REQUEST['CityID']);
		$Street = 				updateParseParameterWithEncode($_REQUEST['Street']);
		$Building = 			updateParseParameterWithEncode($_REQUEST['Building']);
		$FormatID = 			updateParseParameter($_REQUEST['FormatID']);
		$ContractorGroupID = 	updateParseParameter($_REQUEST['ContractorGroupID']);
		$DateOpen = 			updateParseParameterWithEncode($_REQUEST['DateOpen']);
		$DateClose = 			updateParseParameterWithEncode($_REQUEST['DateClose']);
		$ShopArea = 			updateParseParameter($_REQUEST['ShopArea']);
		$Comment = 				updateParseParameterWithEncode($_REQUEST['Comment']);
		$FIAS = 				updateParseParameterWithEncode($_REQUEST['FIAS']);
		$Code = 				updateParseParameterWithEncode($_REQUEST['Code']);
		$DistributorID = 		updateParseParameter($_REQUEST['DistributorID']);

		$query =
		"exec [lasmart_p_MasterWarehouses_Update]
			@UserID = $UserID
			,@ID = $ID
			,@Name = $Name
			,@TempAddress = $TempAddress
			,@PostCode = $PostCode
			,@CityID = $CityID
			,@Street = $Street
			,@Building = $Building
			,@FormatID = $FormatID
			,@ContractorGroupID = $ContractorGroupID
			,@DateOpen = $DateOpen
			,@DateClose = $DateClose
			,@ShopArea = $ShopArea
			,@Comment = $Comment
			,@FIAS = $FIAS
			,@DistributorID = $DistributorID
			,@Code = $Code";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0) {
                $arr['success']	= FALSE;
                $arr['msg']	= $message;
            } else
                $arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	=  mssql_get_last_message(); //'Произошла ошибка';
		}
		return $arr;
	}

	function groupUpdateMasterWarehouses($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameterWithEncode($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не заданы ID позиций';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$Name = 				updateParseParameterWithEncode($_REQUEST['Name']);
		$TempAddress = 			updateParseParameterWithEncode($_REQUEST['TempAddress']);
		$PostCode = 			updateParseParameter($_REQUEST['PostCode']);
		$CityID = 				updateParseParameter($_REQUEST['CityID']);
		$Street = 				updateParseParameterWithEncode($_REQUEST['Street']);
		$Building = 			updateParseParameterWithEncode($_REQUEST['Building']);
		$FormatID = 			updateParseParameter($_REQUEST['FormatID']);
		$ContractorGroupID = 	updateParseParameter($_REQUEST['ContractorGroupID']);
		$DateOpen = 			updateParseParameterWithEncode($_REQUEST['DateOpen']);
		$DateClose = 			updateParseParameterWithEncode($_REQUEST['DateClose']);
		$ShopArea = 			updateParseParameter($_REQUEST['ShopArea']);
		$Comment = 				updateParseParameterWithEncode($_REQUEST['Comment']);
		$FIAS = 				updateParseParameterWithEncode($_REQUEST['FIAS']);
		$DistributorID = 		updateParseParameter($_REQUEST['DistributorID']);

		$query =
			"exec [lasmart_p_MasterWarehouses_Update]
			@UserID = $UserID
			,@ID = $ID
			,@Name = $Name
			,@TempAddress = $TempAddress
			,@PostCode = $PostCode
			,@CityID = $CityID
			,@Street = $Street
			,@Building = $Building
			,@FormatID = $FormatID
			,@ContractorGroupID = $ContractorGroupID
			,@DateOpen = $DateOpen
			,@DateClose = $DateClose
			,@ShopArea = $ShopArea
			,@Comment = $Comment
			,@FIAS = $FIAS			
			,@DistributorID = $DistributorID
			,@Code = null";

		//ob_start();var_dump($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	=  mssql_get_last_message(); //'Произошла ошибка';
		}
		return $arr;
	}

	function getParentGoods($connection, $_REQUEST) {
		$arr = array();

		$query = "select * FROM [lasmart_v_dim_ParentGood]";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gpg.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}

		return $arr;
	}

	function getParentStaff($connection, $_REQUEST) {
		$arr = array();

		$query = "select * FROM [lasmart_v_dim_ParentStaff]";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}

		return $arr;
	}

	function getDistributors($connection, $_REQUEST) {
		$arr = array();

		$query = "select * FROM [lasmart_v_dim_Distributors] order by [Name]";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}

		return $arr;
	}

	function getOrimiGoods($connection, $_REQUEST) {
		$arr = array();

		$query = "select * FROM [lasmart_v_dim_OrimiGoods] order by [Name]";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}

		return $arr;
	}

	function getSlaveGoods($connection, $_REQUEST) {
		$arr = array();
		$ret = array();

		$where = parseFilters($_REQUEST, 'slaveGood');
		$order = parseSorters($_REQUEST);
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));

		$query = "select top $top * FROM [lasmart_v_dim_SlaveGood] $where $order";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$count = 0;
		$query = "select count(*) FROM [lasmart_v_dim_SlaveGood] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		$ret['items'] = $arr;
		$ret['total'] = $count;
		return $ret;
	}

	function getSlaveStores($connection, $_REQUEST) {
		$arr = array();
		$ret = array();

		$where = parseFilters($_REQUEST, 'slaveStores');
		$order = parseSorters($_REQUEST);
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));

		$query = "select top $top * FROM [lasmart_v_dim_SlaveStores] $where $order";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$count = 0;
		$query = "select count(*) FROM [lasmart_v_dim_SlaveStores] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		$ret['items'] = $arr;
		$ret['total'] = $count;
		return $ret;
	}

	function getSlaveStaff($connection, $_REQUEST) {
		$arr = array();
		$ret = array();

		$where = parseFilters($_REQUEST, 'slaveStaff');
		$order = parseSorters($_REQUEST);
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));

		$query = "select top $top * FROM [lasmart_v_dim_SlaveStaff] $where $order";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$count = 0;
		$query = "select count(*) FROM [lasmart_v_dim_SlaveStaff] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		$ret['items'] = $arr;
		$ret['total'] = $count;
		return $ret;
	}

	function getSlaveWarehouses($connection, $_REQUEST) {
		$arr = array();
		$ret = array();

		$where = parseFilters($_REQUEST, 'slaveWarehouses');
		$order = parseSorters($_REQUEST);
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));

		$query = "select top $top * FROM [lasmart_v_dim_SlaveWarehouses] $where $order";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$count = 0;
		$query = "select count(*) FROM [lasmart_v_dim_SlaveWarehouses] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		$ret['items'] = $arr;
		$ret['total'] = $count;
		return $ret;
	}

	function syncSlaveGoods($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции подчиненного справочника';
			return $arr;
		}

		if (isset($_REQUEST['MasterID'])) {
			$MasterID = parseParameter($_REQUEST['MasterID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции мастер-справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];

		$query =
		"exec [lasmart_p_SlaveGood_Sync]
		@UserID = $UserID
		,@ID = $ID
		,@MasterID = $MasterID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function syncSlaveStores($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции подчиненного справочника';
			return $arr;
		}

		if (isset($_REQUEST['MasterID'])) {
			$MasterID = parseParameter($_REQUEST['MasterID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции мастер-справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		
		$query =
		"exec [lasmart_p_SlaveStores_Sync]
		@UserID = $UserID
		,@ID = $ID
		,@MasterID = $MasterID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function syncSlaveStaff($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции подчиненного справочника';
			return $arr;
		}

		if (isset($_REQUEST['MasterID'])) {
			$MasterID = parseParameter($_REQUEST['MasterID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции мастер-справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		//
		$query =
		"exec [lasmart_p_SlaveStaff_Sync]
		@UserID = $UserID
		,@ID = $ID
		,@MasterID = $MasterID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function syncSlaveWarehouses($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции подчиненного справочника';
			return $arr;
		}

		if (isset($_REQUEST['MasterID'])) {
			$MasterID = parseParameter($_REQUEST['MasterID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции мастер-справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		
		$query =
		"exec [lasmart_p_SlaveWarehouses_Sync]
		@UserID = $UserID
		,@ID = $ID
		,@MasterID = $MasterID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function groupSyncSlaveGoods($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не заданы ID позиций подчиненного справочника';
			return $arr;
		}

		if (isset($_REQUEST['MasterID'])) {
			$MasterID = parseParameter($_REQUEST['MasterID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции мастер-справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];

		$query =
		"exec [lasmart_p_SlaveGood_GroupSync]
		@UserID = $UserID
		,@ID = '$ID'
		,@MasterID = $MasterID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function groupSyncWindowSlaveGoods($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['data'])) {
			$data = json_decode(parseParameter($_REQUEST['data']),true);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не заданы ID позиций для привязки';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$query = "SET XACT_ABORT ON
			BEGIN TRANSACTION";

		for ($i=0;$i<count($data);$i++){
			$query .= "
			exec [lasmart_p_SlaveGood_Sync] @UserID = ".$UserID." ,@ID = ".$data[$i]['slaveID']." ,@MasterID = ".$data[$i]['masterID']."";
		}

		$query .= " COMMIT TRANSACTION";	

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gsw.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function groupSyncSlaveStores($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не заданы ID позиций подчиненного справочника';
			return $arr;
		}

		if (isset($_REQUEST['MasterID'])) {
			$MasterID = parseParameter($_REQUEST['MasterID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции мастер-справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		
		$query =
		"exec [lasmart_p_SlaveStores_GroupSync]
		@UserID = $UserID
		,@ID = '$ID'
		,@MasterID = $MasterID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function groupSyncWindowSlaveStores($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['data'])) {
			$data = json_decode(parseParameter($_REQUEST['data']),true);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не заданы ID позиций для привязки';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$query = "SET XACT_ABORT ON
			BEGIN TRANSACTION";

		for ($i=0;$i<count($data);$i++){
			$query .= "
			exec [lasmart_p_SlaveStores_Sync] @UserID = ".$UserID." ,@ID = ".$data[$i]['slaveID']." ,@MasterID = ".$data[$i]['masterID'];
		}

		$query .= " COMMIT TRANSACTION";		

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gsw.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function groupSyncSlaveStaff($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не заданы ID позиций подчиненного справочника';
			return $arr;
		}

		if (isset($_REQUEST['MasterID'])) {
			$MasterID = parseParameter($_REQUEST['MasterID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции мастер-справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		//
		$query =
		"exec [lasmart_p_SlaveStaff_GroupSync]
		@UserID = $UserID
		,@ID = '$ID'
		,@MasterID = $MasterID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function groupSyncWindowSlaveStaff($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['data'])) {
			$data = json_decode(parseParameter($_REQUEST['data']),true);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не заданы ID позиций для привязки';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$query = "SET XACT_ABORT ON
			BEGIN TRANSACTION";

		for ($i=0;$i<count($data);$i++){
			$query .= "
			exec [lasmart_p_SlaveStaff_Sync] @UserID = ".$UserID." ,@ID = ".$data[$i]['slaveID']." ,@MasterID = ".$data[$i]['masterID']."";
		}

		$query .= " COMMIT TRANSACTION";		

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gsw.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function groupSyncSlaveWarehouses($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не заданы ID позиций подчиненного справочника';
			return $arr;
		}

		if (isset($_REQUEST['MasterID'])) {
			$MasterID = parseParameter($_REQUEST['MasterID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции мастер-справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		
		$query =
		"exec [lasmart_p_SlaveWarehouses_GroupSync]
		@UserID = $UserID
		,@ID = '$ID'
		,@MasterID = $MasterID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function groupSyncWindowSlaveWarehouses($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['data'])) {
			$data = json_decode(parseParameter($_REQUEST['data']),true);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не заданы ID позиций для привязки';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$query = "SET XACT_ABORT ON
			BEGIN TRANSACTION";

		for ($i=0;$i<count($data);$i++){
			$query .= "
			exec [lasmart_p_SlaveWarehouses_Sync] @UserID = ".$UserID." ,@ID = ".$data[$i]['slaveID']." ,@MasterID = ".$data[$i]['masterID'];
		}

		$query .= " COMMIT TRANSACTION";		

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gsw.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getSyncedGoods($connection, $_REQUEST) {
		$arr = array();
		$ret = array();

		$where = parseFilters($_REQUEST, 'syncedGood');
		$order = parseSorters($_REQUEST);
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));

		if (isset($_REQUEST['GoodID'])) {
			$GoodIDs = parseParameter($_REQUEST['GoodID']);
			$where = $where.' and [MasterID] in ('.$GoodIDs.')';
		}

		$query = "select top $top * FROM [lasmart_v_dim_SyncedGood] $where $order";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$count = 0;
		$query = "select count(*) FROM [lasmart_v_dim_SyncedGood] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		$ret['items'] = $arr;
		$ret['total'] = $count;
		return $ret;
	}

	function getSyncedStores($connection, $_REQUEST) {
		$arr = array();
		$ret = array();

		$where = parseFilters($_REQUEST, 'syncedStores');
		$order = parseSorters($_REQUEST);
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));

		if (isset($_REQUEST['StoresID'])) {
			$rawStoresIDs = json_decode(parseParameter($_REQUEST['StoresID']),true);
			//ob_start();print_r($_REQUEST);var_dump($rawStoresIDs);file_put_contents('out_save.txt',ob_get_clean());
			$stores = array();
			$nets = array();
			$distrs = array();
			for ($i=0;$i<count($rawStoresIDs);$i++) {
				if ($rawStoresIDs[$i]['ref'] == 0)
					$stores [] = $rawStoresIDs[$i]['value'];
				else
					if ($rawStoresIDs[$i]['ref'] == 1)
						$nets [] = $rawStoresIDs[$i]['value'];
					else
						if ($rawStoresIDs[$i]['ref'] == 2)
							$distrs [] = $rawStoresIDs[$i]['value'];
			}
			$stores = count($stores) ? implode(",", $stores) : '0';
			$nets = count($nets) ? implode(",", $nets) : '0';
			$distrs = count($distrs) ? implode(",", $distrs) : '0';
			$where = $where." and (([MasterID] in (".$stores.") and [MasterType] = 0) or ([MasterID] in (".$nets.") and [MasterType] = 1) or ([MasterID] in (".$distrs.") and [MasterType] = 2))";
		}

		$query = "select top $top * FROM [lasmart_v_dim_SyncedStores] $where $order";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$count = 0;
		$query = "select count(*) FROM [lasmart_v_dim_SyncedStores] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		$ret['items'] = $arr;
		$ret['total'] = $count;
		return $ret;
	}

	function getSyncedStaff($connection, $_REQUEST) {
		$arr = array();
		$ret = array();

		$where = parseFilters($_REQUEST, 'syncedStaff');
		$order = parseSorters($_REQUEST);
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));

		if (isset($_REQUEST['StaffID'])) {
			$StaffIDs = parseParameter($_REQUEST['StaffID']);
			$where = $where.' and [MasterID] in ('.$StaffIDs.')';
		}

		$query = "select top $top * FROM [lasmart_v_dim_SyncedStaff] $where $order"; //проверить
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$count = 0;
		$query = "select count(*) FROM [lasmart_v_dim_SyncedStaff] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		$ret['items'] = $arr;
		$ret['total'] = $count;
		return $ret;
	}

	function getSyncedWarehouses($connection, $_REQUEST) {
		$arr = array();
		$ret = array();

		$where = parseFilters($_REQUEST, 'syncedWarehouses');
		$order = parseSorters($_REQUEST);
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));

		if (isset($_REQUEST['WarehousesID'])) {
			$WarehousesIDs = parseParameter($_REQUEST['WarehousesID']);
			$where = $where.' and [MasterID] in ('.$WarehousesIDs.')';
		}

		$query = "select top $top * FROM [lasmart_v_dim_SyncedWarehouses] $where $order";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$count = 0;
		$query = "select count(*) FROM [lasmart_v_dim_SyncedWarehouses] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		$ret['items'] = $arr;
		$ret['total'] = $count;
		return $ret;
	}

	function unsyncSyncedGoods($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции подчиненного справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];

		$query =
		"exec [lasmart_p_SlaveGood_Unsync]
		@UserID = $UserID
		,@ID = $ID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function unsyncSyncedStores($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции подчиненного справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];

		$query =
		"exec [lasmart_p_SlaveStores_Unsync]
		@UserID = $UserID
		,@ID = $ID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function unsyncSyncedStaff($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции подчиненного справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];

		$query =
		"exec [lasmart_p_SlaveStaff_Unsync]
		@UserID = $UserID
		,@ID = $ID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function unsyncSyncedWarehouses($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции подчиненного справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];

		$query =
		"exec [lasmart_p_SlaveWarehouses_Unsync]
		@UserID = $UserID
		,@ID = $ID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function groupunsyncSyncedGoods($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции подчиненного справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];

		$query =
		"exec [lasmart_p_SlaveGood_GroupUnsync]
		@UserID = $UserID
		,@ID = '$ID'";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function groupunsyncSyncedStores($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции подчиненного справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];

		$query =
		"exec [lasmart_p_SlaveStores_GroupUnsync]
		@UserID = $UserID
		,@ID = '$ID'";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function groupunsyncSyncedStaff($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции подчиненного справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];

		$query =
		"exec [lasmart_p_SlaveStaff_GroupUnsync]
		@UserID = $UserID
		,@ID = '$ID'";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function groupunsyncSyncedWarehouses($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID позиции подчиненного справочника';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];

		$query =
		"exec [lasmart_p_SlaveWarehouses_GroupUnsync]
		@UserID = $UserID
		,@ID = '$ID'";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getWeightGroup($connection, $_REQUEST) { //----------------------------
		$arr = array();
		$query = "select [ID]
			,[Name]
			,[ShortName]
			,[BriefNotation]
			,[Deleted]
			,[Begin]
			,[End]
		FROM [lasmart_t_dim_WeightGroup] where Deleted = 0

		union all

		select [ID]
			,[Name]
			,[ShortName]
			,[BriefNotation]
			,[Deleted]
			,[Begin]
			,[End]
		FROM [lasmart_t_dim_WeightGroup] where Deleted = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createWeightGroup($connection, $_REQUEST){
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Begin = parseParameter($_REQUEST['Begin']);
		$End = parseParameter($_REQUEST['End']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$arr = array();
		$query =
			"insert into [lasmart_t_dim_WeightGroup]
			([Name]
			,[ShortName]
			,[BriefNotation]
			,[Deleted]
			,[Begin]
			,[End])
			VALUES($Name,$ShortName,$BriefNotation,$Deleted,$Begin,$End)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}

		return $arr;
	}

	function updateWeightGroup($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Begin = parseParameter($_REQUEST['Begin']);
		$End = parseParameter($_REQUEST['End']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;

		$Sku_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Номенклатура" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,[ID]) + ','
			FROM [lasmart_t_dim_MasterGood] with (nolock)
			where WeightGroupID = $ID and StatusID = 2
			order by [ID]
				
			if (LEN(@error) > 0) begin
				print $Sku_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				update lasmart_t_dim_MasterGood
				set WeightGroupID = null
				where WeightGroupID = $ID
			end
		end

		update [lasmart_t_dim_WeightGroup]
		set [Name] = $Name
		,[ShortName] = $ShortName
		,[BriefNotation] = $BriefNotation
		,[Deleted] = $Deleted
		,[Begin] = $Begin
		,[End] = $End
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='WeightGroupID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getSelectedAdding($connection, $_REQUEST) {
		$arr = array();
		$GoodID = parseParameter($_REQUEST['GoodID']);
		$query = "
		select a.[ID]
			,a.[Name]
		FROM [lasmart_t_dim_Adding] a
		join [lasmart_t_good_adding] ga on a.[ID] = ga.[AddingID]
		where [GoodID] = $GoodID";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$arr [] = $row;
		}
		return $arr;
	}

	function getSelectedFlavor($connection, $_REQUEST) {
		$arr = array();
		$GoodID = parseParameter($_REQUEST['GoodID']);
		$query = "
		select a.[ID]
			,a.[Name]
		FROM [lasmart_t_dim_Flavor] a
		join [lasmart_t_good_flavor] ga on a.[ID] = ga.[FlavorID]
		where [GoodID] = $GoodID";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$arr [] = $row;
		}
		return $arr;
	}

	function getSelectedSector($connection, $_REQUEST) {
		$arr = array();
		$StaffID = parseParameter($_REQUEST['StaffID']);
		$query = "
		select s.[ID]
			,s.[Name]
		FROM [lasmart_t_dim_Sector] s
		join [lasmart_t_staff_sector] ss on s.[ID] = ss.[SectorID]
		where [StaffID] = $StaffID";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$arr [] = $row;
		}
		return $arr;
	}

	function getSelectedArea($connection, $_REQUEST) {
		$arr = array();
		$StaffID = parseParameter($_REQUEST['StaffID']);
		$query = "
		select s.[ID]
			,s.[Name]
		FROM [lasmart_t_dim_Area] s
		join [lasmart_t_staff_area] ss on s.[ID] = ss.[AreaID]
		where [StaffID] = $StaffID";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$arr [] = $row;
		}
		return $arr;
	}

	function getSimpleAttribute($connection, $table, $_REQUEST) {
		$arr = array();

		$query = "select [ID]
			,[Name]
			,[ShortName]
			,[BriefNotation]
			,[Deleted]
		FROM [lasmart_t_dim_".$table."] where Deleted = 0

		union all

		select [ID]
			,[Name]
			,[ShortName]
			,[BriefNotation]
			,[Deleted]
		FROM [lasmart_t_dim_".$table."] where Deleted = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createSimpleAttribute($connection, $table, $_REQUEST){
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$arr = array();
		$query =
			"insert into [lasmart_t_dim_".$table."]
			([Name]
			,[ShortName]
			,[BriefNotation]
			,[Deleted])
			VALUES($Name,$ShortName,$BriefNotation,$Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_csa.txt',ob_get_clean());

		return $arr;
	}

	function updateSimpleAttribute($connection, $table, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;

		if ($table == 'Currancy') {
			$query =
			"if ($Deleted = 1) begin
				declare @err int
				exec [dbo].[lasmart_p_CheckRelatedMasterOnAttributeChange] @Attribute='".$table."ID', @Value=$ID, @err = @err OUTPUT
				if (@err = 1)
					return
			end
			
			update [lasmart_t_dim_".$table."]
			set [Name] = $Name
			,[BriefNotation] = $BriefNotation
			,[Deleted] = $Deleted
			where ID = $ID

			exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='".$table."ID', @Value=$ID";
		} else {
			$query =
			"if ($Deleted = 1) begin
				declare @err int
				exec [dbo].[lasmart_p_CheckRelatedMasterOnAttributeChange] @Attribute='".$table."ID', @Value=$ID, @err = @err OUTPUT
				if (@err = 1)
					return
			end
			
			update [lasmart_t_dim_".$table."]
			set [Name] = $Name
			,[ShortName] = $ShortName
			,[BriefNotation] = $BriefNotation
			,[Deleted] = $Deleted
			where ID = $ID

			exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='".$table."ID', @Value=$ID";
		}

		

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_usa.txt',ob_get_clean());

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getSector($connection, $_REQUEST) {
		$arr = array();
		$query = "select sec.[ID]
			,sec.[Name]
			,sec.[ShortName]			
			,sec.[BriefNotation]
			,ar.[ID] as [AreaID]
			,ar.[Name] as [Area]
			,div.[ID] as [DivisionID]
			,div.[Name] as [Division]
			,sec.[Deleted]
		FROM [lasmart_t_dim_Sector] sec with (nolock)
		left join [lasmart_t_dim_Area] ar with (nolock)
			on sec.AreaID = ar.ID
		left join [lasmart_t_dim_Division] div with (nolock)
			on ar.[DivisionID] = div.[ID]
		where sec.Deleted = 0

		union all

		select sec.[ID]
			,sec.[Name]
			,sec.[ShortName]
			,sec.[BriefNotation]
			,ar.[ID] as [AreaID]
			,ar.[Name] as [Area]
			,div.[ID] as [DivisionID]
			,div.[Name] as [Division]
			,sec.[Deleted]
		FROM [lasmart_t_dim_Sector] sec with (nolock)
		left join [lasmart_t_dim_Area] ar with (nolock)
			on sec.AreaID = ar.ID
		left join [lasmart_t_dim_Division] div with (nolock)
			on ar.[DivisionID] = div.[ID]
		where sec.Deleted = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$row['Area'] = iconv("cp1251", "UTF-8", $row['Area']);
			$row['Division'] = iconv("cp1251", "UTF-8", $row['Division']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createSector($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$AreaID = parseParameter($_REQUEST['AreaID']);

		$query =
		"insert into [lasmart_t_dim_Sector]
		([Name]
		,[ShortName]
		,[AreaID]
		,[BriefNotation]
		,[Deleted])
		VALUES($Name, $ShortName, $AreaID, $BriefNotation, $Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}

		return $arr;
	}

	function updateSector($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$AreaID = parseParameter($_REQUEST['AreaID']);

		$other = parseParameterWithEncode('ПРОЧЕЕ');

		$TT_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Торговые точки" с этим атрибутом: ID ');
		$Distr_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Дистрибьюторы" с этим атрибутом: ID ');
		$Staff_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Сотрудники" с этим атрибутом: ID ');
		$Wrhs_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Склады" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,ms.[ID]) + ','
			FROM [lasmart_t_dim_MasterStores] ms with (nolock)
			left join [lasmart_t_dim_City] c on ms.CityID = c.ID
			where SectorID = $ID and StatusID = 2
			order by ms.[ID]
				
			if (LEN(@error) > 0) begin
				print $TT_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				SELECT @error = @error + convert(varchar,md.[ID]) + ','
				FROM [lasmart_t_dim_MasterDistributors] md with (nolock)
				left join [lasmart_t_dim_City] c on md.CityID = c.ID
				where SectorID = $ID and StatusID = 2
				order by md.[ID]
					
				if (LEN(@error) > 0) begin
					print $Distr_error_msg+' '+left(@error,LEN(@error)-1)
					return
				end else begin
					SELECT @error = @error + convert(varchar,ms.[ID]) + ','
					FROM [lasmart_t_dim_MasterStaff] ms with (nolock)
					left join [lasmart_t_staff_sector] ss on ms.[ID] = ss.[StaffID]
					where ss.SectorID = $ID and StatusID = 2
					order by ms.[ID]
						
					if (LEN(@error) > 0) begin
						print $Staff_error_msg+' '+left(@error,LEN(@error)-1)
						return
					end else begin
						
						SELECT @error = @error + convert(varchar,md.[ID]) + ','
						FROM [lasmart_t_dim_MasterWarehouses] md with (nolock)
						left join [lasmart_t_dim_City] c on md.CityID = c.ID
						where SectorID = $ID and StatusID = 2
						order by md.[ID]
							
						if (LEN(@error) > 0) begin
							print $Wrhs_error_msg+' '+left(@error,LEN(@error)-1)
							return
						end else begin
							update [lasmart_t_dim_City]
							set SectorID = (select top 1 ID from [lasmart_t_dim_Sector] where Name = $other)
							where SectorID = $ID
						end

					end
				end
			end
		end

		update [lasmart_t_dim_Sector]
		set [Name] = $Name
		,[ShortName] = $ShortName
		,[AreaID] = $AreaID
		,[BriefNotation] = $BriefNotation
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='SectorID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getArea($connection, $_REQUEST) {
		$arr = array();
		$query = "select ar.[ID]
			,ar.[Name]
			,ar.[ShortName]			
			,ar.[BriefNotation]
			,div.[ID] as [DivisionID]
			,div.[Name] as [Division]
			,ar.[Deleted]
		FROM [lasmart_t_dim_Area] ar with (nolock)
		left join [lasmart_t_dim_Division] div with (nolock)
			on ar.[DivisionID] = div.[ID]
		where ar.Deleted = 0

		union all

		select ar.[ID]
			,ar.[Name]
			,ar.[ShortName]
			,ar.[BriefNotation]
			,div.[ID] as [DivisionID]
			,div.[Name] as [Division]
			,ar.[Deleted]
		FROM [lasmart_t_dim_Area] ar with (nolock)
		left join [lasmart_t_dim_Division] div with (nolock)
			on ar.[DivisionID] = div.[ID]
		where ar.Deleted = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$row['Division'] = iconv("cp1251", "UTF-8", $row['Division']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createArea($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$DivisionID = parseParameter($_REQUEST['DivisionID']);

		$query =
		"insert into [lasmart_t_dim_Area]
		([Name]
		,[ShortName]
		,[DivisionID]
		,[BriefNotation]
		,[Deleted])
		VALUES($Name, $ShortName, $DivisionID, $BriefNotation, $Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}

		return $arr;
	}

	function updateArea($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$DivisionID = parseParameter($_REQUEST['DivisionID']);

		$other = parseParameterWithEncode('ПРОЧЕЕ');

		$TT_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Торговые точки" с этим атрибутом: ID ');
		$Distr_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Дистрибьюторы" с этим атрибутом: ID ');
		$Staff_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Сотрудники" с этим атрибутом: ID ');
		$Wrhs_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Склады" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,ms.[ID]) + ','
			FROM [lasmart_t_dim_MasterStores] ms with (nolock)
			left join [lasmart_t_dim_City] c on ms.CityID = c.ID
			left join [lasmart_t_dim_Sector] sec on c.SectorID = sec.ID
			where sec.AreaID = $ID and StatusID = 2
			order by ms.[ID]
				
			if (LEN(@error) > 0) begin
				print $TT_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				SELECT @error = @error + convert(varchar,md.[ID]) + ','
				FROM [lasmart_t_dim_MasterDistributors] md with (nolock)
				left join [lasmart_t_dim_City] c on md.CityID = c.ID
				left join [lasmart_t_dim_Sector] sec on c.SectorID = sec.ID
				where sec.AreaID = $ID and StatusID = 2
				order by md.[ID]
					
				if (LEN(@error) > 0) begin
					print $Distr_error_msg+' '+left(@error,LEN(@error)-1)
					return
				end else begin
					SELECT @error = @error + convert(varchar,ms.[ID]) + ','
					FROM [lasmart_t_dim_MasterStaff] ms with (nolock)
					left join [lasmart_t_staff_sector] ss on ms.[ID] = ss.[StaffID]
					left join [lasmart_t_dim_Sector] sec on ss.SectorID = sec.ID
					where sec.AreaID = $ID and StatusID = 2
					order by ms.[ID]
						
					if (LEN(@error) > 0) begin
						print $Staff_error_msg+' '+left(@error,LEN(@error)-1)
						return
					end else begin
						
						SELECT @error = @error + convert(varchar,md.[ID]) + ','
						FROM [lasmart_t_dim_MasterWarehouses] md with (nolock)
						left join [lasmart_t_dim_City] c on md.CityID = c.ID
						left join [lasmart_t_dim_Sector] sec on c.SectorID = sec.ID
						where sec.AreaID = $ID and StatusID = 2
						order by md.[ID]
							
						if (LEN(@error) > 0) begin
							print $Wrhs_error_msg+' '+left(@error,LEN(@error)-1)
							return
						end else begin
							update [lasmart_t_dim_Sector]
							set AreaID = (select top 1 ID from [lasmart_t_dim_Area] where Name = $other)
							where AreaID = $ID
						end
					end
				end
			end
		end

		update [lasmart_t_dim_Area]
		set [Name] = $Name
		,[ShortName] = $ShortName
		,[DivisionID] = $DivisionID
		,[BriefNotation] = $BriefNotation
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='AreaID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getDivision($connection, $_REQUEST) {
		$arr = array();
		$query = "select d.[ID]
			,d.[Name]
			,d.[ShortName]
			,d.[BriefNotation]
			,dep.[ID] as [TradentDepartmentID]
			,dep.[Name] as [TradentDepartment]
			,d.[Deleted]
		FROM [lasmart_t_dim_Division] d
		left join [lasmart_t_dim_TradentDepartment] dep on d.[TradentDepNo] = dep.[ID]
		where d.Deleted = 0

		union all

		select d.[ID]
			,d.[Name]
			,d.[ShortName]
			,d.[BriefNotation]
			,dep.[ID] as [TradentDepartmentID]
			,dep.[Name] as [TradentDepartment]
			,d.[Deleted]
		FROM [lasmart_t_dim_Division] d
		left join [lasmart_t_dim_TradentDepartment] dep on d.[TradentDepNo] = dep.[ID]
		where d.Deleted = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$row['TradentDepartment'] = iconv("cp1251", "UTF-8", $row['TradentDepartment']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createDivision($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$TradentDepartmentID = parseParameter($_REQUEST['TradentDepartmentID']);

		$query =
		"insert into [lasmart_t_dim_Division]
		([Name]
		,[ShortName]
		,[BriefNotation]
		,[TradentDepNo]
		,[Deleted])
		VALUES($Name, $ShortName, $BriefNotation, $TradentDepartmentID, $Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}

		return $arr;
	}

	function updateDivision($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$TradentDepartmentID = parseParameter($_REQUEST['TradentDepartmentID']);

		$Area_error_msg = parseParameterWithEncode('Сущствуют позиции справочника атрибутов "Участок" с этим атрибутом: ID ');
		$Staff_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Сотрудники" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,[ID]) + ','
			FROM [lasmart_t_dim_Area] with (nolock)
			where DivisionID = $ID and Deleted = 0
			order by [ID]
				
			if (LEN(@error) > 0) begin
				print $Area_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				SELECT @error = @error + convert(varchar,[ID]) + ','
				FROM [lasmart_v_dim_MasterStaff] with (nolock)
				where DivisionID = $ID and StatusID = 2
				order by [ID]

				if (LEN(@error) > 0) begin
					print $Staff_error_msg+' '+left(@error,LEN(@error)-1)
					return
				end else begin

					update lasmart_t_dim_MasterStaff
					set DivisionID = null
					where DivisionID = $ID

				end
			end
		end

		update [lasmart_t_dim_Division]
		set [Name] = $Name
		,[ShortName] = $ShortName
		,[BriefNotation] = $BriefNotation
		,[TradentDepNo] = $TradentDepartmentID
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='DivisionID', @Value=$ID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_un.txt',ob_get_clean());
		if (mssql_query( $query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getSubChannel($connection, $_REQUEST) {
		$arr = array();
		$query = "select sbch.[ID]
			,sbch.[Name]
			,sbch.[ShortName]
			,sbch.[ChannelID]
			,sbch.[BriefNotation]
			,ch.Name as [Channel]
			,sbch.[Deleted]
		FROM [lasmart_t_dim_SubChannel] sbch
		left join [lasmart_t_dim_Channel] ch on sbch.[ChannelID] = ch.[ID]
		where sbch.Deleted = 0

		union all

		select sbch.[ID]
			,sbch.[Name]
			,sbch.[ShortName]
			,sbch.[ChannelID]
			,sbch.[BriefNotation]
			,ch.Name as [Channel]
			,sbch.[Deleted]
		FROM [lasmart_t_dim_SubChannel] sbch 
		left join [lasmart_t_dim_Channel] ch on sbch.[ChannelID] = ch.[ID]
		where sbch.Deleted = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$row['Channel'] = iconv("cp1251", "UTF-8", $row['Channel']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createSubChannel($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$ChannelID = parseParameter($_REQUEST['ChannelID']);

		$query =
		"insert into [lasmart_t_dim_SubChannel]
		([Name]
		,[ShortName]
		,[ChannelID]
		,[BriefNotation]
		,[Deleted])
		VALUES($Name, $ShortName, $ChannelID, $BriefNotation, $Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}

		return $arr;
	}

	function updateSubChannel($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$ChannelID = parseParameter($_REQUEST['ChannelID']);

		$TT_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Торговые точки" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,[ID]) + ','
			FROM [lasmart_t_dim_MasterStores] with (nolock)
			where SubChannelID = $ID and StatusID = 2
			order by [ID]
				
			if (LEN(@error) > 0) begin
				print $TT_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				update lasmart_t_dim_MasterStores
				set SubChannelID = null
				where SubChannelID = $ID
			end
		end

		update [lasmart_t_dim_SubChannel]
		set [Name] = $Name
		,[ShortName] = $ShortName
		,[ChannelID] = $ChannelID
		,[BriefNotation] = $BriefNotation
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='SubChannelID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getNet($connection, $_REQUEST) {
		$arr = array();
		$query = "select * from [lasmart_v_dim_Net_with_StoresCount]";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['NetType'] = iconv("cp1251", "UTF-8", $row['NetType']);
			$row['RespManager'] = iconv("cp1251", "UTF-8", $row['RespManager']);
			$row['ParentNet'] = iconv("cp1251", "UTF-8", $row['ParentNet']);
			$row['AdvertisingChannel'] = iconv("cp1251", "UTF-8", $row['AdvertisingChannel']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createNet($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$isNielsen = parseParameter($_REQUEST['isNielsen']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$NetTypeID = parseParameter($_REQUEST['NetTypeID']);
		$RespManagerID = parseParameter($_REQUEST['RespManagerID']);
		$ParentNetID = parseParameter($_REQUEST['ParentNetID']);
		$hasSalesData = parseParameter($_REQUEST['hasSalesData']);

		$query =
		"insert into [lasmart_t_dim_Net]
		([Name]
		,[ShortName]
		,[NetTypeID]
		,[isNielsen]
		,[RespManagerID]
		,[ParentNetID]
		,[hasSalesData]
		,[Deleted])
		VALUES($Name, $ShortName, $NetTypeID, $isNielsen, $RespManagerID, $ParentNetID, $hasSalesData, $Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}

		return $arr;
	}

	function updateNet($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$isNielsen = parseParameter($_REQUEST['isNielsen']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$NetTypeID = parseParameter($_REQUEST['NetTypeID']);
		$RespManagerID = parseParameter($_REQUEST['RespManagerID']);
		$ParentNetID = parseParameter($_REQUEST['ParentNetID']);
		$hasSalesData = parseParameter($_REQUEST['hasSalesData']);

		$TT_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Торговые точки" с этим атрибутом: ID ');
		$Net_error_msg = parseParameterWithEncode('У этой сети есть подчиненные элементы: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,[ID]) + ','
			FROM [lasmart_t_dim_MasterStores] with (nolock)
			where NetID = $ID and StatusID = 2
			order by [ID]
				
			if (LEN(@error) > 0) begin
				print $TT_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end

			SELECT @error = @error + convert(varchar,[ID]) + ','
			FROM [lasmart_t_dim_Net] with (nolock)
			where ParentNetID = $ID and Deleted = 0
			order by [ID]

			if (LEN(@error) > 0) begin
				print $Net_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end

			update lasmart_t_dim_MasterStores
			set NetID = null
			where NetID = $ID
		end

		update [lasmart_t_dim_Net]
		set [Name] = $Name
		,[ShortName] = $ShortName
		,[NetTypeID] = $NetTypeID
		,[isNielsen] = $isNielsen
		,[RespManagerID] = $RespManagerID
		,[ParentNetID] = $ParentNetID
		,[hasSalesData] = $hasSalesData
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='NetID', @Value=$ID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_un.txt',ob_get_clean());
		if (mssql_query( $query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0 || strpos($message,'У этой сети есть') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getParentNet($connection, $_REQUEST) {
    		$arr = array();
    		$query = "select n.[ID]
    			,n.[Name]
    			,n.[ShortName]
    			,n.[NetTypeID]
    			,ntp.[Name] as [NetType]
    			,n.[isNielsen]
    			,n.[RespManagerID]
    			,ms.[FIO] as [RespManager]
    			,n.[hasSalesData]
    			,n.[Deleted]
    		FROM [lasmart_t_dim_Net] n
    		left join [lasmart_t_dim_NetType] ntp on n.[NetTypeID] = ntp.[ID]
    		left join [lasmart_t_dim_MasterStaff] ms on n.[RespManagerID] = ms.[ID]
    		where n.Deleted = 0 and ParentNetID is null";

    		$result = mssql_query($query,$connection);
    		while ($row = mssql_fetch_assoc($result)) {
    			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
    			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
    			$row['NetType'] = iconv("cp1251", "UTF-8", $row['NetType']);
    			$row['RespManager'] = iconv("cp1251", "UTF-8", $row['RespManager']);
    			$arr [] = $row;
    		}
    		return $arr;
    	}

	function getMotivation($connection, $_REQUEST) {
		$arr = array();
		$query = "select m.[ID]
			,m.[CurrancyID]
			,c.Name as [Currancy]
			,m.[Name]
			,m.[ShortName]
			,m.[BriefNotation]
			,m.[Deleted]			
		FROM [lasmart_t_dim_Motivation] m
		left join [lasmart_t_dim_Currancy] c on m.[CurrancyID] = c.[ID]
		where m.Deleted = 0

		union all

		select m.[ID]
			,m.[CurrancyID]
			,c.Name as [Currancy]
			,m.[Name]
			,m.[ShortName]
			,m.[BriefNotation]
			,m.[Deleted]			
		FROM [lasmart_t_dim_Motivation] m
		left join [lasmart_t_dim_Currancy] c on m.[CurrancyID] = c.[ID]
		where m.Deleted = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$row['Currancy'] = iconv("cp1251", "UTF-8", $row['Currancy']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createMotivation($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$CurrancyID = parseParameter($_REQUEST['CurrancyID']);

		$query =
		"insert into [lasmart_t_dim_Motivation]
		([Name]
		,[CurrancyID]
		,[ShortName]
		,[BriefNotation]
		,[Deleted])
		VALUES($Name, $CurrancyID, $ShortName,$BriefNotation,$Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}

		return $arr;
	}

	function updateMotivation($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$CurrancyID = parseParameter($_REQUEST['CurrancyID']);

		$Staff_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Сотрудники" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,[ID]) + ','
			FROM [lasmart_t_dim_MasterStaff] with (nolock)
			where MotivationID = $ID and StatusID = 2
			order by [ID]
				
			if (LEN(@error) > 0) begin
				print $Staff_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				update lasmart_t_dim_MasterStaff
				set MotivationID = null
				where MotivationID = $ID
			end
		end

		update [lasmart_t_dim_Motivation]
		set [Name] = $Name
		,[CurrancyID] = $CurrancyID
		,[ShortName] = $ShortName
		,[BriefNotation] = $BriefNotation
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='MotivationID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getCounty($connection, $_REQUEST) {
		$arr = array();
		$query = "select cou.[ID]
			,cou.[Name]
			,cou.[ShortName]
			,cou.[BriefNotation]
			,cou.[CountryID]
			,con.Name as [Country]
			,cou.[Deleted]
		FROM [lasmart_t_dim_County] cou
		left join [lasmart_t_dim_Country] con on cou.[CountryID] = con.[ID]
		where cou.Deleted = 0

		union all

		select cou.[ID]
			,cou.[Name]
			,cou.[ShortName]
			,cou.[BriefNotation]
			,cou.[CountryID]
			,con.Name as [Country]
			,cou.[Deleted]
		FROM [lasmart_t_dim_County] cou
		left join [lasmart_t_dim_Country] con on cou.[CountryID] = con.[ID]
		where cou.Deleted = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$row['Country'] = iconv("cp1251", "UTF-8", $row['Country']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createCounty($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$CountryID = parseParameter($_REQUEST['CountryID']);

		$query =
		"insert into [lasmart_t_dim_County]
		([Name]
		,[ShortName]
		,[BriefNotation]
		,[CountryID]
		,[Deleted])
		VALUES($Name, $ShortName, $BriefNotation, $CountryID, $Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}

		return $arr;
	}

	function updateCounty($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$CountryID = parseParameter($_REQUEST['CountryID']);

		$TT_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Торговые точки" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,s.[ID]) + ','
			FROM [lasmart_t_dim_MasterStores] s with (nolock)
			left join lasmart_t_dim_City cit with (nolock, index = [NonClusteredIndex-20140829-152352]) on s.CityID = cit.ID
			left join lasmart_t_dim_District dis with (nolock) on cit.DistrictID = dis.ID
			left join lasmart_t_dim_Region reg with (nolock) on cit.RegionID = reg.ID or dis.RegionID = reg.ID
			left join lasmart_t_dim_County cou with (nolock) on reg.CountyID = cou.ID
			where cou.ID = $ID and StatusID = 2
			order by s.[ID]
				
			if (LEN(@error) > 0) begin
				print $TT_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				update lasmart_t_dim_Region
				set CountyID = null
				where CountyID = $ID
			end
		end

		update [lasmart_t_dim_County]
		set [Name] = $Name
		,[CountryID] = $CountryID
		,[ShortName] = $ShortName
		,[BriefNotation] = $BriefNotation
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='CountyID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getRegion($connection, $_REQUEST) {
		$arr = array();
		$query = "select reg.[ID]
            ,reg.[Name]
            ,reg.[ShortName]
            ,reg.[BriefNotation]
            ,cou.[CountryID]
            ,con.Name as [Country]
            ,reg.[CountyID]
            ,cou.Name as [County]
            ,reg.[Deleted]
          FROM [lasmart_t_dim_Region] reg
          left join [lasmart_t_dim_County] cou on reg.[CountyID] = cou.[ID]
          left join [lasmart_t_dim_Country] con on cou.[CountryID] = con.[ID]
          where reg.Deleted = 0

          union all

          select reg.[ID]
            ,reg.[Name]
            ,reg.[ShortName]
            ,reg.[BriefNotation]
            ,cou.[CountryID]
            ,con.Name as [Country]
            ,reg.[CountyID]
            ,cou.Name as [County]
            ,reg.[Deleted]
          FROM [lasmart_t_dim_Region] reg
          left join [lasmart_t_dim_County] cou on reg.[CountyID] = cou.[ID]
          left join [lasmart_t_dim_Country] con on cou.[CountryID] = con.[ID]
          where reg.Deleted = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$row['County'] = iconv("cp1251", "UTF-8", $row['County']);
			$row['Country'] = iconv("cp1251", "UTF-8", $row['Country']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createRegion($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$CountyID = parseParameter($_REQUEST['CountyID']);

		$query =
		"insert into [lasmart_t_dim_Region]
		([Name]
		,[ShortName]
		,[BriefNotation]
		,[CountyID]
		,[Deleted])
		VALUES($Name, $ShortName, $BriefNotation, $CountyID, $Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка. Возможно название не уникально';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}

		return $arr;
	}

	function updateRegion($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$CountyID = parseParameter($_REQUEST['CountyID']);

		$TT_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Торговые точки" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,s.[ID]) + ','
			FROM [lasmart_t_dim_MasterStores] s with (nolock)
			left join lasmart_t_dim_City cit with (nolock, index = [NonClusteredIndex-20140829-152352]) on s.CityID = cit.ID
			left join lasmart_t_dim_District dis with (nolock) on cit.DistrictID = dis.ID
			left join lasmart_t_dim_Region reg with (nolock) on cit.RegionID = reg.ID or dis.RegionID = reg.ID
			where reg.ID = $ID and StatusID = 2
			order by s.[ID]
				
			if (LEN(@error) > 0) begin
				print $TT_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				update lasmart_t_dim_City
				set RegionID = null
				where RegionID = $ID
				
				update lasmart_t_dim_District
				set RegionID = null
				where RegionID = $ID
			end
		end

		update [lasmart_t_dim_Region]
		set [Name] = $Name
		,[CountyID] = $CountyID
		,[ShortName] = $ShortName
		,[BriefNotation] = $BriefNotation
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='RegionID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getDistrict($connection, $_REQUEST) {
		$arr = array();
		$query = "select dis.[ID]
            ,dis.[Name]
            ,dis.[ShortName]
            ,dis.[BriefNotation]
            ,cou.[CountryID]
            ,con.Name as [Country]
            ,dis.[RegionID]
            ,reg.Name as [Region]
            ,dis.[Deleted]
          FROM [lasmart_t_dim_District] dis
          left join [lasmart_t_dim_Region] reg on dis.[RegionID] = reg.[ID]
          left join [lasmart_t_dim_County] cou on reg.[CountyID] = cou.[ID]
          left join [lasmart_t_dim_Country] con on cou.[CountryID] = con.[ID]
          where dis.Deleted = 0

          union all

          select dis.[ID]
            ,dis.[Name]
            ,dis.[ShortName]
            ,dis.[BriefNotation]
            ,cou.[CountryID]
            ,con.Name as [Country]
            ,dis.[RegionID]
            ,reg.Name as [Region]
            ,dis.[Deleted]
          FROM [lasmart_t_dim_District] dis
          left join [lasmart_t_dim_Region] reg on dis.[RegionID] = reg.[ID]
          left join [lasmart_t_dim_County] cou on reg.[CountyID] = cou.[ID]
          left join [lasmart_t_dim_Country] con on cou.[CountryID] = con.[ID]
          where dis.Deleted = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$row['Region'] = iconv("cp1251", "UTF-8", $row['Region']);
			$row['Country'] = iconv("cp1251", "UTF-8", $row['Country']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createDistrict($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$RegionID = parseParameter($_REQUEST['RegionID']);

		$query =
		"insert into [lasmart_t_dim_District]
		([Name]
		,[ShortName]
		,[BriefNotation]
		,[RegionID]
		,[Deleted])
		VALUES($Name, $ShortName, $BriefNotation, $RegionID, $Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}

		return $arr;
	}

	function updateDistrict($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$RegionID = parseParameter($_REQUEST['RegionID']);

		$TT_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Торговые точки" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,s.[ID]) + ','
			FROM [lasmart_t_dim_MasterStores] s with (nolock)
			left join lasmart_t_dim_City cit with (nolock, index = [NonClusteredIndex-20140829-152352]) on s.CityID = cit.ID
			where DistrictID = $ID and StatusID = 2
			order by s.[ID]
				
			if (LEN(@error) > 0) begin
				print $TT_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				update lasmart_t_dim_City
				set DistrictID = null
				where DistrictID = $ID
			end
		end

		update [lasmart_t_dim_District]
		set [Name] = $Name
		,[RegionID] = $RegionID
		,[ShortName] = $ShortName
		,[BriefNotation] = $BriefNotation
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='DistrictID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getCity($connection, $_REQUEST) {
		$arr = array();
		$ret = array();
		$where = parseFilters($_REQUEST, 'none');
		$order = parseSorters($_REQUEST);
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));
		
		$search = '';
		if (isset($_REQUEST['ID'])) {
			$where = " where [ID] = ".parseParameter($_REQUEST['ID']);
			$top = 1;
		}

		if (isset($_REQUEST['Name'])) {
			$where = " where [Name] like '%".iconv("UTF-8", "CP1251",str_replace("'","",strip_tags(trim($_REQUEST['Name']))))."%'";
			$top = 10;
		}

		$query = "select top $top * from [lasmart_v_dim_City] $where $order";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gc.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$row['Country'] = iconv("cp1251", "UTF-8", $row['Country']);
			$row['Region'] = iconv("cp1251", "UTF-8", $row['Region']);
			$row['District'] = iconv("cp1251", "UTF-8", $row['District']);
			$row['Sector'] = iconv("cp1251", "UTF-8", $row['Sector']);
			$arr [] = $row;
		}
		if (!isset($_REQUEST['ID'])) {
			$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		}
		$count = 0;
		$query = "select count(*) from [lasmart_v_dim_City] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		$ret['items'] = $arr;
		$ret['total'] = $count;
		return $ret;
	}

	function createCity($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$RegionID = parseParameter($_REQUEST['RegionID']);
		$DistrictID = parseParameter($_REQUEST['DistrictID']);
		$SectorID = parseParameter($_REQUEST['SectorID']);

		$query =
		"insert into [lasmart_t_dim_City]
		([Name]
		,[ShortName]
		,[BriefNotation]
		,[RegionID]
		,[DistrictID]
		,[SectorID]
		,[Deleted])
		VALUES($Name, $ShortName, $BriefNotation, $RegionID, $DistrictID, $SectorID, $Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}

		return $arr;
	}

	function updateCity($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$RegionID = parseParameter($_REQUEST['RegionID']);
		$DistrictID = parseParameter($_REQUEST['DistrictID']);
		$SectorID = parseParameter($_REQUEST['SectorID']);

		$TT_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Торговые точки" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,[ID]) + ','
			FROM [lasmart_t_dim_MasterStores] with (nolock)
			where CityID = $ID and StatusID = 2
			order by [ID]
				
			if (LEN(@error) > 0) begin
				print $TT_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				update lasmart_t_dim_MasterStores
				set CityID = null
				where CityID = $ID
			end
		end

		update [lasmart_t_dim_City]
		set [Name] = $Name
		,[RegionID] = $RegionID
		,[DistrictID] = $DistrictID
		,[SectorID] = $SectorID
		,[ShortName] = $ShortName
		,[BriefNotation] = $BriefNotation
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='CityID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	/*function getStreet($connection, $_REQUEST) {
		$arr = array();
		$query = "select str.[ID]
			,str.[Name]
			,str.[ShortName]
			,str.[BriefNotation]
			,str.[CityID]
			,cit.Name as [City]
			,str.[Deleted]
		FROM [lasmart_t_dim_Street] str
		left join [lasmart_t_dim_City] cit on str.[CityID] = cit.[ID]
		where str.Deleted = 0

		union all

		select str.[ID]
			,str.[Name]
			,str.[ShortName]
			,str.[BriefNotation]
			,str.[CityID]
			,cit.Name as [City]
			,str.[Deleted]
		FROM [lasmart_t_dim_Street] str
		left join [lasmart_t_dim_City] cit on str.[CityID] = cit.[ID]
		where str.Deleted = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$row['City'] = iconv("cp1251", "UTF-8", $row['City']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createStreet($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$CityID = parseParameter($_REQUEST['CityID']);

		$query =
		"insert into [lasmart_t_dim_Street]
		([Name]
		,[ShortName]
		,[BriefNotation]
		,[CityID]
		,[Deleted])
		VALUES($Name, $ShortName, $BriefNotation, $CityID, $Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}

		return $arr;
	}

	function updateStreet($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$CityID = parseParameter($_REQUEST['CityID']);

		$TT_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Торговые точки" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,[ID]) + ','
			FROM [lasmart_t_dim_MasterStores] with (nolock)
			where StreetID = $ID and StatusID = 2
			order by [ID]
				
			if (LEN(@error) > 0) begin
				print $TT_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				update lasmart_t_dim_MasterStores
				set StreetID = null
				where StreetID = $ID
			end
		end

		update [lasmart_t_dim_Street]
		set [Name] = $Name
		,[CityID] = $CityID
		,[ShortName] = $ShortName
		,[BriefNotation] = $BriefNotation
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='StreetID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}*/

	function getBrand($connection, $_REQUEST) {
		$arr = array();
		$query = "select b.[ID]
			,b.[ManufacturerID]
			,m.Name as [Manufacturer]
			,b.[Name]
			,b.[ShortName]
			,b.[BriefNotation]
			,b.[Deleted]
		FROM [lasmart_t_dim_Brand] b
		left join [lasmart_t_dim_Manufacturer] m on b.[ManufacturerID] = m.[ID]
		where b.Deleted = 0

		union all

		select b.[ID]
			,b.[ManufacturerID]
			,m.Name as [Manufacturer]
			,b.[Name]
			,b.[ShortName]
			,b.[BriefNotation]
			,b.[Deleted]
		FROM [lasmart_t_dim_Brand] b
		left join [lasmart_t_dim_Manufacturer] m on b.[ManufacturerID] = m.[ID]
		where b.Deleted = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$row['Manufacturer'] = iconv("cp1251", "UTF-8", $row['Manufacturer']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createBrand($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$ManufacturerID = parseParameter($_REQUEST['ManufacturerID']);

		$query =
		"insert into [lasmart_t_dim_Brand]
		([Name]
		,[ManufacturerID]
		,[ShortName]
		,[BriefNotation]
		,[Deleted])
		VALUES($Name, $ManufacturerID, $ShortName,$BriefNotation,$Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}

		return $arr;
	}

	function updateBrand($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$ManufacturerID = parseParameter($_REQUEST['ManufacturerID']);

		$Sku_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Номенклатура" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,[ID]) + ','
			FROM [lasmart_t_dim_MasterGood] with (nolock)
			where SubBrandID in (select ID from [dbo].[lasmart_t_dim_SubBrand] where BrandID = $ID)
			and StatusID = 2
			order by [ID]
				
			if (LEN(@error) > 0) begin
				print $Sku_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				update lasmart_t_dim_SubBrand
				set BrandID = null
				where BrandID = $ID
			end
		end

		update [lasmart_t_dim_Brand]
		set [Name] = $Name
		,[ManufacturerID] = $ManufacturerID
		,[ShortName] = $ShortName
		,[BriefNotation] = $BriefNotation
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='BrandID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getSubBrand($connection, $_REQUEST) {
		$arr = array();
		$query = "select sb.[ID]
			,b.[ManufacturerID]
			,m.Name as [Manufacturer]
			,sb.[BrandID]
			,b.Name as [Brand]
			,sb.[Name]
			,sb.[ShortName]
			,sb.[BriefNotation]
			,sb.[Deleted]
		FROM [lasmart_t_dim_SubBrand] sb
		left join [lasmart_t_dim_Brand] b on sb.[BrandID] = b.[ID]
		left join [lasmart_t_dim_Manufacturer] m on b.[ManufacturerID] = m.[ID]
		where sb.Deleted = 0

		union all

		select sb.[ID]
			,b.[ManufacturerID]
			,m.Name as [Manufacturer]
			,sb.[BrandID]
			,b.Name as [Brand]
			,sb.[Name]
			,sb.[ShortName]
			,sb.[BriefNotation]
			,sb.[Deleted]
		FROM [lasmart_t_dim_SubBrand] sb
		left join [lasmart_t_dim_Brand] b on sb.[BrandID] = b.[ID]
		left join [lasmart_t_dim_Manufacturer] m on b.[ManufacturerID] = m.[ID]
		where sb.Deleted = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$row['Manufacturer'] = iconv("cp1251", "UTF-8", $row['Manufacturer']);
			$row['Brand'] = iconv("cp1251", "UTF-8", $row['Brand']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createSubBrand($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$BrandID = parseParameter($_REQUEST['BrandID']);

		$query =
		"insert into [lasmart_t_dim_SubBrand]
		([Name]
		,[BrandID]
		,[ShortName]
		,[BriefNotation]
		,[Deleted])
		VALUES($Name, $BrandID, $ShortName,$BriefNotation,$Deleted)";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_csb.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}

		return $arr;
	}

	function updateSubBrand($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$BrandID = parseParameter($_REQUEST['BrandID']);

		$Sku_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Номенклатура" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,[ID]) + ','
			FROM [lasmart_t_dim_MasterGood] with (nolock)
			where SubBrandID = $ID and StatusID = 2
			order by [ID]
				
			if (LEN(@error) > 0) begin
				print $Sku_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				update lasmart_t_dim_MasterGood
				set SubBrandID = null
				where SubBrandID = $ID
			end
		end

		update [lasmart_t_dim_SubBrand]
		set [Name] = $Name
		,[BrandID] = $BrandID
		,[ShortName] = $ShortName
		,[BriefNotation] = $BriefNotation
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='SubBrandID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getSegment($connection, $_REQUEST) {
		$arr = array();
		$query = "select s.[ID]
			,s.[CategoryID]
			,c.Name as [Category]
			,s.[Name]
			,s.[ShortName]
			,s.[BriefNotation]
			,s.[Deleted]
		FROM [lasmart_t_dim_Segment] s
		left join [lasmart_t_dim_Category] c on s.[CategoryID] = c.[ID]
		where s.Deleted = 0

		union all

		select s.[ID]
			,s.[CategoryID]
			,c.Name as [Category]
			,s.[Name]
			,s.[ShortName]
			,s.[BriefNotation]
			,s.[Deleted]
		FROM [lasmart_t_dim_Segment] s
		left join [lasmart_t_dim_Category] c on s.[CategoryID] = c.[ID]
		where s.Deleted = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$row['Category'] = iconv("cp1251", "UTF-8", $row['Category']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createSegment($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$CategoryID = parseParameter($_REQUEST['CategoryID']);

		$query =
		"insert into [lasmart_t_dim_Segment]
		([Name]
		,[CategoryID]
		,[ShortName]
		,[BriefNotation]
		,[Deleted])
		VALUES($Name, $CategoryID, $ShortName,$BriefNotation,$Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}

		return $arr;
	}

	function updateSegment($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$CategoryID = parseParameter($_REQUEST['CategoryID']);

		$Sku_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Номенклатура" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,[ID]) + ','
			FROM [lasmart_t_dim_MasterGood] with (nolock)
			where SubSegmentID in (select ID from [dbo].[lasmart_t_dim_SubSegment] where SegmentID = $ID)
			and StatusID = 2
			order by [ID]
				
			if (LEN(@error) > 0) begin
				print $Sku_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				update lasmart_t_dim_SubSegment
				set SegmentID = null
				where SegmentID = $ID
			end
		end

		update [lasmart_t_dim_Segment]
		set [Name] = $Name
		,[CategoryID] = $CategoryID
		,[ShortName] = $ShortName
		,[BriefNotation] = $BriefNotation
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='SegmentID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function getSubSegment($connection, $_REQUEST) {
		$arr = array();
		$query = "select ss.[ID]
			,s.[CategoryID]
			,c.Name as [Category]
			,ss.[SegmentID]
			,s.Name as [Segment]
			,ss.[Name]
			,ss.[ShortName]
			,ss.[BriefNotation]
			,ss.[Deleted]
		FROM [lasmart_t_dim_SubSegment] ss
		left join [lasmart_t_dim_Segment] s on ss.[SegmentID] = s.[ID]
		left join [lasmart_t_dim_Category] c on s.[CategoryID] = c.[ID]
		where ss.Deleted = 0

		union all

		select ss.[ID]
			,s.[CategoryID]
			,c.Name as [Category]
			,ss.[SegmentID]
			,s.Name as [Segment]
			,ss.[Name]
			,ss.[ShortName]
			,ss.[BriefNotation]
			,ss.[Deleted]
		FROM [lasmart_t_dim_SubSegment] ss
		left join [lasmart_t_dim_Segment] s on ss.[SegmentID] = s.[ID]
		left join [lasmart_t_dim_Category] c on s.[CategoryID] = c.[ID]
		where ss.Deleted = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$row['Category'] = iconv("cp1251", "UTF-8", $row['Category']);
			$row['Segment'] = iconv("cp1251", "UTF-8", $row['Segment']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createSubSegment($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$SegmentID = parseParameter($_REQUEST['SegmentID']);

		$query =
		"insert into [lasmart_t_dim_SubSegment]
		([Name]
		,[SegmentID]
		,[ShortName]
		,[BriefNotation]
		,[Deleted])
		VALUES($Name, $SegmentID, $ShortName,$BriefNotation,$Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}

		return $arr;
	}

	function updateSubSegment($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$SegmentID = parseParameter($_REQUEST['SegmentID']);

		$Sku_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Номенклатура" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,[ID]) + ','
			FROM [lasmart_t_dim_MasterGood] with (nolock)
			where SubSegmentID = $ID and StatusID = 2
			order by [ID]
				
			if (LEN(@error) > 0) begin
				print $Sku_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				update lasmart_t_dim_MasterGood
				set SubSegmentID = null
				where SubSegmentID = $ID
			end
		end

		update [lasmart_t_dim_SubSegment]
		set [Name] = $Name
		,[SegmentID] = $SegmentID
		,[ShortName] = $ShortName
		,[BriefNotation] = $BriefNotation
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='SubSegmentID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function getUsers($connection, $_REQUEST) {
		$arr = array();
		$query = "
			SELECT u.*,ur.SectorID, s.Name as SectorName, un.NetID, n.Name as NetName, uc.CategoryID, c.Name as CategoryName, um.ManufacturerID, m.Name as ManufacturerName
			FROM [lasmart_v_sync_Users] u 
			
			left join [lasmart_t_sync_UsersRights] ur on u.[ID] = ur.UserID
			left join [lasmart_t_dim_Area] s on ur.SectorID = s.ID

			left join [lasmart_t_sync_UsersNets] un on u.[ID] = un.UserID
			left join [lasmart_t_dim_Net] n on un.NetID = n.ID

			left join [lasmart_t_sync_UsersCategories] uc on u.[ID] = uc.UserID
			left join [lasmart_t_dim_Category] c on uc.CategoryID = c.ID

			left join [lasmart_t_sync_UsersManufacturers] um on u.[ID] = um.UserID
			left join [lasmart_t_dim_Manufacturer] m on um.ManufacturerID = m.ID

			order by u.ID, s.Name
		";

		$result = mssql_query($query,$connection);
		$currID = -1;
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			if ($row['ID'] != $currID) {
				$row['sectors'] = array();
				if ($row['SectorID']) {
					$row['sectors'][] = array(
						'ElementID' => $row['SectorID'],
						'Name' => $row['SectorName']
					);
				}

				unset($row['SectorID']);
				unset($row['SectorName']);

				$row['nets'] = array();
				if ($row['NetID']) {
					$row['nets'][] = array(
						'ElementID' => $row['NetID'],
						'Name' => $row['NetName']
					);
				}

				unset($row['NetID']);
				unset($row['NetName']);

				$row['categories'] = array();
				if ($row['CategoryID']) {
					$row['categories'][] = array(
						'ElementID' => $row['CategoryID'],
						'Name' => $row['CategoryName']
					);
				}

				unset($row['CategoryID']);
				unset($row['CategoryName']);

				$row['manufacturers'] = array();
				if ($row['ManufacturerID']) {
					$row['manufacturers'][] = array(
						'ElementID' => $row['ManufacturerID'],
						'Name' => $row['ManufacturerName']
					);
				}

				unset($row['ManufacturerID']);
				unset($row['ManufacturerName']);

				$arr [] = $row;
			} else {
				if ($row['SectorID']) {
					if (!in_array(array(
							'ElementID' => $row['SectorID'],
							'Name' => $row['SectorName']
						), $arr[count($arr)-1]['sectors'])) {
						$arr[count($arr)-1]['sectors'][] = array(
							'ElementID' => $row['SectorID'],
							'Name' => $row['SectorName']
						);
					}
				}
				if ($row['NetID']) {
					if (!in_array(array(
							'ElementID' => $row['NetID'],
							'Name' => $row['NetName']
						), $arr[count($arr)-1]['nets'])) {
						$arr[count($arr)-1]['nets'][] = array(
							'ElementID' => $row['NetID'],
							'Name' => $row['NetName']
						);
					}
				}
				if ($row['CategoryID']) {
					if (!in_array(array(
							'ElementID' => $row['CategoryID'],
							'Name' => $row['CategoryName']
						), $arr[count($arr)-1]['categories'])) {
						$arr[count($arr)-1]['categories'][] = array(
							'ElementID' => $row['CategoryID'],
							'Name' => $row['CategoryName']
						);
					}
				}
				if ($row['ManufacturerID']) {
					if (!in_array(array(
							'ElementID' => $row['ManufacturerID'],
							'Name' => $row['ManufacturerName']
						), $arr[count($arr)-1]['manufacturers'])) {
						$arr[count($arr)-1]['manufacturers'][] = array(
							'ElementID' => $row['ManufacturerID'],
							'Name' => $row['ManufacturerName']
						);
					}
				}
			}
			$currID = $row['ID'];
		}
		return $arr;
	}

	function createUsers($connection, $_REQUEST){
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$RoleID = parseParameter($_REQUEST['RoleID']);
		$FullName = parseParameterWithEncode($_REQUEST['FullName']);
		$EMail = parseParameterWithEncode($_REQUEST['EMail']);
		$isDeleted = isset($_REQUEST['isDeleted']) ? str_replace("'","",strip_tags($_REQUEST['isDeleted'])) : 0;
		$Tel1 = parseParameterWithEncode($_REQUEST['Tel1']);
		$DateBeginAccess = parseParameterWithEncode($_REQUEST['DateBeginAccess']);
		$DateEndAccess = parseParameterWithEncode($_REQUEST['DateEndAccess']);
		$Password = parseParameterWithEncode($_REQUEST['Password']);
		$Comment = parseParameterWithEncode($_REQUEST['Comment']);

		$arr = array();
		$query =
			"
			SET XACT_ABORT ON
			BEGIN TRANSACTION

			insert into [lasmart_t_sync_Users]
			([Name]
			,[RoleID]
			,[FullName]
			,[isDeleted]
			,[EMail]
			,[Tel1]
			,[DateBeginAccess]
			,[DateEndAccess]
			,[Password]
			,[Comment])
			VALUES($Name,$RoleID,$FullName,$isDeleted,$EMail,$Tel1,$DateBeginAccess,$DateEndAccess,$Password,$Comment)

			declare @id int 
			set @id = (select top 1 ID from [lasmart_t_sync_Users] order by ID desc)

			";

		if (isset($_REQUEST['sector'])) {
			$query .= "insert into [lasmart_t_sync_UsersRights] ([UserID],[SectorID])";
			$arr = explode(",",str_replace("'","",strip_tags($_REQUEST['sector'])));
			for($i=0;$i<count($arr);$i++) {
				$query .= " select @id,".$arr[$i]."  union all ";
			}
			$query = substr($query,0,-11);
		} else {
			$query .= "insert into [lasmart_t_sync_UsersRights] ([UserID],[SectorID]) select @id,-1 ";
		}

		if (isset($_REQUEST['net'])) {
			$query .= "insert into [lasmart_t_sync_UsersNets] ([UserID],[NetID])";
			$arr = explode(",",str_replace("'","",strip_tags($_REQUEST['net'])));
			for($i=0;$i<count($arr);$i++) {
				$query .= " select @id,".$arr[$i]."  union all ";
			}
			$query = substr($query,0,-11);
		} else {
			$query .= "insert into [lasmart_t_sync_UsersNets] ([UserID],[NetID]) select @id,-1 ";
		}

		if (isset($_REQUEST['category'])) {
			$query .= "insert into [lasmart_t_sync_UsersCategories] ([UserID],[CategoryID])";
			$arr = explode(",",str_replace("'","",strip_tags($_REQUEST['category'])));
			for($i=0;$i<count($arr);$i++) {
				$query .= " select @id,".$arr[$i]."  union all ";
			}
			$query = substr($query,0,-11);
		} else {
			$query .= "insert into [lasmart_t_sync_UsersCategories] ([UserID],[CategoryID]) select @id,-1 ";
		}

		if (isset($_REQUEST['manufacturer'])) {
			$query .= "insert into [lasmart_t_sync_UsersManufacturers] ([UserID],[ManufacturerID])";
			$arr = explode(",",str_replace("'","",strip_tags($_REQUEST['manufacturer'])));
			for($i=0;$i<count($arr);$i++) {
				$query .= " select @id,".$arr[$i]."  union all ";
			}
			$query = substr($query,0,-11);
		} else {
			$query .= "insert into [lasmart_t_sync_UsersManufacturers] ([UserID],[ManufacturerID]) select @id,-1 ";
		}

		$query .= " COMMIT TRANSACTION";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}

		return $arr;
	}

	function updateUsers($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID пользователя';
			return $arr;
		}
		$RoleID = parseParameter($_REQUEST['RoleID']);
		$FullName = parseParameterWithEncode($_REQUEST['FullName']);
		$isDeleted = isset($_REQUEST['isDeleted']) ? str_replace("'","",strip_tags($_REQUEST['isDeleted'])) : 0;
		$EMail = parseParameterWithEncode($_REQUEST['EMail']);
		$Tel1 = parseParameterWithEncode($_REQUEST['Tel1']);
		$DateBeginAccess = parseParameterWithEncode($_REQUEST['DateBeginAccess']);
		$DateEndAccess = parseParameterWithEncode($_REQUEST['DateEndAccess']);
		$Password = parseParameterWithEncode($_REQUEST['Password']);
		$Comment = parseParameterWithEncode($_REQUEST['Comment']);

		$query =
		"
		SET XACT_ABORT ON
		BEGIN TRANSACTION

		update [lasmart_t_sync_Users]
		set  [RoleID]  = $RoleID
			,[FullName]  = $FullName
			,[isDeleted] = $isDeleted
			,[EMail] = $EMail
			,[Tel1] = $Tel1
			,[DateBeginAccess] = $DateBeginAccess
			,[DateEndAccess] = $DateEndAccess
			,[Password] = $Password
			,[Comment] = $Comment
		where ID = $ID

		delete [lasmart_t_sync_UsersRights] where UserID = $ID

		delete [lasmart_t_sync_UsersNets] where UserID = $ID

		delete [lasmart_t_sync_UsersCategories] where UserID = $ID

		delete [lasmart_t_sync_UsersManufacturers] where UserID = $ID

		";

		if (isset($_REQUEST['sector'])) {
			$query .= "insert into [lasmart_t_sync_UsersRights] ([UserID],[SectorID])";
			$arr = explode(",",str_replace("'","",strip_tags($_REQUEST['sector'])));
			for($i=0;$i<count($arr);$i++) {
				$query .= " select $ID,".$arr[$i]."  union all ";
			}
			$query = substr($query,0,-11);
		} else {
			$query .= "insert into [lasmart_t_sync_UsersRights] ([UserID],[SectorID]) select $ID,-1 ";
		}

		if (isset($_REQUEST['net'])) {
			$query .= "insert into [lasmart_t_sync_UsersNets] ([UserID],[NetID])";
			$arr = explode(",",str_replace("'","",strip_tags($_REQUEST['net'])));
			for($i=0;$i<count($arr);$i++) {
				$query .= " select $ID,".$arr[$i]."  union all ";
			}
			$query = substr($query,0,-11);
		} else {
			$query .= "insert into [lasmart_t_sync_UsersNets] ([UserID],[NetID]) select $ID,-1 ";
		}

		if (isset($_REQUEST['category'])) {
			$query .= "insert into [lasmart_t_sync_UsersCategories] ([UserID],[CategoryID])";
			$arr = explode(",",str_replace("'","",strip_tags($_REQUEST['category'])));
			for($i=0;$i<count($arr);$i++) {
				$query .= " select $ID,".$arr[$i]."  union all ";
			}
			$query = substr($query,0,-11);
		} else {
			$query .= "insert into [lasmart_t_sync_UsersCategories] ([UserID],[CategoryID]) select $ID,-1 ";
		}

		if (isset($_REQUEST['manufacturer'])) {
			$query .= "insert into [lasmart_t_sync_UsersManufacturers] ([UserID],[ManufacturerID])";
			$arr = explode(",",str_replace("'","",strip_tags($_REQUEST['manufacturer'])));
			for($i=0;$i<count($arr);$i++) {
				$query .= " select $ID,".$arr[$i]."  union all ";
			}
			$query = substr($query,0,-11);
		} else {
			$query .= "insert into [lasmart_t_sync_UsersManufacturers] ([UserID],[ManufacturerID]) select $ID,-1 ";
		}

		$query .= " COMMIT TRANSACTION";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function deleteUsers($connection, $_REQUEST){
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID пользователя';
			return $arr;
		}
		
		$query =
		"SET XACT_ABORT ON
		BEGIN TRANSACTION
		
		delete [lasmart_t_sync_UsersRights] where UserID = $ID
		delete [lasmart_t_sync_UsersNets] where UserID = $ID
		delete [lasmart_t_sync_UsersCategories] where UserID = $ID
		delete [lasmart_t_sync_UsersManufacturers] where UserID = $ID
		delete [lasmart_t_sync_Users] where ID = $ID		

		COMMIT TRANSACTION";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function getRoles($connection, $_REQUEST) {
		$arr = array();
		$query = "SELECT * FROM [lasmart_v_sync_UsersRoles]";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createRoles($connection, $_REQUEST){
		$Name = parseParameterWithEncode($_REQUEST['Name']);

		$Sku = isset($_REQUEST['Sku']) ? str_replace("'","",strip_tags($_REQUEST['Sku'])) : 0;
		$TT = isset($_REQUEST['TT']) ? str_replace("'","",strip_tags($_REQUEST['TT'])) : 0;
		$Staff = isset($_REQUEST['Staff']) ? str_replace("'","",strip_tags($_REQUEST['Staff'])) : 0;
		$Distr = isset($_REQUEST['Distr']) ? str_replace("'","",strip_tags($_REQUEST['Distr'])) : 0;
		$Attributes = isset($_REQUEST['Attributes']) ? str_replace("'","",strip_tags($_REQUEST['Attributes'])) : 0;
		$Wrhs = isset($_REQUEST['Wrhs']) ? str_replace("'","",strip_tags($_REQUEST['Wrhs'])) : 0;
		$Advertising = isset($_REQUEST['Advertising']) ? str_replace("'","",strip_tags($_REQUEST['Advertising'])) : 0;
		
		$Sku_draft_get 				= isset($_REQUEST['Sku_draft_get']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_draft_get'])) 			: 0;
		$Sku_draft_create 			= isset($_REQUEST['Sku_draft_create']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_draft_create'])) 		: 0;
		$Sku_draft_set 				= isset($_REQUEST['Sku_draft_set']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_draft_set'])) 			: 0;
		$Sku_draft_delete 			= isset($_REQUEST['Sku_draft_delete']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_draft_delete'])) 		: 0;
		$Sku_draft_activate 		= isset($_REQUEST['Sku_draft_activate']) 		? str_replace("'","",strip_tags($_REQUEST['Sku_draft_activate'])) 		: 0;
		$Sku_active_get 			= isset($_REQUEST['Sku_active_get']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_active_get'])) 			: 0;
		$Sku_active_set 			= isset($_REQUEST['Sku_active_set']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_active_set'])) 			: 0;
		$Sku_active_delete 			= isset($_REQUEST['Sku_active_delete']) 		? str_replace("'","",strip_tags($_REQUEST['Sku_active_delete'])) 		: 0;
		$Sku_deleted_get 			= isset($_REQUEST['Sku_deleted_get']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_deleted_get'])) 			: 0;
		$Sku_slave_tradent_get 		= isset($_REQUEST['Sku_slave_tradent_get']) 	? str_replace("'","",strip_tags($_REQUEST['Sku_slave_tradent_get'])) 	: 0;
		$Sku_slave_net_get 			= isset($_REQUEST['Sku_slave_net_get']) 		? str_replace("'","",strip_tags($_REQUEST['Sku_slave_net_get'])) 		: 0;
		$Sku_slave_distr_get 		= isset($_REQUEST['Sku_slave_distr_get']) 		? str_replace("'","",strip_tags($_REQUEST['Sku_slave_distr_get'])) 		: 0;
		$Sku_slave_sync 			= isset($_REQUEST['Sku_slave_sync']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_slave_sync'])) 			: 0;

		$Sku_PriceSegmentID 		= isset($_REQUEST['Sku_PriceSegmentID']) 		? str_replace("'","",strip_tags($_REQUEST['Sku_PriceSegmentID'])) 		: 0;
		$Sku_WeightGroupID 			= isset($_REQUEST['Sku_WeightGroupID']) 		? str_replace("'","",strip_tags($_REQUEST['Sku_WeightGroupID'])) 		: 0;

		$TT_draft_get 				= isset($_REQUEST['TT_draft_get']) 				? str_replace("'","",strip_tags($_REQUEST['TT_draft_get'])) 			: 0;
		$TT_draft_create 			= isset($_REQUEST['TT_draft_create']) 			? str_replace("'","",strip_tags($_REQUEST['TT_draft_create'])) 			: 0;
		$TT_draft_set 				= isset($_REQUEST['TT_draft_set']) 				? str_replace("'","",strip_tags($_REQUEST['TT_draft_set'])) 			: 0;
		$TT_draft_delete 			= isset($_REQUEST['TT_draft_delete']) 			? str_replace("'","",strip_tags($_REQUEST['TT_draft_delete'])) 			: 0;
		$TT_draft_activate 			= isset($_REQUEST['TT_draft_activate']) 		? str_replace("'","",strip_tags($_REQUEST['TT_draft_activate'])) 		: 0;
		$TT_active_get 				= isset($_REQUEST['TT_active_get']) 			? str_replace("'","",strip_tags($_REQUEST['TT_active_get'])) 			: 0;
		$TT_active_set 				= isset($_REQUEST['TT_active_set']) 			? str_replace("'","",strip_tags($_REQUEST['TT_active_set'])) 			: 0;
		$TT_active_delete 			= isset($_REQUEST['TT_active_delete']) 			? str_replace("'","",strip_tags($_REQUEST['TT_active_delete'])) 		: 0;
		$TT_deleted_get 			= isset($_REQUEST['TT_deleted_get']) 			? str_replace("'","",strip_tags($_REQUEST['TT_deleted_get'])) 			: 0;
		$TT_slave_tradent_get 		= isset($_REQUEST['TT_slave_tradent_get'])		? str_replace("'","",strip_tags($_REQUEST['TT_slave_tradent_get'])) 	: 0;
		$TT_slave_net_get 			= isset($_REQUEST['TT_slave_net_get'])			? str_replace("'","",strip_tags($_REQUEST['TT_slave_net_get'])) 		: 0;
		$TT_slave_distr_get 		= isset($_REQUEST['TT_slave_distr_get']) 		? str_replace("'","",strip_tags($_REQUEST['TT_slave_distr_get'])) 		: 0;
		$TT_slave_sync 				= isset($_REQUEST['TT_slave_sync']) 			? str_replace("'","",strip_tags($_REQUEST['TT_slave_sync'])) 			: 0;
		$TT_AdvChannel_set 			= isset($_REQUEST['TT_AdvChannel_set']) 		? str_replace("'","",strip_tags($_REQUEST['TT_AdvChannel_set'])) 		: 0;

		$Staff_draft_get 			= isset($_REQUEST['Staff_draft_get']) 			? str_replace("'","",strip_tags($_REQUEST['Staff_draft_get'])) 			: 0;
		$Staff_draft_create 		= isset($_REQUEST['Staff_draft_create']) 		? str_replace("'","",strip_tags($_REQUEST['Staff_draft_create'])) 		: 0;
		$Staff_draft_set 			= isset($_REQUEST['Staff_draft_set']) 			? str_replace("'","",strip_tags($_REQUEST['Staff_draft_set'])) 			: 0;
		$Staff_draft_delete 		= isset($_REQUEST['Staff_draft_delete']) 		? str_replace("'","",strip_tags($_REQUEST['Staff_draft_delete'])) 		: 0;
		$Staff_draft_activate 		= isset($_REQUEST['Staff_draft_activate']) 		? str_replace("'","",strip_tags($_REQUEST['Staff_draft_activate'])) 	: 0;
		$Staff_active_get 			= isset($_REQUEST['Staff_active_get']) 			? str_replace("'","",strip_tags($_REQUEST['Staff_active_get'])) 		: 0;
		$Staff_active_set 			= isset($_REQUEST['Staff_active_set']) 			? str_replace("'","",strip_tags($_REQUEST['Staff_active_set'])) 		: 0;
		$Staff_active_delete 		= isset($_REQUEST['Staff_active_delete']) 		? str_replace("'","",strip_tags($_REQUEST['Staff_active_delete'])) 		: 0;
		$Staff_deleted_get 			= isset($_REQUEST['Staff_deleted_get']) 		? str_replace("'","",strip_tags($_REQUEST['Staff_deleted_get'])) 		: 0;
		$Staff_slave_tradent_get 	= isset($_REQUEST['Staff_slave_tradent_get'])	? str_replace("'","",strip_tags($_REQUEST['Staff_slave_tradent_get']))	: 0;
		$Staff_slave_net_get 		= isset($_REQUEST['Staff_slave_net_get']) 		? str_replace("'","",strip_tags($_REQUEST['Staff_slave_net_get'])) 		: 0;
		$Staff_slave_distr_get 		= isset($_REQUEST['Staff_slave_distr_get']) 	? str_replace("'","",strip_tags($_REQUEST['Staff_slave_distr_get'])) 	: 0;
		$Staff_slave_sync 			= isset($_REQUEST['Staff_slave_sync']) 			? str_replace("'","",strip_tags($_REQUEST['Staff_slave_sync'])) 		: 0;

		$Distr_draft_get 			= isset($_REQUEST['Distr_draft_get']) 			? str_replace("'","",strip_tags($_REQUEST['Distr_draft_get'])) 			: 0;
		$Distr_draft_create 		= isset($_REQUEST['Distr_draft_create']) 		? str_replace("'","",strip_tags($_REQUEST['Distr_draft_create'])) 		: 0;
		$Distr_draft_set 			= isset($_REQUEST['Distr_draft_set']) 			? str_replace("'","",strip_tags($_REQUEST['Distr_draft_set'])) 			: 0;
		$Distr_draft_delete 		= isset($_REQUEST['Distr_draft_delete']) 		? str_replace("'","",strip_tags($_REQUEST['Distr_draft_delete'])) 		: 0;
		$Distr_draft_activate 		= isset($_REQUEST['Distr_draft_activate']) 		? str_replace("'","",strip_tags($_REQUEST['Distr_draft_activate'])) 	: 0;
		$Distr_active_get 			= isset($_REQUEST['Distr_active_get']) 			? str_replace("'","",strip_tags($_REQUEST['Distr_active_get'])) 		: 0;
		$Distr_active_set 			= isset($_REQUEST['Distr_active_set']) 			? str_replace("'","",strip_tags($_REQUEST['Distr_active_set'])) 		: 0;
		$Distr_active_delete 		= isset($_REQUEST['Distr_active_delete']) 		? str_replace("'","",strip_tags($_REQUEST['Distr_active_delete'])) 		: 0;
		$Distr_deleted_get 			= isset($_REQUEST['Distr_deleted_get']) 		? str_replace("'","",strip_tags($_REQUEST['Distr_deleted_get'])) 		: 0;

		$Attributes_Adding 			= isset($_REQUEST['Attributes_Adding']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Adding'])) 		: 0;
        $Attributes_Brand 			= isset($_REQUEST['Attributes_Brand']) 			? str_replace("'","",strip_tags($_REQUEST['Attributes_Brand'])) 		: 0;
        $Attributes_Category 		= isset($_REQUEST['Attributes_Category']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Category'])) 		: 0;
        $Attributes_Channel 		= isset($_REQUEST['Attributes_Channel']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Channel'])) 		: 0;
        $Attributes_City 			= isset($_REQUEST['Attributes_City']) 			? str_replace("'","",strip_tags($_REQUEST['Attributes_City'])) 		    : 0;
        $Attributes_CoffeeSort 		= isset($_REQUEST['Attributes_CoffeeSort']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_CoffeeSort'])) 	: 0;
        $Attributes_Country 		= isset($_REQUEST['Attributes_Country']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Country'])) 		: 0;
        $Attributes_County 			= isset($_REQUEST['Attributes_County']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_County'])) 		: 0;
        $Attributes_Currancy 		= isset($_REQUEST['Attributes_Currancy']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Currancy'])) 		: 0;
        $Attributes_District 		= isset($_REQUEST['Attributes_District']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_District'])) 		: 0;
        $Attributes_Division 		= isset($_REQUEST['Attributes_Division']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Division'])) 		: 0;
        $Attributes_ExternalPack 	= isset($_REQUEST['Attributes_ExternalPack']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_ExternalPack'])) 	: 0;
        $Attributes_Flavor 			= isset($_REQUEST['Attributes_Flavor']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Flavor'])) 		: 0;
        $Attributes_Format 			= isset($_REQUEST['Attributes_Format']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Format'])) 		: 0;
        $Attributes_Gift 			= isset($_REQUEST['Attributes_Gift']) 			? str_replace("'","",strip_tags($_REQUEST['Attributes_Gift'])) 		    : 0;
        $Attributes_GiftType 		= isset($_REQUEST['Attributes_GiftType']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_GiftType'])) 		: 0;
        $Attributes_InternalPack 	= isset($_REQUEST['Attributes_InternalPack']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_InternalPack'])) 	: 0;
        $Attributes_Manufacturer 	= isset($_REQUEST['Attributes_Manufacturer']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_Manufacturer'])) 	: 0;
        $Attributes_Motivation		= isset($_REQUEST['Attributes_Motivation']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_Motivation'])) 	: 0;
        $Attributes_Net 			= isset($_REQUEST['Attributes_Net']) 			? str_replace("'","",strip_tags($_REQUEST['Attributes_Net'])) 		    : 0;
        $Attributes_NetType 		= isset($_REQUEST['Attributes_NetType']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_NetType'])) 		: 0;
        $Attributes_NonAccReason	= isset($_REQUEST['Attributes_NonAccReason']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_NonAccReason'])) 	: 0;
        $Attributes_PriceSegment 	= isset($_REQUEST['Attributes_PriceSegment']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_PriceSegment'])) 	: 0;
        $Attributes_Region 			= isset($_REQUEST['Attributes_Region']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Region'])) 		: 0;
        $Attributes_SachetPack 		= isset($_REQUEST['Attributes_SachetPack']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_SachetPack'])) 	: 0;
        $Attributes_Sector			= isset($_REQUEST['Attributes_Sector']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Sector'])) 		: 0;
        $Attributes_Segment 		= isset($_REQUEST['Attributes_Segment']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Segment'])) 		: 0;
        $Attributes_SubBrand 		= isset($_REQUEST['Attributes_SubBrand']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_SubBrand'])) 		: 0;
        $Attributes_SubChannel 		= isset($_REQUEST['Attributes_SubChannel']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_SubChannel'])) 	: 0;
        $Attributes_SubSegment 		= isset($_REQUEST['Attributes_SubSegment']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_SubSegment'])) 	: 0;
        $Attributes_TeaColor 		= isset($_REQUEST['Attributes_TeaColor']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_TeaColor'])) 		: 0;
        $Attributes_TypeStaff 		= isset($_REQUEST['Attributes_TypeStaff']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_TypeStaff'])) 	: 0;
        $Attributes_WeightGroup 	= isset($_REQUEST['Attributes_WeightGroup']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_WeightGroup'])) 	: 0;
        $Attributes_NetSalesDataSource 	= isset($_REQUEST['Attributes_NetSalesDataSource']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_NetSalesDataSource'])) 	: 0;
        $Attributes_Area 	= isset($_REQUEST['Attributes_Area']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_Area'])) 	: 0;
        $Attributes_RequestStatus 	= isset($_REQUEST['Attributes_RequestStatus']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_RequestStatus'])) 	: 0;
        $Attributes_AdvertisingChannel 	= isset($_REQUEST['Attributes_AdvertisingChannel']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_AdvertisingChannel'])) 	: 0;
        $Attributes_PaymentType 	= isset($_REQUEST['Attributes_PaymentType']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_PaymentType'])) 	: 0;

        $Wrhs_draft_get 		= isset($_REQUEST['Wrhs_draft_get']) 			? str_replace("'","",strip_tags($_REQUEST['Wrhs_draft_get'])) 			: 0;
		$Wrhs_draft_create 		= isset($_REQUEST['Wrhs_draft_create']) 		? str_replace("'","",strip_tags($_REQUEST['Wrhs_draft_create'])) 		: 0;
		$Wrhs_draft_set 		= isset($_REQUEST['Wrhs_draft_set']) 			? str_replace("'","",strip_tags($_REQUEST['Wrhs_draft_set'])) 			: 0;
		$Wrhs_draft_delete 		= isset($_REQUEST['Wrhs_draft_delete']) 		? str_replace("'","",strip_tags($_REQUEST['Wrhs_draft_delete'])) 		: 0;
		$Wrhs_draft_activate 	= isset($_REQUEST['Wrhs_draft_activate']) 		? str_replace("'","",strip_tags($_REQUEST['Wrhs_draft_activate'])) 	: 0;
		$Wrhs_active_get 		= isset($_REQUEST['Wrhs_active_get']) 			? str_replace("'","",strip_tags($_REQUEST['Wrhs_active_get'])) 		: 0;
		$Wrhs_active_set 		= isset($_REQUEST['Wrhs_active_set']) 			? str_replace("'","",strip_tags($_REQUEST['Wrhs_active_set'])) 		: 0;
		$Wrhs_active_delete 	= isset($_REQUEST['Wrhs_active_delete']) 		? str_replace("'","",strip_tags($_REQUEST['Wrhs_active_delete'])) 		: 0;
		$Wrhs_deleted_get 		= isset($_REQUEST['Wrhs_deleted_get']) 			? str_replace("'","",strip_tags($_REQUEST['Wrhs_deleted_get'])) 		: 0;
		$Wrhs_slave_sync 		= isset($_REQUEST['Wrhs_slave_sync']) 			? str_replace("'","",strip_tags($_REQUEST['Wrhs_slave_sync'])) 		: 0;

		$Advertising_boss 		= isset($_REQUEST['Advertising_boss']) 			? str_replace("'","",strip_tags($_REQUEST['Advertising_boss'])) 			: 0;
		$Advertising_boss_set_stat 		= isset($_REQUEST['Advertising_boss_set_stat']) 			? str_replace("'","",strip_tags($_REQUEST['Advertising_boss_set_stat'])) 			: 0;

		$Advertising_events 	= isset($_REQUEST['Advertising_events']) 		? str_replace("'","",strip_tags($_REQUEST['Advertising_events'])) 		: 0;
		$Advertising_events_edit 	= isset($_REQUEST['Advertising_events_edit']) 		? str_replace("'","",strip_tags($_REQUEST['Advertising_events_edit'])) 		: 0;
		$Advertising_events_adv_appr 	= isset($_REQUEST['Advertising_events_adv_appr']) 		? str_replace("'","",strip_tags($_REQUEST['Advertising_events_adv_appr'])) 		: 0;
		$Advertising_events_sales_appr 	= isset($_REQUEST['Advertising_events_sales_appr']) 		? str_replace("'","",strip_tags($_REQUEST['Advertising_events_sales_appr'])) 		: 0;
		$Advertising_events_s_boss_appr 	= isset($_REQUEST['Advertising_events_s_boss_appr']) 		? str_replace("'","",strip_tags($_REQUEST['Advertising_events_s_boss_appr'])) 		: 0;
		$Advertising_events_net_appr 	= isset($_REQUEST['Advertising_events_net_appr']) 		? str_replace("'","",strip_tags($_REQUEST['Advertising_events_net_appr'])) 		: 0;
		$Advertising_events_manual 	= isset($_REQUEST['Advertising_events_manual']) 		? str_replace("'","",strip_tags($_REQUEST['Advertising_events_manual'])) 		: 0;
		$Advertising_events_delete 	= isset($_REQUEST['Advertising_events_delete']) 		? str_replace("'","",strip_tags($_REQUEST['Advertising_events_delete'])) 		: 0;
		$Advertising_events_rp_appr 	= isset($_REQUEST['Advertising_events_rp_appr']) 		? str_replace("'","",strip_tags($_REQUEST['Advertising_events_rp_appr'])) 		: 0;
		$Advertising_events_fact_appr 	= isset($_REQUEST['Advertising_events_fact_appr']) 		? str_replace("'","",strip_tags($_REQUEST['Advertising_events_fact_appr'])) 		: 0;		

        if($Attributes_SubBrand == 2 && $Attributes_Brand == 0) {
            $Attributes_Brand = 1;
        }

        if($Attributes_Brand == 2 && $Attributes_Manufacturer == 0) {
            $Attributes_Manufacturer = 1;
        }

        if($Attributes_SubSegment == 2 && $Attributes_Segment == 0) {
            $Attributes_Segment = 1;
        }

        if($Attributes_Segment == 2 && $Attributes_Category == 0) {
            $Attributes_Category = 1;
        }

        if($Attributes_SubChannel == 2 && $Attributes_Channel == 0) {
            $Attributes_Channel = 1;
        }

        if($Attributes_Net == 2 && $Attributes_NetType == 0) {
            $Attributes_NetType = 1;
        }

        if($Attributes_Motivation == 2 && $Attributes_Currancy == 0) {
            $Attributes_Currancy = 1;
        }

        if($Attributes_Sector == 2 && $Attributes_Area == 0) {
            $Attributes_Area = 1;
        }

        if($Attributes_Area == 2 && $Attributes_Division == 0) {
            $Attributes_Division = 1;
        }

        if($Attributes_City == 2 && $Attributes_District == 0) {
            $Attributes_District = 1;

        }

        if($Attributes_City == 2 && $Attributes_Region == 0) {
            $Attributes_Region = 1;
        }

        if($Attributes_District == 2 && $Attributes_Region == 0) {
            $Attributes_Region = 1;
        }

        if($Attributes_Region == 2 && $Attributes_County == 0) {
            $Attributes_County = 1;
        }

        if($Attributes_County == 2 && $Attributes_Country == 0) {
            $Attributes_Country = 1;
        }

		$arr = array();
		$query =
			"insert into [lasmart_t_sync_UsersRoles]
			([Name],[Sku],[TT],[Staff],[Distr],[Attributes],[Wrhs],[Advertising]
			,[Sku_draft_get],[Sku_draft_create],[Sku_draft_set],[Sku_draft_delete],[Sku_draft_activate],[Sku_active_get],[Sku_active_set],[Sku_active_delete],[Sku_deleted_get],[Sku_slave_tradent_get],[Sku_slave_net_get],[Sku_slave_distr_get],[Sku_slave_sync]
			,[Sku_PriceSegmentID],[Sku_WeightGroupID]
			,[TT_draft_get],[TT_draft_create],[TT_draft_set],[TT_draft_delete],[TT_draft_activate],[TT_active_get],[TT_active_set],[TT_active_delete],[TT_deleted_get],[TT_slave_tradent_get],[TT_slave_net_get],[TT_slave_distr_get],[TT_slave_sync],[TT_AdvChannel_set]
			,[Staff_draft_get],[Staff_draft_create],[Staff_draft_set],[Staff_draft_delete],[Staff_draft_activate],[Staff_active_get],[Staff_active_set],[Staff_active_delete],[Staff_deleted_get],[Staff_slave_tradent_get],[Staff_slave_net_get],[Staff_slave_distr_get],[Staff_slave_sync]
			,[Distr_draft_get],[Distr_draft_create],[Distr_draft_set],[Distr_draft_delete],[Distr_draft_activate],[Distr_active_get],[Distr_active_set],[Distr_active_delete],[Distr_deleted_get]

			,[Attributes_Adding],[Attributes_Brand],[Attributes_Category],[Attributes_Channel],[Attributes_City],[Attributes_CoffeeSort],[Attributes_Country],[Attributes_County],[Attributes_Currancy],[Attributes_District],[Attributes_Division]
            ,[Attributes_ExternalPack],[Attributes_Flavor],[Attributes_Format],[Attributes_Gift],[Attributes_GiftType],[Attributes_InternalPack],[Attributes_Manufacturer],[Attributes_Motivation],[Attributes_Net],[Attributes_NetType],[Attributes_NonAccReason]
            ,[Attributes_PriceSegment],[Attributes_Region],[Attributes_SachetPack],[Attributes_Sector],[Attributes_Segment],[Attributes_SubBrand],[Attributes_SubChannel],[Attributes_SubSegment],[Attributes_TeaColor],[Attributes_TypeStaff],[Attributes_WeightGroup]
            ,[Attributes_NetSalesDataSource],[Attributes_Area],[Attributes_RequestStatus],[Attributes_AdvertisingChannel],[Attributes_PaymentType]

            ,[Wrhs_draft_get],[Wrhs_draft_create],[Wrhs_draft_set],[Wrhs_draft_delete],[Wrhs_draft_activate],[Wrhs_active_get],[Wrhs_active_set],[Wrhs_active_delete],[Wrhs_deleted_get],[Wrhs_slave_sync]

            ,[Advertising_boss],[Advertising_boss_set_stat],[Advertising_events],[Advertising_events_edit],[Advertising_events_adv_appr],[Advertising_events_sales_appr],[Advertising_events_s_boss_appr],[Advertising_events_net_appr],[Advertising_events_manual],[Advertising_events_delete],[Advertising_events_rp_appr],[Advertising_events_fact_appr]
            )
			
			VALUES($Name,$Sku,$TT,$Staff,$Distr,$Attributes,$Wrhs,$Advertising
			,$Sku_draft_get,$Sku_draft_create,$Sku_draft_set,$Sku_draft_delete,$Sku_draft_activate,$Sku_active_get,$Sku_active_set,$Sku_active_delete,$Sku_deleted_get,$Sku_slave_tradent_get,$Sku_slave_net_get,$Sku_slave_distr_get,$Sku_slave_sync
			,$Sku_PriceSegmentID,$Sku_WeightGroupID
			,$TT_draft_get,$TT_draft_create,$TT_draft_set,$TT_draft_delete,$TT_draft_activate,$TT_active_get,$TT_active_set,$TT_active_delete,$TT_deleted_get,$TT_slave_tradent_get,$TT_slave_net_get,$TT_slave_distr_get,$TT_slave_sync,$TT_AdvChannel_set
			,$Staff_draft_get,$Staff_draft_create,$Staff_draft_set,$Staff_draft_delete,$Staff_draft_activate,$Staff_active_get,$Staff_active_set,$Staff_active_delete,$Staff_deleted_get,$Staff_slave_tradent_get,$Staff_slave_net_get,$Staff_slave_distr_get,$Staff_slave_sync
			,$Distr_draft_get,$Distr_draft_create,$Distr_draft_set,$Distr_draft_delete,$Distr_draft_activate,$Distr_active_get,$Distr_active_set,$Distr_active_delete,$Distr_deleted_get
			
			,$Attributes_Adding,$Attributes_Brand,$Attributes_Category,$Attributes_Channel,$Attributes_City,$Attributes_CoffeeSort,$Attributes_Country,$Attributes_County,$Attributes_Currancy,$Attributes_District,$Attributes_Division
            ,$Attributes_ExternalPack,$Attributes_Flavor,$Attributes_Format,$Attributes_Gift,$Attributes_GiftType,$Attributes_InternalPack,$Attributes_Manufacturer,$Attributes_Motivation,$Attributes_Net,$Attributes_NetType,$Attributes_NonAccReason
            ,$Attributes_PriceSegment,$Attributes_Region,$Attributes_SachetPack,$Attributes_Sector,$Attributes_Segment,$Attributes_SubBrand,$Attributes_SubChannel,$Attributes_SubSegment,$Attributes_TeaColor,$Attributes_TypeStaff,$Attributes_WeightGroup
            ,$Attributes_NetSalesDataSource,$Attributes_Area,$Attributes_RequestStatus,$Attributes_AdvertisingChannel,$Attributes_PaymentType

            ,$Wrhs_draft_get,$Wrhs_draft_create,$Wrhs_draft_set,$Wrhs_draft_delete,$Wrhs_draft_activate,$Wrhs_active_get,$Wrhs_active_set,$Wrhs_active_delete,$Wrhs_deleted_get,$Wrhs_slave_sync
			
			,$Advertising_boss,$Advertising_boss_set_stat,$Advertising_events,$Advertising_events_edit,$Advertising_events_adv_appr,$Advertising_events_sales_appr,$Advertising_events_s_boss_appr,$Advertising_events_net_appr,$Advertising_events_manual,$Advertising_events_delete,$Advertising_events_rp_appr,$Advertising_events_fact_appr
			)";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}

		return $arr;
	}

	function updateRoles($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID';
			return $arr;
		}
		
		$Sku = isset($_REQUEST['Sku']) ? str_replace("'","",strip_tags($_REQUEST['Sku'])) : 0;
		$TT = isset($_REQUEST['TT']) ? str_replace("'","",strip_tags($_REQUEST['TT'])) : 0;
		$Staff = isset($_REQUEST['Staff']) ? str_replace("'","",strip_tags($_REQUEST['Staff'])) : 0;
		$Distr = isset($_REQUEST['Distr']) ? str_replace("'","",strip_tags($_REQUEST['Distr'])) : 0;
		$Attributes = isset($_REQUEST['Attributes']) ? str_replace("'","",strip_tags($_REQUEST['Attributes'])) : 0;
		$Wrhs = isset($_REQUEST['Wrhs']) ? str_replace("'","",strip_tags($_REQUEST['Wrhs'])) : 0;
		$Advertising = isset($_REQUEST['Advertising']) ? str_replace("'","",strip_tags($_REQUEST['Advertising'])) : 0;

		$Sku_draft_get 				= isset($_REQUEST['Sku_draft_get']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_draft_get'])) 			: 0;
		$Sku_draft_create 			= isset($_REQUEST['Sku_draft_create']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_draft_create'])) 		: 0;
		$Sku_draft_set 				= isset($_REQUEST['Sku_draft_set']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_draft_set'])) 			: 0;
		$Sku_draft_delete 			= isset($_REQUEST['Sku_draft_delete']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_draft_delete'])) 		: 0;
		$Sku_draft_activate 		= isset($_REQUEST['Sku_draft_activate']) 		? str_replace("'","",strip_tags($_REQUEST['Sku_draft_activate'])) 		: 0;
		$Sku_active_get 			= isset($_REQUEST['Sku_active_get']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_active_get'])) 			: 0;
		$Sku_active_set 			= isset($_REQUEST['Sku_active_set']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_active_set'])) 			: 0;
		$Sku_active_delete 			= isset($_REQUEST['Sku_active_delete']) 		? str_replace("'","",strip_tags($_REQUEST['Sku_active_delete'])) 		: 0;
		$Sku_deleted_get 			= isset($_REQUEST['Sku_deleted_get']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_deleted_get'])) 		: 0;
		$Sku_slave_tradent_get 		= isset($_REQUEST['Sku_slave_tradent_get']) 	? str_replace("'","",strip_tags($_REQUEST['Sku_slave_tradent_get'])) 	: 0;
		$Sku_slave_net_get 			= isset($_REQUEST['Sku_slave_net_get']) 		? str_replace("'","",strip_tags($_REQUEST['Sku_slave_net_get'])) 		: 0;
		$Sku_slave_distr_get 		= isset($_REQUEST['Sku_slave_distr_get']) 		? str_replace("'","",strip_tags($_REQUEST['Sku_slave_distr_get'])) 	: 0;
		$Sku_slave_sync 			= isset($_REQUEST['Sku_slave_sync']) 			? str_replace("'","",strip_tags($_REQUEST['Sku_slave_sync'])) 			: 0;

		$Sku_PriceSegmentID 		= isset($_REQUEST['Sku_PriceSegmentID']) 		? str_replace("'","",strip_tags($_REQUEST['Sku_PriceSegmentID'])) 		: 0;
		$Sku_WeightGroupID 			= isset($_REQUEST['Sku_WeightGroupID']) 		? str_replace("'","",strip_tags($_REQUEST['Sku_WeightGroupID'])) 		: 0;

		$TT_draft_get 				= isset($_REQUEST['TT_draft_get']) 				? str_replace("'","",strip_tags($_REQUEST['TT_draft_get'])) 			: 0;
		$TT_draft_create 			= isset($_REQUEST['TT_draft_create']) 			? str_replace("'","",strip_tags($_REQUEST['TT_draft_create'])) 		: 0;
		$TT_draft_set 				= isset($_REQUEST['TT_draft_set']) 				? str_replace("'","",strip_tags($_REQUEST['TT_draft_set'])) 			: 0;
		$TT_draft_delete 			= isset($_REQUEST['TT_draft_delete']) 			? str_replace("'","",strip_tags($_REQUEST['TT_draft_delete'])) 		: 0;
		$TT_draft_activate 			= isset($_REQUEST['TT_draft_activate']) 		? str_replace("'","",strip_tags($_REQUEST['TT_draft_activate'])) 		: 0;
		$TT_active_get 				= isset($_REQUEST['TT_active_get']) 			? str_replace("'","",strip_tags($_REQUEST['TT_active_get'])) 			: 0;
		$TT_active_set 				= isset($_REQUEST['TT_active_set']) 			? str_replace("'","",strip_tags($_REQUEST['TT_active_set'])) 			: 0;
		$TT_active_delete 			= isset($_REQUEST['TT_active_delete']) 			? str_replace("'","",strip_tags($_REQUEST['TT_active_delete'])) 		: 0;
		$TT_deleted_get 			= isset($_REQUEST['TT_deleted_get']) 			? str_replace("'","",strip_tags($_REQUEST['TT_deleted_get'])) 			: 0;
		$TT_slave_tradent_get 		= isset($_REQUEST['TT_slave_tradent_get'])		? str_replace("'","",strip_tags($_REQUEST['TT_slave_tradent_get'])) 	: 0;
		$TT_slave_net_get 			= isset($_REQUEST['TT_slave_net_get'])			? str_replace("'","",strip_tags($_REQUEST['TT_slave_net_get'])) 		: 0;
		$TT_slave_distr_get 		= isset($_REQUEST['TT_slave_distr_get']) 		? str_replace("'","",strip_tags($_REQUEST['TT_slave_distr_get'])) 		: 0;
		$TT_slave_sync 				= isset($_REQUEST['TT_slave_sync']) 			? str_replace("'","",strip_tags($_REQUEST['TT_slave_sync'])) 			: 0;
		$TT_AdvChannel_set 			= isset($_REQUEST['TT_AdvChannel_set']) 		? str_replace("'","",strip_tags($_REQUEST['TT_AdvChannel_set'])) 		: 0;

		$Staff_draft_get 			= isset($_REQUEST['Staff_draft_get']) 			? str_replace("'","",strip_tags($_REQUEST['Staff_draft_get'])) 		    : 0;
		$Staff_draft_create 		= isset($_REQUEST['Staff_draft_create']) 		? str_replace("'","",strip_tags($_REQUEST['Staff_draft_create'])) 		: 0;
		$Staff_draft_set 			= isset($_REQUEST['Staff_draft_set']) 			? str_replace("'","",strip_tags($_REQUEST['Staff_draft_set'])) 		    : 0;
		$Staff_draft_delete 		= isset($_REQUEST['Staff_draft_delete']) 		? str_replace("'","",strip_tags($_REQUEST['Staff_draft_delete'])) 		: 0;
		$Staff_draft_activate 		= isset($_REQUEST['Staff_draft_activate']) 		? str_replace("'","",strip_tags($_REQUEST['Staff_draft_activate'])) 	: 0;
		$Staff_active_get 			= isset($_REQUEST['Staff_active_get']) 			? str_replace("'","",strip_tags($_REQUEST['Staff_active_get'])) 		: 0;
		$Staff_active_set 			= isset($_REQUEST['Staff_active_set']) 			? str_replace("'","",strip_tags($_REQUEST['Staff_active_set'])) 		: 0;
		$Staff_active_delete 		= isset($_REQUEST['Staff_active_delete']) 		? str_replace("'","",strip_tags($_REQUEST['Staff_active_delete'])) 	    : 0;
		$Staff_deleted_get 			= isset($_REQUEST['Staff_deleted_get']) 		? str_replace("'","",strip_tags($_REQUEST['Staff_deleted_get'])) 		: 0;
		$Staff_slave_tradent_get 	= isset($_REQUEST['Staff_slave_tradent_get'])	? str_replace("'","",strip_tags($_REQUEST['Staff_slave_tradent_get']))  : 0;
		$Staff_slave_net_get 		= isset($_REQUEST['Staff_slave_net_get']) 		? str_replace("'","",strip_tags($_REQUEST['Staff_slave_net_get'])) 	    : 0;
		$Staff_slave_distr_get 		= isset($_REQUEST['Staff_slave_distr_get']) 	? str_replace("'","",strip_tags($_REQUEST['Staff_slave_distr_get'])) 	: 0;
		$Staff_slave_sync 			= isset($_REQUEST['Staff_slave_sync']) 			? str_replace("'","",strip_tags($_REQUEST['Staff_slave_sync'])) 		: 0;

		$Distr_draft_get 			= isset($_REQUEST['Distr_draft_get']) 			? str_replace("'","",strip_tags($_REQUEST['Distr_draft_get'])) 			: 0;
		$Distr_draft_create 		= isset($_REQUEST['Distr_draft_create']) 		? str_replace("'","",strip_tags($_REQUEST['Distr_draft_create'])) 		: 0;
		$Distr_draft_set 			= isset($_REQUEST['Distr_draft_set']) 			? str_replace("'","",strip_tags($_REQUEST['Distr_draft_set'])) 			: 0;
		$Distr_draft_delete 		= isset($_REQUEST['Distr_draft_delete']) 		? str_replace("'","",strip_tags($_REQUEST['Distr_draft_delete'])) 		: 0;
		$Distr_draft_activate 		= isset($_REQUEST['Distr_draft_activate']) 		? str_replace("'","",strip_tags($_REQUEST['Distr_draft_activate'])) 	: 0;
		$Distr_active_get 			= isset($_REQUEST['Distr_active_get']) 			? str_replace("'","",strip_tags($_REQUEST['Distr_active_get'])) 		: 0;
		$Distr_active_set 			= isset($_REQUEST['Distr_active_set']) 			? str_replace("'","",strip_tags($_REQUEST['Distr_active_set'])) 		: 0;
		$Distr_active_delete 		= isset($_REQUEST['Distr_active_delete']) 		? str_replace("'","",strip_tags($_REQUEST['Distr_active_delete'])) 		: 0;
		$Distr_deleted_get 			= isset($_REQUEST['Distr_deleted_get']) 		? str_replace("'","",strip_tags($_REQUEST['Distr_deleted_get'])) 		: 0;

		$Attributes_Adding 			= isset($_REQUEST['Attributes_Adding']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Adding'])) 		: 0;
		$Attributes_Brand 			= isset($_REQUEST['Attributes_Brand']) 			? str_replace("'","",strip_tags($_REQUEST['Attributes_Brand'])) 		: 0;
		$Attributes_Category 		= isset($_REQUEST['Attributes_Category']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Category'])) 		: 0;
		$Attributes_Channel 		= isset($_REQUEST['Attributes_Channel']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Channel'])) 		: 0;
		$Attributes_City 			= isset($_REQUEST['Attributes_City']) 			? str_replace("'","",strip_tags($_REQUEST['Attributes_City'])) 		    : 0;
		$Attributes_CoffeeSort 		= isset($_REQUEST['Attributes_CoffeeSort']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_CoffeeSort'])) 	: 0;
		$Attributes_Country 		= isset($_REQUEST['Attributes_Country']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Country'])) 		: 0;
		$Attributes_County 			= isset($_REQUEST['Attributes_County']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_County'])) 		: 0;
		$Attributes_Currancy 		= isset($_REQUEST['Attributes_Currancy']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Currancy'])) 		: 0;
		$Attributes_District 		= isset($_REQUEST['Attributes_District']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_District'])) 		: 0;
		$Attributes_Division 		= isset($_REQUEST['Attributes_Division']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Division'])) 		: 0;
		$Attributes_ExternalPack 	= isset($_REQUEST['Attributes_ExternalPack']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_ExternalPack'])) 	: 0;
		$Attributes_Flavor 			= isset($_REQUEST['Attributes_Flavor']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Flavor'])) 		: 0;
		$Attributes_Format 			= isset($_REQUEST['Attributes_Format']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Format'])) 		: 0;
		$Attributes_Gift 			= isset($_REQUEST['Attributes_Gift']) 			? str_replace("'","",strip_tags($_REQUEST['Attributes_Gift'])) 		    : 0;
		$Attributes_GiftType 		= isset($_REQUEST['Attributes_GiftType']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_GiftType'])) 		: 0;
		$Attributes_InternalPack 	= isset($_REQUEST['Attributes_InternalPack']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_InternalPack'])) 	: 0;
		$Attributes_Manufacturer 	= isset($_REQUEST['Attributes_Manufacturer']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_Manufacturer'])) 	: 0;
		$Attributes_Motivation		= isset($_REQUEST['Attributes_Motivation']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_Motivation'])) 	: 0;
		$Attributes_Net 			= isset($_REQUEST['Attributes_Net']) 			? str_replace("'","",strip_tags($_REQUEST['Attributes_Net'])) 		    : 0;
		$Attributes_NetType 		= isset($_REQUEST['Attributes_NetType']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_NetType'])) 		: 0;
		$Attributes_NonAccReason	= isset($_REQUEST['Attributes_NonAccReason']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_NonAccReason'])) 	: 0;
		$Attributes_PriceSegment 	= isset($_REQUEST['Attributes_PriceSegment']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_PriceSegment'])) 	: 0;
		$Attributes_Region 			= isset($_REQUEST['Attributes_Region']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Region'])) 		: 0;
		$Attributes_SachetPack 		= isset($_REQUEST['Attributes_SachetPack']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_SachetPack'])) 	: 0;
		$Attributes_Sector			= isset($_REQUEST['Attributes_Sector']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Sector'])) 		: 0;
		$Attributes_Segment 		= isset($_REQUEST['Attributes_Segment']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_Segment'])) 		: 0;
		$Attributes_SubBrand 		= isset($_REQUEST['Attributes_SubBrand']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_SubBrand'])) 		: 0;
		$Attributes_SubChannel 		= isset($_REQUEST['Attributes_SubChannel']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_SubChannel'])) 	: 0;
		$Attributes_SubSegment 		= isset($_REQUEST['Attributes_SubSegment']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_SubSegment'])) 	: 0;
		$Attributes_TeaColor 		= isset($_REQUEST['Attributes_TeaColor']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_TeaColor'])) 		: 0;
		$Attributes_TypeStaff 		= isset($_REQUEST['Attributes_TypeStaff']) 		? str_replace("'","",strip_tags($_REQUEST['Attributes_TypeStaff'])) 	: 0;
		$Attributes_WeightGroup 	= isset($_REQUEST['Attributes_WeightGroup']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_WeightGroup'])) 	: 0;
		$Attributes_NetSalesDataSource 	= isset($_REQUEST['Attributes_NetSalesDataSource']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_NetSalesDataSource'])) 	: 0;
		$Attributes_Area 	= isset($_REQUEST['Attributes_Area']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_Area'])) 	: 0;
		$Attributes_RequestStatus 	= isset($_REQUEST['Attributes_RequestStatus']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_RequestStatus'])) 	: 0;
		$Attributes_AdvertisingChannel 	= isset($_REQUEST['Attributes_AdvertisingChannel']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_AdvertisingChannel'])) 	: 0;
		$Attributes_PaymentType 	= isset($_REQUEST['Attributes_PaymentType']) 	? str_replace("'","",strip_tags($_REQUEST['Attributes_PaymentType'])) 	: 0;
		
		$Wrhs_draft_get 			= isset($_REQUEST['Wrhs_draft_get']) 			? str_replace("'","",strip_tags($_REQUEST['Wrhs_draft_get'])) 		    : 0;
		$Wrhs_draft_create 			= isset($_REQUEST['Wrhs_draft_create']) 		? str_replace("'","",strip_tags($_REQUEST['Wrhs_draft_create'])) 		: 0;
		$Wrhs_draft_set 			= isset($_REQUEST['Wrhs_draft_set']) 			? str_replace("'","",strip_tags($_REQUEST['Wrhs_draft_set'])) 		    : 0;
		$Wrhs_draft_delete 			= isset($_REQUEST['Wrhs_draft_delete']) 		? str_replace("'","",strip_tags($_REQUEST['Wrhs_draft_delete'])) 		: 0;
		$Wrhs_draft_activate 		= isset($_REQUEST['Wrhs_draft_activate']) 		? str_replace("'","",strip_tags($_REQUEST['Wrhs_draft_activate'])) 	: 0;
		$Wrhs_active_get 			= isset($_REQUEST['Wrhs_active_get']) 			? str_replace("'","",strip_tags($_REQUEST['Wrhs_active_get'])) 		: 0;
		$Wrhs_active_set 			= isset($_REQUEST['Wrhs_active_set']) 			? str_replace("'","",strip_tags($_REQUEST['Wrhs_active_set'])) 		: 0;
		$Wrhs_active_delete 		= isset($_REQUEST['Wrhs_active_delete']) 		? str_replace("'","",strip_tags($_REQUEST['Wrhs_active_delete'])) 	    : 0;
		$Wrhs_deleted_get 			= isset($_REQUEST['Wrhs_deleted_get']) 			? str_replace("'","",strip_tags($_REQUEST['Wrhs_deleted_get'])) 		: 0;
		$Wrhs_slave_sync 			= isset($_REQUEST['Wrhs_slave_sync']) 			? str_replace("'","",strip_tags($_REQUEST['Wrhs_slave_sync'])) 		: 0;

		$Advertising_boss 			= isset($_REQUEST['Advertising_boss']) 			? str_replace("'","",strip_tags($_REQUEST['Advertising_boss'])) 		: 0;
		$Advertising_boss_set_stat 			= isset($_REQUEST['Advertising_boss_set_stat']) 			? str_replace("'","",strip_tags($_REQUEST['Advertising_boss_set_stat'])) 		: 0;
		$Advertising_events 		= isset($_REQUEST['Advertising_events']) 	? str_replace("'","",strip_tags($_REQUEST['Advertising_events'])) 		: 0;
		$Advertising_events_edit 		= isset($_REQUEST['Advertising_events_edit']) 	? str_replace("'","",strip_tags($_REQUEST['Advertising_events_edit'])) 		: 0;
		$Advertising_events_adv_appr 		= isset($_REQUEST['Advertising_events_adv_appr']) 	? str_replace("'","",strip_tags($_REQUEST['Advertising_events_adv_appr'])) 		: 0;
		$Advertising_events_sales_appr 		= isset($_REQUEST['Advertising_events_sales_appr']) 	? str_replace("'","",strip_tags($_REQUEST['Advertising_events_sales_appr'])) 		: 0;
		$Advertising_events_s_boss_appr 		= isset($_REQUEST['Advertising_events_s_boss_appr']) 	? str_replace("'","",strip_tags($_REQUEST['Advertising_events_s_boss_appr'])) 		: 0;
		$Advertising_events_net_appr 		= isset($_REQUEST['Advertising_events_net_appr']) 	? str_replace("'","",strip_tags($_REQUEST['Advertising_events_net_appr'])) 		: 0;
		$Advertising_events_manual 		= isset($_REQUEST['Advertising_events_manual']) 	? str_replace("'","",strip_tags($_REQUEST['Advertising_events_manual'])) 		: 0;
		$Advertising_events_delete 		= isset($_REQUEST['Advertising_events_delete']) 	? str_replace("'","",strip_tags($_REQUEST['Advertising_events_delete'])) 		: 0;
		$Advertising_events_rp_appr 		= isset($_REQUEST['Advertising_events_rp_appr']) 	? str_replace("'","",strip_tags($_REQUEST['Advertising_events_rp_appr'])) 		: 0;
		$Advertising_events_fact_appr 		= isset($_REQUEST['Advertising_events_fact_appr']) 	? str_replace("'","",strip_tags($_REQUEST['Advertising_events_fact_appr'])) 		: 0;		

        if($Attributes_SubBrand == 2 && $Attributes_Brand == 0) {
            $Attributes_Brand = 1;
        }

        if($Attributes_Brand == 2 && $Attributes_Manufacturer == 0) {
            $Attributes_Manufacturer = 1;
        }

        if($Attributes_SubSegment == 2 && $Attributes_Segment == 0) {
            $Attributes_Segment = 1;
        }

        if($Attributes_Segment == 2 && $Attributes_Category == 0) {
            $Attributes_Category = 1;
        }

        if($Attributes_SubChannel == 2 && $Attributes_Channel == 0) {
            $Attributes_Channel = 1;
        }

        if($Attributes_Net == 2 && $Attributes_NetType == 0) {
            $Attributes_NetType = 1;
        }

        if($Attributes_Motivation == 2 && $Attributes_Currancy == 0) {
            $Attributes_Currancy = 1;
        }

        if($Attributes_Sector == 2 && $Attributes_Area == 0) {
            $Attributes_Area = 1;
        }

        if($Attributes_Area == 2 && $Attributes_Division == 0) {
            $Attributes_Division = 1;
        }

        if($Attributes_City == 2 && $Attributes_District == 0) {
            $Attributes_District = 1;

        }

        if($Attributes_City == 2 && $Attributes_Region == 0) {
            $Attributes_Region = 1;
        }

        if($Attributes_District == 2 && $Attributes_Region == 0) {
            $Attributes_Region = 1;
        }

        if($Attributes_Region == 2 && $Attributes_County == 0) {
            $Attributes_County = 1;
        }

        if($Attributes_County == 2 && $Attributes_Country == 0) {
            $Attributes_Country = 1;
        }



		$query =
		"update [lasmart_t_sync_UsersRoles]
		set  [Sku] = $Sku
			,[TT] = $TT
			,[Staff] = $Staff
			,[Distr] = $Distr
			,[Attributes] = $Attributes
			,[Wrhs] = $Wrhs
			,[Advertising] = $Advertising

			,[Sku_draft_get] = $Sku_draft_get
			,[Sku_draft_create] = $Sku_draft_create
			,[Sku_draft_set] = $Sku_draft_set
			,[Sku_draft_delete] = $Sku_draft_delete
			,[Sku_draft_activate] = $Sku_draft_activate
			,[Sku_active_get] = $Sku_active_get
			,[Sku_active_set] = $Sku_active_set
			,[Sku_active_delete] = $Sku_active_delete
			,[Sku_deleted_get] = $Sku_deleted_get
			,[Sku_slave_tradent_get] = $Sku_slave_tradent_get
			,[Sku_slave_net_get] = $Sku_slave_net_get
			,[Sku_slave_distr_get] = $Sku_slave_distr_get
			,[Sku_slave_sync] = $Sku_slave_sync

			,[Sku_PriceSegmentID] = $Sku_PriceSegmentID
			,[Sku_WeightGroupID] = $Sku_WeightGroupID

			,[TT_draft_get] = $TT_draft_get
			,[TT_draft_create] = $TT_draft_create
			,[TT_draft_set] = $TT_draft_set
			,[TT_draft_delete] = $TT_draft_delete
			,[TT_draft_activate] = $TT_draft_activate
			,[TT_active_get] = $TT_active_get
			,[TT_active_set] = $TT_active_set
			,[TT_active_delete] = $TT_active_delete
			,[TT_deleted_get] = $TT_deleted_get
			,[TT_slave_tradent_get] = $TT_slave_tradent_get
			,[TT_slave_net_get] = $TT_slave_net_get
			,[TT_slave_distr_get] = $TT_slave_distr_get
			,[TT_slave_sync] = $TT_slave_sync
			,[TT_AdvChannel_set] = $TT_AdvChannel_set

			,[Staff_draft_get] = $Staff_draft_get
			,[Staff_draft_create] = $Staff_draft_create
			,[Staff_draft_set] = $Staff_draft_set
			,[Staff_draft_delete] = $Staff_draft_delete
			,[Staff_draft_activate] = $Staff_draft_activate
			,[Staff_active_get] = $Staff_active_get
			,[Staff_active_set] = $Staff_active_set
			,[Staff_active_delete] = $Staff_active_delete
			,[Staff_deleted_get] = $Staff_deleted_get
			,[Staff_slave_tradent_get] = $Staff_slave_tradent_get
			,[Staff_slave_net_get] = $Staff_slave_net_get
			,[Staff_slave_distr_get] = $Staff_slave_distr_get
			,[Staff_slave_sync] = $Staff_slave_sync

			,[Distr_draft_get] = $Distr_draft_get
			,[Distr_draft_create] = $Distr_draft_create
			,[Distr_draft_set] = $Distr_draft_set
			,[Distr_draft_delete] = $Distr_draft_delete
			,[Distr_draft_activate] = $Distr_draft_activate
			,[Distr_active_get] = $Distr_active_get
			,[Distr_active_set] = $Distr_active_set
			,[Distr_active_delete] = $Distr_active_delete
			,[Distr_deleted_get] = $Distr_deleted_get

            ,[Attributes_Adding] = $Attributes_Adding
            ,[Attributes_Brand] = $Attributes_Brand
            ,[Attributes_Category] = $Attributes_Category
            ,[Attributes_Channel] = $Attributes_Channel
            ,[Attributes_City] = $Attributes_City
            ,[Attributes_CoffeeSort] = $Attributes_CoffeeSort
            ,[Attributes_Country] = $Attributes_Country
            ,[Attributes_County] = $Attributes_County
            ,[Attributes_Currancy] = $Attributes_Currancy
            ,[Attributes_District] = $Attributes_District
            ,[Attributes_Division] = $Attributes_Division
            ,[Attributes_ExternalPack] = $Attributes_ExternalPack
            ,[Attributes_Flavor] = $Attributes_Flavor
            ,[Attributes_Format] = $Attributes_Format
            ,[Attributes_Gift] = $Attributes_Gift
            ,[Attributes_GiftType] = $Attributes_GiftType
            ,[Attributes_InternalPack] = $Attributes_InternalPack
            ,[Attributes_Manufacturer] = $Attributes_Manufacturer
            ,[Attributes_Motivation] = $Attributes_Motivation
            ,[Attributes_Net] = $Attributes_Net
            ,[Attributes_NetType] = $Attributes_NetType
            ,[Attributes_NonAccReason] = $Attributes_NonAccReason
            ,[Attributes_PriceSegment] = $Attributes_PriceSegment
            ,[Attributes_Region] = $Attributes_Region
            ,[Attributes_SachetPack] = $Attributes_SachetPack
            ,[Attributes_Sector] = $Attributes_Sector
            ,[Attributes_Segment] = $Attributes_Segment
            ,[Attributes_SubBrand] = $Attributes_SubBrand
            ,[Attributes_SubChannel] = $Attributes_SubChannel
            ,[Attributes_SubSegment] = $Attributes_SubSegment
            ,[Attributes_TeaColor] = $Attributes_TeaColor
            ,[Attributes_TypeStaff] = $Attributes_TypeStaff
            ,[Attributes_WeightGroup] = $Attributes_WeightGroup

            ,[Attributes_NetSalesDataSource] = $Attributes_NetSalesDataSource
            ,[Attributes_Area] = $Attributes_Area
            ,[Attributes_RequestStatus] = $Attributes_RequestStatus
            ,[Attributes_AdvertisingChannel] = $Attributes_AdvertisingChannel
            ,[Attributes_PaymentType] = $Attributes_PaymentType

            ,[Wrhs_draft_get] = $Wrhs_draft_get
			,[Wrhs_draft_create] = $Wrhs_draft_create
			,[Wrhs_draft_set] = $Wrhs_draft_set
			,[Wrhs_draft_delete] = $Wrhs_draft_delete
			,[Wrhs_draft_activate] = $Wrhs_draft_activate
			,[Wrhs_active_get] = $Wrhs_active_get
			,[Wrhs_active_set] = $Wrhs_active_set
			,[Wrhs_active_delete] = $Wrhs_active_delete
			,[Wrhs_deleted_get] = $Wrhs_deleted_get
			,[Wrhs_slave_sync] = $Wrhs_slave_sync

			,[Advertising_boss] = $Advertising_boss
			,[Advertising_boss_set_stat] = $Advertising_boss_set_stat
			,[Advertising_events] = $Advertising_events
			,[Advertising_events_edit] = $Advertising_events_edit
			,[Advertising_events_adv_appr] = $Advertising_events_adv_appr
			,[Advertising_events_sales_appr] = $Advertising_events_sales_appr
			,[Advertising_events_s_boss_appr] = $Advertising_events_s_boss_appr
			,[Advertising_events_net_appr] = $Advertising_events_net_appr
			,[Advertising_events_manual] = $Advertising_events_manual
			,[Advertising_events_delete] = $Advertising_events_delete
			,[Advertising_events_rp_appr] = $Advertising_events_rp_appr
			,[Advertising_events_fact_appr] = $Advertising_events_fact_appr
		
		where ID = $ID";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function deleteRoles($connection, $_REQUEST){
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID пользователя';
			return $arr;
		}
		
		$query = 
		"SET XACT_ABORT ON
		BEGIN TRANSACTION

		update [lasmart_t_sync_Users] set RoleID = null, isDeleted = 1 where RoleID = $ID
		delete [lasmart_t_sync_UsersRoles] where ID = $ID

		COMMIT TRANSACTION";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function getSectorUserFilter($connection, $_REQUEST){
		$arr = array();
		$prep = array();
		$checked = isset($_REQUEST['checked']) ? (parseParameter($_REQUEST['checked'])==='true') : false; 

		$query = "select [ID],[Name]
		FROM [lasmart_t_dim_Area]
		where Deleted = 0
		order by [Name]";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$prep['id'] = $row['ID'];
			$prep['text'] = iconv("cp1251", "UTF-8", $row['Name']);
			$prep['value'] = $row['ID'];
			$prep['leaf'] = true;
			$prep['checked'] = $checked;
			$arr [] = $prep;
		}
		return $arr;
	}

	function getNetUserFilter($connection, $_REQUEST){
		$arr = array();
		$prep = array();
		$checked = isset($_REQUEST['checked']) ? (parseParameter($_REQUEST['checked'])==='true') : false; 

		$query = "select [ID],[Name]
		FROM [lasmart_t_dim_Net]
		where Deleted = 0
		order by [Name]";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$prep['id'] = $row['ID'];
			$prep['text'] = iconv("cp1251", "UTF-8", $row['Name']);
			$prep['value'] = $row['ID'];
			$prep['leaf'] = true;
			$prep['checked'] = $checked;
			$arr [] = $prep;
		}
		return $arr;
	}

	function getCategoryUserFilter($connection, $_REQUEST){
		$arr = array();
		$prep = array();
		$checked = isset($_REQUEST['checked']) ? (parseParameter($_REQUEST['checked'])==='true') : false; 

		$query = "select [ID],[Name]
		FROM [lasmart_t_dim_Category]
		where Deleted = 0
		order by [Name]";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$prep['id'] = $row['ID'];
			$prep['text'] = iconv("cp1251", "UTF-8", $row['Name']);
			$prep['value'] = $row['ID'];
			$prep['leaf'] = true;
			$prep['checked'] = $checked;
			$arr [] = $prep;
		}
		return $arr;
	}

	function getManufacturerUserFilter($connection, $_REQUEST){
		$arr = array();
		$prep = array();
		$checked = isset($_REQUEST['checked']) ? (parseParameter($_REQUEST['checked'])==='true') : false; 

		$query = "select [ID],[Name]
		FROM [lasmart_t_dim_Manufacturer]
		where Deleted = 0
		order by [Name]";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$prep['id'] = $row['ID'];
			$prep['text'] = iconv("cp1251", "UTF-8", $row['Name']);
			$prep['value'] = $row['ID'];
			$prep['leaf'] = true;
			$prep['checked'] = $checked;
			$arr [] = $prep;
		}
		return $arr;
	}

	function getTemplate($connection, $_REQUEST) {
		$ID_User = $_SESSION['idUser'];
		$Ref = parseParameterWithEncode($_REQUEST['Ref']);
		$arr = array();
		$query = "select [ID]
			,[Name]
			,[DateCreate]
			,[DateUsed]
			,[State]
			,[StateSlaveLeft]
			,[StateSlaveRight]
			,[Ref]
		FROM [sync_template]
		where [Ref] = $Ref and ID_User = $ID_User";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createTemplate($connection, $_REQUEST){
		$ID_User = $_SESSION['idUser'];
		$Ref = parseParameterWithEncode($_REQUEST['Ref']);
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$State = strip_tags(trim($_REQUEST['State']));
		$StateSlaveLeft = strip_tags(trim($_REQUEST['StateSlaveLeft']));
		$StateSlaveRight = strip_tags(trim($_REQUEST['StateSlaveRight']));
		$arr = array();
		$query =
			"insert into [sync_template]
			([ID_User]
			,[Name]
			,[Ref]
			,[State]
			,[StateSlaveLeft]
			,[StateSlaveRight])
			VALUES($ID_User,$Name,$Ref,'$State','$StateSlaveLeft','$StateSlaveRight')";
		//ob_start();print_r($_SESSION);print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}

		return $arr;
	}

	function deleteTemplate($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не выбран шаблон';
			return $arr;
		}

		$query = "delete [sync_template] WHERE [ID] = $ID";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function updateStatusSku($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$query = "exec [lasmart_p_MasterGood_SetActiveStatus] @UserID = $UserID, @ID = $ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function updateStatusTT($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$query = "exec [lasmart_p_MasterStores_SetActiveStatus] @UserID = $UserID, @ID = $ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function updateStatusStaff($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$query = "exec [lasmart_p_MasterStaff_SetActiveStatus] @UserID = $UserID, @ID = $ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function updateStatusDistr($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$query = "exec [lasmart_p_MasterDistributors_SetActiveStatus] @UserID = $UserID, @ID = $ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function updateStatusWrhs($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$query = "exec [lasmart_p_MasterWarehouses_SetActiveStatus] @UserID = $UserID, @ID = $ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function setDeleteStatusSku($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$query = "exec [lasmart_p_MasterGood_SetDeleteStatus] @UserID = $UserID, @ID = $ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'У позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function setDeleteStatusTT($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи'; //???
			return $arr;
		}

		$UserID = $_SESSION['idUser'];		
		$query = "exec [lasmart_p_MasterStores_SetDeleteStatus] @UserID = $UserID, @ID = $ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'У позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function setDeleteStatusStaff($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи'; //???
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$query = "exec [lasmart_p_MasterStaff_SetDeleteStatus] @UserID = $UserID, @ID = $ID"; //TODO
		//$Event = iconv("UTF-8", "cp1251", "'Статус изменен на Помечена на удаление'");

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'У позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function setDeleteStatusDistr($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи'; //???
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$query = "exec [lasmart_p_MasterDistributors_SetDeleteStatus] @UserID = $UserID, @ID = $ID"; //TODO
		//$Event = iconv("UTF-8", "cp1251", "'Статус изменен на Помечена на удаление'");

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'У позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function setDeleteStatusWrhs($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи'; //???
			return $arr;
		}

		$UserID = $_SESSION['idUser'];		
		$query = "exec [lasmart_p_MasterWarehouses_SetDeleteStatus] @UserID = $UserID, @ID = $ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'У позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function getEventHistory($connection, $_REQUEST) {
		$arr = array();
		$ElementID = parseParameter($_REQUEST['ElementID']);
		$Ref = parseParameterWithEncode($_REQUEST['Ref']);
		$query = "select convert(varchar,cast([DateUpdate] as date)) + ' ' + left(convert(varchar,cast([DateUpdate] as time)),5) as [Date]
				,[Author]
				,[Event]
			FROM [lasmart_t_sync_MasterRefLog] where [Ref] = $Ref and [ElementID] = $ElementID";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Author'] = iconv("cp1251", "UTF-8", $row['Author']);
			$row['Event'] = iconv("cp1251", "UTF-8", $row['Event']);
			$arr [] = $row;
		}
		return $arr;
	}

	function getManagers($connection, $_REQUEST) {
		$arr = array();
		
		$StoresID = parseParameter($_REQUEST['StoresID']);
		$isNet = parseParameter($_REQUEST['isNet']);
		if ($isNet == 0 || $isNet == 1)
			$Ref = 'TT';
		else 
			$Ref = 'Distr';

		$query = "select mn.[ID]
			,mn.[ManagersID]
			,mfi.FIO as [Managers]
			,mn.[DateHired]
			,mn.[DateFired]
		FROM [lasmart_t_stores_managers] mn
		left join [lasmart_t_dim_MasterStaff] mfi on mn.[ManagersID] = mfi.[ID]
		where [StoresID] = $StoresID and [Ref] = '$Ref'
		order by mn.[DateHired]";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Managers'] = iconv("cp1251", "UTF-8", $row['Managers']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createManagers($connection, $_REQUEST){
		$arr = array();
		$isNet = parseParameter($_REQUEST['isNet']);
		if ($isNet == 0 || $isNet == 1)
			$Ref = 'TT';
		else 
			$Ref = 'Distr';

		$StoresID = parseParameter($_REQUEST['StoresID']);
		$ManagersID = parseParameter($_REQUEST['ManagersID']);
		$DateHired = parseParameterWithEncode($_REQUEST['DateHired']);
		$DateFired = parseParameterWithEncode($_REQUEST['DateFired']);
		
		$query =
		"insert into [lasmart_t_stores_managers]
		([StoresID]
		,[Ref]
		,[ManagersID]
		,[DateHired]
		,[DateFired])
		VALUES($StoresID, '$Ref', $ManagersID, $DateHired, $DateFired)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}

		return $arr;
	}

	function updateManagers($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$ManagersID = parseParameter($_REQUEST['ManagersID']);
		$DateHired = parseParameterWithEncode($_REQUEST['DateHired']);
		$DateFired = parseParameterWithEncode($_REQUEST['DateFired']);

		$query =
		"update [lasmart_t_stores_managers]
		set 
		[ManagersID] = $ManagersID
		,[DateHired] = $DateHired
		,[DateFired] = $DateFired
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='ManagersID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function deleteManagers($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не выбран менеджер';
			return $arr;
		}

		$query = "delete [lasmart_t_stores_managers] WHERE [ID] = $ID";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function getSyncReport($connection, $_REQUEST) {
		$arr = array();
		
		$ref = parseParameter($_REQUEST['ref']);
		$columns = parseParameter($_REQUEST['columns']);
		$UserID = $_SESSION['idUser'];

		$where = "where 0 = 0 ";
		$where = parseFilters($_REQUEST);
		
		if (isset($_SESSION['SkuStatus'])) {
			if ($ref == 'Goods')
				$where .= ' and masterStatusID in ('.$_SESSION['SkuStatus'].')';
		}

		if (isset($_SESSION['TTStatus'])) {
			if ($ref == 'Stores')
				$where .= ' and masterStatusID in ('.$_SESSION['TTStatus'].')';
		}

		if (isset($_SESSION['StaffStatus'])) {
			if ($ref == 'Staff')
				$where .= ' and masterStatusID in ('.$_SESSION['StaffStatus'].')';
		}

		if (isset($_SESSION['WrhsStatus'])) {
			if ($ref == 'Warehouses')
				$where .= ' and masterStatusID in ('.$_SESSION['WrhsStatus'].')';
		}

		if (isset($_SESSION['SkuSlaveSource'])) {
			if ($ref == 'Goods')
				$where .= ' and slaveDataSourceID in ('.$_SESSION['SkuSlaveSource'].')';
		}

		if (isset($_SESSION['TTSlaveSource'])) {
			if ($ref == 'Stores')
				$where .= ' and slaveDataSourceID in ('.$_SESSION['TTSlaveSource'].')';
		}

		if (isset($_SESSION['StaffSlaveSource'])) {
			if ($ref == 'Staff')
				$where .= ' and slaveDataSourceID in ('.$_SESSION['StaffSlaveSource'].')';
		}

		if (isset($_SESSION['WrhsSlaveSource'])) {
			if ($ref == 'Warehouses')
				$where .= ' and slaveDataSourceID in ('.$_SESSION['WrhsSlaveSource'].')';
		}

		if (isset($_SESSION['Sectors'])) {
			if ($ref == 'Stores')
				$where .= ' and [masterAreaID] in ('.$_SESSION['Sectors'].')';
		}

		if (isset($_SESSION['SectorNames'])) {
			if ($ref == 'Stores')
				$where .= ' and ([slaveSector] in ('.$_SESSION['SectorNames'].') or (slaveDataSourceID <> 3))';
		}

		if (isset($_SESSION['Nets'])) {
			if ($ref == 'Stores')
				$where .= ' and [masterNetID] in ('.$_SESSION['Nets'].')';
		}

		if (isset($_SESSION['Categories'])) {
			if ($ref == 'Goods')
				$where .= ' and [masterCategoryID] in ('.$_SESSION['Categories'].')';
		}

		if (isset($_SESSION['Manufacturers'])) {
			if ($ref == 'Goods')
				$where .= ' and [masterManufacturerID] in ('.$_SESSION['Manufacturers'].')';
		}

		if (isset($_SESSION['TT_isNet'])) {
			if ($ref == 'Stores')
				$where .= ' and [masterisNet] in ('.$_SESSION['TT_isNet'].')';
		}

		if (isset($_SESSION['TT_isConsignee'])) {
			if ($ref == 'Stores')
				$where .= ' and isnull([masterisConsignee],0) in ('.$_SESSION['TT_isConsignee'].')';
		}


		$query = "select $columns from [lasmart_v_sync_SyncReport".$ref."] $where";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gsr.txt',ob_get_clean());
		
		$name = $ref.'_'.$UserID.'_'.time();
		$fp = fopen('../../files/'.$name.'.csv', 'w');
		$csv_output = '';

		switch ($ref) {
			case 'Goods':
				$fields = array(
					 'masterID' 			=> 'ИД'
					,'masterParent'			=> 'Агрегирующая позиция'
					,'masterCode' 			=> 'Код'
					,'masterName'  			=> 'Полное название'
					,'masterShortName' 		=> 'Короткое название'
					,'masterTempName' 		=> 'Черновое название'
					,'masterFrom' 			=> 'Продукция ОТ'
					,'masterManufacturer' 	=> 'Производитель'
					,'masterBrand' 			=> 'Бренд'
					,'masterSubBrand' 		=> 'Саббренд'
					,'masterCategory' 		=> 'Категория'
					,'masterSegment' 		=> 'Сегмент'
					,'masterSubSegment' 	=> 'Сабсегмент'
					,'masterTeaColor'		=> 'Цвет чая'
					,'masterExternalPack'	=> 'Тип упаковки внешней'
					,'masterSachetPack' 	=> 'Тип упаковки саше'
					,'masterInternalPack' 	=> 'Тип упаковки внутренней'
					,'masterBagQty' 		=> 'Количество пакетов'
					,'masterBagWeight' 		=> 'Вес пакета'
					,'masterWeight' 		=> 'Вес'
					,'masterWeightGroup' 	=> 'Вес (группа)'
					,'masterisWithFlavor' 	=> 'Аромат'
					,'masterFlavor' 		=> 'Ароматы'
					,'masterisWithAdding' 	=> 'Добавка'
					,'masterAdding' 		=> 'Добавки'
					,'masterisHorekaPack' 	=> 'Упаковка Хорека'
					,'masterPriceSegment' 	=> 'Ценовой сегмент'
					,'masterCountry' 		=> 'Страна происхождения'
					,'masterCoffeeSort' 	=> 'Сорт кофе'
					,'masterisCaffeine' 	=> 'Кофеин'
					,'masterGift' 			=> 'Акционная позиция'
					,'masterGiftType' 		=> 'Тип подарка'
					,'masterPhoto' 			=> 'Фотография'
					,'masterLength' 		=> 'Длина'
					,'masterWidth' 			=> 'Ширина'
					,'masterHeight' 		=> 'Высота'
					,'masterisWeightNet' 	=> 'Весовой продукт в сети'
					,'masterComment' 		=> 'Комментарий'
					,'masterCollection' 	=> 'Коллекция/Доп.имя'
					,'masterisPrivateMark' 	=> 'Частная марка сети'
					,'masterBarcode' 		=> 'Штрихкод'
					,'masterAuthor' 		=> 'Автор изменений'
					,'masterDateChange' 	=> 'Дата последнего изменения'
					,'masterStatus' 		=> 'Статус'
					,'masterDataSource' 	=> 'Источник данных'

					,'slaveID'	 			=> 'ИД'
					,'slaveCode' 			=> 'Код'
					,'slaveOrimiCode' 		=> 'Код Орими'
					,'slaveName' 			=> 'Название'
					,'slaveFrom' 			=> 'Продукция ОТ'
					,'slaveManufacturer' 	=> 'Производитель'
					,'slaveBrand' 			=> 'Бренд'
					,'slaveCategory' 		=> 'Категория'
					,'slaveSegment' 		=> 'Сегмент'
					,'slaveTeaColor' 		=> 'Цвет чая'
					,'slavePack' 			=> 'Тип упаковки'
					,'slaveBagQty' 			=> 'Количество пакетов'
					,'slaveBagWeight' 		=> 'Вес пакета'
					,'slaveWeight' 			=> 'Вес'
					,'slavePriceSegment' 	=> 'Ценовой сегмент'
					,'slaveCountry' 		=> 'Страна происхождения'
					,'slaveDataSource' 		=> 'Источник данных'
					,'slaveDateChange' 		=> 'Дата привязки'
					,'slaveSyncType' 		=> 'авто/ручн'
				);
			break;
			case 'Stores':
				$fields = array(
					 'masterID'					=> 'ИД'
					,'masterCode'				=> 'Код'
					,'masterName'				=> 'Название'
					,'masterTempAddress'		=> 'Черновой адрес'
					,'masterFullAddress'		=> 'Полный адрес'
					,'masterAddress'			=> 'Адрес'
					,'masterCountry'			=> 'Страна'
					,'masterPostCode'			=> 'Индекс'
					,'masterCounty'				=> 'Округ'
					,'masterRegion'				=> 'Регион'
					,'masterDistrict'			=> 'Район'
					,'masterCity'				=> 'Город'
					,'masterStreet'				=> 'Улица'
					,'masterBuilding'			=> 'Дом'
					,'masterDivision'			=> 'Дивизион'
					,'masterArea'				=> 'Участок'
					,'masterSector'				=> 'Сектор'
					,'masterChannel'			=> 'Канал'
					,'masterSubChannel'			=> 'Подканал'
					,'masterAdvertisingChannel'	=> 'Канал для рекламы'
					,'masterFormat'				=> 'Формат'
					,'masterisNetName'			=> 'Сеть'
					,'masterNet'				=> 'Название сети'
					,'masterNetType'			=> 'Тип сети'
					,'masterisNielsen'			=> 'КАИ Nielsen'
					,'masterRespMerch'			=> 'Отв. мерчандайзер'
					,'masterRespManager'		=> 'Отв. менеджер'
					,'masterContractorGroup'	=> 'Группа контрагентов'
					,'masterDateOpen'			=> 'Дата открытия ТТ'
					,'masterDateClose'			=> 'Дата закрытия ТТ'
					,'masterShopArea'			=> 'Площадь ТТ'
					,'masterCoordinates'		=> 'Координаты'
					,'masterGLN'				=> 'GLN'
					,'masterComment'			=> 'Комментарий'
					,'masterFIAS'				=> 'ФИАС'
					,'masterisInRBP'			=> 'Участвует в РБП'
					,'masterRespTR'				=> 'Отв. ТП'
					,'masterSelection'			=> 'Выборка'
					,'masterAuthor'				=> 'Автор изменений'
					,'masterDateChange'			=> 'Дата последнего изменения'
					,'masterStatus'				=> 'Статус'
					,'masterDataSource' 		=> 'Источник данных'

					,'slaveID'					=> 'ИД'
					,'slaveCode'				=> 'Код'
					,'slaveDistrCode'			=> 'Код Дистр.'
					,'slaveName'				=> 'Название'
					,'slaveTempAddress'			=> 'Черновой адрес'
					,'slaveCounty'				=> 'Округ'
					,'slaveRegion'				=> 'Регион'
					,'slaveCity'				=> 'Город'
					,'slaveDivision'			=> 'Дивизион'
					,'slaveSector'				=> 'Сектор'
					,'slaveSubChannel'			=> 'Подканал'
					,'slaveFormat'				=> 'Формат'
					,'slaveisNet'				=> 'Сеть'
					,'slaveNet'					=> 'Название сети'
					,'slaveContractorGroup'		=> 'Группа контрагентов'
					,'slaveDataSource'			=> 'Источник данных'
					,'slaveDateChange'			=> 'Дата привязки'
					,'slaveSyncType'			=> 'авто/ручн'
				);
			break;
			case 'Staff':
				$fields = array(
					 'masterID'					=> 'ИД'
					,'masterCode'				=> 'Код'
					,'masterSurName'			=> 'Фамилия'
					,'masterName'				=> 'Имя'
					,'masterLastName'			=> 'Отчество'
					,'masterFIO'				=> 'ФИО'
					,'masterBirthDate'			=> 'Дата рождения'
					,'masterType'				=> 'Тип'
					,'masterDivision'			=> 'Дивизион'
					,'masterDateHired'			=> 'Дата начала работы'
					,'masterDateFired'			=> 'Дата увольнения'
					,'masterEMail'				=> 'E-mail'
					,'masterPhone'				=> 'Телефон'
					,'masterComment'			=> 'Комментарий'
					,'masterMotivation'			=> 'Мотивация'
					,'masterFeatureName'		=> 'Признак'
					,'masterSector'				=> 'Сектор'
					,'masterParent'				=> 'Кому подчиняется'
					,'masterConsiderInTeam'		=> 'Учитывать в команде'
					,'masterStatus'				=> 'Статус'
					,'masterAuthor'				=> 'Автор изменений'
					,'masterDateChange'			=> 'Дата последнего изменения'
					,'masterDataSource' 		=> 'Источник данных'

					,'slaveID'					=> 'ИД'
					,'slaveCode'				=> 'Код'
					,'slaveFIO'					=> 'ФИО'
					,'slaveBirthDate'			=> 'Дата рождения'
					,'slaveType'				=> 'Тип'
					,'slaveDivision'			=> 'Принадлежность к дивизиону'
					,'slaveDateHired'			=> 'Дата начала работы'
					,'slaveDateFired'			=> 'Дата увольнения'
					,'slaveEMail'				=> 'E-mail'
					,'slavePhone'				=> 'Телефон'
					,'slaveMotivation'			=> 'Мотивация'
					,'slaveFeature'				=> 'Признак'
					,'slaveSector'				=> 'Сектор'
					,'slaveParent'				=> 'Кому подчиняется'
					,'slaveDataSource'			=> 'Источник данных'
					,'slaveDateChange'			=> 'Дата привязки'
					,'slaveSyncType'			=> 'авто/ручн'
				);
			break;
			case 'Warehouses':
				$fields = array(
					 'masterID'					=> 'ИД'
					,'masterCode'				=> 'Код'
					,'masterName'				=> 'Название'
					,'masterDistributor'		=> 'Дистрибьютор'
					,'masterDistrCode'			=> 'Код Дистр.'
					,'masterTempAddress'		=> 'Черновой адрес'
					,'masterFullAddress'		=> 'Полный адрес'
					,'masterAddress'			=> 'Адрес'
					,'masterCountry'			=> 'Страна'
					,'masterPostCode'			=> 'Индекс'
					,'masterCounty'				=> 'Округ'
					,'masterRegion'				=> 'Регион'
					,'masterDistrict'			=> 'Район'
					,'masterCity'				=> 'Город'
					,'masterStreet'				=> 'Улица'
					,'masterBuilding'			=> 'Дом'
					,'masterDivision'			=> 'Дивизион'
					,'masterArea'				=> 'Участок'
					,'masterSector'				=> 'Сектор'					
					,'masterFormat'				=> 'Формат'
					,'masterDateOpen'			=> 'Дата открытия ТТ'
					,'masterDateClose'			=> 'Дата закрытия ТТ'
					,'masterShopArea'			=> 'Площадь ТТ'					
					,'masterComment'			=> 'Комментарий'
					,'masterFIAS'				=> 'ФИАС'
					,'masterAuthor'				=> 'Автор изменений'
					,'masterDateChange'			=> 'Дата последнего изменения'
					,'masterStatus'				=> 'Статус'
					,'masterDataSource' 		=> 'Источник данных'

					,'slaveID'					=> 'ИД'
					,'slaveCode'				=> 'Код'
					,'slaveDistrCode'			=> 'Код Дистр.'
					,'slaveName'				=> 'Название'
					,'slaveTempAddress'			=> 'Черновой адрес'
					,'slaveCounty'				=> 'Округ'
					,'slaveRegion'				=> 'Регион'
					,'slaveCity'				=> 'Город'
					,'slaveDivision'			=> 'Дивизион'
					,'slaveSector'				=> 'Сектор'					
					,'slaveDataSource'			=> 'Источник данных'
					,'slaveDateChange'			=> 'Дата привязки'
					,'slaveSyncType'			=> 'авто/ручн'
				);
			break;
		}

		$columnsArr = split(',',$columns);

		for($i=0;$i<count($columnsArr);$i++) {
			$csv_output .= iconv("UTF-8", "cp1251", $fields[$columnsArr[$i]].';');
		}

		$csv_output = substr($csv_output,0,-1);
		$csv_output .= "\n";
		fwrite($fp, $csv_output);

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$csv_output = '';
			$count = count($row);
			for ($i=0;$i<$count-1;$i++) {
				$csv_output .= str_replace("\r","",str_replace("\n","",str_replace(";","",$row[$i]))).';';
			}
			$csv_output .= str_replace("\r","",str_replace("\n","",str_replace(";","",$row[$count-1])));
			$csv_output .= "\n";
			fwrite($fp, $csv_output);
		}
		fclose($fp);

		$zip = new ZipArchive(); 
		$zipRes = $zip->open('../../files/'.$name.'.zip', ZipArchive::CREATE);	
		$zip->addFile('../../files/'.$name.'.csv', $name.'.csv');
		$zip->close();
		unlink('../../files/'.$name.'.csv'); 

		$arr['name'] = $name;

		return $arr;
	}

	function getFile($connection, $_REQUEST) {
		$idUser = $_SESSION ['idUser'];

		$name = $_GET['file'];
		
		$file = "../../files/".$name.".zip";
		header ("Content-Type: application/zip"); 
		header ("Content-Length: ".filesize($file));  
		header ("Content-Disposition: attachment; filename=".$name.".zip");
		readfile($file);
		unlink($file); 

		exit;
	}

	function getGroupSyncGoods($connection, $_REQUEST) {
		$arr = array();
		
		$join = "on ";		
		$masterFields = explode(",",parseParameter($_REQUEST['master']));
		$slaveFields = explode(",",parseParameter($_REQUEST['slave']));
		$count = count($masterFields);
		
		for($i=0;$i<$count;$i++) {
			$join .= 'mg.'.$masterFields[$i].' = sg.'.$slaveFields[$i].' and ';
		}
		$join = substr($join,0,-4);

		$source = parseParameter($_REQUEST['source']);
		$where = 'where mg.StatusID = 2';
		if ($source != '0') {
			$where .= ' and sg.DataSourceID = '.$source;
		}
		
		$UserID = $_SESSION['idUser'];	
		
		$query = "SELECT 
		mg.[ID] as [masterID]
		,mg.[Parent] as [masterParent]
		,mg.[Code] as [masterCode]
		,mg.[Name] as [masterName]
		,mg.[ShortName] as [masterShortName]
		,mg.[TempName] as [masterTempName]
		,mg.[From] as [masterFrom]
		,mg.[Manufacturer] as [masterManufacturer]
		,mg.[Brand] as [masterBrand]
		,mg.[SubBrand] as [masterSubBrand]
		,mg.[Category] as [masterCategory]
		,mg.[Segment] as [masterSegment]
		,mg.[SubSegment] as [masterSubSegment]
		,mg.[TeaColor] as [masterTeaColor]
		,mg.[ExternalPack] as [masterExternalPack]
		,mg.[SachetPack] as [masterSachetPack]
		,mg.[InternalPack] as [masterInternalPack]
		,mg.[BagQty] as [masterBagQty]
		,mg.[BagWeight] as [masterBagWeight]
		,mg.[Weight] as [masterWeight]
		,mg.[WeightGroup] as [masterWeightGroup]
		,mg.[isWithFlavor] as [masterisWithFlavor]
		,mg.[isWithAdding] as [masterisWithAdding]
		,mg.[isHorekaPack] as [masterisHorekaPack]
		,mg.[PriceSegment] as [masterPriceSegment]
		,mg.[Country] as [masterCountry]
		,mg.[CoffeeSort] as [masterCoffeeSort]
		,mg.[isCaffeine] as [masterisCaffeine]
		,mg.[Gift] as [masterGift]
		,mg.[GiftType] as [masterGiftType]
		,mg.[Photo] as [masterPhoto]
		,mg.[Length] as [masterLength]
		,mg.[Width] as [masterWidth]
		,mg.[Height] as [masterHeight]
		,mg.[isWeightNet] as [masterisWeightNet]
		,mg.[Comment] as [masterComment]
		,mg.[Collection] as [masterCollection]
		,mg.[isPrivateMark] as [masterisPrivateMark]
		,mg.[Adding] as [masterAdding]
		,mg.[Flavor] as [masterFlavor]
		,mg.[Barcode] as [masterBarcode]
		,mg.[Status] as [masterStatus]
		,mg.[Author] as [masterAuthor]
		,mg.[DateChange] as [masterDateChange]
		,mg.[DataSource] as [masterDataSource]

		,0 as [toSync]

		,sg.[ID] as [slaveID]
		,sg.[Code] as [slaveCode]
		,sg.[OrimiCode] as [slaveOrimiCode]
		,sg.[Name] as [slaveName]
		,sg.[From] as [slaveFrom]
		,sg.[Manufacturer] as [slaveManufacturer]
		,sg.[Brand] as [slaveBrand]
		,sg.[Category] as [slaveCategory]
		,sg.[Segment] as [slaveSegment]
		,sg.[TeaColor] as [slaveTeaColor]
		,sg.[Pack] as [slavePack]
		,sg.[BagQty] as [slaveBagQty]
		,sg.[BagWeight] as [slaveBagWeight]
		,sg.[Weight] as [slaveWeight]
		,sg.[isWithFlavor] as [slaveisWithFlavor]
		,sg.[isWithAdding] as [slaveisWithAdding]
		,sg.[PriceSegment] as [slavePriceSegment]
		,sg.[Country] as [slaveCountry]
		,sg.[DataSource] as [slaveDataSource]
		,sg.[DateChange] as [slaveDateChange]

		FROM [lasmart_v_dim_MasterGood] mg
		join [lasmart_v_dim_SlaveGood] sg $join
		$where";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_ggsg.txt',ob_get_clean());
		$_SESSION['GoodSyncResultReportQuery'] = $query;
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$ret['items'] = $arr;

		return $ret;
	}

	function getGroupSyncStores($connection, $_REQUEST) {
		$arr = array();
		
		$join = "on ";		
		$masterFields = explode(",",parseParameter($_REQUEST['master']));
		$slaveFields = explode(",",parseParameter($_REQUEST['slave']));
		$count = count($masterFields);
		
		for($i=0;$i<$count;$i++) {
			$join .= 'ms.'.$masterFields[$i].' = ss.'.$slaveFields[$i].' and ';
			//$join .= "(ms.".$masterFields[$i]." like '%'+ss.".$slaveFields[$i]."+'%' or ss.".$slaveFields[$i]." like '%'+ms.".$masterFields[$i]."+'%') and ";
		}
		$join = substr($join,0,-4);

		$source = parseParameter($_REQUEST['source']);
		$where = 'where ms.StatusID = 2';
		if ($source != '0') {
			$where .= ' and ss.DataSourceID = '.$source;
		}
		
		$UserID = $_SESSION['idUser'];	
		
		$query = "SELECT top 25000
		 ms.[ID] as [masterID]
		,ms.[Code] as [masterCode]
		,ms.[Name] as [masterName]
		,ms.[TempAddress] as [masterTempAddress]
		,ms.[PostCode] as [masterPostCode]
		,ms.[Country] as [masterCountry]
		,ms.[County] as [masterCounty]
		,ms.[Region] as [masterRegion]
		,ms.[District] as [masterDistrict]
		,ms.[City] as [masterCity]
		,ms.[Street] as [masterStreet]
		,ms.[Building] as [masterBuilding]
		,ms.[FullAddress] as [masterFullAddress]
		,ms.[Address] as [masterAddress]
		,ms.[Sector] as [masterSector]
		,ms.[Area] as [masterArea]
		,ms.[Division] as [masterDivision]
		,ms.[SubChannel] as [masterSubChannel]
		,ms.[Channel] as [masterChannel]
		,ms.[Format] as [masterFormat]
		,ms.[Net] as [masterNet]
		,ms.[isNetName] as [masterisNetName]
		,ms.[NetType] as [masterNetType]
		,ms.[RespManager] as [masterRespManager]
		,ms.[isNielsen] as [masterisNielsen]
		,ms.[ContractorGroup] as [masterContractorGroup]		
		,ms.[DateOpen] as [masterDateOpen] 
		,ms.[DateClose] as [masterDateClose]
		,ms.[ShopArea] as [masterShopArea]
		,ms.[Coordinates] as [masterCoordinates]
		,ms.[GLN] as [masterGLN]
		,ms.[Comment] as [masterComment]
		,ms.[FIAS] as [masterFIAS] 
		,ms.[isInRBP] as [masterisInRBP]
		,ms.[RespMerch] as [masterRespMerch]
		,ms.[RespTR] as [masterRespTR] 
		,ms.[Selection] as [masterSelection]
		,ms.[Status] as [masterStatus]
		,ms.[DateChange] as [masterDateChange]
		,ms.[Author] as [masterAuthor]
		,ms.[DataSource] as [masterDataSource]

		,0 as [toSync]

		,ss.[ID] as [slaveID]
		,ss.[DistrCode] as [slaveDistrCode]
		,ss.[Code] as [slaveCode]
		,ss.[Name] as [slaveName]
		,ss.[TempAddress] as [slaveTempAddress]
		,ss.[County] as [slaveCounty]
		,ss.[Region] as [slaveRegion]
		,ss.[City] as [slaveCity]
		,ss.[Division] as [slaveDivision] 
		,ss.[Sector] as [slaveSector]
		,ss.[SubChannel] as [slaveSubChannel]
		,ss.[Format] as [slaveFormat]
		,ss.[Net] as [slaveNet]
		,ss.[isNet] as [slaveisNet]
		,ss.[ContractorGroup] as [slaveContractorGroup]
		,ss.[DataSource] as [slaveDataSource]
		,ss.[DateChange] as [slaveDateChange]

		FROM [lasmart_v_dim_MasterStores] ms
		join [lasmart_v_dim_SlaveStores] ss $join
		$where";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_ggss.txt',ob_get_clean());
		$_SESSION['StoreSyncResultReportQuery'] = $query;
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$ret['items'] = $arr;

		return $ret;
	}

	function getGroupSyncStaff($connection, $_REQUEST) {
		$arr = array();
		
		$join = "on ";		
		$masterFields = explode(",",parseParameter($_REQUEST['master']));
		$slaveFields = explode(",",parseParameter($_REQUEST['slave']));
		$count = count($masterFields);
		
		for($i=0;$i<$count;$i++) {
			$join .= 'ms.'.$masterFields[$i].' = ss.'.$slaveFields[$i].' and ';
		}
		$join = substr($join,0,-4);

		$source = parseParameter($_REQUEST['source']);
		$where = 'where ms.StatusID = 2';
		if ($source != '0') {
			$where .= ' and ss.DataSourceID = '.$source;
		}
		
		$UserID = $_SESSION['idUser'];	
		
		$query = "SELECT 
		 ms.[ID] as [masterID]
		,ms.[Parent] as [masterParent]
		,ms.[Code] as [masterCode]
		,ms.[SurName] as [masterSurName]
		,ms.[Name] as [masterName]
		,ms.[LastName] as [masterLastName]
		,ms.[FIO] as [masterFIO]
		,ms.[BirthDate] as [masterBirthDate]
		,ms.[Type] as [masterType]
		,ms.[Division] as [masterDivision]
		,ms.[DateHired] as [masterDateHired]
		,ms.[DateFired] as [masterDateFired]
		,ms.[EMail] as [masterEMail]
		,ms.[Phone] as [masterPhone]
		,ms.[Comment] as [masterComment]
		,ms.[Motivation] as [masterMotivation]
		,ms.[FeatureName] as [masterFeatureName]
		,ms.[ConsiderInTeam] as [masterConsiderInTeam]
		,ms.[Sector] as [masterSector]
		,ms.[Status] as [masterStatus]
		,ms.[DateChange] as [masterDateChange]
		,ms.[Author] as [masterAuthor]
		,ms.[DataSource] as [masterDataSource]

		,0 as [toSync]

		,ss.[ID] as [slaveID]
		,ss.[Parent] as [slaveParent]
		,ss.[Code] as [slaveCode]
		,ss.[FIO] as [slaveFIO]
		,ss.[BirthDate] as [slaveBirthDate]
		,ss.[Type] as [slaveType]
		,ss.[Division] as [slaveDivision]
		,ss.[DateHired] as [slaveDateHired]
		,ss.[DateFired] as [slaveDateFired]
		,ss.[EMail] as [slaveEMail]
		,ss.[Phone] as [slavePhone]
		,ss.[Comment] as [slaveComment]
		,ss.[Motivation] as [slaveMotivation]
		,ss.[Feature] as [slaveFeature]
		,ss.[Sector] as [slaveSector]
		,ss.[DataSource] as [slaveDataSource]
		,ss.[DateChange] as [slaveDateChange]

		FROM [lasmart_v_dim_MasterStaff] ms
		join [lasmart_v_dim_SlaveStaff] ss $join
		$where";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_ggss.txt',ob_get_clean());
		$_SESSION['StaffSyncResultReportQuery'] = $query;
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$ret['items'] = $arr;

		return $ret;
	}

	function getGroupSyncWarehouses($connection, $_REQUEST) {
		$arr = array();
		
		$join = "on ";		
		$masterFields = explode(",",parseParameter($_REQUEST['master']));
		$slaveFields = explode(",",parseParameter($_REQUEST['slave']));
		$count = count($masterFields);
		
		for($i=0;$i<$count;$i++) {
			$join .= 'ms.'.$masterFields[$i].' = ss.'.$slaveFields[$i].' and ';
			//$join .= "(ms.".$masterFields[$i]." like '%'+ss.".$slaveFields[$i]."+'%' or ss.".$slaveFields[$i]." like '%'+ms.".$masterFields[$i]."+'%') and ";
		}
		$join = substr($join,0,-4);

		$source = parseParameter($_REQUEST['source']);
		$where = 'where ms.StatusID = 2';
		if ($source != '0') {
			$where .= ' and ss.DataSourceID = '.$source;
		}
		
		$UserID = $_SESSION['idUser'];	
		
		$query = "SELECT top 25000
		 ms.[ID] as [masterID]
		,ms.[Code] as [masterCode]
		,ms.[Name] as [masterName]
		,ms.[Distributor] as [masterDistributor]
		,ms.[DistrCode] as [masterDistrCode]
		,ms.[TempAddress] as [masterTempAddress]
		,ms.[PostCode] as [masterPostCode]
		,ms.[Country] as [masterCountry]
		,ms.[County] as [masterCounty]
		,ms.[Region] as [masterRegion]
		,ms.[District] as [masterDistrict]
		,ms.[City] as [masterCity]
		,ms.[Street] as [masterStreet]
		,ms.[Building] as [masterBuilding]
		,ms.[FullAddress] as [masterFullAddress]
		,ms.[Address] as [masterAddress]
		,ms.[Sector] as [masterSector]
		,ms.[Area] as [masterArea]
		,ms.[Division] as [masterDivision]
		,ms.[Format] as [masterFormat]
		,ms.[DateOpen] as [masterDateOpen] 
		,ms.[DateClose] as [masterDateClose]
		,ms.[ShopArea] as [masterShopArea]
		,ms.[Comment] as [masterComment]
		,ms.[FIAS] as [masterFIAS] 
		,ms.[Status] as [masterStatus]
		,ms.[DateChange] as [masterDateChange]
		,ms.[Author] as [masterAuthor]
		,ms.[DataSource] as [masterDataSource]

		,0 as [toSync]

		,ss.[ID] as [slaveID]
		,ss.[DistrCode] as [slaveDistrCode]
		,ss.[Code] as [slaveCode]
		,ss.[Name] as [slaveName]
		,ss.[TempAddress] as [slaveTempAddress]
		,ss.[County] as [slaveCounty]
		,ss.[Region] as [slaveRegion]
		,ss.[City] as [slaveCity]
		,ss.[Division] as [slaveDivision] 
		,ss.[Sector] as [slaveSector]
		,ss.[DataSource] as [slaveDataSource]
		,ss.[DateChange] as [slaveDateChange]

		FROM [lasmart_v_dim_MasterWarehouses] ms
		join [lasmart_v_dim_SlaveWarehouses] ss $join
		$where";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_ggss.txt',ob_get_clean());
		$_SESSION['WarehouseSyncResultReportQuery'] = $query;
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$ret['items'] = $arr;

		return $ret;
	}

	function getSyncResultReport($connection, $_REQUEST) {
		$arr = array();
		
		$ref = parseParameter($_REQUEST['ref']);
		$columns = parseParameter($_REQUEST['columns']);
		$UserID = $_SESSION['idUser'];
		
		$name = $ref.'_'.$UserID.'_'.time();
		$fp = fopen('../../files/'.$name.'.csv', 'w');
		$csv_output = '';

		switch ($ref) {
			case 'Goods':
				$fields = array(
					 'masterID' 			=> 'ИД'
					,'masterParent'			=> 'Агрегирующая позиция'
					,'masterCode' 			=> 'Код'
					,'masterName'  			=> 'Полное название'
					,'masterShortName' 		=> 'Короткое название'
					,'masterTempName' 		=> 'Черновое название'
					,'masterFrom' 			=> 'Продукция ОТ'
					,'masterManufacturer' 	=> 'Производитель'
					,'masterBrand' 			=> 'Бренд'
					,'masterSubBrand' 		=> 'Саббренд'
					,'masterCategory' 		=> 'Категория'
					,'masterSegment' 		=> 'Сегмент'
					,'masterSubSegment' 	=> 'Сабсегмент'
					,'masterTeaColor'		=> 'Цвет чая'
					,'masterExternalPack'	=> 'Тип упаковки внешней'
					,'masterSachetPack' 	=> 'Тип упаковки саше'
					,'masterInternalPack' 	=> 'Тип упаковки внутренней'
					,'masterBagQty' 		=> 'Количество пакетов'
					,'masterBagWeight' 		=> 'Вес пакета'
					,'masterWeight' 		=> 'Вес'
					,'masterWeightGroup' 	=> 'Вес (группа)'
					,'masterisWithFlavor' 	=> 'Аромат'
					,'masterFlavor' 		=> 'Ароматы'
					,'masterisWithAdding' 	=> 'Добавка'
					,'masterAdding' 		=> 'Добавки'
					,'masterisHorekaPack' 	=> 'Упаковка Хорека'
					,'masterPriceSegment' 	=> 'Ценовой сегмент'
					,'masterCountry' 		=> 'Страна происхождения'
					,'masterCoffeeSort' 	=> 'Сорт кофе'
					,'masterisCaffeine' 	=> 'Кофеин'
					,'masterGift' 			=> 'Акционная позиция'
					,'masterGiftType' 		=> 'Тип подарка'
					,'masterPhoto' 			=> 'Фотография'
					,'masterLength' 		=> 'Длина'
					,'masterWidth' 			=> 'Ширина'
					,'masterHeight' 		=> 'Высота'
					,'masterisWeightNet' 	=> 'Весовой продукт в сети'
					,'masterComment' 		=> 'Комментарий'
					,'masterCollection' 	=> 'Коллекция/Доп.имя'
					,'masterisPrivateMark' 	=> 'Частная марка сети'
					,'masterBarcode' 		=> 'Штрихкод'
					,'masterAuthor' 		=> 'Автор изменений'
					,'masterDateChange' 	=> 'Дата последнего изменения'
					,'masterStatus' 		=> 'Статус'
					,'masterDataSource' 	=> 'Источник данных'

					,'toSync' 				=> 'Привязать'

					,'slaveID'	 			=> 'ИД'
					,'slaveCode' 			=> 'Код'
					,'slaveOrimiCode' 		=> 'Код Орими'
					,'slaveName' 			=> 'Название'
					,'slaveFrom' 			=> 'Продукция ОТ'
					,'slaveManufacturer' 	=> 'Производитель'
					,'slaveBrand' 			=> 'Бренд'
					,'slaveCategory' 		=> 'Категория'
					,'slaveSegment' 		=> 'Сегмент'
					,'slaveTeaColor' 		=> 'Цвет чая'
					,'slavePack' 			=> 'Тип упаковки'
					,'slaveBagQty' 			=> 'Количество пакетов'
					,'slaveBagWeight' 		=> 'Вес пакета'
					,'slaveWeight' 			=> 'Вес'
					,'slavePriceSegment' 	=> 'Ценовой сегмент'
					,'slaveCountry' 		=> 'Страна происхождения'
					,'slaveDataSource' 		=> 'Источник данных'
					,'slaveDateChange' 		=> 'Дата импорта'
					,'slaveSyncType' 		=> 'авто/ручн'
				);
				$query = 'select '.iconv("UTF-8", "cp1251",$columns).' from ('.iconv("UTF-8", "cp1251",$_SESSION['GoodSyncResultReportQuery']).') a';
			break;
			case 'Stores':
				$fields = array(
					 'masterID'					=> 'ИД'
					,'masterCode'				=> 'Код'
					,'masterName'				=> 'Название'
					,'masterTempAddress'		=> 'Черновой адрес'
					,'masterFullAddress'		=> 'Полный адрес'
					,'masterAddress'			=> 'Адрес'
					,'masterCountry'			=> 'Страна'
					,'masterPostCode'			=> 'Индекс'
					,'masterCounty'				=> 'Округ'
					,'masterRegion'				=> 'Регион'
					,'masterDistrict'			=> 'Район'
					,'masterCity'				=> 'Город'
					,'masterStreet'				=> 'Улица'
					,'masterBuilding'			=> 'Дом'
					,'masterDivision'			=> 'Дивизион'
					,'masterArea'				=> 'Участок'
					,'masterSector'				=> 'Сектор'
					,'masterChannel'			=> 'Канал'
					,'masterSubChannel'			=> 'Подканал'
					,'masterFormat'				=> 'Формат'
					,'masterisNetName'			=> 'Сеть'
					,'masterNet'				=> 'Название сети'
					,'masterNetType'			=> 'Тип сети'
					,'masterisNielsen'			=> 'КАИ Nielsen'
					,'masterRespMerch'			=> 'Отв. мерчандайзер'
					,'masterRespManager'		=> 'Отв. менеджер'
					,'masterContractorGroup'	=> 'Группа контрагентов'
					,'masterisSubDistributor'	=> 'Субдистрибьютор'
					,'masterisAccounting'		=> 'Учитывать в расчетах'
					,'masterNonAccReason'		=> 'Причина неучета'
					,'masterDateOpen'			=> 'Дата открытия ТТ'
					,'masterDateClose'			=> 'Дата закрытия ТТ'
					,'masterShopArea'			=> 'Площадь ТТ'
					,'masterCoordinates'		=> 'Координаты'
					,'masterGLN'				=> 'GLN'
					,'masterComment'			=> 'Комментарий'
					,'masterFIAS'				=> 'ФИАС'
					,'masterisInRBP'			=> 'Участвует в РБП'
					,'masterRespTR'				=> 'Отв. ТП'
					,'masterSelection'			=> 'Выборка'
					,'masterAuthor'				=> 'Автор изменений'
					,'masterDateChange'			=> 'Дата последнего изменения'
					,'masterStatus'				=> 'Статус'
					,'masterDataSource' 		=> 'Источник данных'

					,'toSync' 					=> 'Привязать'

					,'slaveID'					=> 'ИД'
					,'slaveCode'				=> 'Код'
					,'slaveDistrCode'			=> 'Код Дистр.'
					,'slaveName'				=> 'Название'
					,'slaveTempAddress'			=> 'Черновой адрес'
					,'slaveCounty'				=> 'Округ'
					,'slaveRegion'				=> 'Регион'
					,'slaveCity'				=> 'Город'
					,'slaveDivision'			=> 'Дивизион'
					,'slaveSector'				=> 'Сектор'
					,'slaveSubChannel'			=> 'Подканал'
					,'slaveFormat'				=> 'Формат'
					,'slaveisNet'				=> 'Сеть'
					,'slaveNet'					=> 'Название сети'
					,'slaveContractorGroup'		=> 'Группа контрагентов'
					,'slaveDataSource'			=> 'Источник данных'
					,'slaveDateChange'			=> 'Дата импорта'
					,'slaveSyncType'			=> 'авто/ручн'
				);
				$query = 'select '.iconv("UTF-8", "cp1251",$columns).' from ('.iconv("UTF-8", "cp1251",$_SESSION['StoreSyncResultReportQuery']).') a';
			break;
			case 'Staff':
				$fields = array(
					 'masterID'					=> 'ИД'
					,'masterCode'				=> 'Код'
					,'masterSurName'			=> 'Фамилия'
					,'masterName'				=> 'Имя'
					,'masterLastName'			=> 'Отчество'
					,'masterFIO'				=> 'ФИО'
					,'masterBirthDate'			=> 'Дата рождения'
					,'masterType'				=> 'Тип'
					,'masterDivision'			=> 'Дивизион'
					,'masterDateHired'			=> 'Дата начала работы'
					,'masterDateFired'			=> 'Дата увольнения'
					,'masterEMail'				=> 'E-mail'
					,'masterPhone'				=> 'Телефон'
					,'masterComment'			=> 'Комментарий'
					,'masterMotivation'			=> 'Мотивация'
					,'masterFeatureName'		=> 'Признак'
					,'masterSector'				=> 'Сектор'
					,'masterParent'				=> 'Кому подчиняется'
					,'masterConsiderInTeam'		=> 'Учитывать в команде'
					,'masterStatus'				=> 'Статус'
					,'masterAuthor'				=> 'Автор изменений'
					,'masterDateChange'			=> 'Дата последнего изменения'
					,'masterDataSource' 		=> 'Источник данных'

					,'toSync' 					=> 'Привязать'

					,'slaveID'					=> 'ИД'
					,'slaveCode'				=> 'Код'
					,'slaveFIO'					=> 'ФИО'
					,'slaveBirthDate'			=> 'Дата рождения'
					,'slaveType'				=> 'Тип'
					,'slaveDivision'			=> 'Принадлежность к дивизиону'
					,'slaveDateHired'			=> 'Дата начала работы'
					,'slaveDateFired'			=> 'Дата увольнения'
					,'slaveEMail'				=> 'E-mail'
					,'slavePhone'				=> 'Телефон'
					,'slaveMotivation'			=> 'Мотивация'
					,'slaveFeature'				=> 'Признак'
					,'slaveSector'				=> 'Сектор'
					,'slaveParent'				=> 'Кому подчиняется'
					,'slaveDataSource'			=> 'Источник данных'
					,'slaveDateChange'			=> 'Дата импорта'
					,'slaveSyncType'			=> 'авто/ручн'
				);
				$query = 'select '.iconv("UTF-8", "cp1251",$columns).' from ('.iconv("UTF-8", "cp1251",$_SESSION['StaffSyncResultReportQuery']).') a';
			break;

			case 'Warehouses':
				$fields = array(
					 'masterID'					=> 'ИД'
					,'masterCode'				=> 'Код'
					,'masterName'				=> 'Название'
					,'masterDistributor'		=> 'Дистрибьютор'
					,'masterDistrCode'			=> 'Код. Дистр'
					,'masterTempAddress'		=> 'Черновой адрес'
					,'masterFullAddress'		=> 'Полный адрес'
					,'masterAddress'			=> 'Адрес'
					,'masterCountry'			=> 'Страна'
					,'masterPostCode'			=> 'Индекс'
					,'masterCounty'				=> 'Округ'
					,'masterRegion'				=> 'Регион'
					,'masterDistrict'			=> 'Район'
					,'masterCity'				=> 'Город'
					,'masterStreet'				=> 'Улица'
					,'masterBuilding'			=> 'Дом'
					,'masterDivision'			=> 'Дивизион'
					,'masterArea'				=> 'Участок'
					,'masterSector'				=> 'Сектор'
					,'masterFormat'				=> 'Формат'
					,'masterDateOpen'			=> 'Дата открытия ТТ'
					,'masterDateClose'			=> 'Дата закрытия ТТ'
					,'masterShopArea'			=> 'Площадь ТТ'
					,'masterComment'			=> 'Комментарий'
					,'masterFIAS'				=> 'ФИАС'
					,'masterAuthor'				=> 'Автор изменений'
					,'masterDateChange'			=> 'Дата последнего изменения'
					,'masterStatus'				=> 'Статус'
					,'masterDataSource' 		=> 'Источник данных'

					,'toSync' 					=> 'Привязать'

					,'slaveID'					=> 'ИД'
					,'slaveCode'				=> 'Код'
					,'slaveDistrCode'			=> 'Код Дистр.'
					,'slaveName'				=> 'Название'
					,'slaveTempAddress'			=> 'Черновой адрес'
					,'slaveCounty'				=> 'Округ'
					,'slaveRegion'				=> 'Регион'
					,'slaveCity'				=> 'Город'
					,'slaveDivision'			=> 'Дивизион'
					,'slaveSector'				=> 'Сектор'
					,'slaveDataSource'			=> 'Источник данных'
					,'slaveDateChange'			=> 'Дата импорта'
					,'slaveSyncType'			=> 'авто/ручн'
				);
				$query = 'select '.iconv("UTF-8", "cp1251",$columns).' from ('.iconv("UTF-8", "cp1251",$_SESSION['WarehouseSyncResultReportQuery']).') a';
			break;
		}		

		$columnsArr = split(',',$columns);

		for($i=0;$i<count($columnsArr);$i++) {
			$csv_output .= iconv("UTF-8", "cp1251", $fields[$columnsArr[$i]].';');
		}

		$csv_output = substr($csv_output,0,-1);
		$csv_output .= "\n";
		fwrite($fp, $csv_output);

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;echo "\n\n".$csv_output;file_put_contents('out_gsrr.txt',ob_get_clean());

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$csv_output = '';
			$count = count($row);
			for ($i=0;$i<$count-1;$i++) {
				$csv_output .= $row[$i].';';
			}
			$csv_output .= $row[$count-1];
			$csv_output .= "\n";
			fwrite($fp, $csv_output);
		}
		fclose($fp);

		$zip = new ZipArchive(); 
		$zipRes = $zip->open('../../files/'.$name.'.zip', ZipArchive::CREATE);	
		$zip->addFile('../../files/'.$name.'.csv', $name.'.csv');
		$zip->close();
		unlink('../../files/'.$name.'.csv'); 

		$arr['name'] = $name;

		return $arr;
	}

	function getAdvertisingBossHeaders($connection, $_REQUEST) {
		$arr = array();
		$ret = array();

		$where = parseFilters($_REQUEST, 'advertisingBossHeaders');
		$order = parseSorters($_REQUEST);
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));
		
		$query = "select top $top * FROM [lasmart_v_fact_AdvertisingBossHeaders] $where $order";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$ret['items'] = $arr;

		$count = 0;
		$query = "select count(*) FROM [lasmart_v_fact_AdvertisingBossHeaders] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		
		$ret['total'] = $count;

		return $ret;
	}

	function updateAdvertisingBossHeaders($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$StatusID = 		updateParseParameter($_REQUEST['StatusID']);

		$query =
		"exec [lasmart_p_AdvertisingBossHeaders_Update]
		@UserID = $UserID
		,@ID = $ID
		,@StatusID = $StatusID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection)) {
            $message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0 || strpos($message,'Ошибка!') === 0) {
                $arr['success']	= FALSE;
                $arr['msg']	= $message;
            } else
                $arr['success']	= TRUE;
        } else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function getAdvertisingBossRows($connection, $_REQUEST) {
		$arr = array();
		$ret = array();

		$where = parseFilters($_REQUEST, 'advertisingBossRows');
		$order = parseSorters($_REQUEST);
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));

		if (isset($_REQUEST['OrderID'])) {
			$OrderID = parseParameter($_REQUEST['OrderID']);
			$where = $where.' and [OrderID] in ('.$OrderID.')';
		}
		
		$query = "select top $top * FROM [lasmart_v_fact_AdvertisingBossRows] $where $order";
		ob_start();print_r($_REQUEST);echo "\n\n".$query;echo "\n\n".$where;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$ret['items'] = $arr;

		$count = 0;
		$query = "select count(*) FROM [lasmart_v_fact_AdvertisingBossRows] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		
		$ret['total'] = $count;

		return $ret;
	}

	function getAdvertisingBossCurrentOrders($connection, $_REQUEST) {
		$arr = array();
		
		$GoodID = 		parseParameter($_REQUEST['GoodID']);
		$DateStart = 	parseParameterWithEncode($_REQUEST['DateStart']);

		$query = "select top 1 [OrderID],[FGD],[MinPrice],[BPLPrice],[MaxDiscountDepth]
		FROM [dbo].[orimi_f_get_DirectorOrder_Values](".$GoodID.", convert(date,".$DateStart."))";
		
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$arr [] = $row;
		}
		return $arr;
	}

	function getNetStoreAmountByArea($connection, $_REQUEST) {
		$arr = array();
		
		$NetID = 	parseParameter($_REQUEST['NetID']);
		$AreaID = 	parseParameter($_REQUEST['AreaID']);

		$query = "SELECT count(*) as [StoreAmount]
		FROM [dbo].[lasmart_t_dim_MasterStores] s
			left join lasmart_t_dim_City cit with (nolock, index = [NonClusteredIndex-20140829-152352]) on s.CityID = cit.ID
			left join lasmart_t_dim_Sector sec with (nolock) on cit.SectorID = sec.ID
		where [NetID] = ".$NetID." and [AreaID] = ".$AreaID." and StatusID = 2 and (DateClose > GETDATE() or DateClose is null)";
		
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$arr [] = $row;
		}
		return $arr;
	}

	function getAdvertisingEventsHeaders($connection, $_REQUEST) {
		$arr = array();
		$ret = array();

		$where = parseFilters($_REQUEST, 'advertisingEventsHeaders');

		if (isset($_SESSION['Sectors'])) {
			$where .= ' and (AreaID in ('.$_SESSION['Sectors'].')
					or (AreaID is null and 
						ID in (SELECT aeh.ID from [lasmart_v_fact_AdvertisingEventsHeaders] aeh left join lasmart_v_dim_Distributors d on aeh.ContractorGroupID = d.ContractorGroupID where d.AreaID in ('.$_SESSION['Sectors'].'))
					)
				)';
		}

		$order = parseSorters($_REQUEST);
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));
		
		$query = "select top $top * FROM [lasmart_v_fact_AdvertisingEventsHeaders] $where $order";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$ret['items'] = $arr;

		$count = 0;
		$query = "select count(*) FROM [lasmart_v_fact_AdvertisingEventsHeaders] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		
		$ret['total'] = $count;

		return $ret;
	}

	function createAdvertisingEventsHeaders($connection, $_REQUEST) {
		$arr = array();

		$UserID = $_SESSION['idUser'];
		$DocNum = 				parseParameterWithEncode($_REQUEST['DocNum']);
		$Title = 				parseParameterWithEncode($_REQUEST['Title']);
		$DateStart = 			parseParameterWithEncode($_REQUEST['DateStart']);
		$DateEnd = 				parseParameterWithEncode($_REQUEST['DateEnd']);
		$ContractorGroupID = 	parseParameter($_REQUEST['ContractorGroupID']);
		$AreaID = 				parseParameter($_REQUEST['AreaID']);
		$NetID = 				parseParameter($_REQUEST['NetID']);
		$StoreID = 				parseParameter($_REQUEST['StoreID']);
		$StoreAmount = 			parseParameter($_REQUEST['StoreAmount']);
		$StatusID = 			parseParameter($_REQUEST['StatusID']);
		$DateStartShipping = 	parseParameterWithEncode($_REQUEST['DateStartShipping']);
		$DateEndShipping = 		parseParameterWithEncode($_REQUEST['DateEndShipping']);
		$Comment = 				parseParameterWithEncode($_REQUEST['Comment']);
		$isInTradent = 			parseParameter($_REQUEST['isInTradent']);
		$TSZCode = 				parseParameterWithEncode($_REQUEST['TSZCode']);
		$PaymentTypeID = 		parseParameter($_REQUEST['PaymentTypeID']);
		

		$query = 
		"set ansi_nulls on
        set ansi_warnings on
		
		exec [lasmart_p_AdvertisingEventsHeaders_Create]
		@UserID = $UserID
		,@DocNum = $DocNum
		,@Title = $Title
		,@DateStart = $DateStart
		,@DateEnd = $DateEnd
		,@ContractorGroupID = $ContractorGroupID
		,@AreaID = $AreaID
		,@NetID = $NetID
		,@StoreID = $StoreID
		,@StoreAmount = $StoreAmount
		,@RequestStatusID = $StatusID
		,@DateStartShipping = $DateStartShipping
		,@DateEndShipping = $DateEndShipping
		,@Comment = $Comment
		,@isInTradent = $isInTradent
		,@TSZCode = $TSZCode
		,@PaymentTypeID = $PaymentTypeID
		";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_cg.txt',ob_get_clean());
		if (mssql_query($query,$connection)) {
			$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function updateAdvertisingEventsHeaders($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$DocNum = 				parseParameterWithEncode($_REQUEST['DocNum']);
		$Title = 				parseParameterWithEncode($_REQUEST['Title']);
		$DateStart = 			parseParameterWithEncode($_REQUEST['DateStart']);
		$DateEnd = 				parseParameterWithEncode($_REQUEST['DateEnd']);
		$ContractorGroupID = 	parseParameter($_REQUEST['ContractorGroupID']);
		$AreaID = 				parseParameter($_REQUEST['AreaID']);
		$NetID = 				parseParameter($_REQUEST['NetID']);
		$StoreID = 				parseParameter($_REQUEST['StoreID']);
		$StoreAmount = 			parseParameter($_REQUEST['StoreAmount']);
		$StatusID = 			parseParameter($_REQUEST['StatusID']);
		$DateStartShipping = 	parseParameterWithEncode($_REQUEST['DateStartShipping']);
		$DateEndShipping = 		parseParameterWithEncode($_REQUEST['DateEndShipping']);
		$Comment = 				parseParameterWithEncode($_REQUEST['Comment']);
		$isInTradent = 			parseParameter($_REQUEST['isInTradent']);
		$TSZCode = 				parseParameterWithEncode($_REQUEST['TSZCode']);
		$PaymentTypeID = 		parseParameter($_REQUEST['PaymentTypeID']);

		$query =
		"set ansi_nulls on
        set ansi_warnings on
		
		exec [lasmart_p_AdvertisingEventsHeaders_Update]
		@UserID = $UserID
		,@ID = $ID
		,@DocNum = $DocNum
		,@Title = $Title
		,@DateStart = $DateStart
		,@DateEnd = $DateEnd
		,@ContractorGroupID = $ContractorGroupID
		,@AreaID = $AreaID
		,@NetID = $NetID
		,@StoreID = $StoreID
		,@StoreAmount = $StoreAmount
		,@RequestStatusID = $StatusID
		,@DateStartShipping = $DateStartShipping
		,@DateEndShipping = $DateEndShipping
		,@Comment = $Comment
		,@isInTradent = $isInTradent
		,@TSZCode = $TSZCode
		,@PaymentTypeID = $PaymentTypeID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection)) {
            $message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0 || strpos($message,'Ошибка!') === 0) {
                $arr['success']	= FALSE;
                $arr['msg']	= $message;
            } else
                $arr['success']	= TRUE;
        } else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function deleteAdvertisingEventsHeaders($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не заданы ID элементов';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];

		$query =
		"exec [lasmart_p_AdvertisingEventsHeaders_Delete]
		@UserID = $UserID
		,@ID = '$ID'";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function updateAdvertisingEventsHeadersStatus($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не заданы ID элементов';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		$Status = 			parseParameterWithEncode($_REQUEST['Status']);
		$Comment = 			parseParameterWithEncode($_REQUEST['Comment']);

		$query =
		"exec [lasmart_p_AdvertisingEventsHeaders_UpdateStatus]
		@UserID = $UserID
		,@Status = $Status
		,@Comment = $Comment
		,@ID = '$ID'";

		if (mssql_query($query,$connection)) {
            $message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0 || strpos($message,'Ошибка!') === 0) {
                $arr['success']	= FALSE;
                $arr['msg']	= $message;
            } else
                $arr['success']	= TRUE;
        } else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function duplicateAdvertisingEvents($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];

		$query =
		"set ansi_nulls on
        set ansi_warnings on
		
		exec [lasmart_p_AdvertisingEventsHeaders_Duplicate]
		@UserID = $UserID
		,@ID = $ID";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection)) {
            $message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            ob_start();print_r($_REQUEST);echo "\n\n".$query;echo "\n\n".$message;file_put_contents('out_gu.txt',ob_get_clean());
            if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0 || strpos($message,'Ошибка!') === 0) {
                $arr['success']	= FALSE;
                $arr['msg']	= $message;
            } else
                $arr['success']	= TRUE;
        } else {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            ob_start();print_r($_REQUEST);echo "\n\n".$query;echo "\n\n".$message;file_put_contents('out_duplicate_error.txt',ob_get_clean());

			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function recalculateAdvertisingEvents($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];

		$query = "
			set ansi_nulls on
	        set ansi_warnings on
			
			exec [lasmart_p_AdvertisingEventsHeaders_Recalculate]
				@UserID = $UserID
				,@ID = $ID

			select top 1 * from [lasmart_v_fact_AdvertisingEventsHeaders] where ID = $ID
		";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		if ($result) {
            $message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            // ob_start();print_r($_REQUEST);echo "\n\n".$query;echo "\n\n".$message;file_put_contents('out_gu.txt',ob_get_clean());
            if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0 || strpos($message,'Ошибка!') === 0) {
                $arr['success']	= FALSE;
                $arr['msg']	= $message;
            } else
                $arr['success']	= TRUE;

                while ($row = mssql_fetch_assoc($result)) {
					foreach ($row as &$value) {
						$value = iconv("cp1251", "UTF-8", $value);
					}
					$arr['data'] = $row;
				}
        } else {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            // ob_start();print_r($_REQUEST);echo "\n\n".$query;echo "\n\n".$message;file_put_contents('out_recalculate_error.txt',ob_get_clean());

			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}

		return $arr;
	}

	function loadToTSZAdvertisingEvents($connection, $_REQUEST) {
		$where = parseFilters($_REQUEST, 'advertisingEventsHeaders');

		if (isset($_SESSION['Sectors'])) {
			$where .= ' and (AreaID in ('.$_SESSION['Sectors'].')
					or (AreaID is null and 
						ID in (SELECT aeh.ID from [lasmart_v_fact_AdvertisingEventsHeaders] aeh left join lasmart_v_dim_Distributors d on aeh.ContractorGroupID = d.ContractorGroupID where d.AreaID in ('.$_SESSION['Sectors'].'))
					)
				)';
		}

		$UserID = $_SESSION['idUser'];

		$query = "
			set ansi_nulls on
	        set ansi_warnings on

	        declare @IDs varchar(max)
	        declare @err varchar(max)

			begin try 
		        select @IDs = coalesce(@IDs + ',', '') + convert(varchar, ID)
		        from [lasmart_v_fact_AdvertisingEventsHeaders] $where
		        order by ID
				
				exec [lasmart_p_AdvertisingEventsHeaders_LoadToTSZ]
					@UserID = $UserID
					, @ID = @IDs

				SELECT 1 as 'status', '' as 'msg'			
			end try
			begin catch				
				set @err = error_message()
				SELECT 0 as 'status', @err as 'msg'	
			end catch					
		";

		// ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		$res = mssql_query($query,$connection);
		$res = mssql_fetch_assoc($res);
		//echo print_r($res['status'], true);
		if ($res['status']) {
            $arr['success']	= TRUE;
        } else {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            ob_start();print_r($_REQUEST);echo "\n\n".$query;echo "\n\n".$message;file_put_contents('out_error.txt',ob_get_clean());

			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function getAdvertisingEventsRows($connection, $_REQUEST) {
		$arr = array();
		$ret = array();

		$where = parseFilters($_REQUEST, 'advertisingEventsRows');
		$order = parseSorters($_REQUEST);
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));

		if (isset($_REQUEST['RequestID'])) {
			$RequestID = parseParameter($_REQUEST['RequestID']);
			$where = $where.' and [RequestID] in ('.$RequestID.')';
		} else {
			$where = $where.' and [RequestID] in (SELECT ID from [lasmart_v_fact_AdvertisingEventsHeaders] where 0 = 0';

			if (isset($_SESSION['Sectors'])) {
				$where .= ' and (AreaID in ('.$_SESSION['Sectors'].')
						or (AreaID is null and 
							ID in (SELECT aeh.ID from [lasmart_v_fact_AdvertisingEventsHeaders] aeh left join lasmart_v_dim_Distributors d on aeh.ContractorGroupID = d.ContractorGroupID where d.AreaID in ('.$_SESSION['Sectors'].'))
						)
					)';
			}

			$where = $where.')';
		}
		
		$query = "select top $top * FROM [lasmart_v_fact_AdvertisingEventsRows] $where $order";
		ob_start();print_r($_REQUEST);echo "\n\n".$query;echo "\n\n".$where;file_put_contents('out_save.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$ret['items'] = $arr;

		$count = 0;
		$query = "select count(*) FROM [lasmart_v_fact_AdvertisingEventsRows] $where";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$count = $row[0];
		}
		
		$ret['total'] = $count;

		return $ret;
	}

	function createAdvertisingEventsRows($connection, $_REQUEST) {
		$arr = array();

		$UserID = $_SESSION['idUser'];
		
		$RequestID = 	parseParameter($_REQUEST['RequestID']);
		$GoodID = 		parseParameter($_REQUEST['GoodID']);
		$Comment = 		parseParameterWithEncode($_REQUEST['Comment']);

		$OTDiscount = 						parseParameter($_REQUEST['OTDiscount']);
		$DistrDiscount = 					parseParameter($_REQUEST['DistrDiscount']);
		$NetDiscount = 						parseParameter($_REQUEST['NetDiscount']);
		$DistrIncomePrice = 				parseParameter($_REQUEST['DistrIncomePrice']);
		$NetIncomePriceBefore = 			parseParameter($_REQUEST['NetIncomePriceBefore']);
		$SellingPriceBefore = 				parseParameter($_REQUEST['SellingPriceBefore']);
		$EstimatedSalesAmountQty = 			parseParameter($_REQUEST['EstimatedSalesAmountQty']);
		$CataloguePubCost = 				parseParameter($_REQUEST['CataloguePubCost']);

		$query = "
		set ansi_nulls on
        set ansi_warnings on
		exec [lasmart_p_AdvertisingEventsRows_Create]
		@UserID = $UserID
		,@RequestID = $RequestID
		,@GoodID = $GoodID
		,@Comment = $Comment

		,@OTDiscount = $OTDiscount
		,@DistrDiscount = $DistrDiscount
		,@NetDiscount = $NetDiscount
		,@DistrIncomePrice = $DistrIncomePrice
		,@NetIncomePriceBefore = $NetIncomePriceBefore
		,@SellingPriceBefore = $SellingPriceBefore
		,@EstimatedSalesAmountQty = $EstimatedSalesAmountQty			
		,@CataloguePubCost = $CataloguePubCost";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_cg.txt',ob_get_clean());
		if (mssql_query($query,$connection)) {
			$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function updateAdvertisingEventsRows($connection, $_REQUEST) {
		$arr = array();

		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];
		
		$RequestID = 	parseParameter($_REQUEST['RequestID']);
		$GoodID = 		parseParameter($_REQUEST['GoodID']);
		$Comment = 		parseParameterWithEncode($_REQUEST['Comment']);

		$OTDiscount = 						parseParameter($_REQUEST['OTDiscount']);
		$DistrDiscount = 					parseParameter($_REQUEST['DistrDiscount']);
		$NetDiscount = 						parseParameter($_REQUEST['NetDiscount']);
		$DistrIncomePrice = 				parseParameter($_REQUEST['DistrIncomePrice']);
		$NetIncomePriceBefore = 			parseParameter($_REQUEST['NetIncomePriceBefore']);
		$SellingPriceBefore = 				parseParameter($_REQUEST['SellingPriceBefore']);
		$EstimatedSalesAmountQty = 			parseParameter($_REQUEST['EstimatedSalesAmountQty']);			
		$CataloguePubCost = 				parseParameter($_REQUEST['CataloguePubCost']);
		$RealSellingPrice = 				parseParameter($_REQUEST['RealSellingPrice']);
		$RealSalesAmountQty = 				parseParameter($_REQUEST['RealSalesAmountQty']);
		$RealSalesAmountVal = 				parseParameter($_REQUEST['RealSalesAmountVal']);

		$query = "
		set ansi_nulls on
        set ansi_warnings on
		exec [lasmart_p_AdvertisingEventsRows_Update]
		@UserID = $UserID
		,@ID = $ID
		,@RequestID = $RequestID
		,@GoodID = $GoodID
		,@Comment = $Comment

		,@OTDiscount = $OTDiscount
		,@DistrDiscount = $DistrDiscount
		,@NetDiscount = $NetDiscount
		,@DistrIncomePrice = $DistrIncomePrice
		,@NetIncomePriceBefore = $NetIncomePriceBefore
		,@SellingPriceBefore = $SellingPriceBefore
		,@EstimatedSalesAmountQty = $EstimatedSalesAmountQty			
		,@CataloguePubCost = $CataloguePubCost
		,@RealSellingPrice = $RealSellingPrice
		,@RealSalesAmountQty = $RealSalesAmountQty
		,@RealSalesAmountVal = $RealSalesAmountVal";

		if (mssql_query($query,$connection)) {
            $message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0 || strpos($message,'Ошибка!') === 0) {
                $arr['success']	= FALSE;
                $arr['msg']	= $message;
            } else
                $arr['success']	= TRUE;
        } else {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			ob_start();print_r($_REQUEST);echo "\n\n".$message;file_put_contents('out_mssql.txt',ob_get_clean());

			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function deleteAdvertisingEventsRows($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не заданы ID элементов';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];

		$query =
		"exec [lasmart_p_AdvertisingEventsRows_GroupDelete]
		@UserID = $UserID
		,@ID = '$ID'";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function groupCreateAdvertisingEventsRows($connection, $_REQUEST) {
		$arr = array();

		$UserID = $_SESSION['idUser'];
		
		$RequestID = 	parseParameter($_REQUEST['RequestID']);
		$GoodID = 		parseParameterWithEncode($_REQUEST['GoodID']);

		$query = "
		set ansi_nulls on
        set ansi_warnings on

		exec [lasmart_p_AdvertisingEventsRows_GroupCreate]
			@UserID = $UserID
			,@RequestID = $RequestID
			,@GoodID = $GoodID
		";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_cg.txt',ob_get_clean());
		if (mssql_query($query,$connection)) {
			$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function groupDuplicateAdvertisingEventsRows($connection, $_REQUEST) {
		$arr = array();

		$UserID = $_SESSION['idUser'];
		
		$RowIDs = 	parseParameterWithEncode($_REQUEST['RowIDs']);

		$query = "
		set ansi_nulls on
        set ansi_warnings on

		exec [lasmart_p_AdvertisingEventsRows_GroupDuplicate]
			@UserID = $UserID
			,@RowIDs = $RowIDs
		";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_cg.txt',ob_get_clean());
		if (mssql_query($query,$connection)) {
			$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}
		return $arr;
	}

	function recalculateAdvertisingEventsRows($connection, $_REQUEST) {
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$UserID = $_SESSION['idUser'];

		$query = "
			set ansi_nulls on
	        set ansi_warnings on
			
			exec [lasmart_p_AdvertisingEventsRows_Recalculate]
				@UserID = $UserID
				,@ID = $ID

			select top 1 * from [lasmart_v_fact_AdvertisingEventsRows] where ID = $ID
		";

		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gu.txt',ob_get_clean());
		$result = mssql_query($query,$connection);
		if ($result) {
            $message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            // ob_start();print_r($_REQUEST);echo "\n\n".$query;echo "\n\n".$message;file_put_contents('out_gu.txt',ob_get_clean());
            if (strpos($message,'Не заданы') === 0 || strpos($message,'Позиция является') === 0 || strpos($message,'Ошибка!') === 0) {
                $arr['success']	= FALSE;
                $arr['msg']	= $message;
            } else
                $arr['success']	= TRUE;

                while ($row = mssql_fetch_assoc($result)) {
					foreach ($row as &$value) {
						$value = iconv("cp1251", "UTF-8", $value);
					}
					$arr['data'] = $row;
				}
        } else {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
            // ob_start();print_r($_REQUEST);echo "\n\n".$query;echo "\n\n".$message;file_put_contents('out_recalculate_error.txt',ob_get_clean());

			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}

		return $arr;
	}

	function getAvailableArea($connection, $_REQUEST) {
		$arr = array();
		if (!isset($_SESSION['Sectors'])) {
			return getArea($connection, $_REQUEST);
		}

		$query = 'select ar.[ID]
			,ar.[Name]
			,ar.[ShortName]			
			,ar.[BriefNotation]
			,div.[ID] as [DivisionID]
			,div.[Name] as [Division]
			,ar.[Deleted]
		FROM [lasmart_t_dim_Area] ar with (nolock)
		left join [lasmart_t_dim_Division] div with (nolock)
			on ar.[DivisionID] = div.[ID]
		where ar.[Deleted] = 0 and ar.[ID] in ('.$_SESSION['Sectors'].')';

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$row['Division'] = iconv("cp1251", "UTF-8", $row['Division']);
			$arr [] = $row;
		}
		return $arr;
	}

	function getBossOrderStatus($connection, $_REQUEST) {
		$arr = array();
		
		$query = "select
			[ID],
			[Title] as [Name],
			0 as [Deleted]
		FROM [orimi_t_work_adv_OrderStatus]
		order by [ID]";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$arr [] = $row;
		}
		return $arr;
	}

	function getAdvertisingChannel($connection, $_REQUEST) {
		$arr = array();
		$query = "select [ID]
			,[Name]
			,[ShortName]
			,[BriefNotation]
			,[MaxNumberToShip]
			,[Deleted]
		FROM [lasmart_t_dim_AdvertisingChannel]
		where [Deleted] = 0

		union all

		select [ID]
			,[Name]
			,[ShortName]
			,[BriefNotation]
			,[MaxNumberToShip]
			,[Deleted]
		FROM [lasmart_t_dim_AdvertisingChannel]
		where [Deleted] = 1";

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			$row['Name'] = iconv("cp1251", "UTF-8", $row['Name']);
			$row['ShortName'] = iconv("cp1251", "UTF-8", $row['ShortName']);
			$row['BriefNotation'] = iconv("cp1251", "UTF-8", $row['BriefNotation']);
			$arr [] = $row;
		}
		return $arr;
	}

	function createAdvertisingChannel($connection, $_REQUEST){
		$arr = array();
		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$MaxNumberToShip = parseParameter($_REQUEST['MaxNumberToShip']);

		$query =
		"insert into [lasmart_t_dim_AdvertisingChannel]
		([Name]
		,[ShortName]
		,[MaxNumberToShip]
		,[BriefNotation]
		,[Deleted])
		VALUES($Name, $ShortName, $MaxNumberToShip, $BriefNotation, $Deleted)";

		if (mssql_query($query,$connection))
			$arr['success']	= TRUE;
		else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
		}

		return $arr;
	}

	function updateAdvertisingChannel($connection, $_REQUEST){
		$arr = array();
		if (isset($_REQUEST['ID'])) {
			$ID = parseParameter($_REQUEST['ID']);
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Не задан ID записи';
			return $arr;
		}

		$Name = parseParameterWithEncode($_REQUEST['Name']);
		$ShortName = parseParameterWithEncode($_REQUEST['ShortName']);
		$BriefNotation = parseParameterWithEncode($_REQUEST['BriefNotation']);
		$Deleted = isset($_REQUEST['Deleted']) ? str_replace("'","",strip_tags($_REQUEST['Deleted'])) : 0;
		$MaxNumberToShip = parseParameter($_REQUEST['MaxNumberToShip']);

		$TT_error_msg = parseParameterWithEncode('Сущствуют активные позиции справочника "Торговые точки" с этим атрибутом: ID ');

		$query =
		"if ($Deleted = 1) begin
			declare @error varchar(max) = ''
			SELECT @error = @error + convert(varchar,[ID]) + ','
			FROM [lasmart_t_dim_MasterStores] with (nolock)
			where AdvertisingChannelID = $ID and StatusID = 2
			order by [ID]
				
			if (LEN(@error) > 0) begin
				print $TT_error_msg+' '+left(@error,LEN(@error)-1)
				return
			end else begin
				update lasmart_t_dim_MasterStores
				set AdvertisingChannelID = null
				where AdvertisingChannelID = $ID
			end
		end

		update [lasmart_t_dim_AdvertisingChannel]
		set [Name] = $Name
		,[ShortName] = $ShortName
		,[MaxNumberToShip] = $MaxNumberToShip
		,[BriefNotation] = $BriefNotation
		,[Deleted] = $Deleted
		where ID = $ID

		exec [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange] @Attribute='AdvertisingChannelID', @Value=$ID";

		if (mssql_query($query,$connection)) {
			$message = iconv("CP1251", "UTF-8",mssql_get_last_message());
			if (strpos($message,'Сущствуют активные позиции') === 0) {
				$arr['success']	= FALSE;
				$arr['msg']	= $message;
			} else
				$arr['success']	= TRUE;
		} else {
			$arr['success']	= FALSE;
			$arr['msg']	= 'Произошла ошибка';
			$arr['sql_message'] = iconv("CP1251", "UTF-8",mssql_get_last_message());
		}
		return $arr;
	}

	function getActiveStores($connection, $_REQUEST) {
		$arr = array();
		$ret = array();
		$notNeedCount = false;
		$top = str_replace("'","",strip_tags(trim($_REQUEST['start'])))+str_replace("'","",strip_tags(trim($_REQUEST ['limit'])));
		$order = parseSorters($_REQUEST);

		if (isset($_REQUEST['ID'])) {
			$where = " where [ID] = ".parseParameter($_REQUEST['ID']);

			if (isset($_REQUEST['AreaID'])) {
				$AreaID = parseParameter($_REQUEST['AreaID']); // знак = приходит с фронта
				$where .= "and [AreaID] ".$AreaID;
			}

			$top = 1;
		} else  if (isset($_REQUEST['Name'])) {
			$where = " where [Name] like '%".iconv("UTF-8", "CP1251",str_replace("'","",strip_tags(trim($_REQUEST['Name']))))."%'";
			$top = 10;
		} else {
			$where = parseFilters($_REQUEST, 'none');
		}

		if ($_SESSION['masterStores']['fitlers'] === $where){
			$notNeedCount = true;
		}

		$query = "select top $top * FROM [lasmart_v_dim_ActiveMasterStores] $where $order";
		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_assoc($result)) {
			foreach ($row as &$value) {
				$value = iconv("cp1251", "UTF-8", $value);
			}
			$arr [] = $row;
		}
		$arr = array_slice($arr, $_REQUEST['start'],$_REQUEST ['limit']);
		$ret['items'] = $arr;

		if (!$notNeedCount) {
			$count = 0;
			$query = "select count(*) FROM [lasmart_v_dim_ActiveMasterStores] $where";
			$result = mssql_query($query,$connection);
			while ($row = mssql_fetch_row($result)) {
				$count = $row[0];
			}
			
			$ret['total'] = $count;

			$_SESSION['masterStores']['fitlers'] = $where;
			$_SESSION['masterStores']['count'] = $count;
		} else {
			$ret['total'] = $_SESSION['masterStores']['count'];
		}

		return $ret;
	}

	function getAdvertisingEventsReport($connection, $_REQUEST) {
		$arr = array();
		
		$columns = parseParameter($_REQUEST['columns']);
		$columns = $columns.',rowWarnings';
		$UserID = $_SESSION['idUser'];

		$where = "where 0 = 0 ";
		$where = parseFilters($_REQUEST, 'advertisingEventsRows');
		
		if (isset($_SESSION['Sectors'])) {
			$where .= ' and ([headerAreaID] in ('.$_SESSION['Sectors'].')
					or ([headerAreaID] is null and 
						[headerID] in (SELECT aeh.ID from [lasmart_v_fact_AdvertisingEventsHeaders] aeh left join lasmart_v_dim_Distributors d on aeh.ContractorGroupID = d.ContractorGroupID where d.AreaID in ('.$_SESSION['Sectors'].'))
					)
				)';
		}

		if (isset($_SESSION['Nets'])) {
			$where .= ' and ([rowNetID] in ('.trim($_SESSION['Nets'], ',').') or [rowNetID] is null)';
		}

        $columnsArr = split(',',$columns);

        //@todo task 92. Not a good solution
        if ($key = array_search('rowSalesAmountBeforeQty', $columnsArr)) {
            $columnsArr[$key] = "format(cast(rowSalesAmountBeforeQty as money), '#,0') as rowSalesAmountBeforeQty";
        }
        if ($key = array_search('rowSalesAmountBeforeVal', $columnsArr)) {
            $columnsArr[$key] = "format(cast(rowSalesAmountBeforeVal as money), '#,0') as rowSalesAmountBeforeVal";
        }
        $columns = implode(',', $columnsArr);

        $query = "select $columns from [lasmart_v_fact_AdvertisingEventsReport] $where";
		//ob_start();print_r($_REQUEST);echo "\n\n".$query;file_put_contents('out_gsr.txt',ob_get_clean());
		$name = $ref.'_'.$UserID.'_'.time();
		$fp = fopen('../../files/'.$name.'.csv', 'w');
		$csv_output = '';

		$fields = array(
			 'headerID' => 'ИД'
			,'headerDocNum' => 'Номер'
			,'headerTitle' => 'Заголовок'
			,'headerDocDate' => 'Дата'
			,'headerArea' => 'Участок'
			,'headerStatus' => 'Статус'
			,'headerDateStart' => 'Дата начала акции'
			,'headerDateEnd' => 'Дата окончания акции'
			,'headerDateStartMonth' => 'Месяц акции'
			,'headerContractorGroup' => 'Группа контрагентов'
			,'headerNet' => 'Название сети'
			,'headerStoreName' => 'Название ТТ'
			,'headerStoreAddress' => 'Адрес ТТ'
			,'headerStoreAmount' => 'Количество ТТ сети'
			,'headerMaxNumberToShip' => 'Макс количество единиц к отгрузке по акции, шт'
			,'headerMaxNumberToOneStore' => 'Макс разрешенное кол-во на 1 точку, шт'
			,'headerAdvertisingChannel' => 'Канал для рекламы ТТ'
			,'headerDateStartShipping' => 'Дата начала отгрузки'
			,'headerDateEndShipping' => 'Дата окончания отгрузки'
			,'headerAuthor' => 'Автор изменений'
			,'headerDateChange' => 'Дата посл изменения'
			,'headerisInTradent' => 'Отражена в TradeNT'
			,'headerTSZCode' => 'Номер ТСЗ'
			,'headerPaymentType' => 'Тип оплаты'

			,'rowID' => 'ИД'
			,'rowRequestID' => 'ИД заявки'
			,'rowGoodID' => 'ИД товара'
			,'rowGoodName' => 'Наименование товара'
			,'rowOrimiCode' => 'Код Орими'
			,'rowExternalPack' => 'Фасовка товара'
			,'rowBrand' => 'Бренд'
			,'rowSegment' => 'Сегмент'
			,'rowInternalPack' => 'Упаковка товара'
			,'rowComment' => 'Комментарий'
			,'rowAmountOfDaysAfterPrev' => 'Количество дней от предыдущей акции'
			,'rowOTDiscount' => 'Скидка по акции от ОРИМИ ТРЭЙД, %'
			,'rowDistrDiscount' => 'Скидка дистрибьютора, %'
			,'rowNetDiscount' => 'Скидка торговой сети, %'
			,'rowMaxDiscountDepth' => 'Максимальная глубина скидки, %'
			,'rowFGD' => 'Фонд генерального директора, %'
			,'rowMinPrice' => 'Минимально возможная цена на полке за период, руб'
			,'rowBPLPrice' => 'Цена базового прайс-листа, руб'
			,'rowDistrIncomePrice' => 'Входная цена дистрибьютора, руб'
			,'rowSupplyPrice' => 'Цена поставки в ТС данного ассортимента (без акции от БПЛ), %'
			,'rowNetIncomePriceBefore' => 'Входная цена торговой сети до акции, руб'
			,'rowSellingPriceBefore' => 'Цена позиции на полке в ТТ до акции, руб'
			,'rowNetMarkup' => 'Наценка сети, %'
			,'rowEstimatedSellingPrice' => 'Прогноз цены на полке во время акции, руб'
			,'rowFactDiscount' => 'Фактически проведенная скидка в сети, %'			
			,'rowNetIncomePriceAction' => 'Входная цена торговой сети во время акции, руб'
			,'rowIncomePriceDifference' => 'Разница между акционной и регулярной ценой, руб'
			,'rowInvoiceDiscount' => 'Скидка в накладной, %'
			,"format(cast(rowSalesAmountBeforeQty as money), '#,0') as rowSalesAmountBeforeQty" => 'Продажи до акции по данной позиции за сопоставимый период времени, шт'
			,"format(cast(rowSalesAmountBeforeVal as money), '#,0') as rowSalesAmountBeforeVal" => 'Продажи до акции по данной позиции за сопоставимый период времени, руб'
			,'rowEstimatedSalesAmountQty' => 'Прогноз компенсации, шт'
			,'rowEstimatesSalesAmountVal' => 'Прогноз компенсации, руб'
			,'rowEstimatesSalesAmountFGD' => 'Прогноз компенсации ФГД, руб'
			,'rowEstimatesSalesAmountD0Bonus' => 'Прогноз компенсации бонус Д0, руб'
			,'rowEstimatedSalesAmountQtyPerStr' => 'Прогноз компенсации на торговую точку, шт'
			,'rowMaxNumberToOneStore' => 'Макс разрешенное кол-во на 1 точку, шт'
			,'rowMaxNumberToShip' => 'Макс количество единиц к отгрузке по акции, шт'
			,'rowCataloguePubCost' => 'Стоимость публикации в каталоге, руб'
			,'rowRealSellingPrice' => 'Реальная цена на полке, руб'
			,'rowRealSalesAmountQty' => 'Продажи во время акции с учетом скидки, шт'
			,'rowRealSalesAmountVal' => 'Продажи во время акции с учетом скидки, руб'
			,'rowRealSalesAmountInRegularPrices' => 'Продажи во время акции в регулярных ценах, руб'
			,'rowDistrCost' => 'Затраты дистрибьютора, руб'
			,'rowOTCost' => 'Затраты ОТ на акцию, руб'
			,'rowFGDComp' => 'Компенсация ФГД, руб'
			,'rowD0Comp' => 'Компенсация Д0, руб'
			,'rowSalesIncrease' => 'Прирост, %'
			,'rowIncreasePercent' => 'Процент затрат, %'
			,'rowWarnings' => 'Заметки'
		);

		for($i=0;$i<count($columnsArr);$i++) {
			$csv_output .= iconv("UTF-8", "cp1251", $fields[$columnsArr[$i]].';');
		}

		$csv_output = substr($csv_output,0,-1);
		$csv_output .= "\n";
		fwrite($fp, $csv_output);

		$result = mssql_query($query,$connection);
		while ($row = mssql_fetch_row($result)) {
			$csv_output = '';
			$count = count($row);
			for ($i=0;$i<$count;$i++) {
				if (is_numeric($row[$i])) {
					$row[$i] = str_replace('.', ',', $row[$i]);
				}

				$csv_output .= str_replace(";","",$row[$i]);
				if ($i < $count-1) {
					$csv_output .= ';';
				}
			}
			$csv_output .= "\n";
			fwrite($fp, $csv_output);
		}
		fclose($fp);

		$zip = new ZipArchive(); 
		$zipRes = $zip->open('../../files/'.$name.'.zip', ZipArchive::CREATE);	
		$zip->addFile('../../files/'.$name.'.csv', $name.'.csv');
		$zip->close();
		unlink('../../files/'.$name.'.csv'); 

		$arr['name'] = $name;

		return $arr;
	}

?>
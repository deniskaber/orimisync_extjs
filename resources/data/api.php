<?php
	require_once "config.php";
	include "db_level.php";

	function api_answer($arr_data) {
		if (strstr($_SERVER['HTTP_ACCEPT_ENCODING'],'gzip')) {
			header("Content-Encoding: gzip");
			echo gzencode(json_encode($arr_data));
		} else {
			echo json_encode($arr_data);
		}
		die;
	}

	function api_error($answ, $status_message) {
		$answ['success'] = FALSE;
		$answ['msg'] = $status_message;

		return $answ;
	}

	function simpleSubAction($subaction, $dimension, $db_conn, $_REQUEST) {
		switch($subaction) {
			default:
				api_error('Invalid subaction');
			break;
			case 'get':
				$answ = getSimpleAttribute($db_conn,$dimension,$_REQUEST);
			break;
			case 'create':
				$answ = createSimpleAttribute($db_conn,$dimension,$_REQUEST);
			break;
			case 'update':
				$answ = updateSimpleAttribute($db_conn,$dimension,$_REQUEST);
			break;
		}

		return $answ;
	}

	$answ = array();
	$answ['success'] = TRUE;

	$action = $_REQUEST['act'];
	$subaction = isset($_REQUEST['subaction']) ?  $_REQUEST['subaction'] : 'get';

	switch ($action) {
		case 'getPortalData': // позиции мастер-справочника
			$answ = getPortalData($db_conn,$_REQUEST);
		break;
		case 'getMasterGoods': // позиции мастер-справочника
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getMasterGoods($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createMasterGoods($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateMasterGoods($db_conn,$_REQUEST);
				break;
				case 'groupUpdate':
					$answ = groupUpdateMasterGoods($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getMasterStores': // позиции справочника торговые точки
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getMasterStores($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createMasterStores($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateMasterStores($db_conn,$_REQUEST);
				break;
				case 'groupUpdate':
					$answ = groupUpdateMasterStores($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getMasterStaff': // позиции справочника сотрудники
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getMasterStaff($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createMasterStaff($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateMasterStaff($db_conn,$_REQUEST);
				break;
				case 'groupUpdate':
					$answ = groupUpdateMasterStaff($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getMasterDistributors': // позиции справочника торговые точки
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getMasterDistributors($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createMasterDistributors($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateMasterDistributors($db_conn,$_REQUEST);
				break;
				case 'groupUpdate':
					$answ = groupUpdateMasterDistributors($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getMasterWarehouses': // позиции справочника склады
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getMasterWarehouses($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createMasterWarehouses($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateMasterWarehouses($db_conn,$_REQUEST);
				break;
				case 'groupUpdate':
					$answ = groupUpdateMasterWarehouses($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getParentGoods':
				$answ = getParentGoods($db_conn,$_REQUEST);
		break;
		case 'getParentStaff':
				$answ = getParentStaff($db_conn,$_REQUEST);
		break;
		case 'getDistributors':
				$answ = getDistributors($db_conn,$_REQUEST);
		break;
		case 'getOrimiGoods':
				$answ = getOrimiGoods($db_conn,$_REQUEST);
		break;
		case 'getSlaveGoods': // позиции подчиненного справочника
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getSlaveGoods($db_conn,$_REQUEST);
				break;
				case 'sync':
					$answ = syncSlaveGoods($db_conn,$_REQUEST);
				break;
				case 'groupSync':
					$answ = groupSyncSlaveGoods($db_conn,$_REQUEST);
				break;
				case 'groupSyncWindow':
					$answ = groupSyncWindowSlaveGoods($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getSlaveStores': // позиции подчиненного справочника Торговые точки
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getSlaveStores($db_conn,$_REQUEST);
				break;
				case 'sync':
					$answ = syncSlaveStores($db_conn,$_REQUEST);
				break;
				case 'groupSync':
					$answ = groupSyncSlaveStores($db_conn,$_REQUEST);
				break;
				case 'groupSyncWindow':
					$answ = groupSyncWindowSlaveStores($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getSlaveStaff': // позиции подчиненного справочника Сотрудники
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getSlaveStaff($db_conn,$_REQUEST);
				break;
				case 'sync':
					$answ = syncSlaveStaff($db_conn,$_REQUEST);
				break;
				case 'groupSync':
					$answ = groupSyncSlaveStaff($db_conn,$_REQUEST);
				break;
				case 'groupSyncWindow':
					$answ = groupSyncWindowSlaveStaff($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getSlaveWarehouses': // позиции подчиненного справочника склады
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getSlaveWarehouses($db_conn,$_REQUEST);
				break;
				case 'sync':
					$answ = syncSlaveWarehouses($db_conn,$_REQUEST);
				break;
				case 'groupSync':
					$answ = groupSyncSlaveWarehouses($db_conn,$_REQUEST);
				break;
				case 'groupSyncWindow':
					$answ = groupSyncWindowSlaveWarehouses($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getSyncedGoods': // синхронизированные позиции подчиненного справочника
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getSyncedGoods($db_conn,$_REQUEST);
				break;
				case 'unsync':
					$answ = unsyncSyncedGoods($db_conn,$_REQUEST);
				break;
				case 'groupunsync':
					$answ = groupunsyncSyncedGoods($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getSyncedStores': // синхронизированные позиции подчиненного справочника Торговые точки
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getSyncedStores($db_conn,$_REQUEST);
				break;
				case 'unsync':
					$answ = unsyncSyncedStores($db_conn,$_REQUEST);
				break;
				case 'groupunsync':
					$answ = groupunsyncSyncedStores($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getSyncedStaff': // синхронизированные позиции подчиненного справочника Сотрудники
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getSyncedStaff($db_conn,$_REQUEST);
				break;
				case 'unsync':
					$answ = unsyncSyncedStaff($db_conn,$_REQUEST);
				break;
				case 'groupunsync':
					$answ = groupunsyncSyncedStaff($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getSyncedWarehouses': // синхронизированные позиции подчиненного справочника склады
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getSyncedWarehouses($db_conn,$_REQUEST);
				break;
				case 'unsync':
					$answ = unsyncSyncedWarehouses($db_conn,$_REQUEST);
				break;
				case 'groupunsync':
					$answ = groupunsyncSyncedWarehouses($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getManufacturer': // производители
			$answ = simpleSubAction($subaction, 'Manufacturer',$db_conn,$_REQUEST);
		break;
		case 'getBrand': // бренды
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getBrand($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createBrand($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateBrand($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getSubBrand': // саббренды
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getSubBrand($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createSubBrand($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateSubBrand($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getCategory': // категории
			$answ = simpleSubAction($subaction, 'Category',$db_conn,$_REQUEST);
		break;
		case 'getSegment': // сегменты
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getSegment($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createSegment($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateSegment($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getSubSegment': // сабсегменты
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getSubSegment($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createSubSegment($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateSubSegment($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getTemplate': //  Шаблоны
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getTemplate($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createTemplate($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateTemplate($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getWeightGroup': // весовая группа
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getWeightGroup($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createWeightGroup($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateWeightGroup($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getTeaColor': // Цвет чая
			$answ = simpleSubAction($subaction, 'TeaColor',$db_conn,$_REQUEST);
		break;
		case 'getCoffeeSort': // Сорт кофе
			$answ = simpleSubAction($subaction, 'CoffeeSort',$db_conn,$_REQUEST);
		break;
		case 'getCountry': // Страна
			$answ = simpleSubAction($subaction, 'Country',$db_conn,$_REQUEST);
		break;
		case 'getCounty': // Округ
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getCounty($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createCounty($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateCounty($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getRegion': // Регион
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getRegion($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createRegion($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateRegion($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getDistrict': // Район
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getDistrict($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createDistrict($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateDistrict($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getCity': // Город
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getCity($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createCity($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateCity($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getStreet': // Улица
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getStreet($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createStreet($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateStreet($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getExternalPack': // внешняя упаковка
			$answ = simpleSubAction($subaction, 'ExternalPack',$db_conn,$_REQUEST);
		break;
		case 'getInternalPack': // внутренняя упаковка
			$answ = simpleSubAction($subaction, 'InternalPack',$db_conn,$_REQUEST);
		break;
		case 'getGift': // подарок
			$answ = simpleSubAction($subaction, 'Gift',$db_conn,$_REQUEST);
		break;
		case 'getGiftType': // тип подарка
			$answ = simpleSubAction($subaction, 'GiftType',$db_conn,$_REQUEST);
		break;
		case 'getPriceSegment': // ценовой сегмент
			$answ = simpleSubAction($subaction, 'PriceSegment',$db_conn,$_REQUEST);
		break;
		case 'getSachetPack': // упаковка саше
			$answ = simpleSubAction($subaction, 'SachetPack',$db_conn,$_REQUEST);
		break;
		case 'getChannel': // канал
			$answ = simpleSubAction($subaction, 'Channel',$db_conn,$_REQUEST);
		break;
		case 'getSubchannel': // подканал
			$answ = simpleSubAction($subaction, 'Subchannel',$db_conn,$_REQUEST);
		break;
		case 'getFormat': // формат
			$answ = simpleSubAction($subaction, 'Format',$db_conn,$_REQUEST);
		break;
		case 'getNetType': // тип сети
			$answ = simpleSubAction($subaction, 'NetType',$db_conn,$_REQUEST);
		break;
		case 'getNet':
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getNet($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createNet($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateNet($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getParentNet':
            $answ = getParentNet($db_conn,$_REQUEST);
        break;
		case 'getTypeStaff': // тип персонала
			$answ = simpleSubAction($subaction, 'Typestaff',$db_conn,$_REQUEST);
		break;
		case 'getMotivation': // мотивация
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getMotivation($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createMotivation($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateMotivation($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getUsers': // пользователи админка
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getUsers($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createUsers($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateUsers($db_conn,$_REQUEST);
				break;
				case 'delete':
					$answ = deleteUsers($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getSectorUserFilter': // участки для админки
			$answ = getSectorUserFilter($db_conn,$_REQUEST);
		break;
		case 'getNetUserFilter': // сети для админки
			$answ = getNetUserFilter($db_conn,$_REQUEST);
		break;
		case 'getCategoryUserFilter': // категории для админки
			$answ = getCategoryUserFilter($db_conn,$_REQUEST);
		break;
		case 'getManufacturerUserFilter': // производители для админки
			$answ = getManufacturerUserFilter($db_conn,$_REQUEST);
		break;
		case 'getRoles': // роли
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getRoles($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createRoles($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateRoles($db_conn,$_REQUEST);
				break;
				case 'delete':
					$answ = deleteRoles($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getBarcode': // Штрихкод
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getBarcode($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createBarcode($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateBarcode($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getAdding': // добавки
			$answ = simpleSubAction($subaction, 'Adding',$db_conn,$_REQUEST);
		break;
		case 'getFlavor': // ароматы
			$answ = simpleSubAction($subaction, 'Flavor',$db_conn,$_REQUEST);
		break;
		case 'getSelectedAdding': // добавки
			$answ = getSelectedAdding($db_conn,$_REQUEST);
		break;
		case 'getSelectedFlavor': // ароматы
			$answ = getSelectedFlavor($db_conn,$_REQUEST);
		break;
		case 'getSelectedSector': // участок
			$answ = getSelectedSector($db_conn,$_REQUEST);
		break;
		case 'getSelectedArea': // участок
			$answ = getSelectedArea($db_conn,$_REQUEST);
		break;
		case 'updateStatusSku': // перевод черновика в активвный статус sku
			$answ = updateStatusSku($db_conn,$_REQUEST);
		break;
		case 'updateStatusTT': // перевод черновика в активвный статус tt
			$answ = updateStatusTT($db_conn,$_REQUEST);
		break;
		case 'updateStatusStaff': // перевод черновика в активвный статус staff
			$answ = updateStatusStaff($db_conn,$_REQUEST);
		break;
		case 'updateStatusDistr': // перевод черновика в активвный статус distr
			$answ = updateStatusDistr($db_conn,$_REQUEST);
		break;
		case 'updateStatusWrhs': // перевод черновика в активвный статус wrhs
			$answ = updateStatusWrhs($db_conn,$_REQUEST);
		break;
		case 'setDeleteStatusSku': // удаление позиции sku
			$answ = setDeleteStatusSku($db_conn,$_REQUEST);
		break;
		case 'setDeleteStatusTT': // удаление позиции tt
			$answ = setDeleteStatusTT($db_conn,$_REQUEST);
		break;
		case 'setDeleteStatusStaff': // удаление позиции Staff
			$answ = setDeleteStatusStaff($db_conn,$_REQUEST);
		break;
		case 'setDeleteStatusDistr': // удаление позиции Distr
			$answ = setDeleteStatusDistr($db_conn,$_REQUEST);
		break;
		case 'setDeleteStatusWrhs': // удаление позиции wrhs
			$answ = setDeleteStatusWrhs($db_conn,$_REQUEST);
		break;
		case 'deleteBarcode': // удаление штрихкода
      		$answ = deleteBarcode($db_conn,$_REQUEST);
    	break;
    	case 'deleteTemplate': // удаление штрихкода
      		$answ = deleteTemplate($db_conn,$_REQUEST);
    	break;
		case 'getEventHistory': // история действий с позицией
      		$answ = getEventHistory($db_conn,$_REQUEST);
    	break;
    	case 'getSector': // участок
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getSector($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createSector($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateSector($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getArea': // участок
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getArea($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createArea($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateArea($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getDivision': // дивизион
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getDivision($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createDivision($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateDivision($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getTradentDepartment':
			$answ = simpleSubAction($subaction, 'TradentDepartment',$db_conn,$_REQUEST);
		break;
		case 'getSubChannel': // подканал
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getSubChannel($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createSubChannel($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateSubChannel($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getChannel': // канал
			$answ = simpleSubAction($subaction, 'Channel',$db_conn,$_REQUEST);
		break;
		case 'getRespTR': // отв ТП
			$answ = simpleSubAction($subaction, 'RespTR',$db_conn,$_REQUEST);
		break;
		case 'getRespMerch': // отв Мерчендайзер
			$answ = simpleSubAction($subaction, 'RespMerch',$db_conn,$_REQUEST);
		break;
		case 'getContractorGroup': // отв Мерчендайзер
			$answ = simpleSubAction($subaction, 'ContractorGroup',$db_conn,$_REQUEST);
		break;
		case 'getCurrancy': // отв Мерчендайзер
			$answ = simpleSubAction($subaction, 'Currancy',$db_conn,$_REQUEST);
		break;
		case 'getNonAccReason': // отв Мерчендайзер
			$answ = simpleSubAction($subaction, 'NonAccReason',$db_conn,$_REQUEST);
		break;
		default:
			$answ = api_error($answ,'Invalid action');
		break;
		case 'getManagers': // менеджеры
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getManagers($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createManagers($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateManagers($db_conn,$_REQUEST);
				break;
				case 'delete':
					$answ = deleteManagers($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getNetSalesDataSource':
            $answ = simpleSubAction($subaction, 'NetSalesDataSource',$db_conn,$_REQUEST);
        break;
		case 'getSyncReport':
			$answ = getSyncReport($db_conn,$_REQUEST);
		break;
		case 'getFile':
			$answ = getFile($db_conn,$_REQUEST);
		break;
		case 'getGroupSyncGoods':
			$answ = getGroupSyncGoods($db_conn,$_REQUEST);
		break;
		case 'getGroupSyncStores':
			$answ = getGroupSyncStores($db_conn,$_REQUEST);
		break;
		case 'getGroupSyncStaff':
			$answ = getGroupSyncStaff($db_conn,$_REQUEST);
		break;
		case 'getGroupSyncWarehouses':
			$answ = getGroupSyncWarehouses($db_conn,$_REQUEST);
		break;
		case 'getSyncResultReport':
			$answ = getSyncResultReport($db_conn,$_REQUEST);
		break;
		case 'getAdvertisingBossHeaders':
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getAdvertisingBossHeaders($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateAdvertisingBossHeaders($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getAdvertisingBossRows':
			$answ = getAdvertisingBossRows($db_conn,$_REQUEST);
		break;
		case 'getAdvertisingBossCurrentOrders':
			$answ = getAdvertisingBossCurrentOrders($db_conn,$_REQUEST);
		break;
		case 'getNetStoreAmountByArea':
			$answ = getNetStoreAmountByArea($db_conn,$_REQUEST);
		break;
		case 'getAdvertisingEventsHeaders':
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getAdvertisingEventsHeaders($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createAdvertisingEventsHeaders($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateAdvertisingEventsHeaders($db_conn,$_REQUEST);
				break;
				case 'delete':
					$answ = deleteAdvertisingEventsHeaders($db_conn,$_REQUEST);
				break;
				case 'updateStatus':
					$answ = updateAdvertisingEventsHeadersStatus($db_conn,$_REQUEST);
				break;
				case 'duplicate':
					$answ = duplicateAdvertisingEvents($db_conn,$_REQUEST);
				break;
				case 'recalculate':
					$answ = recalculateAdvertisingEvents($db_conn,$_REQUEST);
				break;
				case 'loadToTSZ':
					$answ = loadToTSZAdvertisingEvents($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getAdvertisingEventsRows':
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getAdvertisingEventsRows($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createAdvertisingEventsRows($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateAdvertisingEventsRows($db_conn,$_REQUEST);
				break;
				case 'delete':
					$answ = deleteAdvertisingEventsRows($db_conn,$_REQUEST);
				break;
				case 'groupCreate':
					$answ = groupCreateAdvertisingEventsRows($db_conn,$_REQUEST);
				break;
				case 'groupDuplicate':
					$answ = groupDuplicateAdvertisingEventsRows($db_conn,$_REQUEST);
				break;
				case 'recalculate':
					$answ = recalculateAdvertisingEventsRows($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getAdvertisingEventsReport':
			$answ = getAdvertisingEventsReport($db_conn,$_REQUEST);
		break;
		case 'getRequestStatus':
			$answ = simpleSubAction($subaction, 'RequestStatus',$db_conn,$_REQUEST);
		break;
		case 'getBossOrderStatus': 
			$answ = getBossOrderStatus($db_conn,$_REQUEST);
		break;
		case 'getAvailableArea': // участок
			$answ = getAvailableArea($db_conn,$_REQUEST);
		break;
		case 'getAdvertisingChannel': // участок
			switch($subaction) {
				default:
					$answ = api_error($answ,'Invalid subaction');
				break;
				case 'get':
					$answ = getAdvertisingChannel($db_conn,$_REQUEST);
				break;
				case 'create':
					$answ = createAdvertisingChannel($db_conn,$_REQUEST);
				break;
				case 'update':
					$answ = updateAdvertisingChannel($db_conn,$_REQUEST);
				break;
			}
		break;
		case 'getActiveStores':
			$answ = getActiveStores($db_conn,$_REQUEST);
		break;
		case 'getPaymentType': 
			$answ = simpleSubAction($subaction, 'PaymentType',$db_conn,$_REQUEST);
		break;
		
	}
	api_answer($answ);
?>

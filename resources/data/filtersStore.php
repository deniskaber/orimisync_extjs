<?php
require_once '../config.php';
$idSource = (isset($_REQUEST ['filterId']) ? strip_tags(trim(Query_Part::ms_escape_string($_REQUEST ['filterId']))) : 'sku');
$checked = (isset($_REQUEST ['checked']) ? (strip_tags(trim(Query_Part::ms_escape_string($_REQUEST ['checked']))) === 'true') : false);
$search = iconv("UTF-8","cp1251",(isset($_REQUEST ['search']) ? '%'.trim(Query_Part::ms_escape_string($_REQUEST ['search'])).'%' : '%'));
$searchattr0 = iconv("UTF-8","cp1251",(isset($_REQUEST ['searchattr0']) ? '%'.trim(Query_Part::ms_escape_string($_REQUEST ['searchattr0'])).'%' : '%'));
$searchattr1 = iconv("UTF-8","cp1251",(isset($_REQUEST ['searchattr1']) ? '%'.trim(Query_Part::ms_escape_string($_REQUEST ['searchattr1'])).'%' : '%'));
$searchattr2 = iconv("UTF-8","cp1251",(isset($_REQUEST ['searchattr2']) ? '%'.trim(Query_Part::ms_escape_string($_REQUEST ['searchattr2'])).'%' : '%'));
$supplData = (isset($_REQUEST ['supplData']) ? " and g.SupplData = '".strip_tags(trim(Query_Part::ms_escape_string($_REQUEST ['supplData'])))."'" : '');
$onJoinSupplData = ($_SESSION ['admin']==1 ? 'and t.SupplData=g.SupplData' : '');
$prefix = '';
$arr = array();
if ($_SESSION ['admin']==1 && $supplData=='') {
	echo json_encode($arr);
	die();
}
if (isset($_REQUEST ['node']) && $_REQUEST ['node'] !== 'root') {
	$parent = strip_tags(trim(Query_Part::ms_escape_string(substr($_REQUEST ['node'],3))));
	switch ($idSource) {
		case 'sku':
			$idGroup = (isset($_REQUEST ['groupType']) ? strip_tags(trim(Query_Part::ms_escape_string($_REQUEST ['groupType']))) : 'trademark');
			switch ($idGroup) {
				case 'trademark':
					$prefix = "sku";
					$query = "select APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods g where TrademarkID = $parent and NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by NameFull";
				break;
				case 'manufacturer':
					$prefix = "sku";
					$query = "select APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods g where ManufacturerID = $parent and NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by NameFull";
				break;
				case 'goodgroup':
					$prefix = "sku";
					$query = "select APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods g where GoodGroupID = $parent and NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by NameFull";
				break;
				case 'GoodDim0':
					$prefix = "sku";
					$query = "select APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods g where GoodDim0 = $parent and NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by NameFull";
				break;
				case 'GoodDim1':
					$prefix = "sku";
					$query = "select APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods g where GoodDim1 = $parent and NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by NameFull";
				break;
				case 'GoodDim2':
					$prefix = "sku";
					$query = "select APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods g where GoodDim2 = $parent and NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by NameFull";
				break;
				case 'GoodDim3':
					$prefix = "sku";
					$query = "select APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods g where GoodDim3 = $parent and NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by NameFull";
				break;
				case 'GoodDim4':
					$prefix = "sku";
					$query = "select APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods g where GoodDim4 = $parent and NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by NameFull";
				break;
				case 'GoodDim5':
					$prefix = "sku";
					$query = "select APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods g where GoodDim5 = $parent and NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by NameFull";
				break;
				case 'GoodDim6':
					$prefix = "sku";
					$query = "select APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods g where GoodDim6 = $parent and NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by NameFull";
				break;
				case 'GoodDim7':
					$prefix = "sku";
					$query = "select APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods g where GoodDim7 = $parent and NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by NameFull";
				break;
				case 'GoodDim8':
					$prefix = "sku";
					$query = "select APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods g where GoodDim8 = $parent and NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by NameFull";
				break;
				case 'GoodDim9':
					$prefix = "sku";
					$query = "select APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods g where GoodDim9 = $parent and NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by NameFull";
				break;
			}
			break;
		case 'sup':
			$idGroup = (isset($_REQUEST ['groupType']) ? strip_tags(trim(Query_Part::ms_escape_string($_REQUEST ['groupType']))) : 'supplier');
			switch ($idGroup) {
				case 'SupplierDim0':
					$prefix = "sup";
					$query = "select SupplierID as ID, SupplierName as Name, SupplierAttribute0 as Attribute0, SupplierAttribute1 as Attribute1, SupplierAttribute2 as Attribute2 from dbo.t_dim_suppliers g where SupplierDim0 = $parent and SupplierName like '$search' ".$supplData." order by SupplierName";
				break;
				case 'SupplierDim1':
					$prefix = "sup";
					$query = "select SupplierID as ID, SupplierName as Name, SupplierAttribute0 as Attribute0, SupplierAttribute1 as Attribute1, SupplierAttribute2 as Attribute2 from dbo.t_dim_suppliers g where SupplierDim1 = $parent and SupplierName like '$search' ".$supplData." order by SupplierName";
				break;
				case 'SupplierDim2':
					$prefix = "sup";
					$query = "select SupplierID as ID, SupplierName as Name, SupplierAttribute0 as Attribute0, SupplierAttribute1 as Attribute1, SupplierAttribute2 as Attribute2 from dbo.t_dim_suppliers g where SupplierDim2 = $parent and SupplierName like '$search' ".$supplData." order by SupplierName";
				break;
			}
			break;
		case 'str':
			$idGroup = (isset($_REQUEST ['groupType']) ? strip_tags(trim(Query_Part::ms_escape_string($_REQUEST ['groupType']))) : 'region');
			switch ($idGroup) {
				case 'region':
					$prefix = "str";
					$query = "select StoreID as ID, StoreName+' '+StoreAddress as Name, StoreAttribute0 as Attribute0, StoreAttribute1 as Attribute1, StoreAttribute2 as Attribute2 from dbo.t_dim_drugstores g where RegionID = $parent and StoreName+' '+StoreAddress like '$search' ".$supplData." order by StoreName";
				break;
				case 'StoreDim0':
					$prefix = "str";
					$query = "select StoreID as ID, StoreName+' '+StoreAddress as Name, StoreAttribute0 as Attribute0, StoreAttribute1 as Attribute1, StoreAttribute2 as Attribute2 from dbo.t_dim_drugstores g where StoreDim0 = $parent and StoreName+' '+StoreAddress like '$search' ".$supplData." order by StoreName";
				break;
				case 'StoreDim1':
					$prefix = "str";
					$query = "select StoreID as ID, StoreName+' '+StoreAddress as Name, StoreAttribute0 as Attribute0, StoreAttribute1 as Attribute1, StoreAttribute2 as Attribute2 from dbo.t_dim_drugstores g where StoreDim1 = $parent and StoreName+' '+StoreAddress like '$search' ".$supplData." order by StoreName";
				break;
				case 'StoreDim2':
					$prefix = "str";
					$query = "select StoreID as ID, StoreName+' '+StoreAddress as Name, StoreAttribute0 as Attribute0, StoreAttribute1 as Attribute1, StoreAttribute2 as Attribute2 from dbo.t_dim_drugstores g where StoreDim2 = $parent and StoreName+' '+StoreAddress like '$search' ".$supplData." order by StoreName";
				break;
			}
			break;
		default:
			die();
	}
	
	$result = mssql_query($query,($_SESSION ['admin']==1 ? $admin_conn : $db_conn));
	while ($row = mssql_fetch_assoc($result)) {
		$arr[] = array(
			'id'=>$prefix.$row ['ID'],
			'text'=>iconv("cp1251","UTF-8",$row ['Name']),
			//'qtip'=>iconv("cp1251","UTF-8",$row ['Name']),
			'attribute0'=>iconv("cp1251","UTF-8",$row ['Attribute0']),
			'attribute1'=>iconv("cp1251","UTF-8",$row ['Attribute1']),
			'attribute2'=>iconv("cp1251","UTF-8",$row ['Attribute2']),
			'leaf'=>true,
			'checked'=>$checked,
			'expanded'=>false,
			'expandable'=>false
		);
	}
} else {
	switch ($idSource) {
		case 'sku':
			$idGroup = (isset($_REQUEST ['groupType']) ? strip_tags(trim(Query_Part::ms_escape_string($_REQUEST ['groupType']))) : 'trademark');
			switch ($idGroup) {
				case 'trademark':
					$prefix = "trm";
					$query = "select isnull(TMID,0) as ParentID, isnull(TMName,'не задано') as ParentName, APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_trademarks t left join dbo.t_dim_goods g on t.TMID = g.TrademarkID $onJoinSupplData where (NameFull like '$search' or TMName like '$search') and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'manufacturer':
					$prefix = "man";
					$query = "select isnull(ManID,0) as ParentID, isnull(ManName,'не задано') as ParentName, APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_manufacturers t left join dbo.t_dim_goods g on t.ManID = g.ManufacturerID $onJoinSupplData where (NameFull like '$search' or ManName like '$search') and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'goodgroup':
					$prefix = "grp";
					$query = "select isnull(t.GoodGroupID,0) as ParentID, isnull(GoodGroupName,'не задано') as ParentName, APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_GoodGroups t left join dbo.t_dim_goods g on t.GoodGroupID = g.GoodGroupID $onJoinSupplData where (NameFull like '$search' or GoodGroupName like '$search') and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'GoodDim0':
					$prefix = "gd0";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods_dimension0 left join dbo.t_dim_goods on DimID = GoodDim0 where NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'GoodDim1':
					$prefix = "gd1";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods_dimension1 left join dbo.t_dim_goods on DimID = GoodDim1 where NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'GoodDim2':
					$prefix = "gd2";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods_dimension2 left join dbo.t_dim_goods on DimID = GoodDim2 where NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'GoodDim3':
					$prefix = "gd3";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods_dimension3 left join dbo.t_dim_goods on DimID = GoodDim3 where NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'GoodDim4':
					$prefix = "gd4";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods_dimension4 left join dbo.t_dim_goods on DimID = GoodDim4 where NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'GoodDim5':
					$prefix = "gd5";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods_dimension5 left join dbo.t_dim_goods on DimID = GoodDim5 where NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'GoodDim6':
					$prefix = "gd6";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods_dimension6 left join dbo.t_dim_goods on DimID = GoodDim6 where NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'GoodDim7':
					$prefix = "gd7";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods_dimension7 left join dbo.t_dim_goods on DimID = GoodDim7 where NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'GoodDim8':
					$prefix = "gd8";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods_dimension8 left join dbo.t_dim_goods on DimID = GoodDim8 where NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'GoodDim9':
					$prefix = "gd9";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from dbo.t_dim_goods_dimension9 left join dbo.t_dim_goods on DimID = GoodDim9 where NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'good':
					$prefix = "sku";
					$query = "SELECT APCode as ID, NameFull as Name, GoodAttribute0 as Attribute0, GoodAttribute1 as Attribute1, GoodAttribute2 as Attribute2 from t_dim_goods g where NameFull like '$search' and isnull(GoodAttribute0,'') like '$searchattr0' and isnull(GoodAttribute1,'') like '$searchattr1' and isnull(GoodAttribute2,'') like '$searchattr2' ".$supplData." order BY Name";
				break;
			}
			break;
		case 'sup':
			$idGroup = (isset($_REQUEST ['groupType']) ? strip_tags(trim(Query_Part::ms_escape_string($_REQUEST ['groupType']))) : 'supplier');
			switch ($idGroup) {
				case 'SupplierDim0':
					$prefix = "sd0";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, SupplierID as ID, SupplierName as Name, SupplierAttribute0 as Attribute0, SupplierAttribute1 as Attribute1, SupplierAttribute2 as Attribute2 from dbo.t_dim_suppliers_dimension0 left join dbo.t_dim_suppliers g on DimID = SupplierDim0 where SupplierName like '$search' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'SupplierDim1':
					$prefix = "sd1";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, SupplierID as ID, SupplierName as Name, SupplierAttribute0 as Attribute0, SupplierAttribute1 as Attribute1, SupplierAttribute2 as Attribute2 from dbo.t_dim_suppliers_dimension1 left join dbo.t_dim_suppliers g on DimID = SupplierDim1 where SupplierName like '$search' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'SupplierDim2':
					$prefix = "sd2";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, SupplierID as ID, SupplierName as Name, SupplierAttribute0 as Attribute0, SupplierAttribute1 as Attribute1, SupplierAttribute2 as Attribute2 from dbo.t_dim_suppliers_dimension2 left join dbo.t_dim_suppliers g on DimID = SupplierDim2 where SupplierName like '$search' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'supplier':
					$prefix = "sup";
					$query = "SELECT isnull([SupplierID],0)  as ID,isnull([SupplierName],'не задано') as [Name], SupplierAttribute0 as Attribute0, SupplierAttribute1 as Attribute1, SupplierAttribute2 as Attribute2 FROM dbo.t_dim_suppliers g where SupplierName like '$search' ".$supplData." order by Name";
				break;
			}
			break;
		case 'str':
			$idGroup = (isset($_REQUEST ['groupType']) ? strip_tags(trim(Query_Part::ms_escape_string($_REQUEST ['groupType']))) : 'region');
			switch ($idGroup) {
				case 'region':
					$prefix = "reg";
					$query = "select isnull(CompInRegId,0) as ParentID, isnull(CompanyInReg,'не задано') as ParentName, StoreID as ID, StoreName+' '+StoreAddress as Name, StoreAttribute0 as Attribute0, StoreAttribute1 as Attribute1, StoreAttribute2 as Attribute2 from dbo.t_dim_companyinreg t left join dbo.t_dim_drugstores g on t.CompInRegId = g.RegionID $onJoinSupplData where StoreName+' '+StoreAddress like '$search' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'StoreDim0':
					$prefix = "sd0";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, StoreID as ID, StoreName+' '+StoreAddress as Name, StoreAttribute0 as Attribute0, StoreAttribute1 as Attribute1, StoreAttribute2 as Attribute2 from dbo.t_dim_Wrhs_dimension0 left join dbo.t_dim_drugstores g on DimID = StoreDim0 where StoreName+' '+StoreAddress like '$search' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'StoreDim1':
					$prefix = "sd1";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, StoreID as ID, StoreName+' '+StoreAddress as Name, StoreAttribute0 as Attribute0, StoreAttribute1 as Attribute1, StoreAttribute2 as Attribute2 from dbo.t_dim_Wrhs_dimension1 left join dbo.t_dim_drugstores g on DimID = StoreDim1 where StoreName+' '+StoreAddress like '$search' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'StoreDim2':
					$prefix = "sd2";
					$query = "select isnull(DimID,0) as ParentID, isnull(DimName,'не задано') as ParentName, StoreID as ID, StoreName+' '+StoreAddress as Name, StoreAttribute0 as Attribute0, StoreAttribute1 as Attribute1, StoreAttribute2 as Attribute2 from dbo.t_dim_Wrhs_dimension2 left join dbo.t_dim_drugstores g on DimID = StoreDim2 where StoreName+' '+StoreAddress like '$search' ".$supplData." order by ParentName, ParentID, Name";
				break;
				case 'store':
					$prefix = "str";
					$query = "SELECT StoreID as ID, StoreName+' '+StoreAddress as Name, StoreAttribute0 as Attribute0, StoreAttribute1 as Attribute1, StoreAttribute2 as Attribute2 FROM t_dim_drugstores g where StoreName+' '+StoreAddress like '$search' ".$supplData." order BY Name";
				break;
			}
			break;
		default:
			die();
	}
	
	if ($prefix == "sku" || $prefix == "sup" || $prefix == "str") {
		$result = mssql_query($query,($_SESSION ['admin']==1 ? $admin_conn : $db_conn));
		while ($row = mssql_fetch_assoc($result)) {
			$arr[] = array(
				'id'=>$prefix.$row ['ID'],
				'text'=>iconv("cp1251","UTF-8",$row ['Name']),
				//'qtip'=>iconv("cp1251","UTF-8",$row ['Name']),
				'attribute0'=>iconv("cp1251","UTF-8",$row ['Attribute0']),
				'attribute1'=>iconv("cp1251","UTF-8",$row ['Attribute1']),
				'attribute2'=>iconv("cp1251","UTF-8",$row ['Attribute2']),
				'leaf'=>true,
				'checked'=>$checked,
				'expanded'=>false,
				'expandable'=>false
			);
		}
	} else {
		$result = mssql_query($query,($_SESSION ['admin']==1 ? $admin_conn : $db_conn));
		$parent = -1;
		while ($row = mssql_fetch_assoc($result)) {
			if ($parent == $row ['ParentID']) {
				$arr[count($arr)-1]['children'][] = array(
					'id'=>$idSource.$row ['ID'],
					'text'=>iconv("cp1251","UTF-8",$row ['Name']),
					//'qtip'=>iconv("cp1251","UTF-8",$row ['Name']),
					'attribute0'=>iconv("cp1251","UTF-8",$row ['Attribute0']),
					'attribute1'=>iconv("cp1251","UTF-8",$row ['Attribute1']),
					'attribute2'=>iconv("cp1251","UTF-8",$row ['Attribute2']),
					'leaf'=>true,
					'checked'=>$checked,
					'expanded'=>false,
					'expandable'=>false
				);
			} else {
				$parent = $row ['ParentID'];
				$arr[] = array(
					'id'=>$prefix.$row ['ParentID'],
					'text'=>iconv("cp1251","UTF-8",$row ['ParentName']),
					//'qtip'=>iconv("cp1251","UTF-8",$row ['ParentName']),
					'leaf'=>false,
					'checked'=>$checked,
					'expanded'=>false,
					'expandable'=>true,
					'children'=>array(array(
						'id'=>$idSource.$row ['ID'],
						'text'=>iconv("cp1251","UTF-8",$row ['Name']),
						//'qtip'=>iconv("cp1251","UTF-8",$row ['Name']),
						'attribute0'=>iconv("cp1251","UTF-8",$row ['Attribute0']),
						'attribute1'=>iconv("cp1251","UTF-8",$row ['Attribute1']),
						'attribute2'=>iconv("cp1251","UTF-8",$row ['Attribute2']),
						'leaf'=>true,
						'checked'=>$checked,
						'expanded'=>false,
						'expandable'=>false
					))
				);
			}
		}
	}
}
//ob_start();print_r($_SERVER);print_r($_SESSION);print_r($_REQUEST);echo $query;echo $idSource;file_put_contents('out_save.txt',ob_get_clean());
if (strstr($_SERVER['HTTP_ACCEPT_ENCODING'],'gzip')) {
	header("Content-Encoding: gzip");
	echo gzencode(json_encode($arr));
} else {
	echo json_encode($arr);
}
?>
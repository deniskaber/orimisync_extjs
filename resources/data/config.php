<?php
session_start();
if ((!isset($_SESSION)) or (!isset($_SESSION ['auth']) || $_SESSION ['auth'] !== 1)
or (!isset($_SESSION ['HTTP_USER_AGENT']) or ($_SESSION ['HTTP_USER_AGENT'] !== md5($_SERVER ['HTTP_USER_AGENT']))))
{
	session_destroy();
	header("Access-Control-Allow-Origin: *");
	header("Location: index.php");
	die();
}

require_once 'conf_info.php';

$host = $_PAR['host'];
$login = $_PAR['login'];
$pwd = $_PAR['pass'];
$db_conn = mssql_connect($host, $login, $pwd);
mssql_select_db($_PAR['db'],$db_conn);

class Query_Part
{
    public static function doPart()
    {
        $part = '';
        if (!empty($_REQUEST ['field']))
        {
            $f  = $_REQUEST ['field'];
            $vs = $_REQUEST ['vals'];
            $part = " WHERE $f IN ($vs)";
            if (!$_REQUEST['vals'])
            {
                $part = " WHERE $f = 0";
            }
        }
        return $part;
    }

	public static function ms_escape_string($data) {
        if ( !isset($data)/* or empty($data) */) return '';
        if ( is_numeric($data) ) return $data;

        $non_displayables = array(
            '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/',             // url encoded 16-31
            '/[\x00-\x08]/',            // 00-08
            '/\x0b/',                   // 11
            '/\x0c/',                   // 12
            '/[\x0e-\x1f]/'             // 14-31
        );
        foreach ( $non_displayables as $regex )
            $data = preg_replace( $regex, '', $data );
        $data = str_replace("'", "''", $data );
        return $data;
    }
}

if (isset($_GET['logout'])) {
	session_destroy();

	header("Location: index.php");
	die();
}
?>

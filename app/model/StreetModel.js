Ext.define('TestOrimi.model.StreetModel', {
    extend: 'Ext.data.Model',
    fields: [
		{name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'ShortName',		type: 'string'}
		,{name: 'BriefNotation',	type: 'string'}
		,{name: 'City',			type: 'string'}
		,{name: 'CityID',		type: 'int'}
		,{name: 'Deleted',			type: 'int'}
	]
});

Ext.define('TestOrimi.model.CountyModel', {
    extend: 'Ext.data.Model',
    fields: [
		{name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'ShortName',		type: 'string'}
		,{name: 'BriefNotation',	type: 'string'}
		,{name: 'Country',			type: 'string'}
		,{name: 'CountryID',		type: 'int'}
		,{name: 'Deleted',			type: 'int'}
	]
});

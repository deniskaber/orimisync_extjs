Ext.define('TestOrimi.model.StoresFieldsModel', {
    extend: 'Ext.data.Model',
    fields: [
		{name: 'text',		type: 'string'},
		{name: 'value',		type: 'string'}
	]
});

Ext.define('TestOrimi.model.TextValueModel', {
    extend: 'Ext.data.Model',
    fields: [
		{name: 'text',		type: 'string'},
		{name: 'value',	type: 'int'}
	]
});

Ext.define('TestOrimi.model.SubSegmentModel', {
    extend: 'Ext.data.Model',
    fields: [
		{name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'ShortName',		type: 'string'}
		,{name: 'BriefNotation',	type: 'string'}
		,{name: 'Deleted',			type: 'int'}

		,{name: 'CategoryID',		type: 'int'}
		,{name: 'Category',			type: 'string'}

		,{name: 'SegmentID',		type: 'int'}
		,{name: 'Segment',			type: 'string'}
	]
});

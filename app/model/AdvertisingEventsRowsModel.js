Ext.define('TestOrimi.model.AdvertisingEventsRowsModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'ID', type: 'int', useNull: true},

        {name: 'RequestID', type: 'int', useNull: true},
        {name: 'AreaID', type: 'int', useNull: true},
        {name: 'Area', type: 'string', useNull: true},
        {name: 'StatusID', type: 'int', useNull: true},
        {name: 'RequestStatus', type: 'string', useNull: true},
        {name: 'ContractorGroupID', type: 'int', useNull: true},
        {name: 'ContractorGroup', type: 'string', useNull: true},
        {name: 'NetID', type: 'int', useNull: true},
        {name: 'Net', type: 'string', useNull: true},
        {name: 'StoreID', type: 'int', useNull: true},
        {name: 'StoreName', type: 'string', useNull: true},
        {name: 'StoreAddress', type: 'string', useNull: true},
        {name: 'StoreAmount', type: 'int', useNull: true},
        {name: 'MaxNumberToShip', type: 'int', useNull: true},
        {name: 'MaxNumberToOneStore', type: 'int', useNull: true},
        {name: 'AdvertisingChannelID', type: 'int', useNull: true},
        {name: 'AdvertisingChannel', type: 'string', useNull: true},
        {name: 'DateStart', type: 'string', useNull: true},
        {name: 'DateEnd', type: 'string', useNull: true},

        {name: 'GoodID', type: 'int', useNull: true},
        {name: 'GoodName', type: 'string', useNull: true},
        {name: 'Brand', type: 'string', useNull: true},
        {name: 'OrimiCode', type: 'string', useNull: true},
        {name: 'Segment', type: 'string', useNull: true},
        {name: 'ExternalPack', type: 'string', useNull: true},
        {name: 'InternalPack', type: 'string', useNull: true},
        {name: 'Comment', type: 'string', useNull: true},
        {name: 'AmountOfDaysAfterPrev', type: 'int', useNull: true},

        {name: 'OTDiscount', type: 'float', useNull: true},
        {name: 'DistrDiscount', type: 'float', useNull: true},
        {name: 'NetDiscount', type: 'float', useNull: true},
        {name: 'MaxDiscountDepth', type: 'float', useNull: true},

        {name: 'FGD', type: 'float', useNull: true},
        {name: 'MinPrice', type: 'float', useNull: true},
        {name: 'BPLPrice', type: 'float', useNull: true},
        {name: 'DistrIncomePrice', type: 'float', useNull: true},
        {name: 'NetIncomePriceBefore', type: 'float', useNull: true},
        {name: 'SellingPriceBefore', type: 'float', useNull: true},
        {name: 'NetMarkup', type: 'float', useNull: true},

        {name: 'EstimatedSellingPrice', type: 'float', useNull: true},
        {name: 'FactDiscount', type: 'float', useNull: true},
        {name: 'SupplyPrice', type: 'float', useNull: true},
        {name: 'NetIncomePriceAction', type: 'float', useNull: true},
        {name: 'IncomePriceDifference', type: 'float', useNull: true},
        {name: 'InvoiceDiscount', type: 'float', useNull: true},
        {name: 'SalesAmountBeforeQty', type: 'float', useNull: true},
        {name: 'SalesAmountBeforeVal', type: 'float', useNull: true},
        {name: 'EstimatedSalesAmountQty', type: 'float', useNull: true},
        {name: 'EstimatesSalesAmountVal', type: 'float', useNull: true},
        {name: 'EstimatesSalesAmountFGD', type: 'float', useNull: true},
        {name: 'EstimatesSalesAmountD0Bonus', type: 'float', useNull: true},
        {name: 'EstimatedSalesAmountQtyPerStr', type: 'float', useNull: true},
        {name: 'CataloguePubCost', type: 'float', useNull: true},
        {name: 'RealSellingPrice', type: 'float', useNull: true},
        {name: 'RealSalesAmountQty', type: 'float', useNull: true},
        {name: 'RealSalesAmountVal', type: 'float', useNull: true},
        {name: 'RealSalesAmountInRegularPrices', type: 'float', useNull: true},
        {name: 'DistrCost', type: 'float', useNull: true},
        {name: 'OTCost', type: 'float', useNull: true},
        {name: 'FGDComp', type: 'float', useNull: true},
        {name: 'D0Comp', type: 'float', useNull: true},
        {name: 'SalesIncrease', type: 'float', useNull: true},
        {name: 'IncreasePercent', type: 'float', useNull: true}
    ]
});

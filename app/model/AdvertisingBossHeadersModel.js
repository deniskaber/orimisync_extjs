Ext.define('TestOrimi.model.AdvertisingBossHeadersModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'ID',                type: 'int',        useNull: true},
        {name: 'DocNum',            type: 'int',     useNull: true},
        {name: 'Title',             type: 'string',     useNull: true},
        {name: 'DateBegin',         type: 'string',     useNull: true},
        {name: 'DateEnd',           type: 'string',     useNull: true},
        {name: 'Title',             type: 'string',     useNull: true},
        {name: 'StatusID',          type: 'int',        useNull: true},
        {name: 'Status',            type: 'string',     useNull: true},
        {name: 'Author',            type: 'string',     useNull: true},
        {name: 'DateChange',         type: 'string',     useNull: true}
	],

    idProperty: 'ID'
});

Ext.define('TestOrimi.model.BossOrderStatusModel', {
    extend: 'Ext.data.Model',
    fields: [
		{name: 'ID',				type: 'int'}
		,{name: 'Title',				type: 'string'}
	]
});

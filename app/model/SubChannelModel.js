Ext.define('TestOrimi.model.SubChannelModel', {
    extend: 'Ext.data.Model',
    fields: [
		{name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'ShortName',		type: 'string'}
		,{name: 'BriefNotation',	type: 'string'}
		,{name: 'Channel',			type: 'string'}
		,{name: 'ChannelID',		type: 'int'}
		,{name: 'Deleted',			type: 'int'}
	]
});

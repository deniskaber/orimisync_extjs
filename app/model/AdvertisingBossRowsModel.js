Ext.define('TestOrimi.model.AdvertisingBossRowsModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'GoodID', type: 'int', useNull: true},
        {name: 'GoodName', type: 'string', useNull: true},
        {name: 'Brand', type: 'string', useNull: true},
        {name: 'OrimiCode', type: 'string', useNull: true},
        {name: 'FGD', type: 'int', useNull: true},
        {name: 'MinPrice', type: 'float', useNull: true},
        {name: 'BPLPrice', type: 'float', useNull: true},
        {name: 'MaxDiscount', type: 'float', useNull: true}
    ]
});

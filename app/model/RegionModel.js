Ext.define('TestOrimi.model.RegionModel', {
    extend: 'Ext.data.Model',
    fields: [
		{name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'ShortName',		type: 'string'}
		,{name: 'BriefNotation',	type: 'string'}
		,{name: 'Country',			type: 'string'}
		,{name: 'CountryID',		type: 'int', 	useNull: true}
		,{name: 'County',			type: 'string'}
		,{name: 'CountyID',			type: 'int'}
		,{name: 'Deleted',			type: 'int'}
	]
});

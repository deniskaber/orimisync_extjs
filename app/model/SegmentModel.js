Ext.define('TestOrimi.model.SegmentModel', {
    extend: 'Ext.data.Model',
    fields: [
		{name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'ShortName',		type: 'string'}
		,{name: 'BriefNotation',	type: 'string'}
		,{name: 'Deleted',			type: 'int'}

		,{name: 'CategoryID',		type: 'int'}
		,{name: 'Category',			type: 'string'}
	]
});

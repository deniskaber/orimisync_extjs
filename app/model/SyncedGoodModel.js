Ext.define('TestOrimi.model.SyncedGoodModel', {
    extend: 'Ext.data.Model',
    fields: [
    	{ name: 'ID', type: 'int' },
        { name: 'MasterID', type: 'int' },
        { name: 'Code', type: 'string' },
        { name: 'OrimiCode', type: 'string' },
        { name: 'Name', type: 'string' },
        { name: 'From', type: 'int' },
        { name: 'Manufacturer', type: 'string' },
        { name: 'Brand', type: 'string' },
        { name: 'Category', type: 'string' },
        { name: 'Segment', type: 'string' },
        { name: 'TeaColor', type: 'string' },
        { name: 'Pack', type: 'string' },
        { name: 'BagQty', type: 'int' },
        { name: 'BagWeight', type: 'float' },
        { name: 'Weight', type: 'float' },
        { name: 'isWithFlavor', type: 'int' },
        { name: 'isWithAdding', type: 'int' },
        { name: 'PriceSegment', type: 'string' },
        { name: 'Country', type: 'string' },
        { name: 'DataSourceID',    type: 'int' },
        { name: 'DataSource', type: 'string' },
        { name: 'DateChange', type: 'string' },
        { name: 'SyncType', type: 'string' }
	]
});

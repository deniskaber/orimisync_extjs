Ext.define('TestOrimi.model.SlaveStoresModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'ID',            type: 'int'},
        {name: 'Code',          type: 'string'},
        {name: 'DistrCode',     type: 'string'},
        {name: 'Name',          type: 'string'},
        {name: 'TempAddress',   type: 'string'},
        {name: 'County',        type: 'string'},
        {name: 'Region',        type: 'string'},
        {name: 'City',          type: 'string'},
        {name: 'Division',      type: 'string'},
        {name: 'Sector',        type: 'string'},
        {name: 'SubChannel',    type: 'string'},
        {name: 'Format',        type: 'string'},
        {name: 'isNet',         type: 'string'},
        {name: 'Net',           type: 'string'},
        {name: 'ContractorGroup',type: 'string'},
        {name: 'DataSourceID',    type: 'int' },
        {name: 'DataSource',    type: 'string' },
        {name: 'DateChange',    type: 'string' }
	]
});

Ext.define('TestOrimi.model.DivisionModel', {
    extend: 'Ext.data.Model',
    fields: [
		{name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'ShortName',		type: 'string'}
		,{name: 'BriefNotation',	type: 'string'}
		,{name: 'TradentDepartmentID',	type: 'int'}
		,{name: 'TradentDepartment',	type: 'string'}
		,{name: 'Deleted',			type: 'int'}
	]
});

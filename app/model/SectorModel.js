Ext.define('TestOrimi.model.SectorModel', {
    extend: 'Ext.data.Model',
    fields: [
		 {name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'ShortName',		type: 'string'}
		,{name: 'BriefNotation',	type: 'string'}
		,{name: 'Area',			type: 'string'}
		,{name: 'AreaID',		type: 'int'}
		,{name: 'Division',			type: 'string'}
		,{name: 'DivisionID',		type: 'int'}
		,{name: 'Deleted',			type: 'int'}
	]
});

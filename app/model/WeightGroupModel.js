Ext.define('TestOrimi.model.WeightGroupModel', {
    extend: 'Ext.data.Model',
    fields: [
		 {name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'ShortName',		type: 'string'}
		,{name: 'BriefNotation',	type: 'string'}
		,{name: 'Deleted',			type: 'int'}
		,{name: 'Begin',			type: 'int'}
		,{name: 'End',				type: 'int'}
	]
});

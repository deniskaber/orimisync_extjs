Ext.define('TestOrimi.model.SyncedStaffModel', {
    extend: 'Ext.data.Model',
    fields: [
		{ name: 'ID',           type: 'int' },
        { name: 'MasterID',     type: 'int' },
        { name: 'Code',         type: 'string' },
        { name: 'DistrCode',    type: 'string'},
        { name: 'FIO',          type: 'string' },
        { name: 'BirthDate',    type: 'string' },
        { name: 'Type',         type: 'string' },
        { name: 'Division',     type: 'string' },
        { name: 'DateHired',    type: 'string' },
        { name: 'DateFired',    type: 'string' },
        { name: 'EMail',        type: 'string' },
        { name: 'Phone',        type: 'string' },
        { name: 'Motivation',   type: 'string' },
        { name: 'Feature',      type: 'int' },
        { name: 'Sector',       type: 'string' },      
        { name: 'Parent',       type: 'string' },
        { name: 'DataSourceID', type: 'int' },
        { name: 'DataSource',   type: 'string' },
        { name: 'DateChange',   type: 'string' },
        { name: 'SyncType',     type: 'string' }
	]
});

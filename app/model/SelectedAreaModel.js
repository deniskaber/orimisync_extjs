Ext.define('TestOrimi.model.SelectedAreaModel', {
    extend: 'Ext.data.Model',
    fields: [
		 {name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'DivisionID',		type: 'int'}
	]
});

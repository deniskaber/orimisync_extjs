Ext.define('TestOrimi.model.SubBrandModel', {
    extend: 'Ext.data.Model',
    fields: [
		{name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'ShortName',		type: 'string'}
		,{name: 'BriefNotation',	type: 'string'}
		,{name: 'Deleted',			type: 'int'}

		,{name: 'ManufacturerID',	type: 'int'}
		,{name: 'Manufacturer',		type: 'string'}

		,{name: 'BrandID',			type: 'int'}
		,{name: 'Brand',			type: 'string'}
	]
});

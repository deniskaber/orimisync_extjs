Ext.define('TestOrimi.model.SelectedAddingModel', {
    extend: 'Ext.data.Model',
    fields: [
		 {name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
	]
});

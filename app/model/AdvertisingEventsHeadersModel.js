Ext.define('TestOrimi.model.AdvertisingEventsHeadersModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'ID', type: 'int', useNull: true},
        {name: 'DocNum', type: 'string', useNull: true},
        {name: 'Title', type: 'string', useNull: true},
        {name: 'DocDate', type: 'string', useNull: true},
        {name: 'AreaID', type: 'int', useNull: true},
        {name: 'Area', type: 'string', useNull: true},
        {name: 'StatusID', type: 'int', useNull: true},
        {name: 'Status', type: 'string', useNull: true},
        {name: 'ContractorGroupID', type: 'int', useNull: true},
        {name: 'ContractorGroup', type: 'string', useNull: true},
        {name: 'NetID', type: 'int', useNull: true},
        {name: 'Net', type: 'string', useNull: true},
        {name: 'StoreID', type: 'int', useNull: true},
        {name: 'StoreName', type: 'string', useNull: true},
        {name: 'StoreAddress', type: 'string', useNull: true},
        {name: 'StoreAmount', type: 'int', useNull: true},
        {name: 'MaxNumberToShip', type: 'int', useNull: true},
        {name: 'MaxNumberToOneStore', type: 'int', useNull: true},
        {name: 'AdvertisingChannelID', type: 'int', useNull: true},
        {name: 'AdvertisingChannel', type: 'string', useNull: true},
        {name: 'DateStart', type: 'string', useNull: true},
        {name: 'DateEnd', type: 'string', useNull: true},
        {name: 'DateStartMonth', type: 'string', useNull: true},
        {name: 'ReturnComment', type: 'string', useNull: true},
        {name: 'Comment', type: 'string', useNull: true},
        {name: 'DateStartShipping', type: 'string', useNull: true},
        {name: 'DateEndShipping', type: 'string', useNull: true},
        {name: 'Author', type: 'string', useNull: true},
        {name: 'DateChange', type: 'string', useNull: true},
        {name: 'isInTradent', type: 'int', useNull: true},
        {name: 'TSZCode', type: 'string', useNull: true},
        {name: 'PaymentTypeID', type: 'int', useNull: true},
        {name: 'PaymentType', type: 'string', useNull: true}
    ],

    idProperty: 'ID'
});

Ext.define('TestOrimi.model.GoodsSyncResultModel', {
    extend: 'Ext.data.Model',
    fields: [
		{ name: 'masterID', 			type: 'int', 	useNull: true},
		{ name: 'masterParent',			type: 'string', useNull: true},
		{ name: 'masterCode', 			type: 'string', useNull: true},
		{ name: 'masterOrimiCode', 		type: 'string', useNull: true},
		{ name: 'masterName', 			type: 'string', useNull: true},
		{ name: 'masterShortName', 		type: 'string', useNull: true},
		{ name: 'masterTempName', 		type: 'string', useNull: true},
		{ name: 'masterFrom', 			type: 'int', 	useNull: true},
		{ name: 'masterManufacturer', 	type: 'string', useNull: true},
		{ name: 'masterBrand', 			type: 'string', useNull: true},
		{ name: 'masterSubBrand', 		type: 'string', useNull: true},
		{ name: 'masterCategory', 		type: 'string', useNull: true},
		{ name: 'masterSegment', 		type: 'string', useNull: true},
		{ name: 'masterSubSegment', 	type: 'string', useNull: true},
		{ name: 'masterTeaColor', 		type: 'string', useNull: true},
		{ name: 'masterExternalPack', 	type: 'string', useNull: true},
		{ name: 'masterSachetPack', 	type: 'string', useNull: true},
		{ name: 'masterInternalPack', 	type: 'string', useNull: true},
		{ name: 'masterBagQty', 		type: 'int', 	useNull: true},
		{ name: 'masterBagWeight', 		type: 'float',	useNull: true},
		{ name: 'masterWeight',			type: 'float',	useNull: true},
		{ name: 'masterisWithFlavor', 	type: 'int', 	useNull: true},
		{ name: 'masterFlavor', 		type: 'string', useNull: true},
		{ name: 'masterisWithAdding', 	type: 'int', 	useNull: true},
		{ name: 'masterAdding', 		type: 'string', useNull: true},
		{ name: 'masterisHorekaPack', 	type: 'int', 	useNull: true},
		{ name: 'masterPriceSegment', 	type: 'string', useNull: true},
		{ name: 'masterCountry', 		type: 'string', useNull: true},
		{ name: 'masterCoffeeSort', 	type: 'string', useNull: true},
		{ name: 'masterisCaffeine', 	type: 'string', useNull: true},
		{ name: 'masterGift',		 	type: 'string', useNull: true},
		{ name: 'masterGiftType', 		type: 'string', useNull: true},
		{ name: 'masterPhoto', 			type: 'string', useNull: true},
		{ name: 'masterLength', 		type: 'int', 	useNull: true},
		{ name: 'masterWidth', 			type: 'int', 	useNull: true},
		{ name: 'masterHeight', 		type: 'int', 	useNull: true},
		{ name: 'masterisWeightNet', 	type: 'int', 	useNull: true},
		{ name: 'masterComment', 		type: 'string', useNull: true},
		{ name: 'masterCollection', 	type: 'string', useNull: true},
		{ name: 'masterisPrivateMark', 	type: 'int', 	useNull: true},
		{ name: 'masterBarcode',		type: 'string', useNull: true},
		{ name: 'masterStatus', 		type: 'string', useNull: true},
		{ name: 'masterDataSource', 	type: 'string', useNull: true},

		{ name: 'toSync', 	type: 'bool'},

		{ name: 'slaveID', 				type: 'int', 	useNull: true },
		{ name: 'slaveCode', 			type: 'string', useNull: true },
		{ name: 'slaveOrimiCode', 		type: 'string', useNull: true },
		{ name: 'slaveName', 			type: 'string', useNull: true },
		{ name: 'slaveFrom', 			type: 'int', 	useNull: true },
		{ name: 'slaveManufacturer', 	type: 'string', useNull: true },
		{ name: 'slaveBrand', 			type: 'string', useNull: true },
		{ name: 'slaveCategory', 		type: 'string', useNull: true },
		{ name: 'slaveSegment', 		type: 'string', useNull: true },
		{ name: 'slaveTeaColor', 		type: 'string', useNull: true },
		{ name: 'slavePack', 			type: 'string', useNull: true },
		{ name: 'slaveBagQty', 			type: 'int', 	useNull: true },
		{ name: 'slaveBagWeight', 		type: 'float', 	useNull: true },
		{ name: 'slaveWeight', 			type: 'float', 	useNull: true },
		{ name: 'slaveisWithFlavor', 	type: 'int', 	useNull: true },
		{ name: 'slaveisWithAdding', 	type: 'int', 	useNull: true },
		{ name: 'slavePriceSegment', 	type: 'string', useNull: true },
		{ name: 'slaveCountry', 		type: 'string', useNull: true },
		{ name: 'slaveDataSource', 		type: 'string', useNull: true },
		{ name: 'slaveDateChange', 		type: 'string', useNull: true }
	]
});

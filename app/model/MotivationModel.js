Ext.define('TestOrimi.model.MotivationModel', {
    extend: 'Ext.data.Model',
    fields: [
		{name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'ShortName',		type: 'string'}
		,{name: 'BriefNotation',	type: 'string'}
		,{name: 'Deleted',			type: 'int'}

		,{name: 'CurrancyID',	type: 'int'}
		,{name: 'Currancy',		type: 'string'}
	]
});

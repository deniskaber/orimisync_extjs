Ext.define('TestOrimi.model.TextValueStringModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'text', type: 'string'},
        {name: 'value', type: 'string'}
    ]
});

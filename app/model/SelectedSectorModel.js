Ext.define('TestOrimi.model.SelectedSectorModel', {
    extend: 'Ext.data.Model',
    fields: [
		 {name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'AreaID',		type: 'int'}
	]
});

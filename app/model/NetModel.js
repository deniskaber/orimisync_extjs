Ext.define('TestOrimi.model.NetModel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'ID', type: 'int'}
        , {name: 'Name', type: 'string'}
        , {name: 'ShortName', type: 'string'}
        , {name: 'NetType', type: 'string'}
        , {name: 'NetTypeID', type: 'int', useNull: true}
        , {name: 'isNielsen', type: 'int', useNull: true}
        , {name: 'RespManager', type: 'string'}
        , {name: 'RespManagerID', type: 'int', useNull: true}
        , {name: 'hasSalesData', type: 'int', useNull: true}
        , {name: 'ParentNet', type: 'string'}
        , {name: 'ParentNetID', type: 'int', useNull: true}
        , {name: 'hasChildren', type: 'int', useNull: true}
        , {name: 'StoresCount', type: 'int', useNull: true}
        , {name: 'AdvertisingChannel', type: 'string'}
        , {name: 'AdvertisingChannelID', type: 'int', useNull: true}
        , {name: 'Deleted', type: 'int', useNull: true}
    ]
});

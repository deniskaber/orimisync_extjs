Ext.define('TestOrimi.model.TemplateModel', {
    extend: 'Ext.data.Model',
    fields: [
		{name: 'ID',              	type: 'int'},
		{name: 'Name',              type: 'string'},
        {name: 'DateCreate',        type: 'string'},
        {name: 'DateUsed',          type: 'string'},
        {name: 'State',          	type: 'string'},
        {name: 'StateSlaveLeft',    type: 'string'},
        {name: 'StateSlaveRight',   type: 'string'}
	]
});

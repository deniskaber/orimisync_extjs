Ext.define('TestOrimi.model.CityModel', {
    extend: 'Ext.data.Model',
    fields: [
		{name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'ShortName',		type: 'string'}
		,{name: 'BriefNotation',	type: 'string'}
		,{name: 'Country',			type: 'string'}
		,{name: 'CountryID',		type: 'int', 	useNull: true}
		,{name: 'Region',			type: 'string'}
		,{name: 'RegionID',			type: 'int', 	useNull: true}
		,{name: 'District',			type: 'string'}
		,{name: 'DistrictID',		type: 'int', 	useNull: true}
		,{name: 'Sector',			type: 'string'}
		,{name: 'SectorID',			type: 'int'}
		,{name: 'Deleted',			type: 'int'}
	]
});

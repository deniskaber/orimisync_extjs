Ext.define('TestOrimi.model.AreaModel', {
    extend: 'Ext.data.Model',
    fields: [
		 {name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'ShortName',		type: 'string'}
		,{name: 'BriefNotation',	type: 'string'}
		,{name: 'Division',			type: 'string'}
		,{name: 'DivisionID',		type: 'int'}
		,{name: 'Deleted',			type: 'int'}
	]
});

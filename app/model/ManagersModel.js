Ext.define('TestOrimi.model.ManagersModel', {
    extend: 'Ext.data.Model',
    fields: [
		 {name: 'ID',				type: 'int'}
		,{name: 'ManagersID',		type: 'int'}
		,{name: 'Managers',			type: 'string'}
		,{name: 'DateHired',		type: 'string'}
		,{name: 'DateFired',		type: 'string'}
	]
});

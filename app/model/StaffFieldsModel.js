Ext.define('TestOrimi.model.StaffFieldsModel', {
    extend: 'Ext.data.Model',
    fields: [
		{name: 'text',		type: 'string'},
		{name: 'value',		type: 'string'}
	]
});

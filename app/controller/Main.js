﻿Ext.define('TestOrimi.controller.Main', {
    extend: 'Ext.app.Controller',

    views: [
        'TestOrimi.view.Viewport'

        , 'TestOrimi.view.general_grid'
        , 'TestOrimi.view.general_tabpanelFirstcol'
        , 'TestOrimi.view.general_tabpanelSecondcol'
        , 'TestOrimi.view.barcodePanel'
        , 'TestOrimi.view.CreateSkuGroupSyncWindow'

        , 'TestOrimi.view.general_gridTT'
        , 'TestOrimi.view.general_tabpanelFirstcolTT'
        , 'TestOrimi.view.general_tabpanelSecondcolTT'
        , 'TestOrimi.view.CreateTTGroupSyncWindow'
        , 'TestOrimi.view.createTTWindow'

        , 'TestOrimi.view.general_gridStaff'
        , 'TestOrimi.view.general_tabpanelFirstcolStaff'
        , 'TestOrimi.view.general_tabpanelSecondcolStaff'
        , 'TestOrimi.view.CreateStaffGroupSyncWindow'

        , 'TestOrimi.view.general_gridDistr'

        , 'TestOrimi.view.wrhs_general_grid'

        , 'TestOrimi.view.general_gridLbar'
        , 'TestOrimi.view.TemplateSettings'
        , 'TestOrimi.view.SimpleAttributeWindow'
        , 'TestOrimi.view.BrandWindow'
        , 'TestOrimi.view.SubBrandWindow'
        , 'TestOrimi.view.SegmentWindow'
        , 'TestOrimi.view.SubSegmentWindow'
        , 'TestOrimi.view.NetWindow'
        , 'TestOrimi.view.ParentNetWindow'
        , 'TestOrimi.view.ParentID'
        , 'TestOrimi.view.WeightGroupID'
        , 'TestOrimi.view.AddingWindow'
        , 'TestOrimi.view.FlavorWindow'
        , 'TestOrimi.view.CountyWindow'
        , 'TestOrimi.view.RegionWindow'
        , 'TestOrimi.view.DistrictWindow'
        , 'TestOrimi.view.CityWindow'
        , 'TestOrimi.view.ParentStaffWindow'
        , 'TestOrimi.view.SectorWindow'
        , 'TestOrimi.view.AreaWindow'
        , 'TestOrimi.view.DivisionWindow'
        , 'TestOrimi.view.MotivationWindow'
        , 'TestOrimi.view.SubChannelWindow'
        , 'TestOrimi.view.ManagersWindow'
        , 'TestOrimi.view.AdvertisingBossPanel'
        , 'TestOrimi.view.AdvertisingBossHeadersGrid'
        , 'TestOrimi.view.AdvertisingBossRowsGrid'
        , 'TestOrimi.view.AdvertisingEventsPanel'
        , 'TestOrimi.view.AdvertisingEventsHeadersGrid'
        , 'TestOrimi.view.AdvertisingEventsRowsGrid'
        , 'TestOrimi.view.AdvertisingChannelWindow'
        , 'TestOrimi.view.StoreModalWindow'
        , 'TestOrimi.view.CreateAdvertisingEventWindow'
    ],

    refs: [{
        ref: 'viewport',
        selector: 'viewport'
    }, {
        ref: 'general_grid',
        selector: 'general_grid'
    }, {
        ref: 'general_gridTT',
        selector: 'general_gridTT'
    }, {
        ref: 'general_gridStaff',
        selector: 'general_gridStaff'
    }, {
        ref: 'general_gridDistr',
        selector: 'general_gridDistr'
    }, {
        ref: 'wrhs_general_grid',
        selector: 'wrhs_general_grid'
    }, {
        ref: 'general_tabpanelFirstcol',
        selector: 'general_tabpanelFirstcol'
    }, {
        ref: 'general_tabpanelSecondcol',
        selector: 'general_tabpanelSecondcol'
    }, {
        ref: 'general_tabpanelFirstcolTT',
        selector: 'general_tabpanelFirstcolTT'
    }, {
        ref: 'general_tabpanelSecondcolTT',
        selector: 'general_tabpanelSecondcolTT'
    }, {
        ref: 'general_tabpanelFirstcolStaff',
        selector: 'general_tabpanelFirstcolStaff'
    }, {
        ref: 'general_tabpanelSecondcolStaff',
        selector: 'general_tabpanelSecondcolStaff'
    }, {
        ref: 'wrhs_general_tabpanelFirstcol',
        selector: 'wrhs_general_tabpanelFirstcol'
    }, {
        ref: 'wrhs_general_tabpanelSecondcol',
        selector: 'wrhs_general_tabpanelSecondcol'
    }, {
        ref: 'templateSettings',
        selector: 'templateSettings'
    }, {
        ref: 'barcodePanel',
        selector: 'barcodePanel'
    }, {
        ref: 'general_gridLbar',
        selector: 'general_gridLbar'
    }, {
        ref: 'simpleAttributeWindow',
        selector: 'simpleAttributeWindow'
    }, {
        ref: 'brandWindow',
        selector: 'brandWindow'
    }, {
        ref: 'subbrandWindow',
        selector: 'subbrandWindow'
    }, {
        ref: 'segmentWindow',
        selector: 'segmentWindow'
    }, {
        ref: 'subsegmentWindow',
        selector: 'subsegmentWindow'
    }, {
        ref: 'NetWindow',
        selector: 'NetWindow'
    }, {
        ref: 'ParentNetWindow',
        selector: 'ParentNetWindow'
    }, {
        ref: 'parentID',
        selector: 'parentID'
    }, {
        ref: 'WeightGroupID',
        selector: 'WeightGroupID'
    }, {
        ref: 'addingWindow',
        selector: 'addingWindow'
    }, {
        ref: 'flavorWindow',
        selector: 'flavorWindow'
    }, {
        ref: 'createTTWindow',
        selector: 'createTTWindow'
    }, {
        ref: 'countyWindow',
        selector: 'countyWindow'
    }, {
        ref: 'regionWindow',
        selector: 'regionWindow'
    }, {
        ref: 'districtWindow',
        selector: 'districtWindow'
    }, {
        ref: 'cityWindow',
        selector: 'cityWindow'
    }, {
        ref: 'parentStaffWindow',
        selector: 'parentStaffWindow'
    }, {
        ref: 'sectorWindow',
        selector: 'sectorWindow'
    }, {
        ref: 'areaWindow',
        selector: 'areaWindow'
    }, {
        ref: 'divisionWindow',
        selector: 'divisionWindow'
    }, {
        ref: 'motivationWindow',
        selector: 'motivationWindow'
    }, {
        ref: 'subChannelWindow',
        selector: 'subChannelWindow'
    }, {
        ref: 'managersWindow',
        selector: 'managersWindow'
    }, {
        ref: 'createSkuGroupSyncWindow',
        selector: 'createSkuGroupSyncWindow'
    }, {
        ref: 'createTTGroupSyncWindow',
        selector: 'createTTGroupSyncWindow'
    }, {
        ref: 'createStaffGroupSyncWindow',
        selector: 'createStaffGroupSyncWindow'
    }, {
        ref: 'createWrhsGroupSyncWindow',
        selector: 'createWrhsGroupSyncWindow'
    }, {
        ref: 'distributorWindow',
        selector: 'distributorWindow'
    }, {
        ref: 'advertising_boss_panel',
        selector: 'advertising_boss_panel'
    }, {
        ref: 'advertising_boss_headers_grid',
        selector: 'advertising_boss_headers_grid'
    }, {
        ref: 'advertising_boss_rows_grid',
        selector: 'advertising_boss_rows_grid'
    }, {
        ref: 'advertising_events_panel',
        selector: 'advertising_events_panel'
    }, {
        ref: 'advertising_events_headers_grid',
        selector: 'advertising_events_headers_grid'
    }, {
        ref: 'advertising_events_rows_grid',
        selector: 'advertising_events_rows_grid'
    }, {
        ref: 'advertisingChannelWindow',
        selector: 'advertisingChannelWindow'
    }, {
        ref: 'storeModalWindow',
        selector: 'storeModalWindow'
    }, {
        ref: 'createAdvertisingEventWindow',
        selector: 'createAdvertisingEventWindow'
    }],

    init: function() {
        this.control({
            'viewport': {
                skurefbuttonclick: this.onViewportSkuRefButtonClick,
                ttrefbuttonclick: this.onViewportTTRefButtonClick,
                staffrefbuttonclick: this.onViewportStaffRefButtonClick,
                distrrefbuttonclick: this.onViewportDistrRefButtonClick,
                attrrefbuttonclick: this.onViewportAttrRefButtonClick,
                wrhsrefbuttonclick: this.onViewportWrhsRefButtonClick,
                adv_refbuttonclick: this.onViewportAdvRefButtonClick
            },
            'general_grid': {
                searchchange: this.onColumnSearchChange,
                columnshow: this.onGeneralGridColumnShow,
                selectionchange: this.onGeneralGridSelectionChange
                //,showNotification: this.showNotification
            },
            'general_gridTT': {
                searchchange: this.onColumnSearchChange,
                columnshow: this.onGeneralGridColumnShow,
                selectionchange: this.onGeneralGridTTSelectionChange
            },
            'general_gridStaff': {
                searchchange: this.onColumnSearchChange,
                columnshow: this.onGeneralGridColumnShow,
                selectionchange: this.onGeneralGridStaffSelectionChange
            },
            'general_gridDistr': {
                searchchange: this.onColumnSearchChange,
                columnshow: this.onGeneralGridColumnShow
            },
            'wrhs_general_grid': {
                searchchange: this.onColumnSearchChange,
                columnshow: this.onGeneralGridColumnShow,
                selectionchange: this.onGeneralGridWrhsSelectionChange
            },
            'general_tabpanelFirstcol': {
                searchchange: this.onColumnSearchChange,
                unsyncbuttonclick: this.onGeneral_tabpanelFirstcolUnsyncButtonClick,
                reportbuttonclick: this.onGeneral_tabpanelFirstcolReportButtonClick,
                celldblclick: this.onGeneral_tabpanelFirstcolCelldblclick,
                groupunsyncbuttonclick: this.onGeneral_tabpanelFirstcolGroupUnsyncButtonClick
            },
            'general_tabpanelFirstcolTT': {
                searchchange: this.onColumnSearchChange,
                unsyncbuttonclick: this.onGeneral_tabpanelFirstcolUnsyncButtonClick,
                reportbuttonclick: this.onGeneral_tabpanelFirstcolReportButtonClick,
                celldblclick: this.onGeneral_tabpanelFirstcolCelldblclick,
                groupunsyncbuttonclick: this.onGeneral_tabpanelFirstcolGroupUnsyncButtonClick
            },
            'general_tabpanelFirstcolStaff': {
                searchchange: this.onColumnSearchChange,
                unsyncbuttonclick: this.onGeneral_tabpanelFirstcolUnsyncButtonClick,
                reportbuttonclick: this.onGeneral_tabpanelFirstcolReportButtonClick,
                celldblclick: this.onGeneral_tabpanelFirstcolCelldblclick,
                groupunsyncbuttonclick: this.onGeneral_tabpanelFirstcolGroupUnsyncButtonClick
            },
            'wrhs_general_tabpanelFirstcol': {
                searchchange: this.onColumnSearchChange,
                unsyncbuttonclick: this.onGeneral_tabpanelFirstcolUnsyncButtonClick,
                reportbuttonclick: this.onGeneral_tabpanelFirstcolReportButtonClick,
                celldblclick: this.onGeneral_tabpanelFirstcolCelldblclick,
                groupunsyncbuttonclick: this.onGeneral_tabpanelFirstcolGroupUnsyncButtonClick
            },
            'general_tabpanelSecondcol': {
                searchchange: this.onColumnSearchChange,
                syncbuttonclick: this.onGeneral_tabpanelSecondcolSyncButtonClick,
                groupsyncbuttonclick: this.onGeneral_tabpanelSecondcolGroupSyncButtonClick
            },
            'general_tabpanelSecondcolTT': {
                searchchange: this.onColumnSearchChange,
                syncbuttonclick: this.onGeneral_tabpanelSecondcolSyncButtonClick,
                groupsyncbuttonclick: this.onGeneral_tabpanelSecondcolGroupSyncButtonClick
            },
            'general_tabpanelSecondcolStaff': {
                searchchange: this.onColumnSearchChange,
                syncbuttonclick: this.onGeneral_tabpanelSecondcolSyncButtonClick,
                groupsyncbuttonclick: this.onGeneral_tabpanelSecondcolGroupSyncButtonClick
            },
            'wrhs_general_tabpanelSecondcol': {
                searchchange: this.onColumnSearchChange,
                syncbuttonclick: this.onGeneral_tabpanelSecondcolSyncButtonClick,
                groupsyncbuttonclick: this.onGeneral_tabpanelSecondcolGroupSyncButtonClick
            },
            'templateSettings': {
                savebuttonclick: this.onTemplateSettingsSaveButtonClick,
                loadbuttonclick: this.onTemplateSettingsLoadButtonClick,
                deletebuttonclick: this.onTemplateSettingsDeleteButtonClick
            },
            'barcodePanel': {
                barcodeaddclick: this.onBarcodePanelAddButtonClick,
                barcodedeleteclick: this.onBarcodePanelDeleteButtonClick
            },
            //'sectorPanel': {
            //   sectoraddclick: this.onSectorPanelAddButtonClick
            //  ,sectordeleteclick: this.onSectorPanelDeleteButtonClick
            //},
            'general_gridLbar': {
                searchchange: this.checkMasterSearchStatus,
                statusaddclick: this.onGeneralGridLbarStatusAddClick,
                deleteaddclick: this.onGeneralGridLbarDeleteAddClick,
                eventhistoryclick: this.EventHistoryClick,
                templateloadstore: this.TemplateLoadStore
            },
            'simpleAttributeWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'brandWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'subbrandWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'segmentWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'subsegmentWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'NetWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'ParentNetWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'parentID': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'WeightGroupID': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'addingWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter
                //,afterrender: this.onAttributeWindowAfterRender
            },
            'flavorWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter
                //,afterrender: this.onAttributeWindowAfterRender
            },
            'sectorWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter
                //,afterrender: this.onAttributeWindowAfterRender
            },
            'areaWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter
                //,afterrender: this.onAttributeWindowAfterRender
            },
            'divisionWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter
                //,afterrender: this.onAttributeWindowAfterRender
            },
            'motivationWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter
                //,afterrender: this.onAttributeWindowAfterRender
            },
            'createTTWindow': {
                beforedestroy: this.onTTWindowBeforeDestroy
            },
            'countyWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'regionWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'districtWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'cityWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'parentStaffWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'subChannelWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'managersWindow': {
                managerdeletebuttonclick: this.onManagersWindowdeletebuttonclick
            },
            'createSkuGroupSyncWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                columnshow: this.onGeneralGridColumnShow,
                beforedestroy: this.onWindowClearFilter,
                celldblclick: this.onGridCelldblclick,
                reportbuttonclick: this.onGroupSyncWindowReportButtonClick
            },
            'createTTGroupSyncWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                columnshow: this.onGeneralGridColumnShow,
                beforedestroy: this.onWindowClearFilter,
                celldblclick: this.onGridCelldblclick
            },
            'createStaffGroupSyncWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                columnshow: this.onGeneralGridColumnShow,
                beforedestroy: this.onWindowClearFilter,
                celldblclick: this.onGridCelldblclick
            },
            'distributorWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'createWrhsGroupSyncWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                columnshow: this.onGeneralGridColumnShow,
                beforedestroy: this.onWindowClearFilter,
                celldblclick: this.onGridCelldblclick
            },
            'advertising_boss_panel': {
                activate: this.onAdvertisingBossPanelActivate
            },
            'advertising_boss_headers_grid': {
                columnshow: this.onGeneralGridColumnShow,
                selectionchange: this.onAdvertisingBossHeadersGridSelectionChange,
                searchchange: this.onColumnSearchChange,
                celldblclick: this.onAdvertisingBossHeadersGridCellDblClick
            },
            'advertising_boss_rows_grid': {
                columnshow: this.onGeneralGridColumnShow,
                searchchange: this.onColumnSearchChange
            },
            'advertising_events_panel': {
                activate: this.onAdvertisingEventsPanelActivate
            },
            'advertising_events_headers_grid': {
                columnshow: this.onGeneralGridColumnShow,
                selectionchange: this.onAdvertisingEventsHeadersGridSelectionChange,
                searchchange: this.onColumnSearchChange,
                deleteButtonClick: this.onAdvertisingEventsHeadersGridDeleteButtonClick,
                celldblclick: this.onAdvertisingEventsHeadersGridCellDblClick,
                updateStatus: this.onAdvertisingEventsHeadersGridUpdateStatusClick,
                duplicateButtonClick: this.onAdvertisingEventsHeadersGridDuplicateButtonClick,
                eventHistoryButtonClick: this.EventHistoryClick,
                approveAllButtonClick: this.onAdvertisingEventsHeadersGridApproveAllButtonClick,
                loadTSZButtonClick: this.onAdvertisingEventsHeadersGridLoadTSZButton
            },
            'advertising_events_rows_grid': {
                columnshow: this.onGeneralGridColumnShow,
                selectionchange: this.onAdvertisingEventsRowsGridSelectionChange,
                searchchange: this.onColumnSearchChange,
                addNewRowButtonClick: this.onAdvertisingEventsRowsGridAddNewRowButtonClick,
                addMultipleRowButtonClick: this.onAdvertisingEventsRowsGridAddMultipleRowButtonClick,
                createMultipleRows: this.onAdvertisingEventsRowsGridCreateMultipleRows,
                duplicateMultipleRowButtonClick: this.onAdvertisingEventsRowsGridDuplicateMultipleRowButtonClick,
                celldblclick: this.onAdvertisingEventsRowsGridCellDblClick,
                deleteaddclick: this.onAdvertisingEventsRowsGridDeleteButtonClick,
                reportbuttonclick: this.onAdvertisingEventsRowsGridReportButtonClick,
                eventHistoryButtonClick: this.EventHistoryClick,
                headerSearchButtonClick: this.onAdvertisingEventsRowsGridHeaderSearchButtonClick
            },
            'advertisingChannelWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'storeModalWindow': {
                searchchange: this.onAttributeWindowSearchChange,
                beforedestroy: this.onWindowClearFilter,
                afterrender: this.onAttributeWindowAfterRender
            },
            'createAdvertisingEventWindow': {
                beforedestroy: this.onCreateAdvertisingEventWindowBeforeDestroy,
                setManualFactStatus: this.onCreateAdvertisingEventWindowSetManualFactStatus
            }
        });

        Ext.getStore('TestOrimi.store.MasterGoodStore').addListener('applyFilters', this.GridStoreApplyFilters, this);
        Ext.getStore('TestOrimi.store.MasterGoodStore').addListener('beforeload', this.MasterGoodStoreBeforeLoad, this);
        Ext.getStore('TestOrimi.store.MasterGoodStore').addListener('load', this.MasterGoodStoreAfterLoad, this);

        Ext.getStore('TestOrimi.store.MasterStoresStore').addListener('applyFilters', this.GridStoreApplyFilters, this);
        Ext.getStore('TestOrimi.store.MasterStoresStore').addListener('beforeload', this.MasterStoresStoreBeforeLoad, this);
        Ext.getStore('TestOrimi.store.MasterStoresStore').addListener('load', this.MasterStoresStoreAfterLoad, this);

        Ext.getStore('TestOrimi.store.MasterStaffStore').addListener('applyFilters', this.GridStoreApplyFilters, this);
        Ext.getStore('TestOrimi.store.MasterStaffStore').addListener('beforeload', this.MasterStaffStoreBeforeLoad, this);
        Ext.getStore('TestOrimi.store.MasterStaffStore').addListener('load', this.MasterStaffStoreAfterLoad, this);

        Ext.getStore('TestOrimi.store.MasterDistrStore').addListener('applyFilters', this.GridStoreApplyFilters, this);
        Ext.getStore('TestOrimi.store.MasterDistrStore').addListener('beforeload', this.MasterDistrStoreBeforeLoad, this);

        Ext.getStore('TestOrimi.store.MasterWarehousesStore').addListener('applyFilters', this.GridStoreApplyFilters, this);
        Ext.getStore('TestOrimi.store.MasterWarehousesStore').addListener('beforeload', this.MasterWarehousesStoreBeforeLoad, this);
        Ext.getStore('TestOrimi.store.MasterWarehousesStore').addListener('load', this.MasterWarehousesStoreAfterLoad, this);

        Ext.getStore('TestOrimi.store.SyncedGoodStore').addListener('applyFilters', this.GridStoreApplyFilters, this);
        Ext.getStore('TestOrimi.store.SyncedGoodStore').addListener('beforeload', this.SyncedGoodStoreBeforeLoad, this);
        Ext.getStore('TestOrimi.store.SyncedGoodStore').addListener('load', this.SyncedGoodStoreAfterLoad, this);

        Ext.getStore('TestOrimi.store.SyncedStoresStore').addListener('applyFilters', this.GridStoreApplyFilters, this);
        Ext.getStore('TestOrimi.store.SyncedStoresStore').addListener('beforeload', this.SyncedStoresStoreBeforeLoad, this);
        Ext.getStore('TestOrimi.store.SyncedStoresStore').addListener('load', this.SyncedStoresStoreAfterLoad, this);

        Ext.getStore('TestOrimi.store.SyncedStaffStore').addListener('applyFilters', this.GridStoreApplyFilters, this);
        Ext.getStore('TestOrimi.store.SyncedStaffStore').addListener('beforeload', this.SyncedStaffStoreBeforeLoad, this);
        Ext.getStore('TestOrimi.store.SyncedStaffStore').addListener('load', this.SyncedStaffStoreAfterLoad, this);

        Ext.getStore('TestOrimi.store.SyncedWarehousesStore').addListener('applyFilters', this.GridStoreApplyFilters, this);
        Ext.getStore('TestOrimi.store.SyncedWarehousesStore').addListener('beforeload', this.SyncedWarehousesStoreBeforeLoad, this);
        Ext.getStore('TestOrimi.store.SyncedWarehousesStore').addListener('load', this.SyncedWarehousesStoreAfterLoad, this);

        Ext.getStore('TestOrimi.store.SlaveGoodStore').addListener('applyFilters', this.GridStoreApplyFilters, this);
        Ext.getStore('TestOrimi.store.SlaveStoresStore').addListener('applyFilters', this.GridStoreApplyFilters, this);
        Ext.getStore('TestOrimi.store.SlaveStaffStore').addListener('applyFilters', this.GridStoreApplyFilters, this);
        Ext.getStore('TestOrimi.store.SlaveWarehousesStore').addListener('applyFilters', this.GridStoreApplyFilters, this);

        Ext.getStore('TestOrimi.store.CityStore').addListener('beforeload', this.CityStoreBeforeLoad, this);
        Ext.getStore('TestOrimi.store.ActiveStoresStore').addListener('beforeload', this.ActiveStoresStoreBeforeLoad, this);

        Ext.getStore('TestOrimi.store.GoodsSyncResultStore').addListener('filterchange', this.GoodsSyncResultStoreFilterChange, this);

        Ext.getStore('TestOrimi.store.AdvertisingBossHeadersStore').addListener('beforeload', this.AdvertisingBossHeadersStoreBeforeLoad, this);

        Ext.getStore('TestOrimi.store.AdvertisingBossRowsStore').addListener('applyFilters', this.GridStoreApplyFilters, this);
        Ext.getStore('TestOrimi.store.AdvertisingBossRowsStore').addListener('beforeload', this.AdvertisingBossRowsStoreBeforeLoad, this);

        Ext.getStore('TestOrimi.store.AdvertisingEventsHeadersStore').addListener('applyFilters', this.GridStoreApplyFilters, this);
        Ext.getStore('TestOrimi.store.AdvertisingEventsHeadersStore').addListener('beforeload', this.AdvertisingEventsHeadersStoreBeforeLoad, this);
        Ext.getStore('TestOrimi.store.AdvertisingEventsHeadersStore').addListener('load', this.AdvertisingEventsHeadersStoreAfterLoad, this);

        Ext.getStore('TestOrimi.store.AdvertisingEventsRowsStore').addListener('applyFilters', this.GridStoreApplyFilters, this);
        Ext.getStore('TestOrimi.store.AdvertisingEventsRowsStore').addListener('beforeload', this.AdvertisingEventsRowsStoreBeforeLoad, this);
        Ext.getStore('TestOrimi.store.AdvertisingEventsRowsStore').addListener('load', this.AdvertisingEventsRowsStoreAfterLoad, this);
    },

    RefLoadCountCheck: function(card, loadCount, storeCount) {
        if (loadCount == storeCount) {
            card.setLoading(false);
            if (card.name == 'attr_ref_card' || card.name == 'adv_ref_card') {
                card.loaded = true;
            }

        }
    },

    onViewportSkuRefButtonClick: function(viewport, button) {
        viewport.down('[name=domCard]').getLayout().setActiveItem(1);
        viewport.down('[name=ref]').setText('Справочник: Номенклатура');
        document.title = 'НСИ::SKU';
        viewport.down('[name=buttonChange]').show();

        var me         = this,
            card       = viewport.down('[name=sku_ref_card]'),
            stores     = [
                'TestOrimi.store.MasterGoodStore', 'TestOrimi.store.GoodParentStore', 'TestOrimi.store.SlaveGoodStore', 'TestOrimi.store.BrandStore', 'TestOrimi.store.CategoryStore', 'TestOrimi.store.CoffeeSortStore', 'TestOrimi.store.CountryStore', 'TestOrimi.store.ExternalPackStore', 'TestOrimi.store.GiftStore', 'TestOrimi.store.GiftTypeStore', 'TestOrimi.store.InternalPackStore', 'TestOrimi.store.ManufacturerStore', 'TestOrimi.store.PriceSegmentStore', 'TestOrimi.store.SachetPackStore', 'TestOrimi.store.SegmentStore', 'TestOrimi.store.SubBrandStore', 'TestOrimi.store.SubSegmentStore', 'TestOrimi.store.TeaColorStore', 'TestOrimi.store.WeightGroupStore', 'TestOrimi.store.AddingStore', 'TestOrimi.store.FlavorStore'
            ],
            storeCount = stores.length,
            loadCount  = 0;

        if (!Ext.getStore(stores[0]).lastOptions) {
            card.setLoading('Загрузка атрибутов...');

            for (var i = 0; i < storeCount; i++) {
                Ext.getStore(stores[i]).load({
                    callback: function(records, operation, success) {
                        loadCount++;
                        me.RefLoadCountCheck(card, loadCount, storeCount);
                    }
                });
            }
        }
    },

    onViewportTTRefButtonClick: function(viewport, button) {
        viewport.down('[name=domCard]').getLayout().setActiveItem(2);
        viewport.down('[name=ref]').setText('Справочник: Торговые точки');
        document.title = 'НСИ::ТТ';
        viewport.down('[name=buttonChange]').show();

        var me         = this,
            card       = viewport.down('[name=tt_ref_card]'),
            stores     = [
                'TestOrimi.store.MasterStoresStore',
                'TestOrimi.store.SectorStore',
                'TestOrimi.store.ChannelStore',
                'TestOrimi.store.SubChannelStore',
                'TestOrimi.store.FormatStore',
                'TestOrimi.store.NetStore',
                'TestOrimi.store.ParentNetStore',
                'TestOrimi.store.NetTypeStore',
                'TestOrimi.store.CountryStore',
                'TestOrimi.store.AreaStore',
                'TestOrimi.store.DivisionStore',
                'TestOrimi.store.TradentDepartmentStore',
                'TestOrimi.store.SlaveStoresStore',
                'TestOrimi.store.ContractorGroupStore',
                'TestOrimi.store.CurrancyStore',
                'TestOrimi.store.CountyStore',
                'TestOrimi.store.RegionStore',
                'TestOrimi.store.DistrictStore',
                'TestOrimi.store.CityStore',
                'TestOrimi.store.ParentStore',
                'TestOrimi.store.AdvertisingChannelStore'
            ],
            storeCount = stores.length,
            loadCount  = 0;

        if (!Ext.getStore(stores[0]).lastOptions) {
            card.setLoading('Загрузка атрибутов...');

            for (var i = 0; i < storeCount; i++) {
                Ext.getStore(stores[i]).load({
                    callback: function(records, operation, success) {
                        loadCount++;
                        me.RefLoadCountCheck(card, loadCount, storeCount);
                    }
                });
            }
        }
    },

    onViewportStaffRefButtonClick: function(viewport, button) {
        viewport.down('[name=domCard]').getLayout().setActiveItem(3);
        document.title = 'НСИ::Сотрудники';
        viewport.down('[name=ref]').setText('Справочник: Сотрудники');
        viewport.down('[name=buttonChange]').show();

        var me         = this,
            card       = viewport.down('[name=staff_ref_card]'),
            stores     = [
                'TestOrimi.store.MasterStaffStore',
                'TestOrimi.store.SectorStore',
                'TestOrimi.store.AreaStore',
                'TestOrimi.store.DivisionStore',
                'TestOrimi.store.TradentDepartmentStore',
                'TestOrimi.store.MotivationStore',
                'TestOrimi.store.CurrancyStore',
                'TestOrimi.store.ParentStore',
                'TestOrimi.store.TypeStaffStore',
                'TestOrimi.store.SlaveStaffStore'
            ],
            storeCount = stores.length,
            loadCount  = 0;

        if (!Ext.getStore(stores[0]).lastOptions) {
            card.setLoading('Загрузка атрибутов...');

            for (var i = 0; i < storeCount; i++) {
                Ext.getStore(stores[i]).load({
                    callback: function(records, operation, success) {
                        loadCount++;
                        me.RefLoadCountCheck(card, loadCount, storeCount);
                    }
                });
            }
        }
    },

    onViewportDistrRefButtonClick: function(viewport, button) {
        viewport.down('[name=domCard]').getLayout().setActiveItem(4);
        viewport.down('[name=ref]').setText('Справочник: Дистрибьюторы');
        document.title = 'НСИ::Дистрибьюторы';
        viewport.down('[name=buttonChange]').show();

        var me         = this,
            card       = viewport.down('[name=distr_ref_card]'),
            stores     = [
                'TestOrimi.store.MasterDistrStore',
                'TestOrimi.store.SectorStore',
                'TestOrimi.store.ChannelStore',
                'TestOrimi.store.SubChannelStore',
                'TestOrimi.store.FormatStore',
                'TestOrimi.store.CountryStore',
                'TestOrimi.store.AreaStore',
                'TestOrimi.store.DivisionStore',
                'TestOrimi.store.TradentDepartmentStore',
                'TestOrimi.store.ContractorGroupStore',
                'TestOrimi.store.CurrancyStore',
                'TestOrimi.store.NonAccReasonStore',
                'TestOrimi.store.CountyStore',
                'TestOrimi.store.RegionStore',
                'TestOrimi.store.DistrictStore',
                'TestOrimi.store.CityStore',
                'TestOrimi.store.ParentStore'
            ],
            storeCount = stores.length,
            loadCount  = 0;

        if (!Ext.getStore(stores[0]).lastOptions) {
            card.setLoading('Загрузка атрибутов...');

            for (var i = 0; i < storeCount; i++) {
                Ext.getStore(stores[i]).load({
                    callback: function(records, operation, success) {
                        loadCount++;
                        me.RefLoadCountCheck(card, loadCount, storeCount);
                    }
                });
            }
        }
    },

    onViewportAttrRefButtonClick: function(viewport, button) {
        viewport.down('[name=domCard]').getLayout().setActiveItem(6);
        viewport.down('[name=ref]').setText('Справочник: Атрибуты');
        document.title = 'НСИ::Атрибуты';
        viewport.down('[name=buttonChange]').show();

        var me         = this,
            userRights = TestOrimi.getApplication().data,
            card       = viewport.down('[name=attr_ref_card]');

        var stores = [];

        for (var key in userRights) {
            if (userRights.hasOwnProperty(key)) {
                if (key.match(/^Attributes_/) && parseInt(userRights[key], 10) >= 1) {
                    stores.push('TestOrimi.store.' + key.replace('Attributes_', '') + 'Store');
                    if (key === 'Attributes_Net') {
                        stores.push('TestOrimi.store.ParentStore');
                        stores.push('TestOrimi.store.ParentNetStore');
                    }

                    if (key === 'Attributes_Motivation') {
                        stores.push('TestOrimi.store.CurrancyStore');
                    }

                    if (key === 'Attributes_Division') {
                        stores.push('TestOrimi.store.TradentDepartmentStore');
                    }
                }
            }
        }

        /*stores = [
         'TestOrimi.store.BrandStore'
         , 'TestOrimi.store.CategoryStore'
         , 'TestOrimi.store.CoffeeSortStore'
         , 'TestOrimi.store.ExternalPackStore'
         , 'TestOrimi.store.GiftStore'
         , 'TestOrimi.store.GiftTypeStore'
         , 'TestOrimi.store.InternalPackStore'
         , 'TestOrimi.store.ManufacturerStore'
         , 'TestOrimi.store.PriceSegmentStore'
         , 'TestOrimi.store.SachetPackStore'
         , 'TestOrimi.store.SegmentStore'
         , 'TestOrimi.store.SubBrandStore'
         , 'TestOrimi.store.SubSegmentStore'
         , 'TestOrimi.store.TeaColorStore'
         , 'TestOrimi.store.WeightGroupStore'
         , 'TestOrimi.store.AddingStore'
         , 'TestOrimi.store.FlavorStore'
         , 'TestOrimi.store.SectorStore'
         , 'TestOrimi.store.ChannelStore'
         , 'TestOrimi.store.SubChannelStore'
         , 'TestOrimi.store.FormatStore'
         , 'TestOrimi.store.NetStore'
         , 'TestOrimi.store.NetTypeStore'
         , 'TestOrimi.store.CountryStore'
         , 'TestOrimi.store.DivisionStore'
         , 'TestOrimi.store.CurrancyStore'
         , 'TestOrimi.store.NonAccReasonStore'
         , 'TestOrimi.store.CountyStore'
         , 'TestOrimi.store.RegionStore'
         , 'TestOrimi.store.DistrictStore'
         , 'TestOrimi.store.CityStore'
         , 'TestOrimi.store.MotivationStore'
         , 'TestOrimi.store.TypeStaffStore'
         , 'TestOrimi.store.ParentStore'
         ],*/
        var storeCount = stores.length,
            loadCount  = 0;

        if (!card.loaded && storeCount > 0) {
            card.setLoading('Загрузка атрибутов...');

            for (var i = 0; i < storeCount; i++) {
                Ext.getStore(stores[i]).load({
                    callback: function(records, operation, success) {
                        loadCount++;
                        me.RefLoadCountCheck(card, loadCount, storeCount);
                    }
                });
            }
        }

    },

    onViewportWrhsRefButtonClick: function(viewport, button) {
        viewport.down('[name=domCard]').getLayout().setActiveItem(5);
        viewport.down('[name=ref]').setText('Справочник: Склады');
        document.title = 'НСИ::Склады';
        viewport.down('[name=buttonChange]').show();

        var me         = this,
            card       = viewport.down('[name=wrhs_ref_card]'),
            stores     = [
                'TestOrimi.store.MasterWarehousesStore',
                'TestOrimi.store.SectorStore',
                'TestOrimi.store.FormatStore',
                'TestOrimi.store.CountryStore',
                'TestOrimi.store.AreaStore',
                'TestOrimi.store.DivisionStore',
                'TestOrimi.store.CountyStore',
                'TestOrimi.store.RegionStore',
                'TestOrimi.store.DistrictStore',
                'TestOrimi.store.CityStore',
                'TestOrimi.store.DistributorsStore',
                'TestOrimi.store.ContractorGroupStore',
                'TestOrimi.store.SlaveWarehousesStore'
            ],
            storeCount = stores.length,
            loadCount  = 0;

        if (!Ext.getStore(stores[0]).lastOptions) {
            card.setLoading('Загрузка атрибутов...');

            for (var i = 0; i < storeCount; i++) {
                Ext.getStore(stores[i]).load({
                    callback: function(records, operation, success) {
                        loadCount++;
                        me.RefLoadCountCheck(card, loadCount, storeCount);
                    }
                });
            }
        }
    },

    onViewportAdvRefButtonClick: function(viewport, button) {
        viewport.down('[name=domCard]').getLayout().setActiveItem(7);
        viewport.down('[name=ref]').setText('Раздел: Реклама');
        document.title = 'НСИ::Реклама';
        viewport.down('[name=buttonChange]').show();
    },

    onColumnSearchChange: function(grid, field, newValue, oldValue) {
        var filters = grid.store.filters.items,
            i       = 0,
            length  = filters.length;

        if (length) {
            for (; i < length; i++) {
                if (filters[i].property == field.up('gridcolumn').dataIndex) {
                    grid.store.filters.removeAt(i);
                    break;
                }
            }
        }

        if (newValue) {
            grid.store.filter(field.up('gridcolumn').dataIndex, newValue);
            field.up('gridcolumn').addCls('ux-filtered-column');
        } else {
            field.up('gridcolumn').removeCls('ux-filtered-column');
            grid.store.load();
        }

        this.checkMasterSearchStatus(grid);
    },

    onWindowClearFilter: function(win) {
        var field   = win.field,
            store   = win.down('grid').store,
            filters = store.filters.items,
            length  = filters.length,
            i       = length - 1;

        if (length) {
            for (; i >= 0; i--) {
                if (!filters[i].id) {
                    store.filters.removeAt(i);
                }
            }
            if (store.filters.items.length) {
                //set deleted filter back
                for (i = 0; i < store.filters.items.length; i++) {
                    if (store.filters.items[i].property == 'Deleted') {
                        store.filters.items[i].setValue(0);
                    }

                    if (store.filters.items[i].property == 'StatusID') {
                        store.filters.items[i].setValue(2);
                    }

                    if (store.filters.items[i] && store.filters.items[i].hasOwnProperty('defaultValue')) {
                        store.filters.items[i].setValue(store.filters.items[i].defaultValue);
                    }
                }
            }
            if (!store.remoteFilter) {
                if (store.filters.items.length) {
                    store.filter();
                } else {
                    store.clearFilter();
                }
            }
        }

        if (field) {
            if (field.attribute == 'City') {
                if (!field.store.findRecord('ID', field.getValue(), 0, false, false, true)) {
                    field.store.load({
                        params: {
                            ID: field.getValue()
                        }
                    });
                }
            } else if (field.attribute == 'Store') {
                if (!field.store.findRecord('ID', field.getValue(), 0, false, false, true)) {
                    field.store.load({
                        params: {
                            ID: field.getValue()
                        }
                    });
                }
            }
        }
    },

    onTTWindowBeforeDestroy: function(win) {
        this.clearModalFilters(Ext.getStore('TestOrimi.store.CityStore'));
    },

    onCreateAdvertisingEventWindowBeforeDestroy: function(win) {
        var store = Ext.getStore('TestOrimi.store.ActiveStoresStore');

        var filters = store.filters.items,
            length  = filters.length,
            i       = length - 1;

        if (length) {
            for (; i >= 0; i--) {
                if (filters[i].id) {
                    store.filters.removeAt(i);
                }
            }
        }
    },

    clearModalFilters: function(store) {
        var filters = store.filters.items,
            length  = filters.length,
            i       = length - 1;

        if (length) {
            for (; i >= 0; i--) {
                if (filters[i].id) {
                    if (filters[i].id.indexOf('modalcombo') === 0)
                        store.filters.removeAt(i);
                }
            }
        }
    },

    onAttributeWindowAfterRender: function(win) {
        var field = win.field,
            grid  = win.down('grid');

        if (field) {
            this.AttributeStoreApplyFilters(field.store, grid);

            if (field.value) {
                var index = field.store.find('ID', field.value, 0, false, true);
                if (index >= 0) {
                    grid.getView().focus();
                    grid.getSelectionModel().select(index);
                    /*grid.getView().refresh();
                     var node = grid.getView().getNode(index);
                     //console.dir(node);
                     grid.getView().focusNode(node);
                     Ext.fly(node).scrollIntoView(grid.getView().el);
                     //console.dir();*/
                }
            }
        }
    },

    showNotification: function(text, type) {
        //var winError, - Ошибка
        //    winSuccess, - Успех
        //    winInformation; - Уведомление

        Ext.create('widget.uxNotification', {
            position: 'br',
            cls: 'ux-notification-' + type,
            maxWidth: 300,
            resizable: false,
            html: text,
            closable: false,
            slideInAnimation: 'bounceOut',
            slideBackAnimation: 'easeIn',
            /*
             listeners: {
             focus: function () {
             console.dir('true');
             //this.up('widget').destroy();
             }
             }
             */
            stickOnClick: false
        }).show();

        //TODO win.destroy
    },

    onGeneralGridColumnShow: function(ct, column) {
        if (!column.isCheckerHd) {
            var grid      = ct.ownerCt,
                textfield = column.down('textfield'),
                container = textfield.up('container'),
                filters   = grid.store.filters.items,
                i         = 0,
                length    = filters.length;

            container.show();
            column.removeCls('ux-filtered-column');
            textfield.suspendEvents();
            textfield.reset();
            if (length) {
                for (; i < length; i++) {
                    if (filters[i].property == column.dataIndex) {
                        column.addCls('ux-filtered-column');
                        textfield.setValue(filters[i].value);
                        break;
                    }
                }
            }
            textfield.resumeEvents();
            textfield.show();
        }
    },

    checkMasterSearchStatus: function(grid) {
        var store = grid.store;
        var searchButton = grid.down('[name=combSearch]');

        if (!searchButton) {
            return;
        }

        if (store.filters.items.length || store.proxy.extraParams['searchall'].length) {
            grid.down('[name=combSearch]').setText('<span class="oi oiSearch" data-glyph="magnifying-glass" aria-hidden="true"></span>');
        } else {
            grid.down('[name=combSearch]').setText('<span class="oi" data-glyph="magnifying-glass" aria-hidden="true"></span>');
        }
    },

    onGeneralGridSelectionChange: function(selModel, selected) {
        if (!this.getGeneral_tabpanelFirstcol().down('pagingtoolbar').down('[name=syncedShowAll]').getValue()) {
            if (selected.length)
                Ext.getStore('TestOrimi.store.SyncedGoodStore').loadPage(1);
            else
                Ext.getStore('TestOrimi.store.SyncedGoodStore').removeAll();
        }
    },

    onGeneralGridTTSelectionChange: function(selModel, selected) {
        if (selected.length)
            Ext.getStore('TestOrimi.store.SyncedStoresStore').loadPage(1);
        else
            Ext.getStore('TestOrimi.store.SyncedStoresStore').removeAll();
    },

    onGeneralGridStaffSelectionChange: function(selModel, selected) {
        if (selected.length)
            Ext.getStore('TestOrimi.store.SyncedStaffStore').loadPage(1);
        else
            Ext.getStore('TestOrimi.store.SyncedStaffStore').removeAll();
    },

    onGeneralGridWrhsSelectionChange: function(selModel, selected) {
        if (selected.length)
            Ext.getStore('TestOrimi.store.SyncedWarehousesStore').loadPage(1);
        else
            Ext.getStore('TestOrimi.store.SyncedWarehousesStore').removeAll();
    },

    onGeneral_tabpanelFirstcolUnsyncButtonClick: function(grid, rowIndex, colIndex) {
        var rec = grid.getStore().getAt(rowIndex),
            ref = grid.down('[xtype=general_tabpanelLbar]').ref;

        Ext.Msg.confirm('Отвязать', 'Отвязать позицию <strong>' + rec.get('Name') + '</strong>?',
            function(btn, text) {
                if (btn == 'yes') {
                    var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Обработка...'});
                    var tabpanel;
                    if (ref == 'Sku') {
                        ref = 'Goods';
                        tabpanel = 'general_tabpanel';
                    } else if (ref == 'TT') {
                        ref = 'Stores';
                        tabpanel = 'general_tabpanelTT';
                    } else if (ref == 'Staff') {
                        ref = 'Staff';
                        tabpanel = 'general_tabpanelStaff';
                    } else if (ref == 'Wrhs') {
                        ref = 'Warehouses';
                        tabpanel = 'wrhs_general_tabpanel';
                    }

                    myMask.show();
                    Ext.Ajax.request({
                        url: 'resources/data/api.php',
                        timeout: 600000,
                        params: {
                            ID: rec.get('ID'),
                            act: 'getSynced' + ref,
                            subaction: 'unsync'
                        },
                        success: function(response) {
                            myMask.destroy();
                            var responseText = Ext.JSON.decode(response.responseText);

                            if (!responseText.success) {
                                TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + responseText.msg, 'winError');
                            } else {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Позиция отвязана', 'winSuccess');
                                grid.getStore().load();
                                grid.up(tabpanel).items.items[1].getStore().load();
                            }
                        },
                        failure: function(form, action) {
                            myMask.destroy();
                            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сервер не отвечает на запрос', 'winError');
                        }
                    });
                }
            }
        );
    },

    onGeneral_tabpanelFirstcolReportButtonClick: function(button, ref) {
        var me = this;

        Ext.Msg.show({
            title: 'Отчет о синхронизации',
            msg: 'Сформировать отчет о синхронизации?',
            buttons: Ext.Msg.YESNO,
            fn: function(btn) {
                if (btn === 'yes') {
                    var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Формируется файл...'}),
                        masterGrid, slaveGrid, i;

                    myMask.show();
                    if (ref === 'Sku') {
                        ref = 'Goods';
                        masterGrid = me.getGeneral_grid();
                        slaveGrid = me.getGeneral_tabpanelFirstcol();
                    } else if (ref === 'TT') {
                        ref = 'Stores';
                        masterGrid = me.getGeneral_gridTT();
                        slaveGrid = me.getGeneral_tabpanelFirstcolTT();
                    } else if (ref === 'Staff') {
                        ref = 'Staff';
                        masterGrid = me.getGeneral_gridStaff();
                        slaveGrid = me.getGeneral_tabpanelFirstcolStaff();
                    } else if (ref === 'Wrhs') {
                        ref = 'Warehouses';
                        masterGrid = me.getWrhs_general_grid();
                        slaveGrid = me.getWrhs_general_tabpanelFirstcol();
                    }

                    var masterColumns = masterGrid.view.headerCt.getVisibleGridColumns(),
                        slaveColumns  = slaveGrid.view.headerCt.getVisibleGridColumns(),
                        columns       = [];

                    for (i = 0; i < masterColumns.length; i++) {
                        if (masterColumns[i]['dataIndex'])
                            columns.push('master' + masterColumns[i]['dataIndex']);
                    }

                    for (i = 0; i < slaveColumns.length; i++) {
                        if (slaveColumns[i]['dataIndex'])
                            columns.push('slave' + slaveColumns[i]['dataIndex']);
                    }

                    var masterFilters = Ext.Array.map(masterGrid.getStore().filters.items,
                        function(item) {
                            var copy = {
                                property: item.property,
                                value: item.value
                            };

                            copy.property = 'master' + copy.property;
                            return copy;
                        }
                    );

                    var slaveFilters = Ext.Array.map(slaveGrid.getStore().filters.items,
                        function(item) {
                            var copy = {
                                property: item.property,
                                value: item.value
                            };

                            copy.property = 'slave' + copy.property;
                            return copy;
                        }
                    );

                    Ext.Ajax.request({
                        url: 'resources/data/api.php',
                        timeout: 600000,
                        params: {
                            act: 'getSyncReport',
                            ref: ref,
                            columns: columns.toString(),
                            masterFilters: me.encodeStoreFiltersString(masterFilters),
                            slaveFilters: me.encodeStoreFiltersString(slaveFilters)
                        },
                        success: function(response) {
                            var response       = Ext.JSON.decode(response.responseText),
                                hiddenIFrameID = 'hiddenDownloader',
                                iframe         = document.getElementById(hiddenIFrameID);

                            if (iframe === null) {
                                iframe = document.createElement('iframe');
                                iframe.id = hiddenIFrameID;
                                iframe.style.display = 'none';
                                document.body.appendChild(iframe);
                            }
                            iframe.src = 'resources/data/api.php?act=getFile&file=' + response['name'];

                            myMask.destroy();
                        },
                        failure: function(form, action) {
                            myMask.destroy();
                            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                        }
                    });
                }
            }
        });
    },

    onGeneral_tabpanelFirstcolCelldblclick: function(view, td, cellIndex, record, tr, rowIndex) {
        var masterGrid         = this.getViewport().down('[name=domCard]').getLayout().getActiveItem().down('grid'),
            slaveGridShowAllCb = view.ownerCt.down('pagingtoolbar').down('[name=syncedShowAll]'),
            arr                = [];

        arr.push({
            property: 'ID',
            value: '=' + record.get('MasterID')
        });

        if (record.get('MasterType') === 0)
            arr.push({
                property: 'isNetName',
                value: '=ТТ'
            });
        else if (record.get('MasterType') === 1)
            arr.push({
                property: 'isNetName',
                value: '=КСР'
            });
        else if (record.get('MasterType') === 2)
            arr.push({
                property: 'isNetName',
                value: '=Дистрибьютор'
            });

        slaveGridShowAllCb.suspendEvents(false);
        slaveGridShowAllCb.setValue(false);
        slaveGridShowAllCb.resumeEvents();
        this.GridStoreApplyFilters(masterGrid.getStore(), arr, masterGrid);
    },

    onGeneral_tabpanelFirstcolGroupUnsyncButtonClick: function(grid) {
        var selected = grid.getSelectionModel().getSelection(),
            ref      = grid.down('[xtype=general_tabpanelLbar]').ref,
            namePos  = '',
            tabpanel;

        if (ref == 'Sku') {
            ref = 'Goods';
            namePos = 'Name';
        } else if (ref == 'TT') {
            ref = 'Stores';
            namePos = 'Name';
        } else if (ref == 'Staff') {
            ref = 'Staff';
            namePos = 'FIO';
        } else if (ref == 'Wrhs') {
            ref = 'Warehouses';
            namePos = 'Name';
        }

        if (selected.length) {
            var msg = selected[0].get(namePos),
                ids = [];

            ids.push(selected[0].get('ID'));
            for (var i = 1; i < selected.length; i++) {
                msg += '<br>' + selected[i].get(namePos);
                ids.push(selected[i].get('ID'));
            }

            Ext.Msg.confirm('Отвязать', 'Отвязать выбранные позиции?<br><table><tr><td><strong>' + msg + '</strong></td></tr></table>',
                function(btn, text) {
                    if (btn == 'yes') {
                        var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Обработка...'});
                        if (ref == 'Sku') {
                            ref = 'Goods';
                            tabpanel = 'general_tabpanel';
                        } else if (ref == 'TT') {
                            ref = 'Stores';
                            tabpanel = 'general_tabpanelTT';
                        } else if (ref == 'Staff') {
                            ref = 'Staff';
                            tabpanel = 'general_tabpanelStaff';
                        } else if (ref == 'Wrhs') {
                            ref = 'Warehouses';
                            tabpanel = 'wrhs_general_tabpanel';
                        }

                        myMask.show();
                        Ext.Ajax.request({
                            url: 'resources/data/api.php',
                            timeout: 600000,
                            params: {
                                ID: ids.toString(),
                                act: 'getSynced' + ref,
                                subaction: 'groupunsync'
                            },
                            success: function(response) {
                                myMask.destroy();

                                var responseText = Ext.JSON.decode(response.responseText);

                                if (!responseText.success) {
                                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + responseText.msg, 'winError');
                                    return;
                                }

                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Позиции отвязаны', 'winSuccess');
                                grid.getStore().load();
                            },
                            failure: function(form, action) {
                                myMask.destroy();
                                TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сервер не отвечает на запрос', 'winError');
                            }
                        });
                    }
                }
            );
        } else {
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Выберите позиции подчиненного справочника для отвязки', 'winError');
        }
    },

    onGeneral_tabpanelSecondcolSyncButtonClick: function(grid, rowIndex, colIndex) {
        var rec          = grid.getStore().getAt(rowIndex),
            ref          = grid.down('[xtype=general_tabpanelsecLbar]').ref,
            gridSelected = '',
            namePos      = '',
            tabpanel     = '';

        if (ref == 'Sku') {
            var masterGrid     = grid.up('[name=sku_ref_card]').down('general_grid'),
                masterSelected = masterGrid.getSelectionModel().getSelection()[0];

            ref = 'Goods';
            gridSelected = masterSelected;
            namePos = 'Name';
            tabpanel = 'general_tabpanel';

        } else if (ref == 'TT') {
            var storesGrid     = grid.up('[name=tt_ref_card]').down('general_gridTT'),
                storesSelected = storesGrid.getSelectionModel().getSelection()[0];

            ref = 'Stores';
            gridSelected = storesSelected;
            namePos = 'Name';
            tabpanel = 'general_tabpanelTT';

        } else if (ref == 'Staff') {
            var staffGrid     = grid.up('[name=staff_ref_card]').down('general_gridStaff'),
                staffSelected = staffGrid.getSelectionModel().getSelection()[0];

            ref = 'Staff';
            gridSelected = staffSelected;
            namePos = 'FIO';
            tabpanel = 'general_tabpanelStaff';

        } else if (ref == 'Wrhs') {
            var wrhsGrid     = grid.up('[name=wrhs_ref_card]').down('wrhs_general_grid'),
                wrhsSelected = wrhsGrid.getSelectionModel().getSelection()[0];

            ref = 'Warehouses';
            gridSelected = wrhsSelected;
            namePos = 'Name';
            tabpanel = 'wrhs_general_tabpanel';
        }

        if (gridSelected) {
            if (gridSelected.get('StatusID') == 2) {
                //Cделать проверку в НСИ - при попытке привязать к мастер-записи точку, где источник ="Трайдент" и группа контрагентов <> "Объекты мониторинга*", проверяется, стоит ли в мастер-записи галка грузополучатель="да", и если не стоит, то не осуществлять привязку и выводить сообщение "Некорректная информация! Вы синхронизируете грузополучателя ОРИМИ. Требуется указать группу контрагентов в мастер-точке"
                if (ref == 'Stores' && gridSelected.get('isConsignee') != 1) {
                    if (rec.get('DataSourceID') == 1 && rec.get('ContractorGroup').indexOf('Объекты мониторинга') == -1) {
                        TestOrimi.getApplication().getController('Main').showNotification('Некорректная информация! Вы синхронизируете грузополучателя ОРИМИ. Требуется указать группу контрагентов в мастер-точке. Ошибка в точке подчиненного справочника с ID:' + rec.get('ID'), 'winError');
                        return;
                    }
                }
                Ext.Msg.confirm('Привязать', 'Привязать позиции?<br><table><tr><td>Мастер справочник:</td><td><strong>' + gridSelected.get(namePos) + '</strong></td></tr><tr><td>Подчиненный справочник:</td><td><strong>' + rec.get('Name') + '</strong></td></tr></table>', //TODO change 'Мастер справочник:'
                    function(btn, text) {
                        if (btn == 'yes') {
                            var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Обработка...'}),
                                params = {};

                            params = {
                                ID: rec.get('ID'),
                                MasterID: gridSelected.get('ID'),
                                act: 'getSlave' + ref,
                                subaction: 'sync'
                            };

                            if (ref == 'Stores') {
                                params['isNet'] = gridSelected.get('isNet');
                            }

                            myMask.show();
                            Ext.Ajax.request({
                                url: 'resources/data/api.php',
                                timeout: 600000,
                                params: params,
                                success: function(response) {
                                    myMask.destroy();
                                    TestOrimi.getApplication().getController('Main').showNotification('Успешно! Позиция привязана', 'winSuccess');
                                    grid.getStore().load(); //"TestOrimi.store.SlaveGoodStore"
                                    grid.up(tabpanel).items.items[0].getStore().load(); //"TestOrimi.store.SyncedGoodStore"
                                },
                                failure: function(form, action) {
                                    myMask.destroy();
                                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                                }
                            });
                        }
                    }
                );
            } else {
                TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Позиция не является активной', 'winError');
            }
        } else {
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Выберите позицию мастер-справочника для привязки', 'winError');
        }
    },

    onGeneral_tabpanelSecondcolGroupSyncButtonClick: function(grid) { //TODO for all grids
        var selected = grid.getSelectionModel().getSelection(),
            //masterGrid = grid.up('[name=sku_ref_card]').down('general_grid'),
            //masterSelected = masterGrid.getSelectionModel().getSelection()[0];
            ref      = grid.down('[xtype=general_tabpanelsecLbar]').ref,
            gridSelected,
            namePos  = '',
            tabpanel;

        if (ref == 'Sku') {
            var masterGrid     = grid.up('[name=sku_ref_card]').down('general_grid'),
                masterSelected = masterGrid.getSelectionModel().getSelection()[0];

            ref = 'Goods';
            gridSelected = masterSelected;
            namePos = 'Name';
            tabpanel = 'general_tabpanel';

        } else if (ref == 'TT') {
            var storesGrid     = grid.up('[name=tt_ref_card]').down('general_gridTT'),
                storesSelected = storesGrid.getSelectionModel().getSelection()[0];

            ref = 'Stores';
            gridSelected = storesSelected;
            namePos = 'Name';
            tabpanel = 'general_tabpanelTT';

        } else if (ref == 'Staff') {
            var staffGrid     = grid.up('[name=staff_ref_card]').down('general_gridStaff'),
                staffSelected = staffGrid.getSelectionModel().getSelection()[0];

            ref = 'Staff';
            gridSelected = staffSelected;
            namePos = 'FIO';
            tabpanel = 'general_tabpanelStaff';

        } else if (ref == 'Wrhs') {
            var wrhsGrid     = grid.up('[name=wrhs_ref_card]').down('wrhs_general_grid'),
                wrhsSelected = wrhsGrid.getSelectionModel().getSelection()[0];

            ref = 'Warehouses';
            gridSelected = wrhsSelected;
            namePos = 'Name';
            tabpanel = 'wrhs_general_tabpanel';
        }

        if (selected.length) {
            if (gridSelected) {
                if (gridSelected.get('StatusID') == 2) {
                    //Cделать проверку в НСИ - при попытке привязать к мастер-записи точку, где источник ="Трайдент" и группа контрагентов <> "Объекты мониторинга*", проверяется, стоит ли в мастер-записи галка грузополучатель="да", и если не стоит, то не осуществлять привязку и выводить сообщение "Некорректная информация! Вы синхронизируете грузополучателя ОРИМИ. Требуется указать группу контрагентов в мастер-точке"
                    if (ref == 'Stores' && gridSelected.get('isConsignee') != 1) {
                        for (var i = 0; i < selected.length; i++) {
                            if (selected[i].get('DataSourceID') == 1 && selected[i].get('ContractorGroup').indexOf('Объекты мониторинга') == -1) {
                                TestOrimi.getApplication().getController('Main').showNotification('Некорректная информация! Вы синхронизируете грузополучателя ОРИМИ. Требуется указать группу контрагентов в мастер-точке. Ошибка в точке подчиненного справочника с ID:' + selected[i].get('ID'), 'winError');
                                return;
                            }
                        }
                    }

                    var msg = selected[0].get(namePos),
                        ids = [];

                    ids.push(selected[0].get('ID'));
                    for (var i = 1; i < selected.length; i++) {
                        msg += '<br>' + selected[i].get(namePos);
                        ids.push(selected[i].get('ID'));
                    }
                    Ext.Msg.confirm('Привязать', 'Привязать позиции?<br><table><tr><td>Мастер справочник:</td><td><strong>' + gridSelected.get(namePos) + '</strong></td></tr><tr><td style="border-top: 1px solid grey; vertical-align: top;">Подчиненный справочник:</td><td style="border-top: 1px solid grey;"><strong>' + msg + '</strong></td></tr></table>',
                        function(btn, text) {
                            if (btn == 'yes') {
                                var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Обработка...'}),
                                    params = {};

                                params = {
                                    ID: ids.toString(),
                                    MasterID: gridSelected.get('ID'),
                                    act: 'getSlave' + ref,
                                    subaction: 'groupSync'
                                };

                                if (ref == 'Stores') {
                                    params['isNet'] = gridSelected.get('isNet');
                                }

                                myMask.show();
                                Ext.Ajax.request({
                                    url: 'resources/data/api.php',
                                    timeout: 600000,
                                    params: params,
                                    success: function(response) {
                                        myMask.destroy();
                                        TestOrimi.getApplication().getController('Main').showNotification('Успешно! Позиции привязаны', 'winSuccess');
                                        grid.getStore().load(); //"TestOrimi.store.SlaveGoodStore"
                                        grid.up(tabpanel).items.items[0].getStore().load(); //"TestOrimi.store.SyncedGoodStore"
                                    },
                                    failure: function(form, action) {
                                        myMask.destroy();
                                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                                    }
                                });
                            }
                        }
                    );
                } else {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Позиция не является активной', 'winError');
                }
            } else {
                TestOrimi.getApplication().getController('Main').showNotification('Выберите позицию мастер-справочника для привязки!', 'winInformation');
            }
        } else {
            TestOrimi.getApplication().getController('Main').showNotification('Выберите позиции для привязки!', 'winInformation');
        }
    },

    getArrayColumns: function(type) {
        var grid;

        switch (type) {
            case 'main' :
                grid = this.getViewport().down('[name=domCard]').getLayout().getActiveItem().down('grid');
                break;
            case 'slaveL' :
                grid = this.getViewport().down('[name=domCard]').getLayout().getActiveItem().down('[title=Отвязать]');
                break;
            case 'slaveR' :
                grid = this.getViewport().down('[name=domCard]').getLayout().getActiveItem().down('[title=Привязать]');
                break;
        }

        grid.getView().refresh();
        var state,
            columns = grid.view.headerCt.getGridColumns(),
            //columns = grid.columns,
            i       = 0,
            length  = columns.length,
            newarr  = [];

        //console.dir(columns);
        for (; i < length; i++) {
            newarr[i] = {};
            if (columns[i].hidden) {
                newarr[i].hidden = columns[i]['hidden'];
            } else {
                newarr[i].hidden = false;

            }
            newarr[i].dataIndex = columns[i]['dataIndex'];
            newarr[i].width = columns[i]['width'];
        }

        state = {
            columns: newarr,
            sorters: grid.store.sorters
        };

        return Ext.JSON.encode(state);
    },

    onTemplateSettingsSaveButtonClick: function(win, ref) {
        var me = this;

        Ext.Msg.prompt('Создать шаблон?', 'Введите название:',
            function(btn, text, cfg) {
                if (btn == 'ok') {
                    if (Ext.isEmpty(text)) {
                        var emptyInput = '<span style="color:red">Введите название:</span>';
                        Ext.Msg.show(Ext.apply({}, {msg: emptyInput}, cfg));
                    } else {
                        var myMask = new Ext.LoadMask(win, {msg: 'Обработка...'});
                        myMask.show();

                        Ext.Ajax.request({
                            url: 'resources/data/api.php',
                            timeout: 600000,
                            params: {
                                State: me.getArrayColumns('main'),
                                StateSlaveLeft: me.getArrayColumns('slaveL'),
                                StateSlaveRight: me.getArrayColumns('slaveR'),
                                act: 'getTemplate',
                                Ref: ref,
                                subaction: 'create',
                                Name: text
                            },
                            success: function(value, action) {
                                myMask.destroy();
                                TestOrimi.getApplication().getController('Main').showNotification('Готово! Шаблон успешно сохранен', 'winSuccess');
                                Ext.getStore('TestOrimi.store.TemplateStore').load({
                                    params: {
                                        Ref: ref
                                    }
                                });
                            },
                            failure: function(value, action) {
                                myMask.destroy();
                                TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сервер не отвечает на запрос', 'winError');
                            }
                        });
                    }
                }
            }
        );
    },

    onTemplateSettingsLoadButtonClick: function(win, ref) { //function(win)
        //console.dir(ref);
        var selectedRecord = win.down('grid').getSelectionModel().getSelection()[0],
            grid, slaveL, slaveR;
        if (selectedRecord) {
            switch (ref) {
                case 'Sku' :
                    grid = this.getGeneral_grid();
                    slaveL = this.getViewport().down('general_tabpanelFirstcol');
                    slaveR = this.getViewport().down('general_tabpanelSecondcol');
                    break;
                case 'TT' :
                    grid = this.getGeneral_gridTT();
                    slaveL = this.getViewport().down('general_tabpanelFirstcolTT');
                    slaveR = this.getViewport().down('general_tabpanelSecondcolTT');
                    break;
                case 'Staff' :
                    grid = this.getGeneral_gridStaff();
                    slaveL = this.getViewport().down('general_tabpanelFirstcolStaff');
                    slaveR = this.getViewport().down('general_tabpanelSecondcolStaff');
                    break;
            }
            this.applyPortalColumnsState(grid, Ext.JSON.decode(selectedRecord.get('State')));
            this.applyPortalColumnsState(slaveL, Ext.JSON.decode(selectedRecord.get('StateSlaveLeft')));
            this.applyPortalColumnsState(slaveR, Ext.JSON.decode(selectedRecord.get('StateSlaveRight')));
            win.destroy();
        } else {
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Выберите шаблон', 'winError'); //QUESTION
        }
    },

    onManagersWindowdeletebuttonclick: function(win, grid) {
        var me       = this,
            selected = grid.getSelectionModel().getSelection()[0];

        if (selected) {
            Ext.Msg.confirm('Удаление', 'Удалить менеджера?',
                function(btn, text) {
                    if (btn == 'yes') {
                        var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Удаляю...'});
                        myMask.show();

                        Ext.Ajax.request({
                            url: 'resources/data/api.php',
                            timeout: 600000,
                            params: {
                                ID: selected.get('ID'),
                                act: 'getManagers',
                                subaction: 'delete'
                            },
                            success: function(response) {
                                myMask.destroy();
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Менеджер удален', 'winSuccess');
                                Ext.getStore('TestOrimi.store.ManagersStore').load({
                                    params: {
                                        StoresID: ID
                                        ,
                                        isNet: isNet
                                    }
                                });
                            },
                            failure: function(form, action) {
                                myMask.destroy();
                                TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сервер не отвечает на запрос', 'winError');
                            }
                        });
                    }
                });
        } else {
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Позиция не выделена', 'winError');
        }
    },

    onTemplateSettingsDeleteButtonClick: function(win, grid, ref) {
        var me       = this,
            selected = grid.getSelectionModel().getSelection()[0];
        //console.dir(selected.get('ID'));
        if (selected) {
            Ext.Msg.confirm('Удаление', 'Удалить шаблон?',
                function(btn, text) {
                    if (btn == 'yes') {
                        var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Удаляю...'});
                        myMask.show();

                        Ext.Ajax.request({
                            url: 'resources/data/api.php',
                            timeout: 600000,
                            params: {
                                ID: selected.get('ID'),
                                //Ref: ref,
                                act: 'deleteTemplate'
                            },
                            success: function(response) {
                                myMask.destroy();
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Шаблон удален', 'winSuccess');
                                Ext.getStore('TestOrimi.store.TemplateStore').load({
                                    params: {
                                        Ref: ref
                                    }
                                });
                            },
                            failure: function(form, action) {
                                myMask.destroy();
                                TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сервер не отвечает на запрос', 'winError');
                            }
                        });
                    }
                });
        } else
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Позиция не выделена', 'winError');
    },

    applyPortalColumnsState: function(grid, state) {
        var columns         = state.columns,
            length          = columns.length,
            headerCt        = grid.view.headerCt,
            items           = headerCt.items.items,
            itemsCollection = headerCt.items,
            count           = items.length,
            i               = 0,
            c, col, columnState, index;

        for (c = 0; c < length; c++) {
            columnState = columns[c];
            for (index = count; index--;) {
                col = headerCt.items.items[index];
                if (col.dataIndex == columnState.dataIndex) {
                    if (i !== index) {
                        try {
                            headerCt.moveHeader(index, i);
                            //item = itemsCollection.removeAt(index);
                            //itemsCollection.insert(i, item);
                        } catch (e) {
                            //console.trace(e);
                        }
                    }

                    if (col.applyColumnState) {
                        if (columnState.hidden) {
                            col.hide();
                        } else {
                            col.show();
                        }
                    }
                    ++i;
                    break;
                }
            }
        }

        grid.getView().headerCt.doLayout();
        //grid.getView().doComponentLayout();
        grid.getView().refresh();

        if (state.sorters) {
            var sorter = state.sorters.items[0];
            if (sorter)
                grid.store.sorters.add(new Ext.util.Sorter({
                    property: sorter.property,
                    direction: sorter.direction
                }));
        }
    },

    onBarcodePanelAddButtonClick: function(container) {
        var me = this;
        Ext.Msg.prompt('Добавление штрихкода', 'Введите штрихкод:',
            function(btn, text, cfg) {
                if (btn == 'ok') {
                    if (Ext.isEmpty(text)) {
                        var emptyInput = '<span style="color:red">Введите штрихкод:</span>';

                        Ext.Msg.show(Ext.apply({}, {msg: emptyInput}, cfg));
                    } else {
                        var store = Ext.getStore('TestOrimi.store.BarcodeStore');
                        if (store.find('ID', text) < 0)
                            store.add({
                                ID: text
                            });
                    }
                }
            }
        );
    },

    onBarcodePanelDeleteButtonClick: function(list) {
        var store    = Ext.getStore('TestOrimi.store.BarcodeStore'),
            selected = list.getSelectionModel().getSelection();

        if (selected.length) {
            Ext.Msg.confirm('Удаление', 'Удалить штрихкод?',
                function(btn, text) {
                    if (btn == 'yes') {
                        store.remove(selected);
                    }
                }
            );
        } else
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Штрихкод не выделен', 'winError');
    },

    onGeneralGridLbarStatusAddClick: function(grid, ref) {
        var selected = grid.getSelectionModel().getSelection()[0];

        if (selected) {
            if (selected.get('StatusID') == 1) {
                Ext.Msg.confirm('Смена статуса позиции', 'Перевести черновик в активный статус?',
                    function(btn, text) {
                        if (btn == 'yes') {
                            var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Обработка...'});

                            var params = {};
                            params['ID'] = selected.get('ID');
                            params['act'] = 'updateStatus' + ref.ref;

                            myMask.show();
                            Ext.Ajax.request({
                                url: 'resources/data/api.php',
                                timeout: 600000,
                                params: params,
                                success: function(response) {
                                    myMask.destroy();
                                    var responseText = Ext.JSON.decode(response.responseText);

                                    if (responseText.msg) {
                                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + responseText.msg, 'winError');
                                    } else {
                                        TestOrimi.getApplication().getController('Main').showNotification('Успешно! Черновик успешно переведен в активный статус', 'winSuccess');
                                        grid.store.load();
                                        selected.set('StatusID', 2);
                                        console.dir(selected);
                                    }
                                },
                                failure: function(form, action) {
                                    myMask.destroy();
                                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                                }
                            });
                        }
                    }
                );
            } else
                TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Позиция не является черновиком', 'winError');
        } else
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Позиция не выбрана', 'winError');
    },

    onGeneralGridLbarDeleteAddClick: function(grid, ref) {
        var selected   = grid.getSelectionModel().getSelection()[0],
            userRights = TestOrimi.getApplication().data;

        if (selected) {
            if ((selected.get('StatusID') == 1 && userRights[ref.ref + '_draft_delete']) || (selected.get('StatusID') == 2 && userRights[ref.ref + '_active_delete'])) {
                Ext.Msg.confirm('Смена статуса позиции', 'Пометить позицию на удаление?',
                    function(btn, text) {
                        if (btn == 'yes') {
                            var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Обработка...'});
                            myMask.show();

                            var params = {};
                            params['ID'] = selected.get('ID');
                            params['act'] = 'setDeleteStatus' + ref.ref;

                            Ext.Ajax.request({
                                url: 'resources/data/api.php',
                                timeout: 600000,
                                params: params,
                                success: function(response) {
                                    myMask.destroy();
                                    var responseText = Ext.JSON.decode(response.responseText);

                                    if (responseText.msg)
                                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + responseText.msg, 'winError');
                                    else {
                                        var status = selected.get('StatusID');
                                        if (status == 1) {
                                            TestOrimi.getApplication().getController('Main').showNotification('Успешно! Позиция удаленa', 'winSuccess');
                                            grid.store.load();
                                        }
                                        if (status == 2) {
                                            TestOrimi.getApplication().getController('Main').showNotification('Успешно! Позиция помечена на удаление', 'winSuccess');
                                            grid.store.load();
                                        }
                                    }
                                },
                                failure: function(form, action) {
                                    myMask.destroy();
                                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                                }
                            });
                        }
                    }
                );
            } else
                TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Позиция не является черновиком или активной или удаление закрыто правами доступа', 'winError');
        } else
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Позиция не выбрана', 'winError');
    },

    onAttributeWindowSearchChange: function(grid, field, newValue, oldValue) {
        var filters = grid.store.filters.items,
            i       = 0,
            length  = filters.length;

        if (length) {
            for (; i < length; i++) {
                if (filters[i].property == field.up('gridcolumn').dataIndex && (!filters[i].id)) {
                    grid.store.filters.removeAt(i);
                    break;
                }
            }
        }

        if (newValue) {
            grid.store.filter({
                property: field.up('gridcolumn').dataIndex,
                value: newValue,
                anyMatch: true
            });
            field.up('gridcolumn').addCls('ux-filtered-column');
        } else {
            field.up('gridcolumn').removeCls('ux-filtered-column');
            if (grid.store.filters.items.length)
                grid.store.filter();
            else
                grid.store.clearFilter();
        }
    },

    GridStoreApplyFilters: function(store, newfilters, grid) {
        store.clearFilter(true);
        var columns = grid.headerCt.getVisibleGridColumns(),
            i       = 0,
            length  = columns.length;

        for (; i < length; i++) {
            if (columns[i].dataIndex) {
                if (newfilters.length) {
                    var filter = Ext.Array.findBy(newfilters, function(item, index) {
                        if (item.property == columns[i].dataIndex)
                            return item;
                    });
                    if (filter) {
                        var textfield = columns[i].down('textfield');

                        textfield.suspendEvents();
                        textfield.setValue(filter.value);
                        textfield.resumeEvents();
                        columns[i].addCls('ux-filtered-column');
                    } else {
                        columns[i].down('textfield').reset();
                        columns[i].removeCls('ux-filtered-column');
                    }
                } else {
                    columns[i].down('textfield').reset();
                    columns[i].removeCls('ux-filtered-column');
                }
            }
        }
        if (newfilters.length) {
            store.filter(newfilters);
        } else {
            store.loadPage(1);
        }

        this.checkMasterSearchStatus(grid);
    },

    AttributeStoreApplyFilters: function(store, grid) {
        var columns = grid.headerCt.getVisibleGridColumns(),
            i       = 0,
            length  = columns.length,
            filters = store.filters.items;

        for (; i < length; i++) {
            if (columns[i].dataIndex) {
                if (filters.length) {
                    var filter = Ext.Array.findBy(filters, function(item, index) {
                        if (item.property == columns[i].dataIndex && item.value)
                            return item;
                    });
                    if (filter) {
                        var textfield = columns[i].down('textfield');

                        textfield.suspendEvents();
                        textfield.setValue(filter.value);
                        textfield.resumeEvents();
                        columns[i].addCls('ux-filtered-column');
                    } else {
                        columns[i].down('textfield').reset();
                        columns[i].removeCls('ux-filtered-column');
                    }
                } else {
                    columns[i].down('textfield').reset();
                    columns[i].removeCls('ux-filtered-column');
                }
            }
        }

        for (i = 0; i < filters.length; i++) {
            if (filters[i] && filters[i].hasOwnProperty('defaultValue')) {
                filters[i].setValue();
            }
        }
    },

    SyncedGoodStoreBeforeLoad: function(store, operation) {
        if (!this.getGeneral_tabpanelFirstcol().down('pagingtoolbar').down('[name=syncedShowAll]').getValue()) {
            var grid     = this.getGeneral_grid(),
                selected = grid.getSelectionModel().getSelection(),
                length   = selected.length,
                selIDs   = [];

            for (var i = 0; i < length; i++) {
                selIDs.push(selected[i].get('ID'));
            }
            store.proxy.extraParams['GoodID'] = selIDs.toString();

            var slaveSelected = this.getGeneral_tabpanelFirstcol().getSelectionModel().getSelection();

            if (slaveSelected.length == 1) {
                store.selected = slaveSelected[0].get('ID');
            }
        } else {
            delete store.proxy.extraParams['GoodID'];
        }
    },

    SyncedGoodStoreAfterLoad: function(store, records) {
        if (store.selected) {
            var length = records.length;

            for (var i = 0; i < length; i++) {
                if (records[i].get('ID') == store.selected) {
                    this.getGeneral_tabpanelFirstcol().getSelectionModel().select(i);
                    break;
                }
            }
            store.selected = false;
        }
    },

    SyncedStoresStoreBeforeLoad: function(store, operation) {
        if (!this.getGeneral_tabpanelFirstcolTT().down('pagingtoolbar').down('[name=syncedShowAll]').getValue()) {
            var grid     = this.getGeneral_gridTT(),
                selected = grid.getSelectionModel().getSelection(),
                length   = selected.length,
                selIDs   = [];

            for (i = 0; i < length; i++) {
                selIDs.push({
                    value: selected[i].get('ID'),
                    ref: selected[i].get('isNet')
                });
            }
            store.proxy.extraParams['StoresID'] = Ext.JSON.encode(selIDs);

            var slaveSelected = this.getGeneral_tabpanelFirstcolTT().getSelectionModel().getSelection();
            if (slaveSelected.length == 1)
                store.selected = slaveSelected[0].get('ID');
        } else {
            delete store.proxy.extraParams['StoresID'];
        }
    },

    SyncedStoresStoreAfterLoad: function(store, records) {
        if (store.selected) {
            var length = records.length;

            for (i = 0; i < length; i++) {
                if (records[i].get('ID') == store.selected) {
                    this.getGeneral_tabpanelFirstcolTT().getSelectionModel().select(i);
                    break;
                }
            }
            store.selected = false;
        }
    },

    SyncedStaffStoreBeforeLoad: function(store, operation) {
        if (!this.getGeneral_tabpanelFirstcolStaff().down('pagingtoolbar').down('[name=syncedShowAll]').getValue()) {
            var grid     = this.getGeneral_gridStaff(),
                selected = grid.getSelectionModel().getSelection(),
                length   = selected.length,
                selIDs   = [];

            for (i = 0; i < length; i++) {
                selIDs.push(selected[i].get('ID'));
            }
            store.proxy.extraParams['StaffID'] = selIDs.toString();

            var slaveSelected = this.getGeneral_tabpanelFirstcolStaff().getSelectionModel().getSelection();
            if (slaveSelected.length == 1)
                store.selected = slaveSelected[0].get('ID');
        } else {
            delete store.proxy.extraParams['StaffID'];
        }
    },

    SyncedStaffStoreAfterLoad: function(store, records) {
        if (store.selected) {
            var length = records.length;

            for (i = 0; i < length; i++) {
                if (records[i].get('ID') == store.selected) {
                    this.getGeneral_tabpanelFirstcolStaff().getSelectionModel().select(i);
                    break;
                }
            }
            store.selected = false;
        }
    },

    SyncedWarehousesStoreBeforeLoad: function(store, operation) {
        if (!this.getWrhs_general_tabpanelFirstcol().down('pagingtoolbar').down('[name=syncedShowAll]').getValue()) {
            var grid     = this.getWrhs_general_grid(),
                selected = grid.getSelectionModel().getSelection(),
                length   = selected.length,
                selIDs   = [];

            for (var i = 0; i < length; i++) {
                selIDs.push(selected[i].get('ID'));
            }
            store.proxy.extraParams['WarehousesID'] = selIDs.toString();

            var slaveSelected = this.getWrhs_general_tabpanelFirstcol().getSelectionModel().getSelection();
            if (slaveSelected.length == 1)
                store.selected = slaveSelected[0].get('ID');
        } else {
            delete store.proxy.extraParams['WarehousesID'];
        }
    },

    SyncedWarehousesStoreAfterLoad: function(store, records) {
        if (store.selected) {
            var length = records.length;

            for (var i = 0; i < length; i++) {
                if (records[i].get('ID') == store.selected) {
                    this.getWrhs_general_tabpanelFirstcol().getSelectionModel().select(i);
                    break;
                }
            }
            store.selected = false;
        }
    },

    MasterGoodStoreBeforeLoad: function(store, operation) {
        var grid = this.getGeneral_grid();

        operation.limit = Math.floor((grid.view.getHeight()) / 25 - 1);
        store.pageSize = Math.floor((grid.view.getHeight()) / 25 - 1);
    },

    MasterStoresStoreBeforeLoad: function(store, operation) {
        var grid = this.getGeneral_gridTT();

        operation.limit = Math.floor((grid.view.getHeight()) / 25 - 1);
        store.pageSize = Math.floor((grid.view.getHeight()) / 25 - 1);
    },

    MasterStaffStoreBeforeLoad: function(store, operation) {
        var grid = this.getGeneral_gridStaff();

        operation.limit = Math.floor((grid.view.getHeight()) / 25 - 1);
        store.pageSize = Math.floor((grid.view.getHeight()) / 25 - 1);
    },

    MasterDistrStoreBeforeLoad: function(store, operation) {
        var grid = this.getGeneral_gridDistr();

        operation.limit = Math.floor((grid.view.getHeight()) / 25 - 1);
        store.pageSize = Math.floor((grid.view.getHeight()) / 25 - 1);
    },

    MasterWarehousesStoreBeforeLoad: function(store, operation) {
        var grid = this.getWrhs_general_grid();

        operation.limit = Math.floor((grid.view.getHeight()) / 25 - 1);
        store.pageSize = Math.floor((grid.view.getHeight()) / 25 - 1);
    },

    MasterGoodStoreAfterLoad: function(store, records) {
        if (records && records.length == 1) {
            this.getGeneral_grid().getSelectionModel().select(0);
        }
    },

    MasterStoresStoreAfterLoad: function(store, records) {
        if (records && records.length == 1) {
            this.getGeneral_gridTT().getSelectionModel().select(0);
        }
    },

    MasterStaffStoreAfterLoad: function(store, records) {
        if (records && records.length == 1) {
            this.getGeneral_gridStaff().getSelectionModel().select(0);
        }
    },

    MasterWarehousesStoreAfterLoad: function(store, records) {
        if (records && records.length == 1) {
            this.getWrhs_general_grid().getSelectionModel().select(0);
        }
    },

    CityStoreBeforeLoad: function(store, operation) {
        var grid = Ext.getCmp('cityWindowGrid');

        if (grid) {
            if (grid.isVisible()) {
                operation.limit = Math.floor((grid.view.getHeight()) / 25 - 1);
                store.pageSize = Math.floor((grid.view.getHeight()) / 25 - 1);
            }
        }
    },

    ActiveStoresStoreBeforeLoad: function(store, operation) {
        var grid = Ext.getCmp('storeModalWindowGrid');

        if (grid) {
            if (grid.isVisible()) {
                operation.limit = Math.floor((grid.view.getHeight()) / 25 - 1);
                store.pageSize = Math.floor((grid.view.getHeight()) / 25 - 1);
            }
        }
    },

    TemplateLoadStore: function(lbar, ref) {
        Ext.getStore('TestOrimi.store.TemplateStore').load({
            params: {
                Ref: ref.ref
            }
        });

        Ext.create('TestOrimi.view.TemplateSettings', {Ref: ref.ref}).show();
    },

    EventHistoryClick: function(grid, ref) {
        var selected = grid.getSelectionModel().getSelection()[0],
            params   = {
                ElementID: selected.get('ID'),
                Ref: ref.ref
            };

        if (ref.ref == 'TT')
            if (selected.get('isNet') == 2)
                params['Ref'] = 'Distr';

        Ext.getStore('TestOrimi.store.EventHistoryStore').load({
            params: params
        });
        Ext.create('TestOrimi.view.EventHistory').show();
    },

    loadActiveMasterStore: function() {
        var currentItem = this.getViewport().down('[name=domCard]').getLayout().getActiveItem();
        if (currentItem.name !== 'attr_ref_card') {
            currentItem.down('grid').getStore().load();
        }
    },

    GoodsSyncResultStoreFilterChange: function(store, operation) {
        var me  = this,
            win = Ext.getCmp('createSkuGroupSyncWindow-id');

        if (win) {
            win.down('grid').down('toolbar').down('tbtext').updateInfo();
        }
    },

    onGridCelldblclick: function(view, td, cellIndex, record, tr, rowIndex) {
        console.dir(view);
        console.dir(td);
        console.dir(cellIndex);
        console.dir(record);
        console.dir(tr);
        console.dir(rowIndex);
    },

    onGroupSyncWindowReportButtonClick: function(win) {
        var myMask  = new Ext.LoadMask(win, {msg: 'Формируется файл...'}),
            columns = win.down('grid').view.headerCt.getVisibleGridColumns(),
            colArr  = [];

        for (var i = 0; i < columns.length; i++) {
            if (columns[i]['dataIndex'])
                colArr.push(columns[i]['dataIndex']);
        }

        myMask.show();
        Ext.Ajax.request({
            url: 'resources/data/api.php',
            timeout: 600000,
            params: {
                act: 'getSyncResultReport',
                ref: 'Goods',
                columns: colArr.toString()
            },
            success: function(response) {
                response = Ext.JSON.decode(response.responseText);

                var hiddenIFrameID = 'hiddenDownloader',
                    iframe         = document.getElementById(hiddenIFrameID);

                if (iframe === null) {
                    iframe = document.createElement('iframe');
                    iframe.id = hiddenIFrameID;
                    iframe.style.display = 'none';
                    document.body.appendChild(iframe);
                }
                iframe.src = 'resources/data/api.php?act=getFile&file=' + response['name'];

                myMask.destroy();
            }
        });
    },

    AdvertisingBossHeadersStoreBeforeLoad: function(store, operation) {
        var grid = this.getAdvertising_boss_headers_grid();
        var limit;

        if (!grid) {
            operation.limit = 25;
            store.pageSize = 25;
            return;
        }

        limit = Math.floor((grid.view.getHeight()) / 25 - 1);

        operation.limit = limit;
        store.pageSize = limit;
    },

    AdvertisingEventsHeadersStoreBeforeLoad: function(store, operation) {
        var grid = this.getAdvertising_events_headers_grid();
        var limit;

        if (!grid) {
            operation.limit = 25;
            store.pageSize = 25;
            return;
        }

        limit = Math.floor((grid.view.getHeight()) / 25 - 1);

        operation.limit = limit;
        store.pageSize = limit;

        grid.getSelectionModel().deselectAll(true);
    },

    onAdvertisingBossHeadersGridSelectionChange: function(selModel, selected) {
        // if (selected.length) {
        //     Ext.getStore('TestOrimi.store.AdvertisingBossRowsStore').load();
        // } else {
        //     Ext.getStore('TestOrimi.store.AdvertisingBossRowsStore').removeAll();
        // }
        Ext.getStore('TestOrimi.store.AdvertisingBossRowsStore').load();
    },

    onAdvertisingBossHeadersGridCellDblClick: function(grid, td, cellIndex, record, tr, rowIndex) {
        Ext.create('TestOrimi.view.CreateAdvertisingBossWindow', {
            data: record
        }).show();
    },

    onAdvertisingBossPanelActivate: function(panel) {
        if (panel.loaded) return;

        var me         = this,
            stores     = [
                'TestOrimi.store.AdvertisingBossHeadersStore', 'TestOrimi.store.BossOrderStatusStore'
            ],
            storeCount = stores.length,
            loadCount  = 0;

        if (!Ext.getStore(stores[0]).lastOptions) {
            panel.setLoading('Загрузка атрибутов...');

            setTimeout(function() {
                for (var i = 0; i < storeCount; i++) {
                    Ext.getStore(stores[i]).load({
                        callback: function(records, operation, success) {
                            loadCount++;
                            me.RefLoadCountCheck(panel, loadCount, storeCount);
                        }
                    });
                }
            }, 0);
        }
    },

    onAdvertisingEventsPanelActivate: function(panel) {
        if (panel.loaded) return;

        var me         = this,
            stores     = [
                'TestOrimi.store.AdvertisingEventsHeadersStore',
                'TestOrimi.store.AvailableAreaStore',
                'TestOrimi.store.RequestStatusStore',
                'TestOrimi.store.ContractorGroupStore',
                'TestOrimi.store.NetStore',
                'TestOrimi.store.OrimiGoodsStore',
                'TestOrimi.store.ActiveStoresStore',
                'TestOrimi.store.AdvertisingChannelStore',
                'TestOrimi.store.ExternalPackStore',
                'TestOrimi.store.BrandStore',
                'TestOrimi.store.SegmentStore',
                'TestOrimi.store.InternalPackStore',
                'TestOrimi.store.PaymentTypeStore'
            ],
            storeCount = stores.length,
            loadCount  = 0;

        if (!Ext.getStore(stores[0]).lastOptions) {
            panel.setLoading('Загрузка атрибутов...');

            setTimeout(function() {
                for (var i = 0; i < storeCount; i++) {
                    Ext.getStore(stores[i]).load({
                        callback: function(records, operation, success) {
                            loadCount++;
                            me.RefLoadCountCheck(panel, loadCount, storeCount);
                        }
                    });
                }
            }, 0);
        }
    },

    onAdvertisingEventsHeadersGridSelectionChange: function(selModel, selected) {
        var rowsGridLbar = this.getAdvertising_events_rows_grid().getDockedItems('toolbar[dock="left"]')[0];
        var headersGridLbar = this.getAdvertising_events_headers_grid().getDockedItems('toolbar[dock="left"]')[0];
        var userRights = TestOrimi.getApplication().data;

        if (selected.length) {
            if (userRights['Advertising_events_edit'] && selected[0].get('Status') === 'Ввод плана') {
                rowsGridLbar.down('[name=addNewRowButton]').enable();
                rowsGridLbar.down('[name=addMultipleRowButton]').enable();
            } else {
                rowsGridLbar.down('[name=addNewRowButton]').disable();
                rowsGridLbar.down('[name=addMultipleRowButton]').disable();
            }

            headersGridLbar.down('[name=deleteButton]').enable();
            headersGridLbar.down('[name=duplicateButton]').enable();
            Ext.getStore('TestOrimi.store.AdvertisingEventsRowsStore').load();
        } else {
            rowsGridLbar.down('[name=addNewRowButton]').disable();
            rowsGridLbar.down('[name=addMultipleRowButton]').disable();
            headersGridLbar.down('[name=deleteButton]').disable();
            headersGridLbar.down('[name=duplicateButton]').disable();
            Ext.getStore('TestOrimi.store.AdvertisingEventsRowsStore').removeAll();
        }
    },

    onAdvertisingEventsHeadersGridCellDblClick: function(grid, td, cellIndex, record, tr, rowIndex) {
        Ext.create('TestOrimi.view.CreateAdvertisingEventWindow', {
            data: record
        }).show();
    },

    onAdvertisingEventsHeadersGridUpdateStatusClick: function(grid, rowIndex, colIndex, targetStatus) {
        var rec = grid.getStore().getAt(rowIndex);
        var confirmText;
        var needPromt = false;
        var windowMethod = Ext.Msg.confirm;

        switch (targetStatus) {
            case 'forward':
                switch (rec.get('Status')) {
                    case 'Ввод плана':
                        confirmText = 'Отправить заявку <strong>' + rec.get('Title') + '</strong> на утверждение?';
                        break;
                    case 'План на утверждении в отделе рекламы':
                        confirmText = 'Отправить заявку <strong>' + rec.get('Title') + '</strong> на утверждение в отдел продаж?';
                        break;
                    case 'План на утверждении в отделе продаж':
                        confirmText = 'Отправить заявку <strong>' + rec.get('Title') + '</strong> на утверждение старшему менеджеру?';
                        break;
                    case 'План на утверждении у старшего менеджера отдела продаж':
                        confirmText = 'Отправить заявку <strong>' + rec.get('Title') + '</strong> на утверждение сетью?';
                        break;
                    case 'На согласовании с сетью':
                        confirmText = 'Зафиксировать подтверждение согласования сетью заявки <strong>' + rec.get('Title') + '</strong>?';
                        break;
                    case 'Ввод факта':
                    case 'Ручной ввод факта':
                        confirmText = 'Отправить заявку <strong>' + rec.get('Title') + '</strong> на утверждение?';
                        break;
                    case 'Акция проверена РП':
                        confirmText = 'Зафиксировать подтверждение заявки <strong>' + rec.get('Title') + '</strong> от лица РП?';
                        break;
                    default:
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Заявка находится в неизвестном статусе', 'winError');
                        return;
                }
                break;
            case 'return':
                needPromt = true;

                switch (rec.get('Status')) {
                    case 'План на утверждении в отделе рекламы':
                        confirmText = 'Вернуть заявку <strong>' + rec.get('Title') + '</strong> на доработку плана?<br><br>Введите комментарий:';
                        break;
                    case 'План на утверждении в отделе продаж':
                        confirmText = 'Вернуть заявку <strong>' + rec.get('Title') + '</strong> на доработку отдел рекламы?<br><br>Введите комментарий:';
                        break;
                    case 'План на утверждении у старшего менеджера отдела продаж':
                        confirmText = 'Вернуть заявку <strong>' + rec.get('Title') + '</strong> на доработку?<br><br>Введите комментарий:';
                        break;
                    case 'На согласовании с сетью':
                        confirmText = 'Зафиксировать отказ согласования сетью заявки <strong>' + rec.get('Title') + '</strong>?<br><br>Введите комментарий:';
                        break;
                    case 'Ввод факта':
                    case 'Ручной ввод факта':
                        confirmText = 'Зафиксировать отказ ввод факта/ручной ввод факта заявки <strong>' + rec.get('Title') + '</strong>?<br><br>Введите комментарий:';
                        break;
                    default:
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Заявка находится в неизвестном статусе', 'winError');
                        return;
                }
                break;
            default:
                TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Незвестный статус, внутренняя ошибка системы', 'winError');
                return;
        }

        if (needPromt === true) {
            windowMethod = Ext.Msg.prompt;
        }

        windowMethod.call(Ext.Msg, 'Отправить', confirmText, function(btn, text) {
            if (btn !== 'ok' && btn !== 'yes') {
                return;
            }

            var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Обработка...'});

            myMask.show();
            Ext.Ajax.request({
                url: 'resources/data/api.php',
                timeout: 600000,
                params: {
                    ID: rec.get('ID'),
                    act: 'getAdvertisingEventsHeaders',
                    subaction: 'updateStatus',
                    Status: targetStatus,
                    Comment: text
                },
                success: function(response) {
                    myMask.destroy();
                    var responseText = Ext.JSON.decode(response.responseText);

                    if (!responseText.success) {
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + responseText.msg, 'winError');
                    } else {
                        TestOrimi.getApplication().getController('Main').showNotification('Успешно! Статус заявки обновлен', 'winSuccess');
                        grid.getStore().load();
                    }
                },
                failure: function(form, action) {
                    myMask.destroy();
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сервер не отвечает на запрос', 'winError');
                }
            });
        }, null, true);
    },

    onCreateAdvertisingEventWindowSetManualFactStatus: function(win) {
        var form = win.down('form');
        var store = Ext.getStore('TestOrimi.store.AdvertisingEventsHeadersStore');
        var formData = form.getValues();

        win.setLoading('Загрузка...');

        Ext.Ajax.request({
            url: 'resources/data/api.php',
            timeout: 600000,
            params: {
                ID: formData['ID'],
                act: 'getAdvertisingEventsHeaders',
                subaction: 'updateStatus',
                Status: 'toManualFact'
            },
            success: function(response) {
                var responseText = Ext.JSON.decode(response.responseText);

                if (!responseText.success) {
                    TestOrimi.getApplication().getController('Main')
                             .showNotification('Ошибка! Не удалось выполнить смену статуса.', 'winError');
                    win.setLoading(false);
                    return;
                }

                TestOrimi.getApplication().getController('Main')
                         .showNotification('Статус изменен.', 'winSuccess');
                store.load();

                var statusField = win.down('[name=StatusID]');
                var statusStore = Ext.getStore('TestOrimi.store.RequestStatusStore');

                var targetStatus = (win.data.get('Status') === 'Ручной ввод факта') ? 'Ввод факта' : 'Ручной ввод факта';
                var manualStatusValue = statusStore.getAt(statusStore.find('Name', targetStatus));

                statusField.setValue(manualStatusValue.get('ID'));
                win.data.set('Status', targetStatus);

                win.resetManualFactToggleButton();

                win.setLoading(false);
            },
            failure: function() {
                TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сервер не отвечает на запрос', 'winError');
                win.setLoading(false);
            }
        });
    },

    onAdvertisingEventsHeadersGridDeleteButtonClick: function(grid) {
        var selected = grid.getSelectionModel().getSelection();
        var userRights = TestOrimi.getApplication().data;

        if (!selected.length) {
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Не выбрана запись для удаления', 'winError');
            return;
        }

        if (selected[0].get('Status') !== 'Ввод плана' && !userRights['Advertising_events_delete']) {
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Запись в этом статусе нельзя удалить с вашим уровнем доступа', 'winError');
            return;
        }

        Ext.Msg.confirm('Удаление', 'Вы уверены что хотите удалить выбранный элемент?', function(btn, text) {
            if (btn === 'yes') {
                var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Удаляю...'});
                myMask.show();

                Ext.Ajax.request({
                    url: 'resources/data/api.php',
                    timeout: 600000,
                    params: {
                        ID: selected[0].get('ID'),
                        act: 'getAdvertisingEventsHeaders',
                        subaction: 'delete'
                    },
                    success: function(response) {
                        myMask.destroy();
                        TestOrimi.getApplication().getController('Main').showNotification('Успешно! Записи удалены', 'winSuccess');
                        Ext.getStore('TestOrimi.store.AdvertisingEventsHeadersStore').load();
                    },
                    failure: function(form, action) {
                        myMask.destroy();
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                    }
                });
            }
        });
    },

    onAdvertisingEventsHeadersGridDuplicateButtonClick: function(grid) {
        var selected = grid.getSelectionModel().getSelection();

        if (!selected.length) {
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Запись не выбрана', 'winError');
            return;
        }

        Ext.Msg.confirm('Создание', 'Вы уверены что хотите дублировать выбранную акцию? Она будет создана в статусе "Ввод плана"', function(btn, text) {
            if (btn !== 'yes') {
                return;
            }

            var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Дублирую...'});
            myMask.show();

            Ext.Ajax.request({
                url: 'resources/data/api.php',
                timeout: 600000,
                params: {
                    ID: selected[0].get('ID'),
                    act: 'getAdvertisingEventsHeaders',
                    subaction: 'duplicate'
                },
                success: function(response) {
                    myMask.destroy();
                    var responseText = Ext.JSON.decode(response.responseText);

                    if (!responseText.success) {
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + responseText.msg, 'winError');
                    } else {
                        TestOrimi.getApplication().getController('Main').showNotification('Успешно! Дубликат создан', 'winSuccess');
                        Ext.getStore('TestOrimi.store.AdvertisingEventsHeadersStore').load();
                    }
                },
                failure: function(form, action) {
                    myMask.destroy();
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                }
            });
        });
    },

    onAdvertisingEventsHeadersGridApproveAllButtonClick: function(grid) {
        var userRights = TestOrimi.getApplication().data;
        var store = grid.getStore();
        var confirmText = '';
        var filteredRecords;

        filteredRecords = getRecordsByStatus(store.getRange(), 'План на утверждении в отделе рекламы');

        if (userRights['Advertising_events_adv_appr'] && filteredRecords.length > 0) {
            confirmText = 'Отправить заявки <strong>' + filteredRecords.map(function(rec) {return rec.get('Title')}).join(', ') + '</strong> на утверждение в отдел продаж?';
            confirmAndSend(filteredRecords, confirmText);
            return;
        }

        filteredRecords = getRecordsByStatus(store.getRange(), 'План на утверждении в отделе продаж');

        if (userRights['Advertising_events_sales_appr'] && filteredRecords.length > 0) {
            confirmText = 'Отправить заявки <strong>' + filteredRecords.map(function(rec) {return rec.get('Title')}).join(', ') + '</strong> на утверждение старшему менеджеру?';
            confirmAndSend(filteredRecords, confirmText);
            return;
        }

        filteredRecords = getRecordsByStatus(store.getRange(), 'План на утверждении у старшего менеджера отдела продаж');

        if (userRights['Advertising_events_s_boss_appr'] && filteredRecords.length > 0) {
            confirmText = 'Отправить заявки <strong>' + filteredRecords.map(function(rec) {return rec.get('Title')}).join(', ') + '</strong> на утверждение сетью?';
            confirmAndSend(filteredRecords, confirmText);
            return;
        }

        filteredRecords = getRecordsByStatus(store.getRange(), 'На согласовании с сетью');

        if (userRights['Advertising_events_net_appr'] && filteredRecords.length > 0) {
            confirmText = 'Зафиксировать подтверждение согласования сетью заявок <strong>' + filteredRecords.map(function(rec) {return rec.get('Title')}).join(', ') + '</strong> ?';
            confirmAndSend(filteredRecords, confirmText);
            return;
        }

        filteredRecords = getRecordsByStatus(store.getRange(), 'Акция на проверке у РП');

        if (userRights['Advertising_events_edit'] && filteredRecords.length > 0) {
            confirmText = 'Зафиксировать подтверждение заявок <strong>' + filteredRecords.map(function(rec) {return rec.get('Title')}).join(', ') + '</strong> от лица РП?';
            confirmAndSend(filteredRecords, confirmText);
            return;
        }

        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Нет подходящих заявок для утверждения', 'winError');

        function getRecordsByStatus(records, status) {
            return records.filter(function(rec) {
                return rec.get('Status') === status;
            });
        }

        function confirmAndSend(records, confirmText) {
            Ext.Msg.confirm('Утвердить', confirmText, function(btn, text) {
                if (btn !== 'ok' && btn !== 'yes') {
                    return;
                }

                var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Обработка...'});
                myMask.show();

                var pendingRequests = 0;

                Ext.Array.each(records, function(rec) {
                    pendingRequests++;

                    Ext.Ajax.request({
                        url: 'resources/data/api.php',
                        timeout: 600000,
                        params: {
                            ID: rec.get('ID'),
                            act: 'getAdvertisingEventsHeaders',
                            subaction: 'updateStatus',
                            Status: 'forward'
                        },
                        success: function(response) {
                            pendingRequests--;
                            handleResults(pendingRequests);
                            var responseText = Ext.JSON.decode(response.responseText);

                            if (!responseText.success) {
                                TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + responseText.msg, 'winError');
                            }
                        },
                        failure: function(form, action) {
                            pendingRequests--;
                            handleResults(pendingRequests);
                            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сервер не отвечает на запрос', 'winError');
                        }
                    });
                });

                function handleResults(pendingRequests) {
                    if (pendingRequests === 0) {
                        TestOrimi.getApplication().getController('Main').showNotification('Успешно! Статусы заявок обновлены', 'winSuccess');
                        grid.getStore().load();
                        myMask.destroy();
                    }
                }
            });
        }
    },

    onAdvertisingEventsHeadersGridLoadTSZButton: function(grid) {
        var userRights = TestOrimi.getApplication().data;
        var store = grid.getStore();
        var totalCount = store.getTotalCount();

        Ext.Msg.confirm('Выгрузить в ТСЗ', 'Выгрузить в ТСЗ ' + totalCount + ' записей?', function(btn, text) {
                if (btn !== 'ok' && btn !== 'yes') {
                    return;
                }

                var params = {
                    act: 'getAdvertisingEventsHeaders',
                    subaction: 'loadToTSZ'
                };

                if (store.lastOptions.filters && store.lastOptions.filters.length) {
                    params.filter = store.getProxy().encodeFilters(store.lastOptions.filters);
                }

                if (store.getProxy().extraParams.searchall) {
                    params.searchall = store.getProxy().extraParams.searchall;
                }

                var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Обработка...'});
                myMask.show();

                Ext.Ajax.request({
                    url: 'resources/data/api.php',
                    timeout: 600000,
                    params: params,
                    success: function(response) {
                        myMask.destroy();
                        var responseText = Ext.JSON.decode(response.responseText);

                        if (!responseText.success) {
                            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + responseText.msg, 'winError');
                            return;
                        }

                        TestOrimi.getApplication().getController('Main').showNotification('Успешно! Записи выгружены', 'winSuccess');
                    },
                    failure: function(form, action) {
                        myMask.destroy();
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                    }
                });
            });
    },

    onAdvertisingEventsRowsGridSelectionChange: function(selModel, selected) {
        var rowsGridLbar = this.getAdvertising_events_rows_grid().getDockedItems('toolbar[dock="left"]')[0];
        var headersGrid = this.getAdvertising_events_headers_grid();
        var headersGridSelection = headersGrid.getSelectionModel().getSelection();

        if (selected.length && headersGridSelection.length) {
            if (headersGrid.getSelectionModel().getSelection()[0].get('Status') === 'Ввод плана') {
                rowsGridLbar.down('[name=deleteButton]').enable();
            } else {
                rowsGridLbar.down('[name=deleteButton]').disable();
            }

            rowsGridLbar.down('[name=eventHistoryButton]').enable();
            rowsGridLbar.down('[name=duplicateMultipleRowButton]').enable();
            rowsGridLbar.down('[name=headerSearch]').enable();
        } else {
            rowsGridLbar.down('[name=deleteButton]').disable();
            rowsGridLbar.down('[name=eventHistoryButton]').disable();
            rowsGridLbar.down('[name=duplicateMultipleRowButton]').disable();
            rowsGridLbar.down('[name=headerSearch]').disable();
        }
    },

    onAdvertisingEventsRowsGridAddNewRowButtonClick: function(rowsGrid) {
        var grid = this.getAdvertising_events_headers_grid();
        var selected = grid.getSelectionModel().getSelection();

        if (selected && selected.length === 1) {
            Ext.create('TestOrimi.view.CreateAdvertisingEventRowWindow', {
                AdvertisingRequestRecord: selected[0]
            }).show();
        } else {
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Не выбран заголовок заявки', 'winError');
        }
    },

    onAdvertisingEventsRowsGridAddMultipleRowButtonClick: function(rowsGrid) {
        var grid = this.getAdvertising_events_headers_grid();
        var selected = grid.getSelectionModel().getSelection();

        if (!selected || selected.length === 0) {
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Не выбран заголовок заявки', 'winError');
            return;
        }

        var requestStatus = selected[0].get('Status');

        if (requestStatus !== 'Ввод плана') {
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Заявка не находится в статусе "Ввод плана"', 'winError');
            return;
        }

        Ext.create('TestOrimi.view.ParentID', {
            title: 'Выберите товары для добавления в акцию',
            store: 'TestOrimi.store.OrimiGoodsStore',
            multiselect: true,
            callback: function(selectedIds) {
                rowsGrid.fireEvent('createMultipleRows', rowsGrid, selected[0].get('ID'), selectedIds);
            }
        }).show();
    },

    onAdvertisingEventsRowsGridCreateMultipleRows: function(rowsGrid, requestId, selectedGoodIds) {
        if (!selectedGoodIds || !selectedGoodIds.length) {
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
            return;
        }

        var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Создаю...'});
        myMask.show();

        Ext.Ajax.request({
            url: 'resources/data/api.php',
            timeout: 600000,
            params: {
                GoodID: selectedGoodIds.toString(),
                RequestID: requestId,
                act: 'getAdvertisingEventsRows',
                subaction: 'groupCreate'
            },
            success: function(response) {
                myMask.destroy();
                var responseText = Ext.JSON.decode(response.responseText);

                if (!responseText.success) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + responseText.msg, 'winError');
                } else {
                    TestOrimi.getApplication().getController('Main').showNotification('Успешно! Записи созданы', 'winSuccess');
                    Ext.getStore('TestOrimi.store.AdvertisingEventsRowsStore').load();
                }
            },
            failure: function(form, action) {
                myMask.destroy();
                TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
            }
        });
    },

    onAdvertisingEventsRowsGridDuplicateMultipleRowButtonClick: function(rowsGrid) {
        var selected = rowsGrid.getSelectionModel().getSelection();

        if (!selected || selected.length === 0) {
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Не выбраны строки для копирования.', 'winError');
            return;
        }

        var selectedIds = [];
        var RequestIDs = Ext.Array.unique(selected.map(function(record) {
            selectedIds.push(record.get('ID'));

            return record.get('RequestID');
        }));

        if (RequestIDs.length > 1) {
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Выбранные записи относятся к разным заявкам!', 'winError');
            return;
        }

        Ext.Msg.confirm('Создание', 'Вы уверены что хотите дублировать выбранные товары внутри акции', function(btn, text) {
            if (btn !== 'yes') {
                return;
            }

            var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Дублирую...'});
            myMask.show();

            Ext.Ajax.request({
                url: 'resources/data/api.php',
                timeout: 600000,
                params: {
                    RowIDs: selectedIds.toString(),
                    act: 'getAdvertisingEventsRows',
                    subaction: 'groupDuplicate'
                },
                success: function(response) {
                    myMask.destroy();
                    var responseText = Ext.JSON.decode(response.responseText);

                    if (!responseText.success) {
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + responseText.msg, 'winError');
                    } else {
                        TestOrimi.getApplication().getController('Main').showNotification('Успешно! Дубликаты созданы', 'winSuccess');
                        Ext.getStore('TestOrimi.store.AdvertisingEventsRowsStore').load();
                    }
                },
                failure: function(form, action) {
                    myMask.destroy();
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                }
            });
        });
    },

    onAdvertisingEventsRowsGridCellDblClick: function(view, td, cellIndex, record, tr, rowIndex) {
        Ext.create('TestOrimi.view.CreateAdvertisingEventRowWindow', {
            data: record
        }).show();
    },

    onAdvertisingEventsRowsGridHeaderSearchButtonClick: function(rowsGrid) {
        var masterGrid = this.getAdvertising_events_headers_grid();
        var slaveGridShowAllCb = rowsGrid.down('pagingtoolbar').down('[name=showAll]');
        var selected = rowsGrid.getSelectionModel().getSelection();
        var arr = [];

        if (!selected.length) {
            return;
        }

        arr.push({
            property: 'ID',
            value: '=' + selected[0].get('RequestID')
        });

        slaveGridShowAllCb.suspendEvents(false);
        slaveGridShowAllCb.setValue(false);
        slaveGridShowAllCb.resumeEvents();
        this.GridStoreApplyFilters(masterGrid.getStore(), arr, masterGrid);
    },

    onAdvertisingEventsRowsGridDeleteButtonClick: function(rowsGrid) {
        var selected = rowsGrid.getSelectionModel().getSelection();
        var selectedIds = [];

        if (selected && selected.length) {
            Ext.Msg.confirm('Удаление', 'Вы уверены что хотите удалить ' + selected.length + ' элемент(ов)?',
                function(btn, text) {
                    if (btn === 'yes') {
                        var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Удаляю...'});
                        myMask.show();

                        selectedIds = Ext.Array.map(selected, function(item) {
                            return item.get('ID');
                        });

                        Ext.Ajax.request({
                            url: 'resources/data/api.php',
                            timeout: 600000,
                            params: {
                                ID: selectedIds.toString(),
                                act: 'getAdvertisingEventsRows',
                                subaction: 'delete'
                            },
                            success: function(response) {
                                myMask.destroy();
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Записи удалены', 'winSuccess');
                                Ext.getStore('TestOrimi.store.AdvertisingEventsRowsStore').load();
                            },
                            failure: function(form, action) {
                                myMask.destroy();
                                TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                            }
                        });
                    }
                });
        } else {
            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Не выбраны записи для удаления', 'winError');
        }
    },

    encodeStoreFiltersString: function(filters) {
        var min    = [],
            length = filters.length,
            i      = 0;

        for (; i < length; i++) {
            min[i] = {
                property: filters[i].property,
                value: filters[i].value
            };
        }

        return Ext.encode(min);
    },

    onAdvertisingEventsRowsGridReportButtonClick: function(rowsGrid) {
        var me = this;
        var grid = this.getAdvertising_events_headers_grid();

        Ext.Msg.show({
            title: 'Выгрузка в Excel',
            msg: 'Выгрузить рекламные акции?',
            buttons: Ext.Msg.YESNO,
            fn: function(btn) {
                if (btn == 'yes') {
                    var myMask = new Ext.LoadMask(Ext.getBody(), {msg: 'Формируется файл...'});

                    myMask.show();

                    var masterColumns = grid.view.headerCt.getVisibleGridColumns(),
                        slaveColumns  = rowsGrid.view.headerCt.getVisibleGridColumns(),
                        columns       = [],
                        i;

                    for (i = 0; i < masterColumns.length; i++) {
                        if (masterColumns[i]['dataIndex']) {
                            columns.push('header' + masterColumns[i]['dataIndex']);
                        }
                    }

                    for (i = 0; i < slaveColumns.length; i++) {
                        if (slaveColumns[i]['dataIndex']) {
                            columns.push('row' + slaveColumns[i]['dataIndex']);
                        }
                    }

                    var masterFilters = Ext.Array.map(Ext.getStore('TestOrimi.store.AdvertisingEventsHeadersStore').filters.items,
                        function(item) {
                            var copy = {
                                property: item.property,
                                value: item.value
                            };

                            copy.property = 'header' + copy.property;
                            return copy;
                        }
                    );

                    var slaveFilters = Ext.Array.map(Ext.getStore('TestOrimi.store.AdvertisingEventsRowsStore').filters.items,
                        function(item) {
                            var copy = {
                                property: item.property,
                                value: item.value
                            };

                            copy.property = 'row' + copy.property;
                            return copy;
                        }
                    );

                    Ext.Ajax.request({
                        url: 'resources/data/api.php',
                        timeout: 600000,
                        params: {
                            act: 'getAdvertisingEventsReport',
                            columns: columns.toString(),
                            masterFilters: me.encodeStoreFiltersString(masterFilters),
                            slaveFilters: me.encodeStoreFiltersString(slaveFilters)
                        },
                        success: function(response) {
                            response = Ext.JSON.decode(response.responseText);

                            var hiddenIFrameID = 'hiddenDownloader',
                                iframe         = document.getElementById(hiddenIFrameID);

                            if (iframe === null) {
                                iframe = document.createElement('iframe');
                                iframe.id = hiddenIFrameID;
                                iframe.style.display = 'none';
                                document.body.appendChild(iframe);
                            }
                            iframe.src = 'resources/data/api.php?act=getFile&file=' + response['name'];

                            myMask.destroy();
                        }
                    });
                }
            }
        });
    },

    AdvertisingBossRowsStoreBeforeLoad: function(store, operation) {
        var grid     = this.getAdvertising_boss_headers_grid(),
            selected = grid.getSelectionModel().getSelection(),
            length   = selected.length,
            selIDs   = [];

        for (var i = 0; i < length; i++) {
            selIDs.push(selected[i].get('ID'));
        }
        store.proxy.extraParams['OrderID'] = selIDs.toString();
    },

    AdvertisingEventsHeadersStoreAfterLoad: function(store, records) {
        if (!records) {
            return;
        }

        var grid = this.getAdvertising_events_headers_grid();

        if (records.length === 1) {
            grid.getSelectionModel().select(0);
            return;
        }

        this.refreshSelectionRecords(grid, records);
    },

    refreshSelectionRecords: function(grid, newRecords) {
        var selected;
        var filteredRecords;

        if (grid.getSelectionModel().hasSelection()) {
            if (grid.getSelectionModel().getSelection().length === 1) {
                selected = grid.getSelectionModel().getSelection()[0];
                filteredRecords = newRecords.filter(function(record) {
                    return record.get('ID') === selected.get('ID');
                });

                grid.getSelectionModel().deselectAll(true);
                grid.getSelectionModel().select(filteredRecords);
            }
        }
    },

    AdvertisingEventsRowsStoreBeforeLoad: function(store, operation) {
        var grid = this.getAdvertising_events_headers_grid();

        if (!this.getAdvertising_events_rows_grid().down('pagingtoolbar').down('[name=showAll]').getValue()) {
            var selected = grid.getSelectionModel().getSelection(),
                length   = selected.length,
                selIDs   = [];

            for (var i = 0; i < length; i++) {
                selIDs.push(selected[i].get('ID'));
            }
            store.proxy.extraParams['RequestID'] = selIDs.toString();

            var slaveSelected = this.getAdvertising_events_rows_grid().getSelectionModel().getSelection();

            if (slaveSelected.length == 1) {
                store.selected = slaveSelected[0].get('ID');
            }
        } else {
            delete store.proxy.extraParams['RequestID'];
        }
    },

    AdvertisingEventsRowsStoreAfterLoad: function(store, records) {
        if (store.selected) {
            var length = records.length;

            for (var i = 0; i < length; i++) {
                if (records[i].get('ID') == store.selected) {
                    this.getAdvertising_events_rows_grid().getSelectionModel().select(i);
                    break;
                }
            }
            store.selected = false;
        }
    }

});
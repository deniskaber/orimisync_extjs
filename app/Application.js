﻿Ext.define('TestOrimi.Application', {
    name: 'TestOrimi',
    extend: 'Ext.app.Application',

    views: [
        'TestOrimi.view.Viewport',                      //базовый контейнер страницы
        'TestOrimi.view.general_headerpanel',           //шапка окна            "Мастер-справочник"
        'TestOrimi.view.general_grid',                  //главная таблица       "Мастер-справочник"
        'TestOrimi.view.general_tabpanel',              //окно с вкладками      "Мастер-справочник"
        'TestOrimi.view.general_tabpanelFirstcol',      //первая вкладка        "Мастер-справочник"
        'TestOrimi.view.general_tabpanelSecondcol',     //вторая вкладка        "Мастер-справочник"
        'TestOrimi.view.general_tabpanelCombobox',      //верхняя панель вклад. "Мастер-справочник"
        'TestOrimi.view.ModalCombo',                    //                      "ModalCombo"
        'TestOrimi.view.SearchModalCombo',              //
        'TestOrimi.view.SimpleAttributeWindow',         //
        'TestOrimi.view.AddSimpleAttributeWindow',      //
        'TestOrimi.view.SubBrandWindow',                //создание подоконника  "Саббренд"
        'TestOrimi.view.AddSubBrandWindow',             //добавление саббренда  "Саббренд"
        'TestOrimi.view.BrandWindow',                   //создание подоконника  "Бренд"
        'TestOrimi.view.AddBrandWindow',                //добавление бренда
        'TestOrimi.view.createSkuWindow',               //создание окна         "Главное окно"
        'TestOrimi.view.popUpgeneraltab',               //первая вкладка окна   "Главное окно"
        'TestOrimi.view.popUpsecondtab',                //вторая вкладка окна   "Главное окно"
        'TestOrimi.view.popUpthirdtab',                 //третяя вкладка окна   "Главное окно"
        'TestOrimi.view.ItemselectorAdd',               //окно выбора добавки   "Главное окно"
        'TestOrimi.view.ItemselectorFla',               //окно выбора аромата   "Главное окно"
        'TestOrimi.view.general_gridLbar',              //левый бар окна        "Главное окно"
        'TestOrimi.view.createTTWindow',                //создание окна         "Тор. точки"    //теперь все в одном окне йахуу
        'TestOrimi.view.TTFirstTab',                    //шапка окна            "Тор. точки"
        'TestOrimi.view.TTSecondTab',                    //шапка окна            "Тор. точки"

        'TestOrimi.view.createDistrWindow',
        'TestOrimi.view.DistrSecondTab',
        'TestOrimi.view.createWrhsWindow',

        'TestOrimi.view.NetWindow',               //создание окна         "Сети"
        'TestOrimi.view.AddNetWindow',            //создание окна         "Добавление сети"
        'TestOrimi.view.ParentNetWindow',
        'TestOrimi.view.createStaffWindow',             //создание окна         "Сотрудники"
        'TestOrimi.view.CreateSearchWindowSku',         //создание окна         "Комбинированный поиск - Номенклатура"
        'TestOrimi.view.CreateSearchWindowTT',          //создание окна         "Комбинированный поиск - Тор. точки"
        'TestOrimi.view.CreateSearchWindowStaff',       //создание окна         "Комбинированный поиск - Сотрудники"
        'TestOrimi.view.CreateSearchWindowDistr',
        'TestOrimi.view.barcodePanel',                  //                      "Штрихкод"
        'TestOrimi.view.general_gridTT',                //главная таблица       "Главное окно Торговые точ."
        'TestOrimi.view.general_tabpanelTT',            //панель с вкладками    "Главное окно Торговые точ."
        'TestOrimi.view.general_tabpanelFirstcolTT',    //первая вкладка окна   "Главное окно Торговые точки"
        'TestOrimi.view.general_tabpanelSecondcolTT',   //вторая вкладка окна   "Главное окно Торговые точки"
        'TestOrimi.view.general_gridStaff',             //главная таблица       "Главное окно Сотрудники"
        'TestOrimi.view.general_tabpanelStaff',         //панель с вкладками    "Главное окно Сотрудники"
        'TestOrimi.view.general_tabpanelSecondcolStaff', //вторая кладка        "Главное окно Сотрудники"
        'TestOrimi.view.general_tabpanelFirstcolStaff',  //первая вкладка       "Главное окно Сотрудники"
        'TestOrimi.view.SegmentWindow',                 //создание подоконника  "Сегмент"
        'TestOrimi.view.SubSegmentWindow',              //создание подоконника  "Сабсегмент"
        'TestOrimi.view.AddSubSegmentWindow',           //добавление сабсегмента
        'TestOrimi.view.AddSegmentWindow',              //добавление сегмента
        'TestOrimi.view.general_tabpanelLbar',          //левый бар 2 кнопки    "Панель вкладок Привязать Отвязать"
        'TestOrimi.view.general_tabpanelsecLbar',       //левый бар 4 кнопки    "Панель вкладок Привязать Отвязать"
        'TestOrimi.view.EventHistory',                  //создание окна         "Просмотр истории раоты с позицией"
        'TestOrimi.view.createStylegroupTT',            //создание окна         "Групповая установка свойств"
        'TestOrimi.view.createStylegroupStaff',         //создание окна         "Групповая установка свойств"
        'TestOrimi.view.createStylegroupSku',           //создание окна         "Групповая установка свойств"
        'TestOrimi.view.createStylegroupDistr',
        'TestOrimi.view.createStylegroupWrhs',
        'TestOrimi.view.TemplateSettings',              //создание окна         "Шаблоны"
        'TestOrimi.view.ParentID',                      //создание окна         "Агрегирующая позиция"
        'TestOrimi.view.WeightGroupID',                 //создание окна         "Вес (группа)"
        'TestOrimi.view.AddWeightGroupIDWindow',        //добавление данных     "Веса(группы)"
        'TestOrimi.view.AddingWindow',                  //создание таблицы      "Добавки"
        'TestOrimi.view.AddAddingWindow',               //добавление данных     "Добавки"
        'TestOrimi.view.FlavorWindow',                  //создание таблицы      "Ароматы"
        'TestOrimi.view.AddFlavorWindow',               //добавление данных     "Ароматы"
        'TestOrimi.view.ParentStaffWindow',
        //'TestOrimi.view.RespMerchWindow',
        //'TestOrimi.view.StaffWindow',
        'TestOrimi.view.SectorWindow',
        'TestOrimi.view.AddSectorWindow',
        'TestOrimi.view.AreaWindow',
        'TestOrimi.view.AddAreaWindow',
        'TestOrimi.view.DivisionWindow',
        'TestOrimi.view.AddDivisionWindow',
        'TestOrimi.view.SubChannelWindow',
        'TestOrimi.view.AddSubChannelWindow',
        'TestOrimi.view.CountyWindow',
        //'TestOrimi.view.CountryWindow',
        'TestOrimi.view.RegionWindow',
        'TestOrimi.view.DistrictWindow',
        'TestOrimi.view.CityWindow',
        //'TestOrimi.view.StreetWindow',
        //'TestOrimi.view.AddCountryWindow',
        'TestOrimi.view.AddCountyWindow',
        //'TestOrimi.view.AddCountryWindow',
        'TestOrimi.view.AddRegionWindow',
        'TestOrimi.view.AddDistrictWindow',
        'TestOrimi.view.AddCityWindow',
        //'TestOrimi.view.AddStreetWindow',
        'TestOrimi.view.SectorPanel',
        'TestOrimi.view.AreaPanel',
        'TestOrimi.view.MotivationWindow',
        'TestOrimi.view.AddMotivationWindow',
        'TestOrimi.view.ManagersWindow',
        'TestOrimi.view.AddManagersWindow',
        'TestOrimi.view.CreateSkuGroupSyncWindow',
        'TestOrimi.view.CreateTTGroupSyncWindow',
        'TestOrimi.view.CreateStaffGroupSyncWindow',
        'TestOrimi.view.CreateWrhsGroupSyncWindow',
        'TestOrimi.view.GeneralAttributesPanel',
        //'TestOrimi.view.CreateAttributesWindowSku',          //создание окна "Панель справочников - Номенклатура"
        //'TestOrimi.view.CreateAttributesWindowTT',          //создание окна "Панель справочников - Тор. точки"
        //'TestOrimi.view.CreateAttributesWindowStaff',          //создание окна "Панель справочников - Сотрудники"
        'TestOrimi.view.wrhs_general_grid',
        'TestOrimi.view.wrhs_general_tabpanel',
        'TestOrimi.view.wrhs_general_tabpanelFirstcol',
        'TestOrimi.view.wrhs_general_tabpanelSecondcol',
        'TestOrimi.view.DistributorWindow',
        'TestOrimi.view.CreateSearchWindowWrhs',
        'TestOrimi.view.AdvertisingViewportPanel',
        'TestOrimi.view.AdvertisingBossPanel',
        'TestOrimi.view.AdvertisingBossHeadersGrid',
        'TestOrimi.view.AdvertisingBossRowsGrid',
        'TestOrimi.view.AdvertisingEventsPanel',
        'TestOrimi.view.AdvertisingEventsHeadersGrid',
        'TestOrimi.view.AdvertisingEventsRowsGrid',
        'TestOrimi.view.CreateAdvertisingEventWindow',
        'TestOrimi.view.CreateAdvertisingEventRowWindow',
        'TestOrimi.view.CreateAdvertisingBossWindow',
        'TestOrimi.view.AdvertisingChannelWindow',
        'TestOrimi.view.AddAdvertisingChannelWindow',
        'TestOrimi.view.StoreModalWindow',
        'TestOrimi.view.CreateSearchWindowAdvertisingEventHeader',
        'TestOrimi.view.CreateSearchWindowAdvertisingEventRows'
    ],

    controllers: [
        'Main'
    ],

    models: [
        'TestOrimi.model.MasterGoodModel'
        , 'TestOrimi.model.MasterStoresModel'
        , 'TestOrimi.model.MasterStaffModel'
        , 'TestOrimi.model.SyncedGoodModel'
        , 'TestOrimi.model.SlaveGoodModel'
        , 'TestOrimi.model.SimpleAttributeModel'
        , 'TestOrimi.model.BrandModel'
        , 'TestOrimi.model.SubBrandModel'
        , 'TestOrimi.model.SegmentModel'
        , 'TestOrimi.model.SubSegmentModel'
        , 'TestOrimi.model.TemplateModel'
        , 'TestOrimi.model.BarcodeModel'
        , 'TestOrimi.model.WeightGroupModel'
        , 'TestOrimi.model.SelectedAddingModel'
        , 'TestOrimi.model.SelectedFlavorModel'
        , 'TestOrimi.model.SyncedStoresModel'
        , 'TestOrimi.model.SlaveStoresModel'
        , 'TestOrimi.model.SyncedStaffModel'
        , 'TestOrimi.model.SlaveStaffModel'
        , 'TestOrimi.model.NetModel'
        , 'TestOrimi.model.SectorModel'
        , 'TestOrimi.model.AreaModel'
        , 'TestOrimi.model.DivisionModel'
        , 'TestOrimi.model.SubChannelModel'
        , 'TestOrimi.model.CountyModel'
        , 'TestOrimi.model.RegionModel'
        , 'TestOrimi.model.DistrictModel'
        , 'TestOrimi.model.CityModel'
        , 'TestOrimi.model.MotivationModel'
        , 'TestOrimi.model.SelectedSectorModel'
        , 'TestOrimi.model.SelectedAreaModel'
        , 'TestOrimi.model.ManagersModel'
        , 'TestOrimi.model.DublicateDataModel'
        , 'TestOrimi.model.GoodsSyncResultModel'
        , 'TestOrimi.model.TextValueModel'
        , 'TestOrimi.model.GoodsFieldsModel'
        , 'TestOrimi.model.StoresSyncResultModel'
        , 'TestOrimi.model.StoresFieldsModel'
        , 'TestOrimi.model.StaffSyncResultModel'
        , 'TestOrimi.model.StaffFieldsModel'
        , 'TestOrimi.model.MasterWarehousesModel'
        , 'TestOrimi.model.SlaveWarehousesModel'
        , 'TestOrimi.model.SyncedWarehousesModel'
        , 'TestOrimi.model.WarehousesSyncResultModel'
        , 'TestOrimi.model.TextValueStringModel'
        , 'TestOrimi.model.AdvertisingBossHeadersModel'
        , 'TestOrimi.model.AdvertisingBossRowsModel'
        , 'TestOrimi.model.AdvertisingEventsHeadersModel'
        , 'TestOrimi.model.AdvertisingEventsRowsModel'
        , 'TestOrimi.model.BossOrderStatusModel'
    ],

    stores: [
        'TestOrimi.store.MasterGoodStore'              //    "Мастер справочник"
        , 'TestOrimi.store.SyncedGoodStore'			    //	  Отвязать
        , 'TestOrimi.store.SlaveGoodStore'			    //	  Привязать
        , 'TestOrimi.store.GoodParentStore'			    //	  ParentID
        , 'TestOrimi.store.EventHistoryStore'			//	  История действий с позицией
        , 'TestOrimi.store.AddingStore'                  //    "Добавки"
        , 'TestOrimi.store.FlavorStore'                  //    "Ароматы"
        , 'TestOrimi.store.BrandStore'                   //    "Бренд"
        , 'TestOrimi.store.CategoryStore'                //    "Категории"
        , 'TestOrimi.store.CoffeeSortStore'              //    "Сорт офе"
        , 'TestOrimi.store.CountryStore'                 //    "Страна"
        , 'TestOrimi.store.ExternalPackStore'            //    "Внешняя упаковка"
        , 'TestOrimi.store.GiftStore'                    //    "Подарок"
        , 'TestOrimi.store.GiftTypeStore'                //    "Тип подарка"
        , 'TestOrimi.store.InternalPackStore'            //    "Внутренняя упаковка"
        , 'TestOrimi.store.ManufacturerStore'            //    "Прозводитель"
        , 'TestOrimi.store.PriceSegmentStore'            //    "Ценовой сегмент"
        , 'TestOrimi.store.SachetPackStore'              //    "Упаковка Саше"
        , 'TestOrimi.store.SegmentStore'                 //    "Сегмент"
        , 'TestOrimi.store.SubBrandStore'                //    "Саббренд"
        , 'TestOrimi.store.SubSegmentStore'              //    "Сабсегмент"
        , 'TestOrimi.store.TeaColorStore'                //    "Цвет чая"
        , 'TestOrimi.store.WeightGroupStore'             //    "Весовая группа"
        , 'TestOrimi.store.ChannelStore'                 //    "Канал"
        , 'TestOrimi.store.SubChannelStore'              //    "Подканал"
        , 'TestOrimi.store.FormatStore'                  //    "Формат"
        , 'TestOrimi.store.NetTypeStore'                 //    "Тип сети"
        , 'TestOrimi.store.NetStore'                 //    "Сеть"
        , 'TestOrimi.store.ParentNetStore'
        , 'TestOrimi.store.NonAccReasonStore'
        , 'TestOrimi.store.TemplateStore'
        , 'TestOrimi.store.BarcodeStore'
        , 'TestOrimi.store.SelectedAddingStore'
        , 'TestOrimi.store.SelectedFlavorStore'
        , 'TestOrimi.store.MasterStoresStore'
        , 'TestOrimi.store.MasterDistrStore'
        , 'TestOrimi.store.DivisionStore'
        , 'TestOrimi.store.SectorStore'
        , 'TestOrimi.store.AreaStore'
        , 'TestOrimi.store.TradentDepartmentStore'
        //,'TestOrimi.store.RespMerchStore'
        //,'TestOrimi.store.RespTRStore'
        , 'TestOrimi.store.ContractorGroupStore'
        , 'TestOrimi.store.CurrancyStore'
        , 'TestOrimi.store.MasterStaffStore'
        , 'TestOrimi.store.TypeStaffStore'
        , 'TestOrimi.store.MotivationStore'
        , 'TestOrimi.store.ParentStore'
        , 'TestOrimi.store.SyncedStoresStore'
        , 'TestOrimi.store.SlaveStoresStore'
        , 'TestOrimi.store.SyncedStaffStore'
        , 'TestOrimi.store.SlaveStaffStore'
        , 'TestOrimi.store.CountyStore'
        , 'TestOrimi.store.RegionStore'
        , 'TestOrimi.store.DistrictStore'
        , 'TestOrimi.store.CityStore'
        //,'TestOrimi.store.StreetStore'
        //,'TestOrimi.store.StaffStore'
        //,'TestOrimi.store.ParentStore'
        , 'TestOrimi.store.SelectedSectorStore'
        , 'TestOrimi.store.SelectedAreaStore'
        , 'TestOrimi.store.ManagersStore'
        , 'TestOrimi.store.MasterGoodsFieldsStore'
        , 'TestOrimi.store.SlaveGoodsFieldsStore'
        , 'TestOrimi.store.DataSourceStore'
        , 'TestOrimi.store.GoodsSyncResultStore'
        , 'TestOrimi.store.MasterStoresFieldsStore'
        , 'TestOrimi.store.StoresSyncResultStore'
        , 'TestOrimi.store.MasterStaffFieldsStore'
        , 'TestOrimi.store.StaffSyncResultStore'
        , 'TestOrimi.store.SlaveStaffFieldsStore'
        , 'TestOrimi.store.SlaveStoresFieldsStore'
        , 'TestOrimi.store.NetSalesDataSourceStore'

        , 'TestOrimi.store.MasterWarehousesStore'
        , 'TestOrimi.store.SlaveWarehousesStore'
        , 'TestOrimi.store.SyncedWarehousesStore'
        , 'TestOrimi.store.DistributorsStore'
        , 'TestOrimi.store.WarehousesSyncResultStore'
        , 'TestOrimi.store.MasterWarehousesFieldsStore'
        , 'TestOrimi.store.SlaveWarehousesFieldsStore'
        , 'TestOrimi.store.AdvertisingBossHeadersStore'
        , 'TestOrimi.store.AdvertisingBossRowsStore'
        , 'TestOrimi.store.AdvertisingEventsHeadersStore'
        , 'TestOrimi.store.AdvertisingEventsRowsStore'
        , 'TestOrimi.store.RequestStatusStore'
        , 'TestOrimi.store.OrimiGoodsStore'
        , 'TestOrimi.store.AvailableAreaStore'
        , 'TestOrimi.store.BossOrderStatusStore'
        , 'TestOrimi.store.AdvertisingChannelStore'
        , 'TestOrimi.store.ActiveStoresStore'
        , 'TestOrimi.store.PaymentTypeStore'
    ],

    requires: [
        'Ext.window.*'
        , 'Ext.ux.window.Notification'
    ],

    init: function () {
        var me = this;

        Ext.setGlyphFontFamily('Pictos');
        Ext.tip.QuickTipManager.init();
        Ext.Ajax.request({
            url: 'resources/data/api.php?act=getPortalData',
            timeout: 600000,
            success: function (response) {
                if (Ext.JSON.decode(response.responseText)) {
                    me.data = Ext.JSON.decode(response.responseText)[0];
                    //console.dir(me.data);
                    Ext.create("TestOrimi.view.Viewport");
                } else {
                    Ext.Msg.alert('Ошибка!', 'Нет связи с сервером');
                }
            },
            failure: function (form, action) {
                Ext.Msg.alert('Ошибка!', 'Нет связи с сервером');
            }
        });
    }
});

//fix for chrome
Ext.apply(Ext.EventManager, {
    normalizeEvent: function (eventName, fn) {

        //start fix
        var EventManager = Ext.EventManager,
            supports = Ext.supports;
        if (Ext.chromeVersion >= 43 && eventName == 'mouseover') {
            var origFn = fn;
            fn = function () {
                var me = this,
                    args = arguments;
                setTimeout(
                    function () {
                        origFn.apply(me || Ext.global, args);
                    },
                    0);
            };
        }
        //end fix

        if (EventManager.mouseEnterLeaveRe.test(eventName) && !supports.MouseEnterLeave) {
            if (fn) {
                fn = Ext.Function.createInterceptor(fn, EventManager.contains);
            }
            eventName = eventName == 'mouseenter' ? 'mouseover' : 'mouseout';
        } else if (eventName == 'mousewheel' && !supports.MouseWheel && !Ext.isOpera) {
            eventName = 'DOMMouseScroll';
        }
        return {
            eventName: eventName,
            fn: fn
        };
    }
});

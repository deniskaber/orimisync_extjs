Ext.define('TestOrimi.store.TemplateStore', {
    extend: 'Ext.data.Store',    
	model: 'TestOrimi.model.TemplateModel',
    
	pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
		extraParams: {
			act: 'getTemplate'
		}
    },
    autoLoad: false,
    storeId: 'TemplateStore'
});

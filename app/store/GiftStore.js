Ext.define('TestOrimi.store.GiftStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getGift'
		}
	},
	autoLoad: false,
	storeId: 'GiftStore',
	filters: [{
        id: 'GiftStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

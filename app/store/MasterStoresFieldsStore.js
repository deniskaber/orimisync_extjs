Ext.define('TestOrimi.store.MasterStoresFieldsStore', {
	extend: 'Ext.data.ArrayStore',
	pageSize: 100,
	model: 'TestOrimi.model.StoresFieldsModel',
	data: [
		['Код',               'Code'],
		['Название',          'Name'],
		['Черновой адрес',    'TempAddress'],
		['Округ',             'County'],
		['Регион',            'Region'],
		['Город',             'City'],
		['Полный адрес',      'FullAddress'],
		['Адрес',             'Address'],
		['Дивизион',          'Division'],
		['Участок',           'Sector'],
		['Канал',             'Channel'],
		['Подканал',          'SubChannel'],
		['Формат',            'Format'],
		['Сеть',              'isNetName'],
		['Название сети',       'Net'],
		['Группа контрагентов', 'ContractorGroup'],
		['Комментарий',         'Comment'],
		['Участвует в РБП',     'isInRBP'],
		['Источник данных',     'DataSource']
	],
	storeId: 'MasterStoresFieldsStore'
});
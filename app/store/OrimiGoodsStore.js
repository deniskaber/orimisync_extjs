Ext.define('TestOrimi.store.OrimiGoodsStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.MasterGoodModel',
    pageSize: 100000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getOrimiGoods'
        }
    },
    autoLoad: false,
    storeId: 'OrimiGoodsStore'
});

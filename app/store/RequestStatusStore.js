Ext.define('TestOrimi.store.RequestStatusStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getRequestStatus'
		}
	},
	autoLoad: false,
	storeId: 'RequestStatusStore',
	filters: [{
        id: 'RequestStatusStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

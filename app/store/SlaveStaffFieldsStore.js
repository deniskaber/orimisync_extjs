Ext.define('TestOrimi.store.SlaveStaffFieldsStore', {
	extend: 'Ext.data.ArrayStore',
	pageSize: 100,
	model: 'TestOrimi.model.StaffFieldsModel',
	data: [
		['Код', 						'Code' 			],
		['ФИО', 						'FIO' 			],
		['Тип',							'Type'			],
		['Кому подчиняется', 			'Parent' 		],
		['Принадлежность к дивизиону', 	'Division' 		],
		['Участок', 					'Sector' 		],
		['Дата рождения', 				'BirthDate' 	],
		['Дата начала работы', 			'DateHired'		],
		['Дата увольнения', 			'DateFired' 	],
		['E-mail', 						'EMail' 		],
		['Телефон', 					'Phone' 		],
		['Мотивация', 					'Motivation' 	],
		['Признак', 					'Feature' 		],
		['Источник данных',             'DataSource'    ]	
	],
	storeId: 'SlaveStaffFieldsStore'
});
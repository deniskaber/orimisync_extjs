Ext.define('TestOrimi.store.SachetPackStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getSachetPack'
		}
	},
	autoLoad: false,
	storeId: 'SachetPackStore',
	filters: [{
        id: 'SachetPackStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

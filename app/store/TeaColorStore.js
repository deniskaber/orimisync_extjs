Ext.define('TestOrimi.store.TeaColorStore', {
	extend: 'Ext.data.Store',
	model: 'TestOrimi.model.SimpleAttributeModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000,
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getTeaColor'
		}
	},
	autoLoad: false,
	storeId: 'TeaColorStore',
	filters: [{
		id: 'TeaColorStore-deleted-filter',
		property : 'Deleted',
		value: 0
	}]
});
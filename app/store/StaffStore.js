Ext.define('TestOrimi.store.StaffStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.MasterStaffModel',
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
        timeout: 600000, 
        simpleSortMode: true,
        noCache: false,
        reader: {
            type: 'json'/*,
            root: 'items',
            totalProperty: 'total'*/
        },
        extraParams: {
            act: 'getStaff',
            searchall: ''
        }
    },
    /*remoteSort: true,
    remoteFilter: true, */
    autoLoad: false,
    storeId: 'StaffStore'
});

Ext.define('TestOrimi.store.SubBrandStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SubBrandModel',
	pageSize: 10000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getSubBrand'
		}
	},
    autoLoad: false,
    storeId: 'SubBrandStore',
	filters: [{
        id: 'SubBrandStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

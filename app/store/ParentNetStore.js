Ext.define('TestOrimi.store.ParentNetStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.NetModel',
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getParentNet'
        }
    },
    autoLoad: false,
    storeId: 'ParentNetStore'
});

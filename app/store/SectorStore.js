Ext.define('TestOrimi.store.SectorStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SectorModel',
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getSector'
        }
    },
    autoLoad: false,
    storeId: 'SectorStore',
    filters: [{
        id: 'SectorStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

Ext.define('TestOrimi.store.RespMerchStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getRespMerch'
        }
    },
    autoLoad: false,
    storeId: 'RespMerchStore',
    filters: [{
        id: 'RespMerchStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

Ext.define('TestOrimi.store.AdvertisingEventsRowsStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.AdvertisingEventsRowsModel',
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
        timeout: 600000, 
        simpleSortMode: true,
        noCache: false,
        reader: {
            type: 'json',
            root: 'items',
            totalProperty: 'total'
        },
        extraParams: {
            act: 'getAdvertisingEventsRows',
            subaction: 'get',
            searchall: ''
        }
    },
    remoteSort: true,
    remoteFilter: true, 
    autoLoad: false,
    storeId: 'AdvertisingEventsRowsStore'
});

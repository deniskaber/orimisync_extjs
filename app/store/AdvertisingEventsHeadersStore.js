Ext.define('TestOrimi.store.AdvertisingEventsHeadersStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.AdvertisingEventsHeadersModel',

    constructor: function(config) {
        config = Ext.Object.merge({}, config);
        var me = this;

        // do what you need with the given config object (even deletes) before passing it to the parent contructor
         me.callParent([config]);
         // use me forth on cause the config object is now fully applied

        me.on('load', function( self, records, successful, eOpts) {
            if (self.nextRec)  {
                var page = Math.ceil( self.getTotalCount() / self.getCount() );
                self.loadPage(page);
                self.nextRec = false;
            } 
        })
    },
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
        timeout: 600000, 
        simpleSortMode: true,
        noCache: false,
        reader: {
            type: 'json',
            root: 'items',
            totalProperty: 'total'
        },
        extraParams: {
            act: 'getAdvertisingEventsHeaders',
            searchall: ''
        }
    },
    remoteSort: true,
    remoteFilter: true, 
    autoLoad: false,
    nextRec: false, // The property specifies the transition to the end of the table
    storeId: 'AdvertisingEventsHeadersStore'
});

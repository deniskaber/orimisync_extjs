Ext.define('TestOrimi.store.NonAccReasonStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000,
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getNonAccReason'
        }
    },
    autoLoad: false,
    storeId: 'NonAccReasonStore',
    filters: [{
        id: 'NonAccReasonStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

Ext.define('TestOrimi.store.SlaveGoodsFieldsStore', {
	extend: 'Ext.data.ArrayStore',
	pageSize: 100,
	model: 'TestOrimi.model.GoodsFieldsModel',
	data: [
		['Код', 					'Code' 			],
		['Код Орими', 				'OrimiCode' 	],
		['Название', 				'Name' 			],
		['Продукция от', 			'From' 			],
		['Категория',				'Category'		],
		['Производитель', 			'Manufacturer' 	],
		['Бренд', 					'Brand' 		],
		['Сегмент', 				'Segment' 		],
		['Цвет чая', 				'TeaColor' 		],
		['Тип упаковки', 			'Pack'			],
		['Количество пакетов', 		'BagQty' 		],
		['Вес пакета', 				'BagWeight' 	],
		['Вес', 					'Weight' 		],
		['Ценовой сегмент', 		'PriceSegment' 	],
		['Страна происхождения', 	'Country' 		],
		['Источник данных', 		'DataSource'	]		
	],
	storeId: 'SlaveGoodsFieldsStore'
});
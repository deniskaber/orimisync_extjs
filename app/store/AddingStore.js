Ext.define('TestOrimi.store.AddingStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
    
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getAdding'
        }
    },
    autoLoad: false,
    storeId: 'AddingStore'
});

Ext.define('TestOrimi.store.DivisionStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.DivisionModel',
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getDivision'
        }
    },
    autoLoad: false,
    storeId: 'DivisionStore',
    filters: [{
        id: 'DivisionStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

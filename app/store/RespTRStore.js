Ext.define('TestOrimi.store.RespTRStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getRespTR'
        }
    },
    autoLoad: false,
    storeId: 'RespTRStore',
    filters: [{
        id: 'RespTRStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

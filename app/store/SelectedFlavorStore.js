Ext.define('TestOrimi.store.SelectedFlavorStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SelectedFlavorModel',
    
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getSelectedFlavor'
        }
    },
    autoLoad: false,
    storeId: 'SelectedFlavorStore'
});

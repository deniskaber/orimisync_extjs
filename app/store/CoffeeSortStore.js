Ext.define('TestOrimi.store.CoffeeSortStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getCoffeeSort'
		}
	},
    autoLoad: false,
    storeId: 'CoffeeSortStore',
	filters: [{
		id: 'CoffeeSortStore-deleted-filter',
		property : 'Deleted',
		value: 0
	}]
});

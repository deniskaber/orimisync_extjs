Ext.define('TestOrimi.store.MasterWarehousesFieldsStore', {
    extend: 'Ext.data.ArrayStore',
    pageSize: 100,
    model: 'TestOrimi.model.TextValueStringModel',
    data: [
        ['Код', 'Code'],
        ['Название', 'Name'],
        ['Дистрибьютор', 'Distributor'],
        ['Черновой адрес', 'TempAddress'],
        ['Округ', 'County'],
        ['Регион', 'Region'],
        ['Город', 'City'],
        ['Полный адрес', 'FullAddress'],
        ['Адрес', 'Address'],
        ['Дивизион', 'Division'],
        ['Участок', 'Sector'],
        ['Формат', 'Format'],
        ['Комментарий', 'Comment'],
        ['Источник данных', 'DataSource']
    ],
    storeId: 'MasterWarehousesFieldsStore'
});
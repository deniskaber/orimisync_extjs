Ext.define('TestOrimi.store.TypeStaffStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getTypeStaff'
        }
    },
    autoLoad: false,
    storeId: 'TypeStaffStore',
    filters: [{
        id: 'TypeStaffStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

Ext.define('TestOrimi.store.GoodsSyncResultStore', {
	extend: 'Ext.data.Store',
	model: 'TestOrimi.model.GoodsSyncResultModel',
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
		timeout: 600000,
		noCache: false,
		reader: {
			type: 'json',
			root: 'items',
			totalProperty: 'total'
		},
		extraParams: {
			act: 'getGroupSyncGoods'
		}
	},
	autoLoad: false,
	storeId: 'GoodsSyncResultStore'
});

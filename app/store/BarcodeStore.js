Ext.define('TestOrimi.store.BarcodeStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.BarcodeModel',

    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000,
        noCache: false,
        reader: {
            type: 'json'
        }/*,
        extraParams: {
            act: 'getBarcode'
        }*/
    },
    autoLoad: false,
    storeId: 'BarcodeStore'
});

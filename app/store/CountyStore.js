Ext.define('TestOrimi.store.CountyStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.CountyModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getCounty'
		}
	},
    autoLoad: false,
    storeId: 'CountyStore',
	filters: [{
        id: 'CountyStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

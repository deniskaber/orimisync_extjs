Ext.define('TestOrimi.store.CountryStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getCountry'
		}
	},
    autoLoad: false,
    storeId: 'CountryStore',
	filters: [{
		id: 'CountryStore-deleted-filter',
		property : 'Deleted',
		value: 0
	}]
});

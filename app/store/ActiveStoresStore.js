Ext.define('TestOrimi.store.ActiveStoresStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.MasterStoresModel',
    pageSize: 25,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        actionMethods: {
            create: 'POST',
            read: 'POST',
            update: 'POST',
            destroy: 'POST'
        },
        timeout: 600000,
        simpleSortMode: true,
        noCache: false,
        reader: {
            type: 'json',
            root: 'items',
            totalProperty: 'total'
        },
        extraParams: {
            act: 'getActiveStores'
        }
    },
    remoteSort: true,
    remoteFilter: true,
    autoLoad: false,
    storeId: 'ActiveStoresStore'
});

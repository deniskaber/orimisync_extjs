Ext.define('TestOrimi.store.CityStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.CityModel',
	pageSize: 12,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
		timeout: 600000,
		simpleSortMode: true,
		noCache: false,
		reader: {
			type: 'json',
			root: 'items',
			totalProperty: 'total'
		},
		extraParams: {
			act: 'getCity'
		}
	},
	remoteSort: true,
	remoteFilter: true,
    autoLoad: false,
    storeId: 'CityStore',
	filters: [{
		id: 'CityStore-deleted-filter',
		property : 'Deleted',
		value: 0
	}]/*,
	listeners: {
		filterchange: function (store, filters) {
			console.dir(filters);
		}
	}*/
});
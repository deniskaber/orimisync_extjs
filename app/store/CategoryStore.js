Ext.define('TestOrimi.store.CategoryStore', {
    extend: 'Ext.data.Store',    
	model: 'TestOrimi.model.SimpleAttributeModel',    
	pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
		extraParams: {
			act: 'getCategory'
		}
    },
    autoLoad: false,
    storeId: 'CategoryStore',
	filters: [{
        id: 'CategoryStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

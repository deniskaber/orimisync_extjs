Ext.define('TestOrimi.store.ParentStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.MasterStaffModel',
    pageSize: 100000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getParentStaff'
        }
    },
    autoLoad: false,
    storeId: 'ParentStore'
});

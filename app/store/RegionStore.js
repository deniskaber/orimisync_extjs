Ext.define('TestOrimi.store.RegionStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.RegionModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getRegion'
		}
	},
    autoLoad: false,
    storeId: 'RegionStore',
	filters: [{
        id: 'RegionStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

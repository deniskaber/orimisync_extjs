Ext.define('TestOrimi.store.BossOrderStatusStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getBossOrderStatus'
		}
	},
	autoLoad: false,
	storeId: 'BossOrderStatusStore',
	filters: [{
        id: 'BossOrderStatusStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

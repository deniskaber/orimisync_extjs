Ext.define('TestOrimi.store.DataSourceStore', {
	extend: 'Ext.data.ArrayStore',
	pageSize: 100,
	fields: ['value','text'],
	data: [
		[ 0, 'Все источники'	],
		[ 1, 'TradeNT'			],
		[ 2, 'Сети' 			],
		[ 3, 'Дистрибьюторы'	]		
	],
	storeId: 'DataSourceStore'
});
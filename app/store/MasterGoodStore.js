Ext.define('TestOrimi.store.MasterGoodStore', {
	extend: 'Ext.data.Store',
	model: 'TestOrimi.model.MasterGoodModel',
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
		timeout: 600000,
		simpleSortMode: true,
		noCache: false,
		reader: {
			type: 'json',
			root: 'items',
			totalProperty: 'total'
		},
		extraParams: {
			act: 'getMasterGoods',
			searchall: ''
		}
	},
	remoteSort: true,
	remoteFilter: true,
	autoLoad: false,
	storeId: 'MasterGoodStore'
});

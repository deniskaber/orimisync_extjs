Ext.define('TestOrimi.store.NetSalesDataSourceStore', {
	extend: 'Ext.data.Store',
	model: 'TestOrimi.model.SimpleAttributeModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000,
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getNetSalesDataSource'
		}
	},
	autoLoad: false,
	storeId: 'NetSalesDataSourceStore'
});

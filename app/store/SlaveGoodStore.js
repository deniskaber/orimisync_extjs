Ext.define('TestOrimi.store.SlaveGoodStore', {
	extend: 'Ext.data.Store',
	model: 'TestOrimi.model.SlaveGoodModel',
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
		timeout: 600000,
		simpleSortMode: true,
		noCache: false,
		reader: {
			type: 'json',
			root: 'items',
			totalProperty: 'total'
		},
		extraParams: {
			act: 'getSlaveGoods',
			searchall: ''
		}
	},
	remoteSort: true,
	remoteFilter: true,
	autoLoad: false,
	storeId: 'SlaveGoodStore'
});

Ext.define('TestOrimi.store.SelectedSectorStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SelectedSectorModel',
    
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getSelectedSector'
        }
    },
    autoLoad: false,
    storeId: 'SelectedSectorStore'
});

Ext.define('TestOrimi.store.ManagersStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.ManagersModel',
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getManagers'
        }
    },
    autoLoad: false,
    storeId: 'ManagersStore'
});

Ext.define('TestOrimi.store.GiftTypeStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getGiftType'
		}
	},
	autoLoad: false,
	storeId: 'GiftTypeStore',
	filters: [{
        id: 'GiftTypeStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

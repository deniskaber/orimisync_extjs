Ext.define('TestOrimi.store.SlaveStoresFieldsStore', {
	extend: 'Ext.data.ArrayStore',
	pageSize: 100,
	model: 'TestOrimi.model.StoresFieldsModel',
	data: [
		['Код', 				'Code' 				],
		['Код Дистр.',        	'DistrCode'			],
		['Название', 			'Name' 				],
		['Черновой адрес',		'TempAddress'		],
		['Округ', 				'County' 			],
		['Регион', 				'Region' 			],
		['Город', 				'City' 				],
		['Дивизион', 			'Division' 			],
		['Участок', 			'Sector'			],
		['Подканал', 			'SubChannel' 		],
		['Формат', 				'Format' 			],
		['Сеть', 				'isNet' 			],
		['Название сети', 		'Net' 				],
		['Группа контрагентов', 'ContractorGroup' 	],
		['Источник данных', 	'DataSource'		]			
	],
	storeId: 'SlaveStoresFieldsStore'
});
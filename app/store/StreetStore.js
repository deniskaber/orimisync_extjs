Ext.define('TestOrimi.store.StreetStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.StreetModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getStreet'
		}
	},
    autoLoad: false,
    storeId: 'StreetStore',
	filters: [{
        id: 'StreetStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

Ext.define('TestOrimi.store.DistrictStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.DistrictModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getDistrict'
		}
	},
    autoLoad: false,
    storeId: 'DistrictStore',
	filters: [{
        id: 'DistrictStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

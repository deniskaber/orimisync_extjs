Ext.define('TestOrimi.store.DistributorsStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.MasterDistrModel',
    pageSize: 100000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getDistributors'
        }
    },
    autoLoad: false,
    storeId: 'DistributorsStore'
});

Ext.define('TestOrimi.store.EventHistoryStore', {
    extend: 'Ext.data.Store',
	pageSize: 1000,
	fields: [
		{name: 'Date',		type: 'string'}
		,{name: 'Event',	type: 'string'}
		,{name: 'Author',	type: 'string'}
	],
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getEventHistory'
		}
	},
	autoLoad: false,
	storeId: 'EventHistoryStore'
});

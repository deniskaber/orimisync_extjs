Ext.define('TestOrimi.store.SyncedStaffStore', {
  extend: 'Ext.data.Store',
  model: 'TestOrimi.model.SyncedStaffModel',
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
		timeout: 600000,
		simpleSortMode: true,
		noCache: false,
		reader: {
			type: 'json',
			root: 'items',
			totalProperty: 'total'
		},
		extraParams: {
			act: 'getSyncedStaff',
			searchall: ''
		}
	},
  remoteSort: true,
	remoteFilter: true,
	autoLoad: false,
  storeId: 'SyncedStaffStore'
});

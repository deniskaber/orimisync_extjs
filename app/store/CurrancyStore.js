Ext.define('TestOrimi.store.CurrancyStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getCurrancy'
		}
	},
    autoLoad: false,
    storeId: 'CurrancyStore',
	filters: [{
		id: 'CurrancyStore-deleted-filter',
		property : 'Deleted',
		value: 0
	}]
});

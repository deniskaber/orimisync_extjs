Ext.define('TestOrimi.store.GoodParentStore', {
	extend: 'Ext.data.Store',
	model: 'TestOrimi.model.MasterGoodModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000,
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getParentGoods'
		}
	},
    autoLoad: false,
	storeId: 'GoodParentStore'/*,
  	filters: [{
        id: 'GoodParentStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]*/
});

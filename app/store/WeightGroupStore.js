Ext.define('TestOrimi.store.WeightGroupStore', {
    extend: 'Ext.data.Store',
	model: 'TestOrimi.model.WeightGroupModel',
    pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getWeightGroup'
		}
	},
    autoLoad: false,
    storeId: 'WeightGroupStore',
	filters: [{
        id: 'WeightGroupStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

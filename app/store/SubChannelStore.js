Ext.define('TestOrimi.store.SubChannelStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SubChannelModel',
    pageSize: 1000,
	proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getSubChannel'
        }
    },
	autoLoad: false,
    storeId: 'SubChannelStore',
    filters: [{
        id: 'SubChannelStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]/*,
    listeners: {
        filterchange: function (store, filters) {
            console.dir(filters);
        }
    }*/
});

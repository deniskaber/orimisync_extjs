Ext.define('TestOrimi.store.ExternalPackStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getExternalPack'
		}
	},
	autoLoad: false,
	storeId: 'ExternalPackStore',
	filters: [{
        id: 'ExternalPackStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

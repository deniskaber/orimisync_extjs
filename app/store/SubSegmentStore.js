Ext.define('TestOrimi.store.SubSegmentStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SubSegmentModel',
    pageSize: 1000,
    proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getSubSegment'
		}
	},
    autoLoad: false,
    storeId	: 'SubSegmentStore',
	filters: [{
        id: 'SubSegmentStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

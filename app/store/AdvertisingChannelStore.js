Ext.define('TestOrimi.store.AdvertisingChannelStore', {
    extend: 'Ext.data.Store',
	fields: [
		{name: 'ID',				type: 'int'}
		,{name: 'Name',				type: 'string'}
		,{name: 'ShortName',		type: 'string'}
		,{name: 'BriefNotation',	type: 'string'}
		,{name: 'Deleted',			type: 'int'}

		,{name: 'MaxNumberToShip',	type: 'int'}
	],
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getAdvertisingChannel'
		}
	},
    autoLoad: false,
    storeId: 'AdvertisingChannelStore',
	filters: [{
        id: 'AdvertisingChannelStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

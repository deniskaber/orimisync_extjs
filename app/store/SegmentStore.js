Ext.define('TestOrimi.store.SegmentStore', {
    extend	: 'Ext.data.Store',
    model: 'TestOrimi.model.SegmentModel',
    pageSize: 1000,
    proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getSegment'
		}
	},
    autoLoad: false,
    storeId: 'SegmentStore',
	filters: [{
        id: 'SegmentStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

Ext.define('TestOrimi.store.ContractorGroupStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getContractorGroup'
		}
	},
    autoLoad: false,
    storeId: 'ContractorGroupStore',
	filters: [{
		id: 'ContractorGroupStore-deleted-filter',
		property : 'Deleted',
		value: 0
	}]
});

Ext.define('TestOrimi.store.SlaveWarehousesStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SlaveWarehousesModel',
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
        timeout: 600000,
        simpleSortMode: true,
        noCache: false,
        reader: {
            type: 'json',
            root: 'items',
            totalProperty: 'total'
        },
        extraParams: {
            act: 'getSlaveWarehouses',
            searchall: ''
        }
    },
    remoteSort: true,
    remoteFilter: true,
    autoLoad: false,
    storeId: 'SlaveWarehousesStore'
});

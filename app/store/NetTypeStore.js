Ext.define('TestOrimi.store.NetTypeStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
    pageSize: 1000,
	proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getNetType'
        }
    },
    autoLoad: false,
    storeId: 'NetTypeStore',
    filters: [{
        id: 'NetTypeStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

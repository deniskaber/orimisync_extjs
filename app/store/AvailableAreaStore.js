Ext.define('TestOrimi.store.AvailableAreaStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.AreaModel',
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getAvailableArea'
        }
    },
    autoLoad: false,
    storeId: 'AvailableAreaStore'
});

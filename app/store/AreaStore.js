Ext.define('TestOrimi.store.AreaStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.AreaModel',
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getArea'
        }
    },
    autoLoad: false,
    storeId: 'AreaStore',
    filters: [{
        id: 'AreaStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

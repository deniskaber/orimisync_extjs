Ext.define('TestOrimi.store.AdvertisingBossHeadersStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.AdvertisingBossHeadersModel',
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
        timeout: 600000, 
        simpleSortMode: true,
        noCache: false,
        reader: {
            type: 'json',
            root: 'items',
            totalProperty: 'total'
        },
        extraParams: {
            act: 'getAdvertisingBossHeaders',
            searchall: ''
        }
    },
    remoteSort: true,
    remoteFilter: true, 
    autoLoad: false,
    storeId: 'AdvertisingBossHeadersStore'
});

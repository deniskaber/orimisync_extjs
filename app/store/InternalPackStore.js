Ext.define('TestOrimi.store.InternalPackStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getInternalPack'
		}
	},
	autoLoad: false,
	storeId: 'InternalPackStore',
	filters: [{
        id: 'InternalPackStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

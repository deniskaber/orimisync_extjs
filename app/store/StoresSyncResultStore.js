Ext.define('TestOrimi.store.StoresSyncResultStore', {
	extend: 'Ext.data.Store',
	model: 'TestOrimi.model.StoresSyncResultModel',
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
		timeout: 600000,
		noCache: false,
		reader: {
			type: 'json',
			root: 'items',
			totalProperty: 'total'
		},
		extraParams: {
			act: 'getGroupSyncStores'
		}
	},
	autoLoad: false,
	storeId: 'StoresSyncResultStore'
});

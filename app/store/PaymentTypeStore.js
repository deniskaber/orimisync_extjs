Ext.define('TestOrimi.store.PaymentTypeStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getPaymentType'
		}
	},
	autoLoad: false,
	storeId: 'PaymentTypeStore',
	filters: [{
        id: 'PaymentTypeStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

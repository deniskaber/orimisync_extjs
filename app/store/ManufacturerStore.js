Ext.define('TestOrimi.store.ManufacturerStore', {
	extend: 'Ext.data.Store',
	model: 'TestOrimi.model.SimpleAttributeModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000,
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getManufacturer'
		}
	},
	autoLoad: false,
	storeId: 'ManufacturerStore',
	filters: [{
        id: 'ManufacturerStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

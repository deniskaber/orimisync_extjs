Ext.define('TestOrimi.store.MasterStaffFieldsStore', {
	extend: 'Ext.data.ArrayStore',
	pageSize: 100,
	model: 'TestOrimi.model.StaffFieldsModel',
	data: [
		['Код',                          'Code'               ],
		['Фамилия',                      'SurName'            ],
		['Имя',                          'Name'               ],
		['Отчество',                     'LastName'           ],
		['ФИО',                          'FIO'                ],
		['Дата рождения',                'BirthDate'          ],
		['Тип',                          'Type'               ],
		['Дивизион',                     'Division'           ],
		['Дата начала работы',           'DateHired'          ],
		['Дата увольнения',              'DateFired'          ],
		['E-mail',                       'EMail'              ],
		['Телефон',                      'Phone'              ],
		['Комментарий',                  'Comment'            ],
		['Мотивация',                    'Motivation'         ],
		['Признак',                      'Feature'            ],
		['Учитывать в команде',          'ConsiderInTeam'     ],
		['Участок',                      'Sector'             ],
		['Кому подчиняется',             'Parent'             ],
		['Источник данных',              'DataSource'         ]		
	],
	storeId: 'MasterStaffFieldsStore'
});
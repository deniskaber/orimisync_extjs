Ext.define('TestOrimi.store.SlaveWarehousesFieldsStore', {
    extend: 'Ext.data.ArrayStore',
    pageSize: 100,
    model: 'TestOrimi.model.TextValueStringModel',
    data: [
        ['Код', 'Code'],
        ['Код Дистр.', 'DistrCode'],
        ['Название', 'Name'],
        ['Черновой адрес', 'TempAddress'],
        ['Округ', 'County'],
        ['Регион', 'Region'],
        ['Город', 'City'],
        ['Дивизион', 'Division'],
        ['Участок', 'Sector'],
        ['Источник данных', 'DataSource']
    ],
    storeId: 'SlaveWarehousesFieldsStore'
});
Ext.define('TestOrimi.store.StaffSyncResultStore', {
	extend: 'Ext.data.Store',
	model: 'TestOrimi.model.StaffSyncResultModel',
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
		timeout: 600000,
		noCache: false,
		reader: {
			type: 'json',
			root: 'items',
			totalProperty: 'total'
		},
		extraParams: {
			act: 'getGroupSyncStaff'
		}
	},
	autoLoad: false,
	storeId: 'StaffSyncResultStore'
});

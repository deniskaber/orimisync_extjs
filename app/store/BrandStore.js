Ext.define('TestOrimi.store.BrandStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.BrandModel',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: 'resources/data/api.php',
		timeout: 600000, 
		noCache: false,
		reader: {
			type: 'json'
		},
		extraParams: {
			act: 'getBrand'
		}
	},
    autoLoad: false,
    storeId: 'BrandStore',
	filters: [{
        id: 'BrandStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]/*,
    listeners: {
    	filterchange: function(store, filters) {
    		console.dir(filters);
    	}
    }*/
});

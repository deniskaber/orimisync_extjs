Ext.define('TestOrimi.store.FormatStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SimpleAttributeModel',
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getFormat'
        }
    },
    autoLoad: false,
    storeId: 'FormatStore',
    filters: [{
        id: 'FormatStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

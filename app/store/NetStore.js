Ext.define('TestOrimi.store.NetStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.NetModel',
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getNet'
        }
    },
    autoLoad: false,
    storeId: 'NetStore',
    filters: [{
        id: 'NetStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }, {
        id: 'NetStore-hasChildren-filter',
        property : 'hasChildren',
        value: 0,
        defaultValue: 0
    }]
});

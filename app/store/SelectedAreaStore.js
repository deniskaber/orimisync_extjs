Ext.define('TestOrimi.store.SelectedAreaStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SelectedAreaModel',
    
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getSelectedArea'
        }
    },
    autoLoad: false,
    storeId: 'SelectedAreaStore'
});

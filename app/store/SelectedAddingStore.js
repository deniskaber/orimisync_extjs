Ext.define('TestOrimi.store.SelectedAddingStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.SelectedAddingModel',
    
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getSelectedAdding'
        }
    },
    autoLoad: false,
    storeId: 'SelectedAddingStore'
});

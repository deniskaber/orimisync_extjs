Ext.define('TestOrimi.store.MotivationStore', {
    extend: 'Ext.data.Store',
    model: 'TestOrimi.model.MotivationModel',
    pageSize: 1000,
    proxy: {
        type: 'ajax',
        url: 'resources/data/api.php',
        timeout: 600000, 
        noCache: false,
        reader: {
            type: 'json'
        },
        extraParams: {
            act: 'getMotivation'
        }
    },
    autoLoad: false,
    storeId: 'MotivationStore',
    filters: [{
        id: 'MotivationStore-deleted-filter',
        property : 'Deleted',
        value: 0
    }]
});

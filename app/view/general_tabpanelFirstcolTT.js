Ext.define('TestOrimi.view.general_tabpanelFirstcolTT', {
    extend      : 'Ext.grid.Panel',
    xtype       : 'general_tabpanelFirstcolTT',
    selType     : 'checkboxmodel',
    columnLines : true,
    initComponent: function() {
      var me = this,
          userRights = TestOrimi.getApplication().data;
        
        this.store = 'TestOrimi.store.SyncedStoresStore';

        this.columns  =  [
          { text: 'ID',                dataIndex: 'ID',            width: 80  },
          { text: 'Код',               dataIndex: 'Code',          width: 130 },
          { text: 'Код Дистр.',        dataIndex: 'DistrCode',     width: 130 },
          { text: 'Название',             dataIndex: 'Name',          width: 250,
              renderer: function (value, meta, record) {
                  value = value.replace(/"/g, '\'');
                  meta.tdAttr = 'data-qtip="Синхронизировано с ' + record.get('MasterID') + '"';
                  return value;
              }
          },
          { text: 'Черновой адрес',    dataIndex: 'TempAddress',   width: 180 },
          { text: 'Округ',             dataIndex: 'County',        width: 110 },
          { text: 'Регион',            dataIndex: 'Region',        width: 110 },
          { text: 'Город',             dataIndex: 'City',          width: 130 },
          { text: 'Дивизион',          dataIndex: 'Division',      width: 100 },
          { text: 'Участок',           dataIndex: 'Sector',        width: 100 },
          { text: 'Подканал',          dataIndex: 'SubChannel',    width: 100 },
          { text: 'Формат',            dataIndex: 'Format',        width: 100 },
          { text: 'Сеть',              dataIndex: 'isNet',         width: 100 },
          { text: 'Название сети',     dataIndex: 'Net',           width: 120 },
          { text: 'Группа контрагентов', dataIndex: 'ContractorGroup',width: 180 },
          { text: 'Источник',          dataIndex: 'DataSource',    width: 180 },
          { text: 'Дата привязки',        dataIndex: 'DateChange',    width: 130 },
          { text: 'авто/ручн',         dataIndex: 'SyncType',      width: 100 }

        ];

        if(userRights['TT_slave_sync']) {
          Ext.Array.insert(me.columns, 0, [{
            text: 'Отвязать',
            menuDisabled: true,
            sortable: false,
            xtype: 'actioncolumn',
            menuText: 'Отвязать',
            width: 100,
            items: [{
              iconCls: 'unsync-col',
              handler: function(view, rowIndex, colIndex) {
                  me.fireEvent('unsyncbuttonclick', me, rowIndex, colIndex);
              }
            }]
          }]);
        }

        var i = 1,
            columns = this.columns,
            length = columns.length;

        for (;i<length;i++) {
            columns[i].items = [{
            xtype: 'container',
            margin: 4,
            flex: 1,
            layout: 'fit',
            listeners: {
                scope: this,
                element: 'el',
                mousedown: function(e)
                {
                    e.stopPropagation();
                },
                click: function(e)
                {
                    e.stopPropagation();
                },
                keydown: function(e){
                     e.stopPropagation();
                },
                keypress: function(e){
                     e.stopPropagation();
                },
                keyup: function(e){
                     e.stopPropagation();
                }
            },
            items:[{
              xtype: 'textfield',
              emptyText : 'Поиск',
              enableKeyEvents: true,
              onTriggerClick: function () {
                this.reset();
                this.focus();
              },
              listeners: {
                change: function(field, newValue, oldValue, eOpts) {
                  me.fireEvent('searchchange', me, field, newValue, oldValue);
                },
                buffer: 1000
              }
            }]
          }];
        }

        this.lbar = [{
            xtype   : 'general_tabpanelLbar',
            ref     : 'TT',
            grid: this
        }];

    this.bbar = {
      xtype: 'pagingtoolbar',
      store: this.store,
      displayInfo: true,
      inputItemWidth: 60,
      items : [{
        xtype: 'tbseparator' 
      },{
        xtype     : 'checkbox',
        boxLabel  : 'Показывать все',
        name      : 'syncedShowAll',
        inputValue: 1,
        listeners: {
          change: function( cb, newValue, oldValue, eOpts ) {
            cb.up('pagingtoolbar').store.loadPage(1);
          }
        }
      }]
    };

        this.callParent(arguments);
    }
});

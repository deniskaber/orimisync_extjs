Ext.define('TestOrimi.view.general_tabpanelFirstcolStaff', {
    extend      : 'Ext.grid.Panel',
    xtype       : 'general_tabpanelFirstcolStaff',
    selType     : 'checkboxmodel',
    columnLines : true,
    
    initComponent: function() {
        var me = this,
            userRights = TestOrimi.getApplication().data;
        
        this.store = 'TestOrimi.store.SyncedStaffStore';

        this.columns  =  [
          { text: 'ID',                           dataIndex: 'ID',            width: 80 },
          { text: 'Код',                          dataIndex: 'Code',          width: 130 },
          { text: 'Код Дистр.',                   dataIndex: 'DistrCode',     width: 130 },
          { text: 'ФИО',                          dataIndex: 'FIO',           width: 250,
            renderer: function (value, meta, record) {
              value = value.replace(/"/g, '\'');
              meta.tdAttr = 'data-qtip="Синхронизировано с ' + record.get('MasterID') + '"';
              return value;
            }
          },
          { text: 'Дата рождения',                dataIndex: 'BirthDate',     width: 150 },
          { text: 'Тип',                          dataIndex: 'Type',          width: 110 },
          { text: 'Кому подчиняется',             dataIndex: 'Parent',        width: 190 },
          { text: 'Принадлежность к дивизиону',   dataIndex: 'Division',      width: 200 },
          { text: 'Участок',                      dataIndex: 'Sector',        width: 120 },
          { text: 'Дата начала работы',           dataIndex: 'DateHired',     width: 195 },
          { text: 'Дата увольнения',              dataIndex: 'DateFired',     width: 195 },
          { text: 'E-mail',                       dataIndex: 'EMail',         width: 150 },
          { text: 'Телефон',                      dataIndex: 'Phone',         width: 130 },
          { text: 'Мотивация',                    dataIndex: 'Motivation',    width: 120 },
          { text: 'Признак',                      dataIndex: 'Feature',       width: 120 },
          { text: 'Источник',                     dataIndex: 'DataSource',    width: 215 },
          { text: 'Дата привязки',                dataIndex: 'DateChange',    width: 130 },
          { text: 'авто/ручн',                    dataIndex: 'SyncType',      width: 100 }
        ];

        if(userRights['Staff_slave_sync']) {
          Ext.Array.insert(me.columns, 0, [{
            text: 'Отвязать',
            menuDisabled: true,
            sortable: false,
            xtype: 'actioncolumn',
            menuText: 'Отвязать',
            width: 100,
            items: [{
              iconCls: 'unsync-col',
              handler: function(view, rowIndex, colIndex) {
                  me.fireEvent('unsyncbuttonclick', me, rowIndex, colIndex);
              }
            }]
          }]);
        }

         var i = 1,
            columns = this.columns,
            length = columns.length;

        for (;i<length;i++) {
            columns[i].items = [{
            xtype: 'container',
            margin: 4,
            flex: 1,
            layout: 'fit',
            listeners: {
                scope: this,
                element: 'el',
                mousedown: function(e)
                {
                    e.stopPropagation();
                },
                click: function(e)
                {
                    e.stopPropagation();
                },
                keydown: function(e){
                     e.stopPropagation();
                },
                keypress: function(e){
                     e.stopPropagation();
                },
                keyup: function(e){
                     e.stopPropagation();
                }
            },
            items:[{
              // xtype: 'trigger',
              // triggerCls: 'x-form-clear-trigger',
              xtype: 'textfield',
              //flex : 1,
              //margin: '0 5 5 5',
              emptyText : 'Поиск',
              enableKeyEvents: true,
              onTriggerClick: function () {
                this.reset();
                this.focus();
              },
              listeners: {
                change: function(field, newValue, oldValue, eOpts) {
                  me.fireEvent('searchchange', me, field, newValue, oldValue);
                },
                buffer: 1000
              }
            }]
          }];
        }

        this.lbar = [{ 
            xtype   : 'general_tabpanelLbar',
            ref     : 'Staff',            
            grid    : this

        }];

        this.bbar = {
          xtype: 'pagingtoolbar',
          store: this.store,
          displayInfo: true,
          inputItemWidth: 60,
          items : [{
            xtype: 'tbseparator' 
          },{
            xtype     : 'checkbox',
            boxLabel  : 'Показывать все',
            name      : 'syncedShowAll',
            inputValue: 1,
            listeners: {
              change: function( cb, newValue, oldValue, eOpts ) {
                cb.up('pagingtoolbar').store.loadPage(1);
              }
            }
          }]
        };
                
        this.callParent(arguments);

    }
});
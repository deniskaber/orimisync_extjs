Ext.define('TestOrimi.view.CreateSearchWindowSku', {
	extend	: 'Ext.window.Window',
	title	: 'Комбинированный поиск',
	modal	: true,
	width	: 500,
	height  : 600,
	layout  : 'fit',
	items	: [{
		xtype   : 'form',
		margin	: 10,
		border  : false,
		layout  : {type: 'vbox', align: 'stretch'},
		items: [{
			xtype		: 'textfield'
			,fieldLabel	: 'Поиск по всем атрибутам'
			,name 		: 'searchall'
			,anchor 	: '90%'
			,labelWidth : 170
		},{
			fieldLabel  : 'Применить ко всем справочникам'
			,xtype   	: 'radiogroup'
			,labelWidth	: 170
			,anchor		: 'none'
			,layout		: {
				autoFlex: false
			},
			defaults 	: {
				name 	: 'allgoods',
                margin: '0 15 0 0'
			}
			,items: [{
				boxLabel    : 'нет',
				inputValue  : 0
			},{
				boxLabel    : 'да',
				inputValue  : 1
			},{
				boxLabel    : 'только подчиненные',
				inputValue  : 2,
				margin: 0
			}]
		},{
			xtype		: 'container',
			flex: 1,
			layout  : {type: 'vbox', align: 'stretch'},
			items:[{
				xtype           :'fieldset',
				//checkboxToggle  : true,
				title           : 'Обязательные атрибуты',
				defaultType     : 'textfield',
				flex			: 1,
				autoScroll      : true,
				margin          : '20 0 0 0',
				defaults        : {
					anchor		: '90%', 
					labelWidth 	: 130 
				},
				items :[{
					fieldLabel  : 'ID',
					name        : 'ID'
				},{
					fieldLabel  : 'Полное название',
					name        : 'Name'
				},{
					fieldLabel  : 'Короткое название',
					name        : 'ShortName'
				},{
					fieldLabel  : 'Черновое название',
					name        : 'TempName'
				},{
					fieldLabel  : 'Категория',
					name        : 'Category',
					xtype       : 'searchModalCombo',
					store       : 'TestOrimi.store.CategoryStore'
				},{
					fieldLabel  : 'Продукция ОТ',
					xtype       : 'radiogroup',
					anchor		: 'none',
					layout		: {
						autoFlex: false
					},
					defaults: {
	                        name: 'From',
	                        margin: '0 15 0 0'
	                },
					items: [{
						boxLabel    : 'товар наш',
						inputValue  : 1
					},{
						boxLabel    : 'товар конкурента',
						inputValue  : 0
					}]
				},{
					fieldLabel  : 'Производитель',
					name        : 'Manufacturer',
					xtype       : 'searchModalCombo',
					store       : 'TestOrimi.store.ManufacturerStore'
				},{
					fieldLabel  : 'Бренд',
					name        : 'Brand',
					attribute   : 'Brand',
					xtype       : 'searchModalCombo',
					store       : 'TestOrimi.store.BrandStore'
				},{
					fieldLabel  : 'Саббренд',
					name        : 'SubBrand',
					attribute   : 'SubBrand',
					xtype       : 'searchModalCombo',
					store       : 'TestOrimi.store.SubBrandStore'
				},{
					fieldLabel  : 'Сегмент',
					name        : 'Segment',
					attribute	: 'Segment',
					xtype		: 'searchModalCombo',
					store		: 'TestOrimi.store.SegmentStore'
				},{
					fieldLabel  : 'Сабсегмент',
					name        : 'SubSegment',
					attribute   : 'SubSegment',
					xtype       : 'searchModalCombo',
					store       : 'TestOrimi.store.SubSegmentStore'
				},{
					fieldLabel  : 'Цвет чая',
					name        : 'TeaColor',
					xtype       : 'searchModalCombo',
					store       : 'TestOrimi.store.TeaColorStore'
				},{
					fieldLabel  : 'Тип упаковки внешней',
					name        : 'ExternalPack',
					xtype       : 'searchModalCombo',
					store       : 'TestOrimi.store.ExternalPackStore'
				},{
					fieldLabel  : 'Тип упаковки саше',
					name        : 'SachetPack',
					xtype       : 'searchModalCombo',
					store       : 'TestOrimi.store.SachetPackStore'
				},{
					fieldLabel  : 'Тип упаковки внутренней',
					name        : 'InternalPack',
					xtype       : 'searchModalCombo',
					store       : 'TestOrimi.store.InternalPackStore'
				},{
					fieldLabel  : 'Упаковка Хорека'
					,xtype		: 'radiogroup'
					,anchor		: 'none'
					,layout		: {
						autoFlex: false
					}
					,defaults: {
			            name	: 'isHorekaPack',
			            margin	: '0 15 0 0'
			                }
					//,labelWidth  : 130
					,items: [{
						boxLabel    : 'с упаковкой',
						inputValue  : 1
					},{
						boxLabel    : 'без упаковки',
						inputValue  : 0
					}]
				},{
					fieldLabel  : 'Количество пакетов',
					name        : 'BagQty'
				},{
					fieldLabel  : 'Вес пакета',
					name        : 'BagWeight'
				},{
					fieldLabel  : 'Вес(общий)',
					name        : 'Weight'
				},{
					fieldLabel  : 'Аромат'
			        ,xtype		: 'radiogroup'
			        ,anchor		: 'none'
			        ,layout		: {
			        	autoFlex: false
			        }
			        //,labelWidth  : 130
			        ,defaults: {
	                        name: 'isWithFlavor',
	                        margin: '0 15 0 0'
	                }
			        ,items: [{
			          boxLabel    : 'с ароматом',
			          inputValue  : 1
			        },{
			          boxLabel    : 'без аромата',
			          inputValue  : 0
			        }]
				},{
					fieldLabel  : 'Ароматы',
					name        : 'Flavor'
				},{
					fieldLabel  : 'Добавка'
			        ,xtype		: 'radiogroup'
			        ,anchor		: 'none'
			        ,layout		: {
			        	autoFlex: false
			        }
			        ,defaults: {
			            name	: 'isWithAdding',
			            margin	: '0 15 0 0'
			        }
			        //,labelWidth  : 130
			        ,items: [{
			          boxLabel  : 'с добавкой',
			          inputValue: 1
			        },{
			          boxLabel  : 'без добавки',
			          inputValue: 0
			        }]
				},{
					fieldLabel  : 'Добавки',
					name        : 'Adding'
				}]
			},{
				xtype           :'fieldset',
				defaults        : {anchor: '90%', labelWidth : 130 },
				flex			: 1,
				title           : 'Остальные атрибуты',
				defaultType     : 'textfield',
				autoScroll      : true,
				margin          : '20 0 20 0',
				items: [{
					fieldLabel  : 'Код',
					name        : 'Code'
				},{
					fieldLabel  : 'Код Орими',
					name        : 'OrimiCode'
				},{
					fieldLabel  : 'Вес(группа)',
					name        : 'WeightGroup',
					xtype       : 'searchModalCombo',
					store       : 'TestOrimi.store.WeightGroupStore'
				},{
					fieldLabel  : 'Ценовой сегмент',
					name        : 'PriceSegment',
					xtype       : 'searchModalCombo',
					store       : 'TestOrimi.store.PriceSegmentStore'
				},{
					fieldLabel  : 'Страна происхождения',
					name        : 'Country',
					xtype       : 'searchModalCombo',
					store       : 'TestOrimi.store.CountryStore'
				},{
					fieldLabel  : 'Сорт кофе',
					name        : 'CoffeeSort',
					xtype       : 'searchModalCombo',
					store       : 'TestOrimi.store.CoffeeSortStore'
				},{
					fieldLabel  : 'Кофеин',
					xtype       : 'radiogroup',
					anchor		: 'none',
					layout		: {
						autoFlex: false
					},
					defaults: {
	                     name	: 'isCaffeine',
	                     margin	: '0 15 0 0'
	                },
					items: [{
						boxLabel    : 'с кофеином',
						inputValue  : 1
					},{
						boxLabel    : 'без кофеина',
						inputValue  : 0
					}]
				},{
					fieldLabel  : 'Тип подарка',
					name        : 'GiftType',
					xtype       : 'searchModalCombo',
					store       : 'TestOrimi.store.GiftTypeStore'
				},{
					fieldLabel  : 'Длина',
					name        : 'Length'
				},{
					fieldLabel  : 'Ширина',
					name        : 'Width'
				},{
					fieldLabel  : 'Высота',
					name        : 'Height'
				},{
					fieldLabel  : 'Акционная позиция',
					name        : 'Gift',
					xtype       : 'searchModalCombo',
					store       : 'TestOrimi.store.GiftStore'
				},{
					fieldLabel  : 'Весовой продукт в сети',
					xtype       : 'radiogroup',
					anchor		: 'none',
					layout		: {
						autoFlex: false
					},
					defaults: {
	                     name	: 'isWeightNet',
	                     margin	: '0 15 0 0'
	                },
					items: [{
						boxLabel    : 'да',
						inputValue  : 1
					},{
						boxLabel    : 'нет',
						inputValue  : 0
					}]
				},{
                    xtype       : 'textareafield',
                    name        : 'Comment',
                    fieldLabel  : 'Комментарий'
	            },{
					fieldLabel  : 'Коллекция/Доп.имя',
					name        : 'Collection'
				},{
					fieldLabel  : 'Частная марка сети',
					xtype       : 'radiogroup',
					anchor		: 'none',
					layout		: {
						autoFlex: false
					},
					defaults: {
	                     name	: 'isPrivateMark',
	                     margin	: '0 15 0 0'
	                },
					items: [{
						boxLabel    : 'да',
						inputValue  : 1
					},{
						boxLabel    : 'нет',
						inputValue  : 0
					}]
				},{
					fieldLabel  : 'Агрегирующая позиция',
					name        : 'Parent',
					attribute   : 'Parent',
          			displayField: 'ShortName',
					xtype		: 'searchModalCombo',
					store		: 'TestOrimi.store.GoodParentStore'
				},{
					fieldLabel  : 'Штрихкод',
					name        : 'Barcode'
				},{
	        		fieldLabel  : 'Статус',
	        		name        : 'Status'
	      		},{
					fieldLabel  : 'Источник',
					name        : 'DataSource'
				},{
					fieldLabel  : 'Автор изменений',
					name        : 'Author'
				},{
					fieldLabel  : 'Дата посл. изменения',
					xtype       : 'datefield',
                    format      : 'Y-m-d',
					name        : 'DateChange'
				},{
					fieldLabel  : 'Авто/Ручной',
					xtype       : 'radiogroup',
					anchor		: 'none',
					layout		: {
						autoFlex: false
					},
					defaults: {
	                     name	: 'SyncType',
	                     margin	: '0 15 0 0'
	                },
					items: [{
						boxLabel    : 'Авто',
						inputValue  : 1
					},{
						boxLabel    : 'Ручной',
						inputValue  : 0
					}]
				}]
			}]
		}]
	}],
	
	buttons:  [{
		text: 'Сбросить все',
		handler: function() {
			this.up('window').down('form').getForm().reset();
		}
	},'->',{
		text: 'Применить',
		handler: function() {
			var grid = this.up('window').grid,
				store = grid.store,
				formVals = this.up('window').down('form').getValues(),
				filters = [];

			var modelFields = store.model.getFields(),
				dataFields = [],
				i = 0,
				length = modelFields.length;

			for (;i<length;i++) {
				dataFields.push(modelFields[i].name);
			}

			Ext.Object.each(formVals, function(key, value, myself) {
				if (value || value === 0) {
					if (Ext.Array.contains(dataFields,key)) {
						filters.push({property: key, value: value});
					}
				}
			});


			if (formVals['allgoods'] == 1) {
		        var masterGoodStore = Ext.getStore('TestOrimi.store.MasterGoodStore'),
		            syncedGoodStore = Ext.getStore('TestOrimi.store.SyncedGoodStore'),
		            slaveGoodStore = Ext.getStore('TestOrimi.store.SlaveGoodStore');

		        masterGoodStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
		        masterGoodStore.fireEvent('applyFilters', masterGoodStore, filters, grid.up('viewport').down('general_grid'));
		        syncedGoodStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
		        syncedGoodStore.fireEvent('applyFilters', syncedGoodStore, filters, grid.up('viewport').down('general_tabpanel').down('general_tabpanelFirstcol'));
		        slaveGoodStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
		        slaveGoodStore.fireEvent('applyFilters', slaveGoodStore, filters, grid.up('viewport').down('general_tabpanel').down('general_tabpanelSecondcol'));
			} else if (formVals['allgoods'] == 2) {
				var syncedGoodStore = Ext.getStore('TestOrimi.store.SyncedGoodStore'),
		            slaveGoodStore = Ext.getStore('TestOrimi.store.SlaveGoodStore');

				syncedGoodStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
		        syncedGoodStore.fireEvent('applyFilters', syncedGoodStore, filters, grid.up('viewport').down('general_tabpanel').down('general_tabpanelFirstcol'));
		        slaveGoodStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
		        slaveGoodStore.fireEvent('applyFilters', slaveGoodStore, filters, grid.up('viewport').down('general_tabpanel').down('general_tabpanelSecondcol'));
			} else {
				store.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
				store.fireEvent('applyFilters', store, filters, grid);
			}

			this.up('window').destroy();
		}
	},{
		text: 'Отмена',
		handler: function() {
			this.up('window').destroy();
		}
	}],

	initComponent: function() {
		var me = this,
        	grid = me.grid;

		this.callParent(arguments);

		var cols = [],
	        columns = grid.columns,
	        length = columns.length;

    	for (i=0;i<length;i++) {
			if (columns[i]['dataIndex'])
				cols.push(columns[i]['dataIndex']);
		}
		cols.push('searchall');
    	cols.push('allgoods');

		var fields = me.query('textfield'), //все textfield окна
        	radiogroups = me.query('radiogroup');

	    length = fields.length;

	    for (i=0;i<length;i++) {				//цикл указывания какие поля останутся активными
			if (!(Ext.Array.contains(cols, fields[i].name))) {
				fields[i].hide();
        		fields[i].disable();
			}
		}

		length = radiogroups.length;

    	for (i=0;i<length;i++) {				//цикл указывания какие поля останутся активными
			if (!(Ext.Array.contains(cols, radiogroups[i].items.items[0].name))) {
				radiogroups[i].hide();
       			radiogroups[i].disable();
			}
		}

		var store = grid.store,
	        filterRecord = {},
	        filters = store.filters.items;

    	length = filters.length;

		if (length) {
			for (i=0;i<length;i++) {
				filterRecord[filters[i].property] = filters[i].value;
			}
		}
		if (store.proxy.extraParams['searchall']) {
			filterRecord['searchall'] = store.proxy.extraParams['searchall'];
		}
		this.down('form').getForm().setValues(filterRecord);
	}
});
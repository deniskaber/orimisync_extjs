Ext.define('TestOrimi.view.general_gridLbar', {
    extend: 'Ext.container.Container',
    xtype: 'general_gridLbar',
    name: 'general_gridLbar',
    width: 55,
    padding: '2',
    border: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        margin: '2 0 2 0'
    },
    ref: 'Sku',

    initComponent: function () {
        var me = this,
            userRights = TestOrimi.getApplication().data;

        me.items = [{
            xtype: 'button'
            , tooltip: 'Комб. поиск'
            , name: 'combSearch'
            , scale: 'medium'
            , text: '<span class="oi" data-glyph="magnifying-glass" aria-hidden="true"></span>'
            , handler: function () {
                Ext.create('TestOrimi.view.CreateSearchWindow' + me.ref, {grid: me.grid}).show();
            }
        }, {
            xtype: 'button'
            , tooltip: 'Инверсия выбора'
            , scale: 'medium'
            , text: '<span class="oi" data-glyph="loop" aria-hidden="true"></span>'
            , handler: function () {
                var grid = me.grid,
                    selModel = grid.getSelectionModel(),
                    selected = selModel.getSelection();

                selModel.selectAll(true);
                selModel.deselect(selected, true);
            }
        }, {
            xtype: 'button'
            , tooltip: 'Групповая установка свойств'
            , disabled: (userRights[me.ref + '_draft_set'] || userRights[me.ref + '_active_set']) ? false : true
            , scale: 'medium'
            , text: '<span class="oi" data-glyph="layers" aria-hidden="true"></span>'
            , handler: function () {
                var grid = me.grid,
                    selected = grid.getSelectionModel().getSelection(),
                    length = selected.length,
                    hasActiveStatus = false,
                    editable = [];

                if (selected.length) {
                    for (i = 0; i < length; i++) {
                        if ((selected[i].get('StatusID') == 1 && userRights[me.ref + '_draft_set']) || (selected[i].get('StatusID') == 2 && userRights[me.ref + '_active_set'])) {
                            editable.push(selected[i].get('ID'));
                            if (selected[i].get('StatusID') == 2)
                                hasActiveStatus = true;
                        }
                    }
                    if (editable.length)
                        Ext.create('TestOrimi.view.createStylegroup' + me.ref, {
                            records: editable,
                            hasActiveStatus: hasActiveStatus
                        }).show();
                    else
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Позиции помечены на удаление или редактирование закрыто правами доступа', 'winError');
                } else
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Позиции не выбраны', 'winError');
            }
        }, {
            xtype: 'button',
            tooltip: 'Добавление новой позиции',
            disabled: userRights[me.ref + '_draft_create'] ? false : true,
            scale: 'medium',
            text: '<span class="oi" data-glyph="document" aria-hidden="true"></span>',
            handler: function () {
                Ext.create('TestOrimi.view.create' + me.ref + 'Window').show();
            }
        }, {
            xtype: 'button',
            tooltip: 'Создание дубликата позиции',
            disabled: userRights[me.ref + '_draft_create'] ? false : true,
            scale: 'medium',
            text: '<span class="oi" data-glyph="fork" aria-hidden="true"></span>',
            handler: function () {
                var grid = me.grid,
                    selectedRecord = grid.getSelectionModel().getSelection()[0];

                if (selectedRecord) {
                    var copy = selectedRecord.copy();

                    copy.set('ID', 0);
                    if (me.ref == 'Sku') {
                        copy.set('ParentID', 0);
                        copy.set('Code', '');
                        copy.set('Barcode', '');
                    }
                    copy.set('StatusID', 1);
                    copy.set('TempName', '');

                    Ext.create('TestOrimi.view.create' + me.ref + 'Window', {data: copy}).show();
                }
                else {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Позиция не выбрана', 'winError');
                }
            }
        }, {
            xtype: 'button',
            tooltip: 'Просмотр истории работы с позицией',
            scale: 'medium',
            text: '<span class="oi" data-glyph="book" aria-hidden="true"></span>',
            handler: function () {
                var grid = me.grid,
                    selectedRecord = grid.getSelectionModel().getSelection()[0]; //TODO

                if (selectedRecord)
                    me.fireEvent('eventhistoryclick', me.grid, {ref: me.ref});
                else
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Позиция не выбрана', 'winError');
            }
        }, {
            xtype: 'button',
            tooltip: 'Перевод черновика в активный статус',
            disabled: userRights[me.ref + '_draft_activate'] ? false : true,
            scale: 'medium',
            text: '<span class="oi" data-glyph="pin" aria-hidden="true"></span>',
            handler: function () {
                me.fireEvent('statusaddclick', me.grid, {ref: me.ref});
            }
        }, {
            xtype: 'button',
            tooltip: 'Шаблоны настроек страницы',
            scale: 'medium',
            text: '<span class="oi" data-glyph="task" aria-hidden="true"></span>',
            handler: function () {
                var lbar = me;

                lbar.fireEvent('templateloadstore', lbar, {ref: lbar.ref});
            }
        }, {
            xtype: 'button',
            tooltip: 'Удалить',
            disabled: (userRights[me.ref + '_draft_delete'] || userRights[me.ref + '_active_delete']) ? false : true,
            scale: 'medium',
            text: '<span class="oi" data-glyph="trash" aria-hidden="true"></span>',
            handler: function () {
                var lbar = me;

                lbar.fireEvent('deleteaddclick', lbar.grid, {ref: lbar.ref});
            }
        }];

        this.callParent(arguments);
    }
});

Ext.define('TestOrimi.view.ParentID', {
    extend: 'Ext.window.Window',
    xtype: 'parentID',
    width: 950,
    height: 525,
    title: 'Агрегирующая позиция',
    modal: true,
    layout: 'fit',
    multiselect: false,
    callback: null,
    store: 'TestOrimi.store.OrimiGoodsStore',

    initComponent: function () {
        var me = this;
        var field = me.field;

        me.items = [{
            xtype: 'grid'
            , flex: 1
            , plugins: [{
                ptype: 'bufferedrenderer',
                trailingBufferZone: 20,
                leadingBufferZone: 50
            }]
            , border: true
            , columnLines: true
            , selType: me.multiselect ? 'checkboxmodel' : 'rowmodel'
            , columns: [
                {text: 'ID', dataIndex: 'ID', width: 80},
                {text: 'Код', dataIndex: 'Code', width: 130},
                {text: 'Короткое название', dataIndex: 'ShortName', width: 200},
                {text: 'Полное название', dataIndex: 'Name', width: 200},
                {text: 'Черновое название', dataIndex: 'TempName', width: 200},
                {text: 'Продукция ОТ', dataIndex: 'From', width: 150},
                {text: 'Производитель', dataIndex: 'Manufacturer', width: 150},
                {text: 'Бренд', dataIndex: 'Brand', width: 125},
                {text: 'Саббренд', dataIndex: 'SubBrand', width: 150},
                {text: 'Категория', dataIndex: 'Category', width: 110},
                {text: 'Сегмент', dataIndex: 'Segment', width: 80},
                {text: 'Сабсегмент', dataIndex: 'SubSegment', width: 100},
                {text: 'Цвет чая', dataIndex: 'TeaColor', width: 120},
                {text: 'Тип упаковки внешней', dataIndex: 'ExternalPack', width: 180},
                {text: 'Тип упаковки саше', dataIndex: 'SachetPack', width: 150},
                {text: 'Тип упаковки внутренней', dataIndex: 'InternalPack', width: 200},
                {text: 'Количество пакетов', dataIndex: 'BagQty', width: 165},
                {text: 'Вес пакета', dataIndex: 'BagWeight', width: 100},
                {text: 'Вес', dataIndex: 'Weight', width: 80},
                {text: 'Вес (группа)', dataIndex: 'WeightGroup', width: 120},
                {text: 'Аромат', dataIndex: 'isWithFlavor', width: 80},
                {text: 'Аромат (признак)', dataIndex: 'Flavor', width: 166},
                {text: 'Добавка', dataIndex: 'isWithAdding', width: 80},
                {text: 'Добавки (признак)', dataIndex: 'Adding', width: 166},
                {text: 'Упаковка Хорека', dataIndex: 'isHorekaPack', width: 155},
                {text: 'Ценовой сегмент', dataIndex: 'PriceSegment', width: 150},
                {text: 'Страна происхождения', dataIndex: 'Country', width: 185},
                {text: 'Сорт кофе', dataIndex: 'CoffeeSort', width: 125},
                {text: 'Кофеин', dataIndex: 'isCaffeine', width: 80},
                {text: 'Акционная позиция', dataIndex: 'Gift', width: 195},
                {text: 'Тип подарка', dataIndex: 'GiftType', width: 120},
                {text: 'Фотография', dataIndex: 'Photo', width: 115},
                {text: 'Длина', dataIndex: 'Length', width: 80},
                {text: 'Ширина', dataIndex: 'Width', width: 80},
                {text: 'Высота', dataIndex: 'Height', width: 80},
                {text: 'Весовой продукт в сети', dataIndex: 'isWeightNet', width: 190},
                {text: 'Комментарии', dataIndex: 'Comment', width: 125},
                {text: 'Коллекция/Доп.имя', dataIndex: 'Collection', width: 165},
                {text: 'Частная марка сети', dataIndex: 'isPrivateMark', width: 165},
                {text: 'Штрихкод', dataIndex: 'Barcode', width: 115}
            ]
            , store: me.field ? me.field.store : me.store
            , listeners: {
                celldblclick: function (grid, td, cellIndex, record, tr, rowIndex) {
                    me.down('[name=okButton]').handler();
                }
            }
        }];

        var i = 0,
            columns = this.items[0].columns,
            length = columns.length;

        for (; i < length; i++) {
            columns[i].items = [{
                xtype: 'container',
                margin: 4,
                flex: 1,
                layout: 'fit',
                listeners: {
                    scope: this,
                    element: 'el',
                    mousedown: function (e) {
                        e.stopPropagation();
                    },
                    click: function (e) {
                        e.stopPropagation();
                    },
                    keydown: function (e) {
                        e.stopPropagation();
                    },
                    keypress: function (e) {
                        e.stopPropagation();
                    },
                    keyup: function (e) {
                        e.stopPropagation();
                    }
                },
                items: [{
                    xtype: 'textfield',
                    emptyText: 'Поиск',
                    enableKeyEvents: true,
                    onTriggerClick: function () {
                        this.reset();
                        this.focus();
                    },
                    listeners: {
                        change: function (field, newValue, oldValue, eOpts) {
                            var grid = this.up('grid');
                            me.fireEvent('searchchange', grid, field, newValue, oldValue);
                        },
                        buffer: 1000
                    }
                }]
            }];
        }

        me.buttons = [{
            text: 'Ок',
            name: 'okButton',
            handler: function () {
                var win = this.up('window'),
                    selected = win.down('grid').getSelectionModel().getSelection();

                var selectedIds;
                var fieldName;

                if (!selected || selected.length === 0) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
                    return;
                }

                if (!me.multiselect) {
                    fieldName = me.field.name;

                    if (fieldName.substr(-2) === 'ID') {
                        if (win.field.value !== selected[0].get('ID')) {
                            win.field.setValue(selected[0].get('ID'));
                        }
                    } else {
                        if (win.field.value !== selected[0].get('ShortName')) {
                            win.field.setValue(selected[0].get('ShortName'));
                        }
                    }
                } else {
                    selectedIds = selected.map(function(record) {
                        return record.get('ID');
                    });

                    me.callback(selectedIds);
                }

                win.destroy();
            }
        }, {
            text: 'Отмена',
            handler: function () {
                this.up('window').destroy();
            }
        }];

        this.callParent(arguments);
    }
});

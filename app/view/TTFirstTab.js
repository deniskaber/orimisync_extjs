Ext.define('TestOrimi.view.TTFirstTab', {
    extend: 'Ext.container.Container',
    xtype: 'ttFirstTab',
    border: false,
    margin: '20 20 0 20',
    layout: 'column',
    items: [{
        xtype: 'fieldset',
        defaultType: 'textfield',
        columnWidth: 0.5,
        margin: '20 0 0 0',
        defaults: {anchor: '100%', labelWidth: 160},
        border: false,
        items: [{
            fieldLabel: 'Индекс',
            xtype: 'numberfield',
            minValue: 0,
            name: 'PostCode'
        }, {
            fieldLabel: 'Округ',
            name: 'CountyID',
            attribute: 'County',
            readOnly: true,
            cls: 'noteditable',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.CountyStore'
        }, {
            fieldLabel: 'Район',
            name: 'DistrictID',
            attribute: 'District',
            readOnly: true,
            cls: 'noteditable',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.DistrictStore'
        }, {
            fieldLabel: 'Улица',
            afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
            name: 'Street'
        }, {
            fieldLabel: 'Дивизион',
            name: 'DivisionID',
            attribute: 'Division',
            margin: '25 0 5 0',
            xtype: 'modalcombo',
            readOnly: true,
            cls: 'noteditable',
            store: 'TestOrimi.store.DivisionStore'
        }, {
            fieldLabel: 'Участок',
            name: 'AreaID',
            attribute: 'Area',
            xtype: 'modalcombo',
            readOnly: true,
            cls: 'noteditable',
            store: 'TestOrimi.store.AreaStore'
        }, {
            fieldLabel: 'Сектор',
            name: 'SectorID',
            attribute: 'Sector',
            xtype: 'modalcombo',
            readOnly: true,
            cls: 'noteditable',
            store: 'TestOrimi.store.SectorStore'
        }, {
            xtype: 'datefield',
            fieldLabel: 'Дата открытия ТТ',
            afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
            format: 'Y-m-d',
            name: 'DateOpen'
        }, {
            fieldLabel: 'Площадь ТТ',
            xtype: 'numberfield',
            minValue: 0,
            name: 'ShopArea'
        }, {
            fieldLabel: 'Формат',
            name: 'FormatID',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.FormatStore'
        }]
    }, {
        xtype: 'fieldset',
        defaultType: 'textfield',
        columnWidth: 0.5,
        margin: '20 0 0 0',
        defaults: {anchor: '100%', labelWidth: 160},
        border: false,
        items: [{
            fieldLabel: 'Страна',
            name: 'CountryID',
            xtype: 'modalcombo',
            readOnly: true,
            cls: 'noteditable',
            store: 'TestOrimi.store.CountryStore'
        }, {
            fieldLabel: 'Регион',
            name: 'RegionID',
            attribute: 'Region',
            xtype: 'modalcombo',
            readOnly: true,
            cls: 'noteditable',
            store: 'TestOrimi.store.RegionStore'
        }, {
            fieldLabel: 'Город',
            name: 'CityID',
            afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
            attribute: 'City',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.CityStore'
            /*,validator   : Ext.emptyFn()
             validator   : function(v) {
             var me = this;
             if (v && v === parseInt(v)) {
             return true;
             } else return 'Неизвестный элемент';
             },
             validator   : function(v) {
             var me = this;
             if (v) {
             if (me.getStore().findExact(me.displayField, v) !== -1) {
             me.store.load({
             params: {
             Val: v
             },
             callback: function(records, operation, success) {
             return (me.getStore().findExact(me.displayField, v) !== -1) ? true : 'Неизвестный элемент';
             }
             });
             } else return true;
             } else return true;
             }*/
        }, {
            fieldLabel: 'Дом',
            afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
            name: 'Building'
        }, {
            fieldLabel: 'Канал',
            name: 'ChannelID',
            readOnly: true,
            margin: '25 0 5 0',
            cls: 'noteditable',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.ChannelStore'
        }, {
            fieldLabel: 'Подканал',
            name: 'SubChannelID',
            afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
            attribute: 'SubChannel',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.SubChannelStore'
        }, {
            fieldLabel: 'Канал для рекламы',
            name: 'AdvertisingChannelID',
            readOnly: true,
            attribute: 'AdvertisingChannel',
            cls: 'noteditable',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.AdvertisingChannelStore'
        }, {
            xtype: 'datefield',
            fieldLabel: 'Дата закрытия ТТ',
            format: 'Y-m-d',
            name: 'DateClose'
        }, {
            fieldLabel: 'Координаты',
            name: 'Coordinates'
        }, {
            fieldLabel: 'ФИАС',
            name: 'FIAS'
        }]
    }]
});

Ext.define('TestOrimi.view.GeneralAttributesPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'general_grid_attributes',
    //title: 'Панель справочников',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    autoScroll: true,

    initComponent: function() {
        var me = this,
            userRights = TestOrimi.getApplication().data;

        function buttonHandler() {
            var me = this,
                store = Ext.getStore(me.store);

            //disable Deleted filter
            if (store.filters.items[0]) {
                store.filters.items[0].setValue('');
            }

            for (i=0; i < store.filters.items.length; i++) {
                if (store.filters.items[i] && store.filters.items[i].hasOwnProperty('defaultValue')) {
                    store.filters.items[i].setValue('');
                }
            }

            store.filter();

            Ext.create(me.window, {
                button: me
            }).show();
        }

        me.items = [{
            xtype: 'fieldset',
            title: 'Товары',
            layout: 'fit',
            items: [{
                xtype: 'panel',
                title: '',
                layout: 'column',
                //autoScroll: true,
                defaults: {
                    xtype: 'button',
                    scale: 'large',
                    //width: 110,
                    cls: 'attribute-button',
                    columnWidth: 0.25,
                    handler: buttonHandler
                },
                items: [
                    {
                        name: 'Manufacturer',
                        text: 'Производитель',
                        store: 'TestOrimi.store.ManufacturerStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'Category',
                        text: 'Категория',
                        store: 'TestOrimi.store.CategoryStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'Segment',
                        text: 'Сегмент',
                        store: 'TestOrimi.store.SegmentStore',
                        window: 'TestOrimi.view.SegmentWindow'
                    },
                    {
                        name: 'SubSegment',
                        text: 'Сабсегмент',
                        store: 'TestOrimi.store.SubSegmentStore',
                        window: 'TestOrimi.view.SubSegmentWindow'
                    },
                    {
                        name: 'Brand',
                        text: 'Бренд',
                        store: 'TestOrimi.store.BrandStore',
                        window: 'TestOrimi.view.BrandWindow'
                    },
                    {
                        name: 'SubBrand',
                        text: 'Саббренд',
                        store: 'TestOrimi.store.SubBrandStore',
                        window: 'TestOrimi.view.SubBrandWindow'
                    },
                    {
                        name: 'TeaColor',
                        text: 'Цвет чая',
                        store: 'TestOrimi.store.TeaColorStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'CoffeeSort',
                        text: 'Сорт кофе',
                        store: 'TestOrimi.store.CoffeeSortStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'Flavor',
                        text: 'Ароматы',
                        store: 'TestOrimi.store.FlavorStore',
                        window: 'TestOrimi.view.FlavorWindow'
                    },
                    {
                        name: 'Adding',
                        text: 'Добавки',
                        store: 'TestOrimi.store.AddingStore',
                        window: 'TestOrimi.view.AddingWindow'
                    },
                    {
                        name: 'ExternalPack',
                        text: 'Тип упаковки внешней',
                        store: 'TestOrimi.store.ExternalPackStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'SachetPack',
                        text: 'Тип упаковки саше',
                        store: 'TestOrimi.store.SachetPackStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'InternalPack',
                        text: 'Тип упаковки внутренней',
                        store: 'TestOrimi.store.InternalPackStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'GiftType',
                        text: 'Тип подарка',
                        store: 'TestOrimi.store.GiftTypeStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'Gift',
                        text: 'Акционная позиция',
                        store: 'TestOrimi.store.GiftStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'WeightGroup',
                        text: 'Вес(группа)',
                        store: 'TestOrimi.store.WeightGroupStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'PriceSegment',
                        text: 'Ценовой сегмент',
                        store: 'TestOrimi.store.PriceSegmentStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    }
                ]
            }]
        }, {
            xtype: 'fieldset',
            title: 'География',
            layout: 'fit',
            items: [{
                xtype: 'panel',
                title: '',
                layout: 'column',
                //autoScroll: true,
                defaults: {
                    xtype: 'button',
                    scale: 'large',
                    //width: 110,
                    cls: 'attribute-button',
                    columnWidth: 0.25,
                    handler: buttonHandler
                },
                items: [
                    {
                        name: 'Country',
                        text: 'Страна',
                        store: 'TestOrimi.store.CountryStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'County',
                        text: 'Округ',
                        store: 'TestOrimi.store.CountyStore',
                        window: 'TestOrimi.view.CountyWindow'
                    },
                    {
                        name: 'Region',
                        text: 'Регион',
                        store: 'TestOrimi.store.RegionStore',
                        window: 'TestOrimi.view.RegionWindow'
                    },
                    {
                        name: 'District',
                        text: 'Район',
                        store: 'TestOrimi.store.DistrictStore',
                        window: 'TestOrimi.view.DistrictWindow'
                    },
                    {
                        name: 'City',
                        text: 'Город',
                        store: 'TestOrimi.store.CityStore',
                        window: 'TestOrimi.view.CityWindow'
                    }
                ]
            }]
        }, {
            xtype: 'fieldset',
            title: 'Торговые точки',
            layout: 'fit',
            items: [{
                xtype: 'panel',
                title: '',
                layout: 'column',
                //autoScroll: true,
                defaults: {
                    xtype: 'button',
                    scale: 'large',
                    //width: 110,
                    cls: 'attribute-button',
                    columnWidth: 0.25,
                    handler: buttonHandler
                },
                items: [
                    {
                        name: 'Channel',
                        text: 'Канал',
                        store: 'TestOrimi.store.ChannelStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'SubChannel',
                        text: 'Подканал',
                        store: 'TestOrimi.store.SubChannelStore',
                        window: 'TestOrimi.view.SubChannelWindow'
                    },
                    {
                        name: 'Net',
                        text: 'Сеть',
                        store: 'TestOrimi.store.NetStore',
                        window: 'TestOrimi.view.NetWindow'
                    },
                    {
                        name: 'NetType',
                        text: 'Тип сети',
                        store: 'TestOrimi.store.NetTypeStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'Format',
                        text: 'Формат',
                        store: 'TestOrimi.store.FormatStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'AdvertisingChannel',
                        text: 'Канал для рекламы',
                        store: 'TestOrimi.store.AdvertisingChannelStore',
                        window: 'TestOrimi.view.AdvertisingChannelWindow'
                    }
                ]
            }]
        }, {
            xtype: 'fieldset',
            title: 'Сотрудники',
            layout: 'fit',
            items: [{
                xtype: 'panel',
                title: '',
                layout: 'column',
                //autoScroll: true,
                defaults: {
                    xtype: 'button',
                    scale: 'large',
                    //width: 110,
                    cls: 'attribute-button',
                    columnWidth: 0.25,
                    handler: buttonHandler
                },
                items: [
                    {
                        name: 'TypeStaff',
                        text: 'Тип сотрудника',
                        store: 'TestOrimi.store.TypeStaffStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'Motivation',
                        text: 'Мотивация',
                        store: 'TestOrimi.store.MotivationStore',
                        window: 'TestOrimi.view.MotivationWindow'
                    }
                ]
            }]
        }, {
            xtype: 'fieldset',
            title: 'Прочее',
            layout: 'fit',
            items: [{
                xtype: 'panel',
                title: '',
                layout: 'column',
                //autoScroll: true,
                defaults: {
                    xtype: 'button',
                    scale: 'large',
                    //width: 110,
                    cls: 'attribute-button',
                    columnWidth: 0.25,
                    handler: buttonHandler
                },
                items: [
                    {
                        name: 'Sector',
                        text: 'Сектор',
                        store: 'TestOrimi.store.SectorStore',
                        window: 'TestOrimi.view.SectorWindow'
                    },
                    {
                        name: 'Area',
                        text: 'Участок',
                        store: 'TestOrimi.store.AreaStore',
                        window: 'TestOrimi.view.AreaWindow'
                    },
                    {
                        name: 'Division',
                        text: 'Дивизион',
                        store: 'TestOrimi.store.DivisionStore',
                        window: 'TestOrimi.view.DivisionWindow'
                    },
                    {
                        name: 'Currancy',
                        text: 'Валюта',
                        store: 'TestOrimi.store.CurrancyStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'NonAccReason',
                        text: 'Причина неучета',
                        store: 'TestOrimi.store.NonAccReasonStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    },
                    {
                        name: 'NetSalesDataSource',
                        text: 'Источник данных по сетям',
                        store: 'TestOrimi.store.NetSalesDataSourceStore',
                        window: 'TestOrimi.view.SimpleAttributeWindow'
                    }
                ]
            }]
        }];

        for (var i = 0; i < me.items.length; i++) {
            for (var k = 0; k < me.items[i].items[0].items.length; k++) {
                if (userRights['Attributes_' + me.items[i].items[0].items[k].name] != 1 && userRights['Attributes_' + me.items[i].items[0].items[k].name] != 2) {
                    me.items[i].items[0].items.splice(k, 1);
                    k--;
                }
            }
            if (me.items[i].items[0].items.length === 0) {
                me.items.splice(i, 1);
                i--;
            }
        }


        this.callParent(arguments);
    }

});

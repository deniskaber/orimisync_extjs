Ext.define('TestOrimi.view.CreateSearchWindowAdvertisingEventRows', {
    extend: 'Ext.window.Window',
    title: 'Комбинированный поиск',
    modal: true,
    width: 500,
    height: 600,
    layout: 'fit',
    items: [{
        xtype: 'form',
        margin: 10,
        border: false,
        layout: {type: 'vbox', align: 'stretch'},
        autoScroll: true,
        items: [{
            xtype: 'textfield',
            fieldLabel: 'Поиск по всем столбцам',
            name: 'searchall',
            anchor: '90%',
            labelWidth: 170
        }, {
            xtype: 'container',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            margin: '20 0 0 0',
            defaults: {
                xtype: 'textfield',
                anchor: '90%',
                labelWidth: 160,
                margin: '3 15 2 5'
            },
            items: [{
                fieldLabel: 'ID',
                name: 'ID'
            }, {
                fieldLabel: 'ID заявки',
                name: 'RequestID'
            }, {
                fieldLabel: 'ID товара',
                name: 'GoodID'
            }, {
                fieldLabel: 'Наименование товара',
                name: 'GoodName',
                attribute: 'OrimiGood',
                displayField: 'ShortName',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.OrimiGoodsStore'
            }, {
                fieldLabel: 'Код Орими',
                name: 'OrimiCode'
            }, {
                fieldLabel: 'Фасовка товара',
                name: 'ExternalPack',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.ExternalPackStore'
            }, {
                fieldLabel: 'Бренд',
                name: 'Brand',
                attribute: 'Brand',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.BrandStore'
            }, {
                fieldLabel: 'Сегмент',
                name: 'Segment',
                attribute: 'Segment',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.SegmentStore'
            }, {
                fieldLabel: 'Упаковка товара',
                name: 'InternalPack',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.InternalPackStore'
            }, {
                fieldLabel: 'Комментарий',
                name: 'Comment'
            }]
        }, {
            xtype: 'fieldset',
            title: 'Ввод плана',
            collapsible: true,
            collapsed: false,
            defaultType: 'textfield',
            defaults: {anchor: '100%', labelWidth: 150},
            layout: 'anchor',
            items: [{
                fieldLabel: 'Количество дней от предыдущей акции',
                name: 'AmountOfDaysAfterPrev'
            }, {
                fieldLabel: 'Скидка по акции от ОРИМИ ТРЭЙД, %',
                name: 'OTDiscount'
            }, {
                fieldLabel: 'Скидка дистрибьютора, %',
                name: 'DistrDiscount'
            }, {
                fieldLabel: 'Скидка торговой сети, %',
                name: 'NetDiscount'
            }, {
                fieldLabel: 'Входная цена дистрибьютора, руб',
                name: 'DistrIncomePrice'
            }, {
                fieldLabel: 'Входная цена торговой сети до акции, руб',
                name: 'NetIncomePriceBefore'
            }, {
                fieldLabel: 'Цена позиции на полке в ТТ до акции, руб',
                name: 'SellingPriceBefore'
            }, {
                fieldLabel: 'Входная цена торговой сети во время акции, руб',
                name: 'NetIncomePriceAction'
            }, {
                fieldLabel: 'Разница между акционной и регулярной ценой, руб',
                name: 'IncomePriceDifference'
            }, {
                fieldLabel: 'Скидка в накладной, %',
                name: 'InvoiceDiscount'
            }, {
                fieldLabel: 'Максимальная глубина скидки, %',
                name: 'MaxDiscountDepth'
            }, {
                fieldLabel: 'Фонд генерального директора, %',
                name: 'FGD'
            }, {
                fieldLabel: 'Минимально возможная цена на полке за период, руб',
                name: 'MinPrice'
            }, {
                fieldLabel: 'Цена базового прайс-листа, руб',
                name: 'BPLPrice'
            }, {
                fieldLabel: 'Наценка сети, %',
                name: 'NetMarkup'
            }, {
                fieldLabel: 'Прогноз цены на полке во время акции, руб',
                name: 'EstimatedSellingPrice'
            }, {
                fieldLabel: 'Фактически проведенная скидка в сети, %',
                name: 'FactDiscount'
            }, {
                fieldLabel: 'Цена поставки в ТС данного ассортимента (без акции от БПЛ), %',
                name: 'SupplyPrice'
            }, {
                fieldLabel: 'Продажи до акции по данной позиции за сопоставимый период времени, шт',
                name: 'SalesAmountBeforeQty'
            }, {
                fieldLabel: 'Продажи до акции по данной позиции за сопоставимый период времени, руб',
                name: 'SalesAmountBeforeVal'
            }, {
                fieldLabel: 'Прогноз компенсации, шт',
                name: 'EstimatedSalesAmountQty'
            }, {
                fieldLabel: 'Прогноз компенсации, руб',
                name: 'EstimatesSalesAmountVal'
            }, {
                fieldLabel: 'Прогноз компенсации  ФГД, руб',
                name: 'EstimatesSalesAmountFGD'
            }, {
                fieldLabel: 'Прогноз компенсации бонус Д0, руб',
                name: 'EstimatesSalesAmountD0Bonus'
            }, {
                fieldLabel: 'Прогноз компенсации на торговую точку, шт',
                name: 'EstimatedSalesAmountQtyPerStr'
            }, {
                fieldLabel: 'Макс. количество единиц к отгрузке по акции, шт',
                name: 'MaxNumberToShip'
            }, {
                fieldLabel: 'Стоимость публикации в каталоге, руб',
                name: 'CataloguePubCost'
            }]
        }, {
            xtype: 'fieldset',
            title: 'Ввод факта',
            collapsible: true,
            collapsed: false,
            defaultType: 'textfield',
            defaults: {anchor: '100%', labelWidth: 150},
            layout: 'anchor',
            items: [{
                fieldLabel: 'Реальная цена на полке, руб',
                name: 'RealSellingPrice'
            }, {
                fieldLabel: 'Продажи во время акции с учетом скидки, шт',
                name: 'RealSalesAmountQty'
            }, {
                fieldLabel: 'Продажи во время акции с учетом скидки, руб',
                name: 'RealSalesAmountVal'
            }, {
                fieldLabel: 'Продажи во время акции в регулярных ценах, руб',
                name: 'RealSalesAmountInRegularPrices'
            }, {
                fieldLabel: 'Затраты дистрибьютора, руб',
                name: 'DistrCost'
            }, {
                fieldLabel: 'Затраты ОТ на акцию, руб',
                name: 'OTCost'
            }, {
                fieldLabel: 'Компенсация ФГД, руб',
                name: 'FGDComp'
            }, {
                fieldLabel: 'Компенсация Д0, руб',
                name: 'D0Comp'
            }, {
                fieldLabel: 'Прирост, %',
                name: 'SalesIncrease'
            }, {
                fieldLabel: 'Процент затрат, %',
                name: 'IncreasePercent'
            }]
        }, {
            fieldLabel: 'Автор изменений',
            name: 'Author'
        }, {
            fieldLabel: 'Дата посл. изменения',
            xtype: 'datefield',
            format: 'Y-m-d',
            name: 'DateChange'
        }]
    }],

    buttons: [{
        text: 'Сбросить все',
        handler: function() {
            this.up('window').down('form').getForm().reset();
        }
    }, '->', {
        text: 'Применить',
        handler: function() {
            var grid = this.up('window').grid,
                store = grid.store,
                formVals = this.up('window').down('form').getValues(),
                filters = [];

            var modelFields = store.model.getFields(),
                dataFields = [],
                i = 0,
                length = modelFields.length;

            for (; i < length; i++) {
                dataFields.push(modelFields[i].name);
            }

            Ext.Object.each(formVals, function(key, value, myself) {
                if (value || value === 0) {
                    if (Ext.Array.contains(dataFields, key)) {
                        filters.push({property: key, value: value});
                    }
                }
            });

            store.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
            store.fireEvent('applyFilters', store, filters, grid);

            this.up('window').destroy();
        }
    }, {
        text: 'Отмена',
        handler: function() {
            this.up('window').destroy();
        }
    }],

    initComponent: function() {
        var me = this,
            grid = me.grid;

        this.callParent(arguments);

        var cols = [],
            columns = grid.columns,
            length = columns.length,
            i;

        for (i = 0; i < length; i++) {
            if (columns[i]['dataIndex'])
                cols.push(columns[i]['dataIndex']);
        }
        cols.push('searchall');
        // cols.push('allgoods');

        var fields = me.query('textfield'), //все textfield окна
            radiogroups = me.query('radiogroup');

        length = fields.length;

        for (i = 0; i < length; i++) {				//цикл указывания какие поля останутся активными
            if (!(Ext.Array.contains(cols, fields[i].name))) {
                fields[i].hide();
                fields[i].disable();
            }
        }

        length = radiogroups.length;

        for (i = 0; i < length; i++) {				//цикл указывания какие поля останутся активными
            if (!(Ext.Array.contains(cols, radiogroups[i].items.items[0].name))) {
                radiogroups[i].hide();
                radiogroups[i].disable();
            }
        }

        var store = grid.store,
            filterRecord = {},
            filters = store.filters.items;

        length = filters.length;

        if (length) {
            for (i = 0; i < length; i++) {
                filterRecord[filters[i].property] = filters[i].value;
            }
        }
        if (store.proxy.extraParams['searchall']) {
            filterRecord['searchall'] = store.proxy.extraParams['searchall'];
        }
        this.down('form').getForm().setValues(filterRecord);
    }
});

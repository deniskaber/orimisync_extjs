Ext.define('TestOrimi.view.countryaddDatapanel', {
                extend  : 'Ext.panel.Panel',
                xtype   : 'countryaddDatapanel',
                split   : true, 
                border  : false,
                layout  : 'vbox',
items: [
        {
        xtype       :'fieldset',
        margin      : '20 20 0 20',
        defaults    : {anchor: '100%'},
        border      : false,
        flex        : 1,
        defaultType : 'textfield',
    items: [
        {
            fieldLabel  : 'Наименование',
            name        : 'nameCntry'
        },{
            fieldLabel  : 'Краткое наим.',
            name        : 'cnameCntry'
        } 
        ]
        }
]    
});
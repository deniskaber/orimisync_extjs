Ext.define('TestOrimi.view.CreateAdvertisingEventRowWindow', {
    extend: 'Ext.window.Window',
    xtype: 'createAdvertisingEventRowWindow',
    width: 1000,
    height: 740,
    modal: true,
    title: 'Заявка на рекламную акцию',
    layout: 'fit',

    initComponent: function() {
        var me = this;
        var userRights = TestOrimi.getApplication().data;

        var formTabs = [];
        var requestStatus = (me.data) ? me.data.get('RequestStatus') : me.AdvertisingRequestRecord.get('Status');
        var fields;
        var mcombos;
        var i;
        var form;

        me.rendererThousandField = function(val) {
            if (val !== 0) {
                return Ext.util.Format.number(val, '0.000/i').replace(/\./g, ' ');
            }
        }

        me.buttons = [{
            text: 'Пересчет факта',
            name: 'recalculateButton',
            handler: function() {
                var win = this.up('window');
                var form = win.down('form');
                var store = Ext.getStore('TestOrimi.store.AdvertisingEventsRowsStore');
                if (!form || win.isFormLoadingRecord) {
                    return;
                }
                var formData = form.getValues();

                win.setLoading('Загрузка...');

                Ext.Ajax.request({
                    url: 'resources/data/api.php',
                    timeout: 600000,
                    params: {
                        ID: formData['ID'],
                        act: store.proxy.extraParams['act'],
                        subaction: 'recalculate'
                    },
                    success: function(response) {
                        var responseText = Ext.JSON.decode(response.responseText);

                        if (!responseText.success) {
                            TestOrimi.getApplication().getController('Main')
                                     .showNotification('Ошибка! Не удалось выполнить пересчет фактов.', 'winError');
                            win.setLoading(false);
                            return;
                        }

                        TestOrimi.getApplication().getController('Main')
                                 .showNotification('Позиция заявки пересчитана.', 'winSuccess');
                        store.load();

                        var updatedRecord = TestOrimi.model.AdvertisingEventsRowsModel.create(responseText.data);
                        form.loadRecord(updatedRecord);
                        win.setLoading(false);
                    },
                    failure: function() {
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сервер не отвечает на запрос', 'winError');
                        win.setLoading(false);
                    }
                });
            }
        }, '->', {
            text: 'Ок',
            name: 'okButton',
            handler: function() {
                var store = Ext.getStore('TestOrimi.store.AdvertisingEventsRowsStore'),
                    win   = this.up('window'),
                    form  = win.down('form').getForm();

                var requestStatus = (win.data) ? win.data.get('RequestStatus') : win.AdvertisingRequestRecord.get('Status');

                checkMaxNumberToOneStore();

                if (form.isValid()) {
                    var myMask = new Ext.LoadMask(win, {msg: 'Сохраняю...'});

                    myMask.show();
                    form.submit({
                        url: 'resources/data/api.php',
                        params: {
                            act: store.proxy.extraParams['act'],
                            subaction: (win.data) ? (win.data.get('ID') ? 'update' : 'create') : 'create',
                            RequestID: (win.data) ? win.data.get('RequestID') : win.AdvertisingRequestRecord.get('ID')
                        },
                        timeout: 600000,
                        success: function(form, response) {
                            myMask.destroy();
                            if (win.data) {
                                if (win.data.get('ID') === 0) {
                                    TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess');
                                } else {
                                    TestOrimi.getApplication().getController('Main').showNotification('Успешно! Элемент обновлен', 'winSuccess');
                                }
                            } else {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess');
                            }

                            store.load();

                            if (requestStatus === 'Ввод факта') {
                                TestOrimi.getApplication().getController('Main').showNotification('Сделай Фото акции!!!', 'winError');
                            }

                            win.destroy();
                        },
                        failure: function(form, action) {
                            myMask.destroy();
                            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                        }
                    });
                } else {
                    console.dir(form);
                }
            }
        }, {
            text: 'Отмена',
            handler: function() {
                this.up('window').destroy();
            }
        }];

        me.afterLabelTextTpl = '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>';

        me.recalculateAll = function() {
            if (me.data && !(requestStatus === 'Ввод плана' || requestStatus === 'Ввод факта' || requestStatus === 'Ручной ввод факта')) {
                return;
            }

            var fields = me.down('form').query('numberfield');

            Ext.Array.each(fields, function(field) {
                if (field.recalculateFormula) {
                    field.recalculateFormula();
                }
            });
        };

        me.fieldChangeHandler = function(field, newValue) {
            if (me.data && !(requestStatus === 'Ввод плана' || requestStatus === 'Ввод факта' || requestStatus === 'Ручной ввод факта')) {
                return;
            }

            var fields,
                i,
                value,
                form = me.down('form'),
                currentField;

            switch (field.name) {
                case 'StoreAmount':
                    fields = ['EstimatedSalesAmountQtyPerStr'];
                    break;

                case 'OTDiscount':
                    fields = ['EstimatedSellingPrice', 'NetIncomePriceAction', 'OTCost', 'EstimatesSalesAmountVal', 'EstimatesSalesAmountFGD', 'EstimatesSalesAmountD0Bonus'];
                    break;

                case 'DistrDiscount':
                    fields = ['EstimatedSellingPrice', 'NetIncomePriceAction', 'DistrCost'];
                    break;

                case 'NetDiscount':
                    fields = ['EstimatedSellingPrice'];
                    break;

                case 'BPLPrice':
                    fields = ['NetIncomePriceBefore'];
                    break;

                case 'FGD':
                    fields = ['EstimatesSalesAmountFGD', 'FGDComp'];
                    break;

                case 'DistrIncomePrice':
                    fields = ['EstimatedSellingPrice'];
                    break;

                case 'SupplyPrice'  :
                    fields = ['NetIncomePriceBefore'];
                    break;

                case 'NetIncomePriceBefore':
                    var SupplyPriceField = form.down('[name=SupplyPrice]');
                    var BPLPrice = form.down('[name=BPLPrice]').getValue();

                    if (BPLPrice !== 0 && !field.readOnly) {
                        value = ((newValue / BPLPrice) - 1) * 100;

                        SupplyPriceField.suspendEvents();
                        SupplyPriceField.setValue(value);
                        SupplyPriceField.resumeEvents();
                    }

                    fields = ['NetMarkup', 'NetIncomePriceAction', 'IncomePriceDifference', 'InvoiceDiscount', 'EstimatesSalesAmountVal', 'EstimatesSalesAmountFGD', 'EstimatesSalesAmountD0Bonus', 'SupplyPrice'];
                    break;

                case 'SellingPriceBefore':
                    fields = ['NetMarkup', 'EstimatedSellingPrice', 'FactDiscount', 'RealSalesAmountInRegularPrices'];
                    break;

                case 'EstimatedSellingPrice':
                    fields = ['FactDiscount'];
                    break;

                case 'NetIncomePriceAction':
                    fields = ['IncomePriceDifference', 'InvoiceDiscount'];
                    break;

                case 'SalesAmountBeforeQty':
                    fields = ['SalesIncrease'];
                    break;

                case 'CataloguePubCost':
                    fields = ['IncreasePercent'];
                    break;

                case 'EstimatedSalesAmountQty':
                    fields = ['EstimatesSalesAmountVal', 'EstimatesSalesAmountFGD', 'EstimatesSalesAmountD0Bonus', 'EstimatedSalesAmountQtyPerStr'];
                    break;

                case 'EstimatesSalesAmountFGD':
                    fields = ['EstimatesSalesAmountD0Bonus'];
                    break;

                case 'RealSalesAmountQty':
                    fields = ['RealSalesAmountInRegularPrices', 'SalesIncrease'];
                    break;

                case 'RealSalesAmountVal':
                    fields = ['IncreasePercent'];
                    break;

                case 'RealSalesAmountInRegularPrices':
                    fields = ['DistrCost', 'OTCost', 'FGDComp', 'D0Comp'];
                    break;

                case 'OTCost':
                    fields = ['FGDComp', 'D0Comp', 'IncreasePercent'];
                    break;
            }

            if (!fields) return;

            for (i = 0; i < fields.length; i++) {
                currentField = form.down('[name=' + fields[i] + ']');

                if (currentField && currentField.recalculateFormula) {
                    currentField.recalculateFormula();
                }
            }
        };

        me.setBossOrder = function(field, newValue) {
            if (me.data && requestStatus !== 'Ввод плана' || !field.isValid()) {
                return;
            }

            var form = me.down('form');
            var formData = form.getValues();

            if (!formData['GoodID'] || !formData['DateStart']) {
                return;
            }

            Ext.Ajax.request({
                url: 'resources/data/api.php',
                timeout: 600000,
                params: {
                    GoodID: formData['GoodID'],
                    DateStart: formData['DateStart'],
                    act: 'getAdvertisingBossCurrentOrders'
                },
                success: function(response) {
                    var responseText = Ext.JSON.decode(response.responseText);

                    if (!responseText.length) {
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Нет подходящего распоряжения генерального директора.', 'winError');
                        return;
                    }

                    var data = responseText[0];
                    var MaxDiscountDepth = form.down('[name=MaxDiscountDepth]');
                    MaxDiscountDepth && MaxDiscountDepth.setValue(data['MaxDiscountDepth']);
                    var FGD = form.down('[name=FGD]');
                    FGD && FGD.setValue(data['FGD']);
                    var MinPrice = form.down('[name=MinPrice]');
                    MinPrice && MinPrice.setValue(data['MinPrice']);
                    var BPLPrice = form.down('[name=BPLPrice]');
                    BPLPrice && BPLPrice.setValue(data['BPLPrice']);
                },
                failure: function(form, action) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сервер не отвечает на запрос', 'winError');
                }
            });
        };

        var planTab = {
            xtype: 'container',
            autoScroll: true,
            border: false,
            layout: 'column',
            title: 'Ввод плана',
            items: [{
                xtype: 'fieldset',
                columnWidth: 0.33,
                defaults: {
                    xtype: 'numberfield',
                    anchor: '100%',
                    labelWidth: 160,
                    decimalSeparator: '.'
                },
                border: false,
                items: [{
                    fieldLabel: 'Скидка по акции от ОРИМИ ТРЭЙД, %',
                    name: 'OTDiscount',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    value: 0,
                    minValue: 0,
                    maxValue: 100,
                    listeners: {
                        'change': me.fieldChangeHandler
                    }
                }, {
                    fieldLabel: 'Скидка дистрибьютора, %',
                    name: 'DistrDiscount',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    value: 0,
                    minValue: 0,
                    maxValue: 100,
                    listeners: {
                        'change': me.fieldChangeHandler
                    }
                }, {
                    fieldLabel: 'Скидка торговой сети, %',
                    name: 'NetDiscount',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    value: 0,
                    minValue: 0,
                    maxValue: 100,
                    listeners: {
                        'change': me.fieldChangeHandler
                    }
                }, {
                    fieldLabel: 'Входная цена дистрибьютора, руб',
                    name: 'DistrIncomePrice',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowDecimals: true,
                    value: 0,
                    minValue: 0,
                    listeners: {
                        'change': me.fieldChangeHandler
                    }
                }, {
                    fieldLabel: 'Цена поставки в ТС данного ассортимента (без акции от БПЛ), %',
                    name: 'SupplyPrice',
                    allowDecimals: true,
                    value: 0,
                    listeners: {
                        'change': me.fieldChangeHandler
                    }
                }, {
                    fieldLabel: 'Входная цена торговой сети до акции, руб',
                    name: 'NetIncomePriceBefore',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowDecimals: true,
                    value: 0,
                    minValue: 0,
                    listeners: {
                        'change': me.fieldChangeHandler
                    },
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var SupplyPriceField = form.down('[name=SupplyPrice]');
                        var SupplyPrice = SupplyPriceField.getValue();
                        var BPLPrice = form.down('[name=BPLPrice]').getValue();
                        var value;

                        if (SupplyPrice && SupplyPrice > 0) {
                            field.setReadOnly(true);
                            field.addCls('noteditable');
                            field.removeCls('editable');
                            SupplyPriceField.setReadOnly(false);
                            SupplyPriceField.addCls('editable');
                            SupplyPriceField.removeCls('noteditable');

                            value = (SupplyPrice + 100) * BPLPrice / 100;

                            field.setValue(value);
                        } else {
                            field.setReadOnly(false);
                            field.addCls('editable');
                            field.removeCls('noteditable');
                        }
                    }
                }, {
                    fieldLabel: 'Цена позиции на полке в ТТ до акции, руб',
                    name: 'SellingPriceBefore',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowDecimals: true,
                    value: 0,
                    minValue: 0,
                    listeners: {
                        'change': me.fieldChangeHandler
                    }
                }, {
                    fieldLabel: 'Входная цена торговой сети во время акции, руб',
                    name: 'NetIncomePriceAction',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable',
                    listeners: {
                        'change': me.fieldChangeHandler
                    },
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var NetIncomePriceBefore = form.down('[name=NetIncomePriceBefore]').getValue();
                        var OTDiscount = form.down('[name=OTDiscount]').getValue();
                        var DistrDiscount = form.down('[name=DistrDiscount]').getValue();
                        var value;

                        value = NetIncomePriceBefore - (NetIncomePriceBefore * (OTDiscount + DistrDiscount) / 100);

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }, {
                    fieldLabel: 'Разница между акционной и регулярной ценой, руб',
                    name: 'IncomePriceDifference',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable',
                    listeners: {
                        'change': me.fieldChangeHandler
                    },
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var NetIncomePriceBefore = form.down('[name=NetIncomePriceBefore]').getValue();
                        var NetIncomePriceAction = form.down('[name=NetIncomePriceAction]').getValue();
                        var value;

                        value = NetIncomePriceBefore - NetIncomePriceAction;

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }, {
                    fieldLabel: 'Скидка в накладной, %',
                    name: 'InvoiceDiscount',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable',
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var NetIncomePriceBefore = form.down('[name=NetIncomePriceBefore]').getValue();
                        var NetIncomePriceAction = form.down('[name=NetIncomePriceAction]').getValue();
                        var value;

                        if (+NetIncomePriceBefore === 0) {
                            value = 0;
                        } else {
                            value = 100 - (NetIncomePriceAction / NetIncomePriceBefore) * 100;
                        }

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }]
            }, {
                xtype: 'fieldset',
                columnWidth: 0.33,
                defaults: {
                    xtype: 'numberfield',
                    anchor: '100%',
                    labelWidth: 160,
                    decimalSeparator: '.'
                },
                border: false,
                items: [{
                    fieldLabel: 'Количество дней от предыдущей акции',
                    name: 'AmountOfDaysAfterPrev',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable'
                }, {
                    fieldLabel: 'Максимальная глубина скидки, %',
                    name: 'MaxDiscountDepth',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable'
                }, {
                    fieldLabel: 'Фонд генерального директора, %',
                    name: 'FGD',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable',
                    listeners: {
                        'change': me.fieldChangeHandler
                    }
                }, {
                    fieldLabel: 'Минимально возможная цена на полке за период, руб',
                    name: 'MinPrice',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable'
                }, {
                    fieldLabel: 'Цена базового прайс-листа, руб',
                    name: 'BPLPrice',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable'
                }, {
                    fieldLabel: 'Наценка сети, %',
                    name: 'NetMarkup',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable',
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var SellingPriceBefore = form.down('[name=SellingPriceBefore]').getValue();
                        var NetIncomePriceBefore = form.down('[name=NetIncomePriceBefore]').getValue();
                        var value;

                        if (+NetIncomePriceBefore === 0) {
                            value = 0;
                        } else {
                            value = (SellingPriceBefore / NetIncomePriceBefore - 1) * 100;
                        }

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }, {
                    fieldLabel: 'Прогноз цены позиции на полке в ТТ во время акции, руб',
                    name: 'EstimatedSellingPrice',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable',
                    listeners: {
                        'change': me.fieldChangeHandler
                    },
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var SellingPriceBefore = form.down('[name=SellingPriceBefore]').getValue();
                        var OTDiscount = form.down('[name=OTDiscount]').getValue();
                        var DistrDiscount = form.down('[name=DistrDiscount]').getValue();
                        var NetDiscount = form.down('[name=NetDiscount]').getValue();
                        var value;

                        value = SellingPriceBefore - (SellingPriceBefore * (OTDiscount + DistrDiscount + NetDiscount) / 100);

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }, {
                    fieldLabel: 'Фактически проведенная скидка в сети, %',
                    name: 'FactDiscount',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable',
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var SellingPriceBefore = form.down('[name=SellingPriceBefore]').getValue();
                        var EstimatedSellingPrice = form.down('[name=EstimatedSellingPrice]').getValue();
                        var value;

                        if (+SellingPriceBefore === 0) {
                            value = 0;
                        } else {
                            value = (SellingPriceBefore - EstimatedSellingPrice) / SellingPriceBefore * 100;
                        }

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }]
            }, {
                xtype: 'fieldset',
                columnWidth: 0.33,
                defaults: {
                    xtype: 'numberfield',
                    anchor: '100%',
                    labelWidth: 160,
                    decimalSeparator: '.'
                },
                border: false,
                items: [{
                    fieldLabel: 'Продажи до акции по данной позиции за сопоставимый период времени, шт',
                    xtype: 'textfield',
                    name: 'SalesAmountBeforeQty',
                    allowDecimals: false,
                    readOnly: true,
                    cls: 'noteditable',
                    listeners: {
                        beforerender: function (self) {
                            self.setValue(me.rendererThousandField(self.getValue()))
                        }
                    }
                }, {
                    fieldLabel: 'Продажи до акции по данной позиции за сопоставимый период времени, руб',
                    xtype: 'textfield',
                    name: 'SalesAmountBeforeVal',
                    allowDecimals: false,
                    readOnly: true,
                    cls: 'noteditable',
                    listeners: {
                        'change': me.fieldChangeHandler,
                        beforerender: function (self) {
                            self.setValue(me.rendererThousandField(self.getValue()))
                        }
                    }
                }, {
                    fieldLabel: 'Прогноз компенсации, шт',
                    name: 'EstimatedSalesAmountQty',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    minValue: 0,
                    value: 0,
                    allowBlank: false,
                    listeners: {
                        'change': me.fieldChangeHandler
                    }
                }, {
                    fieldLabel: 'Прогноз компенсации, руб',
                    name: 'EstimatesSalesAmountVal',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable',
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var EstimatedSalesAmountQty = form.down('[name=EstimatedSalesAmountQty]').getValue();
                        var OTDiscount = form.down('[name=OTDiscount]').getValue();
                        var NetIncomePriceBefore = form.down('[name=NetIncomePriceBefore]').getValue();
                        var value;

                        value = (EstimatedSalesAmountQty * OTDiscount * NetIncomePriceBefore) / 100;

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }, {
                    fieldLabel: 'Прогноз компенсации  ФГД, руб',
                    name: 'EstimatesSalesAmountFGD',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable',
                    listeners: {
                        'change': me.fieldChangeHandler
                    },
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var EstimatedSalesAmountQty = form.down('[name=EstimatedSalesAmountQty]').getValue();
                        var NetIncomePriceBefore = form.down('[name=NetIncomePriceBefore]').getValue();
                        var OTDiscount = form.down('[name=OTDiscount]').getValue();
                        var FGD = form.down('[name=FGD]').getValue();
                        var value;

                        value = NetIncomePriceBefore * EstimatedSalesAmountQty * Math.min(OTDiscount, FGD) / 100;

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }, {
                    fieldLabel: 'Прогноз компенсации бонус Д0, руб',
                    name: 'EstimatesSalesAmountD0Bonus',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable',
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var EstimatedSalesAmountQty = form.down('[name=EstimatedSalesAmountQty]').getValue();
                        var OTDiscount = form.down('[name=OTDiscount]').getValue();
                        var NetIncomePriceBefore = form.down('[name=NetIncomePriceBefore]').getValue();
                        var EstimatesSalesAmountFGD = form.down('[name=EstimatesSalesAmountFGD]').getValue();
                        var value;

                        value = (NetIncomePriceBefore * EstimatedSalesAmountQty * OTDiscount / 100) - EstimatesSalesAmountFGD;

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }, {
                    fieldLabel: 'Макс. разрешенное кол-во на 1 точку, шт',
                    name: 'MaxNumberToOneStore',
                    cls: 'noteditable',
                    readOnly: true
                }, {
                    fieldLabel: 'Прогноз компенсации на 1 торговую точку, шт',
                    name: 'EstimatedSalesAmountQtyPerStr',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable',
//                    validator: function(value) {
//                        if (value > me.MaxNumberToOneStore.getValue()) {
//                            return '"Прогноз компенсации на 1 торговую точку" должен быть меньше "Макс. разрешенное кол-во на 1 точку"';
//                        }
//
//                        return true;
//                    },
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var EstimatedSalesAmountQty = form.down('[name=EstimatedSalesAmountQty]').getValue();
                        var StoreAmount = form.down('[name=StoreAmount]').getValue();
                        var value;

                        if (+StoreAmount === 0) {
                            value = 0;
                        } else {
                            value = EstimatedSalesAmountQty / StoreAmount;
                        }

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }, {
                    fieldLabel: 'Стоимость публикации в каталоге, руб',
                    name: 'CataloguePubCost',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowDecimals: true,
                    value: 0,
                    minValue: 0
                }]

            }]
        };

        var factTab = {
            xtype: 'container',
            autoScroll: true,
            border: false,
            layout: 'column',
            title: 'Ввод факта',
            items: [{
                xtype: 'fieldset',
                columnWidth: 0.5,
                defaults: {
                    xtype: 'numberfield',
                    anchor: '100%',
                    labelWidth: 160,
                    decimalSeparator: '.'
                },
                border: false,
                items: [{
                    fieldLabel: 'Реальная цена на полке, руб',
                    name: 'RealSellingPrice',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowDecimals: true,
                    value: 0,
                    minValue: 0,
                    allowBlank: false
                }, {
                    fieldLabel: 'Продажи во время акции с учетом скидки, шт',
                    name: 'RealSalesAmountQty',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable',
                    listeners: {
                        'change': me.fieldChangeHandler
                    }
                }, {
                    fieldLabel: 'Продажи во время акции с учетом скидки, руб',
                    name: 'RealSalesAmountVal',
                    allowDecimals: true,
                    readOnly: true,
                    cls: 'noteditable',
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var RealSalesAmountQty = form.down('[name=RealSalesAmountQty]').getValue();
                        var NetIncomePriceAction = form.down('[name=NetIncomePriceAction]').getValue();
                        var value;

                        value = NetIncomePriceAction * RealSalesAmountQty;

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }]
            }, {
                xtype: 'fieldset',
                columnWidth: 0.5,
                defaults: {
                    xtype: 'numberfield',
                    anchor: '100%',
                    labelWidth: 160,
                    decimalSeparator: '.'
                },
                border: false,
                items: [{
                    fieldLabel: 'Продажи во время акции в регулярных ценах, руб',
                    name: 'RealSalesAmountInRegularPrices',
                    readOnly: true,
                    cls: 'noteditable',
                    listeners: {
                        'change': me.fieldChangeHandler
                    },
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        //@todo Delete if task 86 success 
                        //Incorrect, should be used by NetIncomMoreBefore, according to the stored procedure
                        //var SellingPriceBefore = form.down('[name=SellingPriceBefore]').getValue(); 
                        var NetIncomePriceBefore = form.down('[name=NetIncomePriceBefore]').getValue();
                        var RealSalesAmountQty = form.down('[name=RealSalesAmountQty]').getValue();
                        var value;

                        value = NetIncomePriceBefore * RealSalesAmountQty;

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }, {
                    fieldLabel: 'Затраты дистрибьютора, руб',
                    name: 'DistrCost',
                    readOnly: true,
                    cls: 'noteditable',
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var RealSalesAmountInRegularPrices = form.down('[name=RealSalesAmountInRegularPrices]').getValue();
                        var DistrDiscount = form.down('[name=DistrDiscount]').getValue();
                        var value;

                        value = RealSalesAmountInRegularPrices * DistrDiscount / 100;

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }, {
                    fieldLabel: 'Затраты ОТ на акцию, руб',
                    name: 'OTCost',
                    readOnly: true,
                    cls: 'noteditable',
                    listeners: {
                        'change': me.fieldChangeHandler
                    },
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var RealSalesAmountInRegularPrices = form.down('[name=RealSalesAmountInRegularPrices]').getValue();
                        var OTDiscount = form.down('[name=OTDiscount]').getValue();
                        var value;

                        value = RealSalesAmountInRegularPrices * OTDiscount / 100;

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }, {
                    fieldLabel: 'Компенсация ФГД, руб',
                    name: 'FGDComp',
                    readOnly: true,
                    cls: 'noteditable',
                    listeners: {
                        'change': me.fieldChangeHandler
                    },
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var RealSalesAmountInRegularPrices = form.down('[name=RealSalesAmountInRegularPrices]').getValue();
                        var OTCost = form.down('[name=OTCost]').getValue();
                        var FGD = form.down('[name=FGD]').getValue();
                        var value;

                        //value = RealSalesAmountInRegularPrices * OTCost * FGD / 100;
                        if ((RealSalesAmountInRegularPrices * FGD / 100) > OTCost) {
                           value = OTCost;
                        } else {
                            value = RealSalesAmountInRegularPrices * FGD / 100;
                        }

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }, {
                    fieldLabel: 'Компенсация Д0, руб',
                    name: 'D0Comp',
                    readOnly: true,
                    cls: 'noteditable',
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var OTCost = form.down('[name=OTCost]').getValue();
                        var FGDComp = form.down('[name=FGDComp]').getValue();
                        var value;

                        value = OTCost - FGDComp;

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }, {
                    fieldLabel: 'Прирост, %',
                    name: 'SalesIncrease',
                    readOnly: true,
                    cls: 'noteditable',
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var RealSalesAmountQty = form.down('[name=RealSalesAmountQty]').getValue();
                        var SalesAmountBeforeQty = form.down('[name=SalesAmountBeforeQty]').getValue();
                        var value;

                        if (+SalesAmountBeforeQty === 0) {
                            value = 0;
                        } else {
                            value = (RealSalesAmountQty / SalesAmountBeforeQty - 1) * 100;
                        }

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }, {
                    fieldLabel: 'Процент затрат, %',
                    name: 'IncreasePercent',
                    readOnly: true,
                    cls: 'noteditable',
                    recalculateFormula: function() {
                        var form = me.down('form');
                        var field = this;
                        var OTCost = form.down('[name=OTCost]').getValue();
                        var CataloguePubCost = form.down('[name=CataloguePubCost]').getValue();
                        var RealSalesAmountInRegularPrices = form.down('[name=RealSalesAmountInRegularPrices]').getValue();
                        var value;

                        if (!RealSalesAmountInRegularPrices || RealSalesAmountInRegularPrices === 0) {
                            value = 0;
                        } else {
                            value = ((OTCost + CataloguePubCost) / RealSalesAmountInRegularPrices) * 100
                        }

                        field.suspendEvents();
                        field.reset();
                        field.resumeEvents();
                        field.setValue(value);
                    }
                }]
            }]
        };

        formTabs.push(planTab);

        me.items = [{
            xtype: 'form',
            name: 'headerForm',
            bodyPadding: 5,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [{
                xtype: 'container',
                border: false,
                layout: 'column',
                items: [{
                    xtype: 'fieldset',
                    columnWidth: 0.5,
                    defaults: {
                        xtype: 'textfield',
                        anchor: '100%',
                        labelWidth: 150
                    },
                    border: false,
                    items: [{
                        cls: 'noteditable',
                        name: 'RequestID',
                        fieldLabel: 'ID заявки',
                        readOnly: true
                    }, {
                        cls: 'noteditable',
                        name: 'ID',
                        fieldLabel: 'ID',
                        readOnly: true
                    }, {
                        xtype: 'fieldcontainer',
                        fieldLabel: 'Период акции',
                        afterLabelTextTpl: me.afterLabelTextTpl,
                        combineErrors: true,
                        msgTarget: 'side',
                        layout: 'hbox',
                        defaults: {
                            flex: 1,
                            hideLabel: true
                        },
                        items: [
                            {
                                xtype: 'datefield',
                                fieldLabel: 'Дата начала акции',
                                format: 'Y-m-d',
                                name: 'DateStart',
                                margin: '0 5 0 0',
                                cls: 'noteditable',
                                readOnly: true
                            }, {
                                xtype: 'datefield',
                                fieldLabel: 'Дата окончания акции',
                                format: 'Y-m-d',
                                name: 'DateEnd',
                                cls: 'noteditable',
                                readOnly: true
                            }
                        ]
                    }, {
                        fieldLabel: 'Наименование товара',
                        name: 'GoodID',
                        attribute: 'OrimiGoodID',
                        afterLabelTextTpl: me.afterLabelTextTpl,
                        xtype: 'modalcombo',
                        store: 'TestOrimi.store.OrimiGoodsStore',
                        allowBlank: false,
                        listeners: {
                            'change': me.setBossOrder
                        }
                    }, {
                        fieldLabel: 'Код ОРИМИ ТРЭЙД',
                        name: 'OrimiCode',
                        cls: 'noteditable',
                        readOnly: true
                    }, {
                        fieldLabel: 'Фасовка товара',
                        name: 'ExternalPack',
                        readOnly: true,
                        cls: 'noteditable'
                    }, {
                        fieldLabel: 'Бренд',
                        name: 'Brand',
                        readOnly: true,
                        cls: 'noteditable'
                    }, {
                        fieldLabel: 'Сегмент',
                        name: 'Segment',
                        readOnly: true,
                        cls: 'noteditable'
                    }, {
                        fieldLabel: 'Упаковка товара',
                        name: 'InternalPack',
                        readOnly: true,
                        cls: 'noteditable'
                    },  {
                        //fieldLabel: 'Макс. количество единиц<br>к отгрузке по акции, шт',
                        name: 'MaxNumberToShip',
                        inputType: 'hidden',
                        readOnly: true
                    },
                    {
                        fieldLabel: 'Статус записи',
                        xtype: 'textareafield',
                        readOnly: true,
                        height: 48,
                        //cls: 'noteditable',
                        listeners: {
                          render: function(field) {
                            var form = me.down('form');
                            var EstimatedSalesAmountQtyPerStr = form.down('[name=EstimatedSalesAmountQtyPerStr]')?form.down('[name=EstimatedSalesAmountQtyPerStr]').getValue():null; 
                            var MaxNumberToOneStore = form.down('[name=MaxNumberToOneStore]')?form.down('[name=MaxNumberToOneStore]').getValue():null; 
                            var EstimatedSellingPrice = form.down('[name=EstimatedSellingPrice]')?form.down('[name=EstimatedSellingPrice]').getValue():null; 
                            var MinPrice = form.down('[name=MinPrice]')?form.down('[name=MinPrice]').getValue():null; 
                            var RealSellingPrice = form.down('[name=RealSellingPrice]')?form.down('[name=RealSellingPrice]').getValue():null; 
                            var AmountOfDaysAfterPrev = form.down('[name=AmountOfDaysAfterPrev]')?form.down('[name=AmountOfDaysAfterPrev]').getValue():null; 
                            var MaxNumberToShip = form.down('[name=MaxNumberToShip]')?form.down('[name=MaxNumberToShip]').getValue():null; 

                            if (EstimatedSalesAmountQtyPerStr > MaxNumberToOneStore) {
                                field.addCls('alert-row');
                                field.setValue('Прогноз компенсации на 1 тт меньше макс. разрешенное кол-во на 1 точку');
                            } else if (EstimatedSellingPrice < MinPrice)  {
                                field.addCls('alert-row');
                                field.setValue('Прогноз цены позиции на полке в ТТ во время акции меньше Минимальной возможной цены на полке за период');
                            } else if  (RealSellingPrice && RealSellingPrice < MinPrice) {
                                field.addCls('alert-row');
                                field.setValue('Прогноз цены позиции на полке в ТТ во время акции меньше Минимальной возможной цены на полке за период');
                            } else if (AmountOfDaysAfterPrev && AmountOfDaysAfterPrev < 90) {
                                field.addCls('yellow-row');
                                field.setValue('Количество дней от предыдущей акции меньше 90');
                            } else if (EstimatedSalesAmountQtyPerStr > MaxNumberToShip || !MaxNumberToShip) {
                                field.addCls('temp-status-row');
                                field.setValue('Прогноз компенсации на 1 торговую тт > Макс. количество единиц к отгрузке по акции');
                            } else {
                                field.addCls('noteditable');
                            }                            
                          }  
                        }
                    }]
                }, {
                    columnWidth: 0.5,
                    xtype: 'fieldset',
                    defaults: {
                        xtype: 'textfield',
                        anchor: '100%',
                        labelWidth: 160
                    },
                    border: false,
                    items: [{
                        fieldLabel: 'Участок',
                        name: 'Area',
                        readOnly: true,
                        cls: 'noteditable'
                    }, {
                        fieldLabel: 'Группа контрагентов',
                        name: 'ContractorGroup',
                        readOnly: true,
                        cls: 'noteditable'
                    }, {
                        fieldLabel: 'Название сети',
                        name: 'Net',
                        readOnly: true,
                        cls: 'noteditable'
                    }, {
                        fieldLabel: 'Название ТТ',
                        name: 'StoreName',
                        readOnly: true,
                        cls: 'noteditable'
                    }, {
                        fieldLabel: 'Канал для рекламы',
                        name: 'AdvertisingChannel',
                        readOnly: true,
                        cls: 'noteditable'
                    }, {
                        fieldLabel: 'Количество ТТ сети',
                        name: 'StoreAmount',
                        cls: 'noteditable',
                        readOnly: true
                    }, {
                        xtype: 'textareafield',
                        name: 'Comment',
                        fieldLabel: 'Комментарий',
                        height: 110
                    }]
                }]
            }, {
                xtype: 'tabpanel',
                plain: true,
                flex: 1,
                activeTab: 0,
                defaults: {
                    bodyPadding: 10
                },
                items: formTabs
            }]
        }];

        if (requestStatus !== 'Ввод плана') {
            Ext.Array.each(planTab.items, function(column) {
                Ext.Array.each(column.items, function(field) {
                    field.readOnly = true;
                    field.cls = 'noteditable';
                });
            });

            Ext.Array.each(me.items[0].items[0].items, function(column) {
                Ext.Array.each(column.items, function(field) {
                    if (field.name === 'Comment') {
                        if (requestStatus === 'Акция закрыта') {
                            field.readOnly = true;
                            field.hideTrigger = true;
                            field.cls = 'noteditable';
                        } else {
                            return;
                        }
                    }

                    if (field.xtype === 'fieldcontainer') {
                        field.items[0].readOnly = true;
                        field.items[0].hideTrigger = true;
                        field.items[0].cls = 'noteditable';

                        field.items[1].readOnly = true;
                        field.items[1].hideTrigger = true;
                        field.items[1].cls = 'noteditable';
                        return;
                    }

                    field.readOnly = true;
                    field.hideTrigger = true;
                    field.cls = 'noteditable';
                });
            });
        }

        if (requestStatus === 'Ввод факта' || requestStatus === 'Ручной ввод факта' || requestStatus === 'Акция закрыта') {
            formTabs.push(factTab);

            if (requestStatus === 'Ручной ввод факта') {
                Ext.Array.each(factTab.items[0].items, function(field) {
                    if (field.name === 'RealSalesAmountQty') {
                        field.readOnly = false;
                        field.cls = '';
                    }
                });
            }

            if (requestStatus === 'Акция закрыта') {
                Ext.Array.each(factTab.items, function(column) {
                    Ext.Array.each(column.items, function(field) {
                        field.readOnly = true;
                        field.cls = 'noteditable';
                    });
                });
            }
        }

        this.callParent(arguments);

        form = me.down('form');

        if (!userRights['Advertising_events_edit']) {
            form.addCls('noteditable');

            fields = form.query('textfield');
            mcombos = form.query('modalcombo');

            for (i = 0; i < mcombos.length; i++) {
                mcombos[i].readOnly = true;
                mcombos[i].hideTrigger = true;
            }

            for (i = 0; i < fields.length; i++) {
                fields[i].readOnly = true;
            }

            me.down('[name=okButton]').disable();
        }

        me.MaxNumberToOneStore = form.down('[name=MaxNumberToOneStore]');
        me.EstimatedSalesAmountQtyPerStr = form.down('[name=EstimatedSalesAmountQtyPerStr]');

        var copy;

        if (me.data) {
            form.loadRecord(me.data);
            checkMaxNumberToOneStore();
        } else {
            copy = me.AdvertisingRequestRecord.copy();
            copy.set('ID', 0);
            copy.set('Comment', '');
            form.loadRecord(copy);
            form.down('[name=RequestID]').setValue(me.AdvertisingRequestRecord.get('ID'));
        }

        function checkMaxNumberToOneStore() {
            if (me.EstimatedSalesAmountQtyPerStr.getValue() > me.MaxNumberToOneStore.getValue()) {
                setTimeout(function() {
                    TestOrimi.getApplication().getController('Main')
                             .showNotification('Ошибка! "Прогноз компенсации на 1 торговую точку" должен быть меньше "Макс. разрешенное кол-во на 1 точку"',
                                 'winError'
                             );
                }, 100);
            }
        }

        me.recalculateAll();
    }
});

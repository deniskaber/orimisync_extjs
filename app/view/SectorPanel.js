Ext.define('TestOrimi.view.SectorPanel', {
    extend: 'Ext.container.Container',
    xtype: 'sectorPanel',
    border: false,
    req: false,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    height: 100,
    margin: '0 0 10 0',
    items: [{
        xtype: 'container',
        width: 150,
        layout: 'vbox',
        items: [{
            xtype: 'container',
            html: 'Сектор:<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
            height: 20,
            margin: '4 0 10 0'
        }, {
            xtype: 'buttongroup',
            //cls: 'barcodeButtonGroup',
            frame: false,
            name: 'sectoreButtonGroup', //barcodeButtonGroup
            columns: 2,
            border: false,
            items: [{
                xtype: 'button',
                text: '<span class="oi oiadd" data-glyph="plus" aria-hidden="true"></span>',
                height: 37,
                tooltip: 'Добавить',
                scale: 'medium',
                handler: function () {
                    Ext.create('TestOrimi.view.SectorWindow', {selector: this}).show();
                }
            }, {
                xtype: 'button',
                margin: '0 0 0 8',
                height: 37,
                text: '<span class="oi oiadd" data-glyph="trash" aria-hidden="true"></span>',
                tooltip: 'Удалить',
                scale: 'medium',
                handler: function () {
                    var container = this.up('sectorPanel'),
                        listadd = container.down('boundlist'),
                        selected = listadd.getSelectionModel().getSelection()[0];

                    if (selected) {
                        Ext.getStore('TestOrimi.store.SelectedSectorStore').remove(selected);
                    } else {
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сектор не выделен', 'winError');
                    }
                }
            }]
        }]
    }, {
        margin: '0 0 0 16',
        xtype: 'boundlist',
        anchor: '100%',
        height: 98,
        attribute: 'SectorID',
        trackOver: false,
        flex: 1,
        displayField: 'Name',
        valueField: 'ID',
        store: 'TestOrimi.store.SelectedSectorStore',
        //name        : 'SectorID',
        name: 'sector',
        queryMode: 'local',
        multiSelect: true,
        listeners: {
            afterrender: function(boundlist) {
                if (Ext.getStore('TestOrimi.store.SelectedSectorStore').getCount()) {
                    if (!this.up('sectorPanel').down('buttongroup').isDisabled()) {
                        var areaPanel = this.up('window').down('areaPanel');
                        areaPanel.down('buttongroup').disable();
                        areaPanel.addCls('noteditable');
                    }
                } else if (Ext.getStore('TestOrimi.store.SelectedAreaStore').getCount()) {
                    if (!this.up('sectorPanel').down('buttongroup').isDisabled()) {
                        var sectorPanel = this.up('window').down('sectorPanel');
                        sectorPanel.down('buttongroup').disable();
                        sectorPanel.addCls('noteditable');
                    }
                }
            },

            itemadd: function (records, index, node, eOpts) {                
                console.log('SectorPanel itemadd');
                var record,
                    index,
                    area,
                    i = 0,
                    length = records.length;

                if (!this.up('sectorPanel').down('buttongroup').isDisabled()) {
                    var areaPanel = this.up('window').down('areaPanel');
                    areaPanel.down('buttongroup').disable();
                    areaPanel.addCls('noteditable');
                }

                if (!length) {
                    return;
                }

                var areaStore = Ext.getStore('TestOrimi.store.AreaStore');
                var selectedAreaStore = Ext.getStore('TestOrimi.store.SelectedAreaStore')

                for (i=0; i < length; i++) {
                    record = records[i];
                    index = Ext.getStore('TestOrimi.store.SelectedAreaStore').findExact('ID', record.get('AreaID'));

                    if (index < 0) {                        
                        area = areaStore.getAt(areaStore.findExact('ID', record.get('AreaID')));
                        selectedAreaStore.add({
                            Name: area.get('Name'),
                            ID: area.get('ID'),
                            DivisionID: area.get('DivisionID')
                        });
                    }
                }                
            },
            itemremove: function (record, index, eOpts) {
                console.log('SectorPanel itemremove');
                var areaId = record.get('AreaID'),
                    storeSelSec = Ext.getStore('TestOrimi.store.SelectedSectorStore'),
                    storeSelArea = Ext.getStore('TestOrimi.store.SelectedAreaStore');

                if (storeSelSec.findExact('AreaID', areaId) < 0) {
                    var areaIndex = storeSelArea.findExact('ID', areaId);
                    if (areaIndex > -1) {
                        storeSelArea.remove(storeSelArea.getAt(areaIndex));
                    }                    
                }

                if (!storeSelSec.getCount() && !this.up('sectorPanel').down('buttongroup').isDisabled()) {
                    var areaPanel = this.up('window').down('areaPanel');
                    areaPanel.down('buttongroup').enable();
                    areaPanel.removeCls('noteditable');
                    
                }
            }
        }
    }]
});

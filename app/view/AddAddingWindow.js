Ext.define('TestOrimi.view.AddAddingWindow', {
    extend: 'Ext.window.Window',
	width: 400,
    //height: 280,
	modal: true,
	resizable: false,
	layout: 'fit',
    items: [{
		xtype	: 'form',
		layout	: 'anchor',
		padding	: 5,
		defaults: {
			xtype	: 'textfield',
			anchor	: '100%'
		},
		items: [{
			cls			: 'noteditable',
			fieldLabel  : 'ID',
			name 		: 'ID',
			readOnly 	: true
		},{
			fieldLabel	: 'Наименование',
			name		: 'Name',
			allowBlank	: false
		},{
			fieldLabel	: 'Краткое наим.',
			name		: 'ShortName'
		},{
			fieldLabel 	: 'Краткое обозн.',
			name  		: 'BriefNotation'
		},{
			xtype 		: 'checkbox',
			fieldLabel 	: 'Удален',
			name 		: 'Deleted',
			inputValue 	: 1
		}]
	}],
	buttons		:  [{
		text 	: 'Ок',
		handler: function() {
			var win = this.up('window'),
				form = win.down('form').getForm();

			if (form.isValid()){
				var myMask = new Ext.LoadMask(win, {msg:"Сохраняю..."});

				myMask.show();
				form.submit({
					url: 'resources/data/api.php',
					timeout: 600000, 
          			params: {
						act: 'getAdding',
						subaction: win.data ? 'update': 'create'
					},
					success: function(response) {
						myMask.destroy();
						var responseText = Ext.JSON.decode(response.responseText);
						if (responseText)
	                    	TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + responseText.msg, 'winError');
	                    else {
		                    if (win.data) {
								TestOrimi.getApplication().getController('Main').loadActiveMasterStore();
								var store = Ext.getStore('TestOrimi.store.SelectedAddingStore'),
									index = store.findExact('ID', win.data.get('ID'));
								if (index >= 0)
									store.getAt(index).set('Name', win.down('[name=Name]').getValue())

								TestOrimi.getApplication().getController('Main').showNotification('Успешно! Элемент обновлен', 'winSuccess');
							} else {
								TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess');
							}
							Ext.getStore('TestOrimi.store.AddingStore').load();
							win.destroy();
						}
					},
					failure: function(form, action) {
						myMask.destroy();
						TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' +  ( action.result ? action.result.msg : ' Нет ответа от сервера' ) , 'winError');
					}
				});
			}
		}
	},{
		text: 'Отмена',
		handler: function() {
			this.up('window').destroy();
		}
	}],

	initComponent: function() {
		this.callParent(arguments);

		if (this.data)
			this.down('form').loadRecord(this.data);
	}
});

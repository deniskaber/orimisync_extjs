Ext.define('TestOrimi.view.AdvertisingEventsHeadersGrid', {
    extend: 'Ext.grid.Panel',
    requires: ['TestOrimi.store.AdvertisingEventsHeadersStore'],
    xtype: 'advertising_events_headers_grid',
    flex: 1,
    columnLines: true,
    border: false,
    viewConfig: {
        stripeRows: false,
        getRowClass: function(record) {
            switch (record.get('Status')) {
                case 'Акция закрыта':
                    return 'del-status-row';
                case 'Ввод плана':
                    return 'temp-status-row';
                case 'Ввод факта':
                case 'Ручной ввод факта':
                    return 'success-status-row';
            }
        },
        shrinkWrap: 0,
        shadow: false,
        trackOver: false,
        overItemCls: false
    },
    rendererGridCell: function(val) {
        if (val !== 0) {
            return Ext.util.Format.number(val, '0.000/i').replace(/\./g, ' ');
        }
    },

    initComponent: function() {
        var me         = this,
            userRights = TestOrimi.getApplication().data;

        this.store = 'TestOrimi.store.AdvertisingEventsHeadersStore';

        this.columns = [
            {
                text: 'Статус',
                dataIndex: 'Status',
                width: 160
            },
            {
                text: 'ID',
                dataIndex: 'ID',
                width: 80
            },
            {
                text: 'Номер',
                dataIndex: 'DocNum',
                width: 130
            },
            {
                text: 'Заголовок',
                dataIndex: 'Title',
                flex: 1,
                minWidth: 120
            },
            {
                text: 'Участок',
                dataIndex: 'Area',
                width: 130
            },
            {
                text: 'Группа контрагентов',
                dataIndex: 'ContractorGroup',
                width: 220
            },
            {
                text: 'Название сети',
                dataIndex: 'Net',
                width: 130
            },
            {
                text: 'Название ТТ',
                dataIndex: 'StoreName',
                width: 130
            },
            {
                text: 'Адрес ТТ',
                dataIndex: 'StoreAddress',
                width: 130
            },
            {
                text: 'Канал для рекламы',
                dataIndex: 'AdvertisingChannel',
                width: 130
            },
            {
                text: 'Количество ТТ',
                dataIndex: 'StoreAmount',
                width: 130,
                align: 'right'
            },
            {
                text: 'Макс. количество единиц к отгрузке по акции, шт',
                dataIndex: 'MaxNumberToShip',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell,
                hidden: true
            },
            {
                text: 'Макс. разрешенное кол-во на 1 точку, шт',
                dataIndex: 'MaxNumberToOneStore',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Дата начала акции',
                dataIndex: 'DateStart',
                width: 130
            },
            {
                text: 'Дата окончания акции',
                dataIndex: 'DateEnd',
                width: 130
            },
            {
                text: 'Месяц акции',
                dataIndex: 'DateStartMonth',
                width: 130
            },
            {
                text: 'Комментарий',
                dataIndex: 'Comment',
                width: 150,
                hidden: true
            },
            {
                text: 'Дата начала отгрузки',
                dataIndex: 'DateStartShipping',
                width: 130
            },
            {
                text: 'Дата окончания отгрузки',
                dataIndex: 'DateEndShipping',
                width: 130
            },
            {
                text: 'Отражена в TradeNT',
                dataIndex: 'isInTradent',
                width: 160
            },
            {
                text: 'Номер ТСЗ',
                dataIndex: 'TSZCode',
                width: 110
            },
            {
                text: 'Вид оплаты',
                dataIndex: 'PaymentType',
                width: 140
            },
            {
                text: 'Автор изменений',
                dataIndex: 'Author',
                width: 170
            },
            {
                text: 'Дата создания',
                dataIndex: 'DocDate',
                width: 170,
                hidden: true
            },
            {
                text: 'Дата посл. изменения',
                dataIndex: 'DateChange',
                width: 200
            }
        ];

        var i       = 0,
            columns = this.columns,
            length  = columns.length;

        for (; i < length; i++) {
            columns[i].items = [{
                xtype: 'container',
                margin: 4,
                flex: 1,
                layout: 'fit',
                listeners: {
                    scope: this,
                    element: 'el',
                    mousedown: function(e) {
                        e.stopPropagation();
                    },
                    click: function(e) {
                        e.stopPropagation();
                    },
                    keydown: function(e) {
                        e.stopPropagation();
                    },
                    keypress: function(e) {
                        e.stopPropagation();
                    },
                    keyup: function(e) {
                        e.stopPropagation();
                    }
                },
                items: [{
                    xtype: 'textfield',
                    emptyText: 'Поиск',
                    enableKeyEvents: true,
                    onTriggerClick: function() {
                        this.reset();
                        this.focus();
                    },
                    listeners: {
                        change: function(field, newValue, oldValue, eOpts) {
                            me.fireEvent('searchchange', me, field, newValue, oldValue);
                        },
                        buffer: 1000
                    }
                }]
            }];
        }

        var hasApproveRights = userRights['Advertising_events_adv_appr']
            || userRights['Advertising_events_sales_appr']
            || userRights['Advertising_events_s_boss_appr']
            || userRights['Advertising_events_net_appr']
            || userRights['Advertising_events_fact_appr']
            || userRights['Advertising_events_rp_appr'];

        this.lbar = [{
            xtype: 'container',
            width: 55,
            padding: 2,
            border: true,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            defaults: {
                xtype: 'button',
                margin: '2 0 2 0',
                scale: 'medium'
            },
            items: [{
                name: 'combSearch',
                tooltip: 'Комб. поиск',
                text: '<span class="oi" data-glyph="magnifying-glass" aria-hidden="true"></span>',
                handler: function() {
                    Ext.create('TestOrimi.view.CreateSearchWindowAdvertisingEventHeader', {grid: me}).show();
                }
            }, {
                name: 'approveAllButton',
                tooltip: 'Утвердить все акции на текущей странице',
                hidden: !hasApproveRights,
                text: '<span class="oi" data-glyph="thumb-up" aria-hidden="true"></span>',
                handler: function() {
                    me.fireEvent('approveAllButtonClick', me);
                }
            }, {
                name: 'loadTSZButton',
                tooltip: 'Выгрузить в ТСЗ',
                text: '<span class="oi" data-glyph="spreadsheet" aria-hidden="true"></span>',
                handler: function() {
                    me.fireEvent('loadTSZButtonClick', me);
                }
            }, {
                name: 'addNewRowButton',
                tooltip: 'Добавление новой записи',
                text: '<span class="oi" data-glyph="document" aria-hidden="true"></span>',
                disabled: !userRights['Advertising_events_edit'],
                handler: function() {
                    Ext.create('TestOrimi.view.CreateAdvertisingEventWindow').show();
                }
            }, {
                name: 'duplicateButton',
                tooltip: 'Создать на основе выбранного',
                disabled: true,
                scale: 'medium',
                text: '<span class="oi" data-glyph="fork" aria-hidden="true"></span>',
                handler: function() {
                    me.fireEvent('duplicateButtonClick', me);
                }
            }, {
                name: 'eventHistoryButton',
                tooltip: 'Просмотр истории работы с позицией',
                scale: 'medium',
                text: '<span class="oi" data-glyph="book" aria-hidden="true"></span>',
                handler: function() {
                    var selection      = me.getSelectionModel().getSelection(),
                        selectedRecord = selection && selection[0];

                    if (!selectedRecord) {
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Позиция не выбрана', 'winError');
                        return;
                    }

                    me.fireEvent('eventHistoryButtonClick', me, {ref: 'Advertising Events'});
                }
            }, {
                name: 'deleteButton',
                tooltip: 'Удалить',
                disabled: true,
                text: '<span class="oi" data-glyph="trash" aria-hidden="true"></span>',
                handler: function() {
                    me.fireEvent('deleteButtonClick', me);
                }
            }]
        }];

        if (userRights['Advertising_events_edit']) {
            this.columns.push({
                //text: 'Отправить на<br>утверждение',
                text: 'Ввод плана',
                menuDisabled: true,
                sortable: false,
                xtype: 'actioncolumn',
                menuText: 'Отправить на утверждение',
                width: 120,
                items: [{
                    getClass: function(v, meta, rec) {
                        if (rec.get('Status') === 'Ввод плана') {
                            return 'allow-col';
                        }
                    },
                    getTip: function(v, meta, rec) {
                        if (rec.get('Status') === 'Ввод плана') {
                            return 'Отправить';
                        }
                    },
                    handler: function(view, rowIndex, colIndex) {
                        me.fireEvent('updateStatus', me, rowIndex, colIndex, 'forward');
                    }
                }]
            });
        }

        if (userRights['Advertising_events_adv_appr']) {
            this.columns.push({
                text: 'План на утверждение<br>в отделе рекламы',
                menuDisabled: true,
                sortable: false,
                xtype: 'actioncolumn',
                //menuText: 'Утвердить (реклама)',
                menuText: 'План на утверждение в отделе рекламы',
                width: 200,
                items: [{
                    getClass: function(v, meta, rec) {
                        if (rec.get('Status') === 'План на утверждении в отделе рекламы') {
                            return 'allow-col';
                        }
                    },
                    getTip: function(v, meta, rec) {
                        if (rec.get('Status') === 'План на утверждении в отделе рекламы') {
                            return 'Утвердить';
                        }
                    },
                    handler: function(view, rowIndex, colIndex) {
                        me.fireEvent('updateStatus', me, rowIndex, colIndex, 'forward');
                    }
                }, {
                    getClass: function(v, meta, rec) {
                        if (rec.get('Status') === 'План на утверждении в отделе рекламы') {
                            return 'deny-col';
                        }
                    },
                    getTip: function(v, meta, rec) {
                        if (rec.get('Status') === 'План на утверждении в отделе рекламы') {
                            return 'Вернуть';
                        }
                    },
                    handler: function(view, rowIndex, colIndex) {
                        me.fireEvent('updateStatus', me, rowIndex, colIndex, 'return');
                    }
                }]
            });
        }

        if (userRights['Advertising_events_sales_appr']) {
            this.columns.push({
                //text: 'Утвердить<br>(продажи)',
                text: 'План на утверждение<br>в отделе продаж',
                menuDisabled: true,
                sortable: false,
                xtype: 'actioncolumn',
                menuText: 'План на утверждение в отделе продаж',
                width: 220,
                items: [{
                    getClass: function(v, meta, rec) {
                        if (rec.get('Status') === 'План на утверждении в отделе продаж') {
                            return 'allow-col';
                        }
                    },
                    getTip: function(v, meta, rec) {
                        if (rec.get('Status') === 'План на утверждении в отделе продаж') {
                            return 'Утвердить';
                        }
                    },
                    handler: function(view, rowIndex, colIndex) {
                        me.fireEvent('updateStatus', me, rowIndex, colIndex, 'forward');
                    }
                }, {
                    getClass: function(v, meta, rec) {
                        if (rec.get('Status') === 'План на утверждении в отделе продаж') {
                            return 'deny-col';
                        }
                    },
                    getTip: function(v, meta, rec) {
                        if (rec.get('Status') === 'План на утверждении в отделе продаж') {
                            return 'Вернуть';
                        }
                    },
                    handler: function(view, rowIndex, colIndex) {
                        me.fireEvent('updateStatus', me, rowIndex, colIndex, 'return');
                    }
                }]
            });
        }

        if (userRights['Advertising_events_net_appr']) {
            this.columns.push({
                //text: 'Утвердить<br>(сеть)',
                text: 'На согласовании<br> с сетью',
                menuDisabled: true,
                sortable: false,
                xtype: 'actioncolumn',
                menuText: 'На согласовании с сетью',
                width: 180,
                items: [{
                    getClass: function(v, meta, rec) {
                        if (rec.get('Status') === 'На согласовании с сетью') {
                            return 'allow-col';
                        }
                    },
                    getTip: function(v, meta, rec) {
                        if (rec.get('Status') === 'На согласовании с сетью') {
                            return 'Утвердить';
                        }
                    },
                    handler: function(view, rowIndex, colIndex) {
                        me.fireEvent('updateStatus', me, rowIndex, colIndex, 'forward');
                    }
                }, {
                    getClass: function(v, meta, rec) {
                        if (rec.get('Status') === 'На согласовании с сетью') {
                            return 'deny-col';
                        }
                    },
                    getTip: function(v, meta, rec) {
                        if (rec.get('Status') === 'На согласовании с сетью') {
                            return 'Вернуть';
                        }
                    },
                    handler: function(view, rowIndex, colIndex) {
                        me.fireEvent('updateStatus', me, rowIndex, colIndex, 'return');
                    }
                }]
            });
        }

        if (userRights['Advertising_events_fact_appr']) {
            this.columns.push({
                //text: 'Отправить на утверждение<br>после ввода факта',
                text: 'Ввод факта\\<br>Ручной ввод факта',
                menuDisabled: true,
                sortable: false,
                xtype: 'actioncolumn',
                menuText: 'Ввод факта Ручной ввод факта',
                width: 200,
                items: [{
                    getClass: function(v, meta, rec) {
                        if (rec.get('Status') === 'Ввод факта' || rec.get('Status') === 'Ручной ввод факта') {
                            return 'allow-col';
                        }
                    },
                    getTip: function(v, meta, rec) {
                        if (rec.get('Status') === 'Ввод факта' || rec.get('Status') === 'Ручной ввод факта') {
                            return 'Отправить';
                        }
                    },
                    handler: function(view, rowIndex, colIndex) {
                        me.fireEvent('updateStatus', me, rowIndex, colIndex, 'forward');
                    }
                }, {
                    getClass: function(v, meta, rec) {
                        if (rec.get('Status') === 'Ввод факта' || rec.get('Status') === 'Ручной ввод факта') {
                            return 'deny-col';
                        }
                    },
                    getTip: function(v, meta, rec) {
                        if  (rec.get('Status') === 'Ввод факта' || rec.get('Status') === 'Ручной ввод факта') {
                            return 'Вернуть';
                        }
                    },
                    handler: function(view, rowIndex, colIndex) {
                        me.fireEvent('updateStatus', me, rowIndex, colIndex, 'return');
                    }
                }]
            });
        }

        if (userRights['Advertising_events_rp_appr']) {
            this.columns.push({
                //text: 'Утвердить<br>(РП)',
                text: 'Акция<br>проверена РП',
                menuDisabled: true,
                sortable: false,
                xtype: 'actioncolumn',
                menuText: 'Акция проверена РП',
                width: 120,
                items: [{
                    getClass: function(v, meta, rec) {
                        if (rec.get('Status') === 'Акция проверена РП') {
                            return 'allow-col';
                        }
                    },
                    getTip: function(v, meta, rec) {
                        if (rec.get('Status') === 'Акция проверена РП') {
                            return 'Утвердить';
                        }
                    },
                    handler: function(view, rowIndex, colIndex) {
                        me.fireEvent('updateStatus', me, rowIndex, colIndex, 'forward');
                    }
                }]
            });
        }

        if (userRights['Advertising_events_rp_appr']) {
            this.columns.push({
                //text: 'Утвердить<br>(РП)',
                text: 'Акция проверена /<br> завершена',
                menuDisabled: true,
                sortable: false,
                xtype: 'actioncolumn',
                menuText: 'Акция проверена завершена',
                width: 160,
                items: [{
                    getClass: function(v, meta, rec) {

                    },
                    getTip: function(v, meta, rec) {

                    },
                    handler: function(view, rowIndex, colIndex) {

                    }
                }]
            });
        }

        if (!userRights['Advertising_events']) {
            delete this.lbar;
        }

        this.bbar = {
            xtype: 'pagingtoolbar',
            store: this.store,
            inputItemWidth: 60,
            displayInfo: true
        };

        this.callParent(arguments);

    }
});

Ext.define('TestOrimi.view.general_grid', {
	extend		: 'Ext.grid.Panel',
	requires	: ['TestOrimi.store.MasterGoodStore'],
	xtype		: 'general_grid',
	flex		: 1,
	selModel	: {
		selType	: 'checkboxmodel'
	},
	columnLines	: true,
	border		: false,
	viewConfig: {
		stripeRows: false,
		getRowClass: function(record) {
			return record.get('StatusID') == 2 ? '' : (record.get('StatusID') == 1 ? 'temp-status-row' : 'del-status-row');
		},
		shrinkWrap: 0,
		shadow: false,
		trackOver: false,
		overItemCls: false
	},
	plugins: [{
		ptype				: 'bufferedrenderer',
		trailingBufferZone	: 0,
		leadingBufferZone	: 0
	}],
	listeners: {
		celldblclick: function(grid, td, cellIndex, record, tr, rowIndex) {
			Ext.create('TestOrimi.view.createSkuWindow',{data: record}).show();
		}
	},
	initComponent: function() {
	    var me = this;

	    this.store = 'TestOrimi.store.MasterGoodStore';

		this.columns  =  [
			{ text: 'Статус', 				dataIndex: 'Status', 			width: 120 },
			{ text: 'ID', 					dataIndex: 'ID',				width: 80  },
			{ text: 'Код', 					dataIndex: 'Code', 				width: 110  },
			{ text: 'Короткое название', 	dataIndex: 'ShortName', 		width: 200 },
			{ text: 'Полное название', 		dataIndex: 'Name', 				width: 200, 	hidden: true },
			{ text: 'Черновое название', 	dataIndex: 'TempName', 			width: 200 },
			{ text: 'Категория',			dataIndex: 'Category',			width: 110  },
			{ text: 'Продукция ОТ', 		dataIndex: 'From', 				width: 150 },
			{ text: 'Производитель', 		dataIndex: 'Manufacturer', 		width: 150 },
			{ text: 'Бренд', 				dataIndex: 'Brand', 			width: 125 },
			{ text: 'Саббренд', 			dataIndex: 'SubBrand', 			width: 150 },
			{ text: 'Сегмент', 				dataIndex: 'Segment', 			width: 80  },
			{ text: 'Сабсегмент', 			dataIndex: 'SubSegment', 		width: 100  },
			{ text: 'Цвет чая', 			dataIndex: 'TeaColor', 			width: 120, 	hidden: true },
			{ text: 'Тип упаковки внешней', dataIndex: 'ExternalPack',		width: 180,		hidden: true },
			{ text: 'Тип упаковки саше', 	dataIndex: 'SachetPack', 		width: 150,		hidden: true },
			{ text: 'Тип упаковки внутренней', dataIndex: 'InternalPack', 	width: 200,		hidden: true },
			{ text: 'Упаковка Хорека', 		dataIndex: 'isHorekaPack', 		width: 155,		hidden: true },
			{ text: 'Количество пакетов', 	dataIndex: 'BagQty', 			width: 165,		hidden: true },
			{ text: 'Вес пакета', 			dataIndex: 'BagWeight', 		width: 100,		hidden: true },
			{ text: 'Вес', 					dataIndex: 'Weight', 			width: 80,		hidden: true },
			{ text: 'Вес (группа)', 		dataIndex: 'WeightGroup', 		width: 120,		hidden: true },
			{ text: 'Аромат', 				dataIndex: 'isWithFlavor', 		width: 80,		hidden: true },
			{ text: 'Ароматы', 				dataIndex: 'Flavor', 			width: 166,		hidden: true },
			{ text: 'Добавка', 				dataIndex: 'isWithAdding', 		width: 80,		hidden: true },
			{ text: 'Добавки', 				dataIndex: 'Adding', 			width: 166,		hidden: true },
			{ text: 'Ценовой сегмент', 		dataIndex: 'PriceSegment', 		width: 150,		hidden: true },
			{ text: 'Страна происхождения', dataIndex: 'Country', 			width: 185,		hidden: true },
			{ text: 'Сорт кофе', 			dataIndex: 'CoffeeSort', 		width: 125,		hidden: true },
			{ text: 'Кофеин', 				dataIndex: 'isCaffeine', 		width: 80,		hidden: true },
			{ text: 'Акционная позиция', 	dataIndex: 'Gift', 				width: 195,		hidden: true },
			{ text: 'Тип подарка', 			dataIndex: 'GiftType', 			width: 120,		hidden: true },
			{ text: 'Фотография', 			dataIndex: 'Photo', 			width: 115,		hidden: true },
			{ text: 'Длина', 				dataIndex: 'Length', 			width: 80,		hidden: true },
			{ text: 'Ширина', 				dataIndex: 'Width', 			width: 80,		hidden: true },
			{ text: 'Высота', 				dataIndex: 'Height', 			width: 80,		hidden: true },
			{ text: 'Весовой продукт в сети', dataIndex: 'isWeightNet', 	width: 190,		hidden: true },
			{ text: 'Комментарий', 			dataIndex: 'Comment', 			width: 125,		hidden: true },
			{ text: 'Коллекция/Доп.имя', 	dataIndex: 'Collection', 		width: 165,		hidden: true },
			{ text: 'Частная марка сети', 	dataIndex: 'isPrivateMark', 	width: 165,		hidden: true },
			{ text: 'Штрихкод', 			dataIndex: 'Barcode', 			width: 115,		hidden: true },
			{ text: 'Агрегирующая позиция', dataIndex: 'Parent', 			width: 200 },
			{ text: 'Автор изменений', 		dataIndex: 'Author', 			width: 170 },
			{ text: 'Дата последнего изменения', dataIndex: 'DateChange', 	width: 225,		hidden: true },
			{ text: 'Источник данных', 		dataIndex: 'DataSource', 			width: 170 } 		
	    ];

		var i = 0,
			columns = this.columns,
			length = columns.length;

	    for (;i<length;i++) {
			columns[i].items = [{
		        xtype: 'container',
		        margin: 4,
		        flex: 1,
		        layout: 'fit',
		        listeners: {
					scope: this,
					element: 'el',
					mousedown: function(e) {
					  e.stopPropagation();
					},
					click: function(e) {
					  e.stopPropagation();
					},
					keydown: function(e) {
					   e.stopPropagation();
					},
					keypress: function(e) {
					   e.stopPropagation();
					},
					keyup: function(e) {
					   e.stopPropagation();
					}
		        },
		        items:[{
					xtype: 'textfield',
					emptyText : 'Поиск',
					enableKeyEvents: true,
					onTriggerClick: function () {
						this.reset();
						this.focus();
					},
					listeners: {
						change: function(field, newValue, oldValue, eOpts) {
							me.fireEvent('searchchange', me, field, newValue, oldValue);
						},
						buffer: 1000
					}
		        }]
			}];
	    }

		this.lbar = [{
			xtype: 'general_gridLbar',
			ref: 'Sku',
			grid: this
		}];

	    this.bbar = {
			xtype: 'pagingtoolbar',
			store: this.store,
			inputItemWidth: 60,
			displayInfo: true
		};

	    this.callParent(arguments);
	}
});

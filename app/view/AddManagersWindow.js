Ext.define('TestOrimi.view.AddManagersWindow', {
    extend : 'Ext.window.Window',
    width: 400,
    height: 200,
    modal: true,
    resizable: false,
    layout: 'fit',
    items: [{
        xtype: 'form',
        layout: 'anchor',
        padding: 5,
        defaults: {
            xtype   : 'textfield',
            anchor  : '100%',
            labelWidth  : 140
        },
        items: [{
            fieldLabel  : 'Менеджер',
            name        : 'ManagersID',
            displayField: 'FIO', 
            attribute   : 'ParentStaff',
            xtype       : 'modalcombo',
            store       : 'TestOrimi.store.ParentStore'
        },{ 
            fieldLabel  : 'Дата начала работы',
            name        : 'DateHired',
            xtype       : 'datefield',
            format      : 'Y-m-d'
        },{ 
            fieldLabel  : 'Дата увольнения',
            name        : 'DateFired',
            xtype       : 'datefield',
            format      : 'Y-m-d'
        }]
    }],

    buttons:  [{
        text: 'Ок',
        handler: function() {
            var win = this.up('window'),
                form = win.down('form').getForm();

            if (form.isValid()){ //добавить условие на привязку к тт?
                var myMask = new Ext.LoadMask(win, {msg:"Сохраняю..."});

                myMask.show();
                form.submit({
                    url: 'resources/data/api.php',
                    timeout: 600000, 
                    params: {
                        act: 'getManagers',
                        subaction: win.data ? 'update': 'create',
                        ID: win.data ? win.data.get('ID') : '',
                        StoresID: win.data ? '' : win.fieldID,
                        isNet: win.data ? '' : win.isNet
                    },
                    success: function(response) {
                        myMask.destroy();
                        var responseText = Ext.JSON.decode(response.responseText);
                        if (responseText)
                            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + responseText.msg, 'winError');
                        else {
                              if (win.data) {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Элемент обновлен', 'winSuccess');
                            } else {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess');
                            }
                            Ext.getStore('TestOrimi.store.ManagersStore').load({
                                params: {
                                    StoresID: win.fieldID
                                    ,isNet: win.isNet
                                }
                            });
                            win.destroy();
                        }
                    },
                    failure: function(form, action) {
                        myMask.destroy();
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' +  ( action.result ? action.result.msg : ' Нет ответа от сервера' ) , 'winError');
                    }
                });
            }
        }
    },{
        text: 'Отмена',
        handler: function() {
            this.up('window').destroy();
        }
    }],

    initComponent: function() {
        this.callParent(arguments);

        if (this.data)
            this.down('form').loadRecord(this.data);
    }
});

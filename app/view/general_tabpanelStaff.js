Ext.define('TestOrimi.view.general_tabpanelStaff', {
    extend  : 'Ext.tab.Panel',
    xtype   : 'general_tabpanelStaff',
    flex    : 1,
    ref     : 'Staff',
    split   : true,
    collapsible: true,
    header: false,
    plain   : true,
    title: 'Подчиненные справочники',

    initComponent: function() {	
        this.items = [
            { xtype: 'general_tabpanelFirstcolStaff',        title: 'Отвязать' },
            { xtype: 'general_tabpanelSecondcolStaff',       title: 'Привязать' }
        ];
        this.callParent(arguments);
    }
});
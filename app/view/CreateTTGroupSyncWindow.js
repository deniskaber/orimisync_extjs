﻿Ext.define('TestOrimi.view.CreateTTGroupSyncWindow', {
	extend	: 'Ext.window.Window',
	xtype	: 'createTTGroupSyncWindow',
	width 	: '100%',
	height 	: '100%',
	modal	: true,
	id		: 'createTTGroupSyncWindow-id',
	title	: 'Групповое сопоставление',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
		initComponent: function() {
		var me = this,
			resultStore = 'TestOrimi.store.StoresSyncResultStore';
		
		var columns = [
			{ text: 'ID',						dataIndex: 'masterID',				width: 80  },
			{ text: 'Код',						dataIndex: 'masterCode',			width: 130,		hidden: true},
			{ text: 'Название',					dataIndex: 'masterName',			width: 200 },
			{ text: 'Черновой адрес',			dataIndex: 'masterTempAddress',		width: 180 },
			{ text: 'Индекс',					dataIndex: 'masterPostCode',		width: 100,		hidden: true},
			{ text: 'Округ',					dataIndex: 'masterCounty',			width: 110,		hidden: true},
			{ text: 'Регион',					dataIndex: 'masterRegion',			width: 110,		hidden: true},
			{ text: 'Район',					dataIndex: 'masterDistrict',		width: 120,		hidden: true},
			{ text: 'Город',					dataIndex: 'masterCity',			width: 130,		hidden: true},
			{ text: 'Улица',					dataIndex: 'masterStreet',			width: 120,		hidden: true},
			{ text: 'Дом',						dataIndex: 'masterBuilding',		width: 80 ,		hidden: true},
			{ text: 'Полный адрес',				dataIndex: 'masterFullAddress',		width: 150 },
			{ text: 'Адрес',					dataIndex: 'masterAddress',			width: 140,		hidden: true},
			{ text: 'Дивизион',					dataIndex: 'masterDivision',		width: 100,		hidden: true},
			{ text: 'Участок',					dataIndex: 'masterArea',			width: 100,		hidden: true},
			{ text: 'Сектор',					dataIndex: 'masterSector',			width: 100,		hidden: true},			
			{ text: 'Канал',					dataIndex: 'masterChannel',			width: 100,		hidden: true},
			{ text: 'Подканал',					dataIndex: 'masterSubChannel',		width: 100,		hidden: true},
			{ text: 'Формат',					dataIndex: 'masterFormat',			width: 100,		hidden: true},
			{ text: 'Сеть',						dataIndex: 'masterisNetName',		width: 100,		hidden: true},
			{ text: 'Название сети',			dataIndex: 'masterNet',				width: 120,		hidden: true},
			{ text: 'Тип сети',					dataIndex: 'masterNetType',			width: 110,		hidden: true},
			{ text: 'Отв. менеджер',			dataIndex: 'masterRespManager',		width: 180,		hidden: true},
			{ text: 'КАИ Nielsen',				dataIndex: 'masterisNielsen',		width: 110,		hidden: true},
			{ text: 'Группа контрагентов',		dataIndex: 'masterContractorGroup',	width: 220,		hidden: true},
			{ text: 'Субдистрибьютор',			dataIndex: 'masterisSubDistributor',width: 220,		hidden: true},
			{ text: 'Дата открытия ТТ',			dataIndex: 'masterDateOpen',		width: 160,		hidden: true},
			{ text: 'Дата закрытия ТТ',			dataIndex: 'masterDateClose',		width: 160,		hidden: true},
			{ text: 'Площадь ТТ',				dataIndex: 'masterShopArea',		width: 120,		hidden: true},
			{ text: 'Координаты',				dataIndex: 'masterCoordinates',		width: 120,		hidden: true},
			{ text: 'GLN',						dataIndex: 'masterGLN',				width: 100,		hidden: true},
			{ text: 'Комментарий',				dataIndex: 'masterComment',			width: 120,		hidden: true},
			{ text: 'Страна',					dataIndex: 'masterCountry',			width: 120,		hidden: true},
			{ text: 'ФИАС',						dataIndex: 'masterFIAS',			width: 100,		hidden: true},
			{ text: 'Участвует в РБП',			dataIndex: 'masterisInRBP',			width: 150,		hidden: true},
			{ text: 'Отв. мерчандайзер',		dataIndex: 'masterRespMerch',		width: 150,		hidden: true},
			{ text: 'Отв. ТП',					dataIndex: 'masterRespTR',			width: 170,		hidden: true},
			{ text: 'Учитывать в расчетах',		dataIndex: 'masterisAccounting',    width: 170,		hidden: true},
			{ text: 'Причина неучета',			dataIndex: 'masterNonAccReason',    width: 170,		hidden: true},
			{ text: 'Выборка',					dataIndex: 'masterSelection',		width: 170,		hidden: true},
			{ text: 'Автор изменений',			dataIndex: 'masterAuthor',			width: 170,		hidden: true},
			{ text: 'Дата последнего изменения',dataIndex: 'masterDateChange',   	width: 200,		hidden: true},
			{ text: 'Источник данных',			dataIndex: 'masterDataSource',  	width: 170,		hidden: true},

			{xtype : 'checkcolumn', text : 'Привязать', dataIndex : 'toSync', cls: 'greycell'},
			
			{ text: 'ID',                       dataIndex: 'slaveID',				width: 80  },
			{ text: 'Код Дистр.',		        dataIndex: 'slaveDistrCode',		width: 130 },
			{ text: 'Код',			    		dataIndex: 'slaveCode',				width: 130 },
			{ text: 'Название',		    		dataIndex: 'slaveName',				width: 200 },
			{ text: 'Черновой адрес',        	dataIndex: 'slaveTempAddress',      width: 180 },
			{ text: 'Округ',	    	   		dataIndex: 'slaveCounty',			width: 110,      hidden: true},
			{ text: 'Регион',		      		dataIndex: 'slaveRegion',			width: 110,      hidden: true},
			{ text: 'Город',		       		dataIndex: 'slaveCity',				width: 130 },
			{ text: 'Дивизион',		       		dataIndex: 'slaveDivision',			width: 100,      hidden: true},
			{ text: 'Участок',		     		dataIndex: 'slaveSector',			width: 100 },
			{ text: 'Подканал',		       		dataIndex: 'slaveSubChannel',		width: 100,      hidden: true},
			{ text: 'Формат',			     	dataIndex: 'slaveFormat',			width: 100,      hidden: true},
			{ text: 'Сеть',				      	dataIndex: 'slaveisNet',			width: 100 },
			{ text: 'Название сети',	    	dataIndex: 'slaveNet',              width: 120 },
			{ text: 'Группа контрагентов',   	dataIndex: 'slaveContractorGroup',  width: 180 },
			{ text: 'Источник',		    		dataIndex: 'slaveDataSource',		width: 180 },
			{ text: 'Дата импорта',	     		dataIndex: 'slaveDateChange',		width: 125 }
		];

		var i = 0,
			length = columns.length;

		for (;i<length;i++) {
			columns[i].items = [{
				xtype: 'container',
				margin: 4,
				flex: 1,
				layout: 'fit',
				listeners: {
					scope: this,
					element: 'el',
					mousedown: function(e) {
						e.stopPropagation();
					},
					click: function(e) {
						e.stopPropagation();
					},
					keydown: function(e) {
						e.stopPropagation();
					},
					keypress: function(e) {
						e.stopPropagation();
					},
					keyup: function(e) {
						e.stopPropagation();
					}
				},
				items:[{
					xtype: 'textfield',
					emptyText : 'Поиск',
					enableKeyEvents: true,
					onTriggerClick: function () {
						this.reset();
						this.focus();
					},
					listeners: {
						change: function(field, newValue, oldValue, eOpts) {
								me.fireEvent('searchchange', me.down('grid'), field, newValue, oldValue);
						},
						buffer: 1000
					}
				}]
			}];
		}

		me.items = [{
			xtype: 'container',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			height: 250,
			items:[{
				xtype: 'itemselector',
				name: 'masterGroupSyncItemselector',
				id: 'masterGroupSyncItemselector-id',
				flex: 1,
				fieldLabel: 'Мастер справочник',
				labelAlign: 'top',
				store: Ext.getStore('TestOrimi.store.MasterStoresFieldsStore'),
				displayField: 'text',
				valueField: 'value',
				margin: '10 15 10 15',
				allowBlank: false,
				buttons: ['up', 'add', 'remove', 'down'],
				buttonsText: {up: "Переместить выше", add: "Добавить к сопоставлению", remove: "Убрать из сопоставления", down: "Переместить ниже"}
			},{
				xtype: 'itemselector',
				name: 'slaveGroupSyncItemselector',
				id: 'slaveGroupSyncItemselector-id',
				flex: 1,
				fieldLabel: 'Подчиненный справочник',
				labelAlign: 'top',
				store: Ext.getStore('TestOrimi.store.SlaveStoresFieldsStore'),
				displayField: 'text',
				valueField: 'value',
				value: ['Code','DistrCode','Name','TempAddress','County','Region','City','Division','Sector','SubChannel','Format','isNet','Net','ContractorGroup','DataSource'],
				margin: '10 15 10 15',
				allowBlank: false,
				buttons: ['up', 'remove', 'add', 'down'],
				buttonsText: {up: "Переместить выше", add: "Добавить к сопоставлению", remove: "Убрать из сопоставления", down: "Переместить ниже"}
			}]
		},{
			xtype: 'container',
			layout: 'hbox',
			height: 35,
			items:[{
				xtype: 'combobox',
				name: 'sourceCB',
				fieldLabel: 'Источник данных',
				editable: false,
				labelWidth: 150,
				width: 300,
				store: 'TestOrimi.store.DataSourceStore',
				queryMode: 'local',
				displayField: 'text',
				valueField: 'value',
				margin: '0 20 0 20',
				value: 0
			},{
				xtype: 'button',
				text: 'Сопоставить',
				handler: function() {
					var masterSelector = me.down('[name=masterGroupSyncItemselector]'),
						slaveSelector = me.down('[name=slaveGroupSyncItemselector]'),
						sourceSelector = me.down('[name=sourceCB]'),
						masterValue = masterSelector.getSubmitValue(),
						masterDataLength = masterValue.split(',').length,
						slaveData = slaveSelector.fromField.store.data,
						slaveDataLength = slaveData.length,
						slaveValue = [];
					
					if (masterDataLength && slaveDataLength && masterDataLength == slaveDataLength) {
						var mask = new Ext.LoadMask(me, {msg:"Загрузка..."});
						
						mask.show();										
						for(var i=0;i<slaveDataLength;i++) {
							slaveValue.push(slaveData.items[i].get('value'));
						}

						Ext.getStore('TestOrimi.store.StoresSyncResultStore').load({
							params: {
								master: masterValue,
								slave: slaveValue.toString(),
								source: sourceSelector.getValue()
							},
							callback: function(records, operation, success) {
								me.down('grid').down('toolbar').down('tbtext').updateInfo();
								mask.destroy();
							}
						});
					} else {
						TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Не выбраны поля для сопоставления или их количество не совпадает', 'winError');
					}
				}
			}]
		},{		
			xtype: 'grid',
			flex: 1,
			border: false,
			plugins: [{
				ptype: 'bufferedrenderer',
				trailingBufferZone: 0,
				leadingBufferZone: 0
			}],
			viewConfig: {
				loadMask: false
			},
			columns: columns,
			store: resultStore,
			bbar: {
				xtype: 'toolbar',
				items:[{
					xtype: 'button',
					text: 'Выбрать все',
					handler: function() {
						Ext.getStore('TestOrimi.store.StoresSyncResultStore').each( function(record) { record.set('toSync',true); });
					}
				},{
					xtype: 'button',
					text: 'Снять все',
					handler: function() {
						Ext.getStore('TestOrimi.store.StoresSyncResultStore').each( function(record) { record.set('toSync',false); });
					}
				},'-',{
					xtype: 'button',
					text: 'Выгрузить в CSV',
					handler: function() {
						var myMask = new Ext.LoadMask(me, {msg:"Формируется файл..."}),
							columns = me.down('grid').view.headerCt.getVisibleGridColumns(),
							colArr = [];

						for (var i = 0; i < columns.length; i++) {
							if (columns[i]['dataIndex'])
								colArr.push(columns[i]['dataIndex']);						
						}
					
						myMask.show();
						Ext.Ajax.request({
							url: 'resources/data/api.php',
							timeout: 600000,
							params: {
								act: 'getSyncResultReport',
								ref: 'Stores',
								columns: colArr.toString()
							},
							success: function(response){
								var response = Ext.JSON.decode(response.responseText),
									hiddenIFrameID = 'hiddenDownloader',
									iframe = document.getElementById(hiddenIFrameID);

								if (iframe === null) {
									iframe = document.createElement('iframe');
									iframe.id = hiddenIFrameID;
									iframe.style.display = 'none';
									document.body.appendChild(iframe);
								}
								iframe.src = 'resources/data/api.php?act=getFile&file='+response['name'];
								
								myMask.destroy();
							}
						});
					}
				},'->',{
					xtype: 'button',
					text: 'Привязать выбранные',
					handler: function() {
						var selected = [],
							gsrStore = Ext.getStore('TestOrimi.store.StoresSyncResultStore');
						
						gsrStore.each( function(record) { 
							if (record.get('toSync')) {
								selected.push({masterID: record.get('masterID'), slaveID: record.get('slaveID'), isNet: record.get('masterisNet')});
							}															
						});

						if (selected.length) {
							Ext.Msg.confirm('Привязать', 'Привязать позиции?',
								function(btn, text) {
									if (btn == 'yes') {
										var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Обработка..."});								
										
										myMask.show();
										Ext.Ajax.request({
											url: 'resources/data/api.php',
											params: {
												data: Ext.JSON.encode(selected),
												act: 'getSlaveStores',
												subaction: 'groupSyncWindow'
											},
											success: function(response) {																	
												var slaveIDs = [],
													length = selected.length,
													toRemove = [];
												
												for (var i=0;i<length;i++) {																		
													if (Ext.Array.indexOf(slaveIDs, selected[i]['slaveID']) === -1) {
														slaveIDs.push(selected[i]['slaveID']);
													}
												}
												
												length = slaveIDs.length;
												for (var i=0;i<length;i++) {
													gsrStore.remove(gsrStore.query('slaveID',slaveIDs[i],false,false,true).items);
												}
												me.down('grid').down('toolbar').down('tbtext').updateInfo();
												myMask.destroy();
												TestOrimi.getApplication().getController('Main').showNotification('Успешно! Позиции привязаны', 'winSuccess');																											
											},
											failure: function(form, action) {
												myMask.destroy();
												TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сервер не отвечает на запрос', 'winError');
											}
										});
									}
								}
							);
						} else {
							TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Выберите позиции для привязки', 'winError');
						}
					}
				},{
					xtype: 'tbtext',
					text: (Ext.getStore('TestOrimi.store.StoresSyncResultStore').getCount() == 0) ? "Нет записей" : Ext.getStore('TestOrimi.store.StoresSyncResultStore').getCount(),
					updateInfo: function(){
						var numberOfRecords = Ext.getStore('TestOrimi.store.StoresSyncResultStore').getCount();
						if (numberOfRecords == 0)
							this.setText("Нет записей");
						else
							this.setText("Количество записей: "+numberOfRecords);
					}
				}]
			},
			listeners: {
				celldblclick: function(view, td, cellIndex, record, tr, rowIndex) {
					me.fireEvent('celldblclick', view, td, cellIndex, record, tr, rowIndex);
				},
				columnshow: function(ct, column) { 
					me.fireEvent('columnshow', ct, column);
				}
			}
		}];		

		me.buttons = [{
		text: 'Отмена',
			handler: function() {
				this.up('window').destroy();
			}
		}];

		this.callParent(arguments);
	}
});
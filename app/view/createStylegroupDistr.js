Ext.define('TestOrimi.view.createStylegroupDistr', {
    extend: 'Ext.window.Window',
    width: 500,
    height: 600,
    title: 'Групповая установка свойств',
    modal: true,
    layout: 'fit',

    initComponent: function () {
        var me = this,
            userRights = TestOrimi.getApplication().data;

        var myListener = {
            change: function (field, newValue, oldValue) {
                var toField;

                if (newValue) {
                    toField = field.up('window').down('[name=' + field.name.substring(8) + ']');

                    toField.enable();
                } else {
                    toField = field.up('window').down('[name=' + field.name.substring(8) + ']');
                    //toField.reset();
                    toField.disable();
                }
            }
        };

        this.items = [{
            xtype: 'form',
            padding: 10,
            autoScroll: true,
            border: false,
            layout: {
                type: 'column'
            },
            items: [{
                columnWidth: 0.05,
                defaults: {
                    xtype: 'checkboxfield',
                    listeners: myListener
                },
                items: [{
                    inputValue: 'Name',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxName'
                }, {
                    inputValue: 'PostCode',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxPostCode'
                }, {
                    inputValue: 'CityID',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxCityID'
                }, {
                    inputValue: 'Street',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxStreet'
                }, {
                    inputValue: 'Building',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxBuilding'
                }, {
                    inputValue: 'FIAS',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxFIAS'
                }, {
                    inputValue: 'ChannelID',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxChannelID'
                }, {
                    inputValue: 'SubChannelID',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxSubChannelID'
                }, {
                    inputValue: 'FormatID',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxFormatID'
                }, {
                    inputValue: 'ContractorGroupID',
                    hidden: (userRights['custom'] == 1),
                    name: 'checkboxContractorGroupID'
                }, {
                    inputValue: 'CurrancyID',
                    hidden: (userRights['custom'] == 1),
                    name: 'checkboxCurrancyID'
                }, {
                    inputValue: 'isSubDistributorGroup',
                    hidden: (userRights['custom'] == 1),
                    name: 'checkboxisSubDistributorGroup'
                }, {
                    inputValue: 'isAccountingGroup',
                    hidden: (userRights['custom'] == 1),
                    name: 'checkboxisAccountingGroup'
                }, {
                    inputValue: 'NonAccReasonID',
                    hidden: (userRights['custom'] == 1),
                    name: 'checkboxNonAccReasonID',
                    disabled: true
                }, {
                    inputValue: 'DateOpen',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxDateOpen'
                }, {
                    inputValue: 'DateClose',
                    name: 'checkboxDateClose'
                }, {
                    inputValue: 'ShopArea',
                    name: 'checkboxShopArea'
                }, {
                    inputValue: 'Coordinates',
                    name: 'checkboxCoordinates'
                }, {
                    inputValue: 'GLN',
                    name: 'checkboxGLN'
                }, {
                    inputValue: 'Comment',
                    name: 'checkboxComment',
                    margin: '0 0 44 0'
                }, {
                    inputValue: 'isInRBPGroup',
                    name: 'checkboxisInRBPGroup'
                }]

            }, {
                columnWidth: 0.95,
                name: 'fieldCont',
                defaults: {
                    anchor: '90%',
                    labelWidth: 180,
                    disabled: true
                },
                layout: {
                    type: 'vbox'
                    , align: 'stretch'
                },
                items: [{
                    fieldLabel: 'Название',
                    xtype: 'textfield',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'Name',
                    allowBlank: (!this.hasActiveStatus)
                }, {
                    fieldLabel: 'Индекс',
                    xtype: 'numberfield',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    minValue: 0,
                    name: 'PostCode'
                }, {
                    fieldLabel: 'Город',
                    name: 'CityID',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    attribute: 'City',
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.CityStore',
                    allowBlank: (!this.hasActiveStatus)
                }, {
                    fieldLabel: 'Улица',
                    xtype: 'textfield',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'Street',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowBlank: (!this.hasActiveStatus)
                }, {
                    fieldLabel: 'Дом',
                    xtype: 'textfield',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'Building',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowBlank: (!this.hasActiveStatus)
                }, {
                    fieldLabel: 'ФИАС',
                    xtype: 'textfield',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'FIAS'
                }, {
                    fieldLabel: 'Канал',
                    name: 'ChannelID',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.ChannelStore'
                }, {
                    fieldLabel: 'Подканал',
                    name: 'SubChannelID',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    attribute: 'SubChannel',
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.SubChannelStore',
                    allowBlank: (!this.hasActiveStatus)
                }, {
                    fieldLabel: 'Формат',
                    name: 'FormatID',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.FormatStore'
                }, {
                    fieldLabel: 'Группа контрагентов',
                    name: 'ContractorGroupID',
                    hidden: (userRights['custom'] == 1),
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.ContractorGroupStore'
                }, {
                    fieldLabel: 'Валюта',
                    name: 'CurrancyID',
                    hidden: (userRights['custom'] == 1),
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.CurrancyStore'
                }, {
                    fieldLabel: 'Сабдистрибьютор',
                    hidden: (userRights['custom'] == 1),
                    xtype: 'radiogroup',
                    anchor: 'none',
                    name: 'isSubDistributorGroup',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    layout: {
                        autoFlex: false
                    },
                    defaults: {
                        name: 'isSubDistributor',
                        margin: '0 15 0 0'
                    },
                    items: [{
                        boxLabel: 'Да',
                        inputValue: '1'
                    }, {
                        boxLabel: 'Нет',
                        inputValue: '0'
                    }]
                }, {
                    fieldLabel: 'Учитывать в расчетах',
                    name: 'isAccountingGroup', //for listeners
                    hidden: (userRights['custom'] == 1),
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    xtype: 'radiogroup',
                    anchor: 'none',
                    layout: {
                        autoFlex: false
                    },
                    defaults: {
                        name: 'isAccounting',
                        margin: '0 15 0 0'
                    },
                    items: [{
                        boxLabel: 'Да',
                        inputValue: '1'
                    }, {
                        boxLabel: 'Нет',
                        inputValue: '0'
                    }],
                    listeners: {
                        change: function (radio, inputValue, oldValue) {
                            var panel = radio.up('form'),
                                changeInput = inputValue.isAccounting;

                            if (changeInput == 0) {
                                panel.down('[name=checkboxNonAccReasonID]').enable();
                            } else {
                                panel.down('[name=checkboxNonAccReasonID]').setValue(0);
                                panel.down('[name=checkboxNonAccReasonID]').disable();
                            }
                        },
                        disable: function (radio) {
                            var panel = radio.up('form');

                            panel.down('[name=checkboxNonAccReasonID]').setValue(0);
                            panel.down('[name=checkboxNonAccReasonID]').disable();
                        }
                    }
                }, {
                    fieldLabel: 'Причина неучета',
                    name: 'NonAccReasonID',
                    hidden: (userRights['custom'] == 1),
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.NonAccReasonStore',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowBlank: false
                }, {
                    xtype: 'datefield',
                    fieldLabel: 'Дата открытия ТТ',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    format: 'Y-m-d',
                    name: 'DateOpen',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowBlank: (!this.hasActiveStatus)
                }, {
                    xtype: 'datefield',
                    fieldLabel: 'Дата закрытия ТТ',
                    format: 'Y-m-d',
                    name: 'DateClose'
                }, {
                    fieldLabel: 'Площадь ТТ',
                    xtype: 'numberfield',
                    minValue: 0,
                    name: 'ShopArea'
                }, {
                    fieldLabel: 'Координаты',
                    xtype: 'textfield',
                    name: 'Coordinates'
                }, {
                    fieldLabel: 'GLN',
                    xtype: 'textfield',
                    name: 'GLN'
                }, {
                    xtype: 'textareafield',
                    grow: true,
                    name: 'Comment',
                    fieldLabel: 'Комментарий'
                }, {
                    fieldLabel: 'Участвует в РБП',
                    xtype: 'radiogroup',
                    anchor: 'none',
                    name: 'isInRBPGroup',
                    layout: {
                        autoFlex: false
                    },
                    defaults: {
                        name: 'isInRBP',
                        margin: '0 15 0 0'
                    },
                    items: [{
                        boxLabel: 'Да',
                        inputValue: '1'
                    }, {
                        boxLabel: 'Нет',
                        inputValue: '0'
                    }]
                }]
            }]
        }];

        this.buttons = [{
            text: 'Ок',
            handler: function () {
                var store = Ext.getStore('TestOrimi.store.MasterDistrStore'),
                    win = this.up('window'),
                    form = win.down('form').getForm();

                //get cleared form values
                var rawVals = form.getValues(),
                    radiogroups = ['isInRBP','isSubDistributor','isAccounting'],
                    params = {};

                Ext.Object.each(rawVals, function (key, value, myself) {
                    params[key] = value;
                });
                //

                params['ID'] = win.records.toString();
                params['act'] = store.proxy.extraParams['act'];
                params['subaction'] = 'groupUpdate';

                if (form.isValid()) {
                    var myMask = new Ext.LoadMask(win, {msg: "Сохраняю..."});

                    myMask.show();
                    Ext.Ajax.request({
                        url: 'resources/data/api.php',
                        params: params,
                        success: function (response) {
                            myMask.destroy();
                            TestOrimi.getApplication().getController('Main').showNotification('Успешно! Элементы обновлены', 'winSuccess');
                            store.load();
                            win.destroy();
                        },
                        failure: function (form, action) {
                            myMask.destroy();
                            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Нет ответа от сервера', 'winError');
                        }
                    });
                }
            }
        }, {
            text: 'Отмена',
            handler: function () {
                this.up('window').destroy();
            }
        }];

        this.callParent(arguments);
    }
});
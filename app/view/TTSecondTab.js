Ext.define('TestOrimi.view.TTSecondTab', {
    extend: 'Ext.container.Container',
    xtype: 'ttSecondTab',
    border: false,
    margin: '20 20 0 20',
    layout: 'column',

    initComponent: function () {
        var me = this,
            userRights = TestOrimi.getApplication().data;

        me.items = [{
            xtype: 'fieldset',
            defaultType: 'textfield',
            columnWidth: 0.5,
            margin: '20 0 0 0',
            defaults: {anchor: '100%', labelWidth: 160},
            border: false,
            items: [{
                fieldLabel: 'Сеть',
                xtype: 'radiogroup',
                name: 'isNetRadioGroup',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                anchor: 'none',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    name: 'isNet',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'Нет',
                    inputValue: '0',
                    checked: true
                }, {
                    boxLabel: 'Да',
                    inputValue: '1'
                }],
                listeners: {
                    change: function (radio, inputValue, oldValue) {
                        var panel = radio.up('ttSecondTab'),
                            changeInput = inputValue.isNet;

                        if (panel.down('[name=isCons]').getValue()['isConsignee'] == 1) {
                            panel.down('[name=ContractorGroupID]').allowBlank = false;
                            panel.down('[name=ContractorGroupID]').enable();
                        } else {
                            panel.down('[name=ContractorGroupID]').allowBlank = true;
                            panel.down('[name=ContractorGroupID]').disable();
                        }

                        if (changeInput == 1) {
                            panel.down('[name=NetTypeID]').enable();
                            panel.down('[name=NetID]').enable();
                            panel.down('[name=NetID]').allowBlank = false;
                            panel.down('[name=isNiels]').enable();
                            panel.down('[name=RespManagerID]').enable();
                        } else {
                            panel.down('[name=NetTypeID]').disable();
                            panel.down('[name=NetID]').disable();
                            panel.down('[name=NetID]').allowBlank = true;
                            panel.down('[name=isNiels]').disable();
                            panel.down('[name=RespManagerID]').disable();
                        }
                    }
                }
            }, {
                fieldLabel: 'Грузополучатель ОРИМИ',
                xtype: 'radiogroup',
                name: 'isCons',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                anchor: 'none',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    name: 'isConsignee',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'Да',
                    inputValue: '1'
                }, {
                    boxLabel: 'Нет',
                    inputValue: '0',
                    checked: true
                }],
                listeners: {
                    change: function (radio, inputValue, oldValue) {
                        var panel = radio.up('ttSecondTab'),
                            changeInput = inputValue.isConsignee;

                        if (changeInput == 1) {
                            panel.down('[name=ContractorGroupID]').allowBlank = false;
                            panel.down('[name=ContractorGroupID]').enable();
                        } else {
                            panel.down('[name=ContractorGroupID]').allowBlank = true;
                            panel.down('[name=ContractorGroupID]').disable();
                        }
                    }
                }
            }, {
                fieldLabel: 'Группа контрагентов',
                name: 'ContractorGroupID',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                xtype: 'modalcombo',
                store: 'TestOrimi.store.ContractorGroupStore',
                disabled: true
            }, {
                fieldLabel: 'Участвует в РБП',
                xtype: 'radiogroup',
                name: 'isRBP',
                anchor: 'none',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    name: 'isInRBP',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'Да',
                    inputValue: '1'
                }, {
                    boxLabel: 'Нет',
                    inputValue: '0',
                    checked: true
                }]
            }, {
                fieldLabel: 'GLN',
                name: 'GLN',
                margin: '152 0 5 0'
            }, {
                cls: 'noteditable',
                fieldLabel: 'Выборка',
                name: 'Selection',
                readOnly: true
            }]
        }, {
            xtype: 'fieldset',
            defaultType: 'textfield',
            columnWidth: 0.5,
            margin: '20 0 0 0',
            defaults: {anchor: '100%', labelWidth: 160},
            border: false,
            items: [{
                fieldLabel: 'Название сети',
                name: 'NetID',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                attribute: 'Net',
                xtype: 'modalcombo',
                disabled: true,
                store: 'TestOrimi.store.NetStore'
            }, {
                fieldLabel: 'Тип сети',
                name: 'NetTypeID',
                disabled: true,
                readOnly: true,
                //displayField: 'Name',
                cls: 'noteditable',
                xtype: 'modalcombo',
                store: 'TestOrimi.store.NetTypeStore'
            }, {
                fieldLabel: 'КАИ Nielsen',
                xtype: 'radiogroup',
                cls: 'noteditable',
                anchor: 'none',
                disabled: true,
                name: 'isNiels',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    readOnly: true,
                    name: 'isNielsen',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'Да',
                    inputValue: '1'
                }, {
                    boxLabel: 'Нет',
                    inputValue: '0'
                }]
            }, {
                fieldLabel: 'Отв. менеджер',
                name: 'RespManagerID',
                displayField: 'FIO',
                disabled: true,
                readOnly: true,
                cls: 'noteditable',
                attribute: 'ParentStaff',
                xtype: 'modalcombo',
                store: 'TestOrimi.store.ParentStore'
            }, {
                fieldLabel: 'Отв. ТП',
                name: 'RespTRID',
                attribute: 'ParentStaff',
                displayField: 'FIO',
                xtype: 'modalcombo',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                store: 'TestOrimi.store.ParentStore'
            }, {
                fieldLabel: 'Отв. мерчендайзер',
                name: 'RespMerchID',
                displayField: 'FIO',
                attribute: 'ParentStaff',
                xtype: 'modalcombo',
                store: 'TestOrimi.store.ParentStore'
            }, {
                xtype: 'textareafield',
                grow: true,
                name: 'Comment',
                fieldLabel: 'Комментарий',
                margin: '108 0 5 0'
            }]
        }];

        this.callParent(arguments);
    }
});
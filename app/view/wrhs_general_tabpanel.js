Ext.define('TestOrimi.view.wrhs_general_tabpanel', {
    extend: 'Ext.tab.Panel',
    xtype: 'wrhs_general_tabpanel',
    flex: 1,
    ref: 'Wrhs',
    split: true,
    collapsible: true,
    header: false,
    plain: true,
    title: 'Подчиненные справочники',

    initComponent: function () {
        this.items = [
            {xtype: 'wrhs_general_tabpanelFirstcol', title: 'Отвязать'},
            {xtype: 'wrhs_general_tabpanelSecondcol', title: 'Привязать'}
        ];
        this.callParent(arguments);
    }
});
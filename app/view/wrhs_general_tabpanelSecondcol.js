Ext.define('TestOrimi.view.wrhs_general_tabpanelSecondcol', {
    extend: 'Ext.grid.Panel',
    xtype: 'wrhs_general_tabpanelSecondcol',
    selType: 'checkboxmodel',
    columnLines: true,

    initComponent: function () {
        var me = this,
            userRights = TestOrimi.getApplication().data;

        this.store = 'TestOrimi.store.SlaveWarehousesStore';

        this.columns = [
            {text: 'Источник', dataIndex: 'DataSource', width: 180},
            {text: 'Дата импорта', dataIndex: 'DateChange', width: 125},
            {text: 'ID', dataIndex: 'ID', width: 80},
            {text: 'Код Дистр.', dataIndex: 'DistrCode', width: 130},
            {text: 'Код', dataIndex: 'Code', width: 130},
            {text: 'Название', dataIndex: 'Name', width: 200},
            {text: 'Черновой адрес', dataIndex: 'TempAddress', width: 180},
            {text: 'Округ', dataIndex: 'County', width: 110},
            {text: 'Регион', dataIndex: 'Region', width: 110},
            {text: 'Город', dataIndex: 'City', width: 130},
            {text: 'Дивизион', dataIndex: 'Division', width: 100},
            {text: 'Участок', dataIndex: 'Sector', width: 100}

        ];

        if (userRights['Wrhs_slave_sync']) {
            Ext.Array.insert(me.columns, 0, [{
                text: 'Привязать',
                menuDisabled: true,
                menuText: 'Привязать',
                sortable: false,
                xtype: 'actioncolumn',
                width: 100,
                items: [{
                    iconCls: 'sync-col',
                    handler: function (view, rowIndex, colIndex) {
                        me.fireEvent('syncbuttonclick', me, rowIndex, colIndex);
                    }
                }]
            }]);
        }

        var i = 1,
            columns = this.columns,
            length = columns.length;

        for (; i < length; i++) {
            columns[i].items = [{
                xtype: 'container',
                margin: 4,
                flex: 1,
                layout: 'fit',
                listeners: {
                    scope: this,
                    element: 'el',
                    mousedown: function (e) {
                        e.stopPropagation();
                    },
                    click: function (e) {
                        e.stopPropagation();
                    },
                    keydown: function (e) {
                        e.stopPropagation();
                    },
                    keypress: function (e) {
                        e.stopPropagation();
                    },
                    keyup: function (e) {
                        e.stopPropagation();
                    }
                },
                items: [{
                    xtype: 'textfield',
                    emptyText: 'Поиск',
                    enableKeyEvents: true,
                    onTriggerClick: function () {
                        this.reset();
                        this.focus();
                    },
                    listeners: {
                        change: function (field, newValue, oldValue, eOpts) {
                            me.fireEvent('searchchange', me, field, newValue, oldValue);
                        },
                        buffer: 1000
                    }
                }]
            }];
        }

        this.lbar = [{
            xtype: 'general_tabpanelsecLbar',
            ref: 'Wrhs',
            grid: this
        }];

        this.bbar = {
            xtype: 'pagingtoolbar',
            store: this.store,
            inputItemWidth: 60,
            displayInfo: true
        };

        this.callParent(arguments);
    }
});

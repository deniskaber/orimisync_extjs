Ext.define('TestOrimi.view.general_tabpanel', {
	extend	: 'Ext.tab.Panel',
	xtype 	: 'general_tabpanel',
	flex	: 1,
	ref		: 'Sku',
	split	: true,
	collapsible: true,
	header: false,
	plain	: true,
	title: 'Подчиненные справочники',

	initComponent: function() {
		this.items = [
			{ xtype: 'general_tabpanelFirstcol', 	title: 'Отвязать' },
			{ xtype: 'general_tabpanelSecondcol', 	title: 'Привязать' }
		];
        this.callParent(arguments);
	}
});

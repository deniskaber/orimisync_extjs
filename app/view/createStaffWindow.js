Ext.define('TestOrimi.view.createStaffWindow', {
    extend: 'Ext.window.Window',
    xtype: 'createStaffWindow',
    width: 900,
    height: 640,
    modal: true,
    title: 'Сотрудники',
    layout: 'fit',
    buttons: [{
        text: 'Ок',
        name: 'okButton',
        handler: function () {
            var store = Ext.getStore('TestOrimi.store.MasterStaffStore'),
                win = this.up('window'),
                form = win.down('form').getForm(),
                formdown = win.down('form'),
                sectorRecords = formdown.down('[name=sector]').store.data.items,
                lengthAdd = sectorRecords.length,
                i = 0,
                sectors = [],
                areaRecords = formdown.down('[name=area]').store.data.items,
                lengthAddArea = areaRecords.length,
                areas = [];

            for (; i < lengthAdd; i++) {
                sectors.push(sectorRecords[i].get('ID'));
            }

            i = 0;

            for (; i < lengthAddArea; i++) {
                areas.push(areaRecords[i].get('ID'));
            }

            //console.dir(sectors);

            if (form.isValid()) {
                var myMask = new Ext.LoadMask(win, {msg: "Сохраняю..."});

                myMask.show();
                form.submit({
                    url: 'resources/data/api.php',
                    params: {
                        act: store.proxy.extraParams['act'],
                        subaction: (win.data) ? (win.data.get('ID') ? 'update' : 'create') : 'create',
                        DataSourceID: (win.data) ? win.data.get('DataSourceID') : 0,
                        Sector: sectors.toString(),
                        Area: areas.toString()
                    },
                    success: function (response) {
                        myMask.destroy(); //TODO new if()
                        if (win.data) {
                            if (win.data.get('ID') == 0) {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess');
                            } else {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Элемент обновлен', 'winSuccess');
                            }
                        } else {
                            TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess');
                        }
                        store.load();
                        win.destroy();
                    },
                    failure: function (form, action) {
                        myMask.destroy();
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                    }
                });
            }
        }
    }, {
        text: 'Отмена',
        handler: function () {
            this.up('window').destroy();
        }
    }],

    listeners: {
        beforedestroy: function (win) {
            Ext.getStore('TestOrimi.store.SelectedSectorStore').removeAll();
            Ext.getStore('TestOrimi.store.SelectedAreaStore').removeAll();
        }
    },

    initComponent: function () {
        var me = this,
            userRights = TestOrimi.getApplication().data,
            statusID = 1;

        if (this.data) {
            if (this.data.get('StatusID'))
                statusID = this.data.get('StatusID');
        }

        me.items = [{
            xtype: 'form',
            bodyPadding: '30 15 0 15',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [{
                xtype: 'container',
                border: false,
                layout: 'column',
                items: [{
                    xtype: 'fieldset',
                    columnWidth: 0.5,
                    defaults: {
                        xtype: 'textfield'
                        , anchor: '100%'
                        , labelWidth: 160
                    },
                    border: false,
                    items: [{
                        cls: 'noteditable',
                        name: 'ID',
                        fieldLabel: 'ID',
                        readOnly: true
                    }, {
                        cls: 'noteditable',
                        xtype: 'textareafield',
                        name: 'FIO',
                        fieldLabel: 'ФИО',
                        readOnly: true,
                        height: 50
                    }, {
                        fieldLabel: 'Фамилия',
                        name: 'SurName',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                        margin: '29 0 5 0'
                    }, {
                        fieldLabel: 'Отчество',
                        name: 'LastName'
                    }, {
                        fieldLabel: 'E-Mail',
                        name: 'EMail',
                        vtype: 'email'
                    }, {
                        fieldLabel: 'Дата начала работы',
                        name: 'DateHired',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                        xtype: 'datefield',
                        format: 'Y-m-d',
                        margin: '29 0 5 0'
                    }, {
                        fieldLabel: 'Тип',
                        name: 'TypeID',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                        xtype: 'modalcombo',
                        store: 'TestOrimi.store.TypeStaffStore'
                    }, {
                        fieldLabel: 'Дивизион',
                        name: 'DivisionID',
                        attribute: 'Division',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                        xtype: 'modalcombo',
                        store: 'TestOrimi.store.DivisionStore'
                    }, {
                        xtype: 'areaPanel',
                        req: true
                    }, {
                        xtype: 'sectorPanel',
                        req: true
                    }]
                }, {
                    columnWidth: 0.5,
                    xtype: 'fieldset',
                    defaults: {
                        xtype: 'textfield'
                        , anchor: '100%'
                        , labelWidth: 160
                    },
                    border: false,
                    items: [{
                        cls: (statusID != 1) ? 'noteditable' : '',
                        fieldLabel: 'Код',
                        name: 'Code',
                        readOnly: (statusID != 1) ? true : false
                    }, {
                        fieldLabel: 'Кому подчиняется',
                        name: 'ParentID',
                        attribute: 'ParentStaff',
                        displayField: 'FIO',
                        xtype: 'modalcombo',
                        store: 'TestOrimi.store.ParentStore'
                    }, {
                        fieldLabel: 'Имя',
                        name: 'Name',
                        margin: '58 0 5 0'
                    }, {
                        fieldLabel: 'Дата рождения',
                        name: 'BirthDate',
                        xtype: 'datefield',
                        format: 'Y-m-d'
                    }, {
                        fieldLabel: 'Телефон',
                        name: 'Phone'
                    }, {
                        fieldLabel: 'Дата увольнения',
                        name: 'DateFired',
                        xtype: 'datefield',
                        format: 'Y-m-d',
                        margin: '29 0 5 0'
                    }, {
                        fieldLabel: 'Мотивация',
                        name: 'MotivationID',
                        attribute: 'Motivation',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                        xtype: 'modalcombo',
                        store: 'TestOrimi.store.MotivationStore'
                    }, {
                        fieldLabel: 'Признак',
                        xtype: 'radiogroup',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                        anchor: 'none',
                        layout: {
                            autoFlex: false
                        },
                        defaults: {
                            name: 'Feature',
                            margin: '0 15 0 0'
                        },
                        items: [{
                            boxLabel: 'белый',
                            inputValue: 1
                        }, {
                            boxLabel: 'серый',
                            inputValue: 2
                        }]
                    }, {
                        fieldLabel: 'Учитывать в команде',
                        name: 'ConsiderInTeamGroup',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                        disabled: true,
                        xtype: 'radiogroup',
                        anchor: 'none',
                        layout: {
                            autoFlex: false
                        },
                        defaults: {
                            name: 'ConsiderInTeam',
                            margin: '0 15 0 0'
                        },
                        items: [{
                            boxLabel: 'да',
                            inputValue: 1
                        }, {
                            boxLabel: 'нет',
                            inputValue: 0,
                            margin: '0 15 0 22'
                        }]
                    }, {
                        xtype: 'textareafield',
                        name: 'Comment',
                        fieldLabel: 'Комментарий',
                        height: 70
                    }]
                }]
            }]
        }];

        this.callParent(arguments);

        if (this.data) {
            this.down('form').loadRecord(this.data);

            //--
            var sectorStore = Ext.getStore('TestOrimi.store.SectorStore'),
                sectorIDs = this.data.get('SectorID') ? this.data.get('SectorID').trim() : '';

            if (sectorIDs.length) {
                var sectorIDs = sectorIDs.split(','),
                    slength = sectorIDs.length,
                    sectorsData = [];

                for (i = 0; i < slength; i++) {
                    var foundSectorIndex = sectorStore.findExact('ID', parseInt(sectorIDs[i]));
                    if (foundSectorIndex >= 0) {
                        var foundSector = sectorStore.getAt(foundSectorIndex);
                        sectorsData.push({
                            ID: foundSector.get('ID'),
                            Name: foundSector.get('Name'),
                            AreaID: foundSector.get('AreaID')
                        });
                    }
                }
                Ext.getStore('TestOrimi.store.SelectedSectorStore').loadData(sectorsData);
            } else {
                Ext.getStore('TestOrimi.store.SelectedSectorStore').removeAll();
            }
            //--

            //--
            var areaStore = Ext.getStore('TestOrimi.store.AreaStore'),
                areaIDs = this.data.get('AreaID') ? this.data.get('AreaID').trim() : '';

            if (areaIDs.length) {
                var areaIDs = areaIDs.split(','),
                    alength = areaIDs.length,
                    areasData = [];

                for (i = 0; i < alength; i++) {
                    var foundAreaIndex = areaStore.findExact('ID', parseInt(areaIDs[i]));
                    if (foundAreaIndex >= 0) {
                        var foundArea = areaStore.getAt(foundAreaIndex);
                        areasData.push({
                            ID: foundArea.get('ID'),
                            Name: foundArea.get('Name'),
                            DivisionID: foundArea.get('DivisionID')
                        });
                    }
                }
                Ext.getStore('TestOrimi.store.SelectedAreaStore').loadData(areasData);
            } else {
                Ext.getStore('TestOrimi.store.SelectedAreaStore').removeAll();
            }
            //--
            
            if (this.data.get('ID') == 0) {
                this.down('form').down('[name=ID]').reset();
            }

            if (this.data.get('StatusID') == 3 || (this.data.get('StatusID') == 1 && !userRights['Staff_draft_set']) || (this.data.get('StatusID') == 2 && !userRights['Staff_active_set'])) { //удаленная позиция или редактирование отключено в правах
                var form = this.down('form');
                var fields = form.query('textfield');
                var mcombos = form.query('modalcombo');
                var radios = form.query('radio');
                var buttons = form.query('buttongroup');


                form.addCls('noteditable');

                //----//
                for (var i = 0; i < mcombos.length; i++) {
                    mcombos[i].readOnly = true;
                    mcombos[i].hideTrigger = true;
                }

                for (var i = 0; i < fields.length; i++) {
                    fields[i].readOnly = true;
                }

                for (var i = 0; i < radios.length; i++) {
                    radios[i].readOnly = true;
                }

                for (var i = 0; i < buttons.length; i++) {
                    buttons[i].disable();
                }
                //----//

                this.down('[name=okButton]').disable();
            }
        }
    }
});
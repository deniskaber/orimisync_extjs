Ext.define('TestOrimi.view.general_tabpanelCombobox', {
    extend          : 'Ext.form.field.ComboBox',
    xtype           : 'general_tabpanelCombobox',
    fieldLabel      : 'Все источники',
    displayField    : 'name',
    valueField      : 'point',
    value			: 'all',
	typeAhead       : true,
    forceSelection  : true,
    transform       : 'stateSelect',


    initComponent: function() {
        this.store = {
                fields: [
                {type: 'string',    name: 'point'},
                {type: 'string',    name: 'name'}
                ],
                data :  [
                            { point : 'all',    name: 'Все источники'},
                            { point : 'ist1',   name: 'Источник1'},
                            { point : 'ist2',   name: 'Хороший источник'},
                            { point : 'ist3',   name: 'Плохой источник'}
                ]
            }; 

            this.callParent(arguments);

    }
});
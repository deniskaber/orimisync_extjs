Ext.define('TestOrimi.view.createStylegroupTT', {
    extend: 'Ext.window.Window',
    width: 500,
    height: 600,
    title: 'Групповая установка свойств',
    modal: true,
    layout: 'fit',

    initComponent: function() {
        var me = this,
            userRights = TestOrimi.getApplication().data;

        var myListener = {
            change: function(field, newValue, oldValue) {
                var toField;

                if (newValue) {
                    toField = field.up('window').down('[name='+field.name.substring(8)+']');
                    
                    toField.enable();
                    if (field.name == 'checkboxisNetGroup') {
                        toField.fireEvent('change', toField, toField.getValue());
                    }
                } else {
                    toField = field.up('window').down('[name='+field.name.substring(8)+']');
                    //toField.reset();
                    toField.disable();
                }
            }
        };

        this.items = [{
    		xtype   : 'form',
            padding : 10,
            autoScroll: true,
            border  : false,
            layout  : {
                type: 'column' 
            },
            items: [{
            columnWidth: 0.05,
            defaults  : {
                xtype       : 'checkboxfield',
                listeners   : myListener
            },
    		items: [{
                inputValue  : 'Name',
                hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                name        : 'checkboxName'
            },{
                inputValue  : 'PostCode',
                hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                name        : 'checkboxPostCode'
            },{
                inputValue  : 'CityID',
                hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                name        : 'checkboxCityID'
            },{
                inputValue  : 'Street',
                hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                name        : 'checkboxStreet'
            },{
                inputValue  : 'Building',
                hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                name        : 'checkboxBuilding'
            },{
                inputValue  : 'FIAS',
                hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                name        : 'checkboxFIAS'
            },{
                inputValue  : 'SubChannelID',
                hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                name        : 'checkboxSubChannelID'
            },{
                inputValue  : 'AdvertisingChannelID',
                hidden      : (userRights['TT_AdvChannel_set'] !== 1),
                name        : 'checkboxAdvertisingChannelID'
            },{
                inputValue  : 'FormatID',
                hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                name        : 'checkboxFormatID'
            },{
                inputValue  : 'isNetGroup',
                hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                name        : 'checkboxisNetGroup'
            },{
                inputValue  : 'NetID',
                hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                name        : 'checkboxNetID',
                disabled    : true
            },{
                inputValue  : 'RespTRID',
                name        : 'checkboxRespTRID',
                disabled    : (userRights['custom'] == 1 ? false : true)
            },{
                inputValue  : 'RespMerchID',
                hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                name        : 'checkboxRespMerchID',
                disabled    : (userRights['custom'] == 1 ? false : true)
            },{
                inputValue  : 'isConsigneeGroup',
                hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                name        : 'checkboxisConsigneeGroup'
            },{
                inputValue  : 'ContractorGroupID',
                hidden      : (userRights['custom'] == 1),
                name        : 'checkboxContractorGroupID',
                disabled    : true
            },{
                inputValue  : 'DateOpen',
                hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                name        : 'checkboxDateOpen'
            },{
                inputValue  : 'DateClose',
                name        : 'checkboxDateClose'
            },{
                inputValue  : 'ShopArea',
                name        : 'checkboxShopArea'
            },{
                inputValue  : 'Coordinates',
                name        : 'checkboxCoordinates'
            },{
                inputValue  : 'GLN',
                name        : 'checkboxGLN'
            },{
                inputValue  : 'Comment',
                name        : 'checkboxComment',
                margin      : '0 0 44 0'
            },{
                inputValue  : 'isInRBPGroup',
                name        : 'checkboxisInRBPGroup'
            }]

        },{
            columnWidth: 0.95,
            name        : 'fieldCont',
            defaults  : {
                anchor: '90%', 
                labelWidth : 180,
                disabled : true   
            },
            layout  : {
                type: 'vbox' 
                ,align: 'stretch'
            },
            items: [{
        			fieldLabel  : 'Название',
                    xtype       : 'textfield',
                    hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                    name        : 'Name',
                    allowBlank  : (!this.hasActiveStatus)
            },{
                    fieldLabel  : 'Индекс',
                    xtype       : 'numberfield',
                    hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                    minValue    : 0,
                    name        : 'PostCode'
            },{
                    fieldLabel  : 'Город',
                    name        : 'CityID',
                    hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    attribute   : 'City',
                    xtype       : 'modalcombo',
                    store       : 'TestOrimi.store.CityStore',
                    allowBlank  : (!this.hasActiveStatus)
            },{
                    fieldLabel  : 'Улица',
                    xtype       : 'textfield',
                    hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                    name        : 'Street',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowBlank  : (!this.hasActiveStatus)
            },{
                    fieldLabel  : 'Дом',
                    xtype       : 'textfield',
                    hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                    name        : 'Building',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowBlank  : (!this.hasActiveStatus)
            },{
                    fieldLabel  : 'ФИАС',
                    xtype       : 'textfield',
                    hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                    name        : 'FIAS'
            },{
                    fieldLabel  : 'Подканал',
                    name        : 'SubChannelID',
                    hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    attribute   : 'SubChannel',
                    xtype       : 'modalcombo',
                    store       : 'TestOrimi.store.SubChannelStore',
                    allowBlank  : (!this.hasActiveStatus)
            },{
                    fieldLabel  : 'Канал',
                    name        : 'ChannelID',
                    hidden      : true, //needed to set AdvertisingChannelID on SubChannelID change
                    attribute   : 'Channel',
                    xtype       : 'modalcombo',
                    store       : 'TestOrimi.store.ChannelStore'
            },{
                    fieldLabel  : 'Канал для рекламы',
                    name        : 'AdvertisingChannelID',
                    xtype       : 'modalcombo',
                    attribute   : 'AdvertisingChannel',
                    hidden      : (userRights['TT_AdvChannel_set'] !== 1),
                    store       : 'TestOrimi.store.AdvertisingChannelStore'
            },{
                    fieldLabel  : 'Формат',
                    name        : 'FormatID',
                    hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                    xtype       : 'modalcombo',
                    store       : 'TestOrimi.store.FormatStore'
            },{
                    fieldLabel  : 'Сеть',
                    xtype       : 'radiogroup',
                    hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                    anchor      : 'none',
                    name        : 'isNetGroup',
                    //afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowBlank  : (!this.hasActiveStatus),
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Это поле не поменяет тип элемента (ТТ или Дистрибьютор)">*</span>',
                    layout      : {
                        autoFlex: false
                    },
                    defaults: {
                        name: 'isNet',
                        margin: '0 15 0 0'
                    },
                    items: [{
                            boxLabel    : 'Нет',
                            inputValue  : '0'
                    },{
                            boxLabel    : 'Да',
                            inputValue  : '1'
                    }],
                    listeners : {
                        change: function(radio, inputValue, oldValue) {
                            var panel = radio.up('form'),
                                changeInput = inputValue.isNet;

                            panel.down('[name=checkboxRespTRID]').enable();
                            panel.down('[name=checkboxRespMerchID]').enable();

                            if ( changeInput == 1 ) {
                                panel.down('[name=checkboxNetID]').enable();
                            } else {
                                panel.down('[name=checkboxNetID]').setValue(0);
                                panel.down('[name=checkboxNetID]').disable();
                            }
                        },
                        disable: function(radio) {
                            var panel = radio.up('form');

                            if (userRights['custom'] != 1) {
                                panel.down('[name=checkboxRespTRID]').setValue(0);
                                panel.down('[name=checkboxRespTRID]').disable();
                                panel.down('[name=checkboxRespMerchID]').setValue(0);
                                panel.down('[name=checkboxRespMerchID]').disable();
                            }
                            panel.down('[name=checkboxContractorGroupID]').setValue(0);
                            panel.down('[name=checkboxContractorGroupID]').disable();
                            panel.down('[name=checkboxNetID]').setValue(0);
                            panel.down('[name=checkboxNetID]').disable();
                        }
                    }
            },{
                    fieldLabel  : 'Название сети',
                    name        : 'NetID',
                    hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    xtype       : 'modalcombo',
                    attribute: 'Net',
                    store       : 'TestOrimi.store.NetStore',
                    allowBlank  : (!this.hasActiveStatus)
            },{
                    fieldLabel  : 'Отв. ТП',
                    name        : 'RespTRID',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    attribute   : 'ParentStaff',
                    xtype       : 'modalcombo',
                    displayField: 'FIO', 
                    store       : 'TestOrimi.store.ParentStore',
                    allowBlank  : (!this.hasActiveStatus)
            },{
                    fieldLabel  : 'Отв. Мерчендайзер',
                    name        : 'RespMerchID',
                    hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                    attribute   : 'ParentStaff',
                    xtype       : 'modalcombo',
                    displayField: 'FIO', 
                    store       : 'TestOrimi.store.ParentStore'
            },{
                    fieldLabel  : 'Грузополучатель ОРИМИ',
                    xtype       : 'radiogroup',
                    hidden      : (userRights['custom'] == 1),
                    anchor      : 'none',
                    name        : 'isConsigneeGroup',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowBlank  : (!this.hasActiveStatus),
                    layout      : {
                        autoFlex: false
                    },
                    defaults: {
                        name: 'isConsignee',
                        margin: '0 15 0 0'
                    },
                    items: [{
                            boxLabel    : 'Нет',
                            inputValue  : '0'
                    },{
                            boxLabel    : 'Да',
                            inputValue  : '1'
                    }],
                    listeners : {
                        change: function(radio, inputValue, oldValue) {
                            var panel = radio.up('form'),
                                changeInput = inputValue.isConsignee;

                            if ( changeInput == 1 ) {
                                panel.down('[name=checkboxContractorGroupID]').enable();
                            } else {
                                panel.down('[name=checkboxContractorGroupID]').setValue(0);
                                panel.down('[name=checkboxContractorGroupID]').disable();
                            }
                        },
                        disable: function(radio) {
                            var panel = radio.up('form');

                            panel.down('[name=checkboxContractorGroupID]').setValue(0);
                            panel.down('[name=checkboxContractorGroupID]').disable();
                        }
                    }
            },{
                    fieldLabel  : 'Группа контрагентов',
                    name        : 'ContractorGroupID',
                    hidden      : (userRights['custom'] == 1),
                    xtype       : 'modalcombo',
                    store       : 'TestOrimi.store.ContractorGroupStore'
            },{
                    xtype       : 'datefield',
                    fieldLabel  : 'Дата открытия ТТ',
                    hidden      : (userRights['custom'] == 1 && this.hasActiveStatus),
                    format      : 'Y-m-d',
                    name        : 'DateOpen',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowBlank  : (!this.hasActiveStatus)
            },{
                    xtype       : 'datefield',
                    fieldLabel  : 'Дата закрытия ТТ',
                    format      : 'Y-m-d',
                    name        : 'DateClose'
            },{
                    fieldLabel  : 'Площадь ТТ',
                    xtype       : 'numberfield',
                    minValue    : 0,
                    name        : 'ShopArea'
            },{
                    fieldLabel  : 'Координаты',
                    xtype       : 'textfield',
                    name        : 'Coordinates'
            },{
                    fieldLabel  : 'GLN',
                    xtype       : 'textfield',
                    name        : 'GLN'
            },{
                    xtype       : 'textareafield',
                    grow        : true,
                    name        : 'Comment',
                    fieldLabel  : 'Комментарий'
            },{
                    fieldLabel  : 'Участвует в РБП',
                    xtype       : 'radiogroup',
                    anchor      : 'none',
                    name        : 'isInRBPGroup',
                    layout      : {
                        autoFlex: false
                    },
                    defaults: {
                        name: 'isInRBP',
                        margin: '0 15 0 0'
                    },
                    items: [{
                            boxLabel    : 'Да',
                            inputValue  : '1'
                    },{
                            boxLabel    : 'Нет',
                            inputValue  : '0'
                    }]
            }]
          }]
        }];

    	this.buttons = [{
            text    : 'Ок',
            handler: function() {
              var store = Ext.getStore('TestOrimi.store.MasterStoresStore'),
                  win = this.up('window'),
                  form = win.down('form').getForm();

              //get cleared form values
              var rawVals = form.getValues(),
                  radiogroups = ['isNet','isInRBP'],
                  params = {};

              Ext.Object.each(rawVals, function(key, value, myself) {
                  params[key] = value;
              });
              //

              params['ID'] = win.records.toString();
              params['act'] = store.proxy.extraParams['act'];
              params['subaction'] = 'groupUpdate';

              if (form.isValid()) {
                    var myMask = new Ext.LoadMask(win, {msg:"Сохраняю..."});
                    
                    myMask.show();
                    Ext.Ajax.request({
                        url: 'resources/data/api.php',
                        params: params,
                        success: function(response) {
                            myMask.destroy();
                            var responseText = Ext.JSON.decode(response.responseText);

                            TestOrimi.getApplication().getController('Main').showNotification('Успешно! Элементы обновлены', 'winSuccess');
                            if (responseText && responseText.msg) {
                                TestOrimi.getApplication().getController('Main').showNotification(''+responseText.msg, 'winError');
                            }
                            store.load();
                            win.destroy();
                        },
                        failure: function(form, action) {
                            myMask.destroy();
                            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Нет ответа от сервера', 'winError');
                        }
                    });
                }
            }
        },{
          text    : 'Отмена',
          handler: function() {
            this.up('window').destroy();
          }
        }];
      
        this.callParent(arguments);
    }
});
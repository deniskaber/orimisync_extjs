Ext.define('TestOrimi.view.ParentNetWindow', {
    extend: 'Ext.window.Window',
    xtype: 'ParentNetWindow',
    width: '760px',
    height: '525px',
    title: 'Агрегирующая сеть',
    modal: true,
    layout: 'fit',

    initComponent: function () {
        var me = this,
            field = me.field;

        me.items = [{
            xtype: 'grid'
            , flex: 1
            , plugins: [{
                ptype: 'bufferedrenderer',
                trailingBufferZone: 0,
                leadingBufferZone: 0
            }]
            , border: true
            , columnLines: true
            , columns: [
                {text: 'ID', dataIndex: 'ID', width: 60}
                , {text: 'Наименование', dataIndex: 'Name', width: 250}
                , {text: 'Краткое наим.', dataIndex: 'ShortName', width: 140}
                , {text: 'Тип сети', dataIndex: 'NetType', width: 140}
                , {text: 'КАИ Nielsen', dataIndex: 'isNielsen', width: 140}
                , {text: 'Отв. менеджер', dataIndex: 'RespManager', width: 140}
                , {text: 'Предоставляет данные', dataIndex: 'hasSalesData', width: 180}
            ]
            , store: field.store
            , listeners: {
                celldblclick: function (grid, td, cellIndex, record, tr, rowIndex) {
                    me.down('[name=okButton]').handler();
                }
            }
        }];

        var i = 0,
            columns = this.items[0].columns,
            length = columns.length;

        for (; i < length; i++) {
            columns[i].items = [{
                xtype: 'container',
                margin: 4,
                flex: 1,
                layout: 'fit',
                listeners: {
                    scope: this,
                    element: 'el',
                    mousedown: function (e) {
                        e.stopPropagation();
                    },
                    click: function (e) {
                        e.stopPropagation();
                    },
                    keydown: function (e) {
                        e.stopPropagation();
                    },
                    keypress: function (e) {
                        e.stopPropagation();
                    },
                    keyup: function (e) {
                        e.stopPropagation();
                    }
                },
                items: [{
                    xtype: 'textfield',
                    emptyText: 'Поиск',
                    enableKeyEvents: true,
                    onTriggerClick: function () {
                        this.reset();
                        this.focus();
                    },
                    listeners: {
                        change: function (field, newValue, oldValue, eOpts) {
                            var grid = this.up('grid');
                            me.fireEvent('searchchange', grid, field, newValue, oldValue);
                        },
                        buffer: 1000
                    }
                }]
            }];
        }

        me.buttons = [{
            text: 'Ок',
            name: 'okButton',
            disabled: me.button,
            handler: function () {
                var win = this.up('window'),
                    selected = win.down('grid').getSelectionModel().getSelection()[0];

                if (!selected) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
                } else {
                    if (selected.get('Deleted') == 1) {
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение помечено на удаление', 'winError');
                    } else if ( selected.get('ID') == field.up('window').down('[name=ID]').value ) {
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сеть не может быть родителем самой себя', 'winError');
                    } else {
                        if (field.value != selected.get('ID')) {
                            win.field.setValue(selected.get('ID'));
                        }
                        win.destroy();
                    }
                }
            }
        }, {
            text: 'Отмена',
            handler: function () {
                this.up('window').destroy();
            }
        }];

        this.callParent(arguments);
    }
});
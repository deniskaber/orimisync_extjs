Ext.define('TestOrimi.view.AdvertisingEventsRowsGrid', {
    extend: 'Ext.grid.Panel',
    requires: ['TestOrimi.store.AdvertisingEventsRowsStore'],
    xtype: 'advertising_events_rows_grid',
    flex: 1,
    columnLines: true,
    border: false,
    selType: 'checkboxmodel',
    viewConfig: {
        stripeRows: false,
        getRowClass: function(record) {
            var rowClass = '';
            if (record.get('EstimatedSalesAmountQtyPerStr') > record.get('MaxNumberToOneStore')) {
                rowClass = 'alert-row';
            } else if (
                (record.get('EstimatedSellingPrice') < record.get('MinPrice')) ||
                (record.get('RealSellingPrice') && record.get('RealSellingPrice') < record.get('MinPrice'))
            ) {
                rowClass = 'alert-row';
            } else if (record.get('AmountOfDaysAfterPrev') < 90) {
                rowClass = 'yellow-row';
            } else if (record.get('EstimatedSalesAmountQtyPerStr') > record.get('MaxNumberToShip') || !record.get('MaxNumberToShip')) {
                rowClass = 'temp-status-row';
            }

            return rowClass;
        },
        shrinkWrap: 0,
        shadow: false,
        trackOver: false,
        overItemCls: false
    },
    plugins: [{
        ptype: 'bufferedrenderer',
        trailingBufferZone: 0,
        leadingBufferZone: 0
    }],
    rendererGridCell: function(val) {
        if (val !== 0) {
            return Ext.util.Format.number(val, '0.000/i').replace(/\./g, ' ');
        }
    },
    rendererGridCellDigits: function(val) {
        if (val !== 0) {
            return Ext.util.Format.number(val, '0.000,00/i').replace(/\./g, ' ');
        }
    },
    initComponent: function() {
        var me         = this,
            userRights = TestOrimi.getApplication().data;

        this.store = 'TestOrimi.store.AdvertisingEventsRowsStore';

        this.columns = [
            {
                text: 'ID<br>&nbsp',
                dataIndex: 'ID',
                width: 100,
                hidden: true
            },
            {
                text: 'ID заявки<br>&nbsp',
                dataIndex: 'RequestID',
                width: 100,
                hidden: true
            },
            {
                text: 'ID товара<br>&nbsp',
                dataIndex: 'GoodID',
                width: 100,
                hidden: true
            },
            {
                text: 'Наименование товара<br>&nbsp',
                dataIndex: 'GoodName',
                width: 220
            },
            {
                text: 'Код Орими<br>&nbsp',
                dataIndex: 'OrimiCode',
                width: 120
            },
            {
                text: 'Фасовка товара<br>&nbsp',
                dataIndex: 'ExternalPack',
                width: 140
            },
            {
                text: 'Бренд<br>&nbsp',
                dataIndex: 'Brand',
                width: 140
            },
            {
                text: 'Сегмент<br>&nbsp',
                dataIndex: 'Segment',
                width: 140
            },
            {
                text: 'Упаковка товара<br>&nbsp',
                dataIndex: 'InternalPack',
                width: 140
            },
            {
                text: 'Комментарий<br>&nbsp',
                dataIndex: 'Comment',
                width: 150,
                hidden: true
            },
            {
                text: 'Количество дней от<br>предыдущей акции',
                dataIndex: 'AmountOfDaysAfterPrev',
                width: 180,
                align: 'right'
            },

            {
                text: 'Скидка по акции<br>от ОРИМИ ТРЭЙД, %',
                dataIndex: 'OTDiscount',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Скидка<br>дистрибьютора, %',
                dataIndex: 'DistrDiscount',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Скидка<br>торговой сети, %',
                dataIndex: 'NetDiscount',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Входная цена<br>дистрибьютора, руб',
                dataIndex: 'DistrIncomePrice',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Входная цена торговой<br>сети до акции, руб',
                dataIndex: 'NetIncomePriceBefore',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Цена позиции на полке<br>в ТТ до акции, руб',
                dataIndex: 'SellingPriceBefore',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Входная цена торговой<br>сети во время акции, руб',
                dataIndex: 'NetIncomePriceAction',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Разница между акционной<br>и регулярной ценой, руб',
                dataIndex: 'IncomePriceDifference',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Скидка в<br>накладной, %',
                dataIndex: 'InvoiceDiscount',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Максимальная<br>глубина скидки, %',
                dataIndex: 'MaxDiscountDepth',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Фонд генерального<br>директора, %',
                dataIndex: 'FGD',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Минимально возможная цена<br>на полке за период, руб',
                dataIndex: 'MinPrice',
                width: 230,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Цена базового<br>прайс-листа, руб',
                dataIndex: 'BPLPrice',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Наценка сети, %<br>&nbsp',
                dataIndex: 'NetMarkup',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Прогноз цены на полке<br>во время акции, руб',
                dataIndex: 'EstimatedSellingPrice',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Фактически проведенная<br>скидка в сети, %',
                dataIndex: 'FactDiscount',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Цена поставки в ТС данного<br>ассортимента (без акции от БПЛ), %',
                dataIndex: 'SupplyPrice',
                width: 280,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Продажи до акции по данной позиции<br>за сопоставимый период времени, шт',
                dataIndex: 'SalesAmountBeforeQty',
                width: 300,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Продажи до акции по данной позиции<br>за сопоставимый период времени, руб',
                dataIndex: 'SalesAmountBeforeVal',
                width: 300,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Прогноз<br>компенсации, шт',
                dataIndex: 'EstimatedSalesAmountQty',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Прогноз<br>компенсации, руб',
                dataIndex: 'EstimatesSalesAmountVal',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Прогноз<br>компенсации  ФГД, руб',
                dataIndex: 'EstimatesSalesAmountFGD',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Прогноз<br>компенсации бонус Д0, руб',
                dataIndex: 'EstimatesSalesAmountD0Bonus',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Прогноз компенсации<br>на торговую точку, шт',
                dataIndex: 'EstimatedSalesAmountQtyPerStr',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Макс. разрешенное кол-во<br>на 1 точку, шт',
                dataIndex: 'MaxNumberToOneStore',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Макс. количество единиц<br>к отгрузке по акции, шт',
                dataIndex: 'MaxNumberToShip',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Стоимость публикации<br>в каталоге, руб',
                dataIndex: 'CataloguePubCost',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Реальная цена<br>на полке, руб',
                dataIndex: 'RealSellingPrice',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Продажи во время акции<br>с учетом скидки, шт',
                dataIndex: 'RealSalesAmountQty',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Продажи во время акции<br>с учетом скидки, руб',
                dataIndex: 'RealSalesAmountVal',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Продажи во время акции<br>в регулярных ценах, руб',
                dataIndex: 'RealSalesAmountInRegularPrices',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Затраты<br>дистрибьютора, руб',
                dataIndex: 'DistrCost',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Затраты ОТ<br>на акцию, руб',
                dataIndex: 'OTCost',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Компенсация ФГД, руб<br>&nbsp',
                dataIndex: 'FGDComp',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Компенсация Д0, руб<br>&nbsp',
                dataIndex: 'D0Comp',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCell
            },
            {
                text: 'Прирост, %<br>&nbsp',
                dataIndex: 'SalesIncrease',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            },
            {
                text: 'Процент затрат, %<br>&nbsp',
                dataIndex: 'IncreasePercent',
                width: 200,
                align: 'right',
                renderer: this.rendererGridCellDigits
            }
        ];

        var i       = 0,
            columns = this.columns,
            length  = columns.length;

        for (; i < length; i++) {
            columns[i].items = [{
                xtype: 'container',
                margin: 4,
                flex: 1,
                layout: 'fit',
                listeners: {
                    scope: this,
                    element: 'el',
                    mousedown: function(e) {
                        e.stopPropagation();
                    },
                    click: function(e) {
                        e.stopPropagation();
                    },
                    keydown: function(e) {
                        e.stopPropagation();
                    },
                    keypress: function(e) {
                        e.stopPropagation();
                    },
                    keyup: function(e) {
                        e.stopPropagation();
                    }
                },
                items: [{
                    xtype: 'textfield',
                    emptyText: 'Поиск',
                    enableKeyEvents: true,
                    onTriggerClick: function() {
                        this.reset();
                        this.focus();
                    },
                    listeners: {
                        change: function(field, newValue, oldValue, eOpts) {
                            me.fireEvent('searchchange', me, field, newValue, oldValue);
                        },
                        buffer: 1000
                    }
                }]
            }];
        }

        this.lbar = [{
            xtype: 'container',
            name: 'lbar',
            width: 55,
            padding: 2,
            border: true,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            defaults: {
                xtype: 'button',
                margin: '2 0 2 0',
                scale: 'medium'
            },
            items: [{
                name: 'combSearch',
                tooltip: 'Комб. поиск',
                text: '<span class="oi" data-glyph="magnifying-glass" aria-hidden="true"></span>',
                handler: function() {
                    Ext.create('TestOrimi.view.CreateSearchWindowAdvertisingEventRows', {grid: me}).show();
                }
            }, {
                name: 'headerSearch',
                tooltip: 'Отфильтровать заголовки по выбранной строке',
                disabled: true,
                text: '<span class="oi" data-glyph="arrow-thick-top" aria-hidden="true"></span>',
                handler: function() {
                    me.fireEvent('headerSearchButtonClick', me);
                }
            }, {
                name: 'addNewRowButton',
                tooltip: 'Добавление новой записи',
                disabled: true,
                text: '<span class="oi" data-glyph="document" aria-hidden="true"></span>',
                handler: function() {
                    me.fireEvent('addNewRowButtonClick', me);
                }
            }, {
                name: 'addMultipleRowButton',
                tooltip: 'Добавить несколько товаров',
                disabled: true,
                text: '<span class="oi" data-glyph="layers" aria-hidden="true"></span>',
                handler: function() {
                    me.fireEvent('addMultipleRowButtonClick', me);
                }
            }, {
                name: 'duplicateMultipleRowButton',
                tooltip: 'Дублировать выбранные строки внутри акции',
                disabled: true,
                text: '<span class="oi" data-glyph="fork" aria-hidden="true"></span>',
                handler: function() {
                    me.fireEvent('duplicateMultipleRowButtonClick', me);
                }
            }, {
                name: 'eventHistoryButton',
                tooltip: 'Просмотр истории работы с позицией',
                disabled: true,
                text: '<span class="oi" data-glyph="book" aria-hidden="true"></span>',
                handler: function() {
                    var selection      = me.getSelectionModel().getSelection(),
                        selectedRecord = selection && selection[0];

                    if (!selectedRecord) {
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Позиция не выбрана', 'winError');
                        return;
                    }

                    me.fireEvent('eventHistoryButtonClick', me, {ref: 'Advertising Events Rows'});
                }
            }, {
                name: 'deleteButton',
                tooltip: 'Удалить',
                disabled: true,
                text: '<span class="oi" data-glyph="trash" aria-hidden="true"></span>',
                handler: function() {
                    me.fireEvent('deleteaddclick', me);
                }
            }, {
                name: 'reportButton',
                tooltip: 'Выгрузка в Excel',
                text: '<span class="oi" data-glyph="spreadsheet" aria-hidden="true"></span>',
                handler: function() {
                    me.fireEvent('reportbuttonclick', me);
                }
            }]
        }];

        if (!userRights['Advertising_events']) {
            delete this.lbar;
        }

        this.bbar = {
            xtype: 'pagingtoolbar',
            store: this.store,
            inputItemWidth: 60,
            displayInfo: true,
            items: [{
                xtype: 'tbseparator'
            }, {
                xtype: 'checkbox',
                boxLabel: 'Показывать все',
                name: 'showAll',
                inputValue: 1,
                listeners: {
                    change: function(cb, newValue, oldValue, eOpts) {
                        cb.up('pagingtoolbar').store.loadPage(1);
                    }
                }
            }]
        };

        this.callParent(arguments);

    }
});

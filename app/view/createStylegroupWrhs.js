Ext.define('TestOrimi.view.createStylegroupWrhs', {
    extend: 'Ext.window.Window',
    width: 500,
    height: 600,
    title: 'Групповая установка свойств',
    modal: true,
    layout: 'fit',

    initComponent: function () {
        var me = this,
            userRights = TestOrimi.getApplication().data;

        var myListener = {
            change: function (field, newValue, oldValue) {
                var toField;

                if (newValue) {
                    toField = field.up('window').down('[name=' + field.name.substring(8) + ']');

                    toField.enable();
                    if (field.name == 'checkboxisNetGroup') {
                        toField.fireEvent('change', toField, toField.getValue());
                    }
                } else {
                    toField = field.up('window').down('[name=' + field.name.substring(8) + ']');
                    //toField.reset();
                    toField.disable();
                }
            }
        };

        me.items = [{
            xtype: 'form',
            padding: 10,
            autoScroll: true,
            border: false,
            layout: {
                type: 'column'
            },
            items: [{
                columnWidth: 0.05,
                defaults: {
                    xtype: 'checkboxfield',
                    listeners: myListener
                },
                items: [{
                    inputValue: 'Name',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxName'
                }, {
                    inputValue: 'DistributorID',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxDistributorID'
                }, {
                    inputValue: 'PostCode',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxPostCode'
                }, {
                    inputValue: 'CityID',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxCityID'
                }, {
                    inputValue: 'Street',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxStreet'
                }, {
                    inputValue: 'Building',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxBuilding'
                }, {
                    inputValue: 'FIAS',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxFIAS'
                }, {
                    inputValue: 'FormatID',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxFormatID'
                }, {
                    inputValue: 'DateOpen',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'checkboxDateOpen'
                }, {
                    inputValue: 'DateClose',
                    name: 'checkboxDateClose'
                }, {
                    inputValue: 'ShopArea',
                    name: 'checkboxShopArea'
                }, {
                    inputValue: 'Comment',
                    name: 'checkboxComment',
                    margin: '0 0 44 0'
                }]

            }, {
                columnWidth: 0.95,
                name: 'fieldCont',
                defaults: {
                    anchor: '90%',
                    labelWidth: 180,
                    disabled: true
                },
                layout: {
                    type: 'vbox'
                    , align: 'stretch'
                },
                items: [{
                    fieldLabel: 'Название',
                    xtype: 'textfield',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'Name',
                    allowBlank: (!this.hasActiveStatus)
                }, {
                    fieldLabel: 'Дистрибьютор',
                    name: 'DistributorID',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    attribute: 'Distributor',
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.DistributorsStore'
                }, {
                    fieldLabel: 'Индекс',
                    xtype: 'numberfield',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    minValue: 0,
                    name: 'PostCode'
                }, {
                    fieldLabel: 'Город',
                    name: 'CityID',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    attribute: 'City',
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.CityStore',
                    allowBlank: (!this.hasActiveStatus)
                }, {
                    fieldLabel: 'Улица',
                    xtype: 'textfield',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'Street',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowBlank: (!this.hasActiveStatus)
                }, {
                    fieldLabel: 'Дом',
                    xtype: 'textfield',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'Building',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowBlank: (!this.hasActiveStatus)
                }, {
                    fieldLabel: 'ФИАС',
                    xtype: 'textfield',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    name: 'FIAS'
                }, {
                    fieldLabel: 'Формат',
                    name: 'FormatID',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.FormatStore'
                }, {
                    xtype: 'datefield',
                    fieldLabel: 'Дата открытия',
                    hidden: (userRights['custom'] == 1 && this.hasActiveStatus),
                    format: 'Y-m-d',
                    name: 'DateOpen',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowBlank: (!this.hasActiveStatus)
                }, {
                    xtype: 'datefield',
                    fieldLabel: 'Дата закрытия',
                    format: 'Y-m-d',
                    name: 'DateClose'
                }, {
                    fieldLabel: 'Площадь',
                    xtype: 'numberfield',
                    minValue: 0,
                    name: 'ShopArea'
                }, {
                    xtype: 'textareafield',
                    grow: true,
                    name: 'Comment',
                    fieldLabel: 'Комментарий'
                }]
            }]
        }];

        me.buttons = [{
            text: 'Ок',
            handler: function () {
                var store = Ext.getStore('TestOrimi.store.MasterWarehousesStore'),
                    win = this.up('window'),
                    form = win.down('form').getForm();

                //get cleared form values
                var rawVals = form.getValues(),
                    params = {};

                Ext.Object.each(rawVals, function (key, value, myself) {
                    params[key] = value;
                });
                //

                params['ID'] = win.records.toString();
                params['act'] = store.proxy.extraParams['act'];
                params['subaction'] = 'groupUpdate';

                if (form.isValid()) {
                    var myMask = new Ext.LoadMask(win, {msg: "Сохраняю..."});

                    myMask.show();
                    Ext.Ajax.request({
                        url: 'resources/data/api.php',
                        params: params,
                        success: function (response) {
                            myMask.destroy();

                            var responseText = Ext.JSON.decode(response.responseText);

                            if (!responseText.success) {
                                TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + responseText.msg, 'winError');
                            } else {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Элементы обновлены', 'winSuccess');
                                store.load();
                                win.destroy();
                            }
                        },
                        failure: function (form, action) {
                            myMask.destroy();
                            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Нет ответа от сервера', 'winError');
                        }
                    });
                }
            }
        }, {
            text: 'Отмена',
            handler: function () {
                this.up('window').destroy();
            }
        }];

        this.callParent(arguments);
    }
});
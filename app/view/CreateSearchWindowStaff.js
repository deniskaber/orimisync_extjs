Ext.define('TestOrimi.view.CreateSearchWindowStaff', {
    extend: 'Ext.window.Window',
    title: 'Комбинированный поиск',
    modal: true,
    width: 500,
    height: 600,
    layout: 'fit',
    items: [{
        xtype: 'form',
        margin: 10,
        border: false,
        layout: {type: 'vbox', align: 'stretch'},
        items: [{
            xtype: 'textfield'
            , fieldLabel: 'Поиск по всем атрибутам'
            , name: 'searchall'
            , anchor: '90%'
            , labelWidth: 170
        }, {
            fieldLabel: 'Применить ко всем справочникам'
            , xtype: 'radiogroup'
            , labelWidth: 170
            , anchor: 'none'
            , layout: {
                autoFlex: false
            },
            defaults: {
                name: 'allstaff',
                margin: '0 15 0 0'
            }
            , items: [{
                boxLabel: 'нет',
                inputValue: 0
            }, {
                boxLabel: 'да',
                inputValue: 1
            }, {
                boxLabel: 'только подчиненные',
                inputValue: 2,
                margin: 0
            }]
        }, {
            xtype: 'container',
            flex: 1,
            autoScroll: true,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            margin: '20 0 0 0',
            defaults: {
                xtype: 'textfield',
                anchor: '90%',
                labelWidth: 160,
                margin: '3 15 2 5'
            },
            items: [{
                fieldLabel: 'ID',
                name: 'ID'
            }, {
                fieldLabel: 'Фамилия',
                name: 'SurName'
            }, {
                fieldLabel: 'Имя',
                name: 'Name'
            }, {
                fieldLabel: 'Отчество',
                name: 'LastName'
            }, {
                fieldLabel: 'Тип',
                name: 'Type',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.TypeStaffStore'
            }, {
                fieldLabel: 'Принадлеж. к дивизиону', //Принадлежность
                name: 'Division',
                attribute: 'Division',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.DivisionStore'
            }, {
                fieldLabel: 'Дата начала работы',
                name: 'DateHired',
                xtype: 'datefield',
                format: 'Y-m-d'
            }, {
                fieldLabel: 'Сектор',
                name: 'Sector',
                attribute: 'Sector',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.SectorStore'
            },
                // {
                //     fieldLabel: 'Участок',
                //     name: 'Area',
                //     attribute: 'Area',
                //     xtype: 'searchModalCombo',
                //     store: 'TestOrimi.store.AreaStore'
                // },
                {
                    fieldLabel: 'Код',
                    name: 'Code'
                }, {
                    fieldLabel: 'ФИО',
                    name: 'FIO'
                }, {
                    fieldLabel: 'Дата рождения',
                    name: 'BirthDate',
                    xtype: 'datefield',
                    format: 'Y-m-d'
                }, {
                    fieldLabel: 'Дата увольнения',
                    name: 'DateFired',
                    xtype: 'datefield',
                    format: 'Y-m-d'
                }, {
                    fieldLabel: 'E-mail',
                    name: 'EMail'
                }, {
                    fieldLabel: 'Телефон',
                    name: 'Phone'
                }, {
                    fieldLabel: 'Комментарий',
                    name: 'Comment'
                }, {
                    fieldLabel: 'Мотивация',
                    name: 'Motivation',
                    attribute: 'Motivation',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.MotivationStore'
                }, {
                    fieldLabel: 'Признак',
                    xtype: 'radiogroup',
                    anchor: 'none',
                    layout: {
                        autoFlex: false
                    },
                    defaults: {
                        name: 'Feature',
                        margin: '0 15 0 0'
                    },
                    items: [{
                        boxLabel: 'Белый',
                        inputValue: 1
                    }, {
                        boxLabel: 'Серый',
                        inputValue: 2
                    }]
                }, {
                    fieldLabel: 'Учитывать в команде',
                    xtype: 'radiogroup',
                    anchor: 'none',
                    layout: {
                        autoFlex: false
                    },
                    defaults: {
                        name: 'ConsiderInTeam',
                        margin: '0 15 0 0'
                    },
                    items: [{
                        boxLabel: 'да',
                        inputValue: 1
                    }, {
                        boxLabel: 'нет',
                        inputValue: 0
                    }]
                }, {
                    fieldLabel: 'Кому подчиняется',
                    name: 'Parent',
                    attribute: 'ParentStaff',
                    displayField: 'FIO',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.ParentStore'
                }, {
                    fieldLabel: 'Статус',
                    name: 'Status'
                }, {
                    fieldLabel: 'Источник',
                    name: 'DataSource'
                }, {
                    fieldLabel: 'Автор изменений',
                    name: 'Author'
                }, {
                    fieldLabel: 'Дата посл. изменения',
                    xtype: 'datefield',
                    format: 'Y-m-d',
                    name: 'DateChange'
                }, {
                    fieldLabel: 'Авто/Ручной',
                    xtype: 'radiogroup',
                    anchor: 'none',
                    layout: {
                        autoFlex: false
                    },
                    defaults: {
                        name: 'SyncType',
                        margin: '0 15 0 0'
                    },
                    items: [{
                        boxLabel: 'Авто',
                        inputValue: 1
                    }, {
                        boxLabel: 'Ручной',
                        inputValue: 0
                    }]
                }]
        }]
    }],

    buttons: [{
        text: 'Сбросить все',
        handler: function () {
            this.up('window').down('form').getForm().reset();
        }
    }, '->', {
        text: 'Применить',
        handler: function () {
            var grid = this.up('window').grid,
                store = grid.store,
                formVals = this.up('window').down('form').getValues(),
                filters = [];

            var modelFields = store.model.getFields(),
                dataFields = [],
                i = 0,
                length = modelFields.length;

            for (; i < length; i++) {
                dataFields.push(modelFields[i].name);
            }

            Ext.Object.each(formVals, function (key, value, myself) {
                if (value || value === 0) {
                    if (Ext.Array.contains(dataFields, key)) {
                        filters.push({property: key, value: value});
                    }
                }
            });

            if (formVals['allstaff'] == 1) {
                var masterStore = Ext.getStore('TestOrimi.store.MasterStaffStore'),
                    syncedStore = Ext.getStore('TestOrimi.store.SyncedStaffStore'),
                    slaveStore = Ext.getStore('TestOrimi.store.SlaveStaffStore');

                masterStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                masterStore.fireEvent('applyFilters', masterStore, filters, grid.up('viewport').down('general_gridStaff'));
                syncedStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                syncedStore.fireEvent('applyFilters', syncedStore, filters, grid.up('viewport').down('general_tabpanelStaff').down('general_tabpanelFirstcolStaff'));
                slaveStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                slaveStore.fireEvent('applyFilters', slaveStore, filters, grid.up('viewport').down('general_tabpanelStaff').down('general_tabpanelSecondcolStaff'));
            } else if (formVals['allstaff'] == 2) {
                var syncedStore = Ext.getStore('TestOrimi.store.SyncedStaffStore'),
                    slaveStore = Ext.getStore('TestOrimi.store.SlaveStaffStore');

                syncedStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                syncedStore.fireEvent('applyFilters', syncedStore, filters, grid.up('viewport').down('general_tabpanelStaff').down('general_tabpanelFirstcolStaff'));
                slaveStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                slaveStore.fireEvent('applyFilters', slaveStore, filters, grid.up('viewport').down('general_tabpanelStaff').down('general_tabpanelSecondcolStaff'));
            } else {
                store.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                store.fireEvent('applyFilters', store, filters, grid);
            }

            this.up('window').destroy();
        }
    }, {
        text: 'Отмена',
        handler: function () {
            this.up('window').destroy();
        }
    }],

    initComponent: function () {
        var me = this,
            grid = me.grid;

        this.callParent(arguments);

        var cols = [],
            columns = grid.columns,
            length = columns.length;

        for (i = 0; i < length; i++) {
            if (columns[i]['dataIndex'])
                cols.push(columns[i]['dataIndex']);
        }
        cols.push('searchall');
        cols.push('allstaff');
        cols.push('Feature');

        var fields = me.query('textfield'), //все textfield окна
            radiogroups = me.query('radiogroup');

        length = fields.length;

        for (i = 0; i < length; i++) {                //цикл указывания какие поля останутся активными
            if (!(Ext.Array.contains(cols, fields[i].name))) {
                fields[i].hide();
                fields[i].disable();
            }
        }

        length = radiogroups.length;

        for (i = 0; i < length; i++) {                //цикл указывания какие поля останутся активными
            if (!(Ext.Array.contains(cols, radiogroups[i].items.items[0].name))) {
                radiogroups[i].hide();
                radiogroups[i].disable();
            }
        }

        var store = grid.store,
            filterRecord = {},
            filters = store.filters.items;

        length = filters.length;

        if (length) {
            for (i = 0; i < length; i++) {
                filterRecord[filters[i].property] = filters[i].value;
            }
        }
        if (store.proxy.extraParams['searchall']) {
            filterRecord['searchall'] = store.proxy.extraParams['searchall'];
        }
        this.down('form').getForm().setValues(filterRecord);
    }
});

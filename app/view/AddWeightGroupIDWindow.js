Ext.define('TestOrimi.view.AddWeightGroupIDWindow', {
    extend: 'Ext.window.Window',
	width: 400,
    height: 280,
	modal: true,
	resizable: false,
	layout: 'fit',
    items: [{
		xtype	: 'form',
		layout	: 'anchor',
		padding	: 5,
		defaults: {
			xtype	: 'textfield',
			anchor	: '100%'
		},
		items: [{
			fieldLabel  : 'ID'
			,name 		: 'ID'
			,cls		: 'noteditable'
			,readOnly 	: true
		},{
			fieldLabel	: 'Наименование'
			,name		: 'Name'
			,cls		: 'noteditable'
			,readOnly 	: true
		},{
			fieldLabel	: 'Краткое наим.',
			name		: 'ShortName'
		},{
			fieldLabel 	: 'Краткое обозн.',
			name  		: 'BriefNotation'
		},{
			fieldLabel  : 'Вес от',
			name        : 'Begin',
			xtype		: 'numberfield',
			value    	: 0,
			minValue    : 0,
			allowBlank	: false
		},{
			fieldLabel  : 'Вес до',
			name        : 'End',
			xtype		: 'numberfield',
			value    	: 0,
			minValue    : 0,
			allowBlank	: false
		},{
			xtype 		: 'checkbox',
			fieldLabel 	: 'Удален',
			name 		: 'Deleted',
			inputValue 	: 1
		}]
	}],
	buttons		:  [{
		text 	: 'Ок',
		handler: function() {
			var win = this.up('window'),
				form = win.down('form').getForm(),
				formInput = win.down('form'),
				name = formInput.down('[name=Name]'),
				begin = formInput.down('[name=Begin]').getValue(),
				end = formInput.down('[name=End]').getValue();


			if (begin<=end) {
				if (form.isValid()){
					var myMask = new Ext.LoadMask(win, {msg:"Сохраняю..."});

					name.setValue('От ' + begin + ' гр., до ' + end + ' гр.');
					myMask.show();
					form.submit({
						url: 'resources/data/api.php',
						timeout: 60000, 
            			params: {
							act: win.store.proxy.extraParams['act'],
							subaction: win.data ? 'update': 'create'
						},
						success: function(response) {
							myMask.destroy();
							var responseText = Ext.JSON.decode(response.responseText);
							if (responseText)
		                    	TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + responseText.msg, 'winError');
		                    else {
			                      if (win.data) {
									TestOrimi.getApplication().getController('Main').showNotification('Успешно! Элемент обновлен', 'winSuccess');
								} else {
									TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess');
								}
								win.store.load();
								win.destroy();
							}
						},
						failure: function(form, action) {
							myMask.destroy();
							TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' +  ( action.result ? action.result.msg : ' Нет ответа от сервера' ) , 'winError');
						}
					});
				}
			} 	else {
					TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение "От" не может быть больше "До"', 'winError');
		 	}

		}
	},{
		text: 'Отмена',
		handler: function() {
			this.up('window').destroy();
		}
	}],

	initComponent: function() {
		this.callParent(arguments);

		if (this.data)
			this.down('form').loadRecord(this.data);
	}
});

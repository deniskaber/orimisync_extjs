Ext.define('TestOrimi.view.popUpthirdtab', {
	extend      : 'Ext.container.Container',
	xtype       : 'popUpthirdtab',
	border      : false,
	margin      : '20 20 0 20',
	layout      : 'column',
	items:  [{
		title       : '<p style="font-size: 16px;">Размер</p>',
		xtype       :'fieldset',
		columnWidth : 0.5,
		defaultType : 'textfield',
		defaults    : {anchor: '100%', labelWidth  : 160},
		border      : false,
		items: [{
			xtype       : 'numberfield',
			fieldLabel  : 'Длина:',
			name        : 'Length',
			minValue    : 0
		},{
			xtype       : 'numberfield',
			fieldLabel  : 'Ширина:',
			name        : 'Width',
			minValue    : 0
		},{
			xtype       : 'numberfield',
			fieldLabel  : 'Высота:',
			name        : 'Height',
			minValue    : 0
		},{
			fieldLabel  : 'Кофеин',
			xtype       : 'radiogroup',
			defaultType : 'radiofield',
			vertical    : true,
			columns     : 2,
			layout      : 'hbox',
			items: [{
				boxLabel    : 'с кофеином',
				name        : 'isCaffeine',
				inputValue  : 1
			},{
				boxLabel    : 'без кофеина',
				name        : 'isCaffeine',
				inputValue  : 0,
				margin      : '0 0 0 20'
			}]
		},{
			fieldLabel  : 'Ценовой сегмент:',
			name        : 'PriceSegmentID',
			xtype       : 'modalcombo',
			store       : 'TestOrimi.store.PriceSegmentStore'
		},{
			fieldLabel  : 'Страна происхождения:',
			name        : 'CountryID',
			xtype       : 'modalcombo',
			store       : 'TestOrimi.store.CountryStore'
		},{
			fieldLabel  : 'Сорт кофе:',
			name        : 'CoffeeSortID',
			xtype       : 'modalcombo',
			store       : 'TestOrimi.store.CoffeeSortStore'
		}]
	},{
		xtype       :'fieldset',
		defaultType : 'textfield',
		columnWidth : 0.5,
		defaults    : {anchor: '100%'},
		border      : false,
		title       : '<p style="font-size: 16px;">Другое</p>',
		items: [{
			xtype       : 'barcodePanel'
		},{
			fieldLabel  : 'Фотография:',
			name        : 'Photo',
			labelWidth  : 160
		},{
			xtype       : 'textareafield',
			grow        : true,
			name        : 'Comment',
			fieldLabel  : 'Комментарий',
			labelWidth  : 160
		}]
	}]
});

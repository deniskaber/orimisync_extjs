Ext.define('TestOrimi.view.general_tabpanelSecondcol', {
  extend      : 'Ext.grid.Panel',
  xtype       : 'general_tabpanelSecondcol',
  selType     : 'checkboxmodel',
  columnLines : true,

  initComponent: function() {
    var me = this,
        userRights = TestOrimi.getApplication().data;

    this.store = 'TestOrimi.store.SlaveGoodStore';

    this.columns  = [
      { text: 'Источник',             dataIndex: 'DataSource',    width: 100 },
      { text: 'Дата импорта',         dataIndex: 'DateChange',    width: 130 },
      { text: 'ID',                   dataIndex: 'ID',            width: 80  },
      { text: 'Код',                  dataIndex: 'Code',          width: 80  },
      { text: 'Код Орими',            dataIndex: 'OrimiCode',     width: 100 },
      { text: 'Название',             dataIndex: 'Name',          width: 250 },
      { text: 'Продукция ОТ',         dataIndex: 'From',          width: 150 },
      { text: 'Производитель',        dataIndex: 'Manufacturer',  width: 150 },
      { text: 'Бренд',                dataIndex: 'Brand',         width: 125 },
      { text: 'Категория',            dataIndex: 'Category',      width: 110 },
      { text: 'Сегмент',              dataIndex: 'Segment',       width: 80  },
      { text: 'Цвет чая',             dataIndex: 'TeaColor',      width: 120 },
      { text: 'Тип упаковки',         dataIndex: 'Pack',          width: 180 },
      { text: 'Количество пакетов',   dataIndex: 'BagQty',        width: 165 },
      { text: 'Вес пакета',           dataIndex: 'BagWeight',     width: 100 },
      { text: 'Вес',                  dataIndex: 'Weight',        width: 80  },
      //{ text: 'Аромат',               dataIndex: 'isWithFlavor',  width: 80  },
      //{ text: 'Добавка',              dataIndex: 'isWithAdding',  width: 80  },
      { text: 'Ценовой сегмент',      dataIndex: 'PriceSegment',  width: 150 },
      { text: 'Страна происхождения', dataIndex: 'Country',       width: 185 }
    ];

    if(userRights['Sku_slave_sync']) {
      Ext.Array.insert(me.columns, 0, [{
        text: 'Привязать',
        menuDisabled: true,
        menuText: 'Привязать',
        sortable: false,
        xtype: 'actioncolumn',
        width: 100,
        items: [{
          iconCls: 'sync-col',
          handler: function(view, rowIndex, colIndex) {
              me.fireEvent('syncbuttonclick', me, rowIndex, colIndex);
          }
        }]
      }]);
    }

    var i = 1,
      columns = this.columns,
      length = columns.length;

    for (;i<length;i++) {
      columns[i].items = [{
        xtype: 'container',
        margin: 4,
        flex: 1,
        layout: 'fit',
        listeners: {
            scope: this,
            element: 'el',
            mousedown: function(e)
            {
                e.stopPropagation();
            },
            click: function(e)
            {
                e.stopPropagation();
            },
            keydown: function(e){
                 e.stopPropagation();
            },
            keypress: function(e){
                 e.stopPropagation();
            },
            keyup: function(e){
                 e.stopPropagation();
            }
        },
        items:[{
          // xtype: 'trigger',
          // triggerCls: 'x-form-clear-trigger',
          xtype: 'textfield',
          //flex : 1,
          //margin: '0 5 5 5',
          emptyText : 'Поиск',
          enableKeyEvents: true,
          onTriggerClick: function () {
            this.reset();
            this.focus();
          },
          listeners: {
            change: function(field, newValue, oldValue, eOpts) {
              me.fireEvent('searchchange', me, field, newValue, oldValue);
            },
            buffer: 1000
          }
        }]
      }];
    }

    this.lbar = [{
      xtype: 'general_tabpanelsecLbar',
      ref: 'Sku',
      grid: this
    }];

    this.bbar = {
      xtype: 'pagingtoolbar',
      store: this.store,
      inputItemWidth: 60,
      displayInfo: true
    };

    this.callParent(arguments);
  }
});

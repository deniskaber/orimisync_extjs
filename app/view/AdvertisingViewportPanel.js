Ext.define('TestOrimi.view.AdvertisingViewportPanel', {
    extend: 'Ext.tab.Panel',
    requires: [
        'TestOrimi.view.AdvertisingBossPanel',
        'TestOrimi.view.AdvertisingEventsPanel'
    ],
    xtype: 'advertising_viewport_panel',
    flex: 1,
    header: false,
    plain: true,
    title: 'Подчиненные справочники',

    initComponent: function() {
        var me = this;
        var userRights = TestOrimi.getApplication().data;
        var buttons = [];

        me.items = buttons;

        if (userRights['Advertising_boss']) {
            buttons.push({xtype: 'advertising_boss_panel', title: 'Распоряжения'});
        }

        if (userRights['Advertising_events']) {
            buttons.push({xtype: 'advertising_events_panel', title: 'Акции'});
        }

        if (!buttons.length) {
            buttons.push({
                xtype: 'container',
                title: 'Ошибка',
                html: 'Нет доступных разделов. Обратитесь к администратору для получения соответствующих прав.'
            });
        }

        this.callParent(arguments);
    }

});

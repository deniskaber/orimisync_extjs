Ext.define('TestOrimi.view.createStylegroupSku', {
  extend  : 'Ext.window.Window',
  width   : 550,
  height  : 650,
  title	: 'Групповая установка свойств',
  modal	: true,
  layout	: 'fit',

  initComponent: function() {
	var myListener = {
		change: function(field, newValue, oldValue) {
			var fieldName = field.name.substring(8),
				win = field.up('window');
			
			if (newValue) {
				win.down('[name='+fieldName+']').enable();
			} else {
				win.down('[name='+fieldName+']').disable();
			}
		}
	}

	this.items = [{
		xtype   : 'form',
		padding	: 10,
		autoScroll: true,
    	border  : false,
		layout  : {
			type: 'column' 
		},
	    items: [{
	    	columnWidth: 0.05,
	    	defaults  : {
	    		xtype		: 'checkboxfield',
	    		listeners: myListener
	   	 	},
	    	items: [{
				inputValue 	: 'Code',
				name        : 'checkboxCode'
			},{
				inputValue 	: 'SubBrandID',
				name        : 'checkboxSubBrandID'
			},{
				inputValue 	: 'SubSegmentID',
				name        : 'checkboxSubSegmentID'
			},{
				inputValue 	: 'TeaColorID',
				name        : 'checkboxTeaColorID'
			},{
				inputValue 	: 'TempName',
				name        : 'checkboxTempName'
			},{
				inputValue 	: 'ParentID',
				name        : 'checkboxParentID'
			},{
				inputValue 	: 'ExternalPackID',
				name        : 'checkboxExternalPackID'
			},{
				inputValue 	: 'SachetPackID',
				name        : 'checkboxSachetPackID'
			},{
				inputValue 	: 'InternalPackID',
				name        : 'checkboxInternalPackID'
			},{
				inputValue 	: 'isHorekaPack',
				name        : 'checkboxisHorekaPackGroup'
			},{
				inputValue 	: 'BagQty',
				name        : 'checkboxBagQty'
			},{
				inputValue 	: 'BagWeight',
				name        : 'checkboxBagWeight'
			},{
				inputValue 	: 'isWithFlavor',
				name        : 'checkboxisWithFlavorGroup',
				margin		: '0 0 103 0'
			},{
				inputValue 	: 'isWithAdding',
				name        : 'checkboxisWithAddingGroup',
				margin		: '0 0 105 0'
			},{
				inputValue 	: 'WeightGroupID',
				name        : 'checkboxWeightGroupID'
			},{
				inputValue 	: 'PriceSegmentID',
				name        : 'checkboxPriceSegmentID'
			},{
				inputValue 	: 'CountryID',
				name        : 'checkboxCountryID'
			},{
				inputValue 	: 'CoffeeSortID',
				name        : 'checkboxCoffeeSortID'
			},{
				inputValue 	: 'isCaffeineGroup',
				name        : 'checkboxisCaffeineGroup'
			},{
				inputValue 	: 'GiftID',
				name        : 'checkboxGiftID'
			},{
				inputValue 	: 'GiftTypeID',
				name        : 'checkboxGiftTypeID'
			},{
				inputValue 	: 'Length',
				name        : 'checkboxLength'
			},{
				inputValue 	: 'Width',
				name        : 'checkboxWidth'
			},{
				inputValue 	: 'Height',
				name        : 'checkboxHeight'
			},{
				inputValue 	: 'isWeightNet',
				name        : 'checkboxisWeightNetGroup'
			},{
				inputValue 	: 'Collection',
				name        : 'checkboxCollection'
			},{
				inputValue 	: 'isPrivateMark',
				name        : 'checkboxisPrivateMarkGroup'
			},{
				inputValue 	: 'Photo',
				name        : 'checkboxPhoto'
			},{
				inputValue 	: 'Comment',
				name        : 'checkboxComment'
			}]
		},{
	    	columnWidth: 0.95,
	    	defaults  : {
	   	 		anchor: '90%', 
	   	 		labelWidth : 180
	   	 		,disabled : true   
	   	 	},
	   	 	layout  : {
				type: 'vbox' 
				,align: 'stretch'
			},
	    	items: [{
				xtype		: 'textfield',
				fieldLabel  : 'Код',
				name        : 'Code'
			},{
				fieldLabel  : 'Саббренд',
		 		name        : 'SubBrandID',
		 		afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
				attribute   : 'SubBrand',
				xtype       : 'modalcombo',
				store       : 'TestOrimi.store.SubBrandStore',
				allowBlank	: (!this.hasActiveStatus)	
			},{
				fieldLabel  : 'Сабсегмент',
				name        : 'SubSegmentID',
				afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
				attribute   : 'SubSegment',
				xtype       : 'modalcombo',
				store       : 'TestOrimi.store.SubSegmentStore',
				allowBlank	: (!this.hasActiveStatus)	
			},{
				fieldLabel  : 'Цвет чая',
				name        : 'TeaColorID',
				afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
				xtype       : 'modalcombo',
				store       : 'TestOrimi.store.TeaColorStore',
				allowBlank	: (!this.hasActiveStatus)	
			},{
				fieldLabel  : 'Черновое название',
				xtype		: 'textfield',
				name        : 'TempName'
			},{
				fieldLabel  : 'Агрегирующая позиция',
				name        : 'ParentID',
				attribute   : 'ParentID',
				xtype		: 'modalcombo',
				store		: 'TestOrimi.store.GoodParentStore'
			},{
				fieldLabel  : 'Тип упаковки внешней',
				name        : 'ExternalPackID',
				afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
				xtype       : 'modalcombo',
				store       : 'TestOrimi.store.ExternalPackStore',
				allowBlank	: (!this.hasActiveStatus)	
			},{
				fieldLabel  : 'Тип упаковки саше',
				name        : 'SachetPackID',
				afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
				xtype       : 'modalcombo',
				store       : 'TestOrimi.store.SachetPackStore',
				allowBlank	: (!this.hasActiveStatus)	
			},{
				fieldLabel  : 'Тип упаковки внутренней',
				name        : 'InternalPackID',
				afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
				xtype       : 'modalcombo',
				store       : 'TestOrimi.store.InternalPackStore',
				allowBlank	: (!this.hasActiveStatus)	
			},{
				fieldLabel  : 'Упаковка Хорека'
				,xtype		: 'radiogroup'
				,name		: 'isHorekaPackGroup'
				,afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>'
				,anchor		: 'none'
				,layout		: {
					autoFlex: false
				}
				,defaults: {
			        name	: 'isHorekaPack',
			        margin	: '0 15 0 0'
			    }
				,items: [{
					boxLabel    : 'с упаковкой',
					inputValue  : 1
				},{
					boxLabel    : 'без упаковки',
					inputValue  : 0
				}]
			},{
				fieldLabel  : 'Количество пакетов',
				name        : 'BagQty',
				afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
				xtype       : 'numberfield',
				allowBlank	: (!this.hasActiveStatus),
				minValue    : 0
			},{
				fieldLabel      : 'Вес пакета',
				name            : 'BagWeight',
				afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
				xtype           : 'numberfield',
				allowBlank	: (!this.hasActiveStatus),
	      		decimalSeparator: '.',
	      		allowDecimals	: true,
	      		step			: 0.1,
				minValue        : 0
			},{
	    		xtype: 'itemselectorFla',
	    		margin: '0 0 5 0'
	  		},{
	    		xtype: 'itemselectorAdd'
	  		},{
				fieldLabel  : 'Вес(группа)',
				margin      : '5 0 5 0',
	      		name        : 'WeightGroupID',
				xtype       : 'modalcombo',
				store       : 'TestOrimi.store.WeightGroupStore'
			},{
				fieldLabel  : 'Ценовой сегмент',
				name        : 'PriceSegmentID',
				xtype       : 'modalcombo',
				store       : 'TestOrimi.store.PriceSegmentStore'
			},{
				fieldLabel  : 'Страна происхождения',
				name        : 'CountryID',
				xtype       : 'modalcombo',
				store       : 'TestOrimi.store.CountryStore'
			},{
				fieldLabel  : 'Сорт кофе',
				name        : 'CoffeeSortID',
				xtype       : 'modalcombo',
				store       : 'TestOrimi.store.CoffeeSortStore'
			},{
				fieldLabel  : 'Кофеин',
				xtype       : 'radiogroup',
				name		: 'isCaffeineGroup',
				anchor		: 'none',
				layout		: {
				   autoFlex : false
				},
				defaults: {
	                 name	: 'isCaffeine',
	                 margin	: '0 15 0 0'
	               },
				items: [{
					boxLabel    : 'с кофеином',
					inputValue  : 1
				},{
					boxLabel    : 'без кофеина',
					inputValue  : 0
				}]
			},{
	      		fieldLabel  : 'Акционная позиция',
	      		name        : 'GiftID',
	      		afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
	      		xtype       : 'modalcombo',
	      		store       : 'TestOrimi.store.GiftStore',
	      		allowBlank	: (!this.hasActiveStatus)
	    	},{
				fieldLabel  : 'Тип подарка',
				name        : 'GiftTypeID',
				xtype       : 'modalcombo',
				store       : 'TestOrimi.store.GiftTypeStore'
			},{
				fieldLabel  : 'Длина',
				name        : 'Length',
				xtype       : 'numberfield',
				minValue    : 0
			},{
				fieldLabel  : 'Ширина',
				name        : 'Width',
				xtype       : 'numberfield',
				minValue    : 0
			},{
				fieldLabel  : 'Высота',
				name        : 'Height',
				xtype       : 'numberfield',
				minValue    : 0
			},{
				fieldLabel  : 'Весовой продукт в сети',
				xtype       : 'radiogroup',
				name		: 'isWeightNetGroup',
				anchor		: 'none',
				layout		: {
					autoFlex: false
				},
				defaults: {
	                    name	: 'isWeightNet',
	                    margin	: '0 15 0 0'
	               },
				items: [{
					boxLabel    : 'да',
					inputValue  : 1
				},{
					boxLabel    : 'нет',
					inputValue  : 0
				}]
			},{
				xtype		: 'textfield',
				fieldLabel  : 'Коллекция/Доп.имя',
				name        : 'Collection'
			},{
				fieldLabel  : 'Частная марка сети',
				xtype       : 'radiogroup',
				afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
				name		: 'isPrivateMarkGroup',
				anchor		: 'none',
				layout		: {
					autoFlex: false
				},
				defaults: {
					name	: 'isPrivateMark',
					margin	: '0 15 0 0'
				},
				items: [{
					boxLabel    : 'да',
					inputValue  : 1
				},{
					boxLabel    : 'нет',
					inputValue  : 0
				}]
			},{
				fieldLabel  : 'Фотография:',
				xtype       : 'textfield',
				name        : 'Photo'
			},{
				xtype       : 'textareafield',
				grow        : true,
				name        : 'Comment',
				fieldLabel  : 'Комментарий'
			}]
		}]
	}];

	this.buttons = [{
		text	: 'Ок',
		handler: function() {
			var store = Ext.getStore('TestOrimi.store.MasterGoodStore'),
    			win = this.up('window'),
    			form = win.down('form').getForm();

		//get cleared form values
			var rawVals = form.getValues(),
				radiogroups = ['isHorekaPack','isWithAdding','isWithFlavor','isCaffeine','isWeightNet','isPrivateMark'],
				params = {};

			Ext.Object.each(rawVals, function(key, value, myself) {
				/*if (Ext.Array.contains(radiogroups,key)) {
	            	params[key] = value;
	        	} else {
	          		if (value) {
  						params[key] = value;
	  				}
		        }*/
		        params[key] = value;
			});
		//

			var formdown = win.down('form'),
				addingRecords = formdown.down('[name=adding]').store.data.items,
				flavorRecords = formdown.down('[name=flavor]').store.data.items,
				lengthAdd = addingRecords.length,
				lengthFla = flavorRecords.length,
				addings = [],
				flavors = [];

			if (formdown.down('itemselectorAdd').down('radiogroup').getValue()['isWithAdding']) {
				params['Adding'] = [];
				for (i = 0; i < lengthAdd; i++) {
					params['Adding'].push(addingRecords[i].get('ID'));
				}
				params['Adding'] = params['Adding'].toString();
			}

			if (formdown.down('itemselectorFla').down('radiogroup').getValue()['isWithFlavor']) {
				params['Flavor'] = [];
				for (i = 0; i < lengthFla; i++) {
					params['Flavor'].push(flavorRecords[i].get('ID'));
				}
				params['Flavor'] = params['Flavor'].toString();
			}


			params['ID'] = win.records.toString();
			params['act'] = store.proxy.extraParams['act'];
			params['subaction'] = 'groupUpdate';

			if (form.isValid()) {
				var myMask = new Ext.LoadMask(win, {msg:"Сохраняю..."});
				
				myMask.show();
				Ext.Ajax.request({
					url: 'resources/data/api.php',
					params: params,
					success: function(response) {
						myMask.destroy();
						TestOrimi.getApplication().getController('Main').showNotification('Успешно! Элементы обновлены', 'winSuccess');
						store.load();
						win.destroy();
					},
					failure: function(form, action) {
						myMask.destroy();
						TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Нет ответа от сервера', 'winError');
					}
				});
			}
		}
	},{
		text	: 'Отмена',
		handler: function() {
			this.up('window').destroy();
		}
	}];

	this.callParent(arguments);
  }
});
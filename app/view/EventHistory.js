Ext.define('TestOrimi.view.EventHistory', {
    extend      : 'Ext.window.Window',  
	xtype		: 'eventHistory',
	width       : 600,
    height      : 525,
    title		: 'Просмотр истории работы с позицией', 
	modal		: true,
	layout		: 'fit',
	
	initComponent: function() {
    	var me = this;
        
        me.items = [{   
            xtype        : 'grid'
            ,flex        : 1
            ,border      : true
            ,columnLines : true
            ,columns: [
				{ text: 'Дата и время', dataIndex: 'Date', width: 130 }, 
				{ text: 'Событие',		dataIndex: 'Event', flex:1 }, 
				{ text: 'Автор',      	dataIndex: 'Author', width: 150 }
			]
            ,store: 'TestOrimi.store.EventHistoryStore'
        }];

		me.buttons = [{
			text     : 'Ок',
			handler: function() {
                this.up('window').destroy();
			}
		}];
	
		this.callParent(arguments);
	}
});

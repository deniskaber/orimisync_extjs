Ext.define('TestOrimi.view.StoreModalWindow', {
    extend: 'Ext.window.Window',
    xtype: 'storeModalWindow',
    width: 950,
    height: 525,
    title: 'Торговые точки',
    modal: true,
    layout: 'fit',

    initComponent: function () {
        var me = this,
            field = me.field || me.button;

        me.items = [{
            id: 'storeModalWindowGrid'
            , xtype: 'grid'
            , flex: 1
            , border: true
            , columnLines: true
            , columns: [
                {text: 'ID', dataIndex: 'ID', width: 80},
                {text: 'Код', dataIndex: 'Code', width: 130, hidden: true},
                {text: 'Название', dataIndex: 'Name', width: 200},
                {text: 'Черновой адрес', dataIndex: 'TempAddress', width: 180},
                {text: 'Индекс', dataIndex: 'PostCode', width: 100, hidden: true},
                {text: 'Округ', dataIndex: 'County', width: 110, hidden: true},
                {text: 'Регион', dataIndex: 'Region', width: 110, hidden: true},
                {text: 'Район', dataIndex: 'District', width: 120, hidden: true},
                {text: 'Город', dataIndex: 'City', width: 130, hidden: true},
                {text: 'Улица', dataIndex: 'Street', width: 120, hidden: true},
                {text: 'Дом', dataIndex: 'Building', width: 80, hidden: true},
                {text: 'Полный адрес', dataIndex: 'FullAddress', width: 150},
                {text: 'Адрес', dataIndex: 'Address', width: 140, hidden: true},
                {text: 'Дивизион', dataIndex: 'Division', width: 100},
                {text: 'Участок', dataIndex: 'Area', width: 100},
                {text: 'Сектор', dataIndex: 'Sector', width: 100},
                {text: 'Канал', dataIndex: 'Channel', width: 100},
                {text: 'Подканал', dataIndex: 'SubChannel', width: 100},
                {text: 'Формат', dataIndex: 'Format', width: 100},
                {text: 'Сеть', dataIndex: 'isNetName', width: 100},
                {text: 'Название сети', dataIndex: 'Net', width: 120},
                {text: 'Тип сети', dataIndex: 'NetType', width: 110},
                {text: 'Отв. менеджер', dataIndex: 'RespManager', width: 180},
                {text: 'КАИ Nielsen', dataIndex: 'isNielsen', width: 110},
                {text: 'Грузополучатель ОРИМИ', dataIndex: 'isConsignee', width: 150},
                {text: 'Группа контрагентов', dataIndex: 'ContractorGroup', width: 220},
                {text: 'Дата открытия ТТ', dataIndex: 'DateOpen', width: 160},
                {text: 'Дата закрытия ТТ', dataIndex: 'DateClose', width: 160},
                {text: 'Площадь ТТ', dataIndex: 'ShopArea', width: 120, hidden: true},
                {text: 'Координаты', dataIndex: 'Coordinates', width: 120, hidden: true},
                {text: 'GLN', dataIndex: 'GLN', width: 100, hidden: true},
                {text: 'Комментарий', dataIndex: 'Comment', width: 120, hidden: true},
                {text: 'Страна', dataIndex: 'Country', width: 120, hidden: true},
                {text: 'ФИАС', dataIndex: 'FIAS', width: 100, hidden: true},
                {text: 'Участвует в РБП', dataIndex: 'isInRBP', width: 150, hidden: true},
                {text: 'Отв. мерчандайзер', dataIndex: 'RespMerch', width: 150, hidden: true},
                {text: 'Отв. ТП', dataIndex: 'RespTR', width: 170},
                {text: 'Канал для рекламы', dataIndex: 'AdvertisingChannel', width: 200, hidden: true},
                {text: 'Выборка', dataIndex: 'Selection', width: 170},
                {text: 'Автор изменений', dataIndex: 'Author', width: 170},
                {text: 'Дата последнего изменения', dataIndex: 'DateChange', width: 200, hidden: true},
                {text: 'Источник данных', dataIndex: 'DataSource', width: 170}
            ]
            , store: field.store
            , listeners: {
                celldblclick: function (grid, td, cellIndex, record, tr, rowIndex) {
                    me.down('[name=okButton]').handler();
                }
            },
            bbar: {
                xtype: 'pagingtoolbar',
                store: field.store,
                displayInfo: true
            }
        }];

        var i = 0,
            columns = this.items[0].columns,
            length = columns.length;

        for (; i < length; i++) {
            columns[i].items = [{
                xtype: 'container',
                margin: 4,
                flex: 1,
                layout: 'fit',
                listeners: {
                    scope: this,
                    element: 'el',
                    mousedown: function (e) {
                        e.stopPropagation();
                    },
                    click: function (e) {
                        e.stopPropagation();
                    },
                    keydown: function (e) {
                        e.stopPropagation();
                    },
                    keypress: function (e) {
                        e.stopPropagation();
                    },
                    keyup: function (e) {
                        e.stopPropagation();
                    }
                },
                items: [{
                    xtype: 'textfield',
                    emptyText: 'Поиск',
                    enableKeyEvents: true,
                    onTriggerClick: function () {
                        this.reset();
                        this.focus();
                    },
                    listeners: {
                        change: function (field, newValue, oldValue, eOpts) {
                            var grid = this.up('grid');
                            me.fireEvent('searchchange', grid, field, newValue, oldValue);
                        },
                        buffer: 1000
                    }
                }]
            }];
        }

        me.buttons = [{
            text: 'Ок',
            name: 'okButton',
            handler: function () {
                var win = this.up('window'),
                    selected = win.down('grid').getSelectionModel().getSelection()[0];

                if (!selected) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
                } else {
                    var fieldName = me.field.name;
                    if (fieldName.substr(-2) == 'ID') {
                        if (field.value != selected.get('ID')) {
                            win.field.setValue(selected.get('ID'));
                            if (win.field.queryFilter)
                                win.field.queryFilter.setValue('');
                        }
                    } else {
                        if (field.value != selected.get('Name')) {
                            win.field.setValue(selected.get('Name'));
                        }
                    }
                    win.destroy();
                }
            }
        }, {
            text: 'Отмена',
            handler: function () {
                this.up('window').destroy();
            }
        }];

        this.callParent(arguments);
    }
});

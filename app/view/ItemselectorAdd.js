Ext.define('TestOrimi.view.ItemselectorAdd', {
    extend: 'Ext.container.Container',
    xtype: 'itemselectorAdd',
    name: 'isWithAddingGroup',
    border: false,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    width: 100,
    items: [{
        fieldLabel: 'Добавка',
        xtype: 'radiogroup',
        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
        anchor: 'none',
        layout: {
            autoFlex: false
        },
        defaults: {
            name: 'isWithAdding',
            margin: '0 15 0 0'
        },
        items: [{
            boxLabel: 'с добавкой',
            inputValue: 1
        }, {
            boxLabel: 'без добавки',
            inputValue: 0
        }],
        listeners: {
            change: function (field, newValue, oldValue) {
                if (newValue['isWithAdding']) {
                    field.up('itemselectorAdd').down('[name=addingSelectors]').enable();
                } else {
                    field.up('itemselectorAdd').down('[name=addingSelectors]').disable();
                }
            }
        }
    }, {
        xtype: 'container',
        name: 'addingSelectors',
        disabled: true,
        layout: {
            type: 'hbox',
            align: 'stretch',
            height: 100
        },
        items: [{
            xtype: 'buttongroup',
            name: 'AddingButtonGgroup',
            frame: false,
            columns: 1,
            border: false,
            items: [{
                xtype: 'button',
                text: '<span class="oi oiadd" data-glyph="plus" aria-hidden="true"></span>',
                height: 37,
                tooltip: 'Добавить',
                scale: 'medium',
                handler: function () {
                    Ext.create('TestOrimi.view.AddingWindow').show();
                }
            }, {
                xtype: 'button',
                margin: '5 0 0 0',
                height: 37,
                text: '<span class="oi oiadd" data-glyph="trash" aria-hidden="true"></span>',
                tooltip: 'Удалить',
                scale: 'medium',
                handler: function () {
                    var container = this.up('itemselectorAdd'),
                        listadd = container.down('boundlist'),
                        selected = listadd.getSelectionModel().getSelection()[0];

                    if (selected) {
                        Ext.getStore('TestOrimi.store.SelectedAddingStore').remove(selected);
                    } else {
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Добавка не выделена', 'winError');
                    }
                }
            }]
        }, {
            xtype: 'boundlist',
            anchor: '100%',
            attribute: 'AddingID',
            margin: '0 0 0 20',
            height: 92,
            flex: 1,
            displayField: 'Name',
            valueField: 'ID',
            store: 'TestOrimi.store.SelectedAddingStore',
            name: 'adding',
            queryMode: 'local',
            multiSelect: true
        }]
    }]
});

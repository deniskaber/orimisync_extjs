Ext.define('TestOrimi.view.WeightGroupID', {
  extend  : 'Ext.window.Window',
	xtype		: 'WeightGroupID',
	width   : 950,
  height  : 525,
  title		: 'Вес группа',
	modal		: true,
	layout	: 'fit',

  initComponent: function() {
  	var me = this,
        field = me.field;

    me.items = [{
      xtype        : 'grid'
      ,viewConfig: {
        stripeRows: false,
        getRowClass: function(record) {
          return record.get('Deleted') == 1 ? 'disabled-row' : '';
        },
        shrinkWrap: 0,
        shadow: false,
        trackOver: false,
        overItemCls: false
      }
      ,flex        : 1
      ,border      : true
      ,columnLines : true
      ,columns: [
         { text: 'ID',                  dataIndex: 'ID',                width: 60  }
        ,{ text: 'Наименование',        dataIndex: 'Name',              width: 250 }
        ,{ text: 'Краткое наим.',       dataIndex: 'ShortName',         width: 150 }
        ,{ text: 'Краткое обозн.',      dataIndex: 'BriefNotation',     width: 150 }
        ,{ text: 'Вес от',              dataIndex: 'Begin',             width: 120 }
        ,{ text: 'Вес до',              dataIndex: 'End',               width: 120 }
        ,{ text: 'Удален'
          , dataIndex: 'Deleted'
          , width: 90
          , renderer: function (val, meta, record) {
            if (val == 1) {
              return '+';
            }
          }
        }
      ]
      ,store: field.store
      ,listeners: {
        celldblclick: function(grid, td, cellIndex, record, tr, rowIndex) {
          me.down('[name=okButton]').handler();
        }
      }
    }];

  	me.buttons = [{
      text: 'Редактировать',
      handler: function() {
        var win = this.up('window'),
          selected = win.down('grid').getSelectionModel().getSelection()[0];

        if (!selected) {
          TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
        } else {
          Ext.create('TestOrimi.view.AddWeightGroupIDWindow',{
            title: 'Редактирование элемента'
            ,store: field.store
            ,data: selected
          }).show();
        }
      }
    },{
      text: 'Создать',
      handler: function() {
        Ext.create('TestOrimi.view.AddWeightGroupIDWindow',{
          title: 'Создание нового элемента'
          ,store: field.store
        }).show();
      }
    },'->',{
  		text     : 'Ок',
      name: 'okButton',
      handler: function() {
        var win = this.up('window'),
            selected = win.down('grid').getSelectionModel().getSelection()[0];

        if (!selected) {
          TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
        } else {
          if (selected.get('Deleted') == 1) {
                  TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение помечено на удаление', 'winError');
          } else {
            var fieldName = me.field.name;
            if (fieldName.substr(-2) == 'ID') {
              if (field.value != selected.get('ID')) {
                win.field.setValue(selected.get('ID'));
              }
            } else {
              if (field.value != selected.get('Name')) {
                win.field.setValue(selected.get('Name'));
              }
            }
            win.destroy();
          }
        }
      }
  	},{
  		text     : 'Отмена',
  		handler: function() {
        this.up('window').destroy();
      }
  	}];

    var i = 0,
      columns = this.items[0].columns,
      length = columns.length;

    for (;i<length;i++) {
      columns[i].items = [{
        xtype: 'container',
        margin: 4,
        flex: 1,
        layout: 'fit',
        listeners: {
          scope: this,
          element: 'el',
          mousedown: function(e)
          {
              e.stopPropagation();
          },
          click: function(e)
          {
              e.stopPropagation();
          },
          keydown: function(e){
               e.stopPropagation();
          },
          keypress: function(e){
               e.stopPropagation();
          },
          keyup: function(e){
               e.stopPropagation();
          }
        },
        items:[{
          xtype: 'textfield',
          emptyText : 'Поиск',
          enableKeyEvents: true,
          onTriggerClick: function() {
            this.reset();
            this.focus();
          },
          listeners: {
            change: function(field, newValue, oldValue, eOpts) {
              var grid = this.up('grid');
              me.fireEvent('searchchange', grid, field, newValue, oldValue);
            },
            buffer: 1000
          }
        }]
      }];
    }

	  this.callParent(arguments);
	}
});
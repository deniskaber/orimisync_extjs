Ext.define('TestOrimi.view.AdvertisingBossHeadersGrid', {
    extend: 'Ext.grid.Panel',
    requires: ['TestOrimi.store.AdvertisingBossHeadersStore'],
    xtype: 'advertising_boss_headers_grid',
    flex: 1,
    columnLines: true,
    border: false,
    viewConfig: {
        stripeRows: false,
        shrinkWrap: 0,
        shadow: false,
        trackOver: false,
        overItemCls: false
    },
    plugins: [{
        ptype: 'bufferedrenderer',
        trailingBufferZone: 0,
        leadingBufferZone: 0
    }],
    initComponent: function () {
        var me = this;

        this.store = 'TestOrimi.store.AdvertisingBossHeadersStore';

        this.columns = [
            {text: 'Статус', dataIndex: 'Status', width: 160},
            {text: 'ID', dataIndex: 'ID', width: 80, hidden: true},
            {text: 'Номер', dataIndex: 'DocNum', width: 130},
            {text: 'Дата начала', dataIndex: 'DateBegin', width: 130},
            {text: 'Дата окончания', dataIndex: 'DateEnd', width: 130},
            {text: 'Заголовок', dataIndex: 'Title', flex: 1},
            {text: 'Автор изменений', dataIndex: 'Author', width: 170},
            {text: 'Дата посл. изменения', dataIndex: 'DateChange', width: 200}
        ];

        var i = 0,
            columns = this.columns,
            length = columns.length;

        for (; i < length; i++) {
            columns[i].items = [{
                xtype: 'container',
                margin: 4,
                flex: 1,
                layout: 'fit',
                listeners: {
                    scope: this,
                    element: 'el',
                    mousedown: function (e) {
                        e.stopPropagation();
                    },
                    click: function (e) {
                        e.stopPropagation();
                    },
                    keydown: function (e) {
                        e.stopPropagation();
                    },
                    keypress: function (e) {
                        e.stopPropagation();
                    },
                    keyup: function (e) {
                        e.stopPropagation();
                    }
                },
                items: [{
                    xtype: 'textfield',
                    emptyText: 'Поиск',
                    enableKeyEvents: true,
                    onTriggerClick: function () {
                        this.reset();
                        this.focus();
                    },
                    listeners: {
                        change: function (field, newValue, oldValue, eOpts) {
                            me.fireEvent('searchchange', me, field, newValue, oldValue);
                        },
                        buffer: 1000
                    }
                }]
            }];
        }

        // this.lbar = [{
        //     xtype: 'general_gridLbar',
        //     ref: 'TT',
        //     grid: this
        // }];

        this.bbar = {
            xtype: 'pagingtoolbar',
            store: this.store,
            inputItemWidth: 60,
            displayInfo: true
        };

        this.callParent(arguments);

    }
});

Ext.define('TestOrimi.view.barcodePanel', {
  extend  : 'Ext.container.Container',
  xtype   : 'barcodePanel',
  border  : false,
  layout  : {
    type  : 'hbox',
    align : 'stretch'
  },
  height  : 100,
  margin  : '0 0 10 0',
  items: [{
    xtype: 'container',
    width: 150,
    layout: 'vbox',
    items: [{
      xtype   : 'container',
      html    : 'Штрихкод:',
      height: 20,
      margin: '0 0 10 0'
    },{
      xtype: 'buttongroup',
      cls: 'barcodeButtonGroup',
      frame : false,
      name: 'barcodeButtonGroup',
      columns: 2,
      border: false,
      items:[{
        xtype   : 'button',
        text      : '<span class="oi oiadd" data-glyph="plus" aria-hidden="true"></span>',
        height    : 37,
        tooltip   : 'Добавить',
        scale     : 'medium',
        handler: function() {
          var container = this.up('barcodePanel');

          container.fireEvent('barcodeaddclick', container);
        }
      },{
        xtype   : 'button',
        margin    : '0 0 0 8',
        height    : 37,
        text      : '<span class="oi oiadd" data-glyph="trash" aria-hidden="true"></span>',
        tooltip   : 'Удалить',
        scale     : 'medium',
        handler: function() {
          var container = this.up('barcodePanel'),
              list = container.down('boundlist');

          container.fireEvent('barcodedeleteclick', list);
        }
      }]
    }]
  },{
    margin      : '0 0 0 16',
    xtype       : 'boundlist',
    anchor      : '100%',
    height      : 98,
    trackOver   : false,
    flex        : 1,
    displayField: 'ID',
    valueField  : 'ID',
    store       : 'TestOrimi.store.BarcodeStore',
    name        : 'barcode',
    queryMode   : 'local',
    multiSelect : true,
    listeners   : {
      itemdblclick: function(list, record, item, index, e) {
        TestOrimi.getApplication().getController('Main').showNotification(record.get('ID'), 'winInformation');
      }
    }
  }]
});

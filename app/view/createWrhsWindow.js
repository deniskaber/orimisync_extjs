Ext.define('TestOrimi.view.createWrhsWindow', {
    extend: 'Ext.window.Window',
    xtype: 'createWrhsWindow',
    name: 'WrhsWindow',
    width: 1000,
    height: 700,
    title: 'Склады',
    modal: true,
    layout: 'fit',
    items: [{
        xtype: 'form',
        bodyPadding: 10,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'container',
            border: false,
            layout: 'column',
            items: [{
                xtype: 'fieldset',
                columnWidth: 0.5,
                border: false,
                defaults: {
                    xtype: 'textfield'
                    , anchor: '100%'
                    , labelWidth: 160
                },
                items: [{
                    cls: 'noteditable',
                    name: 'ID',
                    fieldLabel: 'ID',
                    readOnly: true
                }, {
                    fieldLabel: 'Код',
                    name: 'Code'
                }, {
                    cls: 'noteditable',
                    xtype: 'textareafield',
                    name: 'FullAddress',
                    fieldLabel: 'Полный адрес',
                    readOnly: true,
                    anchor: '100%'
                }]
            }, {
                columnWidth: 0.5,
                xtype: 'fieldset',
                border: false,
                defaults: {
                    xtype: 'textfield'
                    , anchor: '100%'
                    , labelWidth: 160
                },
                items: [{
                    fieldLabel: 'Название',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    name: 'Name',
                    maxLength: 50,
                    maxLengthText: 'Максимальная длина поля составляет {0} симв.'
                }, {
                    fieldLabel: 'Черновой адрес',
                    name: 'TempAddress'
                }, {
                    cls: 'noteditable',
                    xtype: 'textareafield',
                    name: 'Address',
                    fieldLabel: 'Адрес',
                    readOnly: true
                }]
            }]
        }, { // ------ нижняя часть окна ------

            xtype: 'container',
            border: false,
            layout: 'column',
            items: [{
                xtype: 'fieldset',
                defaultType: 'textfield',
                columnWidth: 0.5,
                margin: '20 0 0 0',
                defaults: {anchor: '100%', labelWidth: 160},
                border: false,
                items: [{
                    fieldLabel: 'Индекс',
                    //margin: '87 0 5 0',
                    xtype: 'numberfield',
                    minValue: 0,
                    name: 'PostCode'
                }, {
                    fieldLabel: 'Округ',
                    name: 'CountyID',
                    attribute: 'County',
                    readOnly: true,
                    cls: 'noteditable',
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.CountyStore'
                }, {
                    fieldLabel: 'Район',
                    name: 'DistrictID',
                    attribute: 'District',
                    readOnly: true,
                    cls: 'noteditable',
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.DistrictStore'
                }, {
                    fieldLabel: 'Улица',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    name: 'Street'
                }, {
                    fieldLabel: 'Дивизион',
                    name: 'DivisionID',
                    attribute: 'Division',
                    margin: '25 0 5 0',
                    xtype: 'modalcombo',
                    readOnly: true,
                    cls: 'noteditable',
                    store: 'TestOrimi.store.DivisionStore'
                }, {
                    fieldLabel: 'Участок',
                    name: 'AreaID',
                    attribute: 'Area',
                    xtype: 'modalcombo',
                    readOnly: true,
                    cls: 'noteditable',
                    store: 'TestOrimi.store.AreaStore'
                }, {
                    fieldLabel: 'Сектор',
                    name: 'SectorID',
                    attribute: 'Sector',
                    xtype: 'modalcombo',
                    readOnly: true,
                    cls: 'noteditable',
                    store: 'TestOrimi.store.SectorStore'
                }, {
                    xtype: 'datefield',
                    fieldLabel: 'Дата открытия',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    format: 'Y-m-d',
                    name: 'DateOpen'
                }, {
                    fieldLabel: 'Площадь',
                    xtype: 'numberfield',
                    minValue: 0,
                    name: 'ShopArea'
                }, {
                    fieldLabel: 'Формат',
                    name: 'FormatID',
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.FormatStore'
                }]
            }, {
                xtype: 'fieldset',
                defaultType: 'textfield',
                columnWidth: 0.5,
                margin: '20 0 0 0',
                defaults: {anchor: '100%', labelWidth: 160},
                border: false,
                items: [{
                    fieldLabel: 'Страна',
                    name: 'CountryID',
                    xtype: 'modalcombo',
                    readOnly: true,
                    cls: 'noteditable',
                    store: 'TestOrimi.store.CountryStore'
                }, {
                    fieldLabel: 'Регион',
                    name: 'RegionID',
                    attribute: 'Region',
                    xtype: 'modalcombo',
                    readOnly: true,
                    cls: 'noteditable',
                    store: 'TestOrimi.store.RegionStore'
                }, {
                    fieldLabel: 'Город',
                    name: 'CityID',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    attribute: 'City',
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.CityStore'
                }, {
                    fieldLabel: 'Дом',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    name: 'Building'
                }, {
                    fieldLabel: 'Дистрибьютор',
                    margin: '25 0 5 0',
                    name: 'DistributorID',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    attribute: 'Distributor',
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.DistributorsStore'
                }, {
                    fieldLabel: 'Код дистрибьютора',
                    name: 'DistrCode',
                    readOnly: true,
                    cls: 'noteditable'
                }, {
                    fieldLabel: 'Группа контрагентов',
                    name: 'ContractorGroup',
                    readOnly: true,
                    cls: 'noteditable'
                }, {
                    xtype: 'datefield',                    
                    fieldLabel: 'Дата закрытия',
                    format: 'Y-m-d',
                    name: 'DateClose'
                }, {
                    fieldLabel: 'ФИАС',
                    name: 'FIAS'
                }, {
                    xtype: 'textareafield',
                    grow: true,
                    name: 'Comment',
                    fieldLabel: 'Комментарий'
                }]
            }]
        }]
    }],

    buttons: ['->', {
        text: 'Ок',
        name: 'okButton',
        handler: function () {
            var store = Ext.getStore('TestOrimi.store.MasterWarehousesStore'),
                win = this.up('window'),
                form = win.down('form').getForm();

            //console.dir(form.getValues());

            if (form.isValid()) {
                var myMask = new Ext.LoadMask(win, {msg: "Сохраняю..."});
                myMask.show();
                form.submit({
                    url: 'resources/data/api.php',
                    params: {
                        act: store.proxy.extraParams['act'],
                        subaction: (win.data) ? (win.data.get('ID') ? 'update' : 'create') : 'create',
                        DataSourceID: (win.data) ? win.data.get('DataSourceID') : 0
                    },
                    success: function (response) {
                        myMask.destroy();
                        if (win.data) {
                            if (win.data.get('ID') == 0) {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess');
                            } else {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Элемент обновлен', 'winSuccess');
                            }
                        } else {
                            TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess');
                        }
                        store.load();
                        win.destroy();
                    },
                    failure: function (form, action) {
                        myMask.destroy();
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                    }
                });
            } else {
                TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Не все поля заполнены корректно. Проверьте все вкладки', 'winError');
                console.dir(form.getValues());
            }
        }
    }, {
        text: 'Отмена',
        handler: function () {
            this.up('window').destroy();
        }
    }],

    initComponent: function () {
        var me = this,
            userRights = TestOrimi.getApplication().data;

        this.callParent(arguments);

        var form = this.down('form');

        if (this.data) {
            form.loadRecord(this.data);

            if (this.data.get('ID') == 0) {
                form.down('[name=ID]').reset();
            } else {
                if (this.data.get('isNet') == 2) {
                    var rg = this.down('form').down('[name=isNetRadioGroup]');

                    rg.items.items[0].disable();
                    rg.items.items[1].disable();
                }
            }

            if (this.data.get('StatusID') && this.data.get('StatusID') != 1) {
                me.down('[name=Code]').setReadOnly(true);
                me.down('[name=Code]').addCls('noteditable');
            }

            /*удаленная позиция или редактирование отключено в правах*/
            if (this.data.get('StatusID') == 3 ||
                (this.data.get('StatusID') == 1 && !userRights['Wrhs_draft_set']) ||
                (this.data.get('StatusID') == 2 && !userRights['Wrhs_active_set'])) {

                var fields = form.query('textfield');
                var mcombos = form.query('modalcombo');
                var radios = form.query('radio');

                form.addCls('noteditable');

                for (var i = 0; i < mcombos.length; i++) {
                    mcombos[i].readOnly = true;
                    mcombos[i].hideTrigger = true;
                }

                for (var i = 0; i < fields.length; i++) {
                    fields[i].readOnly = true;
                }

                for (var i = 0; i < radios.length; i++) {
                    radios[i].readOnly = true;
                }

                //----//

                this.down('[name=okButton]').disable();

            } else if (this.data.get('StatusID') == 2 && userRights['custom'] == 1) {
                /*предустановленная роль*/
                var fields = form.query('textfield');
                var mcombos = form.query('modalcombo');
                var radios = form.query('radio');

                for (var i = 0; i < fields.length; i++) {
                    if (!Ext.Array.contains(['TempAddress', 'DateClose', 'ShopArea', 'Coordinates', 'GLN', 'Comment', 'FormatID', 'RespTRID'], fields[i].name)) {
                        if (fields[i].name === 'Name') {
                            fields[i].maxLength = 999;
                        }

                        fields[i].readOnly = true;
                        fields[i].addCls('noteditable');
                    }
                }

                for (var i = 0; i < mcombos.length; i++) {
                    if (!Ext.Array.contains(['FormatID', 'RespTRID'], mcombos[i].name)) {
                        mcombos[i].addCls('noteditable');
                        mcombos[i].readOnly = true;
                        mcombos[i].hideTrigger = true;
                    }
                }

                for (var i = 0; i < radios.length; i++) {
                    if (radios[i].name != 'isInRBP') {
                        radios[i].up('radiogroup').addCls('noteditable');
                        radios[i].readOnly = true;
                    }
                }
            }

            if (userRights['custom'] == 1 && userRights['Wrhs_isConsignee_1'] == 0) {
                var isCons = form.down('[name=isCons]');
                isCons.addCls('noteditable');
                var isConsRadios = isCons.query('radio');
                for (var i = 0; i < isConsRadios.length; i++) {
                    isConsRadios[i].readOnly = true;
                }

                var ContractorGroupID = form.down('[name=ContractorGroupID]');
                ContractorGroupID.addCls('noteditable');
                ContractorGroupID.readOnly = true;
                ContractorGroupID.hideTrigger = true;
            }

        }
    }
});

Ext.define('TestOrimi.view.AdvertisingEventsPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'advertising_events_panel',
    flex: 1,
    border: false,
    layout: 'border',
    items: [
        {
            xtype: 'advertising_events_headers_grid',
            region: 'center'
        }, {
            xtype: 'advertising_events_rows_grid',
            region: 'south',
            split: true,
            collapsible: true,
            header: false
        }
    ],
    loaded: false
});

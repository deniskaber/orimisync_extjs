Ext.define('TestOrimi.view.general_tabpanelSecondcolStaff', {
    extend      : 'Ext.grid.Panel',
    xtype       : 'general_tabpanelSecondcolStaff',
    selType     : 'checkboxmodel',
    columnLines : true,

    initComponent: function() {
        var me = this,
            userRights = TestOrimi.getApplication().data;

        this.store = 'TestOrimi.store.SlaveStaffStore';

        this.columns  =  [
          { text: 'Источник',                     dataIndex: 'DataSource',    width: 100 },
          { text: 'Дата импорта',                 dataIndex: 'DateChange',    width: 125 },  
          { text: 'ID',                           dataIndex: 'ID',            width: 80  },
          { text: 'Код',                          dataIndex: 'Code',          width: 130 },
          { text: 'Код Дистр.',                   dataIndex: 'DistrCode',     width: 130 },
          { text: 'ФИО',                          dataIndex: 'FIO',           width: 190 },
          { text: 'Тип',                          dataIndex: 'Type',          width: 110 },
          { text: 'Кому подчиняется',             dataIndex: 'Parent',        width: 190 },
          { text: 'Принадлежность к дивизиону',   dataIndex: 'Division',      width: 200 },
          { text: 'Участок',                      dataIndex: 'Sector',        width: 120 },
          { text: 'Дата рождения',                dataIndex: 'BirthDate',     width: 150 },
          { text: 'Дата начала работы',           dataIndex: 'DateHired',     width: 195 },
          { text: 'Дата увольнения',              dataIndex: 'DateFired',     width: 195 },
          { text: 'E-mail',                       dataIndex: 'EMail',         width: 150 },
          { text: 'Телефон',                      dataIndex: 'Phone',         width: 130 },
          { text: 'Мотивация',                    dataIndex: 'Motivation',    width: 120 },
          { text: 'Признак',                      dataIndex: 'Feature',       width: 120 }
        ];

        if(userRights['Staff_slave_sync']) {
          Ext.Array.insert(me.columns, 0, [{
            text: 'Привязать',
            menuDisabled: true,
            menuText: 'Привязать',
            sortable: false,
            xtype: 'actioncolumn',
            width: 100,
            items: [{
              iconCls: 'sync-col',
              handler: function(view, rowIndex, colIndex) {
                  me.fireEvent('syncbuttonclick', me, rowIndex, colIndex);
              }
            }]
          }]);
        }

        var i = 1,
            columns = this.columns,
            length = columns.length;

        for (;i<length;i++) {
            columns[i].items = [{
            xtype: 'container',
            margin: 4,
            flex: 1,
            layout: 'fit',
            listeners: {
                scope: this,
                element: 'el',
                mousedown: function(e)
                {
                    e.stopPropagation();
                },
                click: function(e)
                {
                    e.stopPropagation();
                },
                keydown: function(e){
                     e.stopPropagation();
                },
                keypress: function(e){
                     e.stopPropagation();
                },
                keyup: function(e){
                     e.stopPropagation();
                }
            },
            items:[{
              // xtype: 'trigger',
              // triggerCls: 'x-form-clear-trigger',
              xtype: 'textfield',
              //flex : 1,
              //margin: '0 5 5 5',
              emptyText : 'Поиск',
              enableKeyEvents: true,
              onTriggerClick: function () {
                this.reset();
                this.focus();
              },
              listeners: {
                change: function(field, newValue, oldValue, eOpts) {
                  me.fireEvent('searchchange', me, field, newValue, oldValue);
                },
                buffer: 1000
              }
            }]
          }];
        }

        this.lbar = [ { 
            xtype   : 'general_tabpanelsecLbar',
            ref     : 'Staff',
            grid    : this
        }];

    this.bbar = {
      xtype: 'pagingtoolbar',
      store: this.store,
      inputItemWidth: 60,
      displayInfo: true
    };
      
      this.callParent(arguments);
    }
});
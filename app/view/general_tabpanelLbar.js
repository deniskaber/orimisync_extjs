Ext.define('TestOrimi.view.general_tabpanelLbar', {
    extend: 'Ext.container.Container',
    xtype: 'general_tabpanelLbar',
    width: 55,
    padding: '2',
    border: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        margin: '2 0 2 0'
    },
    ref: 'Sku',

    initComponent: function () {

        var me = this,
            userRights = TestOrimi.getApplication().data;

        me.items = [{
            xtype: 'button'
            , tooltip: 'Комбинированный поиск'
            , name: 'combSearch'
            , scale: 'medium'
            , text: '<span class="oi" data-glyph="magnifying-glass" aria-hidden="true"></span>'
            , handler: function () {
                Ext.create('TestOrimi.view.CreateSearchWindow' + this.up('general_tabpanelLbar').ref, {grid: this.up('general_tabpanelLbar').grid}).show();
            }
        }, {
            xtype: 'button'
            //, disabled: true
            , tooltip: 'Сформировать отчет о синхронизации'
            , scale: 'medium'
            , text: '<span class="oi" data-glyph="spreadsheet" aria-hidden="true"></span>'
            , handler: function (button) {
                this.up('general_tabpanelLbar').grid.fireEvent('reportbuttonclick', button, this.up('general_tabpanelLbar').ref);
            }
        }, {
            xtype: 'button'
            , disabled: userRights[me.ref + '_slave_sync'] ? false : true
            , tooltip: 'Отвязать выбранные'
            , scale: 'medium'
            , text: '<span class="oi" data-glyph="link-broken" aria-hidden="true"></span>'
            , handler: function (button) {
                var grid = this.up('general_tabpanelLbar').grid;
                grid.fireEvent('groupunsyncbuttonclick', grid);
            }
        }];
        
        this.callParent(arguments);
    }
});

Ext.define('TestOrimi.view.Viewport', {
    extend: 'Ext.container.Viewport',
    requires: ['Ext.layout.container.Border'],

    layout: 'border',
    name: 'viewport',
    id: 'viewport',

    initComponent: function() {
        var me = this,
            userRights = TestOrimi.getApplication().data,
            buttons = [],
            cards = [];

        if (userRights['Sku']) {
            buttons.push({
                text: '<span class="oi" data-glyph="briefcase" aria-hidden="true"></span><br><ins>Номенклатура</ins>'
                , handler: function() {
                    var viewport = this.up('[name=viewport]');
                    viewport.fireEvent('skurefbuttonclick', viewport, this);
                }
            });
        }

        if (userRights['TT']) {
            buttons.push({
                text: '<span class="oi" data-glyph="cart" aria-hidden="true"></span><br><ins>Торговые точки</ins>'
                , handler: function() {
                    var viewport = this.up('[name=viewport]');
                    viewport.fireEvent('ttrefbuttonclick', viewport, this);
                }
            });
        }

        if (userRights['Staff']) {
            buttons.push({
                text: '<span class="oi" data-glyph="people" aria-hidden="true"></span><br><ins>Сотрудники</ins>'
                , handler: function() {
                    var viewport = this.up('[name=viewport]');
                    viewport.fireEvent('staffrefbuttonclick', viewport, this);
                }
            });
        }

        if (userRights['Distr']) {
            buttons.push({
                text: '<span class="oi" data-glyph="project" aria-hidden="true"></span><br><ins>Дистрибьюторы</ins>'
                , handler: function() {
                    var viewport = this.up('[name=viewport]');
                    viewport.fireEvent('distrrefbuttonclick', viewport, this);
                }
            });
        }

        if (userRights['Wrhs']) {
            buttons.push({
                text: '<span class="oi" data-glyph="task" aria-hidden="true"></span><br><ins>Склады</ins>'
                , handler: function() {
                    var viewport = this.up('[name=viewport]');
                    viewport.fireEvent('wrhsrefbuttonclick', viewport, this);
                }
            });
        }

        if (userRights['Advertising']) {
            buttons.push({
                text: '<span class="oi" data-glyph="dollar" aria-hidden="true"></span><br><ins>Реклама</ins>'
                , handler: function() {
                    var viewport = this.up('[name=viewport]');
                    viewport.fireEvent('adv_refbuttonclick', viewport, this);
                }
            });
        }

        if (userRights['Attributes']) {
            buttons.push({
                text: '<span class="oi" data-glyph="grid-three-up" aria-hidden="true"></span><br><ins>Атрибуты</ins>'
                , handler: function() {
                    var viewport = this.up('[name=viewport]');
                    viewport.fireEvent('attrrefbuttonclick', viewport, this);
                }
            });
        }

        me.items = [{
            xtype: 'general_headerpanel',
            width: '100%',
            region: 'north'
        }, {
            xtype: 'panel',
            region: 'center',
            layout: 'card',
            name: 'domCard',
            items: [{
                xtype: 'container'
                , cls: 'backgroundGenMenu'
                , layout: {
                    type: 'hbox',
                    align: 'center',
                    pack: 'center'
                }
                , items: [{
                    xtype: 'panel'
                    , region: 'center'
                    , title: 'Выберите справочник'
                    , margin: 200
                    , bodyPadding: '35 30 20 30'
                    , defaults: {
                        xtype: 'button'
                        , margin: '0 20 20 20'
                        , width: 110
                    }
                    , layout: 'column'
                    , cls: 'genbuttons'
                    , items: buttons
                }]
            }, {
                xtype: 'container',
                name: 'sku_ref_card',
                layout: 'border',
                items: [
                    {xtype: 'general_grid', region: 'center'}
                    , {xtype: 'general_tabpanel', region: 'south'}
                ]
            }, {
                xtype: 'container',
                name: 'tt_ref_card',
                layout: 'border',
                items: [
                    {xtype: 'general_gridTT', region: 'center'}
                    , {xtype: 'general_tabpanelTT', region: 'south'}
                ]
            }, {
                xtype: 'container',
                name: 'staff_ref_card',
                layout: 'border',
                items: [
                    {xtype: 'general_gridStaff', region: 'center'}
                    , {xtype: 'general_tabpanelStaff', region: 'south'}
                ]
            }, {
                xtype: 'container',
                name: 'distr_ref_card',
                layout: 'border',
                items: [
                    {xtype: 'general_gridDistr', region: 'center'}
                ]
            }, {
                xtype: 'container',
                name: 'wrhs_ref_card',
                layout: 'border',
                items: [
                    {xtype: 'wrhs_general_grid', region: 'center'}
                    , {xtype: 'wrhs_general_tabpanel', region: 'south'}
                ]
            }, {
                xtype: 'container',
                name: 'attr_ref_card',
                loaded: false,
                layout: 'border',
                items: [
                    {xtype: 'general_grid_attributes', region: 'center'}
                ]
            }, {
                xtype: 'container',
                name: 'adv_ref_card',
                loaded: false,
                layout: 'border',
                items: [
                    {xtype: 'advertising_viewport_panel', region: 'center'}
                ]
            }]
        }];

        this.callParent(arguments);
    }
});

Ext.define('TestOrimi.view.DistributorWindow', {
    extend: 'Ext.window.Window',
    xtype: 'distributorWindow',
    width: 950,
    height: 525,
    modal: true,
    layout: 'fit',

    initComponent: function () {
        var me = this,
            field = me.field;

        me.title = field.fieldLabel;

        me.items = [{
            xtype: 'grid'
            , flex: 1
            , viewConfig: {
                stripeRows: false,
                getRowClass: function (record) {
                    return record.get('StatusID') == 1 ? 'temp-status-row' : '';
                },
                shrinkWrap: 0,
                shadow: false,
                trackOver: false,
                overItemCls: false
            }
            , plugins: [{
                ptype: 'bufferedrenderer',
                trailingBufferZone: 0,
                leadingBufferZone: 0
            }]
            , border: true
            , columnLines: true
            , columns: [
                {text: 'ID', dataIndex: 'ID', width: 80},
                {text: 'Код', dataIndex: 'Code', width: 130},
                {text: 'Название', dataIndex: 'Name', width: 160},
                {text: 'Город', dataIndex: 'City', width: 130},
                {text: 'Полный адрес', dataIndex: 'FullAddress', width: 150},
                {text: 'Дивизион', dataIndex: 'Division', width: 100},
                {text: 'Участок', dataIndex: 'Area', width: 100},
                {text: 'Сектор', dataIndex: 'Sector', width: 100},
                {text: 'Отв. менеджер', dataIndex: 'RespManager', width: 100},
                {text: 'Группа контрагентов', dataIndex: 'ContractorGroup', width: 100}
            ]
            , store: field.store
            , listeners: {
                celldblclick: function (grid, td, cellIndex, record, tr, rowIndex) {
                    me.down('[name=okButton]').handler();
                }
            }
        }];

        var i = 0,
            columns = this.items[0].columns,
            length = columns.length;

        for (; i < length; i++) {
            columns[i].items = [{
                xtype: 'container',
                margin: 4,
                flex: 1,
                layout: 'fit',
                items: [{
                    xtype: 'textfield',
                    emptyText: 'Поиск',
                    enableKeyEvents: true,
                    onTriggerClick: function () {
                        this.reset();
                        this.focus();
                    },
                    listeners: {
                        change: function (field, newValue, oldValue, eOpts) {
                            var grid = this.up('grid');
                            me.fireEvent('searchchange', grid, field, newValue, oldValue);
                        },
                        buffer: 1000
                    }
                }]
            }];
        }

        me.buttons = [{
            text: 'Ок',
            name: 'okButton',
            handler: function () {
                var win = this.up('window'),
                    selected = win.down('grid').getSelectionModel().getSelection()[0];

                if (!selected) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
                } else {
                    var fieldName = me.field.name;
                    if (fieldName.substr(-2) == 'ID') {
                        win.field.setValue(selected.get('ID'));
                    } else {
                        win.field.setValue(selected.get('Name'));
                    }
                    win.destroy();
                }
            }
        }, {
            text: 'Отмена',
            handler: function () {
                this.up('window').destroy();
            }
        }];

        this.callParent(arguments);
    }
});

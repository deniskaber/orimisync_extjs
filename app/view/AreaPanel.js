Ext.define('TestOrimi.view.AreaPanel', {
    extend: 'Ext.container.Container',
    xtype: 'areaPanel',
    border: false,
    req: false,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    height: 100,
    margin: '0 0 10 0',
    items: [{
        xtype: 'container',
        width: 150,
        layout: 'vbox',
        items: [{
            xtype: 'container',
            html: 'Участок:<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
            height: 20,
            margin: '4 0 10 0'
        }, {
            xtype: 'buttongroup',
            frame: false,
            name: 'areaButtonGroup',
            columns: 2,
            border: false,
            items: [{
                xtype: 'button',
                text: '<span class="oi oiadd" data-glyph="plus" aria-hidden="true"></span>',
                height: 37,
                tooltip: 'Добавить',
                scale: 'medium',
                handler: function () {
                    Ext.create('TestOrimi.view.AreaWindow', {selector: this}).show();
                }
            }, {
                xtype: 'button',
                margin: '0 0 0 8',
                height: 37,
                text: '<span class="oi oiadd" data-glyph="trash" aria-hidden="true"></span>',
                tooltip: 'Удалить',
                scale: 'medium',
                handler: function () {
                    var container = this.up('areaPanel'),
                        listadd = container.down('boundlist'),
                        selected = listadd.getSelectionModel().getSelection()[0];

                    if (selected) {
                        Ext.getStore('TestOrimi.store.SelectedAreaStore').remove(selected);
                    } else {
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Участок не выделен', 'winError');
                    }
                }
            }]
        }]
    }, {
        margin: '0 0 0 16',
        xtype: 'boundlist',
        anchor: '100%',
        height: 98,
        attribute: 'AreaID',
        trackOver: false,
        flex: 1,
        displayField: 'Name',
        valueField: 'ID',
        store: 'TestOrimi.store.SelectedAreaStore',
        name: 'area',
        queryMode: 'local',
        multiSelect: true,
        listeners: {
            itemadd: function (records, index, node, eOpts) {
                console.log('AreaPanel itemadd');
                var divisions = [],
                    storeSelSec = Ext.getStore('TestOrimi.store.SelectedAreaStore'),
                    lengthItems = storeSelSec.getCount(),
                    form = this.up('form'),
                    divisionId;

                if (!this.up('areaPanel').down('buttongroup').isDisabled()) {
                    var sectorPanel = form.up('window').down('sectorPanel');
                    sectorPanel.down('buttongroup').disable();
                    sectorPanel.addCls('noteditable');
                }

                for (var i = 0; i < lengthItems; i++) {
                    divisionId = storeSelSec.getAt(i).get('DivisionID');

                    if (divisionId && !Ext.Array.contains(divisions, divisionId)) {
                        divisions.push(divisionId);
                    }
                }

                if (divisions.length == 1) {
                    form.down('[name=DivisionID]').setValue(divisions[0]);
                } else {
                    form.down('[name=DivisionID]').reset();
                }
            },
            itemremove: function (record, index, eOpts) {
                console.log('AreaPanel itemremove');
                var storeSelSec = Ext.getStore('TestOrimi.store.SelectedAreaStore'),
                    lengthItems = storeSelSec.getCount(),
                    form = this.up('form'),
                    divisionId;

                if (lengthItems > 0) {
                    var divisions = [];

                    for (var i = 0; i < lengthItems; i++) {
                        divisionId = storeSelSec.getAt(i).get('DivisionID');

                        if (divisionId && !Ext.Array.contains(divisions, divisionId)) {
                            divisions.push(divisionId);
                        }
                    }

                    if (divisions.length == 1) {
                        form.down('[name=DivisionID]').setValue(divisions[0]);
                    } else {
                        form.down('[name=DivisionID]').reset();
                    }
                } else {
                    if (!this.up('areaPanel').down('buttongroup').isDisabled()) {
                        var sectorPanel = form.up('window').down('sectorPanel');
                        sectorPanel.down('buttongroup').enable();
                        sectorPanel.removeCls('noteditable');
                    }
                }
            }
        }
    }]
});

Ext.define('TestOrimi.view.AdvertisingBossPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'advertising_boss_panel',
    flex: 1,
    border: false,
    layout: 'border',
    items: [
        {
            xtype: 'advertising_boss_headers_grid',
            region: 'center'
        }, {
            xtype: 'advertising_boss_rows_grid',
            region: 'south',
            split: true,
            collapsible: true,
            header: false
        }
    ],
    loaded: false
});

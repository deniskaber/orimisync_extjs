Ext.define('TestOrimi.view.ManagersWindow', {
    extend      : 'Ext.window.Window',
    xtype       : 'managersWindow',
    width       : '790px',
    height      : '525px',
    modal       : true,
    layout      : 'fit',
    title       : 'Менеджеры',

    initComponent: function() {
        var me = this,
            store = Ext.getStore('TestOrimi.store.ManagersStore');
            win = me.win,
            ID = win.data.get('ID'),
            isNet = win.data.get('isNet'),
            active = (win.data.get('StatusID') == 2) ? true : false;
        
        store.load({
            params: {
                StoresID: ID
                ,isNet: isNet
            }
        });
        
        me.items = [{   
            xtype       : 'grid'
            ,flex        : 1
            ,border      : true
            ,columnLines : true
            ,columns: [
                 { text: 'ФИО',                     dataIndex: 'Managers',      flex: 1} 
                ,{ text: 'Дата начала работы',      dataIndex: 'DateHired',     width: 200}
                ,{ text: 'Дата увольнения',         dataIndex: 'DateFired',     width: 200}
            ]
            ,store: store
            ,listeners: {
                celldblclick: function(grid, td, cellIndex, record, tr, rowIndex) {
                    me.down('[name=editButton]').handler();
                }
            }
        }];

        var i = 0,
            columns = this.items[0].columns,
            length = columns.length;

        for (;i<length;i++) {
            columns[i].items = [{
                xtype: 'container',
                margin: 4,
                flex: 1,
                layout: 'fit',
                listeners: {
                  scope: this,
                  element: 'el',
                  mousedown: function(e)
                  {
                      e.stopPropagation();
                  },
                  click: function(e)
                  {
                      e.stopPropagation();
                  },
                  keydown: function(e){
                       e.stopPropagation();
                  },
                  keypress: function(e){
                       e.stopPropagation();
                       
                  },
                  keyup: function(e){
                       e.stopPropagation();
                  }
                },
                items:[{
                    xtype: 'textfield',
                    emptyText : 'Поиск',
                    enableKeyEvents: true,
                    onTriggerClick: function () {
                        this.reset();
                        this.focus();
                    },
                    listeners: {
                        change: function(field, newValue, oldValue, eOpts) {
                            var grid = this.up('grid');
                            me.fireEvent('searchchange', grid, field, newValue, oldValue);
                        },
                        buffer: 1000
                    }
                }]
            }];
        }

        me.buttons = [{
            text: 'Создать',
            name: 'createButton',
            disabled: (!active),
            handler: function() { 
                //console.dir(fieldID);
                Ext.create('TestOrimi.view.AddManagersWindow',{
                    title: 'Создание нового элемента'
                    ,fieldID: ID
                    ,isNet: isNet
                }).show();
            }
        },{
            text: 'Редактировать',
            name: 'editButton',
            disabled: (!active),
            handler: function() {
                var win = this.up('window'),
                    selected = win.down('grid').getSelectionModel().getSelection()[0];

                if (!selected) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
                } else {
                    Ext.create('TestOrimi.view.AddManagersWindow',{
                        title: 'Редактирование элемента'
                        ,data: selected
                        ,fieldID: ID
                        ,isNet: isNet
                    }).show();
                }
            }
        },{
            text: 'Удалить',
            name: 'deleteButton',
            disabled: (!active),
            handler: function() {
                var win = this.up('window'),
              		grid = win.down('grid'),
                    selected = grid.getSelectionModel().getSelection()[0];

                if (!selected) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
                } else {
                	this.up('window').fireEvent('managerdeletebuttonclick', win, grid);
                }
            }
        },'->',{
            text: 'Ок',
            handler: function() {
                this.up('window').destroy();
            }
        }];
    
        this.callParent(arguments);
    }
});
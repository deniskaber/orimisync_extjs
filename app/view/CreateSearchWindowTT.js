Ext.define('TestOrimi.view.CreateSearchWindowTT', {
    extend: 'Ext.window.Window',
    title: 'Комбинированный поиск',
    modal: true,
    width: 500,
    height: 600,
    layout: 'fit',
    items: [{
        xtype: 'form',
        margin: 10,
        border: false,
        layout: {type: 'vbox', align: 'stretch'},
        items: [{
            xtype: 'textfield'
            , fieldLabel: 'Поиск по всем атрибутам'
            , name: 'searchall'
            , anchor: '90%'
            , labelWidth: 170
        }, {
            fieldLabel: 'Применить ко всем справочникам'
            , xtype: 'radiogroup'
            , labelWidth: 170
            , anchor: 'none'
            , layout: {
                autoFlex: false
            },
            defaults: {
                name: 'allstores',
                margin: '0 15 0 0'
            }
            , items: [{
                boxLabel: 'нет',
                inputValue: 0
            }, {
                boxLabel: 'да',
                inputValue: 1
            }, {
                boxLabel: 'только подчиненные',
                inputValue: 2,
                margin: 0
            }]
        }, {
            xtype: 'container',
            flex: 1,
            autoScroll: true,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            margin: '20 0 0 0',
            defaults: {
                xtype: 'textfield',
                anchor: '90%',
                labelWidth: 160,
                margin: '3 15 2 5'
            },
            items: [{
                fieldLabel: 'ID',
                name: 'ID'
            }, {
                fieldLabel: 'Код',
                name: 'Code'
            }, {
                fieldLabel: 'Название',
                name: 'Name'
            }, {
                fieldLabel: 'Черновой адрес',
                name: 'TempAddress'
            }, {
                fieldLabel: 'Полный адрес',
                name: 'FullAddress'
            }, {
                xtype: 'fieldset',
                title: 'Адресные данные',
                collapsible: true,
                collapsed: true,
                defaultType: 'textfield',
                defaults: {anchor: '100%', labelWidth: 150},
                layout: 'anchor',
                items: [{
                    fieldLabel: 'Адрес',
                    name: 'Address'
                }, {
                    fieldLabel: 'Индекс',
                    name: 'PostCode'
                }, {
                    fieldLabel: 'Страна',
                    name: 'Country',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.CountryStore'
                }, {
                    fieldLabel: 'Округ',
                    name: 'County',
                    attribute: 'County',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.CountyStore'
                }, {
                    fieldLabel: 'Регион',
                    name: 'Region',
                    attribute: 'Region',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.RegionStore'
                }, {
                    fieldLabel: 'Район',
                    name: 'District',
                    attribute: 'District',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.DistrictStore'
                }, {
                    fieldLabel: 'Город',
                    name: 'City',
                    attribute: 'City',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.CityStore'
                }, {
                    fieldLabel: 'Улица',
                    name: 'Street'
                }, {
                    fieldLabel: 'Дом',
                    name: 'Building'
                }]
            }, {
                fieldLabel: 'Канал',
                name: 'Channel',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.ChannelStore'
            }, {
                fieldLabel: 'Подканал',
                name: 'SubChannel',
                attribute: 'SubChannel',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.SubChannelStore'
            }, {
                fieldLabel: 'Формат',
                name: 'Format',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.FormatStore'
            }, {
                fieldLabel: 'Сектор',
                name: 'Sector',
                attribute: 'Sector',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.SectorStore'
            }, {
                fieldLabel: 'Участок',
                name: 'Area',
                attribute: 'Area',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.AreaStore'
            }, {
                fieldLabel: 'Дивизион',
                name: 'Division',
                attribute: 'Division',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.DivisionStore'
            }, {
                fieldLabel: 'Дата открытия ТТ',
                name: 'DateOpen'
            }, {
                fieldLabel: 'Дата закрытия ТТ',
                name: 'DateClose'
            }, {
                fieldLabel: 'Сеть',
                xtype: 'radiogroup',
                anchor: 'none',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    name: 'isNet',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'ТТ',
                    inputValue: '0'
                }, {
                    boxLabel: 'КСР',
                    inputValue: '1'
                }]
            }, {
                fieldLabel: 'Название сети',
                name: 'Net',
                attribute: 'Net',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.NetStore'
            }, {
                fieldLabel: 'Тип сети',
                name: 'NetType',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.NetTypeStore'

            }, {
                fieldLabel: 'КАИ Nielsen',
                xtype: 'radiogroup',
                anchor: 'none',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    name: 'isNielsen',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'да',
                    inputValue: '1'
                }, {
                    boxLabel: 'нет',
                    inputValue: '0'
                }]
            }, {
                fieldLabel: 'Грузополучатель ОРИМИ',
                xtype: 'radiogroup',
                anchor: 'none',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    name: 'isConsignee',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'да',
                    inputValue: '1'
                }, {
                    boxLabel: 'нет',
                    inputValue: '0'
                }]
            }, {
                fieldLabel: 'Группа контрагентов',
                name: 'ContractorGroup',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.ContractorGroupStore'
            }, {
                fieldLabel: 'Участвует в РБП',
                xtype: 'radiogroup',
                anchor: 'none',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    name: 'isInRBP',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'да',
                    inputValue: '1'
                }, {
                    boxLabel: 'нет',
                    inputValue: '0'
                }]
            }, {
                fieldLabel: 'Код Дистр.',
                name: 'DistrCode'
            }, {
                fieldLabel: 'Площадь ТТ',
                name: 'ShopArea'
            }, {
                fieldLabel: 'Координаты',
                name: 'Coordinates'
            }, {
                fieldLabel: 'GLN',
                name: 'GLN'
            }, {
                xtype: 'textareafield',
                name: 'Comment',
                fieldLabel: 'Комментарий'
            }, {
                fieldLabel: 'ФИАС',
                name: 'FIAS'
            }, {
                fieldLabel: 'Отв. менеджер',
                name: 'RespManager',
                attribute: 'ParentStaff',
                displayField: 'FIO',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.ParentStore'
            }, {
                fieldLabel: 'Отв. мерчендайзер',
                name: 'RespMerch',
                attribute: 'ParentStaff',
                displayField: 'FIO',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.ParentStore'
            }, {
                fieldLabel: 'Отв. ТП',
                name: 'RespTR',
                attribute: 'ParentStaff',
                displayField: 'FIO',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.ParentStore'
            }, {
                fieldLabel: 'Выборка',
                name: 'Selection'
            }, {
                fieldLabel: 'Статус',
                name: 'Status'
            }, {
                fieldLabel: 'Источник',
                name: 'DataSource'
            }, {
                fieldLabel: 'Автор изменений',
                name: 'Author'
            }, {
                fieldLabel: 'Дата посл. изменения',
                name: 'DateChange'
            }, {
                fieldLabel: 'Авто/Ручной',
                xtype: 'radiogroup',
                anchor: 'none',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    name: 'SyncType',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'Авто',
                    inputValue: 1
                }, {
                    boxLabel: 'Ручной',
                    inputValue: 0
                }]
            }]
        }]
    }],

    buttons: [{
        text: 'Сбросить все',
        handler: function () {
            this.up('window').down('form').getForm().reset();
        }
    }, '->', {
        text: 'Применить',
        handler: function () {
            var grid = this.up('window').grid,
                store = grid.store,
                formVals = this.up('window').down('form').getValues(),
                filters = [];

            var modelFields = store.model.getFields(),
                dataFields = [],
                i = 0,
                length = modelFields.length;

            for (; i < length; i++) {
                dataFields.push(modelFields[i].name);
            }

            Ext.Object.each(formVals, function (key, value, myself) {
                if (value || value === 0) {
                    if (Ext.Array.contains(dataFields, key)) {
                        filters.push({property: key, value: value});
                    }
                }
            });

            if (formVals['allstores'] == 1) {
                var masterStore = Ext.getStore('TestOrimi.store.MasterStoresStore'),
                    syncedStore = Ext.getStore('TestOrimi.store.SyncedStoresStore'),
                    slaveStore = Ext.getStore('TestOrimi.store.SlaveStoresStore');

                masterStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                masterStore.fireEvent('applyFilters', masterStore, filters, grid.up('viewport').down('general_gridTT'));
                syncedStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                syncedStore.fireEvent('applyFilters', syncedStore, filters, grid.up('viewport').down('general_tabpanelTT').down('general_tabpanelFirstcolTT'));
                slaveStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                slaveStore.fireEvent('applyFilters', slaveStore, filters, grid.up('viewport').down('general_tabpanelTT').down('general_tabpanelSecondcolTT'));
            } else if (formVals['allstores'] == 2) {
                var syncedStore = Ext.getStore('TestOrimi.store.SyncedStoresStore'),
                    slaveStore = Ext.getStore('TestOrimi.store.SlaveStoresStore');

                syncedStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                syncedStore.fireEvent('applyFilters', syncedStore, filters, grid.up('viewport').down('general_tabpanelTT').down('general_tabpanelFirstcolTT'));
                slaveStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                slaveStore.fireEvent('applyFilters', slaveStore, filters, grid.up('viewport').down('general_tabpanelTT').down('general_tabpanelSecondcolTT'));
            } else {
                store.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                store.fireEvent('applyFilters', store, filters, grid);
            }

            this.up('window').destroy();
        }
    }, {
        text: 'Отмена',
        handler: function () {
            this.up('window').destroy();
        }
    }],

    initComponent: function () {
        var me = this,
            grid = me.grid;

        this.callParent(arguments);

        var cols = [],
            columns = grid.columns,
            length = columns.length;

        for (i = 0; i < length; i++) {
            if (columns[i]['dataIndex'])
                cols.push(columns[i]['dataIndex']);
        }
        cols.push('searchall');
        cols.push('allstores');
        cols.push('isNet');

        var fields = me.query('textfield'), //все textfield окна
            radiogroups = me.query('radiogroup');

        length = fields.length;

        for (i = 0; i < length; i++) {                //цикл указывания какие поля останутся активными
            if (!(Ext.Array.contains(cols, fields[i].name))) {
                fields[i].hide();
                fields[i].disable();
            }
        }

        length = radiogroups.length;

        for (i = 0; i < length; i++) {                //цикл указывания какие поля останутся активными
            if (!(Ext.Array.contains(cols, radiogroups[i].items.items[0].name))) {
                radiogroups[i].hide();
                radiogroups[i].disable();
            }
        }

        var store = grid.store,
            filterRecord = {},
            filters = store.filters.items;

        length = filters.length;

        if (length) {
            for (i = 0; i < length; i++) {
                filterRecord[filters[i].property] = filters[i].value;
            }
        }
        if (store.proxy.extraParams['searchall']) {
            filterRecord['searchall'] = store.proxy.extraParams['searchall'];
        }
        this.down('form').getForm().setValues(filterRecord);
    }
});

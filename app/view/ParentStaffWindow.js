Ext.define('TestOrimi.view.ParentStaffWindow', {
    extend: 'Ext.window.Window',
    xtype: 'parentStaffWindow',
    width: 950,
    height: 525,
    modal: true,
    layout: 'fit',

    initComponent: function () {
        var me = this,
            field = me.field;

        me.title = field.fieldLabel;

        me.items = [{
            xtype: 'grid'
            , flex: 1
            , viewConfig: {
                stripeRows: false,
                getRowClass: function (record) {
                    return record.get('StatusID') == 1 ? 'temp-status-row' : '';
                },
                shrinkWrap: 0,
                shadow: false,
                trackOver: false,
                overItemCls: false
            }
            , plugins: [{
                ptype: 'bufferedrenderer',
                trailingBufferZone: 0,
                leadingBufferZone: 0
            }]
            , border: true
            , columnLines: true
            , columns: [
                {text: 'ID', dataIndex: 'ID', width: 80},
                {text: 'Код', dataIndex: 'Code', width: 130},
                {text: 'Фамилия', dataIndex: 'SurName', width: 160},
                {text: 'Имя', dataIndex: 'Name', width: 160},
                {text: 'Отчество', dataIndex: 'LastName', width: 160},
                {text: 'ФИО', dataIndex: 'FIO', width: 190},
                {text: 'Дата рождения', dataIndex: 'BirthDate', width: 150},
                {text: 'Тип', dataIndex: 'Type', width: 110},
                {text: 'Дивизион', dataIndex: 'Division', width: 200},
                {text: 'Дата начала работы', dataIndex: 'DateHired', width: 195},
                {text: 'Дата увольнения', dataIndex: 'DateFired', width: 195},
                {text: 'E-mail', dataIndex: 'EMail', width: 150},
                {text: 'Телефон', dataIndex: 'Phone', width: 130},
                {text: 'Комментарий', dataIndex: 'Comment', width: 120},
                {text: 'Мотивация', dataIndex: 'Motivation', width: 120},
                {text: 'Признак', dataIndex: 'FeatureName', width: 120},
                {text: 'Участок', dataIndex: 'Sector', width: 120}
            ]
            , store: field.store
            , listeners: {
                celldblclick: function (grid, td, cellIndex, record, tr, rowIndex) {
                    me.down('[name=okButton]').handler();
                }
            }
        }];

        var i = 0,
            columns = this.items[0].columns,
            length = columns.length;

        for (; i < length; i++) {
            columns[i].items = [{
                xtype: 'container',
                margin: 4,
                flex: 1,
                layout: 'fit',
                items: [{
                    xtype: 'textfield',
                    emptyText: 'Поиск',
                    enableKeyEvents: true,
                    onTriggerClick: function () {
                        this.reset();
                        this.focus();
                    },
                    listeners: {
                        change: function (field, newValue, oldValue, eOpts) {
                            var grid = this.up('grid');
                            me.fireEvent('searchchange', grid, field, newValue, oldValue);
                        },
                        buffer: 1000
                    }
                }]
            }];
        }

        me.buttons = [{
            text: 'Ок',
            name: 'okButton',
            handler: function () {
                var win = this.up('window'),
                    selected = win.down('grid').getSelectionModel().getSelection()[0];

                if (!selected) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
                } else {
                    var fieldName = me.field.name;
                    if (fieldName.substr(-2) == 'ID') {
                        win.field.setValue(selected.get('ID'));
                    } else {
                        win.field.setValue(selected.get('FIO'));
                    }
                    win.destroy();
                }
            }
        }, {
            text: 'Отмена',
            handler: function () {
                this.up('window').destroy();
            }
        }];

        this.callParent(arguments);
    }
});

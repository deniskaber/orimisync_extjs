Ext.define('TestOrimi.view.CreateAdvertisingBossWindow', {
    extend: 'Ext.window.Window',
    name: 'createAdvertisingBossWindow',
    width: 450,
    height: 300,
    modal: true,
    title: 'Распоряжение генерального директора',
    layout: 'fit',
    items: [{
        xtype: 'form',
        bodyPadding: 10,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'fieldset',
            defaults: {
                xtype: 'textfield'
                , anchor: '100%'
                , labelWidth: 160
            },
            border: false,
            items: [{
                name: 'ID',
                fieldLabel: 'ID',
                readOnly: true,
                cls: 'noteditable'
            }, {
                name: 'DocNum',
                fieldLabel: 'Номер',
                readOnly: true,
                cls: 'noteditable'
            }, {
                fieldLabel: 'Заголовок',
                name: 'Title',
                readOnly: true,
                cls: 'noteditable'
            }, {
                fieldLabel: 'Дата начала',
                name: 'DateBegin',
                readOnly: true,
                cls: 'noteditable'
            }, {
                fieldLabel: 'Дата окончания',
                name: 'DateEnd',
                readOnly: true,
                cls: 'noteditable'
            }, {
                fieldLabel: 'Статус заявки',
                name: 'StatusID',
                xtype: 'modalcombo',
                store: 'TestOrimi.store.BossOrderStatusStore',
                value: 10,
                editable: false
            }, {
                fieldLabel: 'Автор изменений',
                name: 'Author',
                readOnly: true,
                cls: 'noteditable'
            }, {
                fieldLabel: 'Дата посл. изменения',
                name: 'DateChange',
                readOnly: true,
                cls: 'noteditable'
            }]
        }]
    }],

    buttons: [{
        text: 'Ок',
        name: 'okButton',
        handler: function() {
            var store = Ext.getStore('TestOrimi.store.AdvertisingBossHeadersStore'),
                win = this.up('window'),
                form = win.down('form').getForm();

            if (form.isValid()) {
                var myMask = new Ext.LoadMask(win, {msg: "Сохраняю..."});

                myMask.show();
                form.submit({
                    url: 'resources/data/api.php',
                    params: {
                        act: store.proxy.extraParams['act'],
                        subaction: (win.data) ? (win.data.get('ID') ? 'update' : 'create') : 'create'
                    },
                    success: function(form, response) {
                        myMask.destroy();
                        if (win.data) {
                            if (win.data.get('ID') == 0) {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess');
                            } else {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Элемент обновлен', 'winSuccess');
                            }
                        } else {
                            TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess');
                        }
                        store.load();
                        win.destroy();
                    },
                    failure: function(form, action) {
                        myMask.destroy();
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                    }
                });
            }
        }
    }, {
        text: 'Отмена',
        handler: function() {
            this.up('window').destroy();
        }
    }],

    initComponent: function() {
        var me = this;
        var userRights = TestOrimi.getApplication().data;
        var statusField;

        this.callParent(arguments);

        var form = me.down('form');
        statusField = form.down('[name=StatusID]');

        if (!userRights['Advertising_boss_set_stat']) {
            statusField.readOnly = true;
            statusField.hideTrigger = true;
            statusField.cls = 'noteditable';

            this.down('[name=okButton]').disable();
        }

        if (this.data) {
            form.loadRecord(this.data);
        }
    }
});

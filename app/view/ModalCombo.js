Ext.define('TestOrimi.view.ModalCombo', {
    extend: 'Ext.form.ComboBox',
    xtype: 'modalcombo',
    triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
    attribute: 'simple', //by deafult
    queryMode: 'local',
    displayField: 'Name',
    queryDelay: 500,
    valueField: 'ID',
    anyMatch: true,
    clearFilterOnBlur: false,
    autoSelect: false,
    validator: function (v) {
        if (v) {
            return (this.getStore().findExact(this.displayField, v) !== -1) ? true : 'Неизвестный элемент';
        }

        return true;
    },
    listeners: {
        select: function (combo, records, eOpts) {
            if (records.length) {
                combo.reset();
                combo.setValue(records[0]);
            }
        },
        change: function (field, newValue, oldValue) {
            var rec;

            if (field.isValid() && newValue) {
                if (newValue !== parseInt(newValue)) {
                    rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                    if (rec) {
                        field.setValue(rec.get('ID'));
                        return;
                    }
                }
            }
            switch (field.name) {
                case 'ManufacturerID':
                    (function setManufacturerIDFields() {
                        var brandField = field.up('form').down('[name=BrandID]');
                        if (field.isValid() && newValue) {
                            if (brandField) {
                                rec = brandField.store.findRecord('ID', brandField.value, 0, false, false, true);
                                if (rec) {
                                    if (rec.get('ManufacturerID') != newValue)
                                        brandField.setValue();
                                } else
                                    brandField.setValue();
                            }
                        } else {
                            if (brandField)
                                brandField.setValue();
                        }

                        var fromField = field.up('form').down('[name=FromGroup]');
                        if (fromField) {
                            fromField.suspendEvents();
                            if (newValue == 292) { //TODO choose valid ID
                                fromField.setValue({From: 1});
                            } else {
                                fromField.setValue({From: 0});
                            }
                            fromField.resumeEvents();
                        }
                    })();
                    break;
                case 'BrandID':
                    (function setBrandIDFields() {
                        var manField      = field.up('form').down('[name=ManufacturerID]'),
                            subBrandField = field.up('form').down('[name=SubBrandID]');

                        if (field.isValid() && newValue) {
                            if (manField) {
                                rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                if (rec) {
                                    manField.suspendEvents();
                                    manField.reset();
                                    manField.resumeEvents();
                                    manField.setValue(rec.get('ManufacturerID'));
                                }
                            }
                            if (subBrandField) {
                                rec = subBrandField.store.findRecord('ID', subBrandField.value, 0, false, false, true);
                                if (rec) {
                                    if (rec.get('BrandID') != newValue)
                                        subBrandField.setValue();
                                } else
                                    subBrandField.setValue();
                            }
                        } else {
                            if (subBrandField)
                                subBrandField.setValue();
                        }
                    })();
                    break;
                case 'SubBrandID':
                    (function setSubBrandIDFields() {
                        var brandField = field.up('form').down('[name=BrandID]');

                        if (newValue) {
                            if (field.isValid()) {
                                if (brandField) {
                                    rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                    if (rec) {
                                        brandField.suspendEvents();
                                        brandField.reset();
                                        brandField.resumeEvents();
                                        brandField.setValue(rec.get('BrandID'));
                                    }
                                }
                            } else {
                                if (newValue === parseInt(newValue)) {
                                    field.store.load({
                                        callback: function (records, operation, success) {
                                            if (field.isValid()) {
                                                if (brandField) {
                                                    rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                                    if (rec) {
                                                        brandField.suspendEvents();
                                                        brandField.reset();
                                                        brandField.resumeEvents();
                                                        brandField.setValue(rec.get('BrandID'));
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    })();
                    break;
                case 'CategoryID':
                    (function setCategoryIDFields() {
                        var segmentField = field.up('form').down('[name=SegmentID]');
                        if (field.isValid() && newValue) {
                            if (segmentField) {
                                rec = segmentField.store.findRecord('ID', segmentField.value, 0, false, false, true);
                                if (rec) {
                                    if (rec.get('CategoryID') != newValue)
                                        segmentField.setValue();
                                } else
                                    segmentField.setValue();
                            }
                        } else {
                            if (segmentField)
                                segmentField.setValue();
                        }
                    })();
                    break;
                case 'SegmentID':
                    (function setSegmentIDFields() {
                        var categField   = field.up('form').down('[name=CategoryID]'),
                            subSegmField = field.up('form').down('[name=SubSegmentID]');

                        if (field.isValid() && newValue) {
                            if (categField) {
                                rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                if (rec) {
                                    categField.suspendEvents();
                                    categField.reset();
                                    categField.resumeEvents();
                                    categField.setValue(rec.get('CategoryID'));
                                }
                            }
                            if (subSegmField) {
                                rec = subSegmField.store.findRecord('ID', subSegmField.value, 0, false, false, true);
                                if (rec) {
                                    if (rec.get('SegmentID') != newValue)
                                        subSegmField.setValue();
                                } else
                                    subSegmField.setValue();
                            }
                        } else {
                            if (subSegmField)
                                subSegmField.setValue();
                        }
                    })();
                    break;
                case 'SubSegmentID':
                    (function setSubSegmentIDFields() {
                        var segmField = field.up('form').down('[name=SegmentID]');

                        if (newValue) {
                            if (field.isValid()) {
                                if (segmField) {
                                    rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                    if (rec) {
                                        segmField.suspendEvents();
                                        segmField.reset();
                                        segmField.resumeEvents();
                                        segmField.setValue(rec.get('SegmentID'));
                                    }
                                }
                            } else {
                                if (newValue === parseInt(newValue)) {
                                    field.store.load({
                                        callback: function (records, operation, success) {
                                            if (field.isValid()) {
                                                if (segmField) {
                                                    rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                                    if (rec) {
                                                        segmField.suspendEvents();
                                                        segmField.reset();
                                                        segmField.resumeEvents();
                                                        segmField.setValue(rec.get('SegmentID'));
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    })();
                    break;
                //-------------------------
                case 'CountyID':
                    (function setCountyIDFields() {
                        var countryField = field.up('form').down('[name=CountryID]'); //задает

                        if (field.isValid() && newValue) {
                            if (countryField) {
                                rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                if (rec) {
                                    countryField.suspendEvents();
                                    countryField.reset();
                                    countryField.resumeEvents();
                                    countryField.setValue(rec.get('CountryID'));
                                }
                            }
                        }
                    })();
                    break;
                case 'RegionID':
                    (function setRegionIDFields() {
                        var countyField = field.up('form').down('[name=CountyID]'); //задает

                        if (field.isValid() && newValue) {
                            if (countyField) {
                                rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                if (rec) {
                                    countyField.suspendEvents();
                                    countyField.reset();
                                    countyField.resumeEvents();
                                    countyField.setValue(rec.get('CountyID'));
                                }
                            }
                        }
                    })();
                    break;
                case 'DistrictID':
                    (function setCityIDFields() {
                        var regionField = field.up('form').down('[name=RegionID]'); //задает

                        if (field.isValid() && newValue) {
                            if (regionField) {
                                rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                if (rec) {
                                    regionField.suspendEvents();
                                    regionField.reset();
                                    regionField.resumeEvents();
                                    regionField.setValue(rec.get('RegionID'));
                                }
                            }
                        }
                    })();
                    break;

                case 'CityID':
                    (function setCityIDFields() {
                        var regionField = field.up('form').down('[name=RegionID]'), //задает
                            distrField  = field.up('form').down('[name=DistrictID]'), //задает
                            sectField   = field.up('form').down('[name=SectorID]'), //задает
                            fiasField   = field.up('form').down('[name=FIAS]'); //задает

                        if (newValue) {
                            if (newValue === parseInt(newValue)) { //ID
                                if (field.isValid()) {
                                    rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                    if (regionField) {
                                        if (rec.get('RegionID')) {
                                            regionField.suspendEvents();
                                            regionField.reset();
                                            regionField.resumeEvents();
                                            regionField.setValue(rec.get('RegionID'));
                                        }
                                    }

                                    if (distrField) {
                                        if (rec.get('DistrictID')) {
                                            distrField.suspendEvents();
                                            distrField.reset();
                                            distrField.resumeEvents();
                                            distrField.setValue(rec.get('DistrictID'));
                                        }
                                    }

                                    if (sectField) {
                                        if (rec.get('SectorID') === 0) {
                                            sectField.setValue(sectField.store.findRecord('Name', 'Прочее', 0, false, false, true).get('ID'));
                                        } else {
                                            sectField.suspendEvents();
                                            sectField.reset();
                                            sectField.resumeEvents();
                                            sectField.setValue(rec.get('SectorID'));
                                        }
                                    }

                                    if (fiasField) {
                                        fiasField.setValue(rec.get('BriefNotation'));
                                    }
                                } else {
                                    field.store.load({
                                        params: {
                                            ID: newValue
                                        },
                                        callback: function (records, operation, success) {
                                            if (field.isValid()) {
                                                rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                                if (regionField) {
                                                    if (rec.get('RegionID')) {
                                                        regionField.suspendEvents();
                                                        regionField.reset();
                                                        regionField.resumeEvents();
                                                        regionField.setValue(rec.get('RegionID'));
                                                    }
                                                }

                                                if (distrField) {
                                                    if (rec.get('DistrictID')) {
                                                        distrField.suspendEvents();
                                                        distrField.reset();
                                                        distrField.resumeEvents();
                                                        distrField.setValue(rec.get('DistrictID'));
                                                    }
                                                }

                                                if (sectField) {
                                                    if (rec.get('SectorID') === 0) {
                                                        sectField.setValue(sectField.store.findRecord('Name', 'Прочее', 0, false, false, true).get('ID'));
                                                    } else {
                                                        sectField.suspendEvents();
                                                        sectField.reset();
                                                        sectField.resumeEvents();
                                                        sectField.setValue(rec.get('SectorID'));
                                                    }
                                                }

                                                if (fiasField) {
                                                    fiasField.setValue(rec.get('BriefNotation'));
                                                }
                                            }
                                        }
                                    });
                                }
                            } else { //Name
                                if (!field.queryFilter) {
                                    field.store.load({
                                        params: {
                                            Name: newValue
                                        },
                                        callback: function (records, operation, success) {
                                            rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                                            if (rec) {
                                                field.setValue(rec.get('ID'));
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    })();
                    break;

                case 'SectorID':
                    (function setSectorIDFields() {
                        var areaField = field.up('form').down('[name=AreaID]'); //задает

                        if (field.isValid() && newValue) {
                            if (areaField) {
                                rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                if (rec) {
                                    areaField.suspendEvents();
                                    areaField.reset();
                                    areaField.resumeEvents();
                                    areaField.setValue(rec.get('AreaID'));
                                }
                            }
                        }
                    })();
                    break;

                case 'AreaID':
                    (function setAreaIDFields() {
                        var divisField = field.up('form').down('[name=DivisionID]'); //задает

                        if (field.isValid() && newValue) {
                            if (divisField) {
                                rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                if (rec) {
                                    divisField.suspendEvents();
                                    divisField.reset();
                                    divisField.resumeEvents();
                                    divisField.setValue(rec.get('DivisionID'));
                                }
                            }
                        }
                    })();
                    break;

                case 'SubChannelID':
                    (function setSubChannelIDFields() {
                        var chanField = field.up('form').down('[name=ChannelID]'); //задает
                        if (field.isValid() && newValue) {
                            if (chanField) {
                                rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                if (rec) {
                                    chanField.suspendEvents();
                                    chanField.reset();
                                    chanField.resumeEvents();
                                    chanField.setValue(rec.get('ChannelID'));
                                }
                            }
                        }
                    })();
                    break;

                case 'ChannelID':
                    (function setChannelIDFields() {
                        var schanField = field.up('form').down('[name=SubChannelID]'); //стирает
                        var schanFieldFieldValue;
                        var advChannelField = field.up('form').down('[name=AdvertisingChannelID]');
                        var advChannelFieldValue;

                        if (field.isValid() && newValue) {
                            rec = field.store.findRecord('ID', newValue, 0, false, false, true);

                            if (schanField) {
                                schanFieldFieldValue = schanField.store.findRecord('ID', schanField.value, 0, false, false, true);
                                if (schanFieldFieldValue) {
                                    if (schanFieldFieldValue.get('ChannelID') != newValue)
                                        schanField.setValue();
                                } else {
                                    schanField.setValue();
                                }
                            }

                            if (advChannelField) {
                                if (rec) {
                                    advChannelFieldValue = advChannelField.store.findRecord('BriefNotation', rec.get('BriefNotation'), 0, false, false, true);
                                    if (advChannelFieldValue) {
                                        advChannelField.suspendEvents();
                                        advChannelField.reset();
                                        advChannelField.resumeEvents();
                                        advChannelField.setValue(advChannelFieldValue.get('ID'));
                                    }
                                }
                            }
                        } else {
                            if (schanField) {
                                schanField.setValue();
                            }
                        }
                    })();
                    break;

                case 'NetID':
                    (function setNetIDFields() {
                        var form                    = field.up('form'),
                            nettypeField            = form.down('[name=NetTypeID]'),
                            respField               = form.down('[name=RespManagerID]'),
                            nielsField              = form.down('[name=isNielsen]'), //задает
                            storeAmountField        = form.down('[name=StoreAmount]'), //задает
                            advertisingChannelField = form.down('[name=AdvertisingChannel]'),
                            storeField              = form.down('[name=StoreID]');

                        if (field.isValid() && newValue) {
                            rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                            if (!rec) return;

                            if (nettypeField) {
                                nettypeField.suspendEvents();
                                nettypeField.reset();
                                nettypeField.resumeEvents();
                                nettypeField.setValue(rec.get('NetTypeID'));
                            }
                            if (respField) {
                                respField.suspendEvents();
                                respField.reset();
                                respField.resumeEvents();
                                respField.setValue(rec.get('RespManagerID'));
                            }
                            if (nielsField) {
                                nielsField.suspendEvents();
                                nielsField.reset();
                                nielsField.resumeEvents();
                                nielsField.setValue(rec.get('isNielsen'));
                            }
                            if (storeAmountField) {
                                storeAmountField.suspendEvents();
                                storeAmountField.reset();
                                storeAmountField.resumeEvents();
                                if (!field.up('window').isFormLoadingRecord) {
                                    storeAmountField.setValue(rec.get('StoresCount'));
                                }
                            }

                            if (advertisingChannelField) {
                                advertisingChannelField.suspendEvents();
                                advertisingChannelField.reset();
                                advertisingChannelField.resumeEvents();
                                advertisingChannelField.setValue(rec.get('AdvertisingChannel'));
                            }

                            if (storeField) {
                                storeField.suspendEvents();
                                storeField.reset();
                                storeField.resumeEvents();
                                storeField.allowBlank = true;
                                storeField.disable();
                            }
                        } else {
                            if (storeField) {
                                storeField.suspendEvents();
                                storeField.reset();
                                storeField.resumeEvents();
                                storeField.allowBlank = false;
                                storeField.enable();
                            }
                        }
                    })();
                    break;

                case 'TypeID':
                    (function setTypeIDFields() {
                        var considerInTeam = field.up('form').down('[name=ConsiderInTeamGroup]');

                        if (field.isValid() && newValue) {
                            if (newValue === 1)
                                considerInTeam.enable();
                            else
                                considerInTeam.disable();
                        } else {
                            considerInTeam.disable();
                        }
                    })();
                    break;

                case 'ManagersID':
                    (function setManagersIDFields() {
                        var dateHiredField = field.up('form').down('[name=DateHired]'),
                            dateFiredField = field.up('form').down('[name=DateFired]');

                        if (field.isValid() && newValue) {

                            if (dateHiredField) {
                                rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                if (rec) {
                                    dateHiredField.suspendEvents();
                                    dateHiredField.reset();
                                    dateHiredField.resumeEvents();
                                    dateHiredField.setValue(rec.get('DateHired'));
                                }
                            }
                            if (dateFiredField) {
                                rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                                if (rec) {
                                    dateFiredField.suspendEvents();
                                    dateFiredField.reset();
                                    dateFiredField.resumeEvents();
                                    dateFiredField.setValue(rec.get('DateFired'));
                                }
                            }
                        }
                    })();
                    break;

                case 'DistributorID':
                    (function setDistributorIDFields() {
                        var distrCodeField       = field.up('form').down('[name=DistrCode]'), //задает
                            contractorGroupField = field.up('form').down('[name=ContractorGroup]'); //задает

                        if (field.isValid() && newValue) {
                            rec = field.store.findRecord('ID', newValue, 0, false, false, true);

                            if (distrCodeField) {
                                if (rec) {
                                    distrCodeField.suspendEvents();
                                    distrCodeField.reset();
                                    distrCodeField.resumeEvents();
                                    distrCodeField.setValue(rec.get('Code'));
                                }
                            }

                            if (contractorGroupField) {
                                if (rec) {
                                    contractorGroupField.suspendEvents();
                                    contractorGroupField.reset();
                                    contractorGroupField.resumeEvents();
                                    contractorGroupField.setValue(rec.get('ContractorGroup'));
                                }
                            }
                        }
                    })();
                    break;

                case 'GoodID':
                    (function setGoodIDFields() {
                        var orimiCodeField    = field.up('form').down('[name=OrimiCode]'),
                            externalPackField = field.up('form').down('[name=ExternalPack]'),
                            brandField        = field.up('form').down('[name=Brand]'),
                            segmentField      = field.up('form').down('[name=Segment]'),
                            internalPackField = field.up('form').down('[name=InternalPack]');

                        if (field.isValid() && newValue) {
                            rec = field.store.findRecord('ID', newValue, 0, false, false, true);
                            if (!rec) return;

                            if (orimiCodeField) {
                                orimiCodeField.suspendEvents();
                                orimiCodeField.reset();
                                orimiCodeField.resumeEvents();
                                orimiCodeField.setValue(rec.get('Code'));
                            }
                            if (externalPackField) {
                                externalPackField.suspendEvents();
                                externalPackField.reset();
                                externalPackField.resumeEvents();
                                externalPackField.setValue(rec.get('ExternalPack'));
                            }
                            if (brandField) {
                                brandField.suspendEvents();
                                brandField.reset();
                                brandField.resumeEvents();
                                brandField.setValue(rec.get('Brand'));
                            }
                            if (segmentField) {
                                segmentField.suspendEvents();
                                segmentField.reset();
                                segmentField.resumeEvents();
                                segmentField.setValue(rec.get('Segment'));
                            }
                            if (internalPackField) {
                                internalPackField.suspendEvents();
                                internalPackField.reset();
                                internalPackField.resumeEvents();
                                internalPackField.setValue(rec.get('InternalPack'));
                            }
                        }
                    })();
                    break;

                case 'StoreID':
                    (function setStoreFields() {
                        var form                    = field.up('form'),
                            storeAmountField        = form.down('[name=StoreAmount]'),
                            advertisingChannelField = form.down('[name=AdvertisingChannel]'),
                            netField                = form.down('[name=NetID]');

                        if (storeAmountField) {
                            storeAmountField.suspendEvents();
                            storeAmountField.reset();
                            storeAmountField.resumeEvents();
                            storeAmountField.setValue(1);
                        }

                        if (netField) {
                            netField.suspendEvents();
                            netField.reset();
                            netField.resumeEvents();
                            netField.allowBlank = true;
                            netField.disable();
                        }

                        if (advertisingChannelField) {
                            advertisingChannelField.suspendEvents();
                            advertisingChannelField.reset();
                            advertisingChannelField.resumeEvents();
                        }

                        if (newValue) {
                            if (newValue === parseInt(newValue)) { //ID
                                if (!field.isValid()) {
                                    var params = {
                                        ID: newValue
                                    };

                                    var areaFilter = field.store.filters.findBy(function(item) {
                                        return item.id === 'ActiveStoresStore-AreaID-filter';
                                    });

                                    if (areaFilter) {
                                        params.AreaID = areaFilter.value;
                                    }

                                    field.store.load({
                                        params: params,
                                        callback: function (records, operation, success) {
                                            if (field.isValid()) {
                                                rec = field.store.findRecord('ID', newValue, 0, false, false, true);

                                                if (!rec) {
                                                    return;
                                                }

                                                if (storeAmountField) {
                                                    storeAmountField.suspendEvents();
                                                    storeAmountField.reset();
                                                    storeAmountField.resumeEvents();
                                                    storeAmountField.setValue(1);
                                                }

                                                if (advertisingChannelField) {
                                                    advertisingChannelField.suspendEvents();
                                                    advertisingChannelField.reset();
                                                    advertisingChannelField.resumeEvents();
                                                    advertisingChannelField.setValue(rec.get('AdvertisingChannel'));
                                                }
                                            }
                                        }
                                    });

                                } else {
                                    rec = field.store.findRecord('ID', newValue, 0, false, false, true);

                                    if (storeAmountField) {
                                        storeAmountField.suspendEvents();
                                        storeAmountField.reset();
                                        storeAmountField.resumeEvents();
                                        storeAmountField.setValue(1);
                                    }

                                    if (advertisingChannelField) {
                                        advertisingChannelField.suspendEvents();
                                        advertisingChannelField.reset();
                                        advertisingChannelField.resumeEvents();
                                        advertisingChannelField.setValue(rec.get('AdvertisingChannel'));
                                    }
                                }
                            } else { //Name
                                if (!field.queryFilter) {
                                    field.store.load({
                                        params: {
                                            Name: newValue
                                        },
                                        callback: function (records, operation, success) {
                                            rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                                            if (!rec) {
                                                return;
                                            }

                                            field.setValue(rec.get('ID'));
                                        }
                                    });
                                }
                            }
                        } else {
                            if (netField) {
                                netField.suspendEvents();
                                netField.reset();
                                netField.resumeEvents();
                                netField.allowBlank = false;
                                netField.enable();
                            }
                        }
                    })();
                    break;
            }

            this.fireEvent('orimi_afterModalcomboChange', field, newValue, oldValue);
        },
        //delay: 500,
        focus: function (me) {
            if (!me.isValid()) {
                me.expand();
            }
        }
    },

    onTriggerClick: function () {
        var me         = this,
            attribute  = me.attribute,
            userRights = TestOrimi.getApplication().data,
            editable   = true,
            filters,
            i          = 0;

        if (userRights['custom'] === 1) {
            editable = false;
        }

        me.duringTriggerClick = true;
        if (!me.readOnly && !me.disabled) {
            if (me.isExpanded) {
                me.collapse();
            }

            if (me.queryFilter) {
                me.queryFilter.setValue('');
            }

            filters = me.store.filters.items;

            if (filters.length) {
                for (i = 0; i < filters.length; i++) {
                    if (filters[i].property === 'Deleted' || filters[i].hasOwnProperty('defaultValue')) {
                        filters[i].setValue('');
                    }
                }
            }

            me.store.filter();

            switch (attribute) {
                case 'simple':
                    Ext.create('TestOrimi.view.SimpleAttributeWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Brand':
                    Ext.create('TestOrimi.view.BrandWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'SubBrand':
                    Ext.create('TestOrimi.view.SubBrandWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Segment':
                    Ext.create('TestOrimi.view.SegmentWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'SubSegment':
                    Ext.create('TestOrimi.view.SubSegmentWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'ParentID': //агрегирующая позиция
                    Ext.create('TestOrimi.view.ParentID', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'ParentStaff':
                    Ext.create('TestOrimi.view.ParentStaffWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Distributor':
                    Ext.create('TestOrimi.view.DistributorWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'WeightGroupID':
                    Ext.create('TestOrimi.view.WeightGroupID', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Net':
                    Ext.create('TestOrimi.view.NetWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'ParentNet':
                    Ext.create('TestOrimi.view.ParentNetWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Sector':
                    Ext.create('TestOrimi.view.SectorWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Area':
                    Ext.create('TestOrimi.view.AreaWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Division':
                    Ext.create('TestOrimi.view.DivisionWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'SubChannel':
                    Ext.create('TestOrimi.view.SubChannelWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'County':
                    Ext.create('TestOrimi.view.CountyWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Region':
                    Ext.create('TestOrimi.view.RegionWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'District':
                    Ext.create('TestOrimi.view.DistrictWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'City':
                    Ext.create('TestOrimi.view.CityWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Motivation':
                    Ext.create('TestOrimi.view.MotivationWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'OrimiGoodID':
                    Ext.create('TestOrimi.view.ParentID', {
                        title: 'Номенклатура',
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'AdvertisingChannel':
                    Ext.create('TestOrimi.view.AdvertisingChannelWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Store':
                    Ext.create('TestOrimi.view.StoreModalWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                default:
                    console.log('no attribute assined');
                    break;
            }
        }
        delete me.duringTriggerClick;
    },

    // overrided from the source
    doQuery: function (queryString, forceAll, rawQuery) {
        var me        = this,

            // Decide if, and how we are going to query the store
            queryPlan = me.beforeQuery({
                query: queryString || '',
                rawQuery: rawQuery,
                forceAll: forceAll,
                combo: me,
                cancel: false
            });

        // Allow veto.
        if (queryPlan === false || queryPlan.cancel) {
            return false;
        }

        // If they're using the same value as last time, just show the dropdown
        if (me.queryCaching && queryPlan.query === me.lastQuery) {
            me.expand();
            /*if (me.queryMode === 'local') {
             me.doAutoSelect();
             }*/
        }

        // Otherwise filter or load the store
        else {
            me.lastQuery = queryPlan.query;

            if (me.queryMode === 'local') {
                me.doLocalQuery(queryPlan);

            } else {
                me.doRemoteQuery(queryPlan);
            }
        }

        return true;
    },

    initComponent: function () {
        if (this.additionalListeners) {
            this.listeners = Ext.Object.merge(this.listeners, this.additionalListeners);
        }

        this.callParent(arguments);
    }
});

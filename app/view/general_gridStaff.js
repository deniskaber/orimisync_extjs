Ext.define('TestOrimi.view.general_gridStaff', {
    extend: 'Ext.grid.Panel',
    requires: ['TestOrimi.store.MasterStaffStore'],
    xtype: 'general_gridStaff',
    name: 'gridStaff',
    flex: 1,
    selModel: {
        selType: 'checkboxmodel'
    },
    columnLines: true,
    border: false,
    viewConfig: {
        stripeRows: false,
        getRowClass: function (record) {
            return record.get('StatusID') == 2 ? '' : (record.get('StatusID') == 1 ? 'temp-status-row' : 'del-status-row');
        },
        shrinkWrap: 0,
        shadow: false,
        trackOver: false,
        overItemCls: false
    },
    plugins: [{
        ptype: 'bufferedrenderer',
        trailingBufferZone: 0,
        leadingBufferZone: 0
    }],
    listeners: {
        celldblclick: function (grid, td, cellIndex, record, tr, rowIndex) {
            Ext.create('TestOrimi.view.createStaffWindow', {data: record}).show();
        }
    },
    initComponent: function () {
        var me = this;

        this.store = 'TestOrimi.store.MasterStaffStore';

        this.columns = [
            {text: 'Статус', dataIndex: 'Status', width: 160},
            {text: 'ID', dataIndex: 'ID', width: 80},
            {text: 'Код', dataIndex: 'Code', width: 130},
            {text: 'Фамилия', dataIndex: 'SurName', width: 160},
            {text: 'Имя', dataIndex: 'Name', width: 160},
            {text: 'Отчество', dataIndex: 'LastName', width: 160},
            {text: 'ФИО', dataIndex: 'FIO', width: 190},
            {text: 'Дата рождения', dataIndex: 'BirthDate', width: 150},
            {text: 'Тип', dataIndex: 'Type', width: 110},
            {text: 'Дивизион', dataIndex: 'Division', width: 200},
            {text: 'Дата начала работы', dataIndex: 'DateHired', width: 195},
            {text: 'Дата увольнения', dataIndex: 'DateFired', width: 195},
            {text: 'E-mail', dataIndex: 'EMail', width: 150},
            {text: 'Телефон', dataIndex: 'Phone', width: 130},
            {text: 'Комментарий', dataIndex: 'Comment', width: 120},
            {text: 'Мотивация', dataIndex: 'Motivation', width: 120},
            {text: 'Признак', dataIndex: 'FeatureName', width: 120},
            {text: 'Учитывать в команде', dataIndex: 'ConsiderInTeam', width: 120},
            {text: 'Сектор', dataIndex: 'Sector', width: 120},
            {text: 'Участок', dataIndex: 'Area', width: 120},
            {text: 'Кому подчиняется', dataIndex: 'Parent', width: 190},
            {text: 'Автор изменений', dataIndex: 'Author', width: 170},
            {text: 'Дата последнего изменения', dataIndex: 'DateChange', width: 200},
            {text: 'Источник данных', dataIndex: 'DataSource', width: 170}
        ];

        var i = 0,
            columns = this.columns,
            length = columns.length;

        for (; i < length; i++) {
            columns[i].items = [{
                xtype: 'container',
                margin: 4,
                flex: 1,
                layout: 'fit',
                listeners: {
                    scope: this,
                    element: 'el',
                    mousedown: function (e) {
                        e.stopPropagation();
                    },
                    click: function (e) {
                        e.stopPropagation();
                    },
                    keydown: function (e) {
                        e.stopPropagation();
                    },
                    keypress: function (e) {
                        e.stopPropagation();
                    },
                    keyup: function (e) {
                        e.stopPropagation();
                    }
                },
                items: [{
                    xtype: 'textfield',
                    emptyText: 'Поиск',
                    enableKeyEvents: true,
                    onTriggerClick: function () {
                        this.reset();
                        this.focus();
                    },
                    listeners: {
                        change: function (field, newValue, oldValue, eOpts) {
                            me.fireEvent('searchchange', me, field, newValue, oldValue);
                        },
                        buffer: 1000
                    }
                }]
            }];
        }

        this.lbar = [{
            xtype: 'general_gridLbar',
            ref: 'Staff',
            grid: this
        }];

        this.bbar = {
            xtype: 'pagingtoolbar',
            store: this.store,
            inputItemWidth: 60,
            displayInfo: true
        };

        this.callParent(arguments);

    }
});
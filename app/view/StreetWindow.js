Ext.define('TestOrimi.view.StreetWindow', {
    extend      : 'Ext.window.Window',
    xtype       : 'streetWindow',
    width       : '760px',
    height      : '525px',
    modal       : true,
    layout      : 'fit',

    initComponent: function() {
        var me = this,
            field = me.field;
        
        me.title = field.fieldLabel;
        
        me.items = [{   
            xtype       : 'grid'
            ,flex        : 1
            ,viewConfig: {
                stripeRows: false,
                getRowClass: function(record) {
                    return record.get('Deleted') == 1 ? 'disabled-row' : '';
                },
                shrinkWrap: 0,
                shadow: false,
                trackOver: false,
                overItemCls: false
            }
            ,border      : true
            ,columnLines : true
            ,columns: [
                         { text: 'ID',                dataIndex: 'ID',              width: 60}
                        ,{ text: 'Наименование',      dataIndex: 'Name',            width: 250} 
                        ,{ text: 'Краткое наим.',     dataIndex: 'ShortName',       width: 140}
                        ,{ text: 'Краткое обозн.',    dataIndex: 'BriefNotation',   width: 140}
                        ,{ text: 'Город',             dataIndex: 'City',            width: 140}
                        ,{ text: 'Удален',        
                            dataIndex: 'Deleted',       
                            width: 85,
                            renderer: function (val, meta, record) {
                                if (val == 1) {
                                    return '+';
                                }
                            }
                        }
            ]
            ,store: field.store
            ,listeners: {
                celldblclick: function(grid, td, cellIndex, record, tr, rowIndex) {
                    me.down('[name=okButton]').handler();
                }
            }
        }];

        var i = 0,
            columns = this.items[0].columns,
            length = columns.length;

        for (;i<length;i++) {
            columns[i].items = [{
        xtype: 'container',
        margin: 4,
        flex: 1,
        layout: 'fit',
        listeners: {
          scope: this,
          element: 'el',
          mousedown: function(e)
          {
              e.stopPropagation();
          },
          click: function(e)
          {
              e.stopPropagation();
          },
          keydown: function(e){
               e.stopPropagation();
          },
          keypress: function(e){
               e.stopPropagation();
          },
          keyup: function(e){
               e.stopPropagation();
          }
        },
                items:[{
          xtype: 'textfield',
                emptyText : 'Поиск',
                enableKeyEvents: true,
                onTriggerClick: function () {
                    this.reset();
                    this.focus();
                },
                listeners: {
                    change: function(field, newValue, oldValue, eOpts) {
                        var grid = this.up('grid');
                        me.fireEvent('searchchange', grid, field, newValue, oldValue);
            },
                    buffer: 1000
                }
        }]
            }];
        }

        me.buttons = [{
            text: 'Редактировать',
            handler: function() {
                var win = this.up('window'),
                    selected = win.down('grid').getSelectionModel().getSelection()[0];

                if (!selected) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
                } else {
                    Ext.create('TestOrimi.view.AddStreetWindow',{
                        title: 'Редактирование элемента'
                        ,store: field.store
                        ,data: selected
                    }).show();
                }
            }
        },{
            text: 'Создать',
            handler: function() {
                Ext.create('TestOrimi.view.AddStreetWindow',{
                    title: 'Создание нового элемента'
                    ,store: field.store
                }).show();
            }
        },
        '->'
        ,{
            text: 'Ок',
            name: 'okButton',
            handler: function() {
                var win = this.up('window'),
                    selected = win.down('grid').getSelectionModel().getSelection()[0];

                if (!selected) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
                } else {
                    if (selected.get('Deleted') == 1) {
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение помечено на удаление', 'winError');
                    } else {
                    var fieldName = me.field.name;

                    if (fieldName.substr(-2) == 'ID') {
                            win.field.setValue(selected.get('ID'));
                            //if (form.down('[name=CityID]')) { form.down('[name=CityID]').setValue(selected.get('CityID')); }
                    } else {
                            if (field.value != selected.get('Name')) {
                                win.field.setValue(selected.get('Name'));
                            }
                            //if (form.down('[name=CityID]')) { form.down('[name=City]').setValue(selected.get('City')); }
                    }
                win.destroy();
            }
                }
            }
        },{
            text    : 'Отмена', 
            handler: function() {
                this.up('window').destroy();
            }
        }];
    
        this.callParent(arguments);
    }
});
Ext.define('TestOrimi.view.AdvertisingBossRowsGrid', {
    extend: 'Ext.grid.Panel',
    requires: ['TestOrimi.store.AdvertisingBossRowsStore'],
    xtype: 'advertising_boss_rows_grid',
    flex: 1,
    columnLines: true,
    border: false,
    viewConfig: {
        stripeRows: false,
        shrinkWrap: 0,
        shadow: false,
        trackOver: false,
        overItemCls: false
    },
    plugins: [{
        ptype: 'bufferedrenderer',
        trailingBufferZone: 0,
        leadingBufferZone: 0
    }],
    rendererGridCellDigits: function(val) {
        if (val !== 0) {
            return Ext.util.Format.number(val, '0.000,00/i').replace(/\./g, ' ');
        }
    },
    initComponent: function() {
        var me = this;

        this.store = 'TestOrimi.store.AdvertisingBossRowsStore';

        this.columns = [
            {text: 'ID товара', dataIndex: 'GoodID', width: 160},
            {text: 'Наименование товара', dataIndex: 'GoodName', flex: 1},
            {text: 'Бренд', dataIndex: 'Brand', width: 130},
            {text: 'Код Орими', dataIndex: 'OrimiCode', width: 130},
            {text: 'ФГД, %', dataIndex: 'FGD', width: 140, align: 'right'},
            {text: 'Мин. цена, руб', dataIndex: 'MinPrice', width: 140, align: 'right', renderer: this.rendererGridCellDigits},
            {text: 'Цена БПЛ, руб', dataIndex: 'BPLPrice', width: 140, align: 'right', renderer: this.rendererGridCellDigits},
            {text: 'Макс. скидка, %', dataIndex: 'MaxDiscount', width: 140, align: 'right'}
        ];

        var i = 0,
            columns = this.columns,
            length = columns.length;

        for (; i < length; i++) {
            columns[i].items = [{
                xtype: 'container',
                margin: 4,
                flex: 1,
                layout: 'fit',
                listeners: {
                    scope: this,
                    element: 'el',
                    mousedown: function(e) {
                        e.stopPropagation();
                    },
                    click: function(e) {
                        e.stopPropagation();
                    },
                    keydown: function(e) {
                        e.stopPropagation();
                    },
                    keypress: function(e) {
                        e.stopPropagation();
                    },
                    keyup: function(e) {
                        e.stopPropagation();
                    }
                },
                items: [{
                    xtype: 'textfield',
                    emptyText: 'Поиск',
                    enableKeyEvents: true,
                    onTriggerClick: function() {
                        this.reset();
                        this.focus();
                    },
                    listeners: {
                        change: function(field, newValue, oldValue, eOpts) {
                            me.fireEvent('searchchange', me, field, newValue, oldValue);
                        },
                        buffer: 1000
                    }
                }]
            }];
        }

        this.bbar = {
            xtype: 'pagingtoolbar',
            store: this.store,
            inputItemWidth: 60,
            displayInfo: true
        };

        this.callParent(arguments);

    }
});

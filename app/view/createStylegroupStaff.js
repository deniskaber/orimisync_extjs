Ext.define('TestOrimi.view.createStylegroupStaff', {
    extend: 'Ext.window.Window',
    width: 500,
    title: 'Групповая установка свойств',
    modal: true,
    layout: 'fit',

    initComponent: function () {
        var myListener = {
            change: function (field, newValue, oldValue) {
                if (newValue) {
                    var toField = field.up('window').down('[name=' + field.name.substring(8) + ']');
                    toField.enable();
                } else {
                    var toField = field.up('window').down('[name=' + field.name.substring(8) + ']');
                    //toField.reset();
                    toField.disable();
                }
            }
        };

        Ext.getStore('TestOrimi.store.SelectedSectorStore').removeAll();
        Ext.getStore('TestOrimi.store.SelectedAreaStore').removeAll();

        this.items = [{
            xtype: 'form',
            padding: 10,
            autoScroll: true,
            border: false,
            layout: {
                type: 'column'
            },
            items: [{
                columnWidth: 0.05,
                defaults: {
                    xtype: 'checkboxfield',
                    listeners: myListener
                },
                items: [{
                    inputValue: 'SurName',
                    name: 'checkboxSurName'
                }, {
                    inputValue: 'Name',
                    name: 'checkboxName'
                }, {
                    inputValue: 'LastName',
                    name: 'checkboxLastName'
                }, {
                    inputValue: 'BirthDate',
                    name: 'checkboxBirthDate'
                }, {
                    inputValue: 'TypeID',
                    name: 'checkboxTypeID'
                }, {
                    inputValue: 'DivisionID',
                    name: 'checkboxDivisionID'
                }, {
                    inputValue: 'DateHired',
                    name: 'checkboxDateHired'
                }, {
                    inputValue: 'DateFired',
                    name: 'checkboxDateFired'
                }, {
                    inputValue: 'EMail',
                    name: 'checkboxEMail'
                }, {
                    inputValue: 'Phone',
                    name: 'checkboxPhone'
                }, {
                    inputValue: 'Comment',
                    name: 'checkboxComment',
                    margin: '0 0 72 0'
                }, {
                    inputValue: 'MotivationID',
                    name: 'checkboxMotivationID'
                }, {
                    inputValue: 'FeatureGroup',
                    name: 'checkboxFeatureGroup'
                }, {
                    inputValue: 'ConsiderInTeamGroup',
                    name: 'checkboxConsiderInTeamGroup'
                }, {
                    inputValue: 'Area',
                    name: 'checkboxArea'
                }, {
                    inputValue: 'Sector',
                    name: 'checkboxSector',
                    margin: '84 0 0 0'
                }, {
                    inputValue: 'ParentID',
                    name: 'checkboxParentID',
                    margin: '84 0 0 0'
                }]

            }, {
                columnWidth: 0.95,
                defaults: {
                    anchor: '90%',
                    labelWidth: 180
                    , disabled: true
                },
                layout: {
                    type: 'vbox'
                    , align: 'stretch'
                },
                items: [{
                    fieldLabel: 'Фамилия',
                    xtype: 'textfield',
                    name: 'SurName',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    allowBlank: (!this.hasActiveStatus)
                }, {
                    fieldLabel: 'Имя',
                    xtype: 'textfield',
                    name: 'Name'
                }, {
                    fieldLabel: 'Отчество',
                    xtype: 'textfield',
                    name: 'LastName'
                }, {
                    fieldLabel: 'Дата рождения',
                    xtype: 'datefield',
                    name: 'BirthDate',
                    format: 'Y-m-d'
                }, {
                    fieldLabel: 'Тип',
                    name: 'TypeID',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.TypeStaffStore',
                    allowBlank: (!this.hasActiveStatus)
                }, {
                    fieldLabel: 'Дивизион',
                    name: 'DivisionID',
                    attribute: 'Division',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.DivisionStore'
                }, {
                    fieldLabel: 'Дата начала работы',
                    xtype: 'datefield',
                    name: 'DateHired',
                    format: 'Y-m-d',
                    allowBlank: (!this.hasActiveStatus)
                }, {
                    fieldLabel: 'Дата увольнения',
                    xtype: 'datefield',
                    name: 'DateFired',
                    format: 'Y-m-d'
                }, {
                    fieldLabel: 'E-mail',
                    xtype: 'textfield',
                    name: 'EMail'
                }, {
                    fieldLabel: 'Телефон',
                    xtype: 'textfield',
                    name: 'Phone'
                }, {
                    xtype: 'textareafield',
                    fieldLabel: 'Комментарий',
                    name: 'Comment',
                    height: 90
                }, {
                    fieldLabel: 'Мотивация',
                    name: 'MotivationID',
                    attribute: 'Motivation',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.MotivationStore'
                }, {
                    fieldLabel: 'Признак',
                    xtype: 'radiogroup',
                    name: 'FeatureGroup',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    anchor: 'none',
                    layout: {
                        autoFlex: false
                    },
                    defaults: {
                        name: 'Feature',
                        margin: '0 15 0 0'
                    },
                    items: [{
                        boxLabel: 'Белый',
                        inputValue: 1
                    }, {
                        boxLabel: 'Серый',
                        inputValue: 2
                    }]
                }, {
                    fieldLabel: 'Учитывать в команде',
                    xtype: 'radiogroup',
                    anchor: 'none',
                    name: 'ConsiderInTeamGroup',
                    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                    layout: {
                        autoFlex: false
                    },
                    defaults: {
                        name: 'ConsiderInTeam',
                        margin: '0 15 0 0'
                    },
                    items: [{
                        boxLabel: 'да',
                        inputValue: 1
                    }, {
                        boxLabel: 'нет',
                        inputValue: 0
                    }]
                }, {
                    xtype: 'areaPanel',
                    name: 'Area'
                }, {
                    xtype: 'sectorPanel',
                    name: 'Sector'
                }, {
                    fieldLabel: 'Кому подчиняется',
                    name: 'ParentID',
                    attribute: 'ParentStaff',
                    displayField: 'FIO',
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.ParentStore',
                    margin: '4 0 0 0'
                }]
            }]
        }];

        this.buttons = [{
            text: 'Ок',
            handler: function () {
                var store = Ext.getStore('TestOrimi.store.MasterStaffStore'),
                    win = this.up('window'),
                    form = win.down('form').getForm();

                //get cleared form values
                var rawVals = form.getValues(),
                    radiogroups = ['Feature', 'ConsiderInTeam'],
                    params = {};

                Ext.Object.each(rawVals, function (key, value, myself) {
                    /*if (Ext.Array.contains(radiogroups,key)) {
                     params[key] = value;
                     } else {
                     if (value) {
                     params[key] = value;
                     }
                     }*/
                    params[key] = value;
                });
                //

                var formdown = win.down('form'),
                    sectorRecords = formdown.down('[name=sector]').store.data.items,
                    sectorLength = sectorRecords.length,
                    i = 0;

                if (formdown.down('[name=checkboxSector]').getValue() || formdown.down('[name=checkboxArea]').getValue()) {
                    params['Sector'] = [];
                    for (i = 0; i < sectorLength; i++) {
                        params['Sector'].push(sectorRecords[i].get('ID'));
                    }
                    params['Sector'] = params['Sector'].toString();
                }

                var areaRecords = formdown.down('[name=area]').store.data.items,
                    areaLength = areaRecords.length;

                if (formdown.down('[name=checkboxArea]').getValue() || formdown.down('[name=checkboxSector]').getValue()) {
                    params['Area'] = [];
                    for (i = 0; i < areaLength; i++) {
                        params['Area'].push(areaRecords[i].get('ID'));
                    }
                    params['Area'] = params['Area'].toString();
                }

                params['ID'] = win.records.toString();
                params['act'] = store.proxy.extraParams['act'];
                params['subaction'] = 'groupUpdate';


                if (form.isValid()) {
                    var myMask = new Ext.LoadMask(win, {msg: "Сохраняю..."});

                    myMask.show();
                    Ext.Ajax.request({
                        url: 'resources/data/api.php',
                        params: params,
                        success: function (response) {
                            myMask.destroy();
                            TestOrimi.getApplication().getController('Main').showNotification('Успешно! Элементы обновлены', 'winSuccess');
                            store.load();
                            win.destroy();
                        },
                        failure: function (form, action) {
                            myMask.destroy();
                            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Нет ответа от сервера', 'winError');
                        }
                    });
                }
            }
        }, {
            text: 'Отмена',
            handler: function () {
                this.up('window').destroy();
            }
        }];

        this.callParent(arguments);
    }

});
Ext.define('TestOrimi.view.DivisionWindow', {
    extend: 'Ext.window.Window',
    xtype: 'divisionWindow',
    width: 690,
    height: 600,
    title  : 'Дивизион',
    modal: true,
    layout: 'fit',
    editable: true,

    initComponent: function () {
        var me = this,
            field = me.field || me.button,
            secStore = 'TestOrimi.store.DivisionStore',
            haveRights = TestOrimi.getApplication().data['Attributes_Division'] == 2;

        me.items = [{
            xtype: 'grid'
            , flex: 1
            , viewConfig: {
                stripeRows: false,
                getRowClass: function (record) {
                    return record.get('Deleted') == 1 ? 'disabled-row' : '';
                },
                shrinkWrap: 0,
                shadow: false,
                trackOver: false,
                overItemCls: false
            }
            , border: true
            , columnLines: true
            , columns: [
                {text: 'ID', dataIndex: 'ID', width: 60}
                , {text: 'Наименование', dataIndex: 'Name', width: 250}
                , {text: 'Краткое наим.', dataIndex: 'ShortName', width: 140}
                , {text: 'Краткое обозн.', dataIndex: 'BriefNotation', width: 140}
                , {text: 'Департамент из TradeNT', dataIndex: 'TradentDepartment', width: 260}
                , {
                    text: 'Удален',
                    dataIndex: 'Deleted',
                    width: 85,
                    renderer: function (val, meta, record) {
                        if (val == 1) {
                            return '+';
                        }
                    }
                }
            ]
            , store: secStore
            , listeners: {
                celldblclick: function (grid, td, cellIndex, record, tr, rowIndex) {
                    if (me.field || me.selector) {
                        me.down('[name=okButton]').handler();
                    } else {
                        if (me.button && haveRights) {
                            me.down('[name=editButton]').handler();
                        }
                    }
                }
            }
        }];

        me.buttons = [{
            text: 'Редактировать',
            name: 'editButton',
            disabled: !me.editable || !haveRights,
            handler: function () {
                var win = this.up('window'),
                    grid = win.down('grid'),
                    selected = grid.getSelectionModel().getSelection()[0];

                if (!selected) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
                } else {
                    Ext.create('TestOrimi.view.AddDivisionWindow', {
                        title: 'Редактирование элемента'
                        , store: secStore
                        , data: selected
                    }).show();
                }
            }
        }, {
            text: 'Создать',
            disabled: !me.editable || !haveRights,
            handler: function () {
                Ext.create('TestOrimi.view.AddDivisionWindow', {
                    title: 'Создание нового элемента'
                    , store: secStore
                }).show();
            }
        }, '->', {
            text: 'Ок',
            name: 'okButton',
            disabled: me.button,
            handler: function () {
                var win = this.up('window'),
                    grid = win.down('grid'),
                    selected = grid.getSelectionModel().getSelection()[0];

                if (!selected) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
                } else {
                    if (selected.get('Deleted') == 1) {
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение помечено на удаление', 'winError');
                    } else {
                        if (win.field) {
                            var fieldName = me.field.name;
                            if (fieldName.substr(-2) == 'ID') {
                                if (field.value != selected.get('ID')) {
                                    win.field.setValue(selected.get('ID'));
                                }
                            } else {
                                if (field.value != selected.get('Name')) {
                                    win.field.setValue(selected.get('Name'));
                                }
                            }
                        }
                    }
                    win.destroy();
                }
            }
        }, {
            text: me.field ? 'Отмена' : 'Закрыть',
            handler: function () {
                this.up('window').destroy();
            }
        }];

        var i = 0,
            columns = this.items[0].columns,
            length = columns.length;

        for (; i < length; i++) {
            columns[i].items = [{
                xtype: 'container',
                margin: 4,
                flex: 1,
                layout: 'fit',
                listeners: {
                    scope: this,
                    element: 'el',
                    mousedown: function (e) {
                        e.stopPropagation();
                    },
                    click: function (e) {
                        e.stopPropagation();
                    },
                    keydown: function (e) {
                        e.stopPropagation();
                    },
                    keypress: function (e) {
                        e.stopPropagation();
                    },
                    keyup: function (e) {
                        e.stopPropagation();
                    }
                },
                items: [{
                    xtype: 'textfield',
                    emptyText: 'Поиск',
                    enableKeyEvents: true,
                    onTriggerClick: function () {
                        this.reset();
                        this.focus();
                    },
                    listeners: {
                        change: function (field, newValue, oldValue, eOpts) {
                            var grid = this.up('grid');
                            me.fireEvent('searchchange', grid, field, newValue, oldValue);
                        },
                        buffer: 1000
                    }
                }]
            }];
        }

        this.callParent(arguments);
    }
});
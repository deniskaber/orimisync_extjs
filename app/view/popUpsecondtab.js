Ext.define('TestOrimi.view.popUpsecondtab', {
	extend      : 'Ext.container.Container',
	xtype       : 'popUpsecondtab',
	border      : false,
	margin      : '20 20 0 20',
	layout      : 'column',
	items:  [{
		title       : '<p style="font-size: 16px;">Тип упаковки</p>',
		xtype       :'fieldset',
		columnWidth : 0.5,
		defaults    : {anchor: '100%'},
		border      : false,
		defaultType : 'textfield',
		items: [{
			fieldLabel  : 'Внешняя',
			value		: 6,
			name        : 'ExternalPackID',
			afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
			xtype       : 'modalcombo',
			store       : 'TestOrimi.store.ExternalPackStore'
		},{
			fieldLabel  : 'Саше (sachet)',
			value		: 4,
			name        : 'SachetPackID',
			afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
			xtype       : 'modalcombo',
			store       : 'TestOrimi.store.SachetPackStore'
		},{
			fieldLabel  : 'Внутренняя',
			value		: 13,
			name        : 'InternalPackID',
			afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
			xtype       : 'modalcombo',
			store       : 'TestOrimi.store.InternalPackStore'
		},{
			title       : '<p style="font-size: 16px;">Вес</p>',
			columnWidth : 0.5,
			xtype       :'fieldset',
			defaultType : 'textfield',
			defaults    : {anchor: '100%'},
			border      : false,
			margin      : '0 -10 0 -10',
			items: [{
				fieldLabel  : 'Группа:',
				name        : 'WeightGroupID',
				attribute   : 'WeightGroupID',
				xtype       : 'modalcombo',
				store       : 'TestOrimi.store.WeightGroupStore'
			},{
				xtype       	: 'numberfield',
				cls				: 'noteditable',
				fieldLabel  	: 'Вес (общий)',
				decimalSeparator: '.',
				allowDecimals	: true,
				name        	: 'Weight',
				readOnly    	: true,
				minValue    	: 0
			},{
				xtype       	: 'numberfield',
				fieldLabel  	: 'Вес пакета:',
				decimalSeparator: '.',
				allowDecimals	: true,
				step						: 0.1,
				name        	: 'BagWeight',
				afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
				minValue    	: 0
			},{
				xtype       : 'numberfield',
				fieldLabel  : 'Кол-во пакетов:',
				name        : 'BagQty',
				afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
				value		: 1,
				minValue    : 1
			},{
				fieldLabel  : 'Весовой продукт в сети',
				xtype       : 'radiogroup',
				defaultType : 'radiofield',
				vertical    : true,
				columns     : 2,
				layout      : 'hbox',
				items: [{
					boxLabel    : 'да',
					name        : 'isWeightNet',
					inputValue  : 1
				},{
					boxLabel    : 'нет',
					name        : 'isWeightNet',
					inputValue  : 0,
					checked     : true,
					margin      : '0 0 0 20'
				}]
			}]
		}]
	},{
		title       : '<p style="font-size: 16px;">Другое</p>',
		columnWidth : 0.5,
		xtype       :'fieldset',
		defaultType : 'textfield',
		defaults    : {anchor: '100%'},
		border      : false,
		items: [{
			fieldLabel  : 'Упаковка Хорека',
			xtype       : 'radiogroup',
			defaultType : 'radiofield',
			afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
			vertical    : true,
			labelWidth  : 130,
			columns     : 2,
			layout      : 'hbox',
			items: [{
				boxLabel    : 'с упаковкой',
				name        : 'isHorekaPack',
				inputValue  : 1
			},{
				boxLabel    : 'без упаковки',
				name        : 'isHorekaPack',
				inputValue  : 0,
				checked     : true,
				margin      : '0 0 0 20'
			}]
		},{
			fieldLabel  : 'Акционная позиция',
			labelWidth  : 130,
			name        : 'GiftID',
			afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
			xtype       : 'modalcombo',
			value		: 4,
			store       : 'TestOrimi.store.GiftStore'
		},{
			fieldLabel  : 'Тип подарка',
			labelWidth  : 130,
			name        : 'GiftTypeID',
			xtype       : 'modalcombo',
			store       : 'TestOrimi.store.GiftTypeStore'
		},{
			fieldLabel  : 'Частная марка сети',
			xtype       : 'radiogroup',
			defaultType : 'radiofield',
			afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
			vertical    : true,
			labelWidth  : 130,
			columns     : 2,
			layout      : 'hbox',
			items: [{
				boxLabel    : 'да',
				name        : 'isPrivateMark',
				inputValue  : 1
			},{
				boxLabel    : 'нет',
				name        : 'isPrivateMark',
				inputValue  : 0,
				checked     : true,
				margin      : '0 0 0 20'
			}]
		}]
	}]
});

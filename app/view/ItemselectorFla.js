Ext.define('TestOrimi.view.ItemselectorFla', {
  extend  : 'Ext.container.Container',
  xtype   : 'itemselectorFla',
  name    : 'isWithFlavorGroup',
  border  : false,
  layout  : {
    type  : 'vbox',
    align : 'stretch'
  },
  width: 100,
  items: [{
    fieldLabel  : 'Аромат',
    xtype       : 'radiogroup',
    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
    anchor      : 'none',
      layout    : {
        autoFlex: false
      },
      defaults: {
        name  : 'isWithFlavor',
        margin  : '0 15 0 0'
      },
    items: [{
      boxLabel    : 'с ароматом',
      inputValue  : 1
    },{
      boxLabel    : 'без аромата',
      inputValue  : 0
    }],
    listeners: {
      change: function(field, newValue, oldValue) {
        if (newValue['isWithFlavor']) {
          field.up('itemselectorFla').down('[name=flavorSelectors]').enable();
        } else {
          field.up('itemselectorFla').down('[name=flavorSelectors]').disable();
        }
      }
    }
  },{
    xtype: 'container',
    name: 'flavorSelectors',
    disabled    : true,
    layout  : {
      type  : 'hbox',
      align : 'stretch',
      height: 100
    },
    items: [{
      xtype: 'buttongroup',
      name: 'FlavorButtonGgroup',
      frame: false,
      columns: 1,
      border: false,
      items:[{
        xtype     : 'button',
        text      : '<span class="oi oiadd" data-glyph="plus" aria-hidden="true"></span>',
        height    : 37,
        tooltip   : 'Добавить',
        scale     : 'medium',
        handler: function() {
          Ext.create('TestOrimi.view.FlavorWindow').show();
        }
      },{
        xtype     : 'button',
        margin    : '5 0 0 0',
        height    : 37,
        text      : '<span class="oi oiadd" data-glyph="trash" aria-hidden="true"></span>',
        tooltip   : 'Удалить',
        scale     : 'medium',
        handler: function() {
          var container = this.up('itemselectorFla'),
              listadd = container.down('boundlist'),
              selected = listadd.getSelectionModel().getSelection()[0];

            if (selected) {
                  Ext.getStore('TestOrimi.store.SelectedFlavorStore').remove(selected);
            } else {
                  TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Аромат не выделен', 'winError');
            }
          }
      }]
    },{
      xtype       : 'boundlist',
      anchor      : '100%',
      attribute   : 'FlavorID',
      margin      : '0 0 0 20',
      height      : 92,
      flex        : 1,
      displayField: 'Name',
      valueField  : 'ID',
      store       : 'TestOrimi.store.SelectedFlavorStore',
      name        : 'flavor',
      queryMode   : 'local',
      multiSelect : true
    }]
  }]
});

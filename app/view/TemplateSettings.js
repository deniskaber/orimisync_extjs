Ext.define('TestOrimi.view.TemplateSettings', {
  extend  : 'Ext.window.Window',
	xtype		: 'templateSettings',
	width   : 600,
  height  : 525,
  title		: 'Сохранение шаблона настроек',
	modal		: true,
	layout	: 'fit',
  //ref     : 'Sku',

  initComponent: function() {
    var me = this,
        ref = me.Ref;

    me.items = [{
      xtype        : 'grid'
      ,flex        : 1
      ,border      : true
      ,columnLines : true
      ,columns: [
        { text: 'Название',             dataIndex: 'Name',flex: 1 },
        { text: 'Дата создания',		    dataIndex: 'DateCreate', width: 160 },
        { text: 'Дата использования',   dataIndex: 'DateUsed', width: 160 }
      ]
      ,store: 'TestOrimi.store.TemplateStore'
    }];

		me.buttons = [{
  		text: 'Сохранить',
      handler: function() {
          this.up('window').fireEvent('savebuttonclick', this.up('window'), ref);
      }
		},{
      text: 'Загрузить',
      handler: function() {
          this.up('window').fireEvent('loadbuttonclick', this.up('window'), ref);
      }
    },{
      text: 'Удалить',
      handler: function() {
          var win = this.up('window'),
              grid = win.down('grid');

          this.up('window').fireEvent('deletebuttonclick', win, grid, ref);
      }
    },'->',{
			text: 'Отмена',
			handler: function() {
          this.up('window').destroy();
      }
		}];

		this.callParent(arguments);
  }
});


Ext.define('TestOrimi.view.createSkuWindow', {
    extend: 'Ext.window.Window',
    name: 'createSkuWindow',
    width: 950,
    height: 670,
    modal: true,
    title: 'Номенклатура',
    layout: 'fit',
    items: [{
        xtype: 'form',
        bodyPadding: 10,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'container',
            border: false,
            layout: 'column',
            items: [{
                xtype: 'fieldset',
                columnWidth: 0.5,
                defaults: {
                    xtype: 'textfield'
                    , anchor: '100%'
                    , labelWidth: 160
                },
                border: false,
                items: [{
                    cls: 'noteditable',
                    name: 'ID',
                    fieldLabel: 'ID',
                    readOnly: true
                }, {
                    fieldLabel: 'Код',
                    name: 'Code'
                }, {
                    cls: 'noteditable',
                    xtype: 'textareafield',
                    name: 'Name',
                    fieldLabel: 'Полное название',
                    readOnly: true,
                    anchor: '100%'
                }]
            }, {
                columnWidth: 0.5,
                xtype: 'fieldset',
                defaults: {
                    xtype: 'textfield'
                    , anchor: '100%'
                    , labelWidth: 160
                },
                border: false,
                items: [{
                    fieldLabel: 'Черновое название',
                    name: 'TempName'
                }, {
                    fieldLabel: 'Агрегирующая позиция',
                    name: 'ParentID',
                    attribute: 'ParentID',
                    xtype: 'modalcombo',
                    store: 'TestOrimi.store.GoodParentStore'
                }, {
                    cls: 'noteditable',
                    xtype: 'textareafield',
                    name: 'ShortName',
                    fieldLabel: 'Короткое название',
                    readOnly: true,
                    anchor: '100%'
                }]
            }]
        }, {
            xtype: 'tabpanel',
            plain: true,
            items: [{
                xtype: 'popUpgeneraltab',
                title: 'Главные характеристики'
            }, {
                xtype: 'popUpsecondtab',
                title: 'Описание товара'
            }, {
                xtype: 'popUpthirdtab',
                title: 'Дополнительно'
            }]
        }]
    }],

    buttons: [{
        text: 'Ок',
        name: 'okButton',
        handler: function() {
            var store = Ext.getStore('TestOrimi.store.MasterGoodStore'),
                win = this.up('window'),
                form = win.down('form').getForm(),
                formdown = win.down('form'),
                addingRecords = formdown.down('[name=adding]').store.data.items,
                flavorRecords = formdown.down('[name=flavor]').store.data.items,
                barcodeRecords = formdown.down('[name=barcode]').store.data.items,
                lengthAdd = addingRecords.length,
                lengthFla = flavorRecords.length,
                lengthBarcode = barcodeRecords.length,
                i = 0,
                addings = [],
                flavors = [],
                barcodes = [];

            if (formdown.down('itemselectorAdd').down('radiogroup').getValue()['isWithAdding'])
                for (; i < lengthAdd; i++) {
                    addings.push(addingRecords[i].get('ID'));
                }

            if (formdown.down('itemselectorFla').down('radiogroup').getValue()['isWithFlavor'])
                for (i = 0; i < lengthFla; i++) {
                    flavors.push(flavorRecords[i].get('ID'));
                }

            if (lengthBarcode) {
                for (i = 0; i < lengthBarcode; i++) {
                    barcodes.push(barcodeRecords[i].get('ID'));
                }
            }
            /* else {
             if (win.data.get('Barcode').trim().length)
             barcodes.push('blank');
             }*/

            if (form.isValid()) {
                var myMask = new Ext.LoadMask(win, {msg: "Сохраняю..."});

                myMask.show();
                form.submit({
                    url: 'resources/data/api.php',
                    params: {
                        act: store.proxy.extraParams['act'],
                        subaction: (win.data) ? (win.data.get('ID') ? 'update' : 'create') : 'create',
                        DataSourceID: (win.data) ? win.data.get('DataSourceID') : 0,
                        Adding: addings.toString(),
                        Flavor: flavors.toString(),
                        Barcode: barcodes.toString()
                    },
                    success: function(response) {
                        myMask.destroy();
                        if (win.data) {
                            if (win.data.get('ID') == 0) {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess');
                            } else {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Элемент обновлен', 'winSuccess');
                            }
                        } else {
                            TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess');
                        }
                        store.load();
                        win.destroy();
                    },
                    failure: function(form, action) {
                        myMask.destroy();
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                    }
                });
            }
        }
    }, {
        text: 'Отмена',
        handler: function() {
            this.up('window').destroy();
        }
    }],

    listeners: {
        beforedestroy: function(win) {
            Ext.getStore('TestOrimi.store.BarcodeStore').removeAll();
            Ext.getStore('TestOrimi.store.SelectedAddingStore').removeAll();
            Ext.getStore('TestOrimi.store.SelectedFlavorStore').removeAll();
        }
    },

    initComponent: function() {
        var me = this,
            userRights = TestOrimi.getApplication().data;

        this.callParent(arguments);

        if (this.data) {
            this.down('form').loadRecord(this.data);

            //console.log('model loaded');

            var dataGoodID = this.data.get('ID'),
                barcodes = this.data.get('Barcode') ? this.data.get('Barcode').trim() : '',
                addings = this.data.get('Adding') ? this.data.get('Adding').trim() : '',
                flavors = this.data.get('Flavor') ? this.data.get('Flavor').trim() : '',
                addingsID = this.data.get('AddingID') ? this.data.get('AddingID').trim() : '',
                flavorsID = this.data.get('FlavorID') ? this.data.get('FlavorID').trim() : '';

            Ext.getStore('TestOrimi.store.BarcodeStore').removeAll();
            if (barcodes.length) {
                var barcodeData = barcodes.split(','),
                    blength = barcodeData.length;
                for (i = 0; i < blength; i++) {
                    barcodeData[i] = {ID: barcodeData[i]};
                }
                Ext.getStore('TestOrimi.store.BarcodeStore').loadData(barcodeData);
            } else {
                Ext.getStore('TestOrimi.store.BarcodeStore').removeAll();
            }

            if (addingsID.length) {
                var addings = addings.split(','),
                    addingsID = addingsID.split(','),
                    alength = addingsID.length,
                    addingsData = [];

                for (i = 0; i < alength; i++) {
                    addingsData[i] = {ID: addingsID[i], Name: addings[i]};
                }
                Ext.getStore('TestOrimi.store.SelectedAddingStore').loadData(addingsData);
            } else {
                Ext.getStore('TestOrimi.store.SelectedAddingStore').removeAll();
            }

            if (flavorsID.length) {
                var flavors = flavors.split(','),
                    flavorsID = flavorsID.split(','),
                    flength = flavorsID.length,
                    flavorsData = [];

                for (i = 0; i < flength; i++) {
                    flavorsData[i] = {ID: flavorsID[i], Name: flavors[i]};
                }
                Ext.getStore('TestOrimi.store.SelectedFlavorStore').loadData(flavorsData);
            } else {
                Ext.getStore('TestOrimi.store.SelectedFlavorStore').removeAll();
            }

            if (this.data.get('ID') == 0) {
                this.down('form').down('[name=ID]').reset();
            }

            if (this.data.get('ParentID') == 0) {
                this.down('form').down('[name=ParentID]').reset();
            }

            if (this.data.get('From') == 0) {
                this.down('form').getForm().setValues('From', 0);
            }

            if (this.data.get('StatusID') == 3) { //удаленная позиция
                var form = this.down('form');
                var fields = form.query('textfield');
                var mcombos = form.query('modalcombo');
                var radios = form.query('radio');
                var radiogroups = form.query('radiogroup');
                var buttons = form.query('buttongroup');
                var barcodePanel = form.down('barcodePanel');

                form.addCls('noteditable');

                for (var i = 0; i < mcombos.length; i++) {
                    mcombos[i].readOnly = true;
                    mcombos[i].hideTrigger = true;
                }

                for (var i = 0; i < fields.length; i++) {
                    fields[i].addCls('noteditable');
                    fields[i].readOnly = true;
                }

                for (var i = 0; i < radios.length; i++) {
                    radios[i].addCls('noteditable');
                    radios[i].readOnly = true;
                }

                for (var i = 0; i < radiogroups.length; i++) {
                    radiogroups[i].addCls('noteditable');
                    radiogroups[i].readOnly = true;
                }

                for (var i = 0; i < buttons.length; i++) {
                    buttons[i].disable();
                }

                barcodePanel.addCls('noteditable');
                barcodePanel.readOnly = true;
                barcodePanel.hideTrigger = true;

                this.down('[name=okButton]').disable();
            } else {
                if ((this.data.get('StatusID') == 1 && !userRights['Sku_draft_set']) || (this.data.get('StatusID') == 2 && !userRights['Sku_active_set'])) { //редактирование отключено в правах
                    var form = this.down('form');
                    var fields = form.query('textfield');
                    var mcombos = form.query('modalcombo');
                    var radios = form.query('radio');
                    var radiogroups = form.query('radiogroup');
                    var buttons = form.query('buttongroup');
                    var barcodePanel = form.down('barcodePanel');

                    var disableOkButtonflag = true;

                    //form.addCls('noteditable');

                    for (var i = 0; i < mcombos.length; i++) {
                        if (userRights['Sku_' + mcombos[i].name] == 1) {
                            //console.log('Sku_'+mcombos[i].name);
                            disableOkButtonflag = false;
                        } else {
                            mcombos[i].addCls('noteditable');
                            mcombos[i].readOnly = true;
                            mcombos[i].hideTrigger = true;
                        }
                    }

                    for (var i = 0; i < fields.length; i++) {
                        if (userRights['Sku_' + fields[i].name] == 1) {
                            disableOkButtonflag = false;
                        } else {
                            fields[i].addCls('noteditable');
                            fields[i].readOnly = true;
                        }
                    }

                    for (var i = 0; i < radios.length; i++) {
                        radios[i].addCls('noteditable');
                        radios[i].readOnly = true;
                    }

                    for (var i = 0; i < radiogroups.length; i++) {
                        radiogroups[i].addCls('noteditable');
                        radiogroups[i].readOnly = true;
                    }

                    for (var i = 0; i < buttons.length; i++) {
                        buttons[i].disable();
                    }

                    barcodePanel.addCls('noteditable');
                    barcodePanel.readOnly = true;
                    barcodePanel.hideTrigger = true;

                    if (disableOkButtonflag)
                        this.down('[name=okButton]').disable();
                }

            }
        } else {
            this.down('itemselectorAdd').down('radiogroup').setValue({isWithAdding: 0});
            this.down('itemselectorFla').down('radiogroup').setValue({isWithFlavor: 0});

            Ext.getStore('TestOrimi.store.BarcodeStore').removeAll();
            Ext.getStore('TestOrimi.store.SelectedAddingStore').removeAll();
            Ext.getStore('TestOrimi.store.SelectedFlavorStore').removeAll();
        }
    }
});

Ext.define('TestOrimi.view.FlavorWindow', {
    extend: 'Ext.window.Window',
    xtype: 'flavorWindow',
    width: 690,
    title: 'Ароматы',
    height: 600,
    modal: true,
    layout: 'fit',

    initComponent: function () {
        var me = this,
            addStore = 'TestOrimi.store.FlavorStore',
            userRights = TestOrimi.getApplication().data,
            haveRights = userRights['Attributes_Flavor'] == 2;

        me.items = [{
            xtype: 'grid'
            , flex: 1
            , viewConfig: {
                stripeRows: false,
                getRowClass: function (record) {
                    return record.get('Deleted') == 1 ? 'disabled-row' : '';
                },
                shrinkWrap: 0,
                shadow: false,
                trackOver: false,
                overItemCls: false
            }
            , border: true
            , columnLines: true
            , columns: [
                {text: 'ID', dataIndex: 'ID', width: 60}
                , {text: 'Наименование', dataIndex: 'Name', width: 250}
                , {text: 'Краткое наим.', dataIndex: 'ShortName', width: 140}
                , {text: 'Краткое обозн.', dataIndex: 'BriefNotation', width: 140}
                , {text: 'Удален', dataIndex: 'Deleted', width: 100}
            ]
            , store: addStore
            , listeners: {
                celldblclick: function (grid, td, cellIndex, record, tr, rowIndex) {
                    if (me.button && haveRights) {
                        me.down('[name=editButton]').handler();
                    } else {
                        me.down('[name=okButton]').handler();
                    }
                }
            }
        }];

        me.buttons = [{
            text: 'Редактировать',
            name: 'editButton',
            disabled: !haveRights,
            handler: function () {
                var win = this.up('window'),
                    grid = win.down('grid'),
                    selected = grid.getSelectionModel().getSelection()[0];

                if (!selected) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
                } else {
                    Ext.create('TestOrimi.view.AddFlavorWindow', {
                        title: 'Редактирование элемента'
                        , store: addStore
                        , data: selected
                    }).show();
                }
            }
        }, {
            text: 'Создать',
            disabled: !haveRights,
            handler: function () {
                Ext.create('TestOrimi.view.AddFlavorWindow', {
                    title: 'Создание нового элемента'
                    , store: addStore
                }).show();
            }
        }, '->', {
            text: 'Ок',
            name: 'okButton',
            disabled: me.button,
            handler: function () {
                var win = this.up('window'),
                    selected = win.down('grid').getSelectionModel().getSelection()[0];

                if (!selected) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Значение не выбрано', 'winError');
                } else {
                    if (Ext.getStore('TestOrimi.store.SelectedFlavorStore').findExact('ID', selected.get('ID')) < 0)
                        Ext.getStore('TestOrimi.store.SelectedFlavorStore').add({
                            Name: selected.get('Name'),
                            ID: selected.get('ID')
                        });

                    win.destroy();
                }
            }
        }, {
            text: me.field ? 'Отмена' : 'Закрыть',
            handler: function () {
                this.up('window').destroy();
            }
        }];

        var i = 0,
            columns = this.items[0].columns,
            length = columns.length;

        for (; i < length; i++) {
            columns[i].items = [{
                xtype: 'container',
                margin: 4,
                flex: 1,
                layout: 'fit',
                listeners: {
                    scope: this,
                    element: 'el',
                    mousedown: function (e) {
                        e.stopPropagation();
                    },
                    click: function (e) {
                        e.stopPropagation();
                    },
                    keydown: function (e) {
                        e.stopPropagation();
                    },
                    keypress: function (e) {
                        e.stopPropagation();
                    },
                    keyup: function (e) {
                        e.stopPropagation();
                    }
                },
                items: [{
                    xtype: 'textfield',
                    emptyText: 'Поиск',
                    enableKeyEvents: true,
                    onTriggerClick: function () {
                        this.reset();
                        this.focus();
                    },
                    listeners: {
                        change: function (field, newValue, oldValue, eOpts) {
                            var grid = this.up('grid');
                            me.fireEvent('searchchange', grid, field, newValue, oldValue);
                        },
                        buffer: 1000
                    }
                }]
            }];
        }

        this.callParent(arguments);
    }
});

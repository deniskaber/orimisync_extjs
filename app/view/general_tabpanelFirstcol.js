Ext.define('TestOrimi.view.general_tabpanelFirstcol', {
  extend      : 'Ext.grid.Panel',
  xtype       : 'general_tabpanelFirstcol',
  selType     : 'checkboxmodel',
  columnLines : true,

  initComponent: function() {
    var me = this,
        userRights = TestOrimi.getApplication().data;
    
    this.store = 'TestOrimi.store.SyncedGoodStore';

    this.columns  = [
      { text: 'ID',                   dataIndex: 'ID',            width: 80  },
      { text: 'Код',                  dataIndex: 'Code',          width: 80  },
      { text: 'Код Орими',            dataIndex: 'OrimiCode',     width: 100 },
      { text: 'Название',             dataIndex: 'Name',          width: 250,
        renderer: function (value, meta, record) {
					value = value.replace(/"/g, '\'');
					meta.tdAttr = 'data-qtip="Синхронизировано с ' + record.get('MasterID') + '"';
					return value;
				}
      },
      { text: 'Продукция ОТ',         dataIndex: 'From',          width: 150 },
      { text: 'Производитель',        dataIndex: 'Manufacturer',  width: 150 },
      { text: 'Бренд',                dataIndex: 'Brand',         width: 125 },
      { text: 'Категория',            dataIndex: 'Category',      width: 110 },
      { text: 'Сегмент',              dataIndex: 'Segment',       width: 80  },
      { text: 'Цвет чая',             dataIndex: 'TeaColor',      width: 120 },
      { text: 'Тип упаковки',         dataIndex: 'Pack',          width: 180 },
      { text: 'Количество пакетов',   dataIndex: 'BagQty',        width: 165 },
      { text: 'Вес пакета',           dataIndex: 'BagWeight',     width: 100 },
      { text: 'Вес',                  dataIndex: 'Weight',        width: 80  },
      //{ text: 'Аромат',               dataIndex: 'isWithFlavor',  width: 80  },
      //{ text: 'Добавка',              dataIndex: 'isWithAdding',  width: 80  },
      { text: 'Ценовой сегмент',      dataIndex: 'PriceSegment',  width: 150 },
      { text: 'Страна происхождения', dataIndex: 'Country',       width: 185 },
      { text: 'Источник',             dataIndex: 'DataSource',    width: 215 },
      { text: 'Дата привязки',        dataIndex: 'DateChange',    width: 130 },
      { text: 'авто/ручн',            dataIndex: 'SyncType',      width: 100 }
    ];

    if(userRights['Sku_slave_sync']) {
      Ext.Array.insert(me.columns, 0, [{
        text: 'Отвязать',
        menuDisabled: true,
        sortable: false,
        xtype: 'actioncolumn',
        menuText: 'Отвязать',
        width: 100,
        items: [{
          iconCls: 'unsync-col',
          handler: function(view, rowIndex, colIndex) {
              me.fireEvent('unsyncbuttonclick', me, rowIndex, colIndex);
          }
        }]
      }]);
    }

    var i = 1,
      columns = this.columns,
      length = columns.length;

    for (;i<length;i++) {
      columns[i].items = [{
        xtype: 'container',
        margin: 4,
        flex: 1,
        layout: 'fit',
        listeners: {
            scope: this,
            element: 'el',
            mousedown: function(e)
            {
                e.stopPropagation();
            },
            click: function(e)
            {
                e.stopPropagation();
            },
            keydown: function(e){
                 e.stopPropagation();
            },
            keypress: function(e){
                 e.stopPropagation();
            },
            keyup: function(e){
                 e.stopPropagation();
            }
        },
        items:[{
          // xtype: 'trigger',
          // triggerCls: 'x-form-clear-trigger',
          xtype: 'textfield',
          //flex : 1,
          //margin: '0 5 5 5',
          emptyText : 'Поиск',
          enableKeyEvents: true,
          onTriggerClick: function () {
            this.reset();
            this.focus();
          },
          listeners: {
            change: function(field, newValue, oldValue, eOpts) {
              me.fireEvent('searchchange', me, field, newValue, oldValue);
            },
            buffer: 1000
          }
        }]
      }];
    }

    this.lbar = [{
      xtype: 'general_tabpanelLbar',
      ref: 'Sku',
      grid: this
    }];

    this.bbar = {
      xtype: 'pagingtoolbar',
      store: this.store,
      displayInfo: true,
      inputItemWidth: 60,
      items : [{
        xtype: 'tbseparator' 
      },{
        xtype     : 'checkbox',
        boxLabel  : 'Показывать все',
        name      : 'syncedShowAll',
        inputValue: 1,
        listeners: {
          change: function( cb, newValue, oldValue, eOpts ) {
            cb.up('pagingtoolbar').store.loadPage(1);
          }
        }
      }]
    };

    this.callParent(arguments);
  }
});
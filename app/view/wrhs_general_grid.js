Ext.define('TestOrimi.view.wrhs_general_grid', {
    extend: 'Ext.grid.Panel',
    requires: ['TestOrimi.store.MasterWarehousesStore'],
    xtype: 'wrhs_general_grid',
    name: 'wrhs_grid',
    flex: 1,
    selModel: {
        selType: 'checkboxmodel'
    },
    columnLines: true,
    border: false,
    viewConfig: {
        stripeRows: false,
        getRowClass: function (record) {
            return record.get('StatusID') == 2 ? '' : (record.get('StatusID') == 1 ? 'temp-status-row' : 'del-status-row');
        },
        shrinkWrap: 0,
        shadow: false,
        trackOver: false,
        overItemCls: false
    },
    plugins: [{
        ptype: 'bufferedrenderer',
        trailingBufferZone: 0,
        leadingBufferZone: 0
    }],
    listeners: {
        celldblclick: function (grid, td, cellIndex, record, tr, rowIndex) {
            Ext.create('TestOrimi.view.createWrhsWindow', {data: record}).show();
        }
    },
    initComponent: function () {
        var me = this;

        this.store = 'TestOrimi.store.MasterWarehousesStore';

        this.columns = [
            {text: 'Статус', dataIndex: 'Status', width: 160},
            {text: 'ID', dataIndex: 'ID', width: 80},
            {text: 'Код', dataIndex: 'Code', width: 130, hidden: true},
            {text: 'Название', dataIndex: 'Name', width: 200},
            {text: 'Черновой адрес', dataIndex: 'TempAddress', width: 180},
            {text: 'Индекс', dataIndex: 'PostCode', width: 100, hidden: true},
            {text: 'Округ', dataIndex: 'County', width: 110, hidden: true},
            {text: 'Регион', dataIndex: 'Region', width: 110, hidden: true},
            {text: 'Район', dataIndex: 'District', width: 120, hidden: true},
            {text: 'Город', dataIndex: 'City', width: 130, hidden: true},
            {text: 'Улица', dataIndex: 'Street', width: 120, hidden: true},
            {text: 'Дом', dataIndex: 'Building', width: 80, hidden: true},
            {text: 'Полный адрес', dataIndex: 'FullAddress', width: 150},
            {text: 'Адрес', dataIndex: 'Address', width: 140, hidden: true},
            {text: 'Дивизион', dataIndex: 'Division', width: 100},
            {text: 'Участок', dataIndex: 'Area', width: 100},
            {text: 'Сектор', dataIndex: 'Sector', width: 100},
            {text: 'Дистрибьютор', dataIndex: 'Distributor', width: 100},
            {text: 'Код дистрибьютора', dataIndex: 'DistrCode', width: 100},
            {text: 'Группа контрагентов', dataIndex: 'ContractorGroup', width: 220},
            {text: 'Формат', dataIndex: 'Format', width: 100},
            {text: 'Дата открытия', dataIndex: 'DateOpen', width: 160},
            {text: 'Дата закрытия', dataIndex: 'DateClose', width: 160},
            {text: 'Площадь', dataIndex: 'ShopArea', width: 120, hidden: true},
            {text: 'Комментарий', dataIndex: 'Comment', width: 120, hidden: true},
            {text: 'Страна', dataIndex: 'Country', width: 120, hidden: true},
            {text: 'ФИАС', dataIndex: 'FIAS', width: 100, hidden: true},
            {text: 'Автор изменений', dataIndex: 'Author', width: 170},
            {text: 'Дата последнего изменения', dataIndex: 'DateChange', width: 200, hidden: true},
            {text: 'Источник данных', dataIndex: 'DataSource', width: 170}
        ];

        var i = 0,
            columns = this.columns,
            length = columns.length;

        for (; i < length; i++) {
            columns[i].items = [{
                xtype: 'container',
                margin: 4,
                flex: 1,
                layout: 'fit',
                listeners: {
                    scope: this,
                    element: 'el',
                    mousedown: function (e) {
                        e.stopPropagation();
                    },
                    click: function (e) {
                        e.stopPropagation();
                    },
                    keydown: function (e) {
                        e.stopPropagation();
                    },
                    keypress: function (e) {
                        e.stopPropagation();
                    },
                    keyup: function (e) {
                        e.stopPropagation();
                    }
                },
                items: [{
                    xtype: 'textfield',
                    emptyText: 'Поиск',
                    enableKeyEvents: true,
                    onTriggerClick: function () {
                        this.reset();
                        this.focus();
                    },
                    listeners: {
                        change: function (field, newValue, oldValue, eOpts) {
                            me.fireEvent('searchchange', me, field, newValue, oldValue);
                        },
                        buffer: 1000
                    }
                }]
            }];
        }

        this.lbar = [{
            xtype: 'general_gridLbar',
            ref: 'Wrhs',
            grid: this
        }];

        this.bbar = {
            xtype: 'pagingtoolbar',
            store: this.store,
            inputItemWidth: 60,
            displayInfo: true
        };

        this.callParent(arguments);

    }
});

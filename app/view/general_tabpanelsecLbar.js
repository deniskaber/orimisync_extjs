Ext.define('TestOrimi.view.general_tabpanelsecLbar', {
    extend: 'Ext.container.Container',
    xtype: 'general_tabpanelsecLbar',
    width: 55,
    padding: '2',
    border: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        margin: '2 0 2 0'
    },
    ref: 'Sku',

    initComponent: function () {
        var me = this,
            userRights = TestOrimi.getApplication().data;

        me.items = [{
            xtype: 'button'
            , tooltip: 'Комбинированный поиск'
            , name: 'combSearch'
            , scale: 'medium'
            , text: '<span class="oi" data-glyph="magnifying-glass" aria-hidden="true"></span>'
            , handler: function () {
                Ext.create('TestOrimi.view.CreateSearchWindow' + this.up('general_tabpanelsecLbar').ref, {grid: this.up('general_tabpanelsecLbar').grid}).show();
            }
        }, {
            xtype: 'button'
            , tooltip: 'Привязать выбранные'
            , disabled: userRights[me.ref + '_slave_sync'] ? false : true
            , scale: 'medium'
            , text: '<span class="oi" data-glyph="link-intact" aria-hidden="true"></span>'
            , handler: function () {
                var grid = this.up('general_tabpanelsecLbar').grid;
                grid.fireEvent('groupsyncbuttonclick', grid);
            }
        }, {
            xtype: 'button'
            , tooltip: 'Создать на основе выбранного'
            , disabled: userRights[me.ref + '_draft_create'] ? false : true
            , scale: 'medium'
            , text: '<span class="oi" data-glyph="puzzle-piece" aria-hidden="true"></span>'
            , handler: function () {
                var grid = this.up('container').grid,
                    selectedRecord = grid.getSelectionModel().getSelection()[0],
                    dataRec = {},
                    storeActiveGrid = this.up('[name=viewport]').down('[name=domCard]').getLayout().getActiveItem().down('grid').getStore(),
                    modelActiveGrid = storeActiveGrid.model.getFields(),
                    ref = this.up('general_tabpanelsecLbar').ref,
                    colNames = [],
                    arrData = [],
                    i;

                if (selectedRecord) {
                    /* making exact copy of selected record data */
                    for(i in selectedRecord.data) {
                        if (selectedRecord.data.hasOwnProperty(i)) {
                            dataRec[i] = selectedRecord.data[i];
                        }
                    }

                    dataRec['ID'] = 0;

                    for (var arrName = 0; arrName < modelActiveGrid.length; arrName++) {
                        colNames.push(modelActiveGrid[arrName].name);
                    }

                    for (i in dataRec) {

                        if (Ext.Array.contains(colNames, i + 'ID') && i != 'DataSource') {
                            dataRec[i + 'ID'] = dataRec[i];
                            delete dataRec[i];
                        }

                        if (i == 'Pack') {
                            dataRec['External' + i + 'ID'] = dataRec[i];
                            delete dataRec[i];
                        }

                        if (i == 'FIO') {
                            dataRec['SurName'] = dataRec[i];
                            delete dataRec[i];
                        }

                    }

                    if (dataRec['CityID']) {
                        delete dataRec['CountyID'];
                        delete dataRec['RegionID'];
                        delete dataRec['DivisionID'];
                        delete dataRec['SectorID'];
                    }

                    if (ref == 'Wrhs' && dataRec.hasOwnProperty('DistrCode')) {
                        var distrRecord = Ext.getStore('TestOrimi.store.DistributorsStore').findRecord('Code', dataRec['DistrCode'], 0, false, false, true);

                        if (distrRecord) {
                            dataRec['DistributorID'] = distrRecord.get('ID');
                        }
                    }

                    var model = TestOrimi.model.DublicateDataModel.create(dataRec);

                    Ext.create('TestOrimi.view.create' + this.up('general_tabpanelsecLbar').ref + 'Window', {data: model}).show();
                } else {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Позиция не выбрана', 'winError');
                }
            }
        }, {
            xtype: 'button'
            , tooltip: 'Групповое сопоставление'
            , disabled: userRights[me.ref + '_slave_sync'] ? false : true
            , scale: 'medium'
            , text: '<span class="oi" data-glyph="transfer" aria-hidden="true"></span>'
            , handler: function () {
                Ext.create('TestOrimi.view.Create' + this.up('general_tabpanelsecLbar').ref + 'GroupSyncWindow').show();
            }
        }];

        this.callParent(arguments);
    }
});

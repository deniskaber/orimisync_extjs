Ext.define('TestOrimi.view.CreateSearchWindowAdvertisingEventHeader', {
    extend: 'Ext.window.Window',
    title: 'Комбинированный поиск',
    modal: true,
    width: 500,
    height: 600,
    layout: 'fit',
    items: [{
        xtype: 'form',
        margin: 10,
        border: false,
        layout: {type: 'vbox', align: 'stretch'},
        autoScroll: true,
        defaults: {
            xtype: 'textfield',
            anchor: '90%',
            labelWidth: 170,
            margin: '3 15 2 5'
        },
        items: [{
            xtype: 'textfield',
            fieldLabel: 'Поиск по всем столбцам',
            name: 'searchall',
            anchor: '90%',
            labelWidth: 170
        }, {
            fieldLabel: 'ID',
            name: 'ID'
        }, {
            fieldLabel: 'Номер',
            name: 'DocNum'
        }, {
            fieldLabel: 'Статус заявки',
            name: 'Status',
            xtype: 'searchModalCombo',
            store: 'TestOrimi.store.RequestStatusStore'
        }, {
            fieldLabel: 'Заголовок',
            name: 'Title'
        }, {
            fieldLabel: 'Участок',
            name: 'Area',
            xtype: 'searchModalCombo',
            store: 'TestOrimi.store.AvailableAreaStore'
        }, {
            fieldLabel: 'Дата начала акции',
            xtype: 'datefield',
            format: 'Y-m-d',
            name: 'DateStart'
        }, {
            fieldLabel: 'Дата окончания акции',
            xtype: 'datefield',
            format: 'Y-m-d',
            name: 'DateEnd'
        }, {
            fieldLabel: 'Группа контрагентов',
            name: 'ContractorGroup',
            xtype: 'searchModalCombo',
            store: 'TestOrimi.store.ContractorGroupStore'
        }, {
            fieldLabel: 'Название сети',
            name: 'Net',
            attribute: 'Net',
            xtype: 'searchModalCombo',
            store: 'TestOrimi.store.NetStore'
        }, {
            fieldLabel: 'Количество ТТ',
            name: 'StoreAmount'
        }, {
            fieldLabel: 'Название ТТ',
            name: 'StoreName',
            attribute: 'Store',
            xtype: 'searchModalCombo',
            store: 'TestOrimi.store.ActiveStoresStore'
        }, {
            fieldLabel: 'Адрес ТТ',
            name: 'StoreAddress'
        }, {
            fieldLabel: 'Канал для рекламы',
            name: 'AdvertisingChannel',
            attribute: 'AdvertisingChannel',
            xtype: 'searchModalCombo',
            store: 'TestOrimi.store.AdvertisingChannelStore'
        }, {
            fieldLabel: 'Комментарий',
            name: 'Comment'
        }, {
            fieldLabel: 'Автор изменений',
            name: 'Author'
        }, {
            fieldLabel: 'Дата посл. изменения',
            xtype: 'datefield',
            format: 'Y-m-d',
            name: 'DateChange'
        }, {
            fieldLabel  : 'Отражена в TradeNT'
            ,xtype      : 'radiogroup'
            ,anchor     : 'none'
            ,layout     : {
                autoFlex: false
            }
            ,defaults: {
                name    : 'isInTradent',
                margin  : '0 15 0 0'
            }
            ,items: [{
                boxLabel    : 'Да',
                inputValue  : 1
            },{
                boxLabel    : 'Нет',
                inputValue  : 0
            }]
        }, {
            fieldLabel: 'Номер ТСЗ',
            name: 'TSZCode'
        }, {
            fieldLabel: 'Тип оплаты',
            name: 'PaymentType',
            xtype: 'searchModalCombo',
            store: 'TestOrimi.store.PaymentTypeStore'
        }]
    }],

    buttons: [{
        text: 'Сбросить все',
        handler: function() {
            this.up('window').down('form').getForm().reset();
        }
    }, '->', {
        text: 'Применить',
        handler: function() {
            var grid = this.up('window').grid,
                store = grid.store,
                formVals = this.up('window').down('form').getValues(),
                filters = [];

            var modelFields = store.model.getFields(),
                dataFields = [],
                i = 0,
                length = modelFields.length;

            for (; i < length; i++) {
                dataFields.push(modelFields[i].name);
            }

            Ext.Object.each(formVals, function(key, value, myself) {
                if (value || value === 0) {
                    if (Ext.Array.contains(dataFields, key)) {
                        filters.push({property: key, value: value});
                    }
                }
            });

            store.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
            store.fireEvent('applyFilters', store, filters, grid);

            this.up('window').destroy();
        }
    }, {
        text: 'Отмена',
        handler: function() {
            this.up('window').destroy();
        }
    }],

    initComponent: function() {
        var me = this,
            grid = me.grid;

        this.callParent(arguments);

        var cols = [],
            columns = grid.columns,
            length = columns.length,
            i;

        for (i = 0; i < length; i++) {
            if (columns[i]['dataIndex'])
                cols.push(columns[i]['dataIndex']);
        }
        cols.push('searchall');
        // cols.push('allgoods');

        var fields = me.query('textfield'), //все textfield окна
            radiogroups = me.query('radiogroup');

        length = fields.length;

        for (i = 0; i < length; i++) {				//цикл указывания какие поля останутся активными
            if (!(Ext.Array.contains(cols, fields[i].name))) {
                fields[i].hide();
                fields[i].disable();
            }
        }

        length = radiogroups.length;

        for (i = 0; i < length; i++) {				//цикл указывания какие поля останутся активными
            if (!(Ext.Array.contains(cols, radiogroups[i].items.items[0].name))) {
                radiogroups[i].hide();
                radiogroups[i].disable();
            }
        }

        var store = grid.store,
            filterRecord = {},
            filters = store.filters.items;

        length = filters.length;

        if (length) {
            for (i = 0; i < length; i++) {
                filterRecord[filters[i].property] = filters[i].value;
            }
        }
        if (store.proxy.extraParams['searchall']) {
            filterRecord['searchall'] = store.proxy.extraParams['searchall'];
        }
        this.down('form').getForm().setValues(filterRecord);
    }
});

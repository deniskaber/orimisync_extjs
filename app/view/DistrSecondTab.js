Ext.define('TestOrimi.view.DistrSecondTab', {
    extend: 'Ext.container.Container',
    xtype: 'distrSecondTab',
    border: false,
    margin: '20 20 0 20',
    layout: 'column',

    initComponent: function () {
        var me = this,
            userRights = TestOrimi.getApplication().data;

        me.items = [{
            xtype: 'fieldset',
            defaultType: 'textfield',
            columnWidth: 0.5,
            margin: '20 0 0 0',
            defaults: {anchor: '100%', labelWidth: 160},
            border: false,
            items: [{
                fieldLabel: 'Субдистрибьютор',
                name: 'isSub',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                xtype: 'radiogroup',
                anchor: 'none',
                //padding     : '34 0 0 0',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    name: 'isSubDistributor',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'Да',
                    inputValue: '1'
                }, {
                    boxLabel: 'Нет',
                    inputValue: '0',
                    checked: true
                }]
            }, {
                fieldLabel: 'Валюта',
                name: 'CurrancyID',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                value: 1,
                xtype: 'modalcombo',
                store: 'TestOrimi.store.CurrancyStore',
                allowBlank: false
            }, {
                fieldLabel: 'Группа контрагентов',
                name: 'ContractorGroupID',
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                xtype: 'modalcombo',
                store: 'TestOrimi.store.ContractorGroupStore',
                allowBlank: false
            }, {
                fieldLabel: 'Участвует в РБП',
                xtype: 'radiogroup',
                name: 'isRBP',
                anchor: 'none',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    name: 'isInRBP',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'Да',
                    inputValue: '1'
                }, {
                    boxLabel: 'Нет',
                    inputValue: '0',
                    checked: true
                }]
            }, {
                fieldLabel: 'GLN',
                name: 'GLN',
                margin: '80 0 5 0'
            }]
        }, {
            xtype: 'fieldset',
            defaultType: 'textfield',
            columnWidth: 0.5,
            margin: '20 0 0 0',
            defaults: {anchor: '100%', labelWidth: 160},
            border: false,
            items: [{
                fieldLabel: 'Отв. менеджер',
                name: 'RespManagerID',
                displayField: 'FIO',
                readOnly: true,
                cls: 'noteditable',
                attribute: 'ParentStaff',
                xtype: 'modalcombo',
                store: 'TestOrimi.store.ParentStore'
            }, {
                fieldLabel: 'Учитывать в расчетах',
                name: 'isAcc', //for listeners
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                xtype: 'radiogroup',
                anchor: 'none',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    name: 'isAccounting',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'Да',
                    inputValue: '1',
                    checked: true
                }, {
                    boxLabel: 'Нет',
                    inputValue: '0'
                }],
                listeners: {
                    change: function (radio, inputValue, oldValue) {
                        var panel = radio.up('distrSecondTab'),
                            changeInput = inputValue.isAccounting;

                        if (changeInput == 0) {
                            panel.down('[name=NonAccReasonID]').enable();
                        } else {
                            panel.down('[name=NonAccReasonID]').disable();
                        }
                    }
                }
            }, {
                fieldLabel: 'Причина неучета',
                name: 'NonAccReasonID',
                xtype: 'modalcombo',
                disabled: true,
                afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                store: 'TestOrimi.store.NonAccReasonStore'
            }, {
                xtype: 'textareafield',
                grow: true,
                name: 'Comment',
                fieldLabel: 'Комментарий',
                margin: '108 0 5 0'
            }]
        }];

        this.callParent(arguments);
    }
});
Ext.define('TestOrimi.view.SearchModalCombo', {
    extend: 'Ext.form.ComboBox',
    xtype: 'searchModalCombo',

    triggerCls: Ext.baseCSSPrefix + 'form-search-trigger'

    , attribute: 'simple' //by deafult
    , queryMode: 'local'
    , displayField: 'Name'
    , queryDelay: 500
    , anyMatch: true
    , clearFilterOnBlur: false

    , validator: function (v) {
        if (v) {
            var index = this.getStore().findExact(this.displayField, v);
            return (index !== -1) ? true : 'Неизвестный элемент';
        } else
            return true;
    }
    , listeners: {
        change: function (field, newValue) {
            switch (field.name) {
                case 'Manufacturer':
                    var brandField = field.up('form').down('[name=Brand]');
                    if (field.isValid() && newValue) {
                        if (brandField) {
                            var rec = brandField.store.findRecord('Name', brandField.value, 0, false, false, true);
                            if (rec) {
                                if (rec.get('Manufacturer') != newValue)
                                    brandField.setValue();
                            } else
                                brandField.setValue();
                        }
                    } else {
                        if (brandField)
                            brandField.setValue();
                    }
                    break;
                case 'Brand':
                    var manField = field.up('form').down('[name=Manufacturer]'),
                        subBrandField = field.up('form').down('[name=SubBrand]'),
                        rec;

                    if (field.isValid() && newValue) {
                        if (manField) {
                            rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                            if (rec) {
                                manField.suspendEvents();
                                manField.reset();
                                manField.resumeEvents();
                                manField.setValue(rec.get('Manufacturer'));
                            }
                        }
                        if (subBrandField) {
                            rec = subBrandField.store.findRecord('Name', subBrandField.value, 0, false, false, true);
                            if (rec) {
                                if (rec.get('Brand') != newValue)
                                    subBrandField.setValue();
                            } else
                                subBrandField.setValue();
                        }
                    } else {
                        if (subBrandField)
                            subBrandField.setValue();
                    }
                    break;
                case 'SubBrand':
                    var brandField = field.up('form').down('[name=Brand]');

                    if (field.isValid() && newValue) {
                        if (brandField) {
                            var rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                            if (rec) {
                                brandField.suspendEvents();
                                brandField.reset();
                                brandField.resumeEvents();
                                brandField.setValue(rec.get('Brand'));
                            }
                        }
                    }
                    break;
                case 'Category':
                    var segmentField = field.up('form').down('[name=Segment]');
                    if (field.isValid() && newValue) {
                        if (segmentField) {
                            var rec = segmentField.store.findRecord('Name', segmentField.value, 0, false, false, true);
                            if (rec) {
                                if (rec.get('Category') != newValue)
                                    segmentField.setValue();
                            } else
                                segmentField.setValue();
                        }
                    } else {
                        if (segmentField)
                            segmentField.setValue();
                    }
                    break;
                case 'Segment':
                    var categField = field.up('form').down('[name=Category]'),
                        subSegmField = field.up('form').down('[name=SubSegment]'),
                        rec;

                    if (field.isValid() && newValue) {
                        if (categField) {
                            rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                            if (rec) {
                                categField.suspendEvents();
                                categField.reset();
                                categField.resumeEvents();
                                categField.setValue(rec.get('Category'));
                            }
                        }
                        if (subSegmField) {
                            rec = subSegmField.store.findRecord('Name', subSegmField.value, 0, false, false, true);
                            if (rec) {
                                if (rec.get('Segment') != newValue)
                                    subSegmField.setValue();
                            } else
                                subSegmField.setValue();
                        }
                    } else {
                        if (subSegmField)
                            subSegmField.setValue();
                    }
                    break;
                case 'SubSegment':
                    var segmField = field.up('form').down('[name=Segment]');

                    if (field.isValid() && newValue) {
                        if (segmField) {
                            var rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                            if (rec) {
                                segmField.suspendEvents();
                                segmField.reset();
                                segmField.resumeEvents();
                                segmField.setValue(rec.get('Segment'));
                            }
                        }
                    }
                    break;
                //-------------------------
                case 'Country':
                    var countField = field.up('form').down('[name=County]'); //стирает
                    if (field.isValid() && newValue) {
                        if (countField) {
                            var rec = countField.store.findRecord('Name', countField.value, 0, false, false, true);
                            if (rec) {
                                if (rec.get('Country') != newValue)
                                    countField.setValue();
                            } else
                                countField.setValue();
                        }
                    } else {
                        if (countField)
                            countField.setValue();
                    }
                    break;
                case 'County':
                    var countryField = field.up('form').down('[name=Country]'), //задает
                        regionField = field.up('form').down('[name=Region]'),//стирает
                        rec;

                    if (field.isValid() && newValue) {
                        if (countryField) {
                            rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                            if (rec) {
                                countryField.suspendEvents();
                                countryField.reset();
                                countryField.resumeEvents();
                                countryField.setValue(rec.get('Country'));
                            }
                        }
                        if (regionField) {
                            rec = regionField.store.findRecord('Name', regionField.value, 0, false, false, true);
                            if (rec) {
                                if (rec.get('County') != newValue)
                                    regionField.setValue();
                            } else
                                regionField.setValue();
                        }
                    } else {
                        if (regionField)
                            regionField.setValue();
                    }
                    break;
                case 'Region':
                    var countyField = field.up('form').down('[name=County]'), //задает
                        districtField = field.up('form').down('[name=District]'),//стирает
                        rec;

                    if (field.isValid() && newValue) {
                        if (countyField) {
                            rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                            if (rec) {
                                countyField.suspendEvents();
                                countyField.reset();
                                countyField.resumeEvents();
                                countyField.setValue(rec.get('County'));
                            }
                        }
                        if (districtField) {
                            rec = districtField.store.findRecord('Name', districtField.value, 0, false, false, true);
                            if (rec) {
                                if (rec.get('Region') != newValue)
                                    districtField.setValue();
                            } else
                                districtField.setValue();
                        }
                    } else {
                        if (districtField)
                            districtField.setValue();
                    }
                    break;
                case 'District':
                    var regionField = field.up('form').down('[name=Region]'), //задает
                        cityField = field.up('form').down('[name=City]'),//стирает
                        rec;

                    if (field.isValid() && newValue) {
                        if (regionField) {
                            rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                            if (rec) {
                                regionField.suspendEvents();
                                regionField.reset();
                                regionField.resumeEvents();
                                regionField.setValue(rec.get('Region'));
                            }
                        }
                        if (cityField) {
                            rec = cityField.store.findRecord('Name', cityField.value, 0, false, false, true);
                            if (rec) {
                                if (rec.get('District') != newValue)
                                    cityField.setValue();
                            } else
                                cityField.setValue();
                        }
                    } else {
                        if (cityField)
                            cityField.setValue();
                    }
                    break;
                case 'City':
                    var distrField = field.up('form').down('[name=District]'), //задает
                        sectField = field.up('form').down('[name=Sector]'), //задает
                        streetField = field.up('form').down('[name=Street]'),//стирает
                        rec;

                    if (field.isValid() && newValue) {
                        if (distrField) {
                            rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                            if (rec) {
                                distrField.suspendEvents();
                                distrField.reset();
                                distrField.resumeEvents();
                                distrField.setValue(rec.get('District'));
                            }
                        }

                        if (sectField) {
                            rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                            //console.dir(sectField);
                            //console.dir(rec);

                            if (rec.get('Sector') == 0) {
                                //console.dir(sectField.store.findRecord('Name', 'Прочее', 0, false, false, true));
                                sectField.setValue(sectField.store.findRecord('Name', 'Прочее', 0, false, false, true).get('ID'));
                            } else {
                                sectField.suspendEvents();
                                sectField.reset();
                                sectField.resumeEvents();
                                sectField.setValue(rec.get('Sector'));
                            }
                        }
                    }
                    break;
                case 'Sector':
                    var areaField = field.up('form').down('[name=Area]'); //задает

                    if (field.isValid() && newValue) {
                        if (areaField) {
                            var rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                            if (rec) {
                                areaField.suspendEvents();
                                areaField.reset();
                                areaField.resumeEvents();
                                areaField.setValue(rec.get('Area'));
                            }
                        }
                    }
                    break;
                case 'Area':
                    var divisField = field.up('form').down('[name=Division]'); //задает

                    if (field.isValid() && newValue) {
                        if (divisField) {
                            var rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                            if (rec) {
                                divisField.suspendEvents();
                                divisField.reset();
                                divisField.resumeEvents();
                                divisField.setValue(rec.get('Division'));
                            }
                        }
                    }
                    break;
                case 'Division':
                    var areaField = field.up('form').down('[name=Area]'); //стирает
                    if (field.isValid() && newValue) {
                        if (areaField) {
                            var rec = areaField.store.findRecord('Name', areaField.value, 0, false, false, true);
                            if (rec) {
                                if (rec.get('Division') != newValue)
                                    areaField.setValue();
                            } else
                                areaField.setValue();
                        }
                    } else {
                        if (areaField)
                            areaField.setValue();
                    }
                    break;
                case 'SubChannel':
                    var chanField = field.up('form').down('[name=Channel]'); //задает

                    if (field.isValid() && newValue) {
                        if (chanField) {
                            var rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                            if (rec) {
                                chanField.suspendEvents();
                                chanField.reset();
                                chanField.resumeEvents();
                                chanField.setValue(rec.get('Channel'));
                            }
                        }
                    }
                    break;
                case 'Channel':
                    var schanField = field.up('form').down('[name=SubChannel]'); //стирает
                    if (field.isValid() && newValue) {
                        if (schanField) {
                            var rec = schanField.store.findRecord('Name', schanField.value, 0, false, false, true);
                            if (rec) {
                                if (rec.get('Channel') != newValue)
                                    schanField.setValue();
                            } else
                                schanField.setValue();
                        }
                    } else {
                        if (schanField)
                            schanField.setValue();
                    }
                    break;
                case 'Net':
                    var nettypeField = field.up('form').down('[name=NetType]'),
                        respField = field.up('form').down('[name=RespManager]'),
                        nielsField = field.up('form').down('[name=isNielsen]'); //задает

                    if (field.isValid() && newValue) {
                        if (nettypeField) {
                            var rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                            if (rec) {
                                nettypeField.suspendEvents();
                                nettypeField.reset();
                                nettypeField.resumeEvents();
                                nettypeField.setValue(rec.get('NetType'));
                            }
                        }
                        if (respField) {
                            var rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                            if (rec) {
                                respField.suspendEvents();
                                respField.reset();
                                respField.resumeEvents();
                                respField.setValue(rec.get('RespManager'));
                            }
                        }
                        if (nielsField) {
                            var rec = field.store.findRecord('Name', newValue, 0, false, false, true);
                            if (rec) {
                                nielsField.suspendEvents();
                                nielsField.reset();
                                nielsField.resumeEvents();
                                nielsField.setValue(rec.get('isNielsen'));
                            }
                        }
                    }
                    break;
                case 'Distributor':
                    var distrCodeField = field.up('form').down('[name=DistrCode]'), //задает
                        contractorGroupField = field.up('form').down('[name=ContractorGroup]'),//стирает
                        rec;

                    if (field.isValid() && newValue) {
                        rec = field.store.findRecord('Name', newValue, 0, false, false, true);

                        if (distrCodeField) {
                            if (rec) {
                                distrCodeField.suspendEvents();
                                distrCodeField.reset();
                                distrCodeField.resumeEvents();
                                distrCodeField.setValue(rec.get('Code'));
                            }
                        }
                        if (contractorGroupField) {
                            if (rec) {
                                contractorGroupField.suspendEvents();
                                contractorGroupField.reset();
                                contractorGroupField.resumeEvents();
                                contractorGroupField.setValue(rec.get('ContractorGroup'));
                            }
                        }
                    }
                    break;
            }
        },
        //delay: 500,
        focus: function (me) {
            if (!me.isValid()) {
                me.expand();
            }
        }
    }

    , onTriggerClick: function () {
        var me = this,
            attribute = me.attribute,
            userRights = TestOrimi.getApplication().data,
            editable = true,
            filters,
            i = 0;

        if (userRights['custom'] == 1) {
            editable = false;
        }

        me.duringTriggerClick = true;
        if (!me.readOnly && !me.disabled) {
            if (me.isExpanded) {
                me.collapse();
            }

            if (me.queryFilter) {
                me.queryFilter.setValue('');
            }

            filters = me.store.filters.items;

            if (filters.length) {
                for (i = 0; i < filters.length; i++) {
                    if (filters[i].property == 'Deleted' || filters[i].hasOwnProperty('defaultValue')) {
                        filters[i].setValue('');
                    }
                }
            }

            me.store.filter();

            switch (attribute) {
                case 'simple':
                    Ext.create('TestOrimi.view.SimpleAttributeWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Brand':
                    Ext.create('TestOrimi.view.BrandWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'SubBrand':
                    Ext.create('TestOrimi.view.SubBrandWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Segment':
                    Ext.create('TestOrimi.view.SegmentWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'SubSegment':
                    Ext.create('TestOrimi.view.SubSegmentWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'ParentID': //агрегирующая позиция
                    Ext.create('TestOrimi.view.ParentID', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'ParentStaff':
                    Ext.create('TestOrimi.view.ParentStaffWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Distributor':
                    Ext.create('TestOrimi.view.DistributorWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'WeightGroupID':
                    Ext.create('TestOrimi.view.WeightGroupID', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Net':
                    Ext.create('TestOrimi.view.NetWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'ParentNet':
                    Ext.create('TestOrimi.view.ParentNetWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Sector':
                    Ext.create('TestOrimi.view.SectorWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Area':
                    Ext.create('TestOrimi.view.AreaWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Division':
                    Ext.create('TestOrimi.view.DivisionWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'SubChannel':
                    Ext.create('TestOrimi.view.SubChannelWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'County':
                    Ext.create('TestOrimi.view.CountyWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Region':
                    Ext.create('TestOrimi.view.RegionWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'District':
                    Ext.create('TestOrimi.view.DistrictWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'City':
                    Ext.create('TestOrimi.view.CityWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Motivation':
                    Ext.create('TestOrimi.view.MotivationWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'OrimiGood':
                    Ext.create('TestOrimi.view.ParentID', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'AdvertisingChannel':
                    Ext.create('TestOrimi.view.AdvertisingChannelWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                case 'Store':
                    Ext.create('TestOrimi.view.StoreModalWindow', {
                        field: me,
                        editable: editable
                    }).show();
                    break;

                default:
                    console.log('no attribute assined');
                    break;
            }
        }
        delete me.duringTriggerClick;
    }
});

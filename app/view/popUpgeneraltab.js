Ext.define('TestOrimi.view.popUpgeneraltab', {
    extend: 'Ext.container.Container',
    xtype: 'popUpgeneraltab',
    border: false,
    margin: '20 20 0 20',
    layout: 'column',
    items: [{
        xtype: 'fieldset',
        columnWidth: 0.5,
        defaults: {anchor: '100%'},
        border: false,
        margin: '20 0 0 0',
        defaultType: 'textfield',
        items: [{
            fieldLabel: 'Производитель:',
            afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
            name: 'ManufacturerID',
            readOnly: true,
            cls: 'noteditable',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.ManufacturerStore'
        }, {
            fieldLabel: 'Бренд:',
            afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
            name: 'BrandID',
            readOnly: true,
            cls: 'noteditable',
            attribute: 'Brand',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.BrandStore'
        }, {
            fieldLabel: 'Саббренд:',
            afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
            name: 'SubBrandID',
            attribute: 'SubBrand',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.SubBrandStore'
        }, {
            fieldLabel: 'Коллекция / Доп.имя:',
            name: 'Collection'
        }, {
            fieldLabel: 'Продукция ОТ',
            xtype: 'radiogroup',
            name: 'FromGroup',
            disabled: true,
            defaults: {
                name: 'From',
                margin: '0 15 0 0'
            },
            items: [{
                boxLabel: 'товар наш',
                inputValue: 1
            }, {
                boxLabel: 'товар конкурента',
                inputValue: 0
            }],
            listeners: {
                change: function (field, newValue, oldValue) {
                    var manField = field.up('fieldset').down('[name=ManufacturerID]');
                    if (newValue['From']) {
                        manField.setValue(292); //TODO choose valid ID
                    } else {
                        if (manField.getValue() == 292)
                            manField.reset();
                    }
                }
            }
        }, {
            xtype: 'itemselectorAdd'
        }]
    }, {
        columnWidth: 0.5,
        xtype: 'fieldset',
        defaultType: 'textfield',
        margin: '20 0 0 0',
        defaults: {anchor: '100%'},
        border: false,
        items: [{
            fieldLabel: 'Категория:',
            afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
            name: 'CategoryID',
            readOnly: true,
            cls: 'noteditable',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.CategoryStore'
        }, {
            fieldLabel: 'Сегмент:',
            afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
            name: 'SegmentID',
            readOnly: true,
            cls: 'noteditable',
            attribute: 'Segment',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.SegmentStore'
        }, {
            fieldLabel: 'Сабсегмент:',
            afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
            name: 'SubSegmentID',
            attribute: 'SubSegment',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.SubSegmentStore'
        }, {
            fieldLabel: 'Цвет чая:',
            afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
            margin: '12 0 0 0',
            name: 'TeaColorID',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.TeaColorStore'
        }, {
            xtype: 'itemselectorFla',
            margin: '42 0 0 0'
        }]
    }]
});

Ext.define('TestOrimi.view.general_tabpanelTT', {
    extend  : 'Ext.tab.Panel',
    xtype   : 'general_tabpanelTT',
    flex    : 1,
    ref     : 'TT',
    split   : true,
    collapsible: true,
    header: false,
    plain   : true,
    title: 'Подчиненные справочники',

    initComponent: function() {	
        this.items = [
            { xtype: 'general_tabpanelFirstcolTT',    title: 'Отвязать' },
            { xtype: 'general_tabpanelSecondcolTT',   title: 'Привязать' }
        ];    
        this.callParent(arguments);
    }       
});
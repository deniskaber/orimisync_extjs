﻿Ext.define('TestOrimi.view.CreateSkuGroupSyncWindow', {
	extend	: 'Ext.window.Window',
	xtype	: 'createSkuGroupSyncWindow',
	width 	: '100%',
	height 	: '100%',
	modal	: true,
	id		: 'createSkuGroupSyncWindow-id',
	title	: 'Групповое сопоставление',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
	initComponent: function() {
		var me = this,
			resultStore = 'TestOrimi.store.GoodsSyncResultStore';
		
		var columns = [
			 {dataIndex: 'masterID'				, text: 'ID' , width: 80 }
			,{dataIndex: 'masterParent'			, text: 'Агрегирующая позиция' , hidden: true }
			,{dataIndex: 'masterCode'			, text: 'Код' , hidden: true }
			,{dataIndex: 'masterName'			, text: 'Полное название' , width: 200 }
			,{dataIndex: 'masterShortName'	 	, text: 'Короткое название' , hidden: true }
			,{dataIndex: 'masterTempName'		, text: 'Черновое название' , width: 200 }
			,{dataIndex: 'masterFrom'			, text: 'Продукция ОТ' , hidden: true }
			,{dataIndex: 'masterManufacturer'	, text: 'Производитель' , hidden: true }
			,{dataIndex: 'masterBrand'		 	, text: 'Бренд' , hidden: true }
			,{dataIndex: 'masterSubBrand'		, text: 'Саббренд' , hidden: true }
			,{dataIndex: 'masterCategory'		, text: 'Категория' , hidden: true }
			,{dataIndex: 'masterSegment'	 	, text: 'Сегмент' , hidden: true }
			,{dataIndex: 'masterSubSegment'		, text: 'Сабсегмент' , hidden: true }
			,{dataIndex: 'masterTeaColor'		, text: 'Цвет чая' , hidden: true }
			,{dataIndex: 'masterExternalPack'	, text: 'Тип упаковки внешней' , hidden: true }
			,{dataIndex: 'masterSachetPack'		, text: 'Тип упаковки саше' , hidden: true }
			,{dataIndex: 'masterInternalPack'	, text: 'Тип упаковки внутренней' , hidden: true }
			,{dataIndex: 'masterBagQty'			, text: 'Количество пакетов' , hidden: true }
			,{dataIndex: 'masterBagWeight'	 	, text: 'Вес пакета' , hidden: true }
			,{dataIndex: 'masterWeight'			, text: 'Вес' , hidden: true }
			,{dataIndex: 'masterWeightGroup' 	, text: 'Вес (группа)' , hidden: true }
			,{dataIndex: 'masterisWithFlavor'	, text: 'Аромат' , hidden: true }
			,{dataIndex: 'masterFlavor'			, text: 'Ароматы' , hidden: true }
			,{dataIndex: 'masterisWithAdding'	, text: 'Добавка' , hidden: true }
			,{dataIndex: 'masterAdding'			, text: 'Добавки' , hidden: true }
			,{dataIndex: 'masterisHorekaPack'	, text: 'Упаковка Хорека' , hidden: true }
			,{dataIndex: 'masterPriceSegment'	, text: 'Ценовой сегмент' , hidden: true }
			,{dataIndex: 'masterCountry'	 	, text: 'Страна происхождения' , hidden: true }
			,{dataIndex: 'masterCoffeeSort'		, text: 'Сорт кофе' , hidden: true }
			,{dataIndex: 'masterisCaffeine'		, text: 'Кофеин' , hidden: true }
			,{dataIndex: 'masterGift'			, text: 'Акционная позиция' , hidden: true }
			,{dataIndex: 'masterGiftType'		, text: 'Тип подарка' , hidden: true }
			,{dataIndex: 'masterPhoto'		 	, text: 'Фотография' , hidden: true }
			,{dataIndex: 'masterLength'			, text: 'Длина' , hidden: true }
			,{dataIndex: 'masterWidth'		 	, text: 'Ширина' , hidden: true }
			,{dataIndex: 'masterHeight'			, text: 'Высота' , hidden: true }
			,{dataIndex: 'masterisWeightNet' 	, text: 'Весовой продукт в сети' , hidden: true }
			,{dataIndex: 'masterComment'	 	, text: 'Комментарий' , hidden: true }
			,{dataIndex: 'masterCollection'		, text: 'Коллекция/Доп.имя' , hidden: true }
			,{dataIndex: 'masterisPrivateMark'	, text: 'Частная марка сети' , hidden: true }
			,{dataIndex: 'masterBarcode'	 	, text: 'Штрихкод' , hidden: true }
			,{dataIndex: 'masterAuthor'			, text: 'Автор изменений' , hidden: true }
			,{dataIndex: 'masterDateChange'		, text: 'Дата последнего изменения' , hidden: true }
			,{dataIndex: 'masterStatus'			, text: 'Статус' , hidden: true }
			,{dataIndex: 'masterDataSource'		, text: 'Источник данных' , hidden: true }

			,{xtype : 'checkcolumn', text : 'Привязать', dataIndex : 'toSync', cls: 'greycell'}

			,{dataIndex: 'slaveID'		 	, text: 'ID' }
			,{dataIndex: 'slaveCode'		, text: 'Код' }
			,{dataIndex: 'slaveOrimiCode'	, text: 'Код Орими' }
			,{dataIndex: 'slaveName'		, text: 'Название' }
			,{dataIndex: 'slaveFrom'		, text: 'Продукция ОТ' , hidden: true }
			,{dataIndex: 'slaveManufacturer', text: 'Производитель' }
			,{dataIndex: 'slaveBrand'		, text: 'Бренд' }
			,{dataIndex: 'slaveCategory'	, text: 'Категория' }
			,{dataIndex: 'slaveSegment'		, text: 'Сегмент' }
			,{dataIndex: 'slaveTeaColor'	, text: 'Цвет чая' }
			,{dataIndex: 'slavePack'		, text: 'Тип упаковки' }
			,{dataIndex: 'slaveBagQty'		, text: 'Количество пакетов' }
			,{dataIndex: 'slaveBagWeight'	, text: 'Вес пакета' }
			,{dataIndex: 'slaveWeight'		, text: 'Вес' }
			,{dataIndex: 'slavePriceSegment', text: 'Ценовой сегмент' , hidden: true }
			,{dataIndex: 'slaveCountry'		, text: 'Страна происхождения' , hidden: true }
			,{dataIndex: 'slaveDataSource'	, text: 'Источник данных' }
			,{dataIndex: 'slaveDateChange'	, text: 'Дата импорта' }
		];

		var i = 0,
			length = columns.length;

		for (;i<length;i++) {
			columns[i].items = [{
				xtype: 'container',
				margin: 4,
				flex: 1,
				layout: 'fit',
				listeners: {
					scope: this,
					element: 'el',
					mousedown: function(e) {
						e.stopPropagation();
					},
					click: function(e) {
						e.stopPropagation();
					},
					keydown: function(e) {
						 e.stopPropagation();
					},
					keypress: function(e) {
						 e.stopPropagation();
					},
					keyup: function(e) {
						 e.stopPropagation();
					}
				},
				items:[{
					xtype: 'textfield',
					emptyText : 'Поиск',
					enableKeyEvents: true,
					onTriggerClick: function () {
						this.reset();
						this.focus();
					},
					listeners: {
						change: function(field, newValue, oldValue, eOpts) {
							me.fireEvent('searchchange', me.down('grid'), field, newValue, oldValue);
						},
						buffer: 1000
					}
				}]
			}];
		}

		me.items = [{
			xtype: 'container',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			height: 250,
			items:[{
				xtype: 'itemselector',
				name: 'masterGroupSyncItemselector',
				id: 'masterGroupSyncItemselector-id',
				flex: 1,
				fieldLabel: 'Мастер справочник',
				labelAlign: 'top',
				store: Ext.getStore('TestOrimi.store.MasterGoodsFieldsStore'),
				displayField: 'text',
				valueField: 'value',
				margin: '10 15 10 15',
				allowBlank: false,
				buttons: ['up', 'add', 'remove', 'down'],
				buttonsText: {up: "Переместить выше", add: "Добавить к сопоставлению", remove: "Убрать из сопоставления", down: "Переместить ниже"}
			},{
				xtype: 'itemselector',
				name: 'slaveGroupSyncItemselector',
				id: 'slaveGroupSyncItemselector-id',
				flex: 1,
				fieldLabel: 'Подчиненный справочник',
				labelAlign: 'top',
				store: Ext.getStore('TestOrimi.store.SlaveGoodsFieldsStore'),
				displayField: 'text',
				valueField: 'value',
				value: ['Code','OrimiCode','Name','From','Category','Manufacturer','Brand','Segment','TeaColor','Pack','BagQty','BagWeight','Weight','PriceSegment','Country','DataSource'],
				margin: '10 15 10 15',
				allowBlank: false,
				buttons: ['up', 'remove', 'add', 'down'],
				buttonsText: {up: "Переместить выше", add: "Добавить к сопоставлению", remove: "Убрать из сопоставления", down: "Переместить ниже"}
			}]
		},{
			xtype: 'container',
			layout: 'hbox',
			height: 35,
			items:[{
				xtype: 'combobox',
				name: 'sourceCB',
				fieldLabel: 'Источник данных',
				editable: false,
				labelWidth: 150,
				width: 300,
				store: 'TestOrimi.store.DataSourceStore',
				queryMode: 'local',
				displayField: 'text',
				valueField: 'value',
				margin: '0 20 0 20',
				value: 0
			},{
				xtype: 'button',
				text: 'Сопоставить',
				handler: function() {
					var masterSelector = me.down('[name=masterGroupSyncItemselector]'),
						slaveSelector = me.down('[name=slaveGroupSyncItemselector]'),
						sourceSelector = me.down('[name=sourceCB]'),
						masterValue = masterSelector.getSubmitValue(),
						masterDataLength = masterValue.split(',').length,
						slaveData = slaveSelector.fromField.store.data,
						slaveDataLength = slaveData.length,
						slaveValue = [];
					
					if (masterDataLength && slaveDataLength && masterDataLength == slaveDataLength) {
						var mask = new Ext.LoadMask(me, {msg:"Загрузка..."});
						
						mask.show();					
						for(var i=0;i<slaveDataLength;i++) {
							slaveValue.push(slaveData.items[i].get('value'));
						}

						Ext.getStore('TestOrimi.store.GoodsSyncResultStore').load({
							params: {
								master: masterValue,
								slave: slaveValue.toString(),
								source: sourceSelector.getValue()
							},
							callback: function(records, operation, success) {
								me.down('grid').down('toolbar').down('tbtext').updateInfo();
								mask.destroy();
							}
						});
					} else {
						TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Не выбраны поля для сопоставления или их количество не совпадает', 'winError');
					}
				}
			}]
		},{		
			xtype: 'grid',
			flex: 1,
			border: false,
			plugins: [{
				ptype: 'bufferedrenderer',
				trailingBufferZone: 0,
				leadingBufferZone: 0
			}],
			viewConfig: {
				loadMask: false
			},
			columns: columns,
			store: resultStore,
			bbar: {
				xtype: 'toolbar',
				items:[{
					xtype: 'button',
					text: 'Выбрать все',
					handler: function() {
						Ext.getStore('TestOrimi.store.GoodsSyncResultStore').each( function(record) { record.set('toSync',true); });
					}
				},{
					xtype: 'button',
					text: 'Снять все',
					handler: function() {
						Ext.getStore('TestOrimi.store.GoodsSyncResultStore').each( function(record) { record.set('toSync',false); });
					}
				},'-',{
					xtype: 'button',
					text: 'Выгрузить в CSV',
					handler: function(button) {
						this.up('createSkuGroupSyncWindow').fireEvent('reportbuttonclick', this.up('createSkuGroupSyncWindow'));
					}
				},'->',{
					xtype: 'button',
					text: 'Привязать выбранные',
					handler: function() {
						var selected = [],
							gsrStore = Ext.getStore('TestOrimi.store.GoodsSyncResultStore');
						
						gsrStore.each( function(record) { 
							if (record.get('toSync')) {
								selected.push({masterID: record.get('masterID'), slaveID: record.get('slaveID')});
							}								
						});

						if (selected.length) {
							Ext.Msg.confirm('Привязать', 'Привязать позиции?',
								function(btn, text) {
									if (btn == 'yes') {
										var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Обработка..."});					
										
										myMask.show();
										Ext.Ajax.request({
											url: 'resources/data/api.php',
											params: {
												data: Ext.JSON.encode(selected),
												act: 'getSlaveGoods',
												subaction: 'groupSyncWindow'
											},
											success: function(response) {									
												var slaveIDs = [],
													length = selected.length,
													toRemove = [];
												
												for (var i=0;i<length;i++) {									
													if (Ext.Array.indexOf(slaveIDs, selected[i]['slaveID']) === -1) {
														slaveIDs.push(selected[i]['slaveID']);
													}
												}
												
												length = slaveIDs.length;
												for (var i=0;i<length;i++) {
													gsrStore.remove(gsrStore.query('slaveID',slaveIDs[i],false,false,true).items);
												}
												me.down('grid').down('toolbar').down('tbtext').updateInfo();
												myMask.destroy();
												TestOrimi.getApplication().getController('Main').showNotification('Успешно! Позиции привязаны', 'winSuccess');														
											},
											failure: function(form, action) {
												myMask.destroy();
												TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сервер не отвечает на запрос', 'winError');
											}
										});
									}
								}
							);
						} else {
							TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Выберите позиции для привязки', 'winError');
						}
					}
				},{
					xtype: 'tbtext',
					text: (Ext.getStore('TestOrimi.store.GoodsSyncResultStore').getCount() == 0) ? "Нет записей" : Ext.getStore('TestOrimi.store.GoodsSyncResultStore').getCount(),
					updateInfo: function(){
						var numberOfRecords = Ext.getStore('TestOrimi.store.GoodsSyncResultStore').getCount();
						if (numberOfRecords == 0)
							this.setText("Нет записей");
						else
							this.setText("Количество записей: "+numberOfRecords);
					}
				}]
			},
			listeners: {
				celldblclick: function(view, td, cellIndex, record, tr, rowIndex) {
					me.fireEvent('celldblclick', view, td, cellIndex, record, tr, rowIndex);
				},
				columnshow: function(ct, column) { 
					me.fireEvent('columnshow', ct, column);
				}
			}
		}];

		me.buttons = [{
			text	: 'Отмена',
			handler: function() {
				this.up('window').destroy();
			}
		}];

		this.callParent(arguments);
	}
});
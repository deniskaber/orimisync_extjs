Ext.define('TestOrimi.view.AddNetWindow', {
    extend: 'Ext.window.Window',
    width: 400,
    height: 340,
    modal: true,
    resizable: false,
    layout: 'fit',
    items: [{
        xtype: 'form',
        layout: 'anchor',
        padding: 5,
        defaults: {
            xtype: 'textfield',
            anchor: '100%'
        },
        items: [{
            cls: 'noteditable',
            fieldLabel: 'ID',
            name: 'ID',
            readOnly: true
        }, {
            fieldLabel: 'Наименование',
            name: 'Name',
            allowBlank: false
        }, {
            fieldLabel: 'Краткое наим.',
            name: 'ShortName'
        }, {
            fieldLabel: 'Тип сети',
            name: 'NetTypeID',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.NetTypeStore',
            allowBlank  : false
        }, {
            fieldLabel: 'Отв. менеджер',
            name: 'RespManagerID',
            displayField: 'FIO',
            attribute: 'ParentStaff',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.ParentStore',
            allowBlank  : false
        }, {
            fieldLabel: 'КАИ Nielsen',
            xtype: 'radiogroup',
            anchor: 'none',
            layout: {
                autoFlex: false
            },
            defaults: {
                name: 'isNielsen',
                margin: '0 15 0 0'
            },
            items: [{
                boxLabel: 'Да',
                inputValue: '1'
            }, {
                boxLabel: 'Нет',
                inputValue: '0',
                checked: true
            }]
        }, {
            fieldLabel: 'Предоставляет данные',
            xtype: 'radiogroup',
            anchor: 'none',
            layout: {
                autoFlex: false
            },
            defaults: {
                name: 'hasSalesData',
                margin: '0 15 0 0'
            },
            items: [{
                boxLabel: 'Да',
                inputValue: '1'
            }, {
                boxLabel: 'Нет',
                inputValue: '0',
                checked: true
            }]
        }, {
            fieldLabel: 'Агрегирующая сеть',
            name: 'ParentNetID',
            attribute: 'ParentNet',
            xtype: 'modalcombo',
            store: 'TestOrimi.store.ParentNetStore'
        }, {
            xtype: 'checkbox',
            fieldLabel: 'Удален',
            name: 'Deleted',
            inputValue: 1
        }]
    }],

    buttons: [{
        text: 'Ок',
        handler: function () {
            var win = this.up('window'),
                form = win.down('form').getForm();

            if (form.isValid()) {
                var myMask = new Ext.LoadMask(win, {msg: "Сохраняю..."});

                myMask.show();
                form.submit({
                    url: 'resources/data/api.php',
                    timeout: 600000,
                    params: {
                        act: win.store.proxy.extraParams['act'],
                        subaction: win.data ? 'update' : 'create'
                    },
                    success: function (response) {
                        myMask.destroy();
                        var responseText = Ext.JSON.decode(response.responseText);
                        if (responseText)
                            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + responseText.msg, 'winError');
                        else {
                            if (win.data) {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Элемент обновлен', 'winSuccess');
                            } else {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess');
                            }
                            win.store.load();
                            Ext.getStore('TestOrimi.store.ParentNetStore').load();
                            win.destroy();
                        }
                    },
                    failure: function (form, action) {
                        myMask.destroy();
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                    }
                });
            }
        }
    }, {
        text: 'Отмена',
        handler: function () {
            this.up('window').destroy();
        }
    }],

    initComponent: function () {
        this.callParent(arguments);

        if (this.data)
            this.down('form').loadRecord(this.data);
    }
});

Ext.define('TestOrimi.view.CreateSearchWindowDistr', {
    extend: 'Ext.window.Window',
    title: 'Комбинированный поиск',
    modal: true,
    width: 500,
    height: 600,
    layout: 'fit',
    items: [{
        xtype: 'form',
        margin: 10,
        border: false,
        layout: {type: 'vbox', align: 'stretch'},
        items: [{
            xtype: 'textfield'
            , fieldLabel: 'Поиск по всем атрибутам'
            , name: 'searchall'
            , anchor: '90%'
            , labelWidth: 170
        }, {
            xtype: 'container',
            flex: 1,
            autoScroll: true,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            margin: '20 0 0 0',
            defaults: {
                xtype: 'textfield',
                anchor: '90%',
                labelWidth: 160,
                margin: '3 15 2 5'
            },
            items: [{
                fieldLabel: 'ID',
                name: 'ID'
            }, {
                fieldLabel: 'Код',
                name: 'Code'
            }, {
                fieldLabel: 'Название',
                name: 'Name'
            }, {
                fieldLabel: 'Черновой адрес',
                name: 'TempAddress'
            }, {
                fieldLabel: 'Полный адрес',
                name: 'FullAddress'
            }, {
                xtype: 'fieldset',
                title: 'Адресные данные',
                collapsible: true,
                collapsed: true,
                defaultType: 'textfield',
                defaults: {anchor: '100%', labelWidth: 150},
                layout: 'anchor',
                items: [{
                    fieldLabel: 'Адрес',
                    name: 'Address'
                }, {
                    fieldLabel: 'Индекс',
                    name: 'PostCode'
                }, {
                    fieldLabel: 'Страна',
                    name: 'Country',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.CountryStore'
                }, {
                    fieldLabel: 'Округ',
                    name: 'County',
                    attribute: 'County',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.CountyStore'
                }, {
                    fieldLabel: 'Регион',
                    name: 'Region',
                    attribute: 'Region',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.RegionStore'
                }, {
                    fieldLabel: 'Район',
                    name: 'District',
                    attribute: 'District',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.DistrictStore'
                }, {
                    fieldLabel: 'Город',
                    name: 'City',
                    attribute: 'City',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.CityStore'
                }, {
                    fieldLabel: 'Улица',
                    name: 'Street'
                }, {
                    fieldLabel: 'Дом',
                    name: 'Building'
                }]
            }, {
                fieldLabel: 'Канал',
                name: 'Channel',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.ChannelStore'
            }, {
                fieldLabel: 'Подканал',
                name: 'SubChannel',
                attribute: 'SubChannel',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.SubChannelStore'
            }, {
                fieldLabel: 'Формат',
                name: 'Format',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.FormatStore'
            }, {
                fieldLabel: 'Сектор',
                name: 'Sector',
                attribute: 'Sector',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.SectorStore'
            }, {
                fieldLabel: 'Участок',
                name: 'Area',
                attribute: 'Area',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.AreaStore'
            }, {
                fieldLabel: 'Дивизион',
                name: 'Division',
                attribute: 'Division',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.DivisionStore'
            }, {
                fieldLabel: 'Дата открытия',
                name: 'DateOpen'
            }, {
                fieldLabel: 'Дата закрытия',
                name: 'DateClose'
            }, {
                fieldLabel: 'Группа контрагентов',
                name: 'ContractorGroup',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.ContractorGroupStore'
            }, {
                fieldLabel: 'Субдистрибьютор',
                xtype: 'radiogroup',
                anchor: 'none',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    name: 'isSubDistributor',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'да',
                    inputValue: '1'
                }, {
                    boxLabel: 'нет',
                    inputValue: '0'
                }]
            }, {
                fieldLabel: 'Участвует в РБП',
                xtype: 'radiogroup',
                anchor: 'none',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    name: 'isInRBP',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'да',
                    inputValue: '1'
                }, {
                    boxLabel: 'нет',
                    inputValue: '0'
                }]
            }, {
                fieldLabel: 'Учитывать в расчетах',
                name: 'isAcc', //for listeners
                xtype: 'radiogroup',
                anchor: 'none',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    name: 'isAccounting',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'да',
                    inputValue: '1'
                }, {
                    boxLabel: 'нет',
                    inputValue: '0'
                }]
            }, {
                fieldLabel: 'Причина неучета',
                name: 'NonAccReason',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.NonAccReasonStore'
            }, {
                fieldLabel: 'Площадь ТТ',
                name: 'ShopArea'
            }, {
                fieldLabel: 'Координаты',
                name: 'Coordinates'
            }, {
                fieldLabel: 'GLN',
                name: 'GLN'
            }, {
                xtype: 'textareafield',
                name: 'Comment',
                fieldLabel: 'Комментарий'
            }, {
                fieldLabel: 'ФИАС',
                name: 'FIAS'
            }, {
                fieldLabel: 'Отв. менеджер',
                name: 'RespManager',
                attribute: 'ParentStaff',
                displayField: 'FIO',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.ParentStore'
            }, {
                fieldLabel: 'Статус',
                name: 'Status'
            }, {
                fieldLabel: 'Автор изменений',
                name: 'Author'
            }, {
                fieldLabel: 'Дата посл. изменения',
                name: 'DateChange'
            }]
        }]
    }],

    buttons: [{
        text: 'Сбросить все',
        handler: function () {
            this.up('window').down('form').getForm().reset();
        }
    }, '->', {
        text: 'Применить',
        handler: function () {
            var grid = this.up('window').grid,
                store = grid.store,
                formVals = this.up('window').down('form').getValues(),
                filters = [];

            var modelFields = store.model.getFields(),
                dataFields = [],
                i = 0,
                length = modelFields.length;

            for (; i < length; i++) {
                dataFields.push(modelFields[i].name);
            }

            Ext.Object.each(formVals, function (key, value, myself) {
                if (value || value === 0) {
                    if (Ext.Array.contains(dataFields, key)) {
                        filters.push({property: key, value: value});
                    }
                }
            });

            store.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
            store.fireEvent('applyFilters', store, filters, grid);

            this.up('window').destroy();
        }
    }, {
        text: 'Отмена',
        handler: function () {
            this.up('window').destroy();
        }
    }],

    initComponent: function () {
        var me = this,
            grid = me.grid;

        this.callParent(arguments);

        var cols = [],
            columns = grid.columns,
            length = columns.length;

        for (i = 0; i < length; i++) {
            if (columns[i]['dataIndex'])
                cols.push(columns[i]['dataIndex']);
        }
        cols.push('searchall');

        var fields = me.query('textfield'), //все textfield окна
            radiogroups = me.query('radiogroup');

        length = fields.length;

        for (i = 0; i < length; i++) {                //цикл указывания какие поля останутся активными
            if (!(Ext.Array.contains(cols, fields[i].name))) {
                fields[i].hide();
                fields[i].disable();
            }
        }

        length = radiogroups.length;

        for (i = 0; i < length; i++) {                //цикл указывания какие поля останутся активными
            if (!(Ext.Array.contains(cols, radiogroups[i].items.items[0].name))) {
                radiogroups[i].hide();
                radiogroups[i].disable();
            }
        }

        var store = grid.store,
            filterRecord = {},
            filters = store.filters.items;

        length = filters.length;

        if (length) {
            for (i = 0; i < length; i++) {
                filterRecord[filters[i].property] = filters[i].value;
            }
        }
        if (store.proxy.extraParams['searchall']) {
            filterRecord['searchall'] = store.proxy.extraParams['searchall'];
        }
        this.down('form').getForm().setValues(filterRecord);
    }
});

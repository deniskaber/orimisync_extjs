Ext.define('TestOrimi.view.general_headerpanel', {
    extend: 'Ext.panel.Panel',
    requires: ['Ext.draw.Text'],
    
    xtype: 'general_headerpanel',
    border: false,
    height: 56,
    cls: 'headerpanel',
    layout: {
        type: 'hbox',
        align: 'middle'
    },
    items: [{
        xtype: 'text',
        height: 20,
        name: 'user',
        text: ' ',
        margin: '0 0 0 60'
    }, {
        xtype: 'text',
        height: 20,
        name: 'ref',
        text: ' ',
        margin: '0 0 0 20'
    }, {
        xtype: 'button',
        name: 'buttonChange',
        text: 'Cменить',
        hidden: true,
        margin: '0 0 0 25',
        handler: function () {
            var viewport = this.up('[name=viewport]');

            viewport.down('[name=domCard]').getLayout().setActiveItem(0);
            viewport.down('[name=buttonChange]').hide();
            viewport.down('[name=ref]').setText(' ');
            //viewport.down('[name=user]').setMargin('0 0 0 60');
        }
    }, {
        xtype: 'container',
        flex: 1,
        layout: {
            type: 'hbox',
            align: 'middle',
            pack: 'end'
        },
        items: [{
            xtype: 'button',
            name: 'buttonOut',
            text: 'Выйти',
            margin: '0 15 0 0',
            handler: function () {
                window.location.href = 'portal.php?logout=1';
            }
        }]
    }],
    listeners: {
        afterrender: function () {
            this.down('[name=user]').setText('Пользователь: ' + TestOrimi.getApplication().data['FullName']);
        }
    }
});

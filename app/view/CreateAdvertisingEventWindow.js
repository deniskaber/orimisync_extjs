Ext.define('TestOrimi.view.CreateAdvertisingEventWindow', {
    extend: 'Ext.window.Window',
    name: 'createAdvertisingEventWindow',
    xtype: 'createAdvertisingEventWindow',
    width: 1000,
    height: 500,
    modal: true,
    title: 'Заявка на рекламную акцию',
    layout: 'fit',
    afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',

    initComponent: function() {
        var me = this;
        var userRights = TestOrimi.getApplication().data;
        var fields;
        var mcombos;
        var i;
        var statusField;

        me.buttons = [{
            text: 'Пересчет факта',
            name: 'recalculateButton',
            handler: function() {
                var win = this.up('window');
                var form = win.down('form');
                var store = Ext.getStore('TestOrimi.store.AdvertisingEventsHeadersStore');
                if (!form || win.isFormLoadingRecord) {
                    return;
                }
                var formData = form.getValues();

                win.setLoading('Загрузка...');

                Ext.Ajax.request({
                    url: 'resources/data/api.php',
                    timeout: 600000,
                    params: {
                        ID: formData['ID'],
                        act: store.proxy.extraParams['act'],
                        subaction: 'recalculate'
                    },
                    success: function(response) {
                        var responseText = Ext.JSON.decode(response.responseText);

                        if (!responseText.success) {
                            TestOrimi.getApplication().getController('Main')
                                     .showNotification('Ошибка! Не удалось выполнить пересчет фактов.', 'winError');
                            win.setLoading(false);
                            return;
                        }

                        TestOrimi.getApplication().getController('Main')
                                 .showNotification('Факты заявки пересчитаны.', 'winSuccess');
                        store.load();

                        var updatedRecord = TestOrimi.model.AdvertisingEventsHeadersModel.create(responseText.data);
                        form.loadRecord(updatedRecord);
                        win.resetManualFactToggleButton();
                        win.setLoading(false);
                    },
                    failure: function() {
                        TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сервер не отвечает на запрос', 'winError');
                        win.setLoading(false);
                    }
                });
            }
        }, {
            text: 'Ручной ввод факта',
            name: 'manualFactToggleButton',
            disabled: 'true',
            hidden: !userRights['Advertising_events_manual'],
            handler: function() {
                var win = this.up('window');

                win.fireEvent('setManualFactStatus', win);
            }
        }, '->', {
            text: 'Ок',
            name: 'okButton',
            handler: function() {
                var store = Ext.getStore('TestOrimi.store.AdvertisingEventsHeadersStore'),
                    win   = this.up('window'),
                    form  = win.down('form').getForm();

                if (form.isValid()) {
                    var myMask = new Ext.LoadMask(win, {msg: 'Сохраняю...'});

                    myMask.show();
                    form.submit({
                        url: 'resources/data/api.php',
                        params: {
                            act: store.proxy.extraParams['act'],
                            subaction: (win.data) ? (win.data.get('ID') ? 'update' : 'create') : 'create'
                        },
                        success: function(form, response) {
                            myMask.destroy();
                            if (win.data) {
                                if (win.data.get('ID') === 0) {
                                    TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess'); 
                                    store.nextRec = true;                                   
                                } else {
                                    TestOrimi.getApplication().getController('Main').showNotification('Успешно! Элемент обновлен', 'winSuccess');
                                }
                            } else {
                                TestOrimi.getApplication().getController('Main').showNotification('Успешно! Новый элемент создан', 'winSuccess');
                                store.nextRec = true;
                            }
                            store.load();
                            win.destroy();
                        },
                        failure: function(form, action) {
                            myMask.destroy();
                            TestOrimi.getApplication().getController('Main').showNotification('Ошибка! ' + ( action.result ? action.result.msg : ' Нет ответа от сервера' ), 'winError');
                        }
                    });
                }
            }
        }, {
            text: 'Отмена',
            handler: function() {
                this.up('window').destroy();
            }
        }];

        this.onAreaChange = function(field, newValue) {
            var form = me.down('form');
            if (!form) {
                return;
            }

            var storeField = form.down('[name=StoreID]');

            if (newValue && field.isValid()) {
                var areaFilter = storeField.store.filters.findBy(function(item) {
                    return item.id === 'ActiveStoresStore-AreaID-filter';
                });

                areaFilter && storeField.store.filters.remove(areaFilter);

                storeField.store.filter({
                    id: 'ActiveStoresStore-AreaID-filter',
                    property: 'AreaID',
                    value: '=' + newValue
                });

                var rec = storeField.store.findRecord('ID', storeField.getValue(), 0, false, false, true);

                if (!rec || !storeField.validate() || rec.get('AreaID') !== newValue) {
                    storeField.markInvalid('Запись не соответствует выбранному участку');
                }

            }

            me.recalculateStoreAmount();
        };

        this.recalculateStoreAmount = function() {
            var form = me.down('form');
            if (!form || me.isFormLoadingRecord) {
                return;
            }
            var formData = form.getValues();
            var netField = form.down('[name=NetID]');
            var storeAmountField = form.down('[name=StoreAmount]');
            var rec;

            storeAmountField.suspendEvents();
            storeAmountField.reset();
            storeAmountField.resumeEvents();

            if (formData['StoreID'] && form.down('[name=StoreID]').isValid()) {
                storeAmountField.setValue(1);
                return;
            }

            if (formData['AreaID'] && form.down('[name=AreaID]').isValid()) {
                me.setStoreAmount();
                return;
            }

            if (formData['NetID'] && netField.isValid()) {
                rec = netField.store.findRecord('ID', netField.getValue(), 0, false, false, true);
                if (!rec) return;

                storeAmountField.setValue(rec.get('StoresCount'));
            }

        };

        this.setStoreAmount = function() {
            var form = me.down('form');
            if (!form || me.isFormLoadingRecord) {
                return;
            }
            var areaField = form.down('[name=AreaID]');
            var netField = form.down('[name=NetID]');
            var formData = form.getValues();

            if (!formData['AreaID'] || !areaField.isValid() || !formData['NetID'] || !netField.isValid()) {
                return;
            }

            var okButton = me.down('[name=okButton]');

            okButton.disable();

            Ext.Ajax.request({
                url: 'resources/data/api.php',
                timeout: 600000,
                params: {
                    AreaID: formData['AreaID'],
                    NetID: formData['NetID'],
                    act: 'getNetStoreAmountByArea'
                },
                success: function(response) {
                    var responseText = Ext.JSON.decode(response.responseText);

                    if (!responseText.length) {
                        TestOrimi.getApplication().getController('Main')
                                 .showNotification('Ошибка! Не удалось получить кол-во ТТ сети в участке.', 'winError');
                        return;
                    }

                    var storeAmount = responseText[0] && responseText[0]['StoreAmount'];
                    var storeAmountField = form.down('[name=StoreAmount]');
                    storeAmountField && storeAmountField.setValue(storeAmount);
                    var okButton = me.down('[name=okButton]');
                    okButton && okButton.enable();
                },
                failure: function(form, action) {
                    TestOrimi.getApplication().getController('Main').showNotification('Ошибка! Сервер не отвечает на запрос', 'winError');
                    var okButton = me.down('[name=okButton]');
                    okButton && okButton.enable();
                }
            });
        };

        this.resetManualFactToggleButton = function() {
            var me = this;

            if (me.data.get('Status') === 'Ввод факта') {
                manualFactToggleButton.enable();
            } else if (this.data.get('Status') === 'Ручной ввод факта') {
                manualFactToggleButton.setText('Автоматический ввод факта');
                manualFactToggleButton.enable();
            }
        };

        this.items = [{
            xtype: 'form',
            bodyPadding: 5,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [{
                xtype: 'container',
                border: false,
                layout: 'column',
                items: [{
                    xtype: 'fieldset',
                    columnWidth: 0.5,
                    defaults: {
                        xtype: 'textfield',
                        anchor: '100%',
                        labelWidth: 150
                    },
                    border: false,
                    items: [{
                        name: 'ID',
                        fieldLabel: 'ID',
                        readOnly: true,
                        cls: 'noteditable'
                    }, {
                        name: 'DocNum',
                        fieldLabel: 'Номер заявки'
                    }, {
                        fieldLabel: 'Заголовок',
                        name: 'Title',
                        afterLabelTextTpl: me.afterLabelTextTpl,
                        allowBlank: false
                    }, {
                        xtype: 'fieldcontainer',
                        fieldLabel: 'Период акции',
                        afterLabelTextTpl: me.afterLabelTextTpl,
                        combineErrors: true,
                        msgTarget: 'side',
                        layout: 'hbox',
                        defaults: {
                            flex: 1,
                            hideLabel: true
                        },
                        items: [
                            {
                                xtype: 'datefield',
                                fieldLabel: 'Дата начала акции',
                                format: 'Y-m-d',
                                name: 'DateStart',
                                margin: '0 5 0 0',
                                allowBlank: false
                            }, {
                                xtype: 'datefield',
                                fieldLabel: 'Дата окончания акции',
                                format: 'Y-m-d',
                                name: 'DateEnd',
                                allowBlank: false
                            }
                        ]
                    }, {
                        fieldLabel: 'Группа контрагентов',
                        name: 'ContractorGroupID',
                        afterLabelTextTpl: me.afterLabelTextTpl,
                        xtype: 'modalcombo',
                        store: 'TestOrimi.store.ContractorGroupStore',
                        allowBlank: false
                    }, {
                        fieldLabel: 'Участок',
                        name: 'AreaID',
                        xtype: 'modalcombo',
                        store: 'TestOrimi.store.AvailableAreaStore',
                        additionalListeners: {
                            'orimi_afterModalcomboChange': me.onAreaChange
                        }
                    }, {
                        fieldLabel: 'Название сети',
                        name: 'NetID',
                        afterLabelTextTpl: me.afterLabelTextTpl,
                        attribute: 'Net',
                        xtype: 'modalcombo',
                        store: 'TestOrimi.store.NetStore',
                        allowBlank: false,
                        additionalListeners: {
                            'orimi_afterModalcomboChange': me.recalculateStoreAmount
                        }
                    }, {
                        fieldLabel: 'Название ТТ',
                        name: 'StoreID',
                        afterLabelTextTpl: me.afterLabelTextTpl,
                        attribute: 'Store',
                        xtype: 'modalcombo',
                        store: 'TestOrimi.store.ActiveStoresStore',
                        queryDelay: 750,
                        checkChangeBuffer: 500,
                        allowBlank: false,
                        additionalListeners: {
                            'orimi_afterModalcomboChange': me.recalculateStoreAmount
                        }
                    }, {
                        fieldLabel: 'Канал для рекламы',
                        name: 'AdvertisingChannel',
                        cls: 'noteditable',
                        readOnly: true
                    }, {
                        xtype: 'numberfield',
                        fieldLabel: 'Количество ТТ',
                        name: 'StoreAmount',
                        minValue: 1,
                        cls: 'noteditable',
                        readOnly: true
                    }, {
                        xtype: 'fieldcontainer',
                        fieldLabel: 'Период отгрузки',
                        combineErrors: true,
                        msgTarget: 'side',
                        layout: 'hbox',
                        defaults: {
                            flex: 1,
                            hideLabel: true
                        },
                        items: [
                            {
                                xtype: 'datefield',
                                fieldLabel: 'Дата начала отгрузки',
                                format: 'Y-m-d',
                                name: 'DateStartShipping',
                                margin: '0 5 0 0'
                            }, {
                                xtype: 'datefield',
                                fieldLabel: 'Дата окончания отгрузки',
                                format: 'Y-m-d',
                                name: 'DateEndShipping'
                            }
                        ]
                    }, {
                        fieldLabel  : 'Отражена в TradeNT',
                        xtype       : 'radiogroup',
                        defaultType : 'radiofield',
                        name        : 'isInTradentRadiogroup',
                        afterLabelTextTpl: '<span style="color:red;font-weight:bold" data-qtip="Поле обязательно к заполнению">*</span>',
                        vertical    : true,
                        labelWidth  : 150,
                        columns     : 2,
                        layout      : 'hbox',
                        items: [{
                            boxLabel    : 'да',
                            name        : 'isInTradent',
                            inputValue  : 1
                        },{
                            boxLabel    : 'нет',
                            name        : 'isInTradent',
                            inputValue  : 0,
                            checked     : true,
                            margin      : '0 0 0 20'
                        }]
                    }, {
                        name: 'TSZCode',
                        fieldLabel: 'Номер ТСЗ'
                    }, {
                        fieldLabel: 'Тип оплаты',
                        name: 'PaymentTypeID',
                        xtype: 'modalcombo',
                        store: 'TestOrimi.store.PaymentTypeStore'
                    }]
                }, {
                    columnWidth: 0.5,
                    xtype: 'fieldset',
                    defaults: {
                        xtype: 'textfield',
                        anchor: '100%',
                        labelWidth: 160
                    },
                    border: false,
                    items: [{
                        fieldLabel: 'Статус заявки',
                        name: 'StatusID',
                        xtype: 'modalcombo',
                        store: 'TestOrimi.store.RequestStatusStore',
                        value: 1
                    }, {
                        fieldLabel: 'Автор изменений',
                        name: 'Author',
                        readOnly: true,
                        cls: 'noteditable'
                    }, {
                        fieldLabel: 'Дата посл. изменения',
                        name: 'DateChange',
                        readOnly: true,
                        cls: 'noteditable'
                    }, {
                        xtype: 'textareafield',
                        name: 'ReturnComment',
                        fieldLabel: 'Комментарий к статусу акции',
                        height: 148,
                        readOnly: true,
                        cls: 'noteditable'
                    }, {
                        xtype: 'textareafield',
                        height: 148,
                        name: 'Comment',
                        fieldLabel: 'Комментарий'
                    }]
                }]
            }]
        }];

        this.callParent(arguments);

        var form = me.down('form');
        var manualFactToggleButton = me.down('[name=manualFactToggleButton]');
        
        statusField = form.down('[name=StatusID]');
        var isInTradentRadiogroup = form.down('[name=isInTradentRadiogroup]');
        var isInTradentRadios = form.query('[name=isInTradent]');

        if (!userRights['Advertising_events_edit'] || (this.data && this.data.get('Status') !== 'Ввод плана')) {
            fields = form.query('textfield');
            mcombos = form.query('modalcombo');
            radiofields = form.query('radiofield');

            for (i = 0; i < mcombos.length; i++) {
                mcombos[i].readOnly = true;
                mcombos[i].hideTrigger = true;
                mcombos[i].cls = 'noteditable';
            }

            for (i = 0; i < fields.length; i++) {
                fields[i].readOnly = true;
                fields[i].cls = 'noteditable';

                if (fields[i].xtype === 'datefield') {
                    fields[i].up('fieldcontainer') && fields[i].up('fieldcontainer').addCls('noteditable');
                }
            }

            for (i = 0; i < radiofields.length; i++) {
                radiofields[i].readOnly = true;
                radiofields[i].up('radiogroup').addCls('noteditable');
            }

            this.down('[name=okButton]').disable();
        }

        if (userRights['Advertising_events_delete']) {
            statusField.readOnly = false;
            statusField.hideTrigger = false;
            statusField.cls = 'editable';

            this.down('[name=okButton]').enable();
        } else {
            statusField.readOnly = true;
            statusField.hideTrigger = true;
            statusField.cls = 'noteditable';
        }

        if (
            (userRights['Advertising_events_sales_appr'] || userRights['Advertising_events_s_boss_appr'])
            && (this.data && (this.data.get('Status') === 'План на утверждении в отделе продаж' || this.data.get('Status') === 'План на утверждении у старшего менеджера отдела продаж'))
        ) {
            isInTradentRadiogroup.removeCls('noteditable');
            isInTradentRadios.forEach(function(field) {
                field.readOnly = false;
            });
        }

        if (me.data) {
            me.isFormLoadingRecord = true;
            form.loadRecord(me.data);

            me.resetManualFactToggleButton();

            this.isFormLoadingRecord = false;
        }
    }
});

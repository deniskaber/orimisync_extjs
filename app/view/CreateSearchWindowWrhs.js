Ext.define('TestOrimi.view.CreateSearchWindowWrhs', {
    extend: 'Ext.window.Window',
    title: 'Комбинированный поиск',
    modal: true,
    width: 500,
    height: 600,
    layout: 'fit',
    items: [{
        xtype: 'form',
        margin: 10,
        border: false,
        layout: {type: 'vbox', align: 'stretch'},
        items: [{
            xtype: 'textfield'
            , fieldLabel: 'Поиск по всем атрибутам'
            , name: 'searchall'
            , anchor: '90%'
            , labelWidth: 170
        }, {
            fieldLabel: 'Применить ко всем справочникам'
            , xtype: 'radiogroup'
            , labelWidth: 170
            , anchor: 'none'
            , layout: {
                autoFlex: false
            },
            defaults: {
                name: 'allwarehouses',
                margin: '0 15 0 0'
            }
            , items: [{
                boxLabel: 'нет',
                inputValue: 0
            }, {
                boxLabel: 'да',
                inputValue: 1
            }, {
                boxLabel: 'только подчиненные',
                inputValue: 2,
                margin: 0
            }]
        }, {
            xtype: 'container',
            flex: 1,
            autoScroll: true,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            margin: '20 0 0 0',
            defaults: {
                xtype: 'textfield',
                anchor: '90%',
                labelWidth: 160,
                margin: '3 15 2 5'
            },
            items: [{
                fieldLabel: 'ID',
                name: 'ID'
            }, {
                fieldLabel: 'Код',
                name: 'Code'
            }, {
                fieldLabel: 'Название',
                name: 'Name'
            }, {
                fieldLabel: 'Черновой адрес',
                name: 'TempAddress'
            }, {
                fieldLabel: 'Полный адрес',
                name: 'FullAddress'
            }, {
                xtype: 'fieldset',
                title: 'Адресные данные',
                collapsible: true,
                collapsed: true,
                defaultType: 'textfield',
                defaults: {anchor: '100%', labelWidth: 150},
                layout: 'anchor',
                items: [{
                    fieldLabel: 'Адрес',
                    name: 'Address'
                }, {
                    fieldLabel: 'Индекс',
                    name: 'PostCode'
                }, {
                    fieldLabel: 'Страна',
                    name: 'Country',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.CountryStore'
                }, {
                    fieldLabel: 'Округ',
                    name: 'County',
                    attribute: 'County',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.CountyStore'
                }, {
                    fieldLabel: 'Регион',
                    name: 'Region',
                    attribute: 'Region',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.RegionStore'
                }, {
                    fieldLabel: 'Район',
                    name: 'District',
                    attribute: 'District',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.DistrictStore'
                }, {
                    fieldLabel: 'Город',
                    name: 'City',
                    attribute: 'City',
                    xtype: 'searchModalCombo',
                    store: 'TestOrimi.store.CityStore'
                }, {
                    fieldLabel: 'Улица',
                    name: 'Street'
                }, {
                    fieldLabel: 'Дом',
                    name: 'Building'
                }]
            }, {
                fieldLabel: 'Формат',
                name: 'Format',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.FormatStore'
            }, {
                fieldLabel: 'Сектор',
                name: 'Sector',
                attribute: 'Sector',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.SectorStore'
            }, {
                fieldLabel: 'Участок',
                name: 'Area',
                attribute: 'Area',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.AreaStore'
            }, {
                fieldLabel: 'Дивизион',
                name: 'Division',
                attribute: 'Division',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.DivisionStore'
            }, {
                fieldLabel: 'Дата открытия',
                name: 'DateOpen'
            }, {
                fieldLabel: 'Дата закрытия',
                name: 'DateClose'
            }, {
                fieldLabel: 'Дистрибьютор',
                name: 'Distributor',
                attribute: 'Distributor',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.DistributorsStore'
            }, {
                fieldLabel: 'Код Дистр.',
                name: 'DistrCode'
            }, {
                fieldLabel: 'Группа контрагентов',
                name: 'ContractorGroup',
                xtype: 'searchModalCombo',
                store: 'TestOrimi.store.ContractorGroupStore'
            }, {
                fieldLabel: 'Площадь',
                name: 'ShopArea'
            }, {
                xtype: 'textareafield',
                name: 'Comment',
                fieldLabel: 'Комментарий'
            }, {
                fieldLabel: 'ФИАС',
                name: 'FIAS'
            }, {
                fieldLabel: 'Статус',
                name: 'Status'
            }, {
                fieldLabel: 'Источник',
                name: 'DataSource'
            }, {
                fieldLabel: 'Автор изменений',
                name: 'Author'
            }, {
                fieldLabel: 'Дата посл. изменения',
                name: 'DateChange'
            }, {
                fieldLabel: 'Авто/Ручной',
                xtype: 'radiogroup',
                anchor: 'none',
                layout: {
                    autoFlex: false
                },
                defaults: {
                    name: 'SyncType',
                    margin: '0 15 0 0'
                },
                items: [{
                    boxLabel: 'Авто',
                    inputValue: 1
                }, {
                    boxLabel: 'Ручной',
                    inputValue: 0
                }]
            }]
        }]
    }],

    buttons: [{
        text: 'Сбросить все',
        handler: function () {
            this.up('window').down('form').getForm().reset();
        }
    }, '->', {
        text: 'Применить',
        handler: function () {
            var grid = this.up('window').grid,
                store = grid.store,
                formVals = this.up('window').down('form').getValues(),
                filters = [];

            var modelFields = store.model.getFields(),
                dataFields = [],
                i = 0,
                length = modelFields.length;

            for (; i < length; i++) {
                dataFields.push(modelFields[i].name);
            }

            Ext.Object.each(formVals, function (key, value, myself) {
                if (value || value === 0) {
                    if (Ext.Array.contains(dataFields, key)) {
                        filters.push({property: key, value: value});
                    }
                }
            });

            if (formVals['allwarehouses'] == 1) {
                var masterStore = Ext.getStore('TestOrimi.store.MasterWarehousesStore'),
                    syncedStore = Ext.getStore('TestOrimi.store.SyncedWarehousesStore'),
                    slaveStore = Ext.getStore('TestOrimi.store.SlaveWarehousesStore');

                masterStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                masterStore.fireEvent('applyFilters', masterStore, filters, grid.up('viewport').down('wrhs_general_grid'));
                syncedStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                syncedStore.fireEvent('applyFilters', syncedStore, filters, grid.up('viewport').down('wrhs_general_tabpanel').down('wrhs_general_tabpanelFirstcol'));
                slaveStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                slaveStore.fireEvent('applyFilters', slaveStore, filters, grid.up('viewport').down('wrhs_general_tabpanel').down('wrhs_general_tabpanelSecondcol'));
            } else if (formVals['allwarehouses'] == 2) {
                var syncedStore = Ext.getStore('TestOrimi.store.SyncedWarehousesStore'),
                    slaveStore = Ext.getStore('TestOrimi.store.SlaveWarehousesStore');

                syncedStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                syncedStore.fireEvent('applyFilters', syncedStore, filters, grid.up('viewport').down('wrhs_general_tabpanel').down('wrhs_general_tabpanelFirstcol'));
                slaveStore.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                slaveStore.fireEvent('applyFilters', slaveStore, filters, grid.up('viewport').down('wrhs_general_tabpanel').down('wrhs_general_tabpanelSecondcol'));
            } else {
                store.proxy.extraParams['searchall'] = formVals['searchall'] ? formVals['searchall'] : '';
                store.fireEvent('applyFilters', store, filters, grid);
            }

            this.up('window').destroy();
        }
    }, {
        text: 'Отмена',
        handler: function () {
            this.up('window').destroy();
        }
    }],

    initComponent: function () {
        var me = this,
            grid = me.grid,
            i;

        this.callParent(arguments);

        var cols = [],
            columns = grid.columns,
            length = columns.length;

        for (i = 0; i < length; i++) {
            if (columns[i]['dataIndex'])
                cols.push(columns[i]['dataIndex']);
        }
        cols.push('searchall');
        cols.push('allwarehouses');

        var fields = me.query('textfield'), //все textfield окна
            radiogroups = me.query('radiogroup');

        length = fields.length;

        for (i = 0; i < length; i++) {                //цикл указывания какие поля останутся активными
            if (!(Ext.Array.contains(cols, fields[i].name))) {
                fields[i].hide();
                fields[i].disable();
            }
        }

        length = radiogroups.length;

        for (i = 0; i < length; i++) {                //цикл указывания какие поля останутся активными
            if (!(Ext.Array.contains(cols, radiogroups[i].items.items[0].name))) {
                radiogroups[i].hide();
                radiogroups[i].disable();
            }
        }

        var store = grid.store,
            filterRecord = {},
            filters = store.filters.items;

        length = filters.length;

        if (length) {
            for (i = 0; i < length; i++) {
                filterRecord[filters[i].property] = filters[i].value;
            }
        }
        if (store.proxy.extraParams['searchall']) {
            filterRecord['searchall'] = store.proxy.extraParams['searchall'];
        }
        this.down('form').getForm().setValues(filterRecord);
    }
});

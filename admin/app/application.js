Ext.define('OrimiAdmin.Application', {
    name: 'OrimiAdmin',

    extend: 'Ext.app.Application',

    controllers: [
		'Main'
    ],

    requires:[
		'Ext.form.*',
		'Ext.Img',
		'Ext.tip.QuickTipManager'
    ],

    init: function(){
		Ext.tip.QuickTipManager.init();
	}
});

//fix for chrome
Ext.apply(Ext.EventManager,{      
      normalizeEvent: function(eventName, fn) {
            
            //start fix
            var EventManager = Ext.EventManager,
                  supports = Ext.supports;
            if(Ext.chromeVersion >=43 && eventName == 'mouseover'){
                  var origFn = fn;
                  fn = function(){
                        var me = this,
                              args = arguments;
                        setTimeout(
                              function(){
                                    origFn.apply(me || Ext.global, args);
                              },
                        0);
                  };
            } 
            //end fix
            
            if (EventManager.mouseEnterLeaveRe.test(eventName) && !supports.MouseEnterLeave) {
                  if (fn) {
                        fn = Ext.Function.createInterceptor(fn, EventManager.contains);
                  }
                  eventName = eventName == 'mouseenter' ? 'mouseover' : 'mouseout';
            } else if (eventName == 'mousewheel' && !supports.MouseWheel && !Ext.isOpera) {
                  eventName = 'DOMMouseScroll';
            }
            return {
                  eventName: eventName,
                  fn: fn
            };
      }
});

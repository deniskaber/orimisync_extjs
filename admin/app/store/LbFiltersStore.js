Ext.define('OrimiAdmin.store.LbFiltersStore', {
    extend: 'Ext.data.TreeStore',
    fields: [
		{ name: 'id',  type: 'string'},
		{ name: 'text',  type: 'string'},
		{ name: 'value',  type: 'int'}
	],
	proxy: {
		type: 'ajax',
		url: '../resources/data/api.php',
		reader: 'json',
		extraParams: {
			act: 'getSectorUserFilter'
		}
	},
	root: {
		id: 'root',
		text: "Все",
		expanded: true,
		checked: false
	},
	noCache: false,
	autoLoad: false,
	storeId: 'LbFiltersStore'
});

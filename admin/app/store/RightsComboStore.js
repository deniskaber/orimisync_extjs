Ext.define('OrimiAdmin.store.RightsComboStore', {
    extend: 'Ext.data.Store',
	fields: [
		{ name: 'value', type: 'int'},
		{ name: 'text',  type: 'string'}
	],
	data : [
		{value: 0,    text: 'Нет'},
		{value: 1,    text: 'Просмотр'},
		{value: 2,    text: 'Редактирование'}
	],
	autoLoad: true,
	storeId: 'RightsComboStore'
});

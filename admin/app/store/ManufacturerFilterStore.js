Ext.define('OrimiAdmin.store.ManufacturerFilterStore', {
    extend: 'Ext.data.TreeStore',
    fields: [
		{ name: 'id',  type: 'string'},
		{ name: 'text',  type: 'string'},
		{ name: 'value',  type: 'int'}
	],
	proxy: {
		type: 'ajax',
		url: '../resources/data/api.php',
		reader: 'json',
		extraParams: {
			act: 'getManufacturerUserFilter'
		}
	},
	root: {
		id: 'root',
		text: "Все",
		expanded: true,
		checked: false
	},
	noCache: false,
	autoLoad: false,
	storeId: 'ManufacturerFilterStore'
});

Ext.define('OrimiAdmin.store.CategoryFilterStore', {
    extend: 'Ext.data.TreeStore',
    fields: [
		{ name: 'id',  type: 'string'},
		{ name: 'text',  type: 'string'},
		{ name: 'value',  type: 'int'}
	],
	proxy: {
		type: 'ajax',
		url: '../resources/data/api.php',
		reader: 'json',
		extraParams: {
			act: 'getCategoryUserFilter'
		}
	},
	root: {
		id: 'root',
		text: "Все",
		expanded: true,
		checked: false
	},
	noCache: false,
	autoLoad: false,
	storeId: 'CategoryFilterStore'
});

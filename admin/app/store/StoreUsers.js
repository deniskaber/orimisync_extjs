Ext.define('OrimiAdmin.store.StoreUsers', {
    extend: 'Ext.data.Store',
	model: 'OrimiAdmin.model.ModelUser',
	pageSize: 1000,
	proxy: {
		type: 'ajax',
		url: '../resources/data/api.php', 
		timeout: 60000,
		reader: {
			type: 'json'
			//,root: 'data'
			//,totalProperty: 'total'
		},
		extraParams: {
			act: 'getUsers'
		}
	},
	//remoteFilter: true,
	autoLoad: true,
	storeId: 'StoreUsers'
});

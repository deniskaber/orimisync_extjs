Ext.define('OrimiAdmin.model.ModelUser', {
	extend: 'Ext.data.Model',
	idProperty: 'idUser',
    fields: [
		{name: "ID",type: "int"},
		{name: "Name", type: "string", matcher: /([a-z]+)[0-9]{2,3}/},
		{name: "FullName",type: "string"}, 
		{name: "Role",type: "string"}, 
		{name: "RoleID",type: "int"}, 
		{name: "isDeleted", type: "int"}, 
		{name: "EMail", type: "string"}, 
		{name: "Tel1", type: "string"}, 
		{name: "DateCreate", type: "string"}, 
		{name: "DateBeginAccess", type: "string"}, 
		{name: "DateEndAccess", type: "string"}, 
		{name: "Password", type: "string"}, 
		{name: "Comment", type: "string"}
	],
	hasMany: [
		{model: 'OrimiAdmin.model.ModelSector',name: 'sectors'},
		{model: 'OrimiAdmin.model.ModelNet',name: 'nets'},
		{model: 'OrimiAdmin.model.ModelCategory',name: 'categories'},
		{model: 'OrimiAdmin.model.ModelManufacturer',name: 'manufacturers'}
	]
});

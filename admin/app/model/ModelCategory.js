Ext.define('OrimiAdmin.model.ModelCategory', {
	extend: 'Ext.data.Model',
    belongsTo: 'OrimiAdmin.model.ModelUser',
    fields: [
		{name: "ElementID",type: "int"},
		{name: "Name", type: "string"},
		{name: "DimensionType", type: "string"}
	]
});

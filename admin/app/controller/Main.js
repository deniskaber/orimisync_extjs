Ext.define('OrimiAdmin.controller.Main', {
	extend: 'Ext.app.Controller',
	xtype: 'mainController',
	stores: [
		'StoreUsers',
		'StoreRoles',
		'LbFiltersStore',
		'NetFilterStore',
		'CategoryFilterStore',
		'ManufacturerFilterStore',
		'RightsComboStore'

	],
	models: [
		'OrimiAdmin.model.ModelUser',
		'OrimiAdmin.model.ModelSector',
		'OrimiAdmin.model.ModelNet',
		'OrimiAdmin.model.ModelCategory',
		'OrimiAdmin.model.ModelManufacturer'
	],
	views: [
		'OrimiAdmin.view.Header',
		'OrimiAdmin.view.ContentPanel',
		'OrimiAdmin.view.UsersGrid',
		'OrimiAdmin.view.NetTree',
		'OrimiAdmin.view.SectorTree',
		'OrimiAdmin.view.CategoryTree',
		'OrimiAdmin.view.ManufacturerTree',
		'OrimiAdmin.view.UserFields',
		'OrimiAdmin.view.AddOrEditUserWindowContent',
		'OrimiAdmin.view.RolesGrid',
		'OrimiAdmin.view.RolesFields'
	],
	
	refs: [{
		ref: 'viewport',
		selector: 'viewport'
	},{
		ref: 'appHeader',
		selector: 'appHeader'
	},{
		ref: 'contentPanel',
		selector: 'contentPanel'
	},{
		ref: 'usersGrid',
		selector: 'usersGrid'
	},{
		ref: 'userFields',
		selector: 'userFields'
	},{
		ref: 'sectorFilter',
		selector: 'sectorFilter'
	},{
		ref: 'netFilter',
		selector: 'netFilter'
	},{
		ref: 'categoryFilter',
		selector: 'categoryFilter'
	},{
		ref: 'manufacturerFilter',
		selector: 'manufacturerFilter'
	},{
		ref: 'addOrEditUserWindowContent',
		selector: 'addOrEditUserWindowContent'
	},{
		ref: 'rolesGrid',
		selector: 'rolesGrid'
	}],
	

	init: function() {
		this.control({
			'usersGrid': {
				searchchange: this.onColumnSearchChange,
				createuserbuttonclick: this.onUsersGridCreateUserButtonClick,
				deleteuserbuttonclick: this.onUsersGridDeleteUserButtonClick,
				edituserbuttonclick: this.onUsersGridEditUserButtonClick,
				celldblclick: this.onUsersGridCelldblclick
			},
			'sectorFilter': {
				checkchange: this.onSectorFilterCheckChange
			},
			'netFilter': {
				checkchange: this.onNetFilterCheckChange
			},
			'categoryFilter': {
				checkchange: this.onCategoryFilterCheckChange
			},
			'manufacturerFilter': {
				checkchange: this.onManufacturerFilterCheckChange
			},
			'rolesGrid': {
				createrolebuttonclick: this.onRolesGridCreateRoleButtonClick,
				deleterolebuttonclick: this.onRolesGridDeleteRoleButtonClick,
				editrolebuttonclick: this.onRolesGridEditRoleButtonClick,
				celldblclick: this.onRolesGridCelldblclick
			}
		});
		Ext.getStore('StoreUsers').addListener('load',this.onUsersStoreAfterLoad,this);
		Ext.getStore('LbFiltersStore').addListener('beforeload', this.onFiltersStoreBeforeLoad, this);
		Ext.getStore('NetFilterStore').addListener('beforeload', this.onFiltersStoreBeforeLoad, this);
		Ext.getStore('CategoryFilterStore').addListener('beforeload', this.onFiltersStoreBeforeLoad, this);
		Ext.getStore('ManufacturerFilterStore').addListener('beforeload', this.onFiltersStoreBeforeLoad, this);
	},
	
	
	onFiltersStoreBeforeLoad: function(store,operation) {
		store.proxy.extraParams['checked'] = operation.node.data.checked;
	},
	
	onUsersStoreAfterLoad: function (store){
		if (store.getTotalCount()!=0) {
			var selModel = this.getUsersGrid().getView().getSelectionModel();
		
			selModel.deselectAll();
			selModel.select(0);
		}
	},
	
	onColumnSearchChange: function(grid, field, newValue, oldValue) {
		var column = field.up('gridcolumn'),
			filters = grid.store.filters.items,
			i = 0,
			length = filters.length;

		if (length) {
			for (;i<length;i++) {
				if (filters[i].property == column.dataIndex) {
					grid.store.filters.removeAt(i);
					break;
				}
			}
		}

		if (newValue) {
			switch (column.dataIndex) {
				case 'sectors' :
					grid.store.filter({property: column.dataIndex, filterFn: function(item) {
						var	flag = false,
							sectors = item.raw.sectors,
							i = 0,
							length = sectors.length;
						
						if (length) {
							for (;i<length;i++) {
								if (sectors[i].ElementID == -1) {
									if ('все'.indexOf(newValue.toLowerCase()) >= 0)
										flag = true; 
								} else {
									if(sectors[i].Name.toLowerCase().indexOf(newValue.toLowerCase()) >= 0)
										flag = true; 
								}
							}
						} else {
							if ('нет прав'.indexOf(newValue.toLowerCase()) >= 0)
								flag = true; 
						}

						return flag;						
					}});
				break;
				case 'nets' :
					grid.store.filter({property: column.dataIndex, filterFn: function(item) {
						var flag = false,
							nets = item.raw.nets,
							i = 0,
							length = nets.length;
						
						if (length) {
							for (;i<length;i++) {
								if (nets[i].ElementID == -1) {
									if ('все'.indexOf(newValue.toLowerCase()) >= 0)
										flag = true; 
								} else {
									if(nets[i].Name.toLowerCase().indexOf(newValue.toLowerCase()) >= 0)
										flag = true; 
								}
							}
						} else {
							if ('нет прав'.indexOf(newValue.toLowerCase()) >= 0)
								flag = true; 
						}

						return flag;					
					}});
				break;
				default:
					grid.store.filter({property: column.dataIndex, value: newValue, anyMatch: true});
				break;
			}
			column.addCls('ux-filtered-column');
		} else {
			if (grid.store.filters.items.length)
				grid.store.filter();
			else
				grid.store.clearFilter();
			
			column.removeCls('ux-filtered-column');			
		}
	},

	onUsersGridEditUserButtonClick: function(){
		var me = this,
			selectedRecord = this.getUsersGrid().getSelectionModel().getSelection()[0],
			win = Ext.create('Ext.Window', {
				title: 'Редактирование пользователя',
				resizable: false,
				modal: true,
				//height: 500,
				//width: 650,
				flex: 1,
				layout	: 'fit',
				items: [{
					xtype		: 'form',
					border 		: false,
					layout		: {
						type	: 'vbox',
						align	: 'stretch'
					},
					url: '../resources/data/api.php?act=getUsers&subaction=update', //--way to DB--------------------
					timeout: 120,
					items: [{
						xtype: 'addOrEditUserWindowContent',
						userData: selectedRecord
					}],
					buttons: [{
						text: 'Сохранить',
						handler: function() {
							var form = this.up('form').getForm(),
								formUp = this.up('form'),
								vals = me.getFiltersVals();
							
							vals['ID'] = selectedRecord.get('ID'); //??
							if (form.isValid()){
								if (formUp.down('[name=DateBeginAccess]').value>formUp.down('[name=DateEndAccess]').value)
									Ext.Msg.alert('Ошибка', 'Дата окончания доступа не может быть раньше даты открытия доступа')
								else {
									var myMask = new Ext.LoadMask(win, {msg:"Сохранение..."});

									myMask.show();
									form.submit({
										params: vals,
										success: function(form, action) {
											 myMask.destroy();
											 Ext.Msg.alert('Успешно', 'Пользователь сохранен');
											 Ext.getStore('StoreUsers').load();
											 win.close();
										},
										failure: function(form, action) {
											myMask.destroy();
											Ext.Msg.alert('Ошибка', action.result ? action.result.msg : 'Нет ответа от сервера');
										}
									});
								}
							}
						}
					},{
						text: 'Отмена',
						handler: function() {
							win.close();
						}
					}]
				}]
			});
		
		win.show();
	},
	
	onUsersGridCelldblclick: function(grid, td, cellIndex, record, tr, rowIndex) {
		this.onUsersGridEditUserButtonClick();
	},

	checkLogin: function(store, name) {
		if (store.findExact('Name',name) >= 0)
			return true;
		else
			return false;
	},
	
	onUsersGridCreateUserButtonClick: function () {
		var me = this;
		var win = Ext.create('Ext.Window', {
			title: 'Создание пользователя',
			resizable: false,
			modal: true,
			//height: 500,
			//width: 650,
			flex: 1,
			layout	: 'fit',
			items: [{
				xtype: 'form',
				border: false,
				fieldDefaults: {
					msgTarget: 'side',
					labelWidth: 200
				},
				url: '../resources/data/api.php?act=getUsers&subaction=create', //--------way to DB
				timeout: 120,
				items: [{
					xtype: 'addOrEditUserWindowContent'
				}],
				buttons: [{
					text: 'Создать',
					handler: function() {
						var form = this.up('form').getForm(),
							formUp = this.up('form'),
							vals = me.getFiltersVals();
						
						if (form.isValid()) {
							if (formUp.down('[name=DateBeginAccess]').value>formUp.down('[name=DateEndAccess]').value)
								Ext.Msg.alert('Ошибка', 'Дата окончания доступа не может быть раньше даты открытия доступа');
							else {
								if (me.checkLogin(Ext.getStore('StoreUsers'),formUp.down('[name=Name]').value))
									Ext.Msg.alert('Ошибка', 'Пользователь с таким именем уже зарегистрирован');
								else {
									var myMask = new Ext.LoadMask(win, {msg:"Создается..."});

									myMask.show();
									form.submit({
										params: vals,
										success: function(form, action) {
											 myMask.destroy();
											 Ext.Msg.alert('Успешно', 'Пользователь создан');
											 Ext.getStore('StoreUsers').load();
											 win.destroy();
										},
										failure: function(form, action) {
											myMask.destroy();
											Ext.Msg.alert('Ошибка', action.result ? action.result.msg : 'Нет ответа от сервера');
										}
									});
								}
							}
						}
					}
				},{
					text: 'Отмена',
					handler: function() {
						win.destroy();
					}
				}]
			}]
		});
		win.show();
	},
	
	onUsersGridDeleteUserButtonClick: function () {
		var me = this,
			selectedRecord = this.getUsersGrid().getSelectionModel().getSelection()[0];
		
		if (selectedRecord) {
			Ext.Msg.show({
				title:'Удаление',
				msg:'Удалить учетную запись пользователя '+selectedRecord.get('Name')+'?', 
				buttons: Ext.Msg.YESNO,
				fn: function(btn){
					if (btn == 'yes'){
						Ext.Ajax.request({
							url: '../resources/data/api.php', //way to db------------------------
							params: {
								ID: selectedRecord.get('ID'),
								act: 'getUsers',
								subaction: 'delete'
							},
							success: function(response){
								var text = response.responseText;
								Ext.getStore('StoreUsers').load();
							},
							failure: function(form, action) {
								Ext.Msg.alert('Ошибка', 'Произошла ошибка');
							}
						});
					}
				}
			});
		}
	},

	onRolesGridEditRoleButtonClick: function(){
		var me = this,
			selectedRecord = this.getRolesGrid().getSelectionModel().getSelection()[0];

		var win = Ext.create('Ext.Window', {
			title: 'Редактирование роли',
			resizable: false,
			modal: true,
			flex: 1,
			layout	: 'fit',
			items: [{
				xtype: 'roleFields',
				url: '../resources/data/api.php', //--------way to DB
				timeout: 120,
				data: selectedRecord.data
			}],
			buttons: [{
				text: 'Сохранить',
				handler: function() {
					var formUp = this.up('window').down('roleFields'),
						form = formUp.getForm();
					
					me.setAttributesRightsForCatalogs(formUp);
					
					if (form.isValid()) {
						var myMask = new Ext.LoadMask(win, {msg:"Создается..."});

						myMask.show();
						form.submit({
							params: {
								ID: selectedRecord.get('ID'),
								act: 'getRoles',
								subaction: 'update'
							},
							success: function(form, action) {
								 myMask.destroy();
								 Ext.Msg.alert('Успешно', 'Роль обновлена');
								 Ext.getStore('StoreRoles').load();
								 win.destroy();
							},
							failure: function(form, action) {
								myMask.destroy();
								Ext.Msg.alert('Ошибка', action.result ? action.result.msg : 'Нет ответа от сервера');
							}
						});													
					} else {
						Ext.Msg.alert('Ошибка', 'Права доступа настроены некорректно. Проверьте вкладки справочников.');
					}
				}
			},{
				text: 'Отмена',
				handler: function() {
					win.destroy();
				}
			}]
		});
		win.show();
	},

	onRolesGridCreateRoleButtonClick: function(){
		var me = this;
		var win = Ext.create('Ext.Window', {
			title: 'Создание роли',
			resizable: false,
			modal: true,
			flex: 1,
			layout	: 'fit',
			items: [{
				xtype: 'roleFields',
				url: '../resources/data/api.php?act=getRoles&subaction=create', //--------way to DB
				timeout: 120
			}],
			buttons: [{
				text: 'Создать',
				handler: function() {
					var formUp = this.up('window').down('roleFields'),
						form = formUp.getForm();

					me.setAttributesRightsForCatalogs(formUp);
					
					if (!form.isValid()) {
						Ext.Msg.alert('Ошибка', 'Права доступа настроены некорректно. Проверьте вкладки справочников.');
						return;
					}


					if (me.checkLogin(Ext.getStore('StoreRoles'),formUp.down('[name=Name]').value)) {
						Ext.Msg.alert('Ошибка', 'Роль с таким именем уже существует');
						return;
					}

					var myMask = new Ext.LoadMask(win, {msg:"Создается..."});

					myMask.show();
					form.submit({
						success: function(form, action) {
							myMask.destroy();
							Ext.Msg.alert('Успешно', 'Роль создана');
							Ext.getStore('StoreRoles').load();
							win.destroy();
						},
						failure: function(form, action) {
							myMask.destroy();
							Ext.Msg.alert('Ошибка', action.result ? action.result.msg : 'Нет ответа от сервера');
						}
					});
				}
			},{
				text: 'Отмена',
				handler: function() {
					win.destroy();
				}
			}]
		});
		win.show();
	},

	setAttributesRightsForCatalogs: function(panel) {
		var checkCombos,
			length,
			i;

		checkCombos = panel.down('[name=SkuTab]').query('checkbox');
		length = checkCombos.length;

		for (i=0; i<length; i++) {
			/*check all fields like Sku_draft_create, Sku_draft_set, Sku_active_set*/
			if (checkCombos[i].name.match(new RegExp('_set$')) || checkCombos[i].name.match(new RegExp('_create$'))) {
				if (checkCombos[i].getValue()) {
					skuAttributesSet(checkCombos[i], true); //Просмотр
					break;
				}
			}
		}

		checkCombos = panel.down('[name=TTTab]').query('checkbox');
		length = checkCombos.length;

		for (i=0; i<length; i++) {
			/*check all fields like TT_draft_create, TT_draft_set, TT_active_set*/
			if (checkCombos[i].name.match(new RegExp('_set$')) || checkCombos[i].name.match(new RegExp('_create$'))) {
				if (checkCombos[i].getValue()) {
					ttAttributesSet(checkCombos[i], true); //Просмотр
					break;
				}
			}
		}

		checkCombos = panel.down('[name=StaffTab]').query('checkbox');
		length = checkCombos.length;

		for (i=0; i<length; i++) {
			/*check all fields like Staff_draft_create, Staff_draft_set, Staff_active_set*/
			if (checkCombos[i].name.match(new RegExp('_set$')) || checkCombos[i].name.match(new RegExp('_create$'))) {
				if (checkCombos[i].getValue()) {
					staffAttributesSet(checkCombos[i], true); //Просмотр
					break;
				}
			}
		}

		checkCombos = panel.down('[name=DistrTab]').query('checkbox');
		length = checkCombos.length;

		for (i=0; i<length; i++) {
			/*check all fields like Distr_draft_create, Distr_draft_set, Distr_active_set*/
			if (checkCombos[i].name.match(new RegExp('_set$')) || checkCombos[i].name.match(new RegExp('_create$'))) {
				if (checkCombos[i].getValue()) {
					distrAttributesSet(checkCombos[i], true); //Просмотр
					break;
				}
			}
		}
		
		

		function skuAttributesSet(field, newValue) {
			if(newValue) {
				var tabpanel = field.up('tabpanel');
				var combos = tabpanel.down('[name=AttributesTab]').query('combobox');
				var sku_fields = ['Manufacturer','Brand','SubBrand','Category','Segment','SubSegment'
					,'TeaColor','ExternalPack','SachetPack','InternalPack','WeightGroup','Flavor','Adding'
					,'PriceSegment','Country','CoffeeSort','Gift','GiftType'
				];

				tabpanel.down('[name=Attributes]').setValue(true);

				for (var i=0; i<combos.length; i++) {
					if (sku_fields.indexOf(combos[i].name.replace('Attributes_','')) > -1) {
						if (!combos[i].getValue()) {
							combos[i].setValue(1); //Просмотр
						}
					}
				}
			}
		}

		function ttAttributesSet(field, newValue) {
			if(newValue) {
				var tabpanel = field.up('tabpanel');
				var combos = tabpanel.down('[name=AttributesTab]').query('combobox');
				var tt_fields = ['Country','County','Region','District','City',
					'Division','Area','Sector','Channel','SubChannel','Format',
					'Net','NetType'
				];

				tabpanel.down('[name=Attributes]').setValue(true);

				for (var i=0; i<combos.length; i++) {
					if (tt_fields.indexOf(combos[i].name.replace('Attributes_','')) > -1) {
						if (!combos[i].getValue()) {
							combos[i].setValue(1); //Просмотр
						}
					}
				}
			}
		}

		function staffAttributesSet(field, newValue) {
			if(newValue) {
				var tabpanel = field.up('tabpanel');
				var combos = tabpanel.down('[name=AttributesTab]').query('combobox');
				var staff_fields = ['Type','Division','Motivation','Sector'];

				tabpanel.down('[name=Attributes]').setValue(true);

				for (var i=0; i<combos.length; i++) {
					if (staff_fields.indexOf(combos[i].name.replace('Attributes_','')) > -1) {
						if (!combos[i].getValue()) {
							combos[i].setValue(1); //Просмотр
						}
					}
				}
			}
		}

        function distrAttributesSet(field, newValue) {
            if(newValue) {
                var tabpanel = field.up('tabpanel');
                var combos = tabpanel.down('[name=AttributesTab]').query('combobox');
                var tt_fields = ['Country','County','Region','District','City',
                    'Division','Area','Sector','Channel','SubChannel','Format',
                    'Currancy','ContractorGroup','NonAccReason'
                ];

                tabpanel.down('[name=Attributes]').setValue(true);

                for (var i=0; i<combos.length; i++) {
                    if (tt_fields.indexOf(combos[i].name.replace('Attributes_','')) > -1) {
                        if (!combos[i].getValue()) {
                            combos[i].setValue(1); //Просмотр
                        }
                    }
                }
            }
        }
	},

	onRolesGridDeleteRoleButtonClick: function(){
		var me = this,
			selectedRecord = this.getRolesGrid().getSelectionModel().getSelection()[0];

		if (selectedRecord && (selectedRecord.get('ID') != 6)) {
			Ext.Msg.show({
				title:'Удаление',
				msg:'Удалить роль '+selectedRecord.get('Name')+'? Пользователи с этой ролью станут неактивны', 
				buttons: Ext.Msg.YESNO,
				fn: function(btn){
					if (btn == 'yes'){
						Ext.Ajax.request({
							url: '../resources/data/api.php', //way to db------------------------
							params: {
								ID: selectedRecord.get('ID'),
								act: 'getRoles',
								subaction: 'delete'
							},
							success: function(response){
								var text = Ext.JSON.decode(response.responseText);
								if (text['success']) {
									Ext.getStore('StoreRoles').load();
									Ext.getStore('StoreUsers').load();
								}
								else
									Ext.Msg.alert('Ошибка', 'Произошла ошибка');
							},
							failure: function(form, action) {
								Ext.Msg.alert('Ошибка', 'Произошла ошибка');
							}
						});
					}
				}
			});
		} else
			Ext.Msg.alert('Ошибка', 'Роль не выбрана или эту роль нельзя удалить');
	},

	onRolesGridCelldblclick: function(grid, td, cellIndex, record, tr, rowIndex) {
		this.onRolesGridEditRoleButtonClick();
	},
	
	onAdminStoreBeforeLoad: function(store,operation) {
		this.RepStoreGetPars('adminGrid',store,operation);
	},
	
	getFiltersVals: function() {
		var filters = this.getAddOrEditUserWindowContent(), //error
			sectors = [],
			nets = [],
			categories = [],
			manufacturers = [],
			vals = {sector: sectors, net: nets, category: categories, manufacturer: manufacturers},
			sectorTree = filters.down('sectorFilter'),
			netTree = filters.down('netFilter'),
			categoryTree = filters.down('categoryFilter'),
			manufacturerTree = filters.down('manufacturerFilter');
		
		if (sectorTree.getRootNode().get('checked')) {
			vals['sector'] = "-1";
		} else {
			sectors = sectorTree.getRootNode().childNodes;
			for (var i=0;i<sectors.length;i++){
				if (sectors[i].get('checked'))
					vals['sector'].push(sectors[i].get('value'));
			}
			if (vals['sector'].length)
				vals['sector'] = vals['sector'].toString();
			else
				vals['sector'] = "-1";
		}

		if (netTree.getRootNode().get('checked')) {
			vals['net'] = "-1";
		} else {
			nets = netTree.getRootNode().childNodes;
			for (var i=0;i<nets.length;i++){
				if (nets[i].get('checked'))
					vals['net'].push(nets[i].get('value'));
			}
			if (vals['net'].length)
				vals['net'] = vals['net'].toString();
			else
				vals['net'] = "-1";
		}

		if (categoryTree.getRootNode().get('checked')) {
			vals['category'] = "-1";
		} else {
			categories = categoryTree.getRootNode().childNodes;
			for (var i=0;i<categories.length;i++){
				if (categories[i].get('checked'))
					vals['category'].push(categories[i].get('value'));
			}
			if (vals['category'].length)
				vals['category'] = vals['category'].toString();
			else
				vals['category'] = "-1";
		}

		if (manufacturerTree.getRootNode().get('checked')) {
			vals['manufacturer'] = "-1";
		} else {
			manufacturers = manufacturerTree.getRootNode().childNodes;
			for (var i=0;i<manufacturers.length;i++){
				if (manufacturers[i].get('checked'))
					vals['manufacturer'].push(manufacturers[i].get('value'));
			}
			if (vals['manufacturer'].length)
				vals['manufacturer'] = vals['manufacturer'].toString();
			else
				vals['manufacturer'] = "-1";
		}

		return vals;
	},
	
	RepStoreGetPars: function(gridName,store,operation) {
		var filters = this.getAddOrEditUserWindowContent(),
			contentPanel = this.getContentPanel(),
			rep = contentPanel.down(gridName),
			pars = [],
			vals = this.getFiltersVals();
			
		operation.limit = Math.floor((rep.view.getHeight() - 20)/23);
		store.pageSize = Math.floor((rep.view.getHeight() - 20)/23);
		
		/*if (vals)
			store.proxy.extraParams['sector'] = vals;
		else
			store.proxy.extraParams['sector'] = '';*/
	},
	
	onSectorFilterCheckChange: function(node, checked) {
		this.onFiltersCheckChange(node, checked, this.getSectorFilter(), true);
	},

	onNetFilterCheckChange: function(node, checked) {
		this.onFiltersCheckChange(node, checked, this.getNetFilter(), true);
	},

	onCategoryFilterCheckChange: function(node, checked) {
		this.onFiltersCheckChange(node, checked, this.getCategoryFilter(), true);
	},

	onManufacturerFilterCheckChange: function(node, checked) {
		this.onFiltersCheckChange(node, checked, this.getManufacturerFilter(), true);
	},

	onFiltersCheckChange: function(node, checked, tree, load) {
		var me = this;
		if (!node.get('leaf')) {
			node.set('cls','');
			node.set('partial', false);

			if (load) {
				node.getOwnerTree().store.load({ 
					node: node,
					callback: function() {
						if (node.data.id != 'root') {
							me.filtersChildCheck(node, checked, tree);
						}
					}
				});
			} else {
				me.filtersChildCheck(node, checked, tree);
			}
		} else {
			me.filtersChildCheck(node, checked, tree);
		}
	},
	
	
	filtersChildCheck: function(node, checked, tree) {
		var me = this;
		
		if (node.data.id != 'root') {
			if (checked) {
				var i = 0,
					n = 0,
					length = node.parentNode.childNodes.length;
				for (; i < length; i++) {
					if (node.parentNode.childNodes[i].get('checked')) {
						n++;
					}
				}
				if (n == length) {
					node.parentNode.set('cls','');
					node.parentNode.set('partial', false);
					node.parentNode.set('checked', true);
					me.filtersChildCheck(node.parentNode,true,tree);
					me.onFiltersTextDecorationChange(node.parentNode,tree,'none');
				} else {
					node.parentNode.set('cls','node-und');
					node.parentNode.set('partial', true);
					node.parentNode.set('checked', false);
					me.filtersChildCheck(node.parentNode,false,tree);
					me.onFiltersTextDecorationChange(node.parentNode,tree,'underline');
				}
			} else {
				var i = 0,
					length = node.parentNode.childNodes.length;
				for (; i < length; i++) {
					if (node.parentNode.childNodes[i].get('checked')) {
						node.parentNode.set('cls','node-und');
						node.parentNode.set('partial', true);
						node.parentNode.set('checked', false);
						me.filtersChildCheck(node.parentNode,false,tree);
						me.onFiltersTextDecorationChange(node.parentNode,tree,'underline');
						break;
					}
					if (i == length - 1) {
						node.parentNode.set('cls','');
						node.parentNode.set('partial', false);
						node.parentNode.set('checked', false);
						me.filtersChildCheck(node.parentNode,false,tree);
						me.onFiltersTextDecorationChange(node.parentNode,tree,'none');
					}
				}
			}
		}
	},
	
	
	onFiltersTextDecorationChange: function(node,tree,style) {
		if (node.internalId!='root') {
			if (style == 'underline') {
				if (node.isLeaf())
					node.parentNode.expand();
				node.parentNode.set('partial', true);
				this.onFiltersTextDecorationChange(node.parentNode,tree,style);
			} else {
				var i = 0,
					length = node.parentNode.childNodes.length;
				for (; i < length; i++) {
					if (node.parentNode.childNodes[i].get('checked'))	
						break;
					
					if (i == length - 1) {
						if (node.isLeaf())
							node.parentNode.expand();
						node.parentNode.set('partial', false);
						this.onFiltersTextDecorationChange(node.parentNode,tree,style);
					}
				}
			}
		}
	},
	
	getPartial: function(view) {
		var partial = [];
		view.node.cascadeBy(function(rec){
			if (rec.get('partial')) {
				partial.push(rec);
			}
		});
		return partial;
	}

});
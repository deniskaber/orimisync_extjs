Ext.define('OrimiAdmin.view.AddOrEditUserWindowContent', {
	extend: 'Ext.panel.Panel',
	xtype: 'addOrEditUserWindowContent',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	
	initComponent: function(){
		var me = this;

		this.items = [{
			xtype: 'sectorFilter',
			userRights: me.userData ? me.userData.getAssociatedData()['sectors'] : false
		},{
			xtype: 'netFilter',
			userRights: me.userData ? me.userData.getAssociatedData()['nets'] : false
		},{
			xtype: 'categoryFilter',
			userRights: me.userData ? me.userData.getAssociatedData()['categories'] : false
		},{
			xtype: 'manufacturerFilter',
			userRights: me.userData ? me.userData.getAssociatedData()['manufacturers'] : false
		},{
			xtype: 'userFields',
			userData: me.userData ? me.userData.data : false
		}];
		
		this.callParent(arguments);
	}
});
			

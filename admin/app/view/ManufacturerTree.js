Ext.define('OrimiAdmin.view.ManufacturerTree', {
    extend: 'Ext.tree.Panel',
    xtype: 'manufacturerFilter',
    width: 300,
    height: 475,
	requires: ['Ext.form.field.Text'],
	useArrows: false,
	rootVisible: true,
	enableAnimations: false,
	title: 'Производители',
	autoScroll: true,
	plugins: {
        ptype: 'bufferedrenderer',
        trailingBufferZone: 1,
        leadingBufferZone: 1,
		variableRowHeight: true
    },
	hideHeaders: true,
	animate: false,
	columns: [{
		xtype: 'treecolumn',
		text: 'SKU',
		flex: 1,
		dataIndex: 'text'
	}],
	vals: [],
	userRights: false,
	border: false,
	style: 'border-right: 1px solid silver;',
	initComponent: function() {
		var me = this;

		this.store = 'ManufacturerFilterStore';	
		this.callParent(arguments);
		
		me.getRootNode().set('checked',false);
		var lb = me.getRootNode().childNodes;
		for (var i=0;i<lb.length;i++){
			lb[i].set('checked',false);
		}
		
		if (this.userRights) {
			if (this.userRights.length) {
				if (this.userRights[0].ElementID=="-1"){
					me.getRootNode().set('checked',true);
					var lb = me.getRootNode().childNodes;
					for (var i=0;i<lb.length;i++){
						lb[i].set('checked',true);
					}
					return;
				} else {
					for (var r=0;r<this.userRights.length;r++){
						var node = me.store.getNodeById(this.userRights[r].ElementID);
						
						if (node) {
							node.set('checked',true);
							me.fireEvent('checkchange',node,true);
						}
					}
				}
			}			
		}
	}
});

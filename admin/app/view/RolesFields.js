Ext.define('OrimiAdmin.view.RolesFields', {
    extend: 'Ext.form.Panel',
    xtype: 'roleFields',
    requires: ['Ext.form.field.Text'],
    width: 960,
    height: 640,
    border: false,
    bodyPadding: '10',
    layout: 'anchor',
    required: '<span style="color:red;font-weight:bold" data-qtip="Необходимое поле">*</span>',
    defaultType: 'textfield',
    fieldDefaults: {
        msgTarget: 'side'
    },

    initComponent: function() {
        var me = this;
        var nameField;

        me.items = [{
            xtype: 'textfield',
            fieldLabel: 'Название',
            afterLabelTextTpl: this.required,
            name: 'Name',
            anchor: '100%',
            allowBlank: false
        }, {
            xtype: 'tabpanel',
            plain: true,
            border: false,
            flex: 1,
            hide_tabs: function(tabname, newValue) {
                var panel = this.down('[name=' + tabname + ']'),
                    tab = panel.tab;

                if (newValue) {
                    if (panel.xtype === 'checkboxgroup') {
                        panel.allowBlank = false;
                    }
                    tab.show();
                } else {
                    if (panel.xtype === 'checkboxgroup') {
                        panel.allowBlank = true;
                    }
                    tab.hide();
                }
            },
            items: [{
                xtype: 'checkboxgroup',
                allowBlank: false,
                blankText: 'Необходимо выбрать права доступа',
                title: 'Разделы',
                layout: 'anchor',
                defaults: {
                    xtype: 'checkbox',
                    inputValue: 1
                },
                items: [{
                    boxLabel: 'Справочник: Номенклатура',
                    name: 'Sku',
                    listeners: {
                        change: function(a, newValue, oldValue, eOpts) {
                            this.up('tabpanel').hide_tabs('SkuTab', newValue);
                        }
                    }
                }, {
                    boxLabel: 'Справочник: Торговые точки',
                    name: 'TT',
                    listeners: {
                        change: function(a, newValue, oldValue, eOpts) {
                            this.up('tabpanel').hide_tabs('TTTab', newValue);
                        }
                    }
                }, {
                    boxLabel: 'Справочник: Сотрудники',
                    name: 'Staff',
                    listeners: {
                        change: function(a, newValue, oldValue, eOpts) {
                            this.up('tabpanel').hide_tabs('StaffTab', newValue);
                        }
                    }
                }, {
                    boxLabel: 'Справочник: Дистрибьюторы',
                    name: 'Distr',
                    listeners: {
                        change: function(a, newValue, oldValue, eOpts) {
                            this.up('tabpanel').hide_tabs('DistrTab', newValue);
                        }
                    }
                }, {
                    boxLabel: 'Атрибуты',
                    name: 'Attributes',
                    listeners: {
                        change: function(a, newValue, oldValue, eOpts) {
                            this.up('tabpanel').hide_tabs('AttributesTab', newValue);
                        }
                    }
                }, {
                    boxLabel: 'Справочник: Склады',
                    name: 'Wrhs',
                    listeners: {
                        change: function(a, newValue, oldValue, eOpts) {
                            this.up('tabpanel').hide_tabs('WrhsTab', newValue);
                        }
                    }
                }, {
                    boxLabel: 'Реклама',
                    name: 'Advertising',
                    listeners: {
                        change: function(a, newValue, oldValue, eOpts) {
                            this.up('tabpanel').hide_tabs('AdvertisingTab', newValue);
                        }
                    }
                }]
            }, {
                xtype: 'checkboxgroup',
                allowBlank: true,
                blankText: 'Необходимо выбрать права доступа',
                title: 'Номенклатура',
                name: 'SkuTab',
                hidden: true,
                layout: 'hbox',
                items: [{
                    xtype: 'container',
                    items: [{
                        xtype: 'fieldset',
                        title: 'Черновики',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр'
                            , name: 'Sku_draft_get'
                        }, {
                            boxLabel: '<b>Создание</b>'
                            , name: 'Sku_draft_create'
                        }, {
                            boxLabel: '<b>Редактирование</b>'
                            , name: 'Sku_draft_set'
                        }, {
                            boxLabel: 'Удаление'
                            , name: 'Sku_draft_delete'
                        }, {
                            boxLabel: 'Перевод в активный статус'
                            , name: 'Sku_draft_activate'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Активные',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр'
                            , name: 'Sku_active_get'
                        }, {
                            boxLabel: '<b>Редактирование</b>'
                            , name: 'Sku_active_set'
                        }, {
                            boxLabel: 'Удаление'
                            , name: 'Sku_active_delete'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Удаленные',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр'
                            , name: 'Sku_deleted_get'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Подчиненные справочники',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Источник TradeNT'
                            , name: 'Sku_slave_tradent_get'
                        }, {
                            boxLabel: 'Источник Сети'
                            , name: 'Sku_slave_net_get'
                        }, {
                            boxLabel: 'Источник Дистрибьюторы'
                            , name: 'Sku_slave_distr_get'
                        }, {
                            boxLabel: 'Синхронизация'
                            , name: 'Sku_slave_sync'
                        }]
                    }]
                }]
            }, {
                xtype: 'checkboxgroup',
                allowBlank: true,
                blankText: 'Необходимо выбрать права доступа',
                title: 'Торговые точки',
                name: 'TTTab',
                hidden: true,
                layout: 'hbox',
                items: [{
                    xtype: 'container',
                    items: [{
                        xtype: 'fieldset',
                        title: 'Черновики',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр'
                            , name: 'TT_draft_get'
                        }, {
                            boxLabel: '<b>Создание</b>'
                            , name: 'TT_draft_create'
                        }, {
                            boxLabel: '<b>Редактирование</b>'
                            , name: 'TT_draft_set'
                        }, {
                            boxLabel: 'Удаление'
                            , name: 'TT_draft_delete'
                        }, {
                            boxLabel: 'Перевод в активный статус'
                            , name: 'TT_draft_activate'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Активные',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр'
                            , name: 'TT_active_get'
                        }, {
                            boxLabel: '<b>Редактирование</b>'
                            , name: 'TT_active_set'
                        }, {
                            boxLabel: 'Удаление'
                            , name: 'TT_active_delete'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Удаленные',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр'
                            , name: 'TT_deleted_get'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Подчиненные справочники',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Источник TradeNT'
                            , name: 'TT_slave_tradent_get'
                        }, {
                            boxLabel: 'Источник Сети'
                            , name: 'TT_slave_net_get'
                        }, {
                            boxLabel: 'Источник Дистрибьюторы'
                            , name: 'TT_slave_distr_get'
                        }, {
                            boxLabel: 'Синхронизация'
                            , name: 'TT_slave_sync'
                        }]
                    }]
                }, {
                    xtype: 'container',
                    margin: '0 0 0 15',
                    items: [{
                        xtype: 'fieldset',
                        title: 'Дополнительно',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Установка канала для рекламы'
                            , name: 'TT_AdvChannel_set'
                        }]
                    }]
                }]
            }, {
                xtype: 'checkboxgroup',
                allowBlank: true,
                blankText: 'Необходимо выбрать права доступа',
                title: 'Сотрудники',
                name: 'StaffTab',
                hidden: true,
                layout: 'hbox',
                items: [{
                    xtype: 'container',
                    items: [{
                        xtype: 'fieldset',
                        title: 'Черновики',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр'
                            , name: 'Staff_draft_get'
                        }, {
                            boxLabel: '<b>Создание</b>'
                            , name: 'Staff_draft_create'
                        }, {
                            boxLabel: '<b>Редактирование</b>'
                            , name: 'Staff_draft_set'
                        }, {
                            boxLabel: 'Удаление'
                            , name: 'Staff_draft_delete'
                        }, {
                            boxLabel: 'Перевод в активный статус'
                            , name: 'Staff_draft_activate'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Активные',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр'
                            , name: 'Staff_active_get'
                        }, {
                            boxLabel: '<b>Редактирование</b>'
                            , name: 'Staff_active_set'
                        }, {
                            boxLabel: 'Удаление'
                            , name: 'Staff_active_delete'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Удаленные',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр'
                            , name: 'Staff_deleted_get'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Подчиненные справочники',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Источник TradeNT'
                            , name: 'Staff_slave_tradent_get'
                        }, {
                            boxLabel: 'Источник Сети'
                            , name: 'Staff_slave_net_get'
                        }, {
                            boxLabel: 'Источник Дистрибьюторы'
                            , name: 'Staff_slave_distr_get'
                        }, {
                            boxLabel: 'Синхронизация'
                            , name: 'Staff_slave_sync'
                        }]
                    }]
                }]
            }, {
                xtype: 'checkboxgroup',
                allowBlank: true,
                blankText: 'Необходимо выбрать права доступа',
                title: 'Дистрибьюторы',
                name: 'DistrTab',
                hidden: true,
                layout: 'hbox',
                items: [{
                    xtype: 'container',
                    items: [{
                        xtype: 'fieldset',
                        title: 'Черновики',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр'
                            , name: 'Distr_draft_get'
                        }, {
                            boxLabel: '<b>Создание</b>'
                            , name: 'Distr_draft_create'
                        }, {
                            boxLabel: '<b>Редактирование</b>'
                            , name: 'Distr_draft_set'
                        }, {
                            boxLabel: 'Удаление'
                            , name: 'Distr_draft_delete'
                        }, {
                            boxLabel: 'Перевод в активный статус'
                            , name: 'Distr_draft_activate'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Активные',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр'
                            , name: 'Distr_active_get'
                        }, {
                            boxLabel: '<b>Редактирование</b>'
                            , name: 'Distr_active_set'
                        }, {
                            boxLabel: 'Удаление'
                            , name: 'Distr_active_delete'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Удаленные',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр'
                            , name: 'Distr_deleted_get'
                        }]
                    }]
                }]
            }, {
                title: 'Атрибуты',
                name: 'AttributesTab',
                hidden: true,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [{
                    xtype: 'fieldset',
                    title: 'Товары',
                    layout: 'fit',
                    items: [{
                        xtype: 'checkboxgroup',
                        columns: 4,
                        defaults: {
                            xtype: 'combobox',
                            store: 'RightsComboStore',
                            editable: false,
                            queryMode: 'local',
                            displayField: 'text',
                            valueField: 'value',
                            margin: '0 5 5 0',
                            width: 220,
                            value: 0
                        },
                        items: [{
                            fieldLabel: 'Производитель',
                            name: 'Attributes_Manufacturer'
                        }, {
                            fieldLabel: 'Категория',
                            name: 'Attributes_Category'
                        }, {
                            fieldLabel: 'Сегмент',
                            name: 'Attributes_Segment'
                        }, {
                            fieldLabel: 'Сабсегмент',
                            name: 'Attributes_SubSegment'
                        }, {
                            fieldLabel: 'Бренд',
                            name: 'Attributes_Brand'
                        }, {
                            fieldLabel: 'Саббренд',
                            name: 'Attributes_SubBrand'
                        }, {
                            fieldLabel: 'Цвет чая',
                            name: 'Attributes_TeaColor'
                        }, {
                            fieldLabel: 'Сорт кофе',
                            name: 'Attributes_CoffeeSort'
                        }, {
                            fieldLabel: 'Аромат',
                            name: 'Attributes_Flavor'
                        }, {
                            fieldLabel: 'Добавки',
                            name: 'Attributes_Adding'
                        }, {
                            fieldLabel: 'Тип упаковки внешней',
                            name: 'Attributes_ExternalPack'
                        }, {
                            fieldLabel: 'Тип упаковки саше',
                            name: 'Attributes_SachetPack'
                        }, {
                            fieldLabel: 'Тип упаковки внутренней',
                            name: 'Attributes_InternalPack'
                        }, {
                            fieldLabel: 'Тип подарка',
                            name: 'Attributes_GiftType'
                        }, {
                            fieldLabel: 'Акционная позиция',
                            name: 'Attributes_Gift'
                        }, {
                            fieldLabel: 'Вес(группа)',
                            name: 'Attributes_WeightGroup'
                        }, {
                            fieldLabel: 'Ценовой сегмент',
                            name: 'Attributes_PriceSegment'
                        }]
                    }]
                }, {
                    xtype: 'fieldset',
                    title: 'География',
                    layout: 'fit',
                    items: [{
                        xtype: 'checkboxgroup',
                        columns: 4,
                        defaults: {
                            xtype: 'combobox',
                            store: 'RightsComboStore',
                            editable: false,
                            queryMode: 'local',
                            displayField: 'text',
                            valueField: 'value',
                            margin: '0 5 5 0',
                            width: 220,
                            value: 0
                        },
                        items: [{
                            fieldLabel: 'Страна',
                            name: 'Attributes_Country'
                        }, {
                            fieldLabel: 'Округ',
                            name: 'Attributes_County'
                        }, {
                            fieldLabel: 'Регион',
                            name: 'Attributes_Region'
                        }, {
                            fieldLabel: 'Район',
                            name: 'Attributes_District'
                        }, {
                            fieldLabel: 'Город',
                            name: 'Attributes_City'
                        }]
                    }]
                }, {
                    xtype: 'fieldset',
                    title: 'Торговые точки',
                    layout: 'fit',
                    items: [{
                        xtype: 'checkboxgroup',
                        columns: 4,
                        defaults: {
                            xtype: 'combobox',
                            store: 'RightsComboStore',
                            editable: false,
                            queryMode: 'local',
                            displayField: 'text',
                            valueField: 'value',
                            margin: '0 5 5 0',
                            width: 220,
                            value: 0
                        },
                        items: [{
                            fieldLabel: 'Канал',
                            name: 'Attributes_Channel'
                        }, {
                            fieldLabel: 'Подканал',
                            name: 'Attributes_SubChannel'
                        }, {
                            fieldLabel: 'Сеть',
                            name: 'Attributes_Net'
                        }, {
                            fieldLabel: 'Тип сети',
                            name: 'Attributes_NetType'
                        }, {
                            fieldLabel: 'Формат',
                            name: 'Attributes_Format'
                        }, {
                            fieldLabel: 'Канал для рекламы',
                            name: 'Attributes_AdvertisingChannel'
                        }]
                    }]
                }, {
                    xtype: 'fieldset',
                    title: 'Сотрудники',
                    layout: 'fit',
                    items: [{
                        xtype: 'checkboxgroup',
                        columns: 4,
                        defaults: {
                            xtype: 'combobox',
                            store: 'RightsComboStore',
                            editable: false,
                            queryMode: 'local',
                            displayField: 'text',
                            valueField: 'value',
                            margin: '0 5 5 0',
                            width: 220,
                            value: 0
                        },
                        items: [{
                            fieldLabel: 'Тип сотрудника',
                            name: 'Attributes_TypeStaff'
                        }, {
                            fieldLabel: 'Мотивация',
                            name: 'Attributes_Motivation'
                        }]
                    }]
                }, {
                    xtype: 'fieldset',
                    title: 'Прочее',
                    layout: 'fit',
                    items: [{
                        xtype: 'checkboxgroup',
                        columns: 4,
                        defaults: {
                            xtype: 'combobox',
                            store: 'RightsComboStore',
                            editable: false,
                            queryMode: 'local',
                            displayField: 'text',
                            valueField: 'value',
                            margin: '0 5 5 0',
                            width: 220,
                            value: 0
                        },
                        items: [{
                            fieldLabel: 'Сектор',
                            name: 'Attributes_Sector'
                        }, {
                            fieldLabel: 'Участок',
                            name: 'Attributes_Area'
                        }, {
                            fieldLabel: 'Дивизион',
                            name: 'Attributes_Division'
                        }, {
                            fieldLabel: 'Валюта',
                            name: 'Attributes_Currancy'
                        }, {
                            fieldLabel: 'Причина неучета',
                            name: 'Attributes_NonAccReason'
                        }, {
                            fieldLabel: 'Источник данных по сетям',
                            name: 'Attributes_NetSalesDataSource'
                        }, {
                            fieldLabel: 'Статус заявки на рекламную акцию',
                            name: 'Attributes_RequestStatus'
                        }, {
                            fieldLabel: 'Тип оплаты',
                            name: 'Attributes_PaymentType'
                        }]
                    }]
                }]
            }, {
                xtype: 'checkboxgroup',
                allowBlank: true,
                blankText: 'Необходимо выбрать права доступа',
                title: 'Склады',
                name: 'WrhsTab',
                hidden: true,
                layout: 'hbox',
                items: [{
                    xtype: 'container',
                    items: [{
                        xtype: 'fieldset',
                        title: 'Черновики',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр'
                            , name: 'Wrhs_draft_get'
                        }, {
                            boxLabel: '<b>Создание</b>'
                            , name: 'Wrhs_draft_create'
                        }, {
                            boxLabel: '<b>Редактирование</b>'
                            , name: 'Wrhs_draft_set'
                        }, {
                            boxLabel: 'Удаление'
                            , name: 'Wrhs_draft_delete'
                        }, {
                            boxLabel: 'Перевод в активный статус'
                            , name: 'Wrhs_draft_activate'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Активные',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр'
                            , name: 'Wrhs_active_get'
                        }, {
                            boxLabel: '<b>Редактирование</b>'
                            , name: 'Wrhs_active_set'
                        }, {
                            boxLabel: 'Удаление'
                            , name: 'Wrhs_active_delete'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Удаленные',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр'
                            , name: 'Wrhs_deleted_get'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Подчиненные справочники',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Синхронизация'
                            , name: 'Wrhs_slave_sync'
                        }]
                    }]
                }]
            }, {
                xtype: 'checkboxgroup',
                allowBlank: true,
                blankText: 'Необходимо выбрать права доступа',
                title: 'Реклама',
                name: 'AdvertisingTab',
                hidden: true,
                layout: 'hbox',
                items: [{
                    xtype: 'container',
                    items: [{
                        xtype: 'fieldset',
                        title: 'Распоряжения генерального директора',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр',
                            name: 'Advertising_boss'
                        }, {
                            boxLabel: 'Смена статусов',
                            name: 'Advertising_boss_set_stat'
                        }]
                    }, {
                        xtype: 'fieldset',
                        title: 'Рекламные акции',
                        layout: 'anchor',
                        defaults: {
                            xtype: 'checkbox',
                            inputValue: 1
                        },
                        items: [{
                            boxLabel: 'Просмотр',
                            name: 'Advertising_events'
                        }, {
                            boxLabel: 'Создание и Редактирование',
                            name: 'Advertising_events_edit'
                        }, {
                            boxLabel: 'Утверждение от отдела рекламы',
                            name: 'Advertising_events_adv_appr'
                        }, {
                            boxLabel: 'Утверждение от отдела продаж',
                            name: 'Advertising_events_sales_appr'
                        }, {
                            boxLabel: 'Утверждение от старшего менеджера отдела продаж',
                            name: 'Advertising_events_s_boss_appr'
                        }, {
                            boxLabel: 'Утверждение сетью',
                            name: 'Advertising_events_net_appr'
                        }, {
                            boxLabel: 'Установка ручного ввода факта',
                            name: 'Advertising_events_manual'
                        }, {
                            boxLabel: 'Утверждение после ввода факта',
                            name: 'Advertising_events_fact_appr'
                        }, {
                            boxLabel: 'Утверждение от РП',
                            name: 'Advertising_events_rp_appr'
                        }, {
                            boxLabel: 'Любая смена статусов',
                            name: 'Advertising_events_delete'
                        }]
                    }]
                }]
            }]
        }];

        this.callParent(arguments);

        if (this.data) {
            this.getForm().setValues(this.data);
            nameField = this.down('[name=Name]');

            nameField.readOnly = true;
            nameField.addCls('noteditable');
        }
    }
});
Ext.define('OrimiAdmin.view.UsersGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'usersGrid',
	
	selType: 'rowmodel',
	border: false,
	disableSelection: false,
	stateId: 'stg',
	flex: 1,
	
	viewConfig: {
		shrinkWrap: 0,
		shadow: false,
		trackOver: false,
		overItemCls: false
	},
	
	autoscroll: true,
	
	rightsRenderer: function (val) {
		if (val==1) {
			return '<span style="color:red; text-align:right; font-weight:bold">+</span>';
		}
	},
	
	sectorRenderer: function(value, metaData, record, row, col, store, gridView){
		var str = '<div style="max-height:100px;overflow-y:auto">';
		if (!record.raw.sectors[0] || record.raw.sectors[0].ElementID==0)
			return '<div style="color:red;font-weight:bold">нет прав</div>';
		if (record.raw.sectors[0].ElementID==-1)
			return '<div style="color:green;font-weight:bold">все</div>';
		
		Ext.Array.each(record.raw.sectors, function(sector, index){
			str += sector.Name+'<br>';
		});
		str+='</div>';
		return str;
	},

	netRenderer: function(value, metaData, record, row, col, store, gridView){
		var str = '<div style="max-height:100px;overflow-y:auto">';
		if (!record.raw.nets[0] || record.raw.nets[0].ElementID==0)
			return '<div style="color:red;font-weight:bold">нет прав</div>';
		if (record.raw.nets[0].ElementID==-1)
			return '<div style="color:green;font-weight:bold">все</div>';
		
		Ext.Array.each(record.raw.nets, function(net, index){
			str += net.Name+'<br>';
		});
		str+='</div>';
		return str;
	},

	categoryRenderer: function(value, metaData, record, row, col, store, gridView){
		var str = '<div style="max-height:100px;overflow-y:auto">';
		if (!record.raw.categories[0] || record.raw.categories[0].ElementID==0)
			return '<div style="color:red;font-weight:bold">нет прав</div>';
		if (record.raw.categories[0].ElementID==-1)
			return '<div style="color:green;font-weight:bold">все</div>';
		
		Ext.Array.each(record.raw.categories, function(category, index){
			str += category.Name+'<br>';
		});
		str+='</div>';
		return str;
	},

	manufacturerRenderer: function(value, metaData, record, row, col, store, gridView){
		var str = '<div style="max-height:100px;overflow-y:auto">';
		if (!record.raw.manufacturers[0] || record.raw.manufacturers[0].ElementID==0)
			return '<div style="color:red;font-weight:bold">нет прав</div>';
		if (record.raw.manufacturers[0].ElementID==-1)
			return '<div style="color:green;font-weight:bold">все</div>';
		
		Ext.Array.each(record.raw.manufacturers, function(manufacturer, index){
			str += manufacturer.Name+'<br>';
		});
		str+='</div>';
		return str;
	},
	
	initComponent: function() {
		var me = this;

		this.store = 'StoreUsers';
		
		this.columns = [{	
			text: "ID",
			dataIndex: 'ID',
			sortable: true,
			width: 60,
			tooltip: 'ID пользователя'
		},{	
			text: "Логин",
			dataIndex: 'Name',
			sortable: true,
			tooltip: 'Логин пользователя, используемый для входа в систему',
			width: 200
		},{	
			text: "Отключен",
			dataIndex: 'isDeleted',
			sortable: true,
			tooltip: 'Флаг активности учетной записи',
			width: 120,
			renderer: this.rightsRenderer
		},{	
			text: "Роль",
			dataIndex: 'Role',
			sortable: true,
			width: 120
		},{	
			text: "Полное имя",
			dataIndex: 'FullName',
			sortable: true,
			tooltip: 'Полное имя пользователя',
			width: 200
		},{	
			text: "E-mail",
			dataIndex: 'EMail',
			sortable: true,
			width: 180
		},{	
			text: "Телефон",
			dataIndex: 'Tel1',
			sortable: true,
			width: 120
		},{	
			text: "Начало доступа",
			xtype: 'datecolumn',
			format : "Y-m-d",
			dataIndex: 'DateBeginAccess',
			sortable: true,
			tooltip: 'Дата начала доступа',
			width: 170
		},{	
			text: "Конец доступа",
			xtype: 'datecolumn',
			format : "Y-m-d",
			dataIndex: 'DateEndAccess',
			sortable: true,
			tooltip: 'Дата окончания доступа',
			width: 180
		},{	
			text: "Дата создания",
			dataIndex: 'DateCreate',
			xtype: 'datecolumn',
			format : "Y-m-d",
			sortable: true,
			tooltip: 'Дата создания',
			width: 120
		},{	
			text: "Участки",
			width: 250,
			dataIndex: 'sectors',
			tooltip: 'Перечень участков, к которым пользователь имеет доступ',
			renderer: this.sectorRenderer
		},{	
			text: "Сети",
			width: 250,
			dataIndex: 'nets',
			tooltip: 'Перечень сетей, к которым пользователь имеет доступ',
			renderer: this.netRenderer
		},{	
			text: "Категории",
			width: 250,
			dataIndex: 'categories',
			tooltip: 'Перечень категорий, к которым пользователь имеет доступ',
			renderer: this.categoryRenderer
		},{	
			text: "Производители",
			width: 250,
			dataIndex: 'manufacturers',
			tooltip: 'Перечень производителей, к которым пользователь имеет доступ',
			renderer: this.manufacturerRenderer
		},{	
			text: "Комментарии",
			dataIndex: 'Comment',
			sortable: true,
			width: 180
		}];
		
		var i = 0,
			columns = this.columns,
			length = columns.length;

	    for (;i<length;i++) {
			columns[i].items = [{
		        xtype: 'container',
		        margin: 4,
		        flex: 1,
		        layout: 'fit',
		        listeners: {
					scope: this,
					element: 'el',
					mousedown: function(e) {
					  e.stopPropagation();
					},
					click: function(e) {
					  e.stopPropagation();
					},
					keydown: function(e) {
					   e.stopPropagation();
					},
					keypress: function(e) {
					   e.stopPropagation();
					},
					keyup: function(e) {
					   e.stopPropagation();
					}
		        },
		        items:[{
					xtype: 'textfield',
					emptyText : 'Поиск',
					enableKeyEvents: true,
					onTriggerClick: function () {
						this.reset();
						this.focus();
					},
					listeners: {
						change: function(field, newValue, oldValue, eOpts) {
							me.fireEvent('searchchange', me, field, newValue, oldValue);
						},
						buffer: 1000
					}
		        }]
			}];
	    }

		this.bbar = Ext.create('Ext.toolbar.Paging', {
			store: this.store,
			displayInfo: true,
			height: 40,
			maxHeight: 40,
			padding: '5 5 5 5',
			displayMsg: 'Показано {0}-{1} из {2} строк',
			emptyMsg: "Нет результатов",
			items : [
				Ext.create('Ext.Button', {
					iconCls: 'x-tbar-addbut',
					text: '<span style=" font-size: 12px;">Создать</span>',
					handler: function() {
						this.up('usersGrid').fireEvent('createuserbuttonclick');
					}
				}),
				'-',
				Ext.create('Ext.Button', {
					iconCls: 'x-tbar-editbut',
					text: '<span style=" font-size: 12px;">Редактировать</span>',
					handler: function() {
						this.up('usersGrid').fireEvent('edituserbuttonclick');
					}
				}),
				'-',
				Ext.create('Ext.Button', {
					iconCls: 'x-tbar-delbut',
					text: '<span style=" font-size: 12px;">Удалить</span>',
					handler: function() {
						this.up('usersGrid').fireEvent('deleteuserbuttonclick');
					}
				})
			]
		});
	
		this.callParent(arguments);
	}
});

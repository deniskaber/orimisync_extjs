Ext.define('OrimiAdmin.view.Viewport', {
    extend: 'Ext.container.Viewport',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border'
    ],

    layout: 'border',

    items: [{
        region: 'north',
        xtype: 'appHeader'
    },/*{
        region: 'west',
        id: 'west-region',
		xtype: 'filters'
    }*/,{
        region: 'center',
        xtype: 'contentPanel'
    }]
});

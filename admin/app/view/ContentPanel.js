Ext.define('OrimiAdmin.view.ContentPanel', {
    extend: 'Ext.tab.Panel',
    xtype: 'contentPanel',
    id: 'content-panel',
	plain: true,
    defaults: {
        autoScroll: true
    },
	
	items: [{
		title: 'Пользователи',
		xtype: 'usersGrid',
		tooltip: 'Управление пользователями'
	},{
		title: 'Роли',
		xtype: 'rolesGrid',
		tooltip: 'Управление ролями'
	}]
});

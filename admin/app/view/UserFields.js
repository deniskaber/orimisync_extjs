Ext.define('OrimiAdmin.view.UserFields', {
    extend: 'Ext.form.Panel',
    xtype: 'userFields',
	requires: ['Ext.form.field.Text'],
	userData: false,
	dis: false,
	width: 460,
	border: false,
	bodyPadding: '10 0 0 0',
	useArrows: false,
	layout: 'auto',	
	required: '<span style="color:red;font-weight:bold" data-qtip="Необходимое поле">*</span>',
	defaultType: 'textfield',
	fieldDefaults: {
		msgTarget: 'side'
	},
	items: [{
		xtype       :'fieldset',
		columnWidth : 1,
		defaults    : {
			xtype: 'textfield'
			,anchor: '100%'
			,labelWidth  : 160
		},
		border      : false,
		items: [{
			fieldLabel: 'Логин',
			afterLabelTextTpl: this.required,
			name: 'Name',
			allowBlank: false
		},{
			xtype: 'combobox',
			//margin: '2 0 10 5',
			queryMode: 'local',
			displayField: 'Name',
			fieldLabel: 'Роль',
			forceSelection: true,
			valueField: 'ID',
			editable: false,
			name: 'RoleID',
			store: 'StoreRoles'
		},{
			fieldLabel: 'Полное имя',
			afterLabelTextTpl: this.required,
			allowBlank: false,
			name: 'FullName'
		},{
			fieldLabel: 'Заблокирован',
			name: 'isDeleted',
			xtype: 'checkbox',
			inputValue: 1
		},{
			fieldLabel: 'E-mail',
			name: 'EMail',
			vtype:'email'
		},{
			fieldLabel: 'Телефон',
			name: 'Tel1'
		},{
			fieldLabel: 'Дата начала доступа',
			afterLabelTextTpl: this.required,
			name: 'DateBeginAccess',
			xtype: 'datefield',
			format: 'Y-m-d',
			allowBlank:false
		},{
			fieldLabel: 'Дата конца доступа',
			afterLabelTextTpl: this.required,
			name: 'DateEndAccess',
			xtype: 'datefield',
			format: 'Y-m-d',
			allowBlank:false
		},{
			fieldLabel: 'Пароль',
			afterLabelTextTpl: this.required,
			name: 'Password',
			allowBlank: false
		},{
			xtype: 'textarea',
			fieldLabel: 'Комментарии',
			name: 'Comment'
		}]
	}],
	initComponent: function() {
		this.callParent(arguments);

		if (this.userData){
			this.getForm().setValues(this.userData);
			this.down('[name=Name]').readOnly = true;
			this.down('[name=Name]').addCls('noteditable');
		} else
			this.down('[name=DateBeginAccess]').setValue(new Date());
	}
});
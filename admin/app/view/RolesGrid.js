Ext.define('OrimiAdmin.view.RolesGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'rolesGrid',
	
	selType: 'rowmodel',
	border: false,
	disableSelection: false,
	flex: 1,	
	viewConfig: {
		shrinkWrap: 0,
		shadow: false,
		trackOver: false,
		overItemCls: false
	},
	autoscroll: true,
	
	rightsRenderer: function (val) {
		if (val==1) {
			return '<span style="color:green; text-align:right; font-weight:bold">+</span>';
		} else 
			return '<span style="color:red; text-align:right; font-weight:bold">-</span>';
	},
	
	initComponent: function() {
		var me = this;

		this.store = 'StoreRoles';
		this.columns = [{	
			text: "ID",
			dataIndex: 'ID',
			sortable: true,
			width: 60
		},{	
			text: "Название",
			dataIndex: 'Name',
			sortable: true,
			width: 200
		},{	
			text: "Дата обновления",
			dataIndex: 'DateUpdate',
			xtype: 'datecolumn',
			format : "Y-m-d",
			sortable: true,
			width: 140
		}];
		
		this.bbar = Ext.create('Ext.toolbar.Paging', {
			store: this.store,
			displayInfo: true,
			height: 40,
			maxHeight: 40,
			padding: '5 5 5 5',
			displayMsg: 'Показано {0}-{1} из {2} строк',
			emptyMsg: "Нет результатов",
			items : [
				Ext.create('Ext.Button', {
					iconCls: 'x-tbar-addbut',
					text: '<span style=" font-size: 12px;">Создать</span>',
					handler: function() {
						this.up('rolesGrid').fireEvent('createrolebuttonclick');
					}
				}),
				'-',
				Ext.create('Ext.Button', {
					iconCls: 'x-tbar-editbut',
					text: '<span style=" font-size: 12px;">Редактировать</span>',
					handler: function() {
						this.up('rolesGrid').fireEvent('editrolebuttonclick');
					}
				}),
				'-',
				Ext.create('Ext.Button', {
					iconCls: 'x-tbar-delbut',
					text: '<span style=" font-size: 12px;">Удалить</span>',
					handler: function() {
						this.up('rolesGrid').fireEvent('deleterolebuttonclick');
					}
				})
			]
		});
	
		this.callParent(arguments);
	}
});

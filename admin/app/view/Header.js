Ext.define('OrimiAdmin.view.Header', {
    extend: 'Ext.Container',
    xtype: 'appHeader',
    id: 'app-header',
    height: 52,
    layout: {
        type: 'hbox',
        align: 'middle'
    },
	items: [
	{
		xtype: 'container',
		margin: '0 0 0 10',
		layout: {type: 'vbox'},
		defaults: {
			xtype: 'text',
			cls: 'header-text',
			shadow: 'sides',
			shadowOffset: 2,
			fill: '#fff'
		},
		items: [{
		//	text: $('#db_log').text()
		},{
		//	text: $('#db_sub').text()
		}]
	},{
		xtype: 'container',
		id: 'logo',
		flex: 1,
		layout: {type: 'hbox', pack: 'end'},
		defaults: {
			xtype: 'button',
			width: 95,
			scale: 'medium',
			margin: '0 10 0 0'
		},
		items: [{
			//glyph: 115,
			text: 'Выход',
			handler: function() {
				//window.location.href='../dev_alvol2/index.php?logout=logout'; 
				window.location.href='../index.php?logout=logout';
			}
		}]
	}],
	
	initComponent: function() {
	//	this.user = $('#db_log').text();
	//	this.subscription = $('#db_sub').text();
		
		this.callParent(arguments);
	}
});

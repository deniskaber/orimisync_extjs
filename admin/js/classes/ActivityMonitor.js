Ext.define('Ext.app.ActivityMonitor', {
    singleton   : true,

    ui          : null,
    runner      : null,
    task        : null,
    lastActive  : null,
    
    ready       : false,
    verbose     : false,
    interval    : (1000 * 60 * 1), //1 minute
    maxInactive : (1000 * 60 * 30), //30 minutes
    
    init : function(config) {
        if (!config) { config = {}; }
        
        Ext.apply(this, config, {
            runner     : new Ext.util.TaskRunner(),
            ui         : Ext.getBody(),
            task       : {
                run      : this.monitorUI,
                interval : config.interval || this.interval,
                scope    : this
            }
        });
        
        this.ready = true;
    },
    
    isReady : function() {
        return this.ready;
    },
    
    isActive   : Ext.emptyFn,
    isInactive : function() {
		Ext.Ajax.request({
			url: 'config.php',
			timeout: 600000,
			method: 'GET',
			params: {
				logout: 'auto'/*,
				SID: '<?php echo session_id();?>'*/
			}
		});
		Ext.Msg.show({
			title:'CSV',
			msg:'Вы были неактивны 30 минут. Сессия автоматически закончилась',
			closable: false,
			buttons: Ext.Msg.OK,
			fn : function(btn){
				if (btn == 'ok'){
					window.location.href='index.php';
				}
			}
		});
	},
    start : function() {
        if (!this.isReady()) {
            this.log('Please run ActivityMonitor.init()');
            return false;
        }
        
        this.ui.on('mousemove', this.captureActivity, this);
        this.ui.on('keydown', this.captureActivity, this);
        
        this.lastActive = new Date();
        this.log('ActivityMonitor has been started.');
        
        this.runner.start(this.task);
    },
    
    stop : function() {
        if (!this.isReady()) {
            this.log('Please run ActivityMonitor.init()');
            return false;
        }
        
        this.runner.stop(this.task);
        this.lastActive = null;
        
        this.ui.un('mousemove', this.captureActivity);
        this.ui.un('keydown', this.captureActivity);
        
        this.log('ActivityMonitor has been stopped.');
    },
    
    captureActivity : function(eventObj, el, eventOptions) {
        this.lastActive = new Date();
    },
    
    monitorUI : function() {
        var now      = new Date(),
            inactive = (now - this.lastActive);
        
        if (inactive >= this.maxInactive) {
            this.log('MAXIMUM INACTIVE TIME HAS BEEN REACHED');
            this.stop(); //remove event listeners
            
            this.isInactive();
        }
        /*else {
            this.log('CURRENTLY INACTIVE FOR ' + inactive + ' (ms)');
            this.isActive();
        }*/
    },
    
    log : function(msg) {
        if (this.verbose) {
            window.console.log(msg);
        }
    }
    
});
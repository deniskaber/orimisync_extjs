﻿Ext.define('Ext.app.PortalItemSelector', {
    extend: 'Ext.ux.form.MultiSelect',
    alias: ['widget.PortalItemSelector', 'widget.PortalItemSelector'],
    requires: [
        'Ext.button.Button',
        'Ext.ux.form.MultiSelect'
    ],

    /**
     * @cfg {Boolean} [hideNavIcons=false] True to hide the navigation icons
     */
    hideNavIcons:false,

    /**
     * @cfg {Array} buttons Defines the set of buttons that should be displayed in between the ItemSelector
     * fields. Defaults to <tt>['top', 'up', 'add', 'remove', 'down', 'bottom']</tt>. These names are used
     * to build the button CSS class names, and to look up the button text labels in {@link #buttonsText}.
     * This can be overridden with a custom Array to change which buttons are displayed or their order.
     */
    buttons: ['addall', 'add', 'remove', 'removeall'],

    /**
     * @cfg {Object} buttonsText The tooltips for the {@link #buttons}.
     * Labels for buttons.
     */
    buttonsText: {
        addall: "Добавить все",
		add: "Добавить выделенные",
		remove: "Убрать выделенные",
		removeall: "Убрать все"
    },

    initComponent: function() {
        var me = this;

        me.ddGroup = me.id + '-dd';
        me.callParent();
		me.bindStore(me.store);
    },

    createFrom: function(){
        var me = this;

        return Ext.create('Ext.ux.form.MultiSelect', {
            submitValue: false,
            flex: 1,
            dragGroup: me.ddGroup,
            dropGroup: me.ddGroup,
            store: {
                model: me.store.model,
                data: [],
				remotefilter: false
            },
            displayField: me.displayField,
            disabled: me.disabled,
            listeners: {
                boundList: {
                    scope: me,
                    itemdblclick: me.onItemDblClick,
                    drop: me.syncValue
                }
            }
        });
    },
	
	createTo: function(){
        var me = this;
		return Ext.create('Ext.ux.form.MultiSelect', {
            submitValue: false,
            flex: 1,
            dragGroup: me.ddGroup,
            dropGroup: me.ddGroup,
            store: {
                model: me.store.model,
                data: [],
				remotefilter: false
            },
            displayField: me.displayField,
            disabled: me.disabled,
            listeners: {
                boundList: {
                    scope: me,
                    itemdblclick: me.onItemDblClick,
                    drop: me.syncValue,
					change : function() {
						alert(me.store.count());
						tpl.overwrite(me.detailField.body, me.store.count());
					}
                }
            }
        });
    },
	
	createTextField: function(){
        var me = this;
		return Ext.create('Ext.form.field.Text', {
			name: 'textField',
			flex: 1,
			emptyText : 'Поиск',
			listeners: {
				change : function() {   
					if (!this.getRawValue())
					{
						me.fromField.store.removeAll();
						me.getStore().clearFilter(true);
						Ext.Array.forEach(me.getStore().getRange(), function(item) {
							if ((me.toField.store.find('text',item.get('text'))) < 0 )
							{
								me.fromField.store.addSorted(item);
							}
						});
						return;
					}
					me.fromField.store.removeAll();
					me.getStore().clearFilter(true);
					var val = this.getRawValue().toLowerCase();
					me.getStore().filter([
						Ext.create('Ext.util.Filter', {filterFn: function(item) {
							return item.get('text').toLowerCase().indexOf(val) + 1;
						}})
					]);                        
					Ext.Array.forEach(me.getStore().getRange(), function(item) {
					if ((me.toField.store.find('text',item.get('text'))) < 0 )
					{
						me.fromField.store.addSorted(item);
					}
					});
				},
				blur : function() {
					if (!this.getRawValue())
					{
						me.fromField.store.removeAll();
						me.getStore().clearFilter(true);
						Ext.Array.forEach(me.getStore().getRange(), function(item) {
							if ((me.toField.store.find('text',item.get('text'))) < 0 )
							{
								me.fromField.store.addSorted(item);
							}
						});
						return;
					} 
					if (me.getStore().count() == 1)
					{
						me.setValue(me.getStore().data.items[0].data.value)
					} 
				}   
			}
		});
	},
	
	setupItems: function() {
        var me = this;

        me.fromField = me.createFrom();
        me.toField = me.createTo();
		me.textField = me.createTextField();
		
		return {
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
			border: false,
            items: [
				me.textField,
				{
					xtype: 'container',
					height: 120,
					maxheight: 120,
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					items: [
						me.fromField,
						{
							xtype: 'container',
							margins: '0 4',
							layout: {
								type: 'vbox',
								pack: 'start'
							},
							items: me.createButtons()
						},
						me.toField
					]
				}
			]
        };
    },

    createButtons: function(){
        var me = this,
            buttons = [];

        if (!me.hideNavIcons) {
            Ext.Array.forEach(me.buttons, function(name) {
                buttons.push({
                    xtype: 'button',
                    tooltip: me.buttonsText[name],
                    handler: me['on' + Ext.String.capitalize(name) + 'BtnClick'],
                    cls: Ext.baseCSSPrefix + 'form-itemselector-btn',
                    iconCls: Ext.baseCSSPrefix + 'form-itemselector-' + name,
                    navBtn: true,
					scope: me,
                    width: 30,
					minWidth: 30,
					height: 30,
					minHeight: 30//,
					//margin: '4 0 0 0'
                });
            });
        }
        return buttons;
    },

    getSelections: function(list){
        var store = list.getStore(),
            selections = list.getSelectionModel().getSelection();

        return Ext.Array.sort(selections, function(a, b){
            a = store.indexOf(a);
            b = store.indexOf(b);

            if (a < b) {
                return -1;
            } else if (a > b) {
                return 1;
            }
            return 0;
        });
    },
	
	onAddallBtnClick : function() {
        var me = this,
            fromList = me.fromField.boundList;
            
		
		fromList.getSelectionModel().selectAll();
		selected = this.getSelections(fromList);
        fromList.getStore().remove(selected);
        this.toField.boundList.getStore().add(selected);
        this.syncValue();
    },
	
	onAddBtnClick : function() {
        var me = this,
            fromList = me.fromField.boundList,
            selected = this.getSelections(fromList);

        fromList.getStore().remove(selected);
        this.toField.boundList.getStore().add(selected);
        this.syncValue();
    },
	
	onRemoveBtnClick : function() {
        var me = this,
            toList = me.toField.boundList,
            selected = this.getSelections(toList);

        toList.getStore().remove(selected);
        this.fromField.boundList.getStore().add(selected);
        this.syncValue();
    },
	
	onRemoveallBtnClick : function() {
        var me = this,
            toList = me.toField.boundList;
            
		
		toList.getSelectionModel().selectAll();
		selected = this.getSelections(toList);
        toList.getStore().remove(selected);
        this.fromField.boundList.getStore().add(selected);
        this.syncValue();
    },

    syncValue: function() {
		var me = this;
        this.setValue(this.toField.store.getRange());
	},

    onItemDblClick: function(view, rec){
        var me = this,
            from = me.fromField.store,
            to = me.toField.store,
            current,
            destination;

        if (view === me.fromField.boundList) {
            current = from;
            destination = to;
        } else {
            current = to;
            destination = from;
        }
        current.remove(rec);
        destination.addSorted(rec);
        me.syncValue();
    },

    setValue: function(value){
        var me = this,
            fromStore = me.fromField.store,
            toStore = me.toField.store,
            selected;

        // Wait for from store to be loaded
        //if (!me.fromField.store.getCount()) {
         //   me.fromField.store.on({
         //       load: Ext.Function.bind(me.setValue, me, [value]),
         //       single: true
        //    });
        //    return;
       // }

        value = me.setupValue(value);
        me.mixins.field.setValue.call(me, value);

        //selected = me.getRecordsForValue(value);

        //Ext.Array.forEach(toStore.getRange(), function(rec){
            //if (!Ext.Array.contains(selected, rec)) {
                // not in the selected group, remove it from the toStore
                //toStore.remove(rec);
                //fromStore.add(rec);
           // }
        //});
        //toStore.removeAll();
	

        //Ext.Array.forEach(selected, function(rec){
            // In the from store, move it over
            //if (fromStore.indexOf(rec) > -1) {
                //fromStore.remove(rec);     
            //}
            //toStore.add(rec);
        //});
	
	fromStore.sort('text','asc');
	toStore.sort('text','asc');
    },

    onBindStore: function(store, initial) {
        var me = this;

        if (me.fromField) {
            me.fromField.store.removeAll()
            me.toField.store.removeAll();

            // Add everything to the from field as soon as the Store is loaded
            if (store.getCount()) {
                me.populateFromStore(store);
            } else {
                me.store.on('load', me.populateFromStore, me);
            }
        }
    },

    populateFromStore: function(store) {
        this.fromField.store.add(store.getRange());
        
        // setValue wait for the from Store to be loaded
        this.fromField.store.fireEvent('load', this.fromField.store);
    },

    onEnable: function(){
        var me = this;

        me.callParent();
        me.fromField.enable();
        me.toField.enable();

        Ext.Array.forEach(me.query('[navBtn]'), function(btn){
            btn.enable();
        });
    },

    onDisable: function(){
        var me = this;

        me.callParent();
        me.fromField.disable();
        me.toField.disable();

        Ext.Array.forEach(me.query('[navBtn]'), function(btn){
            btn.disable();
        });
    },

    onDestroy: function(){
        this.bindStore(null);
        this.callParent();
    }
});
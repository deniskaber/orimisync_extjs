delete dbo.lasmart_t_dim_SubChannel
insert into dbo.lasmart_t_dim_SubChannel
(BriefNotation, Name)

select 0,	'Киоск, рыночная точка'
union all select 1,	'Узкий дискаунтер'
union all select 2,	'Минимаркет, павильон, дискаунтер с чайно-кофейным ассортиментом (до 100 чайных поз. без учета фасовок)'
union all select 3,	'Магазин, гастроном, дискаунтер (более 100 чайных поз. без учета фасовок)'
union all select 4,	'Супермаркет, универсам, кэш'
union all select 5,	'Гипермаркет, кэш'
union all select 6,	'ОПТ'
union all select 7,	'Хорека'
union all select 8,	'РЦ'
union all select 9,	'Прочее'
union all select 99,	'Канал не определен'

delete dbo.lasmart_t_dim_Sector
insert into dbo.lasmart_t_dim_Sector
(Name)

select [Участок]
FROM [dbo].[testtt] 
group by [Участок]

delete dbo.lasmart_t_dim_Net
insert into dbo.lasmart_t_dim_Net
(Name)

select [Название сети]
FROM [dbo].[testtt] 
where [Название сети] is not null and [Название сети] <> 'NULL'
group by [Название сети]

  insert into dbo.lasmart_t_dim_MasterStores
  ([City]
  ,[TempAddress]
  ,[SubChannelID]
  ,[isNet]
  ,[NetID]
  ,[Name]
  ,[Region]
  ,[County]
  ,[SectorID]
  ,[isInRBP])
  
  select 
  [Город] as [City]
  ,[Черновой адрес] as [TempAddress]
  ,sc.ID as [SubChannelID]
  ,[Сеть] as [isNet]
  ,case when [Сеть] = 1 then n.ID else null end as [NetID]
  ,[Название торговой точки] as [Name]
  ,[Область] as [Region]
  ,[Федеральный округ] as [County]
  ,s.ID as [SectorID]
  ,[Участвует в РБП] as [isInRBP]
  FROM [dbo].[testtt] tt
  left join lasmart_t_dim_SubChannel sc on  tt.[Подканал] = sc.BriefNotation
  left join lasmart_t_dim_Sector s on tt.[Участок] = s.Name
  left join lasmart_t_dim_Net n on tt.[Название сети] = n.Name
  
  
  insert into dbo.lasmart_t_dim_SlaveStores
  ([City]
  ,[TempAddress]
  ,[Channel]
  ,[isNet]
  ,[Net]
  ,[Name]
  ,[Region]
  ,[County]
  ,[Sector]
  --,[isInRBP]
  ,[DataSource]
  )
  
  select 
  [Город] as [City]
  ,[Черновой адрес] as [TempAddress]
  ,[Участок] as [Channel]
  ,[Сеть] as [isNet]
  ,case when [Сеть] = 1 then [Название сети] else null end as [Net]
  ,[Название торговой точки] as [Name]
  ,[Область] as [Region]
  ,[Федеральный округ] as [County]
  ,[Участок] as [Sector]
  --,[Участвует в РБП] as [isInRBP]
  ,'Дистрибьюторы' as [DataSource]
  FROM [dbo].[testtt] tt
  
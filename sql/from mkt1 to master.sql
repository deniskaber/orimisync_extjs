  delete lasmart_t_dim_Manufacturer
  insert into lasmart_t_dim_Manufacturer
  ([Name])
  
  select [производитель]
  FROM [192.168.0.205].[Mkt1].[dbo].[Products]
  group by [производитель]
  order by [производитель]
  
  delete lasmart_t_dim_Brand
  insert into lasmart_t_dim_Brand
  ([Name],[ManufacturerID])
  
  
  select [бренд], (select top 1 m.ID from [dbo].lasmart_t_dim_Manufacturer m where m.name = (select top 1 [производитель] from [192.168.0.205].[Mkt1].[dbo].[Products] p2 where p2.[бренд] = p.[бренд]))
  FROM [192.168.0.205].[Mkt1].[dbo].[Products] p
  group by [бренд]
  order by [бренд]
  
  
  delete lasmart_t_dim_Category
  insert into lasmart_t_dim_Category
  ([Name])
  
  select [категория]
  FROM [192.168.0.205].[Mkt1].[dbo].[Products]
  group by [категория]
  order by [категория]
  
  delete lasmart_t_dim_Segment
  insert into lasmart_t_dim_Segment
  ([Name],[CategoryID])
  
  select [сабкатегория],c.ID
  FROM [192.168.0.205].[Mkt1].[dbo].[Products] p
  left join lasmart_t_dim_Category c on p.[категория] = c.name
  group by [сабкатегория],c.ID
  order by [сабкатегория],c.ID
  
  insert into lasmart_t_dim_TeaColor
  ([Name])
  
  select [вид]
  FROM [192.168.0.205].[Mkt1].[dbo].[Products]
  group by [вид]
  order by [вид]
  
  insert into lasmart_t_dim_InternalPack
  ([Name])
  
  select [упаковка]
  FROM [192.168.0.205].[Mkt1].[dbo].[Products]
  group by [упаковка]
  order by [упаковка]
  
  insert into lasmart_t_dim_ExternalPack
  ([Name])
  
  select [упаковка]
  FROM [192.168.0.205].[Mkt1].[dbo].[Products]
  group by [упаковка]
  order by [упаковка]
  
  insert into lasmart_t_dim_PriceSegment
  ([Name])
  
  select [ценовой сегмент]
  FROM [192.168.0.205].[Mkt1].[dbo].[Products]
  where [ценовой сегмент] is not null and [ценовой сегмент] <> '0'
  group by [ценовой сегмент]
  order by [ценовой сегмент]
  
  delete [lasmart_t_dim_MasterGood]
  insert into [lasmart_t_dim_MasterGood]
  ([code]
  ,[orimicode]
  ,[tempname]
  ,[from]
  ,[manufacturerid]
  ,[brandid]
  ,[categoryid]
  ,[segmentid]
  ,[teacolorid]
  ,[externalpackid]
  ,[internalpackid]
  ,[bagqty]
  ,[bagweight]
  ,[weight]
  ,[pricesegmentid])
  
  select 
  [код продукта] as code
  ,[код орими] as orimicode
  ,[Продукт 2] as [tempname]
  ,case when [производитель] like '%orimi%' then 1 else 0 end as [from]
  ,m.ID as [manufacturerid]
  ,b.ID as [brandid]
  ,c.ID as [categoryid]
  ,s.ID as [segmentid]
  ,t.ID as [teacolorid]
  ,e.ID as [externalpackid]
  ,i.ID as [internalpackid]
  
  ,isnull([количество пакетиков],1) as [bagqty]
  ,cast([вес] as money)/cast(isnull([количество пакетиков],1) as money) as [bagweight]
  ,[вес] as [weight]
  
  ,ps.ID as [pricesegmentid]
  
  FROM [192.168.0.205].[Mkt1].[dbo].[Products] p
  left join lasmart_t_dim_Manufacturer m on p.[производитель] = m.Name
  left join lasmart_t_dim_Brand b on p.[бренд] = b.Name
  left join lasmart_t_dim_Category c on p.[категория] = c.Name
  left join lasmart_t_dim_Segment s on p.[сабкатегория] = s.Name
  left join lasmart_t_dim_TeaColor t on p.[вид] = t.Name
  left join lasmart_t_dim_InternalPack i on p.[упаковка] = i.Name
  left join lasmart_t_dim_ExternalPack e on p.[упаковка] = e.Name
  left join lasmart_t_dim_PriceSegment ps on p.[ценовой сегмент] = ps.Name
  
  insert into dbo.lasmart_t_sync_PositionStatus
  (Name)
  
  select 'Черновик' union all 
  select 'Активная' union all 
  select 'Помечена на удаление'
  
  insert into dbo.lasmart_t_sync_SyncStatus
  (Name)
  
  select 'Вручную' union all 
  select 'Автоматически'
  
  
  
  delete [lasmart_t_dim_SlaveGood]
  insert into [lasmart_t_dim_SlaveGood]
	([Code]
	,[OrimiCode]
	,[Name]
	,[From]
	,[Manufacturer]
	,[Brand]
	,[Category]
	,[Segment]
	,[TeaColor]
	,[Pack]
	,[BagQty]
	,[BagWeight]
	,[Weight]
	,[PriceSegment]
	,[DataSource])
  
  select 
  [код продукта] as [Code]
  ,[код орими] as [OrimiCode]
  ,[Продукт 2] as [Name]
  ,case when [производитель] like '%orimi%' then 1 else 0 end as [From]
  ,[производитель] as [Manufacturer]
  ,[бренд] as [Brand]
  ,[категория] as [Category]
  ,[сабкатегория] as [Segment]
  ,[вид] as [TeaColor]
  ,[упаковка] as [Pack]  
  ,isnull([количество пакетиков],1) as [BagQty]
  ,cast([вес] as money)/cast(isnull([количество пакетиков],1) as money) as [BagWeight]
  ,[вес] as [Weight]
  
  ,[ценовой сегмент] as [PriceSegment]
  
  ,'Сети' as [DataSource]
  FROM [192.168.0.205].[Mkt1].[dbo].[Products]
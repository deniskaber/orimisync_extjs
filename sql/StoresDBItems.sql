USE [Orimi_denis]
GO
/****** Object:  Table [dbo].[sync_template]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sync_template](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_User] [int] NOT NULL,
	[Name] [varchar](64) NOT NULL,
	[DateCreate] [datetime] NOT NULL,
	[DateUsed] [datetime] NULL,
	[Ref] [varchar](16) NOT NULL,
	[State] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lasmart_t_dim_TypeStaff]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_TypeStaff](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_SlaveStores]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_SlaveStores](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DistrCode] [int] NULL,
	[Code] [varchar](32) NULL,
	[Name] [varchar](256) NULL,
	[TempAddress] [varchar](256) NULL,
	[County] [varchar](128) NULL,
	[Region] [varchar](128) NULL,
	[City] [varchar](128) NULL,
	[Division] [varchar](128) NULL,
	[Sector] [varchar](128) NULL,
	[SubChannel] [varchar](128) NULL,
	[Format] [varchar](128) NULL,
	[Net] [varchar](128) NULL,
	[isNet] [tinyint] NULL,
	[DataSource] [varchar](32) NOT NULL,
	[DateChange] [datetime] NOT NULL,
	[isSynced] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lasmart_t_dim_SlaveStaff]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_SlaveStaff](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[Code] [varchar](32) NULL,
	[SurName] [nvarchar](256) NULL,
	[Name] [nvarchar](256) NULL,
	[LastName] [nvarchar](256) NULL,
	[FIO] [nvarchar](1024) NULL,
	[BirthDate] [date] NULL,
	[TypeID] [int] NULL,
	[DivisionID] [int] NULL,
	[DateHired] [date] NULL,
	[DateFired] [date] NULL,
	[EMail] [nvarchar](256) NULL,
	[Phone] [nvarchar](128) NULL,
	[Comment] [nvarchar](1024) NULL,
	[MotivationID] [int] NULL,
	[Feature] [tinyint] NULL,
	[SectorID] [int] NULL,
	[DataSource] [varchar](32) NOT NULL,
	[DateChange] [datetime] NOT NULL,
	[isSynced] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Sector]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Sector](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_NonAccReason]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_NonAccReason](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](128) NULL,
	[ShortName] [varchar](32) NULL,
	[BriefNotation] [varchar](32) NULL,
	[Deleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lasmart_t_dim_NetType]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_NetType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Format]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Format](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[ShortName] [varchar](32) NULL,
	[BriefNotation] [varchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Division]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Division](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Country]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Country](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_ContractorGroup]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_ContractorGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[ShortName] [varchar](32) NULL,
	[BriefNotation] [varchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Channel]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Channel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](128) NULL,
	[ShortName] [varchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange]    Script Date: 07/26/2014 18:53:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[lasmart_p_UpdateMasterRefNamesOnAttributeChange]
(@Attribute varchar(64), @Value int)
as
declare
@query varchar(max)
begin
	if (@Attribute = 'AddingID')
		set @query = '
			declare
			@i int,@j int

			SELECT @i=MIN([GoodID]),@j=MAX([GoodID]) FROM [dbo].[lasmart_t_good_adding] where '+@Attribute+' = ' + convert(varchar, @Value) + '
			while @i<=@j
			begin
				exec [lasmart_p_MasterGood_OnChange] @ID = @i
			
				SELECT @i=MIN([GoodID]) FROM [dbo].[lasmart_t_good_adding] where '+@Attribute+' = ' + convert(varchar, @Value) + ' and [GoodID]>@i
			end	--while	
		'
		
	if (@Attribute = 'FlavorID')
		set @query = '
			declare
			@i int,@j int

			SELECT @i=MIN([GoodID]),@j=MAX([GoodID]) FROM [dbo].[lasmart_t_good_flavor] where '+@Attribute+' = ' + convert(varchar, @Value) + '
			while @i<=@j
			begin
				exec [lasmart_p_MasterGood_OnChange] @ID = @i
			
				SELECT @i=MIN([GoodID]) FROM [dbo].[lasmart_t_good_adding] where '+@Attribute+' = ' + convert(varchar, @Value) + ' and [GoodID]>@i
			end	--while	
		'
	if (@Attribute in ('CategoryID','BrandID','SubBrandID','SegmentID','SubSegmentID','TeaColorID','ExternalPackID','InternalPackID','SachetPackID','GiftID','GiftTypeID'))
	set @query = '
		declare
		@i int,@j int

		SELECT @i=MIN([ID]),@j=MAX([ID]) FROM [dbo].[lasmart_t_dim_MasterGood] where '+@Attribute+' = ' + convert(varchar, @Value) + '
		while @i<=@j
		begin
			exec [lasmart_p_MasterGood_OnChange] @ID = @i
		
			SELECT @i=MIN([ID]) FROM [dbo].[lasmart_t_dim_MasterGood] where '+@Attribute+' = ' + convert(varchar, @Value) + ' and [ID]>@i
		end	--while	
	'
	--print @query
	exec (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_LogNonIDAttributeChange]    Script Date: 07/26/2014 18:53:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[lasmart_p_LogNonIDAttributeChange] (
	@Ref varchar(16)
	,@ElementID int
	,@UserName  varchar(128)
	,@AttributeName varchar(64)
	,@newValue varchar(1024)
	,@oldValue varchar(1024) = null
)
as
declare 
@query varchar(max) = ''
BEGIN
	if (isnull(@oldValue,'') <> isnull(@newValue,''))
	begin
		set @query = '
		insert [lasmart_t_sync_MasterRefLog]
		([Ref]
		,[ElementID]
		,[Author]
		,[Event])
		VALUES (
			'''+@Ref+''','+convert(varchar,@ElementID)+','''+@UserName+''',''Обновлено значение '+@AttributeName+' c ''+ISNULL(convert(varchar(max),'+isnull(''''+convert(varchar(max),@oldValue)+'''','null')+'),''Не задано'')+'' на ''+ISNULL(convert(varchar(max),'+isnull(''''+convert(varchar(max),@newValue)+'''','null')+'),''Не задано'')
		)'
		
		--print @query
		--insert into lasmart_t_sync_TempLog
		--([Event])
		--select @query
		exec (@query)
	end
END
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_LogIDAttributeChange]    Script Date: 07/26/2014 18:53:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[lasmart_p_LogIDAttributeChange] (
	@Ref varchar(16)
	,@ElementID int
	,@UserName  varchar(128)
	,@AttributeName varchar(64)
	,@table varchar(64)
	,@newValue int
	,@oldValue int = null
)
as
declare 
@query varchar(max) = ''
BEGIN
	if (isnull(@oldValue,0) <> isnull(@newValue,0))
	begin
		set @query = '
		insert [lasmart_t_sync_MasterRefLog]
		([Ref]
		,[ElementID]
		,[Author]
		,[Event])
		VALUES (
			'''+@Ref+''','+convert(varchar,@ElementID)+','''+@UserName+''',''Обновлено значение '+@AttributeName+' c ''+ISNULL((select top 1 Name from ['+@table+'] with(nolock) where ID = '+isnull(convert(varchar,@oldValue),'null')+'),''Не задано'')+'' на ''+ISNULL((select top 1 Name from ['+@table+'] with(nolock) where ID = '+isnull(convert(varchar,@newValue),'null')+')+'' (ID''+convert(varchar,'+isnull(convert(varchar,@newValue),'null')+')+'')'',''Не задано'')
		)'
		
		--print @query
		exec (@query)
	end
END
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_SlaveStores_GroupSync]    Script Date: 07/26/2014 18:53:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_SlaveStores_GroupSync]  (
	@UserID int
	,@ID varchar(max)
	,@MasterID int
)
AS
declare
@UserName varchar(128) = ''
,@query varchar(max) = ''
,@CurrID int
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRANSACTION
	
	select @UserName = FullName from [lasmart_t_sync_Users] where ID = @UserID

	if LEN(@ID) > 0
	BEGIN
		WHILE LEN(@ID) > 0
		BEGIN
			IF PATINDEX('%,%',@ID) > 0
			BEGIN
				SET @CurrID = SUBSTRING(@ID, 0, PATINDEX('%,%',@ID))
				SET @ID = SUBSTRING(@ID, PATINDEX('%,%',@ID) + 1, LEN(@ID))
				SET @query = '
						insert [lasmart_t_sync_MasterStoresSlave]
						([MasterStoresID]
						,[SlaveStoresID]
						,[Author])
						VALUES (
							'+convert(varchar,@MasterID)+'
							,'+convert(varchar,@CurrID)+'
							,'''+@UserName+'''
						)
								
						update [lasmart_t_dim_SlaveStores]
						set isSynced = 1
						where ID = '+convert(varchar,@CurrID)+'
						
						insert [lasmart_t_sync_MasterRefLog]
						([Ref]
						,[ElementID]
						,[Author]
						,[Event])
						VALUES (
							''TT''
							,'+convert(varchar,@MasterID)+'
							,'''+@UserName+'''
							,''К позиции привязана позиция подчиненного справочника ''+ISNULL((select top 1 Name from lasmart_t_dim_SlaveStores with (nolock) where [ID] = '+convert(varchar,@CurrID)+'),''Не задано'')+'' (ID ''+ISNULL(CONVERT(varchar,'+convert(varchar,@CurrID)+'),''Не задано'')+'')''
						)
					'
				exec sp_sqlexec @query
			END
			ELSE
			BEGIN
				SET @CurrID = @ID
				SET @query = '
						insert [lasmart_t_sync_MasterStoresSlave]
						([MasterStoresID]
						,[SlaveStoresID]
						,[Author])
						VALUES (
							'+convert(varchar,@MasterID)+'
							,'+convert(varchar,@CurrID)+'
							,'''+@UserName+'''
						)
								
						update [lasmart_t_dim_SlaveStores]
						set isSynced = 1
						where ID = '+convert(varchar,@CurrID)+'
						
						insert [lasmart_t_sync_MasterRefLog]
						([Ref]
						,[ElementID]
						,[Author]
						,[Event])
						VALUES (
							''TT''
							,'+convert(varchar,@MasterID)+'
							,'''+@UserName+'''
							,''К позиции привязана позиция подчиненного справочника ''+ISNULL((select top 1 Name from lasmart_t_dim_SlaveStores with (nolock) where [ID] = '+convert(varchar,@CurrID)+'),''Не задано'')+'' (ID ''+ISNULL(CONVERT(varchar,'+convert(varchar,@CurrID)+'),''Не задано'')+'')''
						)
					'
				exec sp_sqlexec @query
				BREAK
			END
		END
	END
	
	COMMIT TRANSACTION
END
GO
/****** Object:  Table [dbo].[lasmart_t_dim_MasterStaff]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_MasterStaff](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[Code] [varchar](32) NULL,
	[SurName] [nvarchar](256) NULL,
	[Name] [nvarchar](256) NULL,
	[LastName] [nvarchar](256) NULL,
	[FIO] [nvarchar](1024) NULL,
	[BirthDate] [date] NULL,
	[TypeID] [int] NULL,
	[DivisionID] [int] NULL,
	[DateHired] [date] NULL,
	[DateFired] [date] NULL,
	[EMail] [nvarchar](256) NULL,
	[Phone] [nvarchar](128) NULL,
	[Comment] [nvarchar](1024) NULL,
	[MotivationID] [int] NULL,
	[Feature] [tinyint] NULL,
	[SectorID] [int] NULL,
	[StatusID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lasmart_t_dim_MasterDistributors]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[lasmart_t_dim_MasterDistributors](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](32) NULL,
	[Name] [varchar](256) NULL,
	[TempAddress] [varchar](1024) NULL,
	[PostCode] [int] NULL,
	[County] [varchar](128) NULL,
	[Region] [varchar](128) NULL,
	[District] [varchar](128) NULL,
	[City] [varchar](128) NULL,
	[Street] [varchar](128) NULL,
	[Building] [varchar](64) NULL,
	[FullAddress] [varchar](1024) NULL,
	[Address] [varchar](1024) NULL,
	[SectorID] [int] NULL,
	[ContractorGroupID] [int] NULL,
	[isSubDistributor] [tinyint] NULL,
	[DateOpen] [date] NULL,
	[DateClose] [date] NULL,
	[Comment] [varchar](1024) NULL,
	[CountryID] [int] NULL,
	[isAccounting] [tinyint] NULL,
	[NonAccReasonID] [int] NULL,
	[StatusID] [int] NULL,
	[isNet] [tinyint] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lasmart_t_dim_SubChannel]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_SubChannel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ChannelID] [int] NULL,
	[Name] [varchar](128) NOT NULL,
	[ShortName] [varchar](32) NULL,
	[BriefNotation] [varchar](32) NULL,
	[Deleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[lasmart_v_sync_MasterStoreRefLog_LastEvents]    Script Date: 07/26/2014 18:50:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_sync_MasterStoreRefLog_LastEvents]
as
select [ElementID] as [StoreID]
      ,[Author]
      ,[DateUpdate]
from [Orimi_denis].[dbo].[lasmart_t_sync_MasterRefLog] with (nolock)
where Ref = 'TT' and ID in
	(SELECT max([ID])
	FROM [Orimi_denis].[dbo].[lasmart_t_sync_MasterRefLog] with (nolock)
	where Ref = 'TT' and Author <> 'Система'
	group by [ElementID])
GO
/****** Object:  View [dbo].[lasmart_v_sync_MasterStaffRefLog_LastEvents]    Script Date: 07/26/2014 18:50:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_sync_MasterStaffRefLog_LastEvents]
as
select [ElementID] as [StaffID]
      ,[Author]
      ,[DateUpdate]
from [Orimi_denis].[dbo].[lasmart_t_sync_MasterRefLog] with (nolock)
where Ref = 'Staff' and ID in
	(SELECT max([ID])
	FROM [Orimi_denis].[dbo].[lasmart_t_sync_MasterRefLog] with (nolock)
	where Ref = 'Staff' and Author <> 'Система'
	group by [ElementID])
GO
/****** Object:  View [dbo].[lasmart_v_sync_MasterDistrRefLog_LastEvents]    Script Date: 07/26/2014 18:50:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_sync_MasterDistrRefLog_LastEvents]
as
select [ElementID] as [DistrID]
      ,[Author]
      ,[DateUpdate]
from [Orimi_denis].[dbo].[lasmart_t_sync_MasterRefLog] with (nolock)
where Ref = 'Distr' and ID in
	(SELECT max([ID])
	FROM [Orimi_denis].[dbo].[lasmart_t_sync_MasterRefLog] with (nolock)
	where Ref = 'Distr' and Author <> 'Система'
	group by [ElementID])
GO
/****** Object:  View [dbo].[lasmart_v_dim_SlaveStores]    Script Date: 07/26/2014 18:50:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_dim_SlaveStores]
as
SELECT [ID]
      ,[DistrCode]
      ,[Code]
      ,[Name]
      ,[TempAddress]
      ,[County]
      ,[Region]
      ,[City]
      ,[Division]
      ,[Sector]
      ,[SubChannel]
      ,[Format]
      ,[Net]
      ,[isNet]
      ,[DataSource]
      ,convert(varchar,cast([DateChange] as date)) + ' ' + left(convert(varchar,cast([DateChange] as time)),5) as [DateChange]
  FROM [Orimi_denis].[dbo].[lasmart_t_dim_SlaveStores]
  where isSynced = 0
GO
/****** Object:  View [dbo].[lasmart_v_dim_SlaveStaff]    Script Date: 07/26/2014 18:50:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_dim_SlaveStaff]
as
SELECT [ID]
      --,[ParentID]
      ,[Code]
      --,[SurName]
      --,[Name]
      --,[LastName]
      ,[FIO]
      ,[BirthDate]
      ,[TypeID]
      ,[DivisionID]
      ,[DateHired]
      ,[DateFired]
      ,[EMail]
      ,[Phone]
      ,[Comment]
      ,[MotivationID]
      ,[Feature]
      ,[SectorID]
      ,[DataSource]
      ,convert(varchar,cast([DateChange] as date)) + ' ' + left(convert(varchar,cast([DateChange] as time)),5) as [DateChange]
  FROM [Orimi_denis].[dbo].[lasmart_t_dim_SlaveStaff]
  where isSynced = 0
GO
/****** Object:  View [dbo].[lasmart_v_dim_ParentStaff]    Script Date: 07/26/2014 18:50:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_dim_ParentStaff]
as
SELECT 
	sf.[ID]
	,sf.[Code]
	,sf.[SurName]
	,sf.[Name]
	,sf.[LastName]
	,sf.[FIO]
	,sf.[BirthDate]
	,typ.Name as [Type]
	,div.Name as [Division]
	,sf.[DateHired]
	,sf.[DateFired]
	,sf.[EMail]
	,sf.[Phone]
	,sf.[Comment]
	,mot.Name as [Motivation]
	,sf.[Feature]
	,sec.Name as [Sector]

FROM [lasmart_t_dim_MasterStaff] sf
  left join lasmart_t_dim_TypeStaff typ on sf.TypeID = typ.ID
  left join lasmart_t_dim_Division div on sf.DivisionID = div.ID
  left join lasmart_t_dim_Motivation mot on sf.MotivationID = mot.ID
  left join lasmart_t_dim_Sector sec on sf.SectorID = sec.ID
  
where sf.ParentID is null and sf.StatusID = 2
GO
/****** Object:  Table [dbo].[lasmart_t_sync_MasterStaffSlave]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_sync_MasterStaffSlave](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MasterStaffID] [int] NOT NULL,
	[SlaveStaffID] [int] NOT NULL,
	[Author] [nvarchar](128) NOT NULL,
	[DateUpdate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[lasmart_v_dim_MasterStaff]    Script Date: 07/26/2014 18:50:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_dim_MasterStaff]
as
SELECT 
	sf.[ID]
	,sf.[ParentID]
		,par.Name as [Parent]
	,sf.[Code]
	,sf.[SurName]
	,sf.[Name]
	,sf.[LastName]
	,sf.[FIO]
	,sf.[BirthDate]
	,sf.[TypeID]
		,typ.Name as [Type]
	,sf.[DivisionID]
		,div.Name as [Division]
	,sf.[DateHired]
	,sf.[DateFired]
	,sf.[EMail]
	,sf.[Phone]
	,sf.[Comment]
	,sf.[MotivationID]
		,mot.Name as [Motivation]
	,sf.[Feature]
	,sf.[SectorID]
		,sec.Name as [Sector]
	,sf.[StatusID]
		,stat.Name as [Status]
	,convert(varchar,cast([DateUpdate] as date)) + ' ' + left(convert(varchar,cast([DateUpdate] as time)),5) as [DateChange]
	,rl.Author
	--,convert(varchar,cast([DateUpdate] as date)) + ' ' + left(convert(varchar,cast([DateUpdate] as time)),5) as [DateChange]
FROM [Orimi_denis].[dbo].[lasmart_t_dim_MasterStaff] sf
  left join lasmart_t_dim_Parent par on sf.ParentID = par.ID
  left join lasmart_t_dim_TypeStaff typ on sf.TypeID = typ.ID
  left join lasmart_t_dim_Division div on sf.DivisionID = div.ID
  left join lasmart_t_dim_Motivation mot on sf.MotivationID = mot.ID
  left join lasmart_t_dim_Sector sec on sf.SectorID = sec.ID
  left join lasmart_t_sync_PositionStatus stat on  sf.StatusID = stat.ID
  left join lasmart_v_sync_MasterStaffRefLog_LastEvents rl on sf.[ID] = rl.StaffID
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Net]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Net](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[NetTypeID] [int] NULL,
	[RespMerchID] [int] NULL,
	[isNielsen] [tinyint] NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_MasterStores]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[lasmart_t_dim_MasterStores](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](32) NULL,
	[Name] [varchar](256) NULL,
	[TempAddress] [varchar](1024) NULL,
	[PostCode] [int] NULL,
	[County] [varchar](128) NULL,
	[Region] [varchar](128) NULL,
	[District] [varchar](128) NULL,
	[City] [varchar](128) NULL,
	[Street] [varchar](128) NULL,
	[Building] [varchar](64) NULL,
	[FullAddress] [varchar](1024) NULL,
	[Address] [varchar](1024) NULL,
	[SectorID] [int] NULL,
	[DivisionID] [int] NULL,
	[SubChannelID] [int] NULL,
	[ChannelID] [int] NULL,
	[FormatID] [int] NULL,
	[NetID] [int] NULL,
	[isNet] [tinyint] NULL,
	[NetTypeID] [int] NULL,
	[isNielsen] [tinyint] NULL,
	[DateOpen] [date] NULL,
	[DateClose] [date] NULL,
	[ShopArea] [money] NULL,
	[Coordinates] [varchar](32) NULL,
	[GLN] [varchar](16) NULL,
	[Comment] [varchar](1024) NULL,
	[CountryID] [int] NULL,
	[FIAS] [varchar](32) NULL,
	[isInRBP] [tinyint] NULL,
	[RespMerchID] [int] NULL,
	[RespTRID] [int] NULL,
	[Selection] [int] NULL,
	[StatusID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_MasterDistributors_SetActiveStatus]    Script Date: 07/26/2014 18:53:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_MasterDistributors_SetActiveStatus]  (
	@UserID int
	,@ID int
)
AS
declare
	@error varchar(max) = ''
	,@dublID int
	,@UserName varchar(128) = ''
	,@Name varchar (256)
	,@County varchar (128)
	,@Region varchar (128)
	,@City varchar (128)
	,@SectorID int
	,@isSubDistributor tinyint
BEGIN
	select @UserName = FullName from [lasmart_t_sync_Users] where ID = @UserID
	
	select 
	@Name = Name
	,@County = County
	,@Region = Region
	,@City = City
	,@SectorID = SectorID
	,@isSubDistributor = isSubDistributor
	from [lasmart_t_dim_MasterDistributors] where ID = @ID
	
	if (@Name is null)
		set @error = @error + 'Название, '
	
	if (@County is null)
		set @error = @error + 'Округ, '
	
	if (@Region is null)
		set @error = @error + 'Регион, '
	
	if (@City is null)
		set @error = @error + 'Город, '
	
	if (@SectorID is null)
		set @error = @error + 'Участок, '
	
	if (@isSubDistributor is null)
		set @error = @error + 'Субдистрибьютор, '
	
	if (LEN(@error)>1) begin
		print 'Не заданы обязательные поля: '+left(@error,LEN(@error)-1)
		return
	end
	
	set @dublID = ( select top 1 ID from [lasmart_t_dim_MasterDistributors]
		where Name = @Name
			and County = @County
			and Region = @Region
			and City = @City
			and SectorID = @SectorID
			and isSubDistributor = @isSubDistributor
			and ID <> @ID
			and StatusID = 2
		)
	if (@dublID is not null) begin
		print 'Позиция является дубликатом позиции с ID '+convert(varchar,@dublID)
		return
	end
	
	SET XACT_ABORT ON
	BEGIN TRANSACTION
		
	update [lasmart_t_dim_MasterDistributors]
	set [StatusID] = 2,
	[Code] = (select isnull(MAX([Code]),0) from [lasmart_t_dim_MasterDistributors])+1
	where ID = @ID

	insert [lasmart_t_sync_MasterRefLog]
	([Ref]
	,[ElementID]
	,[Author]
	,[Event])
	VALUES (
		'Distr'
		,@ID
		,@UserName
		,'Статус изменен на Активна'
	)

	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_MasterDistributors_OnChange]    Script Date: 07/26/2014 18:53:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_MasterDistributors_OnChange]  (
	@ID int
)
AS
declare
@UserName varchar(24) = 'Система'
,@FullAddress nvarchar (1024)
,@old_FullAddress nvarchar (1024)
,@Address nvarchar (256)
,@old_Address nvarchar (256)
BEGIN
	SET XACT_ABORT ON
	BEGIN TRANSACTION
		
		select
			 @old_FullAddress = isnull([FullAddress],'') 
			,@old_Address = isnull([Address],'') 
		from [lasmart_t_dim_MasterDistributors] where ID = @ID
		
		
		select
			 @FullAddress = isnull(convert(varchar,ms.PostCode),'')
			 +' '+isnull(cntr.Name,'')
			 +' '+isnull(ms.County,'')
			 +' '+isnull(ms.Region,'')
			 +' '+isnull(ms.District,'')
			 +' '+isnull(ms.City,'')
			 +' '+isnull(ms.Street,'')
			 +' '+isnull(ms.Building,'')
			,@Address = isnull(ms.Region,'')
			 +' '+isnull(ms.District,'')
			 +' '+isnull(ms.City,'')
			 +' '+isnull(ms.Street,'')
			 +' '+isnull(ms.Building,'')
				
		from [lasmart_t_dim_MasterDistributors] ms
		left join [lasmart_t_dim_Country] cntr on ms.CountryID = cntr.ID
		where ms.ID = @ID
		
		set @Address = replace(left(@Address,len(@Address)), '  ',' ')
		set @FullAddress = replace(left(@FullAddress,len(@FullAddress)), '  ',' ')
		
		exec lasmart_p_LogNonIDAttributeChange 'Distr', @ID, @UserName, 'Полный адрес'	, @FullAddress		, @old_FullAddress
		exec lasmart_p_LogNonIDAttributeChange 'Distr', @ID, @UserName, 'Адрес'			, @Address			, @old_Address
		
		update [lasmart_t_dim_MasterDistributors]
		set  [FullAddress] = @FullAddress
			,[Address] = @Address
		where ID = @ID
			
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_MasterStores_OnChange]    Script Date: 07/26/2014 18:53:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_MasterStores_OnChange]  (
	@ID int
)
AS
declare
@UserName varchar(24) = 'Система'
,@FullAddress nvarchar (1024)
,@old_FullAddress nvarchar (1024)
,@Address nvarchar (256)
,@old_Address nvarchar (256)
BEGIN
	SET XACT_ABORT ON
	BEGIN TRANSACTION
		
		select
			 @old_FullAddress = isnull([FullAddress],'') 
			,@old_Address = isnull([Address],'') 
		from [lasmart_t_dim_MasterStores] where ID = @ID
		
		
		select
			 @FullAddress = isnull(convert(varchar,ms.PostCode),'')
			 +' '+isnull(cntr.Name,'')
			 +' '+isnull(ms.County,'')
			 +' '+isnull(ms.Region,'')
			 +' '+isnull(ms.District,'')
			 +' '+isnull(ms.City,'')
			 +' '+isnull(ms.Street,'')
			 +' '+isnull(ms.Building,'')
			,@Address = isnull(ms.Region,'')
			 +' '+isnull(ms.District,'')
			 +' '+isnull(ms.City,'')
			 +' '+isnull(ms.Street,'')
			 +' '+isnull(ms.Building,'')
		from [lasmart_t_dim_MasterStores] ms
		left join [lasmart_t_dim_Country] cntr on ms.CountryID = cntr.ID
		where ms.ID = @ID
		
		set @Address = replace(left(@Address,len(@Address)), '  ',' ')
		set @FullAddress = replace(left(@FullAddress,len(@FullAddress)), '  ',' ')
		
		exec lasmart_p_LogNonIDAttributeChange 'TT', @ID, @UserName, 'Полный адрес'	, @FullAddress		, @old_FullAddress
		exec lasmart_p_LogNonIDAttributeChange 'TT', @ID, @UserName, 'Адрес'			, @Address			, @old_Address
		
		update [lasmart_t_dim_MasterStores]
		set  [FullAddress] = @FullAddress
			,[Address] = @Address
		where ID = @ID
			
	COMMIT TRANSACTION
END
GO
/****** Object:  Table [dbo].[lasmart_t_sync_MasterStoresSlave]    Script Date: 07/26/2014 18:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lasmart_t_sync_MasterStoresSlave](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MasterStoresID] [int] NOT NULL,
	[SlaveStoresID] [int] NOT NULL,
	[Author] [varchar](128) NOT NULL,
	[DateUpdate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[lasmart_v_dim_SyncedStaff]    Script Date: 07/26/2014 18:50:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_dim_SyncedStaff]
as
SELECT sf.[ID]
    ,mss.[MasterStaffID]
    --,sf.[ParentID]
	,sf.[Code]
	--,sf.[SurName]
	--,sf.[Name]
	--,sf.[LastName]
	,sf.[FIO]
	,sf.[BirthDate]
	,sf.[TypeID]
	,sf.[DivisionID]
	,sf.[DateHired]
	,sf.[DateFired]
	,sf.[EMail]
	,sf.[Phone]
	,sf.[Comment]
	,sf.[MotivationID]
	,sf.[Feature]
	,sf.[SectorID]
    ,sf.[DataSource]
      ,convert(varchar,cast(sf.[DateChange] as date)) + ' ' + left(convert(varchar,cast(sf.[DateChange] as time)),5) as [DateChange]
      ,sf.Name as [SyncType]
  FROM [lasmart_t_dim_SlaveStaff] sf
  left join [lasmart_t_sync_SyncStatus] syns on sf.isSynced = syns.ID
  left join [lasmart_t_sync_MasterStaffSlave] mss on sf.[ID] = mss.[SlaveStaffID]
  where sf.isSynced <> 0
GO
/****** Object:  View [dbo].[lasmart_v_dim_MasterStores]    Script Date: 07/26/2014 18:50:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_dim_MasterStores]
as
SELECT 
	s.[ID]
	,s.[Code]
	,s.[Name]
	,s.[TempAddress]
	,s.[PostCode]
	,s.[County]
	,s.[Region]
	,s.[District]
	,s.[City]
	,s.[Street]
	,s.[Building]
	,s.[FullAddress]
	,s.[Address]
	,s.[SectorID]
		,sec.Name as [Sector]
	,s.[DivisionID]
		,div.Name as [Division]
	,s.[SubChannelID]
		,sbch.Name as [SubChannel]
	,s.[ChannelID]
		,ch.Name as [Channel]
	,s.[FormatID]
		,f.Name as [Format]
	,s.[NetID] 
		,ni.Name as [Net]
	,s.[isNet]
	,s.[NetTypeID]
		,nti.Name as [NetType]
	,s.[isNielsen]
	,s.[ContractorGroupID]
		,cntg.Name as [ContractorGroup]
	,s.[isSubDistributor]
	,s.[isAccounting]
	,s.[NonAccReasonID]
		,nar.Name as [NonAccReason]
	,s.[DateOpen]
	,s.[DateClose]
	,s.[ShopArea]
	,s.[Coordinates]
	,s.[GLN]
	,s.[Comment]
	,s.[CountryID]
		,cntr.Name as [Country]
	,s.[FIAS]
	,s.[isInRBP]
	,s.[RespMerchID]
		,resm.Name as [RespMerch]
	,s.[RespTRID]
		,rst.Name as [RespTR]
	,s.[Selection]
	,s.[StatusID]
		,stat.Name as [Status]
	,s.[DateChange]
	,s.Author
FROM 
(SELECT 
[ID]
,[Code]
,[Name]
,[TempAddress]
,[PostCode]
,[County]
,[Region]
,[District]
,[City]
,[Street]
,[Building]
,[FullAddress]
,[Address]
,[SectorID]
,[DivisionID]
,[SubChannelID]
,[ChannelID]
,[FormatID]
,[NetID]
,[isNet]
,[NetTypeID]
,[isNielsen]
,null as [ContractorGroupID]
,null as [isSubDistributor]
,[DateOpen]
,[DateClose]
,[ShopArea]
,[Coordinates]
,[GLN]
,[Comment]
,[CountryID]
,[FIAS]
,[isInRBP]
,[RespMerchID]
,[RespTRID]
,null as [isAccounting]
,null as [NonAccReasonID]
,[Selection]
,[StatusID]
,convert(varchar,cast([DateUpdate] as date)) + ' ' + left(convert(varchar,cast([DateUpdate] as time)),5) as [DateChange]
,rl.Author
FROM [dbo].[lasmart_t_dim_MasterStores] s
left join lasmart_v_sync_MasterStoreRefLog_LastEvents rl on s.[ID] = rl.StoreID

UNION ALL

SELECT 
[ID]
,[Code]
,[Name]
,[TempAddress]
,[PostCode]
,[County]
,[Region]
,[District]
,[City]
,[Street]
,[Building]
,[FullAddress]
,[Address]
,[SectorID]
,null as [DivisionID]
,null as [SubChannelID]
,null as [ChannelID]
,null as [FormatID]
,null as [NetID]
,[isNet]
,null as [NetTypeID]
,null as [isNielsen]
,[ContractorGroupID]
,[isSubDistributor]
,[DateOpen]
,[DateClose]
,null as [ShopArea]
,null as [Coordinates]
,null as [GLN]
,[Comment]
,[CountryID]
,null as [FIAS]
,null as [isInRBP]
,null as [RespMerchID]
,null as [RespTRID]
,[isAccounting]
,[NonAccReasonID]
,null as [Selection]
,[StatusID]
,convert(varchar,cast([DateUpdate] as date)) + ' ' + left(convert(varchar,cast([DateUpdate] as time)),5) as [DateChange]
,rl.Author
FROM [dbo].[lasmart_t_dim_MasterDistributors] s
left join lasmart_v_sync_MasterDistrRefLog_LastEvents rl on s.[ID] = rl.DistrID
) s

left join lasmart_t_dim_Sector sec on s.SectorID = sec.ID
left join lasmart_t_dim_Division div on s.DivisionID = div.ID
left join lasmart_t_dim_SubChannel sbch on s.SubChannelID = sbch.ID
left join lasmart_t_dim_Channel ch on s.ChannelID = ch.ID
left join lasmart_t_dim_Format f on s.FormatID = f.ID
left join lasmart_t_dim_Net ni on s.NetID = ni.ID 
left join lasmart_t_dim_NetType nti on s.NetTypeID = nti.ID
left join lasmart_t_dim_ContractorGroup cntg on s.ContractorGroupID = cntg.ID
left join lasmart_t_dim_Country cntr on s.CountryID = cntr.ID
left join lasmart_t_dim_RespMerch resm on s.RespMerchID = resm.ID
left join lasmart_t_dim_RespTR rst on s.RespTRID = rst.ID
left join lasmart_t_dim_NonAccReason nar on s.[NonAccReasonID] = nar.ID
left join lasmart_t_sync_PositionStatus stat on  s.StatusID = stat.ID
GO
/****** Object:  View [dbo].[lasmart_v_dim_SyncedStores]    Script Date: 07/26/2014 18:50:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_dim_SyncedStores]
as
SELECT ss.[ID]
	,mss.[MasterStoresID]
	,ss.[DistrCode]
	,ss.[Code]
	,ss.[Name]
	,ss.[TempAddress]
	,ss.[County]
	,ss.[Region]
	,ss.[City]
	,ss.[Division]
	,ss.[Sector]
	,ss.[SubChannel]
	,ss.[Format]
	,ss.[Net]
	,ss.[isNet]
	,ss.[DataSource]
	,convert(varchar,cast(ss.[DateChange] as date)) + ' ' + left(convert(varchar,cast(ss.[DateChange] as time)),5) as [DateChange]
	,syns.Name as [SyncType]
FROM [lasmart_t_dim_SlaveStores] ss
left join [lasmart_t_sync_SyncStatus] syns on ss.isSynced = syns.ID
left join [lasmart_t_sync_MasterStoresSlave] mss on ss.[ID] = mss.[SlaveStoresID]
where ss.isSynced <> 0
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_SlaveStores_Unsync]    Script Date: 07/26/2014 18:53:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_SlaveStores_Unsync]  (
	@UserID int
	,@ID int
)
AS
declare
@UserName varchar(128) = ''
,@MasterID int
BEGIN
	SET XACT_ABORT ON
	BEGIN TRANSACTION
	
	select @UserName = FullName from [lasmart_t_sync_Users] where ID = @UserID
	select @MasterID = [MasterStoresID] from [lasmart_t_sync_MasterStoresSlave] where [SlaveStoresID] = @ID
	
	delete [lasmart_t_sync_MasterStoresSlave] where [SlaveStoresID] = @ID
			
	update [lasmart_t_dim_SlaveStores]
	set isSynced = 0
	where ID = @ID
	
	insert [lasmart_t_sync_MasterRefLog]
	([Ref]
	,[ElementID]
	,[Author]
	,[Event])
	VALUES (
		'TT'
		,@MasterID
		,@UserName
		,'От позиции отвязана позиция подчиненного справочника '+ISNULL((select top 1 Name from lasmart_t_dim_SlaveStores with (nolock) where [ID] = @ID),'Не задано')+' (ID '+ISNULL(CONVERT(varchar,@ID),'Не задано')+')'
	)
			
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_SlaveStores_Sync]    Script Date: 07/26/2014 18:53:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_SlaveStores_Sync]  (
	@UserID int
	,@ID int
	,@MasterID int
)
AS
declare
@UserName varchar(128) = ''
BEGIN
	SET XACT_ABORT ON
	BEGIN TRANSACTION
	
	select @UserName = FullName from [lasmart_t_sync_Users] where ID = @UserID

	insert [lasmart_t_sync_MasterStoresSlave]
	([MasterStoresID]
	,[SlaveStoresID]
	,[Author])
    VALUES (
		@MasterID
		,@ID
		,@UserName
    )
			
	update [lasmart_t_dim_SlaveStores]
	set isSynced = 1
	where ID = @ID
	
	insert [lasmart_t_sync_MasterRefLog]
	([Ref]
	,[ElementID]
	,[Author]
	,[Event])
	VALUES (
		'TT'
		,@MasterID
		,@UserName
		,'К позиции привязана позиция подчиненного справочника '+ISNULL((select top 1 Name from lasmart_t_dim_SlaveStores with (nolock) where [ID] = @ID),'Не задано')+' (ID '+ISNULL(CONVERT(varchar,@ID),'Не задано')+')'
	)
			
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_MasterStores_Update]    Script Date: 07/26/2014 18:53:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_MasterStores_Update]  (
	 @UserID int
	,@ID varchar(max)
	,@Name varchar (256)
	,@TempAddress varchar (1024)
	,@CountryID int
	,@PostCode int
	,@County varchar (128)
	,@Region varchar (128)
	,@District varchar (128)
	,@City varchar (128)
	,@Street varchar (128)
	,@Building varchar (64)
	,@DivisionID int
	,@SectorID int
	,@ChannelID int
	,@SubChannelID int
	,@FormatID int
	,@NetID int
	,@isNet tinyint
	,@ContractorGroupID int
	,@isSubDistributor tinyint
	,@DateOpen date
	,@DateClose date
	,@ShopArea money
	,@Coordinates varchar (32)
	,@GLN varchar (32)
	,@Comment varchar(1024)
	,@FIAS varchar (32)
	,@isInRBP tinyint
	,@RespTRID int
	,@isAccounting tinyint
	,@NonAccReasonID int
)
AS
declare
@query varchar(max) = ''
,@UserName varchar(128) = ''
,@old_Name varchar (256)
,@old_TempAddress varchar (1024)
,@old_CountryID int
,@old_PostCode int
,@old_County varchar (128)
,@old_Region varchar (128)
,@old_District varchar (128)
,@old_City varchar (128)
,@old_Street varchar (128)
,@old_Building varchar (64)
,@old_DivisionID int
,@old_SectorID int
,@old_ChannelID int
,@old_SubChannelID int
,@old_FormatID int
,@old_isNet tinyint
,@old_NetID int
,@old_ContractorGroupID int
,@old_isSubDistributor tinyint
,@old_DateOpen date
,@old_DateClose date
,@old_ShopArea money
,@old_Coordinates varchar (32)
,@old_GLN varchar (32)
,@old_Comment varchar(1024)
,@old_FIAS varchar (32)
,@old_isInRBP tinyint
,@old_RespTRID int
,@old_isAccounting tinyint
,@old_NonAccReasonID int
,@i int
,@j int
BEGIN
	SET XACT_ABORT ON
	BEGIN TRANSACTION
	
	select @UserName = FullName from [lasmart_t_sync_Users] where ID = @UserID
	
	SELECT @i=MIN([Item]),@j=MAX([Item]) FROM dbo.lasmart_f_splitString (@ID)
	while @i<=@j
	begin
	
		if (isnull(@isNet,0) in (0,1)) begin --ТТ
			select @old_Name = isnull(Name,'')
					,@old_TempAddress = isnull(TempAddress,'')
					,@old_CountryID = isnull(CountryID,0)
					,@old_PostCode = isnull(PostCode,0)
					,@old_County = isnull(County,'')
					,@old_Region = isnull(Region,'')
					,@old_District = isnull(District,'')
					,@old_City = isnull(City,'')
					,@old_Street = isnull(Street,'')
					,@old_Building = isnull(Building,'')
					,@old_DivisionID = isnull(DivisionID,0)
					,@old_SectorID = isnull(SectorID,0)
					,@old_ChannelID = isnull(ChannelID,0)
					,@old_SubChannelID = isnull(SubChannelID,0)
					,@old_FormatID = isnull(FormatID,0)
					,@old_isNet = isnull(isNet,0)
					,@old_NetID = isnull(NetID,0)
					,@old_DateOpen = isnull(DateOpen,'')
					,@old_DateClose = isnull(DateClose,'')
					,@old_ShopArea = isnull(ShopArea,0)
					,@old_Coordinates = isnull(Coordinates,'')
					,@old_GLN = isnull(GLN,'')
					,@old_Comment = isnull(Comment,'')
					,@old_FIAS = isnull(FIAS,'')
					,@old_isInRBP = isnull(isInRBP,0)
					,@old_RespTRID = isnull(RespTRID,0)
			from [lasmart_t_dim_MasterStores] where ID = @i
			
			set @query = 'update [lasmart_t_dim_MasterStores] set '
			
			if (@Name is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Название'		, @Name	, @old_Name
				set @query = @query + ' [Name] = '''+convert(varchar,@Name)+''', '
			end
			
			
			if (@TempAddress is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Черновой адрес'		, @TempAddress	, @old_TempAddress
				set @query = @query + ' [TempAddress] = '''+convert(varchar,@TempAddress)+''', '
			end
			
			
			if (@CountryID is not null)
			begin
				exec lasmart_p_LogIDAttributeChange 'TT', @i, @UserName, 'Страна'		,'lasmart_t_dim_Country'	, @CountryID			, @old_CountryID
				set @query = @query + ' [CountryID] = '+convert(varchar,@CountryID)+', '
			end
			
			if (@PostCode is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Индекс'		, @PostCode	, @old_PostCode
				set @query = @query + ' [PostCode] = '+convert(varchar,@PostCode)+', '
			end
			
			if (@County is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Округ'		, @County	, @old_County
				set @query = @query + ' [County] = '''+convert(varchar,@County)+''', '
			end
			
			if (@Region is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Регион'		, @Region	, @old_Region
				set @query = @query + ' [Region] = '''+convert(varchar,@Region)+''', '
			end
			
			if (@District is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Район'		, @District	, @old_District
				set @query = @query + ' [District] = '''+convert(varchar,@District)+''', '
			end
			
			if (@City is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Город'		, @City	, @old_City
				set @query = @query + ' [City] = '''+convert(varchar,@City)+''', '
			end
			
			if (@Street is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Улица'		, @Street	, @old_Street
				set @query = @query + ' [Street] = '''+convert(varchar,@Street)+''', '
			end
			
			if (@Building is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Дом'		, @Building	, @old_Building
				set @query = @query + ' [Building] = '''+convert(varchar,@Building)+''', '
			end
			
			if (@DivisionID is not null)
			begin
				exec lasmart_p_LogIDAttributeChange 'TT', @i, @UserName, 'Дивизион'					,'lasmart_t_dim_Division'			, @DivisionID			, @old_DivisionID
				set @query = @query + ' [DivisionID] = '+convert(varchar,@DivisionID)+', '
			end
			
			if (@SectorID is not null)
			begin
				exec lasmart_p_LogIDAttributeChange 'TT', @i, @UserName, 'Участок'					,'lasmart_t_dim_Sector'			, @SectorID			, @old_SectorID
				set @query = @query + ' [SectorID] = '+convert(varchar,@SectorID)+', '
			end
			
			if (@ChannelID is not null)
			begin
				exec lasmart_p_LogIDAttributeChange 'TT', @i, @UserName, 'Канал'					,'lasmart_t_dim_Channel'			, @ChannelID			, @old_ChannelID
				set @query = @query + ' [ChannelID] = '+convert(varchar,@ChannelID)+', '
			end
			
			if (@SubChannelID is not null)
			begin
				exec lasmart_p_LogIDAttributeChange 'TT', @i, @UserName, 'Подканал'					,'lasmart_t_dim_SubChannel'			, @SubChannelID			, @old_SubChannelID
				set @query = @query + ' [SubChannelID] = '+convert(varchar,@SubChannelID)+', '
			end
			
			if (@FormatID is not null)
			begin
				exec lasmart_p_LogIDAttributeChange 'TT', @i, @UserName, 'Формат'					,'lasmart_t_dim_Format'			, @FormatID			, @old_FormatID
				set @query = @query + ' [FormatID] = '+convert(varchar,@FormatID)+', '
			end
			
			if (@isNet is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Сеть'		, @isNet	, @old_isNet
				set @query = @query + ' [isNet] = '+convert(varchar,@isNet)+', '
				if (@isNet = 1) begin
					if (@NetID is not null)
					begin
						exec lasmart_p_LogIDAttributeChange 'TT', @i, @UserName, 'Название сети'				,'lasmart_t_dim_Net'			, @NetID			, @old_NetID
						set @query = @query + ' [NetID] = '+convert(varchar,@NetID)+', '
					end
				end
				else begin
					exec lasmart_p_LogIDAttributeChange 'TT', @i, @UserName, 'Название сети'				,'lasmart_t_dim_Net'			, null			, @old_NetID
					set @query = @query + ' [NetID] = null, '
				end
			end
			
			if (@DateOpen is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Дата открытия'		, @DateOpen	, @old_DateOpen
				set @query = @query + ' [DateOpen] = '''+convert(varchar,@DateOpen)+''', '
			end
			
			if (@DateClose is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Дата закрытия'		, @DateClose	, @old_DateClose
				set @query = @query + ' [DateClose] = '''+convert(varchar,@DateClose)+''', '
			end
			
			if (@ShopArea is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Площадь ТТ'		, @ShopArea	, @old_ShopArea
				set @query = @query + ' [ShopArea] = '+convert(varchar,@ShopArea)+', '
			end
			
			if (@Coordinates is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Координаты'		, @Coordinates	, @old_Coordinates
				set @query = @query + ' [Coordinates] = '''+convert(varchar,@Coordinates)+''', '
			end
			
			if (@GLN is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'GLN'		, @GLN	, @old_GLN
				set @query = @query + ' [GLN] = '''+convert(varchar,@GLN)+''', '
			end
			
			if (@Comment is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Комментарий'		, @Comment	, @old_Comment
				set @query = @query + ' [Comment] = '''+convert(varchar,@Comment)+''', '
			end
			
			if (@FIAS is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'ФиАС'		, @FIAS	, @old_FIAS
				set @query = @query + ' [FIAS] = '''+convert(varchar,@FIAS)+''', '
			end
			
			if (@isInRBP is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'TT', @i, @UserName, 'Участвует в РБП'		, @isInRBP	, @old_isInRBP
				set @query = @query + ' [isInRBP] = '+convert(varchar,@isInRBP)+', '
			end
			
			if (@RespTRID is not null)
			begin
				exec lasmart_p_LogIDAttributeChange 'TT', @i, @UserName, 'Ответственный ТП'		,'lasmart_t_dim_RespTR'	, @RespTRID			, @old_RespTRID
				set @query = @query + ' [RespTRID] = '+convert(varchar,@RespTRID)+', '
			end
			
			set @query = substring(@query,1,LEN(@query)-1) + ' where ID = '+convert(varchar,@i)
			
			--print @query
			exec (@query)
			
			exec [lasmart_p_MasterStores_OnChange] @ID = @i
		
		end --TT
		else begin --Distributor
		
			select @old_Name = isnull(Name,'')
					,@old_TempAddress = isnull(TempAddress,'')
					,@old_CountryID = isnull(CountryID,0)
					,@old_PostCode = isnull(PostCode,0)
					,@old_County = isnull(County,'')
					,@old_Region = isnull(Region,'')
					,@old_District = isnull(District,'')
					,@old_City = isnull(City,'')
					,@old_Street = isnull(Street,'')
					,@old_Building = isnull(Building,'')
					,@old_SectorID = isnull(SectorID,0)
					,@old_isNet = isnull(isNet,0)
					,@old_ContractorGroupID = isnull(ContractorGroupID,0)
					,@old_isSubDistributor = isnull(isSubDistributor,0)
					,@old_DateOpen = isnull(DateOpen,'')
					,@old_DateClose = isnull(DateClose,'')
					,@old_Comment = isnull(Comment,'')
					,@old_isAccounting = isnull(isAccounting,0)
					,@old_NonAccReasonID = isnull(NonAccReasonID,0)
			from [lasmart_t_dim_MasterDistributors] where ID = @i
		
			set @query = 'update [lasmart_t_dim_MasterDistributors] set '
			
			if (@Name is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'Distr', @i, @UserName, 'Название'		, @Name	, @old_Name
				set @query = @query + ' [Name] = '''+convert(varchar,@Name)+''', '
			end
			
			
			if (@TempAddress is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'Distr', @i, @UserName, 'Черновой адрес'		, @TempAddress	, @old_TempAddress
				set @query = @query + ' [TempAddress] = '''+convert(varchar,@TempAddress)+''', '
			end
			
			
			if (@CountryID is not null)
			begin
				exec lasmart_p_LogIDAttributeChange 'Distr', @i, @UserName, 'Страна'		,'lasmart_t_dim_Country'	, @CountryID			, @old_CountryID
				set @query = @query + ' [CountryID] = '+convert(varchar,@CountryID)+', '
			end
			
			if (@PostCode is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'Distr', @i, @UserName, 'Индекс'		, @PostCode	, @old_PostCode
				set @query = @query + ' [PostCode] = '+convert(varchar,@PostCode)+', '
			end
			
			if (@County is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'Distr', @i, @UserName, 'Округ'		, @County	, @old_County
				set @query = @query + ' [County] = '''+convert(varchar,@County)+''', '
			end
			
			if (@Region is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'Distr', @i, @UserName, 'Регион'		, @Region	, @old_Region
				set @query = @query + ' [Region] = '''+convert(varchar,@Region)+''', '
			end
			
			if (@District is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'Distr', @i, @UserName, 'Район'		, @District	, @old_District
				set @query = @query + ' [District] = '''+convert(varchar,@District)+''', '
			end
			
			if (@City is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'Distr', @i, @UserName, 'Город'		, @City	, @old_City
				set @query = @query + ' [City] = '''+convert(varchar,@City)+''', '
			end
			
			if (@Street is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'Distr', @i, @UserName, 'Улица'		, @Street	, @old_Street
				set @query = @query + ' [Street] = '''+convert(varchar,@Street)+''', '
			end
			
			if (@Building is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'Distr', @i, @UserName, 'Дом'		, @Building	, @old_Building
				set @query = @query + ' [Building] = '''+convert(varchar,@Building)+''', '
			end
		
			if (@SectorID is not null)
			begin
				exec lasmart_p_LogIDAttributeChange 'Distr', @i, @UserName, 'Участок'					,'lasmart_t_dim_Sector'			, @SectorID			, @old_SectorID
				set @query = @query + ' [SectorID] = '+convert(varchar,@SectorID)+', '
			end
			
			if (@isNet is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'Distr', @i, @UserName, 'Сеть'		, @isNet	, @old_isNet
				set @query = @query + ' [isNet] = '+convert(varchar,@isNet)+', '
			end
		
			if (@ContractorGroupID is not null)
			begin
				exec lasmart_p_LogIDAttributeChange 'Distr', @i, @UserName, 'Группа контрагентов'		,'lasmart_t_dim_ContractorGroup'	, @ContractorGroupID			, @old_ContractorGroupID
				set @query = @query + ' [ContractorGroupID] = '+convert(varchar,@ContractorGroupID)+', '
			end
			
			if (@isSubDistributor is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'Distr', @i, @UserName, 'Сабдистрибьютор'		, @isSubDistributor	, @old_isSubDistributor
				set @query = @query + ' [isSubDistributor] = '+convert(varchar,@isSubDistributor)+', '
			end
			
			if (@DateOpen is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'Distr', @i, @UserName, 'Дата открытия'		, @DateOpen	, @old_DateOpen
				set @query = @query + ' [DateOpen] = '''+convert(varchar,@DateOpen)+''', '
			end
			
			if (@DateClose is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'Distr', @i, @UserName, 'Дата закрытия'		, @DateClose	, @old_DateClose
				set @query = @query + ' [DateClose] = '''+convert(varchar,@DateClose)+''', '
			end
			
			if (@Comment is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'Distr', @i, @UserName, 'Комментарий'		, @Comment	, @old_Comment
				set @query = @query + ' [Comment] = '''+convert(varchar,@Comment)+''', '
			end
			
			if (@isAccounting is not null)
			begin
				exec lasmart_p_LogNonIDAttributeChange 'Distr', @i, @UserName, 'Учитывать в расчетах'		, @isAccounting	, @old_isAccounting
				set @query = @query + ' [isAccounting] = '+convert(varchar,@isAccounting)+', '
			end
			
			if (@NonAccReasonID is not null)
			begin
				exec lasmart_p_LogIDAttributeChange 'Distr', @i, @UserName, 'Причина неучета'		,'lasmart_t_dim_NonAccReason'	, @NonAccReasonID			, @old_NonAccReasonID
				set @query = @query + ' [NonAccReasonID] = '+convert(varchar,@NonAccReasonID)+', '
			end
			
			set @query = substring(@query,1,LEN(@query)-1) + ' where ID = '+convert(varchar,@i)
			
			--print @query
			exec (@query)
			
			exec [lasmart_p_MasterDistributors_OnChange] @ID = @i
			
		end --Distributor
	
	
		SELECT @i=MIN([Item]) FROM dbo.lasmart_f_splitString (@ID) WHERE [Item]>@i
	end	--while	
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_MasterStores_SetDeleteStatus]    Script Date: 07/26/2014 18:53:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_MasterStores_SetDeleteStatus]  (
	@UserID int
	,@ID int
)
AS
declare
	@error varchar(max) = ''
	,@UserName varchar(128) = ''
	,@slaveIDs varchar(max) = ''
BEGIN
	select @UserName = FullName from [lasmart_t_sync_Users] where ID = @UserID
	
	SELECT @slaveIDs = @slaveIDs + convert(varchar,[SlaveStoresID]) + ','
	FROM [lasmart_t_sync_MasterStoresSlave]
	where [MasterStoresID] = @ID
	order by [SlaveStoresID]
	
	if (LEN(@slaveIDs) > 0) set @slaveIDs = left(@slaveIDs,len(@slaveIDs)-1)
	else set @slaveIDs = null
	
	if (@slaveIDs is not null) begin
		print 'У позиции есть привязки (ID '+@slaveIDs+')'
		return
	end
	
	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRANSACTION
		insert [lasmart_t_sync_MasterRefLog]
		([Ref]
		,[ElementID]
		,[Author]
		,[Event])
		VALUES (
			'TT'
			,@ID
			,@UserName
			,'Статус изменен на "Помечена на удаление"'
		)

		if ((select top 1 [StatusID] from [lasmart_t_dim_MasterStores] where ID = @ID) = 2) begin
			update [lasmart_t_dim_MasterStores]
			set [StatusID]  = 3
			where ID = @ID
		end

		if ((select top 1 [StatusID] from [lasmart_t_dim_MasterStores] where ID = @ID) = 1) begin
			delete [lasmart_t_dim_MasterStores] where ID = @ID
		end
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_MasterStores_SetActiveStatus]    Script Date: 07/26/2014 18:53:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_MasterStores_SetActiveStatus]  (
	@UserID int
	,@ID int
)
AS
declare
	@error varchar(max) = ''
	,@dublID int
	,@UserName varchar(128) = ''
	,@Name varchar (256)
	,@County varchar (128)
	,@Region varchar (128)
	,@City varchar (128)
	,@SubChannelID int
	,@FormatID int
	,@NetID int
	,@isNet tinyint
	,@isInRBP tinyint
BEGIN
	select @UserName = FullName from [lasmart_t_sync_Users] where ID = @UserID
	
	select 
	@Name = Name
	,@County = County
	,@Region = Region
	,@City = City
	,@SubChannelID = SubChannelID
	,@FormatID = FormatID
	,@NetID = NetID
	,@isNet = isNet
	,@isInRBP = isInRBP
	from [lasmart_t_dim_MasterStores] where ID = @ID
	
	if (@Name is null)
		set @error = @error + 'Название, '
	
	if (@County is null)
		set @error = @error + 'Округ, '
	
	if (@Region is null)
		set @error = @error + 'Регион, '
	
	if (@City is null)
		set @error = @error + 'Город, '
	
	if (@SubChannelID is null)
		set @error = @error + 'Подканал, '
	
	if (@FormatID is null)
		set @error = @error + 'Формат, '
	
	if (@isNet is null)
		set @error = @error + 'Сеть, '
	else begin
		if (@isNet = 1 and @NetID is null)
			set @error = @error + 'Название сети, '
	end
	
	if (@isInRBP is null)
		set @error = @error + 'Участвует в РБП, '
	
	if (LEN(@error)>1) begin
		print 'Не заданы обязательные поля: '+left(@error,LEN(@error)-1)
		return
	end
	
	set @dublID = ( select top 1 ID from [lasmart_v_dim_MasterStores]
		where Name = @Name
			and County = @County
			and Region = @Region
			and City = @City
			and SubChannelID = @SubChannelID
			and FormatID = @FormatID
			and NetID = @NetID
			and isNet = @isNet
			and isInRBP = @isInRBP
			and ID <> @ID
			and StatusID = 2
		)
	if (@dublID is not null) begin
		print 'Позиция является дубликатом позиции с ID '+convert(varchar,@dublID)
		return
	end
	
	SET XACT_ABORT ON
	BEGIN TRANSACTION
		
	update [lasmart_t_dim_MasterStores]
	set [StatusID] = 2
	where ID = @ID

	insert [lasmart_t_sync_MasterRefLog]
	([Ref]
	,[ElementID]
	,[Author]
	,[Event])
	VALUES (
		'TT'
		,@ID
		,@UserName
		,'Статус изменен на Активна'
	)

	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_MasterStores_Create]    Script Date: 07/26/2014 18:53:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_MasterStores_Create]  (
	@UserID int
	,@Name varchar (256)
	,@TempAddress varchar (1024)
	,@CountryID int
	,@PostCode int
	,@County varchar (128)
	,@Region varchar (128)
	,@District varchar (128)
	,@City varchar (128)
	,@Street varchar (128)
	,@Building varchar (64)
	
	,@DivisionID int
	,@SectorID int
	
	,@ChannelID int
	,@SubChannelID int
	
	,@FormatID int
	
	,@NetID int
	,@isNet tinyint
	
	,@ContractorGroupID int
	,@isSubDistributor tinyint
	
	,@DateOpen date
	,@DateClose date
	,@ShopArea money
	,@Coordinates varchar (32)
	,@GLN varchar (32)
	,@Comment varchar(1024)
	
	,@FIAS varchar (32)
	,@isInRBP tinyint
	,@RespTRID int
	,@isAccounting tinyint
	,@NonAccReasonID int
)
AS
declare
	@ID int
	,@query varchar(max) = ''
	,@UserName varchar(128) = ''
BEGIN
	SET XACT_ABORT ON
	BEGIN TRANSACTION
	
	select @UserName = FullName from [lasmart_t_sync_Users] where ID = @UserID
	
	if (isnull(@isNet,0) in (0,1)) begin --ТТ
	
		insert [lasmart_t_dim_MasterStores] (
			[Name]
			,[TempAddress]
			,[CountryID]
			,[PostCode]
			,[County]
			,[Region]
			,[District]
			,[City]
			,[Street]
			,[Building]
			,[DivisionID]
			,[SectorID]
			,[ChannelID]
			,[SubChannelID]
			,[FormatID]
			,[isNet]
			,[NetID]
			,[DateOpen]
			,[DateClose]
			,[ShopArea]
			,[Coordinates]
			,[GLN]
			,[Comment]
			,[FIAS]
			,[isInRBP]
			,[RespTRID]
			,[StatusID]
		)
		VALUES(
			 @Name
			,@TempAddress
			,@CountryID
			,@PostCode
			,@County
			,@Region
			,@District
			,@City
			,@Street
			,@Building
			,@DivisionID
			,@SectorID
			,@ChannelID
			,@SubChannelID
			,@FormatID
			,@isNet
			,@NetID
			,@DateOpen
			,@DateClose
			,@ShopArea
			,@Coordinates
			,@GLN
			,@Comment
			,@FIAS
			,@isInRBP
			,@RespTRID
			,1
		)

		set @ID = (select top 1 ID from [lasmart_t_dim_MasterStores] with (nolock) order by ID desc)
		
		insert [lasmart_t_sync_MasterRefLog]
		([Ref]
		,[ElementID]
		,[Author]
		,[Event])
		VALUES (
			'TT'
			,@ID
			,@UserName
			,'Создана новая ТТ'
		)
		
		exec [lasmart_p_MasterStores_OnChange] @ID = @ID
		
	end --TT
	else begin --Distributor
		insert [lasmart_t_dim_MasterDistributors] (
			[Name]
			,[TempAddress]
			,[CountryID]
			,[PostCode]
			,[County]
			,[Region]
			,[District]
			,[City]
			,[Street]
			,[Building]
			,[SectorID]
			,[isNet]
			,[ContractorGroupID]
			,[isSubDistributor]
			,[DateOpen]
			,[DateClose]
			,[Comment]
			,[isAccounting]
			,[NonAccReasonID]
			,[StatusID]
			
		)
		VALUES(
			 @Name
			,@TempAddress
			,@CountryID
			,@PostCode
			,@County
			,@Region
			,@District
			,@City
			,@Street
			,@Building
			,@SectorID
			,@isNet
			,@ContractorGroupID
			,@isSubDistributor
			,@DateOpen
			,@DateClose
			,@Comment
			,@isAccounting
			,@NonAccReasonID
			,1
		)

		set @ID = (select top 1 ID from [lasmart_t_dim_MasterDistributors] with (nolock) order by ID desc)
		
		insert [lasmart_t_sync_MasterRefLog]
		([Ref]
		,[ElementID]
		,[Author]
		,[Event])
		VALUES (
			'Distr'
			,@ID
			,@UserName
			,'Создан новый Дистрибьютор'
		)
		
		exec [lasmart_p_MasterDistributors_OnChange] @ID = @ID
		
	end --Distributor
	COMMIT TRANSACTION
END
GO
/****** Object:  Default [DF__lasmart_t__Delet__2C3E80C8]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Channel] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__1CFC3D38]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_ContractorGroup] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__155B1B70]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Country] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__1372D2FE]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Division] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__118A8A8C]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Format] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Statu__6E0C4425]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterDistributors] ADD  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF__lasmart_t__Statu__5728DECD]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStaff] ADD  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF__lasmart_t__Statu__5634BA94]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores] ADD  DEFAULT ((1)) FOR [StatusID]
GO
/****** Object:  Default [DF__lasmart_t__Delet__1CC7330E]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Net] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__38A457AD]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_NetType] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__6B2FD77A]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_NonAccReason] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__47E69B3D]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Sector] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__DateC__5EFF0ABF]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_SlaveStaff] ADD  DEFAULT (getdate()) FOR [DateChange]
GO
/****** Object:  Default [DF__lasmart_t__isSyn__5FF32EF8]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_SlaveStaff] ADD  DEFAULT ((0)) FOR [isSynced]
GO
/****** Object:  Default [DF__lasmart_t__DateC__2C09769E]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_SlaveStores] ADD  DEFAULT (getdate()) FOR [DateChange]
GO
/****** Object:  Default [DF__lasmart_t__isSyn__2CFD9AD7]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_SlaveStores] ADD  DEFAULT ((0)) FOR [isSynced]
GO
/****** Object:  Default [DF__lasmart_t__Delet__2F1AED73]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_SubChannel] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__222B06A9]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_TypeStaff] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__DateU__7306036C]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_sync_MasterStaffSlave] ADD  DEFAULT (getdate()) FOR [DateUpdate]
GO
/****** Object:  Default [DF__lasmart_t__DateU__7E42ABEE]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_sync_MasterStoresSlave] ADD  DEFAULT (getdate()) FOR [DateUpdate]
GO
/****** Object:  Default [DF__sync_temp__DateC__09B45E9A]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[sync_template] ADD  DEFAULT (getdate()) FOR [DateCreate]
GO
/****** Object:  ForeignKey [FK__lasmart_t__Contr__6C23FBB3]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterDistributors]  WITH CHECK ADD FOREIGN KEY([ContractorGroupID])
REFERENCES [dbo].[lasmart_t_dim_ContractorGroup] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Count__61A66D40]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterDistributors]  WITH CHECK ADD FOREIGN KEY([CountryID])
REFERENCES [dbo].[lasmart_t_dim_Country] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__NonAc__6D181FEC]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterDistributors]  WITH CHECK ADD FOREIGN KEY([NonAccReasonID])
REFERENCES [dbo].[lasmart_t_dim_NonAccReason] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Secto__60B24907]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterDistributors]  WITH CHECK ADD FOREIGN KEY([SectorID])
REFERENCES [dbo].[lasmart_t_dim_Sector] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Statu__629A9179]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterDistributors]  WITH CHECK ADD FOREIGN KEY([StatusID])
REFERENCES [dbo].[lasmart_t_sync_PositionStatus] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Divis__064DE20A]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStaff]  WITH CHECK ADD FOREIGN KEY([DivisionID])
REFERENCES [dbo].[lasmart_t_dim_Division] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Motiv__01892CED]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStaff]  WITH CHECK ADD FOREIGN KEY([MotivationID])
REFERENCES [dbo].[lasmart_t_dim_Motivation] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Paren__027D5126]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStaff]  WITH CHECK ADD FOREIGN KEY([ParentID])
REFERENCES [dbo].[lasmart_t_dim_MasterStaff] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Statu__4BEC364B]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStaff]  WITH CHECK ADD FOREIGN KEY([StatusID])
REFERENCES [dbo].[lasmart_t_sync_PositionStatus] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__TypeI__04659998]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStaff]  WITH CHECK ADD FOREIGN KEY([TypeID])
REFERENCES [dbo].[lasmart_t_dim_TypeStaff] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Chann__4CAB505A]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([ChannelID])
REFERENCES [dbo].[lasmart_t_dim_Channel] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Count__4D9F7493]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([CountryID])
REFERENCES [dbo].[lasmart_t_dim_Country] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Divis__4E9398CC]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([DivisionID])
REFERENCES [dbo].[lasmart_t_dim_Division] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Forma__4F87BD05]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([FormatID])
REFERENCES [dbo].[lasmart_t_dim_Format] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__NetTy__0C90CB45]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([NetTypeID])
REFERENCES [dbo].[lasmart_t_dim_NetType] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__RespM__0D84EF7E]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([RespMerchID])
REFERENCES [dbo].[lasmart_t_dim_MasterStaff] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__RespT__5911273F]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([RespTRID])
REFERENCES [dbo].[lasmart_t_dim_MasterStaff] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Secto__526429B0]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([SectorID])
REFERENCES [dbo].[lasmart_t_dim_Sector] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Statu__53584DE9]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([StatusID])
REFERENCES [dbo].[lasmart_t_sync_PositionStatus] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__SubCh__544C7222]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([SubChannelID])
REFERENCES [dbo].[lasmart_t_dim_SubChannel] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__NetTy__1ADEEA9C]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Net]  WITH CHECK ADD FOREIGN KEY([NetTypeID])
REFERENCES [dbo].[lasmart_t_dim_NetType] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__RespM__1BD30ED5]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Net]  WITH CHECK ADD FOREIGN KEY([RespMerchID])
REFERENCES [dbo].[lasmart_t_dim_MasterStaff] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Chann__310335E5]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_dim_SubChannel]  WITH CHECK ADD FOREIGN KEY([ChannelID])
REFERENCES [dbo].[lasmart_t_dim_Channel] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Maste__711DBAFA]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_sync_MasterStaffSlave]  WITH CHECK ADD FOREIGN KEY([MasterStaffID])
REFERENCES [dbo].[lasmart_t_dim_MasterStaff] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Slave__7211DF33]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_sync_MasterStaffSlave]  WITH CHECK ADD FOREIGN KEY([SlaveStaffID])
REFERENCES [dbo].[lasmart_t_dim_SlaveStaff] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Maste__7C5A637C]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_sync_MasterStoresSlave]  WITH CHECK ADD FOREIGN KEY([MasterStoresID])
REFERENCES [dbo].[lasmart_t_dim_MasterStores] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Slave__2DF1BF10]    Script Date: 07/26/2014 18:50:54 ******/
ALTER TABLE [dbo].[lasmart_t_sync_MasterStoresSlave]  WITH CHECK ADD FOREIGN KEY([SlaveStoresID])
REFERENCES [dbo].[lasmart_t_dim_SlaveStores] ([ID])
GO

/****** Script for SelectTopNRows command from SSMS  ******/
alter table [lasmart_t_sync_MasterRefLog]
add Ref varchar(32) null

alter table [lasmart_t_sync_MasterRefLog]
add ElementID int null

update [lasmart_t_sync_MasterRefLog]
set Ref = 'Sku' ,ElementID = GoodID

alter table [lasmart_t_sync_MasterRefLog]
alter column Ref varchar(32) not null

alter table [lasmart_t_sync_MasterRefLog]
alter column ElementID int not null

alter table [lasmart_t_sync_MasterRefLog]
drop column GoodID
  
  
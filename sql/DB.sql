USE [master]
GO
/****** Object:  Database [Orimi_denis]    Script Date: 07/14/2014 19:55:09 ******/
CREATE DATABASE [Orimi_denis] ON  PRIMARY 
( NAME = N'Orimi_denis', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Orimi_denis.mdf' , SIZE = 28672KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Orimi_denis_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\Orimi_denis_log.ldf' , SIZE = 22144KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Orimi_denis] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Orimi_denis].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Orimi_denis] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [Orimi_denis] SET ANSI_NULLS OFF
GO
ALTER DATABASE [Orimi_denis] SET ANSI_PADDING OFF
GO
ALTER DATABASE [Orimi_denis] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [Orimi_denis] SET ARITHABORT OFF
GO
ALTER DATABASE [Orimi_denis] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [Orimi_denis] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [Orimi_denis] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [Orimi_denis] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [Orimi_denis] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [Orimi_denis] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [Orimi_denis] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [Orimi_denis] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [Orimi_denis] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [Orimi_denis] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [Orimi_denis] SET  DISABLE_BROKER
GO
ALTER DATABASE [Orimi_denis] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [Orimi_denis] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [Orimi_denis] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [Orimi_denis] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [Orimi_denis] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [Orimi_denis] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [Orimi_denis] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [Orimi_denis] SET  READ_WRITE
GO
ALTER DATABASE [Orimi_denis] SET RECOVERY FULL
GO
ALTER DATABASE [Orimi_denis] SET  MULTI_USER
GO
ALTER DATABASE [Orimi_denis] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [Orimi_denis] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'Orimi_denis', N'ON'
GO
USE [Orimi_denis]
GO
/****** Object:  Table [dbo].[lasmart_t_sync_MasterRefLog]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_sync_MasterRefLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GoodID] [int] NOT NULL,
	[Author] [nvarchar](128) NOT NULL,
	[Event] [nvarchar](1024) NOT NULL,
	[DateUpdate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_WeightGroup]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_WeightGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
	[Begin] [int] NULL,
	[End] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_TeaColor]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_TeaColor](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_sync_Users]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_sync_Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Role] [nvarchar](32) NULL,
	[FullName] [nvarchar](128) NOT NULL,
	[isDeleted] [bit] NOT NULL,
	[EMail] [nvarchar](32) NULL,
	[ContactName] [nvarchar](32) NULL,
	[Tel1] [nvarchar](32) NULL,
	[DateCreate] [date] NOT NULL,
	[DateBeginAccess] [date] NOT NULL,
	[DateEndAccess] [date] NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[admin] [tinyint] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_sync_SyncStatus]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_sync_SyncStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_sync_PositionStatus]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_sync_PositionStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Adding]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Adding](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sync_template]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sync_template](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_User] [int] NOT NULL,
	[Name] [varchar](64) NOT NULL,
	[DateCreate] [datetime] NOT NULL,
	[DateUsed] [datetime] NULL,
	[State] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Products]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Код] [nvarchar](255) NULL,
	[вн код продукта] [nvarchar](255) NULL,
	[код продукта] [nvarchar](255) NULL,
	[продукт] [nvarchar](255) NULL,
	[производитель] [nvarchar](255) NULL,
	[бренд] [nvarchar](255) NULL,
	[вес] [nvarchar](255) NULL,
	[упаковка] [nvarchar](255) NULL,
	[количество пакетиков] [nvarchar](255) NULL,
	[категория] [nvarchar](255) NULL,
	[сабкатегория] [nvarchar](255) NULL,
	[вид] [nvarchar](255) NULL,
	[ценовой сегмент] [nvarchar](255) NULL,
	[SKU] [nvarchar](255) NULL,
	[код орими] [nvarchar](255) NULL,
	[Продукт 2] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Sector]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Sector](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_SachetPack]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_SachetPack](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_RespTR]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_RespTR](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_RespMerch]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_RespMerch](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_PriceSegment]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_PriceSegment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_NetType]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_NetType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Net]    Script Date: 07/14/2014 19:55:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Net](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_LogNonIDAttributeChange]    Script Date: 07/14/2014 19:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[lasmart_p_LogNonIDAttributeChange] (
	@GoodID int
	,@UserName  varchar(128)
	,@AttributeName varchar(64)
	,@newValue varchar(1024)
	,@oldValue varchar(1024) = null
)
as
declare 
@query varchar(max) = ''
BEGIN
	if (isnull(@oldValue,'') <> isnull(@newValue,''))
	begin
		set @query = '
		insert [lasmart_t_sync_MasterRefLog]
		([GoodID]
		,[Author]
		,[Event])
		VALUES (
			'+convert(varchar,@GoodID)+','''+@UserName+''',''Обновлено значение '+@AttributeName+' c ''+ISNULL(convert(varchar(max),'+isnull(''''+convert(varchar(max),@oldValue)+'''','null')+'),''Не задано'')+'' на ''+ISNULL(convert(varchar(max),'+isnull(''''+convert(varchar(max),@newValue)+'''','null')+'),''Не задано'')
		)'
		
		print @query
		exec (@query)
	end
END
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_LogIDAttributeChange]    Script Date: 07/14/2014 19:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[lasmart_p_LogIDAttributeChange] (
	@GoodID int
	,@UserName  varchar(128)
	,@AttributeName varchar(64)
	,@table varchar(64)
	,@newValue int
	,@oldValue int = null
)
as
declare 
@query varchar(max) = ''
BEGIN
	if (isnull(@oldValue,0) <> isnull(@newValue,0))
	begin
		set @query = '
		insert [lasmart_t_sync_MasterRefLog]
		([GoodID]
		,[Author]
		,[Event])
		VALUES (
			'+convert(varchar,@GoodID)+','''+@UserName+''',''Обновлено значение '+@AttributeName+' c ''+ISNULL((select top 1 Name from ['+@table+'] with(nolock) where ID = '+isnull(convert(varchar,@oldValue),'null')+'),''Не задано'')+'' на ''+ISNULL((select top 1 Name from ['+@table+'] with(nolock) where ID = '+isnull(convert(varchar,@newValue),'null')+')+'' (ID''+convert(varchar,'+convert(varchar,@newValue)+')+'')'',''Не задано'')
		)'
		
		print @query
		exec (@query)
	end
END
GO
/****** Object:  UserDefinedFunction [dbo].[lasmart_f_splitString]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[lasmart_f_splitString] ( @stringToSplit VARCHAR(MAX) )
RETURNS
 @returnList TABLE ([Item] [varchar] (128))
AS
BEGIN

 DECLARE @name NVARCHAR(128)
 DECLARE @pos INT

 WHILE CHARINDEX(',', @stringToSplit) > 0
 BEGIN
  SELECT @pos  = CHARINDEX(',', @stringToSplit)  
  SELECT @name = SUBSTRING(@stringToSplit, 1, @pos-1)

  INSERT INTO @returnList 
  SELECT @name

  SELECT @stringToSplit = SUBSTRING(@stringToSplit, @pos+1, LEN(@stringToSplit)-@pos)
 END

 INSERT INTO @returnList
 SELECT @stringToSplit

 RETURN
END
GO
/****** Object:  Table [dbo].[lasmart_dim_TypeStaff]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_dim_TypeStaff](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[ShortName] [nvarchar](255) NULL,
	[BriefNotation] [nvarchar](255) NULL,
	[Deleted] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_dim_SubChannel]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_dim_SubChannel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[ShortName] [nvarchar](255) NULL,
	[BriefNotation] [nvarchar](255) NULL,
	[Deleted] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_dim_NetType]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_dim_NetType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[ShortName] [nvarchar](255) NULL,
	[BriefNotation] [nvarchar](255) NULL,
	[Deleted] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_dim_Motivation]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_dim_Motivation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[ShortName] [nvarchar](255) NULL,
	[BriefNotation] [nvarchar](255) NULL,
	[Deleted] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_dim_Format]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_dim_Format](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[ShortName] [nvarchar](255) NULL,
	[BriefNotation] [nvarchar](255) NULL,
	[Deleted] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_dim_Channel]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_dim_Channel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[ShortName] [nvarchar](255) NULL,
	[BriefNotation] [nvarchar](255) NULL,
	[Deleted] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_SlaveGood]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_SlaveGood](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](32) NULL,
	[OrimiCode] [varchar](32) NULL,
	[Name] [nvarchar](512) NULL,
	[From] [tinyint] NULL,
	[Manufacturer] [varchar](128) NULL,
	[Brand] [varchar](128) NULL,
	[Category] [varchar](128) NULL,
	[Segment] [varchar](128) NULL,
	[TeaColor] [varchar](128) NULL,
	[Pack] [varchar](128) NULL,
	[BagQty] [int] NULL,
	[BagWeight] [money] NULL,
	[Weight] [money] NULL,
	[isWithFlavor] [tinyint] NULL,
	[isWithAdding] [tinyint] NULL,
	[PriceSegment] [varchar](128) NULL,
	[Country] [varchar](128) NULL,
	[DataSource] [varchar](32) NOT NULL,
	[DateChange] [datetime] NOT NULL,
	[isSynced] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Manufacturer]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Manufacturer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_InternalPack]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_InternalPack](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_GiftType]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_GiftType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Gift]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Gift](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Format]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Format](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Flavor]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Flavor](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_ExternalPack]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_ExternalPack](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Division]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Division](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Country]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Country](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_ContractorGroup]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_ContractorGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_CoffeeSort]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_CoffeeSort](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Channel]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Channel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Category]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Category](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Brand]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Brand](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ManufacturerID] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_Segment]    Script Date: 07/14/2014 19:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_Segment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NULL,
	[Name] [nvarchar](255) NOT NULL,
	[ShortName] [nvarchar](255) NULL,
	[BriefNotation] [nvarchar](255) NULL,
	[Deleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[lasmart_v_sync_Users]    Script Date: 07/14/2014 19:55:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_sync_Users]
as
SELECT [ID]
      ,[Name]
      ,[Role] as [RoleID]
      ,null as [Role]
      ,[FullName]
      ,[isDeleted]
      ,[EMail]
      ,[ContactName]
      ,[Tel1]
      ,[DateCreate]
      ,[DateBeginAccess]
      ,[DateEndAccess]
      ,[Password]
  FROM [Orimi_denis].[dbo].[lasmart_t_sync_Users]
GO
/****** Object:  View [dbo].[lasmart_v_sync_MasterRefLog_LastEvents]    Script Date: 07/14/2014 19:55:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
create view [dbo].[lasmart_v_sync_MasterRefLog_LastEvents]
as
select [GoodID]
      ,[Author]
      ,[DateUpdate]
from [Orimi_denis].[dbo].[lasmart_t_sync_MasterRefLog] with (nolock)
where ID in
	(SELECT max([ID])
	FROM [Orimi_denis].[dbo].[lasmart_t_sync_MasterRefLog] with (nolock)
	group by [GoodID])
GO
/****** Object:  View [dbo].[lasmart_v_dim_SyncedGood]    Script Date: 07/14/2014 19:55:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_dim_SyncedGood]
as
SELECT sg.[ID]
      ,sg.[Code]
      ,sg.[OrimiCode]
      ,sg.[Name]
      ,sg.[From]
      ,sg.[Manufacturer]
      ,sg.[Brand]
      ,sg.[Category]
      ,sg.[Segment]
      ,sg.[TeaColor]
      ,sg.[Pack]
      ,sg.[BagQty]
      ,sg.[BagWeight]
      ,sg.[Weight]
      ,sg.[isWithFlavor]
      ,sg.[isWithAdding]
      ,sg.[PriceSegment]
      ,sg.[Country]
      ,sg.[DataSource]
      ,convert(varchar,cast(sg.[DateChange] as date)) + ' ' + left(convert(varchar,cast(sg.[DateChange] as time)),5) as [DateChange]
      ,ss.Name as [SyncType]
  FROM [lasmart_t_dim_SlaveGood] sg
  left join [lasmart_t_sync_SyncStatus] ss on sg.isSynced = ss.ID
  where sg.isSynced <> 0
GO
/****** Object:  View [dbo].[lasmart_v_dim_SlaveGood]    Script Date: 07/14/2014 19:55:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_dim_SlaveGood]
as
SELECT [ID]
      ,[Code]
      ,[OrimiCode]
      ,[Name]
      ,[From]
      ,[Manufacturer]
      ,[Brand]
      ,[Category]
      ,[Segment]
      ,[TeaColor]
      ,[Pack]
      ,[BagQty]
      ,[BagWeight]
      ,[Weight]
      ,[isWithFlavor]
      ,[isWithAdding]
      ,[PriceSegment]
      ,[Country]
      ,[DataSource]
      ,convert(varchar,cast([DateChange] as date)) + ' ' + left(convert(varchar,cast([DateChange] as time)),5) as [DateChange]
  FROM [Orimi_denis].[dbo].[lasmart_t_dim_SlaveGood]
  where isSynced = 0
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_SlaveGood_GroupSync]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_SlaveGood_GroupSync]  (
	@UserID int
	,@ID varchar(max)
	,@MasterID int
)
AS
declare
@UserName varchar(128) = ''
,@query varchar(max) = ''
,@CurrID int
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON
	BEGIN TRANSACTION
	
	select @UserName = FullName from [lasmart_t_sync_Users] where ID = @UserID

	if LEN(@ID) > 0
	BEGIN
		WHILE LEN(@ID) > 0
		BEGIN
			IF PATINDEX('%,%',@ID) > 0
			BEGIN
				SET @CurrID = SUBSTRING(@ID, 0, PATINDEX('%,%',@ID))
				SET @ID = SUBSTRING(@ID, PATINDEX('%,%',@ID) + 1, LEN(@ID))
				SET @query = '
						insert [lasmart_t_sync_MasterSlave]
						([MasterGoodID]
						,[SlaveGoodID]
						,[Author])
						VALUES (
							'+convert(varchar,@MasterID)+'
							,'+convert(varchar,@CurrID)+'
							,'''+@UserName+'''
						)
								
						update [lasmart_t_dim_SlaveGood]
						set isSynced = 1
						where ID = '+convert(varchar,@CurrID)+'
						
						insert [lasmart_t_sync_MasterRefLog]
						([GoodID]
						,[Author]
						,[Event])
						VALUES (
							'+convert(varchar,@MasterID)+'
							,'''+@UserName+'''
							,''К позиции привязана позиция подчиненного справочника ''+ISNULL((select top 1 Name from lasmart_t_dim_SlaveGood with (nolock) where [ID] = '+convert(varchar,@CurrID)+'),''Не задано'')+'' (ID ''+ISNULL(CONVERT(varchar,'+convert(varchar,@CurrID)+'),''Не задано'')+'')''
						)
					'
				exec sp_sqlexec @query
			END
			ELSE
			BEGIN
				SET @CurrID = @ID
				SET @query = '
						insert [lasmart_t_sync_MasterSlave]
						([MasterGoodID]
						,[SlaveGoodID]
						,[Author])
						VALUES (
							'+convert(varchar,@MasterID)+'
							,'+convert(varchar,@CurrID)+'
							,'''+@UserName+'''
						)
								
						update [lasmart_t_dim_SlaveGood]
						set isSynced = 1
						where ID = '+convert(varchar,@CurrID)+'
						
						insert [lasmart_t_sync_MasterRefLog]
						([GoodID]
						,[Author]
						,[Event])
						VALUES (
							'+convert(varchar,@MasterID)+'
							,'''+@UserName+'''
							,''К позиции привязана позиция подчиненного справочника ''+ISNULL((select top 1 Name from lasmart_t_dim_SlaveGood with (nolock) where [ID] = '+convert(varchar,@CurrID)+'),''Не задано'')+'' (ID ''+ISNULL(CONVERT(varchar,'+convert(varchar,@CurrID)+'),''Не задано'')+'')''
						)
					'
				exec sp_sqlexec @query
				BREAK
			END
		END
	END
	
	COMMIT TRANSACTION
END
GO
/****** Object:  Table [dbo].[lasmart_t_dim_SubChannel]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_SubChannel](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ChannelID] [int] NULL,
	[Name] [nvarchar](255) NOT NULL,
	[ShortName] [nvarchar](255) NULL,
	[BriefNotation] [nvarchar](255) NULL,
	[Deleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_SubSegment]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_SubSegment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SegmentID] [int] NULL,
	[Name] [nvarchar](255) NOT NULL,
	[ShortName] [nvarchar](255) NULL,
	[BriefNotation] [nvarchar](255) NULL,
	[Deleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_SubBrand]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_SubBrand](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BrandID] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[ShortName] [nvarchar](32) NULL,
	[BriefNotation] [nvarchar](32) NULL,
	[Deleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC,
	[BrandID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_dim_MasterStores]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_MasterStores](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](32) NULL,
	[Name] [nvarchar](256) NULL,
	[TempAddress] [nvarchar](256) NULL,
	[PostCode] [int] NULL,
	[County] [nvarchar](128) NULL,
	[Region] [nvarchar](128) NULL,
	[District] [nvarchar](128) NULL,
	[City] [nvarchar](128) NULL,
	[Street] [nvarchar](128) NULL,
	[Building] [nvarchar](64) NULL,
	[FullAddress] [nvarchar](1024) NULL,
	[Address] [nvarchar](256) NULL,
	[SectorID] [int] NULL,
	[DivisionID] [int] NULL,
	[SubChannelID] [int] NULL,
	[ChannelID] [int] NULL,
	[FormatID] [int] NULL,
	[NetID] [int] NULL,
	[isNet] [tinyint] NULL,
	[NetTypeID] [int] NULL,
	[isNielsen] [tinyint] NULL,
	[ContractorGroupID] [int] NULL,
	[isSubDistributor] [tinyint] NULL,
	[DateOpen] [date] NULL,
	[DateClose] [date] NULL,
	[ShopArea] [money] NULL,
	[Coordinates] [nvarchar](32) NULL,
	[GLN] [nvarchar](32) NULL,
	[Comment] [nvarchar](1024) NULL,
	[CountryID] [int] NULL,
	[FIAS] [nvarchar](32) NULL,
	[isInRBP] [tinyint] NULL,
	[RespMerchID] [int] NULL,
	[RespTRID] [int] NULL,
	[Selection] [int] NULL,
	[StatusID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lasmart_t_dim_MasterGood]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lasmart_t_dim_MasterGood](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NULL,
	[Code] [varchar](32) NULL,
	[OrimiCode] [varchar](32) NULL,
	[Name] [nvarchar](512) NULL,
	[ShortName] [nvarchar](256) NULL,
	[TempName] [nvarchar](512) NULL,
	[From] [tinyint] NULL,
	[ManufacturerID] [int] NULL,
	[BrandID] [int] NULL,
	[SubBrandID] [int] NULL,
	[CategoryID] [int] NULL,
	[SegmentID] [int] NULL,
	[SubSegmentID] [int] NULL,
	[TeaColorID] [int] NULL,
	[ExternalPackID] [int] NULL,
	[SachetPackID] [int] NULL,
	[InternalPackID] [int] NULL,
	[BagQty] [int] NULL,
	[BagWeight] [money] NULL,
	[Weight] [money] NULL,
	[WeightGroupID] [int] NULL,
	[isWithFlavor] [tinyint] NULL,
	[isWithAdding] [tinyint] NULL,
	[isHorekaPack] [tinyint] NULL,
	[PriceSegmentID] [int] NULL,
	[CountryID] [int] NULL,
	[CoffeeSortID] [int] NULL,
	[isCaffeine] [tinyint] NULL,
	[GiftID] [int] NULL,
	[GiftTypeID] [int] NULL,
	[Photo] [varchar](1024) NULL,
	[Length] [int] NULL,
	[Width] [int] NULL,
	[Height] [int] NULL,
	[isWeightNet] [tinyint] NULL,
	[Comment] [nvarchar](1024) NULL,
	[Collection] [varchar](256) NULL,
	[isPrivateMark] [tinyint] NULL,
	[StatusID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_MasterStores_OnChange]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_MasterStores_OnChange]  (
	@ID int
)
AS
/*declare
@UserName varchar(24) = 'Система'*/
BEGIN
	SET XACT_ABORT ON
	BEGIN TRANSACTION
		
		--Update [lasmart_t_dim_MasterStores]
		--set 
		--where ID = @ID
		
		
		Update [lasmart_t_dim_MasterStores]
			set [FullAddress] = isnull(ms.Region,'')+' '+isnull(ms.District,'')+' '+isnull(ms.City,'')+' '+isnull(ms.City,'')+' '+isnull(ms.Building,'')+' '
				,[Address] = isnull(ms.Region,'')+' '+isnull(ms.District,'')+' ' 
		from [lasmart_t_dim_MasterStores] ms
		left join [lasmart_t_dim_Sector] sec on ms.SectorID = sec.ID
		left join [lasmart_t_dim_Division] div on ms.DivisionID = div.ID
		left join [lasmart_t_dim_SubChannel] sbch on ms.SubChannelID = sbch.ID
		left join [lasmart_t_dim_Channel] ch on ms.ChannelID = ch.ID
		left join [lasmart_t_dim_Format] f on ms.FormatID = f.ID
		left join [lasmart_t_dim_Net] ni on ms.NetID = ni.ID 
		left join [lasmart_t_dim_NetType] nti on ms.NetTypeID = nti.ID
		left join [lasmart_t_dim_ContractorGroup] cntg on ms.ContractorGroupID = cntg.ID
		left join [lasmart_t_dim_Country] cntr on ms.CountryID = cntr.ID
		where ms.ID = @ID
			
	COMMIT TRANSACTION
END
GO
/****** Object:  View [dbo].[lasmart_v_dim_MasterStores]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_dim_MasterStores]
as
SELECT 
	s.[ID]
	,s.[Code]
	,s.[Name]
	,s.[TempAddress]
	,s.[PostCode]
	,s.[County]
	,s.[Region]
	,s.[District]
	,s.[City]
	,s.[Street]
	,s.[Building]
	,s.[FullAddress]
	,s.[Address]
	,s.[SectorID]
		,sec.Name as [Sector]
	,s.[DivisionID]
		,div.Name as [Division]
	,s.[SubChannelID]
		,sbch.Name as [SubChannel]
	,s.[ChannelID]
		,ch.Name as [Channel]
	,s.[FormatID]
		,f.Name as [Format]
	,s.[NetID] 
		,ni.Name as [Net]
	,s.[isNet]
	,s.[NetTypeID]
		,nti.Name as [NetType]
	,s.[isNielsen]
	,s.[ContractorGroupID]
		,cntg.Name as [ContractorGroup]
	,s.[isSubDistributor]
	,s.[DateOpen]
	,s.[DateClose]
	,s.[ShopArea]
	,s.[Coordinates]
	,s.[GLN]
	,s.[Comment]
	,s.[CountryID]
		,cntr.Name as [Country]
	,s.[FIAS]
	,s.[isInRBP]
	,s.[RespMerchID]
		,resm.Name as [RespMerch]
	,s.[RespTRID]
		,rst.Name as [RespTR]
	,s.[Selection]
	,s.[StatusID]
	,rl.Author
	--,convert(varchar,cast([DateUpdate] as date)) + ' ' + left(convert(varchar,cast([DateUpdate] as time)),5) as [DateChange]
FROM [Orimi_denis].[dbo].[lasmart_t_dim_MasterStores] s
  left join lasmart_t_dim_Sector sec on s.SectorID = sec.ID
  left join lasmart_t_dim_Division div on s.DivisionID = div.ID
  left join lasmart_t_dim_SubChannel sbch on s.SubChannelID = sbch.ID
  left join lasmart_t_dim_Channel ch on s.ChannelID = ch.ID
  left join lasmart_t_dim_Format f on s.FormatID = f.ID
  left join lasmart_t_dim_Net ni on s.NetID = ni.ID 
  left join lasmart_t_dim_NetType nti on s.NetTypeID = nti.ID
  left join lasmart_t_dim_ContractorGroup cntg on s.ContractorGroupID = cntg.ID
  left join lasmart_t_dim_Country cntr on s.CountryID = cntr.ID
  left join lasmart_t_dim_RespMerch resm on s.RespMerchID = resm.ID
  left join lasmart_t_dim_RespTR rst on s.RespTRID = rst.ID
  left join lasmart_v_sync_MasterRefLog_LastEvents rl on s.[ID] = rl.GoodID
GO
/****** Object:  Table [dbo].[lasmart_t_good_flavor]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_good_flavor](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GoodID] [int] NULL,
	[FlavorID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lasmart_t_good_barcode]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lasmart_t_good_barcode](
	[ID] [varchar](16) NOT NULL,
	[GoodID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lasmart_t_good_adding]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_good_adding](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GoodID] [int] NULL,
	[AddingID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[lasmart_v_dim_ParentGood]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_dim_ParentGood]
as
SELECT 
	g.[ID]
	,g.[Code]
	,g.[Name]
	,g.[ShortName]
	,g.[TempName]
	,g.[From]
	,m.Name as [Manufacturer]
	,b.Name as [Brand]
	,sb.Name as [SubBrand]
	,c.Name as [Category]
	,s.Name as [Segment]
	,ss.Name as [SubSegment]
	,t.Name as [TeaColor]
	,ep.Name as [ExternalPack]
	,sp.Name as [SachetPack]
	,ip.Name as [InternalPack]
	,g.[BagQty]
	,g.[BagWeight]
	,g.[Weight]
	,wg.Name as [WeightGroup]
	,g.[isWithFlavor]
	,g.[isWithAdding]
	,g.[isHorekaPack]
	,ps.Name as [PriceSegment]
	,ctry.Name as [Country]
	,cs.Name as [CoffeeSort]
	,g.[isCaffeine]
	,gift.Name as [Gift]
	,gt.Name as [GiftType]
	,g.[Photo]
	,g.[Length]
	,g.[Width]
	,g.[Height]
	,g.[isWeightNet]
	,g.[Comment]
	,g.[Collection]
	,g.[isPrivateMark]

FROM [lasmart_t_dim_MasterGood] g
  left join lasmart_t_dim_Brand b on g.BrandID = b.ID
  left join lasmart_t_dim_Category c on g.CategoryID = c.ID
  left join lasmart_t_dim_CoffeeSort cs on g.CoffeeSortID = cs.ID
  left join lasmart_t_dim_Country ctry on g.CountryID = ctry.ID
  left join lasmart_t_dim_ExternalPack ep on g.ExternalPackID = ep.ID
  left join lasmart_t_dim_Gift gift on g.GiftID = gift.ID
  left join lasmart_t_dim_GiftType gt on g.GiftTypeID = gt.ID
  left join lasmart_t_dim_InternalPack ip on g.InternalPackID = ip.ID
  left join lasmart_t_dim_Manufacturer m on g.ManufacturerID = m.ID
  left join lasmart_t_dim_PriceSegment ps on g.PriceSegmentID = ps.ID
  left join lasmart_t_dim_SachetPack sp on g.SachetPackID = sp.ID
  left join lasmart_t_dim_Segment s on g.SegmentID = s.ID
  left join lasmart_t_dim_SubBrand sb on  g.SubBrandID = sb.ID
  left join lasmart_t_dim_Subsegment ss on  g.SubSegmentID = ss.ID
  left join lasmart_t_dim_TeaColor t on g.TeaColorID = t.ID
  left join lasmart_t_dim_WeightGroup wg on  g.WeightGroupID = wg.ID
  
where g.ParentID is null and g.StatusID = 2
GO
/****** Object:  Table [dbo].[lasmart_t_sync_MasterSlave]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lasmart_t_sync_MasterSlave](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MasterGoodID] [int] NOT NULL,
	[SlaveGoodID] [int] NOT NULL,
	[Author] [nvarchar](128) NOT NULL,
	[DateUpdate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_MasterStores_Update]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_MasterStores_Update]  (
	 @UserID int
	,@ID varchar(max)
	,@Name nvarchar (256)
	,@TempAddress nvarchar (256)
	,@PostCode int
	,@County nvarchar (128)
	,@Region nvarchar (128)
	,@District nvarchar (128)
	,@City nvarchar (128)
	,@Street nvarchar (128)
	,@Building nvarchar (64)
	,@FullAddress nvarchar (1024)
	,@Address nvarchar (256)
	,@SectorID int
	,@DivisionID int
	,@SubChannelID int
	,@ChannelID int
	,@FormatID int
	,@NetID int
	,@isNet tinyint
	,@NetTypeID int
	,@ContractorGroupID int
	,@isSubDistributor tinyint
	,@isNielsen tinyint
	,@DateOpen date
	,@DateClose date
	,@ShopArea money
	,@Coordinates nvarchar (32)
	,@GLN nvarchar (32)
	,@Comment nvarchar(1024)
	,@CountryID int
	,@FIAS nvarchar (32)
	,@isInRBP tinyint
	,@RespMerchID int
	,@RespTRID int
	,@Selection int
)
AS
declare
 @UserName varchar(128) = ''
,@old_Name nvarchar (256) 
,@old_TempAddress nvarchar (256)
,@old_PostCode int
,@old_County nvarchar (128)
,@old_Region nvarchar (128)
,@old_District nvarchar (128)
,@old_City nvarchar (128)
,@old_Street nvarchar (128)
,@old_Building nvarchar (64)
,@old_FullAddress nvarchar (1024)
,@old_Address nvarchar (256)
,@old_SectorID int
,@old_DivisionID int
,@old_SubChannelID int
,@old_ChannelID int
,@old_FormatID int
,@old_NetID int
,@old_isNet tinyint
,@old_NetTypeID int
,@old_isNielsen tinyint
,@old_ContractorGroupID int
,@old_isSubDistributor tinyint
,@old_DateOpen date
,@old_DateClose date
,@old_ShopArea money
,@old_Coordinates nvarchar (32)
,@old_GLN nvarchar (32)
,@old_Comment nvarchar (1024)
,@old_CountryID int
,@old_FIAS nvarchar (32)
,@old_isInRBP tinyint
,@old_RespMerchID int
,@old_RespTRID int
,@old_Selection int
BEGIN
	SET XACT_ABORT ON
	BEGIN TRANSACTION
	
	select @UserName = FullName from [lasmart_t_sync_Users] where ID = @UserID

	select @old_Name = isnull(Name,'')
		,@old_TempAddress = isnull(TempAddress,'')
		,@old_PostCode = isnull(PostCode,0)
		,@old_County = isnull(County,'')
		,@old_Region = isnull(Region,'')
		,@old_District = isnull(District,'')
		,@old_City = isnull(City,'')
		,@old_Street = isnull(Street,'')
		,@old_Building = isnull(Building,'')
		,@old_FullAddress = isnull(FullAddress,'')
		,@old_Address = isnull(Address,'')
		,@old_SectorID = isnull(SectorID,0)
		,@old_DivisionID = isnull(DivisionID,0)
		,@old_SubChannelID = isnull(SubChannelID,0)
		,@old_ChannelID = isnull(ChannelID,0)
		,@old_FormatID = isnull(FormatID,0)
		,@old_NetID = isnull(NetID,0)
		,@old_isNet = isnull(isNet,0)
		,@old_NetTypeID = isnull(NetTypeID,0)
		,@old_isNielsen = isnull(isNielsen,0)
		,@old_ContractorGroupID = isnull(ContractorGroupID,0)
		,@old_isSubDistributor = isnull(isSubDistributor,0)
		,@old_DateOpen = isnull(DateOpen,'')
		,@old_DateClose = isnull(DateClose,'')
		,@old_ShopArea = isnull(ShopArea,0)
		,@old_Coordinates = isnull(Coordinates,'')
		,@old_GLN = isnull(GLN,'')
		,@old_Comment = isnull(Comment,'')
		,@old_CountryID = isnull(CountryID,0)
		,@old_FIAS = isnull(FIAS,'')
		,@old_isInRBP = isnull(isInRBP,0)
		,@old_RespMerchID = isnull(RespMerchID,0)
		,@old_RespTRID = isnull(RespTRID,0)
		,@old_Selection = isnull(Selection,0)
		from [lasmart_t_dim_MasterStores] where ID = @ID
		
		if (@old_Name <> @Name)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Черновой адрес с '+ISNULL(convert(varchar,@old_Name),'Не задано')+' на '+ISNULL(convert(nvarchar,@Name),'Не задано')
			)
		end
		
		if (@old_TempAddress <> @TempAddress)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Черновой адрес с '+ISNULL(convert(varchar,@old_TempAddress),'Не задано')+' на '+ISNULL(convert(nvarchar,@TempAddress),'Не задано')
			)
		end
		
		if (@old_PostCode <> @PostCode)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Индекс с '+ISNULL(convert(varchar,@old_PostCode),'Не задано')+' на '+ISNULL(convert(varchar,@PostCode),'Не задано')
			)
		end
		
		if (@old_County <> @County)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Округ с '+ISNULL(convert(varchar,@old_County),'Не задано')+' на '+ISNULL(convert(varchar,@County),'Не задано')
			)
		end
		
		if (@old_Region <> @Region)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Регион с '+ISNULL(convert(varchar,@old_Region),'Не задано')+' на '+ISNULL(convert(varchar,@Region),'Не задано')
			)
		end
		
		if (@old_District <> @District)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Район с '+ISNULL(convert(varchar,@old_District),'Не задано')+' на '+ISNULL(convert(varchar,@District),'Не задано')
			)
		end
		
		if (@old_City <> @City)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Город с '+ISNULL(convert(varchar,@old_City),'Не задано')+' на '+ISNULL(convert(varchar,@City),'Не задано')
			)
		end
		
		if (@old_Street <> @Street)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Улица с '+ISNULL(convert(varchar,@old_Street),'Не задано')+' на '+ISNULL(convert(varchar,@Street),'Не задано')
			)
		end
		
		if (@old_Building <> @Building)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Дом с '+ISNULL(convert(varchar,@old_Building),'Не задано')+' на '+ISNULL(convert(varchar,@Building),'Не задано')
			)
		end
		
		if (@old_FullAddress <> @FullAddress)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Полный адрес с '+ISNULL(convert(varchar,@old_FullAddress),'Не задано')+' на '+ISNULL(convert(varchar,@FullAddress),'Не задано')
			)
		end
		
		if (@old_Address <> @Address)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Адрес с '+ISNULL(convert(varchar,@old_Address),'Не задано')+' на '+ISNULL(convert(varchar,@Address),'Не задано')
			)
		end
		
		if (@old_SectorID <> @SectorID)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Участок с '+ISNULL((select top 1 ShortName from lasmart_t_dim_Sector with(nolock) where ID = @old_SectorID),'Не задано')+' на '+ISNULL((select top 1 ShortName from lasmart_t_dim_Sector with(nolock) where ID = @SectorID),'Не задано')
			)
		end
		
		if (@old_DivisionID <> @DivisionID)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Дивизион с '+ISNULL((select top 1 ShortName from lasmart_t_dim_Division with(nolock) where ID = @old_DivisionID),'Не задано')+' на '+ISNULL((select top 1 ShortName from lasmart_t_dim_Division with(nolock) where ID = @DivisionID),'Не задано')
			)
		end
		
		if (@old_SubChannelID <> @SubChannelID)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Подканал с '+ISNULL((select top 1 ShortName from lasmart_t_dim_SubChannel with(nolock) where ID = @old_SubChannelID),'Не задано')+' на '+ISNULL((select top 1 ShortName from lasmart_t_dim_SubChannel with(nolock) where ID = @SubChannelID),'Не задано')
			)
		end
		
		if (@old_ChannelID <> @ChannelID)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Канал с '+ISNULL((select top 1 ShortName from lasmart_t_dim_Channel with(nolock) where ID = @old_ChannelID),'Не задано')+' на '+ISNULL((select top 1 ShortName from lasmart_t_dim_Channel with(nolock) where ID = @ChannelID),'Не задано')
			)
		end
		
		if (@old_FormatID <> @FormatID)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Формат с '+ISNULL((select top 1 ShortName from lasmart_t_dim_Format with(nolock) where ID = @old_FormatID),'Не задано')+' на '+ISNULL((select top 1 ShortName from lasmart_t_dim_Format with(nolock) where ID = @FormatID),'Не задано')
			)
		end
		
		if (@old_NetID <> @NetID)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Название сети с '+ISNULL((select top 1 ShortName from lasmart_t_dim_Net with(nolock) where ID = @old_NetID),'Не задано')+' на '+ISNULL((select top 1 ShortName from lasmart_t_dim_Net with(nolock) where ID = @NetID),'Не задано')
			)
		end
		
		if (@old_isNet <> @isNet)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Сеть с '+ISNULL(convert(varchar,@old_isNet),'Не задано')+' на '+ISNULL(convert(varchar,@isNet),'Не задано')
			)
		end
		
		if (@old_NetTypeID <> @NetTypeID)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Тип сети с '+ISNULL((select top 1 ShortName from lasmart_t_dim_NetType with(nolock) where ID = @old_NetTypeID),'Не задано')+' на '+ISNULL((select top 1 ShortName from lasmart_t_dim_NetType with(nolock) where ID = @NetTypeID),'Не задано')
			)
		end
		
		if (@old_isNielsen <> @isNielsen)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение КАИ Nielsen с '+ISNULL(convert(varchar,@old_isNielsen),'Не задано')+' на '+ISNULL(convert(varchar,@isNielsen),'Не задано')
			)
		end
		
		if (@old_ContractorGroupID <> @ContractorGroupID)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Группа контрагентов с '+ISNULL((select top 1 ShortName from lasmart_t_dim_ContractorGroup with(nolock) where ID = @old_ContractorGroupID),'Не задано')+' на '+ISNULL((select top 1 ShortName from lasmart_t_dim_ContractorGroup with(nolock) where ID = @ContractorGroupID),'Не задано')
			)
		end
		
		if (@old_isSubDistributor <> @isSubDistributor)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Сабдистрибьютор с '+ISNULL(convert(varchar,@old_isSubDistributor),'Не задано')+' на '+ISNULL(convert(varchar,@isSubDistributor),'Не задано')
			)
		end
		
		if (@old_DateOpen <> @DateOpen)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Дата открытия с '+ISNULL(convert(varchar,@old_DateOpen),'Не задано')+' на '+ISNULL(convert(varchar,@DateOpen),'Не задано')
			)
		end
		
		if (@old_DateClose <> @DateClose)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Дата закрытия с '+ISNULL(convert(varchar,@old_DateClose),'Не задано')+' на '+ISNULL(convert(varchar,@DateClose),'Не задано')
			)
		end
		
		if (@old_ShopArea <> @ShopArea)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Площадь ТТ с '+ISNULL(convert(varchar,@old_ShopArea),'Не задано')+' на '+ISNULL(convert(varchar,@ShopArea),'Не задано')
			)
		end
		
		if (@old_Coordinates <> @Coordinates)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Координаты с '+ISNULL(convert(varchar,@old_Coordinates),'Не задано')+' на '+ISNULL(convert(varchar,@Coordinates),'Не задано')
			)
		end
		
		if (@old_GLN <> @GLN)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение GLN с '+ISNULL(convert(varchar,@old_GLN),'Не задано')+' на '+ISNULL(convert(varchar,@GLN),'Не задано')
			)
		end
		
		if (@old_Comment <> @Comment)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Комментарий с '+ISNULL(convert(varchar,@old_Comment),'Не задано')+' на '+ISNULL(convert(varchar,@Comment),'Не задано')
			)
		end
		
		if (@old_CountryID <> @CountryID)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Страна с '+ISNULL((select top 1 ShortName from lasmart_t_dim_Country with(nolock) where ID = @old_CountryID),'Не задано')+' на '+ISNULL((select top 1 ShortName from lasmart_t_dim_Country with(nolock) where ID = @CountryID),'Не задано')
			)
		end
		
		if (@old_FIAS <> @FIAS)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение ФиАС с '+ISNULL(convert(varchar,@old_FIAS),'Не задано')+' на '+ISNULL(convert(varchar,@FIAS),'Не задано')
			)
		end
		
		if (@old_isInRBP <> @isInRBP)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Участвует в РБП с '+ISNULL(convert(varchar,@old_isInRBP),'Не задано')+' на '+ISNULL(convert(varchar,@isInRBP),'Не задано')
			)
		end
		
		if (@old_RespMerchID <> @RespMerchID)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Ответственный мерчандайзер с '+ISNULL((select top 1 ShortName from lasmart_t_dim_RespMerch with(nolock) where ID = @old_RespMerchID),'Не задано')+' на '+ISNULL((select top 1 ShortName from lasmart_t_dim_RespMerch with(nolock) where ID = @RespMerchID),'Не задано')
			)
		end
		
		
		if (@old_RespTRID <> @RespTRID)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Ответственный ТП с '+ISNULL((select top 1 ShortName from lasmart_t_dim_RespTR with(nolock) where ID = @old_RespTRID),'Не задано')+' на '+ISNULL((select top 1 ShortName from lasmart_t_dim_RespTR with(nolock) where ID = @RespTRID),'Не задано')
			)
		end
		
		if (@old_Selection <> @Selection)
		begin
			insert [lasmart_t_sync_MasterRefLog]
			([GoodID]
			,[Author]
			,[Event])
			VALUES (
				@ID
				,@UserName
				,'Обновлено значение Выборка с '+ISNULL(convert(varchar,@old_Selection),'Не задано')+' на '+ISNULL(convert(varchar,@Selection),'Не задано')
			)
		end
						
		update [lasmart_t_dim_MasterStores]
			set  [Name] = @Name
			,[TempAddress] = @TempAddress
			,[PostCode] = @PostCode
			,[County] = @County
			,[Region] = @Region 
			,[District] = @District
			,[City] = @City 
			,[Street] = @Street
			,[Building] = @Building
			,[FullAddress] = @FullAddress 
			,[Address] = @Address 
			,[SectorID] = @SectorID 
			,[DivisionID] = @DivisionID 
			,[SubChannelID] = @SubChannelID 
			,[ChannelID] = @ChannelID
			,[FormatID] = @FormatID 
			,[NetID] = @NetID
			,[isNet] = @isNet 
			,[NetTypeID] = @NetTypeID 
			,[isNielsen] = @isNielsen
			,[ContractorGroupID] = @ContractorGroupID  
			,[isSubDistributor] = @isSubDistributor 
			,[DateOpen] = @DateOpen
			,[DateClose] = @DateClose
			,[ShopArea] = @ShopArea 
			,[Coordinates] = @Coordinates 
			,[GLN] = @GLN  
			,[Comment] = @Comment
			,[CountryID] = @CountryID 
			,[FIAS] = @FIAS 
			,[isInRBP] = @isInRBP
			,[RespMerchID] = @RespMerchID 
			,[RespTRID] = @RespTRID 
			,[Selection] = @Selection
			where ID = @ID
		
		exec [lasmart_p_MasterStores_OnChange] @ID = @ID
			
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_MasterGood_OnChange]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_MasterGood_OnChange]  (
	@ID int
)
AS
declare
@UserName varchar(24) = 'Система'
,@Weight money
,@old_Weight money
,@Name varchar(512)
,@old_Name varchar(512)
,@ShortName varchar(256)
,@old_ShortName varchar(256)
BEGIN
	SET XACT_ABORT ON
	BEGIN TRANSACTION
		
		select
			@old_Weight = isnull(Weight,0) 
			,@old_Name = isnull(Name,'') 
			,@old_ShortName = isnull(ShortName,'') 
		from [lasmart_t_dim_MasterGood] where ID = @ID
		
		select
			@Weight = BagQty*BagWeight 
			,@ShortName = case when isnull(mg.isHorekaPack,0) <> 0 then 'HRC: ' else '' end + isnull(b.BriefNotation,'')+' '+isnull(sb.BriefNotation,'')+' '+case when isnull(mg.BagQty,1) <> 1 then '('+convert(varchar,isnull(mg.[BagWeight],0))+'гx'+convert(varchar,isnull(mg.BagQty,0))+'п) ' else convert(varchar,isnull(mg.[Weight],0)) end+' '+isnull(ep.BriefNotation,'')+' '+isnull(ip.BriefNotation,'')
			,@Name = case when isnull(mg.isHorekaPack,0) <> 0 then 'HRC: ' else '' end + isnull(b.Name,'')+' '+isnull(sb.Name,'')+' '+case when isnull(mg.BagQty,1) <> 1 then '('+convert(varchar,isnull(mg.[BagWeight],0))+'гx'+convert(varchar,isnull(mg.BagQty,0))+'п) ' else convert(varchar,isnull(mg.[Weight],0)) end+' '+isnull(ep.Name,'')+' '+isnull(ip.Name,'')
		from [lasmart_t_dim_MasterGood] mg
		left join [lasmart_t_dim_Brand] b on mg.BrandID = b.ID
		left join [lasmart_t_dim_SubBrand] sb on mg.SubBrandID = sb.ID
		left join [lasmart_t_dim_ExternalPack] ep on mg.ExternalPackID = ep.ID
		left join [lasmart_t_dim_InternalPack] ip on mg.InternalPackID = ip.ID
		where mg.ID = @ID
		
		exec lasmart_p_LogNonIDAttributeChange @ID, @UserName, 'Вес (общий)'		, @Weight		, @old_Weight
		exec lasmart_p_LogNonIDAttributeChange @ID, @UserName, 'Короткое название'	, @ShortName	, @old_ShortName
		exec lasmart_p_LogNonIDAttributeChange @ID, @UserName, 'Полное название'	, @Name			, @old_Name
		
		update [lasmart_t_dim_MasterGood]
		set [Weight] = @Weight
			,[ShortName] = @ShortName
			,[Name] = @Name
		where ID = @ID
			
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_MasterGood_Update]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_MasterGood_Update]  (
	@UserID int
	,@ID varchar(max)
	,@ParentID int
	,@ManufacturerID int
	,@BrandID int
	,@SubBrandID int
	,@CategoryID int
	,@SegmentID int
	,@SubSegmentID int
	,@TeaColorID int
	,@ExternalPackID int
	,@SachetPackID int
	,@InternalPackID int
	,@BagWeight money
	,@BagQty int
	,@isWeightNet tinyint
	,@isHorekaPack tinyint
	,@GiftID int
	,@GiftTypeID int
	,@isPrivateMark tinyint
	,@WeightGroupID int
	,@isWithAdding tinyint
	,@isWithFlavor tinyint
	,@Length int
	,@Width int
	,@Height int
	,@isCaffeine tinyint
	,@PriceSegmentID int
	,@CountryID int
	,@CoffeeSortID int
	
	,@Adding varchar(1024)
	,@Flavor varchar(1024)
	,@Barcode varchar(1024)
	,@Photo varchar(1024)
	,@Collection varchar(256)
	,@TempName nvarchar(512)
	,@Comment nvarchar(1024)
)
AS
declare
@query varchar(max) = ''
,@UserName varchar(128) = ''
,@old_ParentID int
,@old_ManufacturerID int
,@old_BrandID int
,@old_SubBrandID int
,@old_CategoryID int
,@old_SegmentID int
,@old_SubSegmentID int
,@old_TeaColorID int
,@old_ExternalPackID int
,@old_SachetPackID int
,@old_InternalPackID int
,@old_BagWeight money
,@old_BagQty int
,@old_isWeightNet tinyint
,@old_isHorekaPack tinyint
,@old_GiftID int
,@old_GiftTypeID int
,@old_isPrivateMark tinyint
,@old_WeightGroupID int
,@old_isWithAdding tinyint
,@old_isWithFlavor tinyint
,@old_Length int
,@old_Width int
,@old_Height int
,@old_isCaffeine tinyint
,@old_PriceSegmentID int
,@old_CountryID int
,@old_CoffeeSortID int

,@old_Photo varchar(1024)
,@old_Collection varchar(256)
,@old_TempName nvarchar(512)
,@old_Comment nvarchar(1024)
,@i int
,@j int
BEGIN
	SET XACT_ABORT ON
	BEGIN TRANSACTION
	
	select @UserName = FullName from [lasmart_t_sync_Users] where ID = @UserID
	
	SELECT @i=MIN([Item]),@j=MAX([Item]) FROM dbo.lasmart_f_splitString (@ID)
	while @i<=@j
	begin
		select @old_ParentID = isnull(ParentID,0)
			,@old_ManufacturerID = isnull(ManufacturerID,0)
			,@old_BrandID = isnull(BrandID,0)
			,@old_SubBrandID = isnull(SubBrandID,0)
			,@old_CategoryID = isnull(CategoryID,0)
			,@old_SegmentID = isnull(SegmentID,0)
			,@old_SubSegmentID = isnull(SubSegmentID,0)
			,@old_TeaColorID = isnull(TeaColorID,0)
			,@old_ExternalPackID = isnull(ExternalPackID,0)
			,@old_SachetPackID = isnull(SachetPackID,0)
			,@old_InternalPackID = isnull(InternalPackID,0)
			,@old_BagWeight = isnull(BagWeight,0)
			,@old_BagQty = isnull(BagQty,0)
			,@old_isWeightNet = isnull(isWeightNet,0)
			,@old_isHorekaPack = isnull(isHorekaPack,0)
			,@old_GiftID = isnull(GiftID,0)
			,@old_GiftTypeID = isnull(GiftTypeID,0)
			,@old_isPrivateMark = isnull(isPrivateMark,0)
			,@old_WeightGroupID = isnull(WeightGroupID,0)
			,@old_isWithAdding = isnull(isWithAdding,0)
			,@old_isWithFlavor = isnull(isWithFlavor,0)
			,@old_Length = isnull(Length,0)
			,@old_Width = isnull(Width,0)
			,@old_Height = isnull(Height,0)
			,@old_isCaffeine = isnull(isCaffeine,0)
			,@old_PriceSegmentID = isnull(PriceSegmentID,0)
			,@old_CountryID = isnull(CountryID,0)
			,@old_CoffeeSortID = isnull(CoffeeSortID,0)
			
			,@old_Photo = isnull(Photo,'')
			,@old_Collection = isnull(Collection,'')
			,@old_TempName = isnull(TempName,'')
			,@old_Comment = isnull(Comment,'')
		from [lasmart_t_dim_MasterGood] where ID = @i
			
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Агрегирующая позиция'	,'lasmart_t_dim_MasterGood'		, @ParentID			, @old_ParentID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Производитель'			,'lasmart_t_dim_Manufacturer'	, @ManufacturerID	, @old_ManufacturerID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Бренд'					,'lasmart_t_dim_Brand'			, @BrandID			, @old_BrandID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Саббренд'				,'lasmart_t_dim_SubBrand'		, @SubBrandID		, @old_SubBrandID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Категория'				,'lasmart_t_dim_Category'		, @CategoryID		, @old_CategoryID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Сегмент'				,'lasmart_t_dim_Segment'		, @SegmentID		, @old_SegmentID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Сабсегмент'				,'lasmart_t_dim_SubSegment'		, @SubSegmentID		, @old_SubSegmentID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Цвет чая'				,'lasmart_t_dim_TeaColor'		, @TeaColorID		, @old_TeaColorID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Тип упаковки внешней'	,'lasmart_t_dim_ExternalPack'	, @ExternalPackID	, @old_ExternalPackID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Тип упаковки саше'		,'lasmart_t_dim_SachetPack'		, @SachetPackID		, @old_SachetPackID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Тип упаковки внутренней','lasmart_t_dim_InternalPack'	, @InternalPackID	, @old_InternalPackID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Ценовой сегмент'		,'lasmart_t_dim_PriceSegment'	, @PriceSegmentID	, @old_PriceSegmentID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Страна'					,'lasmart_t_dim_Country'		, @CountryID		, @old_CountryID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Сорт кофе'				,'lasmart_t_dim_CoffeeSort'		, @CoffeeSortID		, @old_CoffeeSortID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Акционная позиция'		,'lasmart_t_dim_Gift'			, @GiftID			, @old_GiftID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Тип подарка'			,'lasmart_t_dim_GiftType'		, @GiftTypeID		, @old_GiftTypeID
		exec lasmart_p_LogIDAttributeChange @i, @UserName, 'Вес (группа)'			,'lasmart_t_dim_WeightGroup'	, @WeightGroupID	, @old_WeightGroupID
		
		exec lasmart_p_LogNonIDAttributeChange @i, @UserName, 'Вес пакета'				, @BagWeight		, @old_BagWeight
		exec lasmart_p_LogNonIDAttributeChange @i, @UserName, 'Количество пакетов'		, @BagQty			, @old_BagQty
		exec lasmart_p_LogNonIDAttributeChange @i, @UserName, 'Весовой продукт в сети'	, @isWeightNet		, @old_isWeightNet
		exec lasmart_p_LogNonIDAttributeChange @i, @UserName, 'Упаковка Хорека'			, @isHorekaPack		, @old_isHorekaPack
		exec lasmart_p_LogNonIDAttributeChange @i, @UserName, 'Частная марка сети'		, @isPrivateMark	, @old_isPrivateMark
		exec lasmart_p_LogNonIDAttributeChange @i, @UserName, 'Добавка'					, @isWithAdding		, @old_isWithAdding
		exec lasmart_p_LogNonIDAttributeChange @i, @UserName, 'Аромат'					, @isWithFlavor		, @old_isWithFlavor
		exec lasmart_p_LogNonIDAttributeChange @i, @UserName, 'Длина'					, @Length			, @old_Length
		exec lasmart_p_LogNonIDAttributeChange @i, @UserName, 'Ширина'					, @Width			, @old_Width
		exec lasmart_p_LogNonIDAttributeChange @i, @UserName, 'Высота'					, @Height			, @old_Height
		exec lasmart_p_LogNonIDAttributeChange @i, @UserName, 'Кофеин'					, @isCaffeine		, @old_isCaffeine
		exec lasmart_p_LogNonIDAttributeChange @i, @UserName, 'Фото'					, @Photo			, @old_Photo
		exec lasmart_p_LogNonIDAttributeChange @i, @UserName, 'Коллекция'				, @Collection		, @old_Collection
		exec lasmart_p_LogNonIDAttributeChange @i, @UserName, 'Черновое название'		, @TempName			, @old_TempName
		exec lasmart_p_LogNonIDAttributeChange @i, @UserName, 'Комментарий'				, @Comment			, @old_Comment
		
		
		set @query = 'update [lasmart_t_dim_MasterGood] set '
		if (@ParentID is not null)
			set @query = @query + ' [ParentID] = '+convert(varchar,@ParentID)+', '
		if (@ManufacturerID is not null)
			set @query = @query + ' [ManufacturerID] = '+convert(varchar,@ManufacturerID)+', '
		if (@BrandID is not null)
			set @query = @query + ' [BrandID] = '+convert(varchar,@BrandID)+', '
		if (@SubBrandID is not null)
			set @query = @query + ' [SubBrandID] = '+convert(varchar,@SubBrandID)+', '
		if (@CategoryID is not null)
			set @query = @query + ' [CategoryID] = '+convert(varchar,@CategoryID)+', '
		if (@SegmentID is not null)
			set @query = @query + ' [SegmentID] = '+convert(varchar,@SegmentID)+', '
		if (@SubSegmentID is not null)
			set @query = @query + ' [SubSegmentID] = '+convert(varchar,@SubSegmentID)+', '
		if (@TeaColorID is not null)
			set @query = @query + ' [TeaColorID] = '+convert(varchar,@TeaColorID)+', '
		if (@ExternalPackID is not null)
			set @query = @query + ' [ExternalPackID] = '+convert(varchar,@ExternalPackID)+', '
		if (@SachetPackID is not null)
			set @query = @query + ' [SachetPackID] = '+convert(varchar,@SachetPackID)+', '
		if (@InternalPackID is not null)
			set @query = @query + ' [InternalPackID] = '+convert(varchar,@InternalPackID)+', '
		if (@BagWeight is not null)
			set @query = @query + ' [BagWeight] = '+convert(varchar,@BagWeight)+', '
		if (@BagQty is not null)
			set @query = @query + ' [BagQty] = '+convert(varchar,@BagQty)+', '
		if (@isWeightNet is not null)
			set @query = @query + ' [isWeightNet] = '+convert(varchar,@isWeightNet)+', '
		if (@isHorekaPack is not null)
			set @query = @query + ' [isHorekaPack] = '+convert(varchar,@isHorekaPack)+', '
		if (@GiftID is not null)
			set @query = @query + ' [GiftID] = '+convert(varchar,@GiftID)+', '
		if (@GiftTypeID is not null)
			set @query = @query + ' [GiftTypeID] = '+convert(varchar,@GiftTypeID)+', '
		if (@isPrivateMark is not null)
			set @query = @query + ' [isPrivateMark] = '+convert(varchar,@isPrivateMark)+', '
		if (@WeightGroupID is not null)
			set @query = @query + ' [WeightGroupID] = '+convert(varchar,@WeightGroupID)+', '
		if (@isWithAdding is not null)
			set @query = @query + ' [isWithAdding] = '+convert(varchar,@isWithAdding)+', '
		if (@isWithFlavor is not null)
			set @query = @query + ' [isWithFlavor] = '+convert(varchar,@isWithFlavor)+', '
		if (@Length is not null)
			set @query = @query + ' [Length] = '+convert(varchar,@Length)+', '
		if (@Width is not null)
			set @query = @query + ' [Width] = '+convert(varchar,@Width)+', '
		if (@Height is not null)
			set @query = @query + ' [Height] = '+convert(varchar,@Height)+', '
		if (@isCaffeine is not null)
			set @query = @query + ' [isCaffeine] = '+convert(varchar,@isCaffeine)+', '
		if (@PriceSegmentID is not null)
			set @query = @query + ' [PriceSegmentID] = '+convert(varchar,@PriceSegmentID)+', '
		if (@CountryID is not null)
			set @query = @query + ' [CountryID] = '+convert(varchar,@CountryID)+', '
		if (@CoffeeSortID is not null)
			set @query = @query + ' [CoffeeSortID] = '+convert(varchar,@CoffeeSortID)+', '
		if (@Photo is not null)
			set @query = @query + ' [Photo] = '''+convert(varchar(max),@Photo)+''', '
		if (@Collection is not null)
			set @query = @query + ' [Collection] = '''+convert(varchar(max),@Collection)+''', '
		if (@TempName is not null)
			set @query = @query + ' [TempName] = '''+convert(varchar(max),@TempName)+''', '
		if (@Comment is not null)
			set @query = @query + ' [Comment] = '''+convert(varchar(max),@Comment)+''', '
		set @query = substring(@query,1,LEN(@query)-1) + ' where ID = '+convert(varchar,@i)
		
		print @query
		exec (@query)
		
		
		--Ароматы, надо доделать логирование
		delete lasmart_t_good_adding where GoodID = @i
		if (LEN(@Adding) > 0)
		begin
			insert lasmart_t_good_adding
			([GoodID]
			,[AddingID])
			select @i, [Item] from dbo.lasmart_f_splitString(@Adding)
		end
		
		--Добавки, надо доделать логирование
		delete lasmart_t_good_flavor where GoodID = @i
		if (LEN(@Flavor) > 0)
		begin
			insert lasmart_t_good_flavor
			([GoodID]
			,[FlavorID])
			select @i, [Item] from dbo.lasmart_f_splitString(@Flavor)
		end
		
		--Штрихкод, надо доделать логирование
		delete lasmart_t_good_barcode where GoodID = @i
		if (LEN(@Barcode) > 0)
		begin
			insert lasmart_t_good_barcode
			([GoodID]
			,[ID])
			select @i, [Item] from dbo.lasmart_f_splitString(@Barcode)
		end
		
		exec [lasmart_p_MasterGood_OnChange] @ID = @i
		
		SELECT @i=MIN([Item]) FROM dbo.lasmart_f_splitString (@ID) WHERE [Item]>@i
	end	--while	
	COMMIT TRANSACTION
END
GO
/****** Object:  UserDefinedFunction [dbo].[lasmart_f_getFlavorsID]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[lasmart_f_getFlavorsID] ( @GoodID int)
RETURNS varchar(512)
AS
BEGIN

 DECLARE @result VARCHAR(512) = ''
 
 SELECT @result = @result + convert(varchar,a.ID) + ','
 from 
 lasmart_t_good_flavor gf with (nolock)
 left join lasmart_t_dim_Flavor a with (nolock) on gf.FlavorID = a.ID
 where gf.GoodID = @GoodID 
 order by a.Name
 
 if (LEN(@result) > 0)
	set @result = left(@result,len(@result)-1)
 RETURN rtrim(@result)
END
GO
/****** Object:  UserDefinedFunction [dbo].[lasmart_f_getFlavors]    Script Date: 07/14/2014 19:55:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[lasmart_f_getFlavors] ( @GoodID int)
RETURNS varchar(512)
AS
BEGIN

 DECLARE @result VARCHAR(512) = ''
 
 SELECT @result = @result + a.Name + ','
 from 
 lasmart_t_good_flavor gf with (nolock)
 left join lasmart_t_dim_Flavor a with (nolock) on gf.FlavorID = a.ID
 where gf.GoodID = @GoodID 
 order by a.Name
 
 if (LEN(@result) > 0)
	set @result = left(@result,len(@result)-1)
 RETURN rtrim(@result)
END
GO
/****** Object:  UserDefinedFunction [dbo].[lasmart_f_getBarcodes]    Script Date: 07/14/2014 19:55:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[lasmart_f_getBarcodes] ( @GoodID int)
RETURNS varchar(512)
AS
BEGIN

 DECLARE @result VARCHAR(512) = ''
 
 SELECT @result = @result + gb.ID + ','
 from lasmart_t_good_barcode gb with (nolock)
 where gb.GoodID = @GoodID
 order by gb.ID

 if (LEN(@result) > 0)
	set @result = left(@result,len(@result)-1)
 RETURN @result
END
GO
/****** Object:  UserDefinedFunction [dbo].[lasmart_f_getAddingsID]    Script Date: 07/14/2014 19:55:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[lasmart_f_getAddingsID] ( @GoodID int)
RETURNS varchar(512)
AS
BEGIN

 DECLARE @result VARCHAR(512) = ''
 
 SELECT @result = @result + convert(varchar,a.ID) + ','
 from 
 lasmart_t_good_adding ga with (nolock)
 left join lasmart_t_dim_Adding a with (nolock) on ga.AddingID = a.ID
 where ga.GoodID = @GoodID
 order by a.Name

 if (LEN(@result) > 0)
	set @result = left(@result,len(@result)-1)
 RETURN @result
END
GO
/****** Object:  UserDefinedFunction [dbo].[lasmart_f_getAddings]    Script Date: 07/14/2014 19:55:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[lasmart_f_getAddings] ( @GoodID int)
RETURNS varchar(512)
AS
BEGIN

 DECLARE @result VARCHAR(512) = ''
 
 SELECT @result = @result + a.Name + ','
 from 
 lasmart_t_good_adding ga with (nolock)
 left join lasmart_t_dim_Adding a with (nolock) on ga.AddingID = a.ID
 where ga.GoodID = @GoodID
 order by a.Name

 if (LEN(@result) > 0)
	set @result = left(@result,len(@result)-1)
 RETURN @result
END
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_SlaveGood_Unsync]    Script Date: 07/14/2014 19:55:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_SlaveGood_Unsync]  (
	@UserID int
	,@ID int
)
AS
declare
@UserName varchar(128) = ''
,@MasterID int
BEGIN
	SET XACT_ABORT ON
	BEGIN TRANSACTION
	
	select @UserName = FullName from [lasmart_t_sync_Users] where ID = @UserID
	select @MasterID = [MasterGoodID] from [lasmart_t_sync_MasterSlave] where [SlaveGoodID] = @ID
	
	delete [lasmart_t_sync_MasterSlave] where [SlaveGoodID] = @ID
			
	update [lasmart_t_dim_SlaveGood]
	set isSynced = 0
	where ID = @ID
	
	insert [lasmart_t_sync_MasterRefLog]
	([GoodID]
	,[Author]
	,[Event])
	VALUES (
		@MasterID
		,@UserName
		,'От позиции отвязана позиция подчиненного справочника '+ISNULL((select top 1 Name from lasmart_t_dim_SlaveGood with (nolock) where [ID] = @ID),'Не задано')+' (ID '+ISNULL(CONVERT(varchar,@ID),'Не задано')+')'
	)
			
	COMMIT TRANSACTION
END
GO
/****** Object:  StoredProcedure [dbo].[lasmart_p_SlaveGood_Sync]    Script Date: 07/14/2014 19:55:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[lasmart_p_SlaveGood_Sync]  (
	@UserID int
	,@ID int
	,@MasterID int
)
AS
declare
@UserName varchar(128) = ''
BEGIN
	SET XACT_ABORT ON
	BEGIN TRANSACTION
	
	select @UserName = FullName from [lasmart_t_sync_Users] where ID = @UserID

	insert [lasmart_t_sync_MasterSlave]
	([MasterGoodID]
	,[SlaveGoodID]
	,[Author])
    VALUES (
		@MasterID
		,@ID
		,@UserName
    )
			
	update [lasmart_t_dim_SlaveGood]
	set isSynced = 1
	where ID = @ID
	
	insert [lasmart_t_sync_MasterRefLog]
	([GoodID]
	,[Author]
	,[Event])
	VALUES (
		@MasterID
		,@UserName
		,'К позиции привязана позиция подчиненного справочника '+ISNULL((select top 1 Name from lasmart_t_dim_SlaveGood with (nolock) where [ID] = @ID),'Не задано')+' (ID '+ISNULL(CONVERT(varchar,@ID),'Не задано')+')'
	)
			
	COMMIT TRANSACTION
END
GO
/****** Object:  View [dbo].[lasmart_v_dim_MasterGood]    Script Date: 07/14/2014 19:55:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [dbo].[lasmart_v_dim_MasterGood]
as
SELECT 
	g.[ID]
	,g.[ParentID]
		,parg.[ShortName] as [Parent]
	,g.[Code]
	,g.[OrimiCode]
	,g.[Name]
	,g.[ShortName]
	,g.[TempName]
	,g.[From]
	,g.[ManufacturerID]
		,m.Name as [Manufacturer]
	,g.[BrandID]
		,b.Name as [Brand]
	,g.[SubBrandID]
		,sb.Name as [SubBrand]
	,g.[CategoryID]
		,c.Name as [Category]
	,g.[SegmentID]
		,s.Name as [Segment]
	,g.[SubSegmentID]
		,ss.Name as [SubSegment]
	,g.[TeaColorID]
		,t.Name as [TeaColor]
	,g.[ExternalPackID]
		,ep.Name as [ExternalPack]
	,g.[SachetPackID]
		,sp.Name as [SachetPack]
	,g.[InternalPackID]
		,ip.Name as [InternalPack]
	,g.[BagQty]
	,g.[BagWeight]
	,g.[Weight]
	,g.[WeightGroupID]
		,wg.Name as [WeightGroup]
	,g.[isWithFlavor]
	,g.[isWithAdding]
	,g.[isHorekaPack]
	,g.[PriceSegmentID]
		,ps.Name as [PriceSegment]
	,g.[CountryID]
		,ctry.Name as [Country]
	,g.[CoffeeSortID]
		,cs.Name as [CoffeeSort]
	,g.[isCaffeine]
	,g.[GiftID]
		,gift.Name as [Gift]
	,g.[GiftTypeID]
		,gt.Name as [GiftType]
	,g.[Photo]
	,g.[Length]
	,g.[Width]
	,g.[Height]
	,g.[isWeightNet]
	,g.[Comment]
	,g.[Collection]
	,g.[isPrivateMark]
	,dbo.lasmart_f_getAddings(g.[ID]) as Adding
	,dbo.lasmart_f_getAddingsID(g.[ID]) as AddingID
	,dbo.lasmart_f_getFlavors(g.[ID]) as Flavor
	,dbo.lasmart_f_getFlavorsID(g.[ID]) as FlavorID
	,dbo.lasmart_f_getBarcodes(g.[ID]) as Barcode
	,g.[StatusID]
		,stat.Name as [Status]
	
	,rl.Author
	,convert(varchar,cast([DateUpdate] as date)) + ' ' + left(convert(varchar,cast([DateUpdate] as time)),5) as [DateChange]
FROM [lasmart_t_dim_MasterGood] g
  left join [lasmart_t_dim_MasterGood] parg on g.ParentID = parg.ID
  --Adding
  left join lasmart_t_dim_Brand b on g.BrandID = b.ID
  left join lasmart_t_dim_Category c on g.CategoryID = c.ID
  left join lasmart_t_dim_CoffeeSort cs on g.CoffeeSortID = cs.ID
  left join lasmart_t_dim_Country ctry on g.CountryID = ctry.ID
  left join lasmart_t_dim_ExternalPack ep on g.ExternalPackID = ep.ID
  --Flavor
  left join lasmart_t_dim_Gift gift on g.GiftID = gift.ID
  left join lasmart_t_dim_GiftType gt on g.GiftTypeID = gt.ID
  left join lasmart_t_dim_InternalPack ip on g.InternalPackID = ip.ID
  left join lasmart_t_dim_Manufacturer m on g.ManufacturerID = m.ID
  left join lasmart_t_dim_PriceSegment ps on g.PriceSegmentID = ps.ID
  left join lasmart_t_dim_SachetPack sp on g.SachetPackID = sp.ID
  left join lasmart_t_dim_Segment s on g.SegmentID = s.ID
  left join lasmart_t_dim_SubBrand sb on  g.SubBrandID = sb.ID
  left join lasmart_t_dim_Subsegment ss on  g.SubSegmentID = ss.ID
  left join lasmart_t_dim_TeaColor t on g.TeaColorID = t.ID
  left join lasmart_t_dim_WeightGroup wg on  g.WeightGroupID = wg.ID
  left join lasmart_t_sync_PositionStatus stat on  g.StatusID = stat.ID
  left join lasmart_v_sync_MasterRefLog_LastEvents rl on g.[ID] = rl.GoodID
GO
/****** Object:  Default [DF__lasmart_t__DateU__3D7E1B63]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_sync_MasterRefLog] ADD  DEFAULT (getdate()) FOR [DateUpdate]
GO
/****** Object:  Default [DF__lasmart_t__Delet__3EA749C6]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_dim_WeightGroup] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__30592A6F]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_dim_TeaColor] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__isDel__351DDF8C]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_sync_Users] ADD  DEFAULT ((0)) FOR [isDeleted]
GO
/****** Object:  Default [DF__lasmart_t__DateC__361203C5]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_sync_Users] ADD  DEFAULT (getdate()) FOR [DateCreate]
GO
/****** Object:  Default [DF__lasmart_t__admin__370627FE]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_sync_Users] ADD  DEFAULT ((0)) FOR [admin]
GO
/****** Object:  Default [DF__lasmart_t__Delet__5832119F]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_sync_SyncStatus] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__436BFEE3]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_sync_PositionStatus] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__32767D0B]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Adding] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__sync_temp__DateC__16CE6296]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[sync_template] ADD  DEFAULT (getdate()) FOR [DateCreate]
GO
/****** Object:  Default [DF__lasmart_t__Delet__0F824689]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Sector] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__28B808A7]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_dim_SachetPack] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__670A40DB]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_dim_RespTR] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__5F691F13]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_dim_RespMerch] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__2116E6DF]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_dim_PriceSegment] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__2E06CDA9]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_dim_NetType] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__2665ABE1]    Script Date: 07/14/2014 19:55:12 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Net] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_d__Delet__236943A5]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_dim_TypeStaff] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_d__Delet__1AD3FDA4]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_dim_SubChannel] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_d__Delet__1F98B2C1]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_dim_NetType] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_d__Delet__2180FB33]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_dim_Motivation] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_d__Delet__1DB06A4F]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_dim_Format] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_d__Delet__18EBB532]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_dim_Channel] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__DateC__48EFCE0F]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_SlaveGood] ADD  DEFAULT (getdate()) FOR [DateChange]
GO
/****** Object:  Default [DF__lasmart_t__isSyn__52793849]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_SlaveGood] ADD  DEFAULT ((0)) FOR [isSynced]
GO
/****** Object:  Default [DF__lasmart_t__Delet__3A179ED3]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Manufacturer] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__1975C517]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_InternalPack] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__11D4A34F]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_GiftType] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__0A338187]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Gift] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__1EC48A19]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Format] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__02925FBF]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Flavor] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__7AF13DF7]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_ExternalPack] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__17236851]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Division] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__73501C2F]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Country] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__636EBA21]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_ContractorGroup] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__6BAEFA67]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_CoffeeSort] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__35A7EF71]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Channel] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__52E34C9D]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Category] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__42ACE4D4]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Brand] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__5B78929E]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Segment] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__3E3D3572]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_SubChannel] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__640DD89F]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_SubSegment] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__Delet__4B422AD5]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_SubBrand] ADD  DEFAULT ((0)) FOR [Deleted]
GO
/****** Object:  Default [DF__lasmart_t__DateU__5090EFD7]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_sync_MasterSlave] ADD  DEFAULT (getdate()) FOR [DateUpdate]
GO
/****** Object:  ForeignKey [FK__lasmart_t__Manuf__41B8C09B]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Brand]  WITH CHECK ADD FOREIGN KEY([ManufacturerID])
REFERENCES [dbo].[lasmart_t_dim_Manufacturer] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Categ__5A846E65]    Script Date: 07/14/2014 19:55:44 ******/
ALTER TABLE [dbo].[lasmart_t_dim_Segment]  WITH CHECK ADD FOREIGN KEY([CategoryID])
REFERENCES [dbo].[lasmart_t_dim_Category] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Chann__3D491139]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_SubChannel]  WITH CHECK ADD FOREIGN KEY([ChannelID])
REFERENCES [dbo].[lasmart_t_dim_Channel] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Segme__6319B466]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_SubSegment]  WITH CHECK ADD FOREIGN KEY([SegmentID])
REFERENCES [dbo].[lasmart_t_dim_Segment] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Brand__4A4E069C]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_SubBrand]  WITH CHECK ADD FOREIGN KEY([BrandID])
REFERENCES [dbo].[lasmart_t_dim_Brand] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Chann__5026DB83]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([ChannelID])
REFERENCES [dbo].[lasmart_t_dim_Channel] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Contr__511AFFBC]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([ContractorGroupID])
REFERENCES [dbo].[lasmart_t_dim_ContractorGroup] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Count__520F23F5]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([CountryID])
REFERENCES [dbo].[lasmart_t_dim_Country] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Divis__5303482E]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([DivisionID])
REFERENCES [dbo].[lasmart_t_dim_Division] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Forma__53F76C67]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([FormatID])
REFERENCES [dbo].[lasmart_t_dim_Format] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__NetID__54EB90A0]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([NetID])
REFERENCES [dbo].[lasmart_t_dim_Net] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__NetTy__55DFB4D9]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([NetTypeID])
REFERENCES [dbo].[lasmart_t_dim_NetType] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Secto__56D3D912]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([SectorID])
REFERENCES [dbo].[lasmart_t_dim_Sector] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__SubCh__57C7FD4B]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterStores]  WITH CHECK ADD FOREIGN KEY([SubChannelID])
REFERENCES [dbo].[lasmart_t_dim_SubChannel] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Brand__062DE679]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([BrandID])
REFERENCES [dbo].[lasmart_t_dim_Brand] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Categ__07220AB2]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([CategoryID])
REFERENCES [dbo].[lasmart_t_dim_Category] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Coffe__08162EEB]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([CoffeeSortID])
REFERENCES [dbo].[lasmart_t_dim_CoffeeSort] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Count__090A5324]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([CountryID])
REFERENCES [dbo].[lasmart_t_dim_Country] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Exter__09FE775D]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([ExternalPackID])
REFERENCES [dbo].[lasmart_t_dim_ExternalPack] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__GiftI__0AF29B96]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([GiftID])
REFERENCES [dbo].[lasmart_t_dim_Gift] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__GiftT__0BE6BFCF]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([GiftTypeID])
REFERENCES [dbo].[lasmart_t_dim_GiftType] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Inter__0CDAE408]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([InternalPackID])
REFERENCES [dbo].[lasmart_t_dim_InternalPack] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Manuf__0DCF0841]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([ManufacturerID])
REFERENCES [dbo].[lasmart_t_dim_Manufacturer] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Paren__0EC32C7A]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([ParentID])
REFERENCES [dbo].[lasmart_t_dim_MasterGood] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Price__0FB750B3]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([PriceSegmentID])
REFERENCES [dbo].[lasmart_t_dim_PriceSegment] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Sache__10AB74EC]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([SachetPackID])
REFERENCES [dbo].[lasmart_t_dim_SachetPack] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Segme__119F9925]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([SegmentID])
REFERENCES [dbo].[lasmart_t_dim_Segment] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Statu__1293BD5E]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([StatusID])
REFERENCES [dbo].[lasmart_t_sync_PositionStatus] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__SubBr__1387E197]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([SubBrandID])
REFERENCES [dbo].[lasmart_t_dim_SubBrand] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__SubSe__147C05D0]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([SubSegmentID])
REFERENCES [dbo].[lasmart_t_dim_SubSegment] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__TeaCo__15702A09]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([TeaColorID])
REFERENCES [dbo].[lasmart_t_dim_TeaColor] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Weigh__16644E42]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_dim_MasterGood]  WITH CHECK ADD FOREIGN KEY([WeightGroupID])
REFERENCES [dbo].[lasmart_t_dim_WeightGroup] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Flavo__60C757A0]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_good_flavor]  WITH CHECK ADD FOREIGN KEY([FlavorID])
REFERENCES [dbo].[lasmart_t_dim_Flavor] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__GoodI__5FD33367]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_good_flavor]  WITH CHECK ADD FOREIGN KEY([GoodID])
REFERENCES [dbo].[lasmart_t_dim_MasterGood] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__GoodI__74643BF9]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_good_barcode]  WITH CHECK ADD FOREIGN KEY([GoodID])
REFERENCES [dbo].[lasmart_t_dim_MasterGood] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Addin__5DEAEAF5]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_good_adding]  WITH CHECK ADD FOREIGN KEY([AddingID])
REFERENCES [dbo].[lasmart_t_dim_Adding] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__GoodI__5CF6C6BC]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_good_adding]  WITH CHECK ADD FOREIGN KEY([GoodID])
REFERENCES [dbo].[lasmart_t_dim_MasterGood] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Maste__4EA8A765]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_sync_MasterSlave]  WITH CHECK ADD FOREIGN KEY([MasterGoodID])
REFERENCES [dbo].[lasmart_t_dim_MasterGood] ([ID])
GO
/****** Object:  ForeignKey [FK__lasmart_t__Slave__4F9CCB9E]    Script Date: 07/14/2014 19:55:47 ******/
ALTER TABLE [dbo].[lasmart_t_sync_MasterSlave]  WITH CHECK ADD FOREIGN KEY([SlaveGoodID])
REFERENCES [dbo].[lasmart_t_dim_SlaveGood] ([ID])
GO

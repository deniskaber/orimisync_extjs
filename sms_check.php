﻿<?php
require_once 'conf_info.php';
session_start ();
if ($_SESSION ['HTTP_USER_AGENT'] !== md5($_SERVER ['HTTP_USER_AGENT']))
{
	session_destroy();
	header ('Location: index.php');
	die();
}
if (isset ($_SESSION['code']) && isset ($_POST['submit']) && !empty ($_POST['code']))
{
	if ($_SESSION['code'] == strip_tags(trim($_POST['code'])))
	{
		$connect = mssql_connect ($_PAW['host'], $_PAW['login'], $_PAW['pass']) or die ("Could not connect to MSSQL ");
		mssql_select_db ($_PAW['db'],$connect);
		$idUser = $_SESSION ['idUser'];
		$query = "IF EXISTS (SELECT TOP 1 [idUser],[LoggedOn],[UserIp],[UserAgent],[SID] FROM [AdminDB].[dbo].[active_users] where [idUser] = $idUser) Select 'true' else select 'false'";
		$res = mssql_query ($query);
		while ($row = mssql_fetch_row($res)) {
			$logged = $row[0];
		}
		if ($logged == 'false')
		{
			if ($_SESSION ['admin']!==2)
			{
				$db_name = $_SESSION ['db_name'];
				$idUser = $_SESSION ['idUser'];
				$UserIP = $_SESSION ['UserIP'];
				$UserAgent = $_SESSION ['HTTP_USER_AGENT'];
				$SID = session_id();
				$event = iconv("UTF-8", "cp1251", "Вход в систему"); 
				$query = "insert into [Log].[dbo].[UserHistory]([DB],[User],[UserIP],[Event]) select '$db_name','$idUser','$UserIP','$event'";
				mssql_query($query,$connect);
				$_SESSION['auth'] = 1;
				unset($_SESSION['code']);
				$query = "SELECT TOP 1 [SKU],[ISG],[DYN],[SOD],[CHART] FROM [RightsOnReports].[dbo].[v_subscriptions_RightsOnReport] where [IDSubscription] = ".$_SESSION ['idS'];
				$result = mssql_query($query,$connect);
				while ($row = mssql_fetch_row($result)) {
					$_SESSION ['REPORT_access'] = implode(",", $row);
				}
				//$query = "insert into active_users ([idUser],[LoggedOn],[UserIp],[UserAgent],[SID]) select $idUser,getdate(),'$UserIP','$UserAgent','$SID'";
				//mssql_query($query);
				//ob_start();print_r($_REQUEST);print_r($_SESSION);echo "\n\n".$query;file_put_contents('out_save.txt',ob_get_clean());
				mssql_close($connect);
				header ('Location: portal.php');
				die();
			}
			else
			{
				mssql_close($connect);
				unset($_SESSION['code']);
				header ('Location: selectDB.php');
				die();
			}
		}
	}
	session_destroy();
	header ('Location: index.php');
	die();
}
if (!isset ($_SESSION['code']))
{
	session_destroy();
	header ('Location: index.php');
	die();
}

else
{
?>
<!DOCTYPE html>
<html>
	<head>
		<!--<meta http-equiv="Content-Language" content="ru">-->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>SMS-код. Информационный портал Retailds.ru</title>
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
	<table id="main">
	<tr>
	<td valign="middle" style="text-align:center;">
	<div id="wrapper">
		<form action="" method="post" style="margin: 80px 0 18px;"><!-- class="well form-inline"-->
			<fieldset>
				<!--<legend>Введите отправленый код, session code  <?php echo $_SESSION['code']; ?></legend>-->
				<legend>На Ваш номер <?php echo $_SESSION ['ph']; ?><br />отправлен код проверки</legend>
				
				<div class="control-group">
					<label for="code" class="control-label">Код:</label>
					<div class="controls">
						<input type="text" name="code" id="code" class="input-medium" placeholder="SMS-код"/>
					</div>
				</div>
				
				<div class="form-actions">
					<input type="submit" name="submit" value="" class="btn btn-primary"/>
				</div>
			</fieldset>
		</form>
	</div>
	</td>
	</tr>
	</table>
	</body>
</html>
<?php
}
?>
